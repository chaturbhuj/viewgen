<?php
    require __DIR__ . '/../vendor/autoload.php';

    function getParameters($url)
    {
        $parts = explode("?", $url, 2);
        $query_string = $parts[1];
        $parts = explode("&", $query_string);
        $params = [];
        foreach ($parts as $part){
            $part = explode('=', $part);
            $name = trim($part[0]);
            $value = trim($part[1]);
            if (! isset($params[$name])) $params[$name] = [];
            $params[$name][] = $value;
        }
        return $params;
    }
    
    function getFacets($url, $filter)
    {
        $params = getParameters($url);
        
        $facets = [];
        foreach ($params as $name => $values) {
            foreach($values as $pureName){
                var_dump($name .'|' .$pureName);
                if ($name == 'facet' && in_array($pureName, $filter)) {
                    $facets[$pureName] = [];
                }
            }
        }

        foreach ($facets as $pureName => $_) {
            $facetValueName = 'facet.' . $pureName;
            if (isset($params[$facetValueName])) {
                $facets[$pureName] = $params[$facetValueName];
            }
        }

        return $facets;
    }
    
    var_dump(getFacets('https://www.linkedin.com/sales/search?keywords=(%22manager%22%20OR%20%22director%22%20OR%20%22cto%22%20OR%20%22ceo%22)%20%20%20AND%20NOT%20(%22fisher%22%20OR%20%22housekeeper%22)&facet=G&facet=I&facet=YC&facet=TE&facet.G=us%3A0&facet.I=80&facet.I=96&facet.I=43&facet.I=4&facet.I=44&facet.I=98&facet.I=33&facet.I=99&facet.I=6&facet.I=28&facet.I=140&facet.I=27&facet.I=42&facet.I=15&facet.I=47&facet.I=137&facet.I=126&facet.I=34&facet.I=115&facet.I=104&facet.I=110&facet.I=19&facet.I=36&facet.I=113&facet.I=48&facet.I=25&facet.I=57&facet.I=8&facet.I=53&facet.I=88&facet.I=12&facet.I=17&facet.I=39&facet.I=35&facet.I=46&facet.I=41&facet.I=116&facet.I=32&facet.I=97&facet.I=91&facet.I=50&facet.I=128&facet.I=38&facet.I=105&facet.I=18&facet.I=1&facet.I=135&facet.I=121&facet.I=106&facet.I=45&facet.I=52&facet.I=112&facet.I=59&facet.I=84&facet.I=23&facet.I=142&facet.I=136&facet.I=109&facet.I=133&facet.I=118&facet.I=127&facet.I=24&facet.I=129&facet.I=51&facet.I=5&facet.I=49&facet.I=111&facet.I=119&facet.I=3&facet.I=55&facet.I=26&facet.I=7&facet.I=138&facet.I=81&facet.I=56&facet.I=147&facet.I=60&facet.I=63&facet.I=108&facet.I=117&facet.I=22&facet.I=61&facet.I=114&facet.I=87&facet.I=65&facet.I=120&facet.I=21&facet.I=66&facet.I=64&titleScope=CURRENT&jobTitleEntities=developer%20AND%20NOT%20(%22magician%22%20OR%20%22student%22)&jobTitleEntities=director%20AND%20NOT%20(%22magician%22%20OR%20%22student%22)&facet.YC=3&facet.TE=4&count=25&start=0&updateHistory=true&searchHistoryId=2015660374&trackingInfoJson.contextId=73E9A18C6767E514C0208B82F92A0000',
        ['I', 'YC', 'YP', 'TE']));
    
    ?>