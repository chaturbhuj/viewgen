<?php

include('header.php');

if (!isset($_GET['company_id']) || !isset($_GET['search_url_id'])) {
	die;
}
function myJsonDecode($jsonText){
	$jsonObject = json_decode('["'.$jsonText.'"]',true);
	return $jsonObject[0];
}


$query = DB::prep("select * from people_list where search_url_id = ?");
$query->execute(array($_GET['search_url_id']));
if ($query->rowCount() == 0) die;

$peopleLists = $query->fetchAll();
?>

<table border=1>
<? foreach ($peopleLists as $peopleList) { ?>
	<tr>
		<td><?=$peopleList["id"]?></td>
		<td><?=$peopleList["search_url_id"]?></td>
		<td><?=$peopleList["html"] != ""?"<a href=debugListPage.php?company_id=".$_GET["company_id"]."&people_list_id=".$peopleList["id"].">HTML</a>":""?></td>
		<td><?=$peopleList["url"]?></td>
		<td><?=$peopleList["last_list"]?></td>
	</tr>
<? } ?>
</table>

