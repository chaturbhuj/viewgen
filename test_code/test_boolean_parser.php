<?php
    require __DIR__ . '/../vendor/autoload.php';
    
    $parser = new Icecave\Dialekt\Parser\ExpressionParser(null, false);
    $renderer = new Icecave\Dialekt\Renderer\ExpressionRenderer;

    $expressionString = '(ceo OR cto AND executive OR "mad joker" ) AND NOT (food OR beverage OR "mad food")';
    #$expressionString = 'data OR analytic OR (analysis AND thinking) OR "love OR extremely" OR driven OR data';
    #$expressionString = 'data AND analytic (analysis AND thinking) AND x';

    $expression = $parser->parse($expressionString);

    if ($expression instanceof Icecave\Dialekt\AST\LogicalOr) {
        $ORArray = [];
        foreach ($expression->children() as $c) {
            $ORArray[] = $renderer->render($c);
        }
        echo 'OR array\n';
        var_dump($ORArray);
    
        die;
    }
    
    echo var_export(trySkipLogicSplit($renderer, $expression), true);
    die;
    
    //this one is needed for SKIP logic splitting in case first part is OR expression
    if ($expression instanceof Icecave\Dialekt\AST\LogicalAnd){
        $children = $expression->children();
        if (count($children) === 2 && $children[1] instanceof Icecave\Dialekt\AST\LogicalNot){
            $skips = $children[1]->child();  //has only one child and only this method
            $includes = $children[0];
            echo 'SKIP Logic: Items to include' . PHP_EOL;
            var_dump($renderer->render($includes));
    
            echo 'SKIP Logic: Items to exclude' . PHP_EOL;
            var_dump($renderer->render($skips));
            
            if ($includes instanceof Icecave\Dialekt\AST\LogicalOr){
                echo 'Splitted: ' . PHP_EOL;
                var_dump(makeSplit($renderer, $includes, $skips));
                die;
            }
            
            echo 'Non-splittable SKIP construction (no ORs in the include section)';
            die;
            
        }
            
    }
    
    echo 'There are no recognizable types of expression\n';
    
    function trySkipLogicSplit($renderer, $expression){
        //this one is needed for SKIP logic splitting in case first part is OR expression
        if ($expression instanceof Icecave\Dialekt\AST\LogicalAnd){
            $children = $expression->children();
            if (count($children) === 2 && $children[1] instanceof Icecave\Dialekt\AST\LogicalNot){
                $skips = $children[1]->child();  //has only one child and only this method
                $includes = $children[0];
                //echo 'SKIP Logic: Items to include' . PHP_EOL;
                $s1 = var_export($renderer->render($includes), true);
        
                //echo 'SKIP Logic: Items to exclude' . PHP_EOL;
                $s2 = var_export($renderer->render($skips), true);
                
                if ($includes instanceof Icecave\Dialekt\AST\LogicalOr){
                    //echo 'Splitted: ' . PHP_EOL;
                    $or_array = [];
                    $skips = $renderer->render($skips);
                    foreach ($includes->children() as $c) {
                        $or_array[] = $renderer->render($c) . ' AND NOT ' . $skips;
                    }
                    return $or_array;
                }
                //echo 'Non-splittable SKIP construction (no ORs in the include section)'
            }
                
        }
        return false;
    }
    
    
    function makeSplit($renderer, $ORExpr, $SkipExpr){
        $ORArray = [];
        $skips = $renderer->render($SkipExpr);
        foreach ($ORExpr->children() as $c) {
            $ORArray[] = $renderer->render($c) . ' AND NOT ' . $skips;
        }
        return '(' . implode(') OR (', $ORArray) . ')';
    }
    ?>