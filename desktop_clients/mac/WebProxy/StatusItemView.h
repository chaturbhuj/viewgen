//
//  StatusItemView.h
//  WebProxy
//
//  Created by macbook on 2014-05-02.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>

#define STATUS_OFF 1
#define STATUS_WAITING 2
#define STATUS_ON 3

@interface StatusItemView : NSView {
@private

    // These images are shown in the menu.
    NSImage *_imageOnInactive;
    NSImage *_imageOnActive;
    NSImage *_imageOffInactive;
    NSImage *_imageOffActive;
    NSImage *_imageWaitingInactive;
    NSImage *_imageWaitingActive;

    NSStatusItem *_statusItem;
    BOOL _isHighlighted;
    int mStatus;
    SEL _action;
    __unsafe_unretained id _target;
}

- (id)initWithStatusItem:(NSStatusItem *)statusItem;

@property (nonatomic, strong, readonly) NSStatusItem *statusItem;

@property (nonatomic, strong) NSImage *mImageOnInactive;
@property (nonatomic, strong) NSImage *mImageOnActive;
@property (nonatomic, strong) NSImage *mImageOffInactive;
@property (nonatomic, strong) NSImage *mImageOffActive;
@property (nonatomic, strong) NSImage *mImageWaitingInactive;
@property (nonatomic, strong) NSImage *mImageWaitingActive;

@property (nonatomic, setter = setHighlighted:) BOOL isHighlighted;
@property (nonatomic, readonly) NSRect globalRect;
@property (nonatomic) SEL action;
@property (nonatomic, unsafe_unretained) id target;

-(void) setStatus:(int) status;

@end

