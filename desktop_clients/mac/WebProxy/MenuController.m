//
//  MenuController.m
//  WebProxy
//
//  Created by macbook on 2014-05-02.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "MenuController.h"
#import "StatusItemView.h"

@implementation MenuController

@synthesize mStatusItemView;

#pragma mark -

- (id)init {
    self = [super init];
    if (self != nil)
    {
        // Install status item into the menu bar
        NSStatusItem *statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:STATUS_ITEM_VIEW_WIDTH];
        mStatusItemView = [[StatusItemView alloc] initWithStatusItem:statusItem];
        
        mStatusItemView.mImageOnInactive = [NSImage imageNamed:@"status_on_inactive"];
        mStatusItemView.mImageOnActive = [NSImage imageNamed:@"status_on_active"];
        mStatusItemView.mImageWaitingInactive = [NSImage imageNamed:@"status_waiting_inactive"];
        mStatusItemView.mImageWaitingActive = [NSImage imageNamed:@"status_waiting_active"];
        mStatusItemView.mImageOffInactive = [NSImage imageNamed:@"status_off_inactive"];
        mStatusItemView.mImageOffActive = [NSImage imageNamed:@"status_off_active"];
        [mStatusItemView setNeedsDisplay:YES];
        mStatusItemView.action = @selector(togglePanel:);
    }
    return self;
}

- (void)dealloc
{
    [[NSStatusBar systemStatusBar] removeStatusItem:self.statusItem];
}

#pragma mark -
#pragma mark Public accessors

- (NSStatusItem *)statusItem
{
    return self.mStatusItemView.statusItem;
}

#pragma mark -

- (BOOL)hasActiveIcon
{
    return self.mStatusItemView.isHighlighted;
}

- (void)setHasActiveIcon:(BOOL)flag
{
    self.mStatusItemView.isHighlighted = flag;
}

@end
