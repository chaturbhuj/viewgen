//
//  MenuController.h
//  WebProxy
//
//  Created by macbook on 2014-05-02.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define STATUS_ITEM_VIEW_WIDTH 24.0

@class StatusItemView;

@interface MenuController : NSObject {
@private
    StatusItemView *mStatusItemView;
}

@property (nonatomic) BOOL hasActiveIcon;
@property (nonatomic, strong, readonly) NSStatusItem *statusItem;
@property (nonatomic, strong) StatusItemView *mStatusItemView;

@end

