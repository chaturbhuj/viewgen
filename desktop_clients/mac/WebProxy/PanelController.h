
#import "BackgroundView.h"
#import "CredentialWindow.h"
#import "LinkedInCredentials.h"
#import "ClientProperties.h"
#import "ProxyWebView.h"
#import "LiStatusIcon.h"

#define FONT_LATO_LIGHT(s) [NSFont fontWithName:@"Lato light" size:s]

@class PanelController;

@protocol PanelControllerDelegate <NSObject>

@optional
- (id)statusItemViewForPanelController:(PanelController *)controller;

@end

@interface PanelController : NSWindowController <NSWindowDelegate> {
    
    NSString *mBaseUrl;
    
    CredentialWindow *mCredentialWindow;
    LinkedInCredentials *mLinkedInCredentialsWindow;
    BOOL mMainWindowIsOpen;
    BOOL _hasActivePanel;
    __unsafe_unretained BackgroundView *_backgroundView;
    __unsafe_unretained id<PanelControllerDelegate> _delegate;

    NSMutableDictionary *mInfo;
    BOOL panelIsExpanded;
}

@property (weak) IBOutlet NSProgressIndicator *SaveKeyIndicator;
@property (nonatomic, unsafe_unretained) IBOutlet BackgroundView *backgroundView;

@property (weak) IBOutlet LiStatusIcon *LiStatusIcon;
@property (nonatomic) BOOL hasActivePanel;
@property (nonatomic, unsafe_unretained, readonly) id<PanelControllerDelegate> delegate;
@property (weak) IBOutlet NSButton *SearchQuantNetButton;
@property (weak) IBOutlet NSTextField *LIUserText;
@property (weak) IBOutlet NSTextField *SQUserText;
@property (weak) IBOutlet NSTextField *StatusText;
@property (weak) IBOutlet NSButton *CredentialsButton;
@property (weak) IBOutlet NSTextField *ApiKeyLabel;
@property (weak) IBOutlet NSButton *ShowMainWindowButton;
@property (weak) IBOutlet NSView *SQKeyView;
@property (weak) IBOutlet NSTextField *SQKeyText;
@property (weak) IBOutlet NSTextField *InfoTextField;
@property (weak) IBOutlet NSButton *SaveKeyButton;

- (void)openLinkedInCredentialsWindow;

- (id)initWithDelegate:(id<PanelControllerDelegate>)delegate;
- (NSRect)statusRectForWindow:(NSWindow *)window;
- (void)openPanel;
- (void)closePanel;

- (void) expandPanel;
- (void) contractPanel;

- (IBAction)ManageClicked:(id)sender;
- (IBAction)ChangeCredentialsClicked:(id)sender;
- (IBAction)SQUserClicked:(id)sender;
- (IBAction)SQSaveKeyClicked:(id)sender;
- (IBAction)OpenMainWindowClicked:(id)sender;
- (IBAction)SearchQuantNetLink:(id)sender;
- (IBAction)QuitApp:(id)sender;
- (IBAction)ChangeLinkedInCredentialsClicked:(id)sender;

- (void) updateInfoObject: (NSDictionary *) info;
- (void) updateClientHash;

- (void)postUri: (NSString *) uri withJson: (NSMutableDictionary *) data onCompleted:(void (^)(NSData *data, NSError *error)) handler;

+(id)hyperlinkFromString:(NSString*)inString withURL:(NSURL*)aURL;

@end
