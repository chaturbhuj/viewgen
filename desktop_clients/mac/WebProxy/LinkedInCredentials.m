//
//  LinkedInCredentials.m
//  SearchQuant
//
//  Created by Josef Cullhed on 2016-02-03.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LinkedInCredentials.h"
#import "ClientProperties.h"
#import "AppDelegate.h"

@implementation LinkedInCredentials
@synthesize mUsername;
@synthesize mPassword;

- (LinkedInCredentials *) init {
    self = [super initWithWindowNibName:@"LinkedInCredentials"];
    
    if (self) {
        
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Make a fully skinned panel
    NSWindow *wnd = (id)[self window];
    [wnd makeKeyWindow];
    [wnd setLevel:NSPopUpMenuWindowLevel];
    [wnd orderFront:self];
    [wnd setOpaque:NO];
}

- (IBAction)SaveCredentialsClicked:(id)sender {
    [[ClientProperties getSingleton] setObject:[mUsername stringValue] forKey:@"LinkedInUsername"];
    [[ClientProperties getSingleton] setObject:[mPassword stringValue] forKey:@"LinkedInPassword"];
    
    NSAlert *alert = [NSAlert alertWithMessageText:@"Your LinkedIn credentials have been saved and will be used to automatically log you back in should you become logged out for any reason." defaultButton:@"Ok" alternateButton:nil otherButton:nil informativeTextWithFormat:@""];
    [alert runModal];
    
    [[self window] close];
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] reschedule];
}

- (void) openWindow {
    
    [[NSApplication sharedApplication] activateIgnoringOtherApps : YES];
    [[self window] makeKeyWindow];
    [[self window] setLevel:NSScreenSaverWindowLevel];
    [[self window] orderFront:self];
    [[self window] setOpaque:NO];
    [[self window] setLevel:NSNormalWindowLevel];
    [[self window] makeKeyWindow];
    
    ClientProperties *clientProp = [ClientProperties getSingleton];
    
    if ([clientProp objectForKey:@"LinkedInUsername"] != nil) {
        [mUsername setStringValue:[clientProp objectForKey:@"LinkedInUsername"]];
    }
    if ([clientProp objectForKey:@"LinkedInPassword"] != nil) {
        [mPassword setStringValue:[clientProp objectForKey:@"LinkedInPassword"]];
    }
    
    [[self window] makeKeyAndOrderFront:nil];
    
}
@end
