//
//  ProxyWebView.m
//  Test
//
//  Created by macbook on 2014-04-27.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ProxyWebView.h"
#import "AppDelegate.h"

/*
 Statuses
 The tool is running normally
 The tool is running but your internet connection is down
 The tool is not running, click the sign in button above
 */

@implementation ProxyWebView

- (ProxyWebView *) init {

    self = [super init];
    
    if (!self) return nil;
    
    mBaseUrl = @"http://dev.searchquant.net/api/";
    if ([[ClientProperties getSingleton] objectForKey:@"base_url"] != nil) {
        mBaseUrl = [[ClientProperties getSingleton] objectForKey:@"base_url"];
    }
    
    mVersion = @"Mac 1.0";
    if ([[ClientProperties getSingleton] objectForKey:@"version"] != nil) {
        mVersion = [[ClientProperties getSingleton] objectForKey:@"version"];
    }
    
    // Default values.
    mClientHash = [[ClientProperties getSingleton] objectForKey:@"client_hash"];
    
    mCompanyName = @"";
    NSLog(@"USERNAME: %@", [[ClientProperties getSingleton] objectForKey:@"username"]);
    mPayload = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[ClientProperties getSingleton] objectForKey:@"company_name"], @"company_name", [[ClientProperties getSingleton] objectForKey:@"username"], @"username", nil];
    mDelta = P_ERROR_TIMEOUT;
    mProbeTimer = nil;
    mScrollTimer = nil;
    [self setFrameLoadDelegate:self];
    
    mStatusBar = [[StatusBar alloc] initWithFrame:[self frame]];
    
    [self addSubview:mStatusBar];
    [self setAutoresizesSubviews:YES];
    
    return self;
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame*)frame {
    @try {
        // Check if this is the main frame.
        if (frame == [sender mainFrame]) {
            NSLog(@"DID LOAD FRAME");
            if (mWebTimer) [mWebTimer invalidate];
            
            if ([mPayload valueForKey:@"javascript"] != nil) {

                [mStatusBar startTimer:@"Executing javascript"];
                NSString *javascriptResult = [sender stringByEvaluatingJavaScriptFromString:[mPayload valueForKey:@"javascript"]];
                
                [mPayload setValue:@"1" forKey:@"isJavascriptRequest"];
                [self handleData: javascriptResult];

            } else {

                // Find html source.
                NSData *data = [[frame dataSource] data];
                NSString *html = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
                
                [mPayload setValue:@"0" forKey:@"isJavascriptRequest"];
                [self handleData: html];
            }
        }
    } @catch (NSException *e) {
        [self scheduleProbe:P_ERROR_TIMEOUT];
    }
}

- (void)webView:(WebView *) sender didFailLoadWithError: (WebFrame *)frame {
    @try {
        // Check if this is the main frame.
        if (frame == [sender mainFrame]) {
            NSLog(@"DID FAIL FRAME");
            if (mWebTimer) [mWebTimer invalidate];
            [self scheduleProbe:P_ERROR_TIMEOUT];
        }
    } @catch (NSException *e) {
        [self scheduleProbe:P_ERROR_TIMEOUT];
    }
}

- (void)handleData: (NSString *) html {
    
    if ([self handleLoginForm:html]) {
        return;
    }
    
    if ([self checkForLogout:html]) {
        [self initiateLogin];
        return;
    }
    
    [mPayload setObject:html forKey:@"html"];
    [mPayload setObject:mVersion forKey:@"version"];
    
    [mPayload removeObjectForKey:@"CrawledBy"];
    [mPayload removeObjectForKey:@"username"];
    [mPayload removeObjectForKey:@"javascript"];

    [mStatusBar startTimer:@"Sending data to the server,"];
    [self postUri:@"save.php" withJson:mPayload onCompleted:^(NSDictionary *res) {
        [mPayload removeObjectForKey:@"html"];
        
        if (!res) {
            [self scheduleProbe:P_ERROR_TIMEOUT];
            return;
        }
        
        NSString *status = [res objectForKey:@"status"];
        
        if ([status isEqualToString:@"error"]) {
            [self scheduleProbe:P_ERROR_TIMEOUT];
            return;
        }
        
        if ([[res objectForKey:@"payload"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *pl = [[NSMutableDictionary alloc] initWithDictionary:[res objectForKey:@"payload"]];
            [mPayload addEntriesFromDictionary:pl];
        } else {
            [mDelegate setInfoObject:mPayload];
            [self scheduleProbe:P_ERROR_TIMEOUT];
            return;
        }
        
        NSLog(@"%@", mPayload);
        
        [[ClientProperties getSingleton] setObject:[mPayload objectForKey:@"company_name"] forKey:@"company_name"];
        
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        [appDelegate->mPanelController updateInfoObject:[[NSDictionary alloc] initWithObjectsAndKeys:
            [[ClientProperties getSingleton] objectForKey:@"company_name"], @"company_name", nil]];
        
        NSString *visits = [mPayload objectForKey:@"VisitedProfiles24"];
        if (visits && [visits intValue] > 0) {
            [mStatusBar setVisits:[visits intValue]];
        }
        
        NSString *delta = [mPayload objectForKey:@"WaitBetweenConnections"];
        if (delta && [delta intValue] > 0) {
            mDelta = [delta intValue];
        }
        
        [mDelegate setInfoObject:mPayload];
        
        NSLog(@"Delta: %d", mDelta);
        
        [self scheduleProbe: mDelta];
    }];
}

- (void)postUri: (NSString *) uri withJson: (NSMutableDictionary *) data onCompleted:(void (^)(NSDictionary *json)) handler {
    
    mClientHash = [[ClientProperties getSingleton] objectForKey:@"client_hash"];
    mCompanyName = [[ClientProperties getSingleton] objectForKey:@"company_name"];
    
    [data setObject:mCompanyName forKey:@"company_name"];
    [data setObject:mClientHash forKey:@"client_hash"];
    [data setObject:mVersion forKey:@"version"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
    
    if (error) {
        [self handleError:error];
        handler(nil);
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", mBaseUrl, uri];
	NSURL *urlObject = [NSURL URLWithString:url];
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:urlObject];
    
    [req setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:jsonData];
    [req setTimeoutInterval:60];
    
    NSLog(@"INVOKING REQUEST");
    
    mRequestTimer = [NSTimer scheduledTimerWithTimeInterval:P_REQUEST_TIMEOUT target:self selector:@selector(cancelRequest:) userInfo:nil repeats:NO];

    [NSURLConnection sendAsynchronousRequest:req
        queue:[NSOperationQueue mainQueue]
        completionHandler:^(NSURLResponse *response, NSData *resData, NSError *error) {
            @try {
                [mRequestTimer invalidate];
                mRequestTimer = nil;
                if (error != nil) {
                    [self handleError:error];
                    handler(nil);
                    return;
                }

                NSString *someString = [[NSString alloc] initWithData:resData encoding:NSASCIIStringEncoding];
                NSLog(@"DATA: %@", someString);

                if (error) {
                    [self handleError:error];
                    handler(nil);
                    return;
                }

                error = nil;
                NSDictionary *res = [NSJSONSerialization JSONObjectWithData:resData options:0 error:&error];

                if (error) {
                    [self handleError:error];
                    handler(nil);
                    return;
                }
                handler(res);
            } @catch (NSException *e) {
                [self scheduleProbe:P_ERROR_TIMEOUT];
            }
    }];
}

- (void)handleError: (NSError *) error {
    //[self postUri:@"error.php" withJson:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[error localizedDescription], @"message", nil]];
    NSLog(@"GOT ERROR %@", error);
    @try {
        [mStatusBar setErrorMessage: [error localizedDescription]];
        NSDictionary *status_message = [[NSDictionary alloc] initWithObjectsAndKeys:@"2", @"status", @"Offline, check internet connection",@"message", nil];
        NSDictionary *info = [[NSDictionary alloc ] initWithObjectsAndKeys:
            status_message, @"status_message", nil];
        
        [mPayload addEntriesFromDictionary:info];
        [mDelegate setInfoObject:mPayload];
    } @catch (NSException *e) {
        NSLog(@"%@", e);
    }
}

- (void)startProbe {
    /*DOMNodeList *links = [[self mainFrameDocument] getElementsByTagName:@"a"];
    for (int i = 0; i < [links length]; i++) {
        DOMNode *link = [links item:i];
        if ([link attr]) {
            
        }
    }*/
        
    //NSLog(@"mPayload=%@", mPayload);
    
    // Reset the url field. Just to make sure we get new urls.
    [mPayload removeObjectForKey:@"url"];
    [mPayload removeObjectForKey:@"status_message"];

    [mStatusBar startTimer:@"Waiting for server,"];
    [self postUri:@"probe.php" withJson:mPayload onCompleted:^(NSDictionary *res) {
        if (!res) {
            [self scheduleProbe: P_ERROR_TIMEOUT];
            return;
        }
        
        NSString *status = [res objectForKey:@"status"];
        if ([status isEqualToString:@"error"]) {
            [self scheduleProbe: P_ERROR_TIMEOUT];
            return;
        }
        
        if ([[res objectForKey:@"payload"] isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary *pl = [[NSMutableDictionary alloc] initWithDictionary:[res objectForKey:@"payload"]];
            //NSLog(@"BEFORE: %@", mPayload);
            [mPayload removeObjectForKey:@"people_id"];
            [mPayload removeObjectForKey:@"people_list_id"];

            [mPayload addEntriesFromDictionary:pl];
        } else {
            [self scheduleProbe:P_ERROR_TIMEOUT];
            return;
        }
        
        [mDelegate setInfoObject:mPayload];
        
        NSString *url = [mPayload objectForKey:@"url"];
        if (url == nil) {
            [self scheduleProbe:P_ERROR_TIMEOUT];
            return;
        }
        
        if ([url isEqualToString:@""]) {
            [self scheduleProbe:P_PROBE_TIMEOUT];
            return;
        }
        
        NSLog(@"Probe got %@", url);
        [self goToURL: url];
    }];
}

- (void)goToURL: (NSString *) url {
    mWebTimer = [NSTimer scheduledTimerWithTimeInterval:P_WEB_TIMEOUT target:self selector:@selector(cancelWebRequest:) userInfo:nil repeats:NO];
    [mStatusBar startTimer:@"Waiting for browser,"];
    
    NSString *referer = @"https://www.linkedin.com/nshome/";
    if ([mPayload objectForKey:@"referer"] != nil) {
        referer = [mPayload objectForKey:@"referer"];
    }
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    if (![referer isEqualToString:@""]) {
        [mStatusBar setReferer: referer];
        [urlRequest addValue:referer forHTTPHeaderField:@"Referer"];
    }
    [[self mainFrame] loadRequest:urlRequest];
}

- (void)scheduleProbe: (int) sec {
    if (mProbeTimer != nil) {
        if ([mProbeTimer isValid]) {
            [mProbeTimer invalidate];
            NSLog(@"CRITICAL trying to start probe with valid timer running.");
        }
    }
    NSLog(@"SCHEDULING PROBE IN %d SEC", sec);
    [mStatusBar setTimeout:@"Next request in" time:sec];
    mProbeTimer = [NSTimer scheduledTimerWithTimeInterval:sec
                                     target:self
                                   selector:@selector(executeTimer:)
                                   userInfo:nil
                                    repeats:NO];
    mScrollTimer = [NSTimer scheduledTimerWithTimeInterval:3
                                                   target:self
                                                 selector:@selector(executeScrollTimer:)
                                                 userInfo:nil
                                                  repeats:NO];
}

- (void)reschedule {
    if (mProbeTimer != nil) {
        [mProbeTimer invalidate];
    }
    if (mWebTimer != nil) {
        [mWebTimer invalidate];
    }
    [self stopLoading:self];
    
    mWebTimer = [NSTimer scheduledTimerWithTimeInterval:P_WEB_TIMEOUT target:self selector:@selector(cancelWebRequest:) userInfo:nil repeats:NO];
    [self setMainFrameURL:@"http://www.linkedin.com/"];

}

- (void)executeTimer:(NSTimer *) timer {
    [mProbeTimer invalidate];
    mProbeTimer = nil;
    @try {
        [self startProbe];
    } @catch (NSException *e) {
        NSLog(@"ERROR %@", e);
        [self scheduleProbe:P_ERROR_TIMEOUT];
    }
}

- (void)executeScrollTimer:(NSTimer *) timer {
    [mScrollTimer invalidate];
    mScrollTimer = nil;
    @try {
        //[self scrollPoint:NSMakePoint(0, 500)];
        //[self scrollPageDown:self];
    } @catch (NSException *e) {
        NSLog(@"ERROR %@", e);
    }
}

- (void)cancelWebRequest:(NSTimer *) timer {
    NSLog(@"STOPPING WEB BROWSER");
    [self stopLoading:self];
    [self scheduleProbe:P_ERROR_TIMEOUT];
}

- (void)cancelRequest:(NSTimer *) timer {
    NSLog(@"STOPPING REQUEST AFTER TIMEOUT");
    [self scheduleProbe:P_ERROR_TIMEOUT];
}

- (void) setDelegate: (id <ProxyWebViewDelegate>) delegate {
    mDelegate = delegate;
}

- (bool) checkForLogout: (NSString *) html {
    
    if ([html rangeOfString: @"<div id=\"page-title\"><h1>You have signed out</h1></div>"].location != NSNotFound) {
        return true;
    }
    if ([html rangeOfString: @"<label for=\"login-email\">"].location != NSNotFound) {
        return true;
    }
    return false;
}

- (void) initiateLogin {
    [self goToURL:@"https://www.linkedin.com/uas/login?goback=&trk=hb_signin"];
}

- (bool) handleLoginForm: (NSString *) html {
    
    // Check for credentials.
    ClientProperties *clientProp = [ClientProperties getSingleton];
    if ([clientProp objectForKey:@"LinkedInUsername"] == nil || [clientProp objectForKey:@"LinkedInPassword"] == nil) {
        return false;
    }
    
    // Handle first login form.
    if ([html rangeOfString: @"id=\"session_key-login\""].location != NSNotFound) {
        [self stringByEvaluatingJavaScriptFromString: [NSString stringWithFormat:@"var inputEmail = document.getElementById(\"session_key-login\"); inputEmail.value = \"%@\";", [clientProp objectForKey:@"LinkedInUsername"]]];
    
        [self stringByEvaluatingJavaScriptFromString: [NSString stringWithFormat:@"var inputPassword = document.getElementById(\"session_password-login\"); inputPassword.value = \"%@\";", [clientProp objectForKey:@"LinkedInPassword"]]];
    
        [self stringByEvaluatingJavaScriptFromString:@"var inputSubmit = document.getElementById(\"btn-primary\");"
            "inputSubmit.click();"];
        
        mWebTimer = [NSTimer scheduledTimerWithTimeInterval:P_LOGIN_TIMEOUT target:self selector:@selector(cancelWebRequest:) userInfo:nil repeats:NO];
        [mStatusBar startTimer:@"Waiting for login,"];
        
    } else {
        return false;
    }
    
    return true;
}

- (NSString *) getCrawledBy {
    return [mPayload objectForKey:@"CrawledBy"];
}


@end
