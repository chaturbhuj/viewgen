//
//  StatusItemView.m
//  WebProxy
//
//  Created by macbook on 2014-05-02.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "StatusItemView.h"
#import "AppDelegate.h"

@implementation StatusItemView

@synthesize statusItem = _statusItem;
@synthesize isHighlighted = _isHighlighted;
@synthesize action = _action;
@synthesize target = _target;

@synthesize mImageOnInactive = _imageOnInctive;
@synthesize mImageOnActive = _imageOnActive;
@synthesize mImageOffInactive = _imageOffInactive;
@synthesize mImageOffActive = _imageOffActive;
@synthesize mImageWaitingInactive = _imageWaitingInactive;
@synthesize mImageWaitingActive = _imageWaitingActive;

#pragma mark -

- (id)initWithStatusItem:(NSStatusItem *)statusItem
{
    CGFloat itemWidth = [statusItem length];
    CGFloat itemHeight = [[NSStatusBar systemStatusBar] thickness];
    NSRect itemRect = NSMakeRect(0.0, 0.0, itemWidth, itemHeight);
    self = [super initWithFrame:itemRect];
    
    if (self != nil) {
        _statusItem = statusItem;
        _statusItem.view = self;
        mStatus = STATUS_OFF;
    }
    return self;
}

#pragma mark -

- (void)drawRect:(NSRect)dirtyRect
{
	[self.statusItem drawStatusBarBackgroundInRect:dirtyRect withHighlight:self.isHighlighted];
    
    NSImage *icon = nil;
    if (mStatus == STATUS_ON) {
        icon = self.isHighlighted ? self.mImageOnActive : self.mImageOnInactive;
    } else if (mStatus == STATUS_WAITING) {
        icon = self.isHighlighted ? self.mImageWaitingActive : self.mImageWaitingInactive;
    } else {
        icon = self.isHighlighted ? self.mImageOffActive : self.mImageOffInactive;
    }

    NSSize iconSize = [icon size];
    NSRect bounds = self.bounds;
    CGFloat iconX = roundf((NSWidth(bounds) - iconSize.width) / 2);
    CGFloat iconY = roundf((NSHeight(bounds) - iconSize.height) / 2);
    NSPoint iconPoint = NSMakePoint(iconX, iconY);
    
	[icon drawAtPoint:iconPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

#pragma mark -
#pragma mark Mouse tracking

- (void)mouseDown:(NSEvent *)theEvent
{
    [NSApp sendAction:self.action to:self.target from:self];
}

#pragma mark -
#pragma mark Accessors

- (void)setHighlighted:(BOOL)newFlag
{
    if (_isHighlighted == newFlag) return;
    _isHighlighted = newFlag;
    [self setNeedsDisplay:YES];
}

#pragma mark -

- (NSRect)globalRect
{
    NSRect frame = [self frame];
    frame.origin = [self.window convertBaseToScreen:frame.origin];
    return frame;
}


-(void) setStatus:(int) status {
    mStatus = status;
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] setStatus:status];
    [self setNeedsDisplay:YES];
}

@end