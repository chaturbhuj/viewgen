//
//  StatusBar.h
//  SearchQuant
//
//  Created by macbook on 2014-06-06.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface StatusBar : NSView {

    NSTextView *mStatusText;
    
    NSString *mMessage;
    NSString *mReferer;
    
    NSTimer *mTimer;
    
    NSString *mError;
    
    int mTicks;
    int mVisits;
}

- (void) startTimer: (NSString *) message;
- (void) stopTimer;
- (void) setTimeout: (NSString *) message time: (int) sec;
- (void) timerTickUp:(NSTimer *) timer;
- (void) timerTickDown:(NSTimer *) timer;
- (void) updateMessage;
- (void) setReferer: (NSString *) referer;
- (void) setVisits: (int) visits;
- (void) setErrorMessage: (NSString *) message;

@end
