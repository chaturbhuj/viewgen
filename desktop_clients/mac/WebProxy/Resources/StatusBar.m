//
//  StatusBar.m
//  SearchQuant
//
//  Created by macbook on 2014-06-06.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "StatusBar.h"

@implementation StatusBar

- (id)initWithFrame:(NSRect)frameRect {
    self = [super initWithFrame:frameRect];
    if (self) {
        
        mVisits = 0;
        mTicks = 0;
        mError = @"";
        mReferer = @"";
        
        // Initialization code here.
        CGRect frame = CGRectMake(0, 0, frameRect.size.width, 50);
        [self setFrame:frame];
        
        CGRect textFrame = CGRectMake(0, 0, frameRect.size.width, 47);
        mStatusText = [[NSTextView alloc] initWithFrame:textFrame];
        [mStatusText setString:@"Waiting for server, 3 seconds - (536 visits last 24h)"];
        
        [self addSubview:mStatusText];
        [self setAutoresizesSubviews:YES];
        
        [mStatusText setAutoresizingMask:NSViewWidthSizable];
        [self setAutoresizingMask: NSViewWidthSizable];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    // Drawing code here.
    
    [[NSColor whiteColor] setFill];
    NSRectFill(dirtyRect);
    
    NSBezierPath *line = [NSBezierPath bezierPath];
    [line moveToPoint:NSMakePoint(NSMinX([self bounds]), 50)];
    [line lineToPoint:NSMakePoint(NSMaxX([self bounds]), 50)];
    [line setLineWidth:1.0]; /// Make it easy to see
    [[NSColor blackColor] set]; /// Make future drawing the color of lineColor.
    [line stroke];
    
    [super drawRect:dirtyRect];
}

- (void) startTimer: (NSString *) message {
    [self stopTimer];
    mMessage = message;
    mTicks = 0;
    mTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTickUp:) userInfo:nil repeats:YES];
    [self updateMessage];
}

- (void) stopTimer {
    if (mTimer == nil) return;
    [mTimer invalidate];
}

- (void) setTimeout: (NSString *) message time: (int) sec {
    [self stopTimer];
    mMessage = message;
    mTicks = sec;
    mTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTickDown:) userInfo:nil repeats:YES];
    [self updateMessage];
}

- (void)timerTickUp:(NSTimer *) timer {
    mTicks++;
    [self updateMessage];
}

- (void)timerTickDown:(NSTimer *) timer {
    if (mTicks == 0) {
        [mTimer invalidate];
        return;
    }
    mTicks--;
    [self updateMessage];
}

- (void) updateMessage {
    NSString *message = [NSString stringWithFormat:@"%@ %d seconds - (%d visits last 24h)",
                         mMessage, mTicks, mVisits];
    
    if (![mError isEqualToString:@""]) {
        message = [message stringByAppendingFormat:@" - Error: %@", mError];
    }
    
    message = [message stringByAppendingFormat:@"\nReferer: %@", mReferer];
    
    [mStatusText setString:message];
}

- (void) setReferer: (NSString *) referer {
    mReferer = referer;
}

- (void) setVisits: (int) visits {
    if (visits > mVisits) {
        mError = @"";
    }
    mVisits = visits;
    [self updateMessage];
}

- (void) setErrorMessage: (NSString *) message {
    mError = message;
    [self updateMessage];
}

@end
