
#import "PanelController.h"
#import "BackgroundView.h"
#import "StatusItemView.h"
#import "AppDelegate.h"
#import "NSAttributedString+Hyperlink.h"

#define OPEN_DURATION .15
#define CLOSE_DURATION .1

#define POPUP_HEIGHT 180
#define PANEL_WIDTH 480

#define MENU_ANIMATION_DURATION .1

@implementation PanelController

@synthesize SaveKeyIndicator;
@synthesize backgroundView = _backgroundView;
@synthesize LiStatusIcon;
@synthesize delegate = _delegate;
@synthesize SearchQuantNetButton;
@synthesize LIUserText;
@synthesize SQUserText;
@synthesize StatusText;
@synthesize CredentialsButton;
@synthesize ApiKeyLabel;
@synthesize ShowMainWindowButton;
@synthesize SQKeyView;
@synthesize SQKeyText;
@synthesize InfoTextField;
@synthesize SaveKeyButton;

- (id)initWithDelegate:(id<PanelControllerDelegate>)delegate {
    self = [super initWithWindowNibName:@"Panel"];
    if (self != nil)
    {
        mBaseUrl = @"http://searchquant.net/api/";
        if ([[ClientProperties getSingleton] objectForKey:@"base_url"] != nil) {
            mBaseUrl = [[ClientProperties getSingleton] objectForKey:@"base_url"];
        }
        mMainWindowIsOpen = NO;
        _delegate = delegate;
        mCredentialWindow = [[CredentialWindow alloc] init];
        panelIsExpanded = NO;
        mInfo = [[NSMutableDictionary alloc] init];
        mLinkedInCredentialsWindow = [[LinkedInCredentials alloc] init];
    }
    return self;
}

- (void)dealloc {
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Make a fully skinned panel
    NSPanel *panel = (id)[self window];
    
    /*LIUserText.font = FONT_LATO_LIGHT(14.0);
    SQUserText.font = FONT_LATO_LIGHT(14.0);
    StatusText.font = FONT_LATO_LIGHT(14.0);
    CredentialsButton.font = FONT_LATO_LIGHT(14.0);
    ApiKeyLabel.font = FONT_LATO_LIGHT(14.0);
    ShowMainWindowButton.font = FONT_LATO_LIGHT(14.0);
    SQKeyText.font = FONT_LATO_LIGHT(14.0);*/
    NSColor *color = [NSColor blueColor];
    
    NSMutableAttributedString *colorTitle =
        [[NSMutableAttributedString alloc] initWithAttributedString:[SearchQuantNetButton attributedTitle]];
    
    NSRange titleRange = NSMakeRange(0, [colorTitle length]);
    
    [colorTitle addAttribute:NSForegroundColorAttributeName value:color range:titleRange];
    
    [SearchQuantNetButton setAttributedTitle:colorTitle];
    
    [panel makeKeyWindow];
    [panel setAcceptsMouseMovedEvents:YES];
    [panel setLevel:NSPopUpMenuWindowLevel];
    [panel setOpaque:NO];
    [panel setStyleMask:NSBorderlessWindowMask];
    [panel setBackgroundColor:[NSColor clearColor]];
    [panel setHasShadow:YES];
    
    [SQKeyText setHidden:YES];
    [InfoTextField setHidden:YES];
    [SaveKeyButton setHidden:YES];
}

- (BOOL)hasActivePanel {
    return _hasActivePanel;
}

- (void)setHasActivePanel:(BOOL)flag {
    if (_hasActivePanel != flag)
    {
        _hasActivePanel = flag;
        
        if (_hasActivePanel)
        {
            [self openPanel];
        }
        else
        {
            [self closePanel];
        }
    }
}

- (void)windowWillClose:(NSNotification *)notification {
    self.hasActivePanel = NO;
}

- (void)windowDidResignKey:(NSNotification *)notification; {
    if ([[self window] isVisible]) {
        self.hasActivePanel = NO;
    }
}

- (void)windowDidResize:(NSNotification *)notification {
    NSWindow *panel = [self window];
    NSRect statusRect = [self statusRectForWindow:panel];
    NSRect panelRect = [panel frame];
    
    CGFloat statusX = roundf(NSMidX(statusRect));
    CGFloat panelX = statusX - NSMinX(panelRect);
    
    self.backgroundView.mArrowX = panelX;
}

- (void)cancelOperation:(id)sender {
    self.hasActivePanel = NO;
}

#pragma mark - Public methods

- (NSRect)statusRectForWindow:(NSWindow *)window {
    NSRect screenRect = [[[NSScreen screens] objectAtIndex:0] frame];
    NSRect statusRect = NSZeroRect;
    
    StatusItemView *statusItemView = nil;
    if ([self.delegate respondsToSelector:@selector(statusItemViewForPanelController:)])
    {
        statusItemView = [self.delegate statusItemViewForPanelController:self];
    }
    
    if (statusItemView)
    {
        statusRect = statusItemView.globalRect;
        statusRect.origin.y = NSMinY(statusRect) - NSHeight(statusRect);
    }
    else
    {
        statusRect.size = NSMakeSize(STATUS_ITEM_VIEW_WIDTH, [[NSStatusBar systemStatusBar] thickness]);
        statusRect.origin.x = roundf((NSWidth(screenRect) - NSWidth(statusRect)) / 2);
        statusRect.origin.y = NSHeight(screenRect) - NSHeight(statusRect) * 2;
    }
    return statusRect;
}

- (void)openPanel {
    NSWindow *panel = [self window];
    
    NSRect screenRect = [[[NSScreen screens] objectAtIndex:0] frame];
    NSRect statusRect = [self statusRectForWindow:panel];
    
    NSRect panelRect = [panel frame];
    panelRect.size.width = PANEL_WIDTH;
    panelRect.size.height = POPUP_HEIGHT;
    panelRect.origin.x = roundf(NSMidX(statusRect) - NSWidth(panelRect) / 2);
    panelRect.origin.y = NSMaxY(statusRect) - NSHeight(panelRect);
    
    if (NSMaxX(panelRect) > (NSMaxX(screenRect) - ARROW_HEIGHT))
        panelRect.origin.x -= NSMaxX(panelRect) - (NSMaxX(screenRect) - ARROW_HEIGHT);
    
    [NSApp activateIgnoringOtherApps:YES];
    [panel setAlphaValue:0];
    [panel setFrame:statusRect display:YES];
    [panel makeKeyAndOrderFront:nil];
    
    NSTimeInterval openDuration = OPEN_DURATION;
    
    NSEvent *currentEvent = [NSApp currentEvent];
    if ([currentEvent type] == NSLeftMouseDown)
    {
        NSUInteger clearFlags = ([currentEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask);
        BOOL shiftPressed = (clearFlags == NSShiftKeyMask);
        BOOL shiftOptionPressed = (clearFlags == (NSShiftKeyMask | NSAlternateKeyMask));
        if (shiftPressed || shiftOptionPressed)
        {
            openDuration *= 10;
            
            if (shiftOptionPressed)
                NSLog(@"Icon is at %@\n\tMenu is on screen %@\n\tWill be animated to %@",
                      NSStringFromRect(statusRect), NSStringFromRect(screenRect), NSStringFromRect(panelRect));
        }
    }
    
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:openDuration];
    [[panel animator] setFrame:panelRect display:YES];
    [[panel animator] setAlphaValue:1];
    [NSAnimationContext endGrouping];
    ClientProperties *cprop = [ClientProperties getSingleton];
    [ApiKeyLabel setStringValue:[NSString stringWithFormat:@"API key: %@", [cprop objectForKey:@"client_hash"]]];
    [self updateInfoObject:mInfo];
}

- (void)closePanel {
    [self contractPanel];
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:CLOSE_DURATION];
    [[[self window] animator] setAlphaValue:0];
    [NSAnimationContext endGrouping];
    
    dispatch_after(dispatch_walltime(NULL, NSEC_PER_SEC * CLOSE_DURATION * 2), dispatch_get_main_queue(), ^{
        
        [self.window orderOut:nil];
    });
}

- (void) expandPanel {
    if (panelIsExpanded) return;
    panelIsExpanded = YES;
    
    [self updateClientHash];

    [SQKeyText setHidden:NO];
    [InfoTextField setHidden:NO];
    [SaveKeyButton setHidden:NO];
}

- (void) contractPanel {
    if (!panelIsExpanded) return;
    panelIsExpanded = NO;

    [SQKeyText setHidden:YES];
    [InfoTextField setHidden:YES];
    [SaveKeyButton setHidden:YES];
}

- (IBAction)ManageClicked:(id)sender {
    [self closePanel];
    
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] showManageWindow];
    //AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    //[appDelegate->mPopUpWindow openPopUpWindow];
    
}

- (IBAction)ChangeCredentialsClicked:(id)sender {
    [self closePanel];

    [mCredentialWindow openCredentialWindow];
}

- (IBAction)ChangeLinkedInCredentialsClicked:(id)sender {
    [self closePanel];
    
    [mLinkedInCredentialsWindow openWindow];
}

- (IBAction)SQUserClicked:(id)sender {
    if (!panelIsExpanded) {
        [self expandPanel];
    } else {
        [self contractPanel];
    }
}

- (IBAction)SQSaveKeyClicked:(id)sender {
    // First test the new api connection.
    NSString *key = [SQKeyText stringValue];

    [SaveKeyIndicator startAnimation:self];

    // Make a request against check_hash.php. Check the response for a valid key.
    [self postUri: @"check_hash.php" withJson:[NSMutableDictionary dictionaryWithObject:key forKey:@"client_hash"] onCompleted:^(NSData *data, NSError *error) {
        
        [SaveKeyIndicator stopAnimation:self];
        
        if (error != nil) {
            // Do some nice error handling
            [self updateClientHash];
            return;
        }
        
        @try {
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];

            if ([@"success" isEqualToString:[json objectForKey:@"status"]]) {
                NSString *company_name = [[json objectForKey:@"payload"] objectForKey:@"company_name"];
                [mInfo setObject:company_name forKey:@"company_name"];

                ClientProperties *cprop = [ClientProperties getSingleton];
                [cprop setObject:key forKey:@"client_hash"];
                [cprop setObject:company_name forKey:@"company_name"];
                [cprop saveProperties];

                [self updateInfoObject:mInfo];
                [self updateClientHash];
            } else {
                [self updateClientHash];
            }
        } @catch (NSException *e) {
        
        }
    }];
}

- (IBAction)OpenMainWindowClicked:(id)sender {
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] showMainWindow];
    mMainWindowIsOpen = YES;
}

- (IBAction)SearchQuantNetLink:(id)sender {
    NSURL *url = [[NSURL alloc] initWithString:@"http://viewgentools.com/settings.php"];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (IBAction)QuitApp:(id)sender {
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] quitAppNicely];
}

- (void) updateInfoObject: (NSDictionary *) info {
    mInfo = [NSMutableDictionary dictionaryWithDictionary:info];
    
    NSLog(@"Setting info object %@", info);
    
    int status = STATUS_ON;
    
    NSString *message = @"Running";
    if ([mInfo objectForKey:@"status_message"] != nil) {
        message = [[mInfo objectForKey:@"status_message"] objectForKey:@"message"];
        status = [[[mInfo objectForKey:@"status_message"] objectForKey:@"status"] intValue];
    }
    
    // Check for username in info object.
    if ([mInfo objectForKey:@"username"] == nil) {
        [LIUserText setStringValue:[NSString stringWithFormat:@"You are not signed in to LinkedIn"]];
        [CredentialsButton setTitle:@"Sign In"];
        status = STATUS_OFF;
    } else {
        [[ClientProperties getSingleton] setObject:[mInfo objectForKey:@"username"] forKey:@"username"];
        [LIUserText setStringValue:[NSString stringWithFormat:@"You are signed in to LinkedIn as %@", [mInfo objectForKey:@"username"]]];
        [CredentialsButton setTitle:@"Change User"];
    }
    
    // Check SearchQuant account.
    if ([mInfo objectForKey:@"company_name"] == nil) {
        [SQUserText setStringValue:@"Not signed in to ViewGen"];
        status = STATUS_OFF;
    } else {
        [[ClientProperties getSingleton] setObject:[mInfo objectForKey:@"company_name"] forKey:@"company_name"];
        [SQUserText setStringValue:[NSString stringWithFormat:@"ViewGen account: %@", [mInfo objectForKey:@"company_name"]]];
    }
    
    if ([mInfo objectForKey:@"CrawledBy"] != nil) {
        [[ClientProperties getSingleton] setObject:[mInfo objectForKey:@"CrawledBy"] forKey:@"last_crawled_by"];
    }
    [[ClientProperties getSingleton] saveProperties];
    
    StatusItemView *statusItemView = nil;
    if ([self.delegate respondsToSelector:@selector(statusItemViewForPanelController:)]) {
        statusItemView = [self.delegate statusItemViewForPanelController:self];
        [statusItemView setStatus:status];
    }

    if (status == STATUS_OFF) {
        [self.LiStatusIcon setRed];
        if ([mInfo objectForKey:@"status"] == nil) {
            [StatusText setStringValue:@"Offline"];
        } else {
            [StatusText setStringValue:[mInfo objectForKey:@"status"]];
        }
    } else {
        if (status == STATUS_ON)
            [self.LiStatusIcon setGreen];
        else
            [self.LiStatusIcon setYellow];
        
        if ([self.delegate respondsToSelector:@selector(statusItemViewForPanelController:)]) {
            statusItemView = [self.delegate statusItemViewForPanelController:self];
            [statusItemView setStatus:status];
        }
        
        [StatusText setAllowsEditingTextAttributes: YES];
        [StatusText setSelectable: YES];
        [StatusText setAttributedStringValue:[PanelController hyperlinkFromString:message withURL:[NSURL URLWithString:@"http://viewgentools.com/"]]];
    }
}

- (void) updateClientHash {
    // Set client hash
    ClientProperties *cprop = [ClientProperties getSingleton];
    [SQKeyText setStringValue:[cprop objectForKey:@"client_hash"]];
    //[SQKeyText setStringValue:[NSString stringWithFormat:@"API key: %@", [cprop objectForKey:@"client_hash"]]];
}

- (void)postUri: (NSString *) uri withJson: (NSMutableDictionary *) data onCompleted:(void (^)(NSData *data, NSError *error)) handler {

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
    
    if (error) {
        handler(nil, nil);
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@", mBaseUrl, uri];
	NSURL *urlObject = [NSURL URLWithString:url];
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:urlObject];
    
    [req setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
	[req setHTTPMethod:@"POST"];
	[req setHTTPBody:jsonData];
    [req setTimeoutInterval:10];
    
    [NSURLConnection sendAsynchronousRequest:req
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *resData, NSError *error) {
 
        handler(resData, error);
    }];
}

+(id)hyperlinkFromString:(NSString*)inString withURL:(NSURL*)aURL
{
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString: inString];
    NSRange range = NSMakeRange(0, [attrString length]);
    
    [attrString beginEditing];
    [attrString addAttribute:NSLinkAttributeName value:[aURL absoluteString] range:range];
    
    // make the text appear in blue
    //[attrString addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:range];
    
    // next make the text appear with an underline
    //[attrString addAttribute:
    // NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSSingleUnderlineStyle] range:range];
    
    [attrString endEditing];
    
    return attrString;
}

- (void)openLinkedInCredentialsWindow {
    [mLinkedInCredentialsWindow openWindow];
}


@end
