//
//  ClientProperties.h
//  WebProxy
//
//  Created by macbook on 2014-05-08.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientProperties : NSObject {
    
    NSMutableDictionary *mDict;
    NSString *mPath;
}

+ (ClientProperties *) getSingleton;

- (NSString *) objectForKey: (NSString *) key;
- (void) setObject:(NSString *)obj forKey:(NSString *) key;
- (void) saveProperties;

@end
