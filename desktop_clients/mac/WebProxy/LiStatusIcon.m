//
//  LiStatusIcon.m
//  SearchQuant
//
//  Created by macbook on 2014-05-18.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "LiStatusIcon.h"

@implementation LiStatusIcon

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        mYellow = [NSImage imageNamed:@"icon_yellow.png"];
        mRed = [NSImage imageNamed:@"icon_red.png"];
        mGreen = [NSImage imageNamed:@"icon_green.png"];
        CGRect imageFrame = NSMakeRect(0, 0, self.frame.size.width, self.frame.size.height);
        mImageView = [[NSImageView alloc] initWithFrame:imageFrame];
        [mImageView setImage:mGreen];
        [self addSubview:mImageView];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

-(void) setYellow {[mImageView setImage:mYellow];}
-(void) setRed {[mImageView setImage:mRed];}
-(void) setGreen {[mImageView setImage:mGreen];}

@end
