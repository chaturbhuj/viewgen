//
//  LinkedInCredentials.h
//  SearchQuant
//
//  Created by Josef Cullhed on 2016-02-03.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface LinkedInCredentials : NSWindowController {

}

- (IBAction)SaveCredentialsClicked:(id)sender;
@property (weak) IBOutlet NSTextField *mUsername;
@property (weak) IBOutlet NSTextField *mPassword;

- (void) openWindow;

@end
