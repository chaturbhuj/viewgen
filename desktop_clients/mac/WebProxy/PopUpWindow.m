//
//  PopUpWindow.m
//  SearchQuant
//
//  Created by macbook on 2014-06-11.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "PopUpWindow.h"
#import "AppDelegate.h"

@implementation PopUpWindow
@synthesize mWebUIWindow;
@synthesize mImage;

- (id) init {
    self = [super initWithWindowNibName:@"PopUpWindow"];
    
    if (self) {
        [self loadWindow];
    }
    
    NSString *url = [@"http://viewgentools.com/client_ui/ui.php?os=mac&client_hash=" stringByAppendingString:[[ClientProperties getSingleton] objectForKey:@"client_hash"]];
    
    NSString *crawledBy = [(AppDelegate *)[[NSApplication sharedApplication] delegate] getCrawledBy];
    if (crawledBy != nil) {
        url = [[url stringByAppendingString:@"&linked_in_id="] stringByAppendingString:[crawledBy stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    } else {
        crawledBy = [[ClientProperties getSingleton] objectForKey:@"last_crawled_by"];
        if (crawledBy != nil) {
            url = [[url stringByAppendingString:@"&linked_in_id="] stringByAppendingString:[crawledBy stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        }
    }
    url = [url stringByAppendingString:@"&rnd="];
    url = [url stringByAppendingString:[NSString stringWithFormat:@"%i", rand()]];
    NSMutableURLRequest *urlRequest2 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [[mWebUIWindow mainFrame] loadRequest:urlRequest2];
    [[mWebUIWindow mainFrame] reload];

    return self;
}

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void) setStatus: (int) status {
}


- (IBAction)CloseButtonClicked:(id)sender {
    NSLog(@"Close clicked");
    NSLog(@"%@", [self window]);
    [[self window] close];
}

- (void) openPopUpWindow {
    [[NSApplication sharedApplication] activateIgnoringOtherApps : YES];
    [[self window] setFrame:[[NSScreen mainScreen] visibleFrame] display:YES];
    [[self window] makeKeyWindow];
    [[self window] setLevel:NSScreenSaverWindowLevel];
    [[self window] orderFront:self];
    [[self window] setOpaque:NO];
    [[self window] setLevel:NSNormalWindowLevel];
    [[self window] makeKeyWindow];
}

@end
