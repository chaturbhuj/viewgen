//
//  PopUpWindow.h
//  SearchQuant
//
//  Created by macbook on 2014-06-11.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <WebKit/WebKit.h>
#import <Cocoa/Cocoa.h>
#import "ClientProperties.h"
#import "StatusItemView.h"


@interface PopUpWindow : NSWindowController <NSWindowDelegate> {

    __weak WebView *mWebUIWindow;
}

- (void) setStatus: (int) status;
@property (weak) IBOutlet NSImageView *mImage;
- (IBAction)CloseButtonClicked:(id)sender;
- (void) openPopUpWindow;

@property (weak) IBOutlet WebView *mWebUIWindow;
@end
