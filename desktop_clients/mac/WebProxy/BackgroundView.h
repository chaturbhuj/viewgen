
#define ARROW_WIDTH 12
#define ARROW_HEIGHT 8
#define STATUS_ITEM_VIEW_WIDTH 24.0

@interface BackgroundView : NSView {
    
    NSInteger mArrowX;
    NSImage *mImage;

}

@property (nonatomic, unsafe_unretained) NSInteger mArrowX;

@end
