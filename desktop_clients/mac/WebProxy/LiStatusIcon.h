//
//  LiStatusIcon.h
//  SearchQuant
//
//  Created by macbook on 2014-05-18.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LiStatusIcon : NSView {
    NSImage *mYellow;
    NSImage *mRed;
    NSImage *mGreen;
    
    NSImageView *mImageView;
}

-(void) setYellow;
-(void) setRed;
-(void) setGreen;

@end
