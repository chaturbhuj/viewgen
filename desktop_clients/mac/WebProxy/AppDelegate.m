//
//  AppDelegate.m
//  Test
//
//  Created by macbook on 2014-04-27.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//
/*
 Creating package:
 http://vincent.bernat.im/en/blog/2013-autoconf-osx-packaging.html#creating-a-package
 http://stackoverflow.com/questions/14211312/how-to-use-pkgbuild-to-create-a-kext-installer-from-within-xcode
 http://stackoverflow.com/questions/11487596/making-os-x-installer-packages-like-a-pro-xcode4-developer-id-mountain-lion-re
 
 Menus:
 https://github.com/shpakovski/Popup/tree/master/Popup
 */

#import "AppDelegate.h"

// The application terminates after this number of seconds.
#define TERMINATION_INTERVAL 3600*3

@implementation AppDelegate

void *kContextActivePanel = &kContextActivePanel;

@synthesize window = mWindow;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    // Make sure the
    
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    //if ([arguments count] )
    BOOL is_daemon = NO;
    BOOL is_debug = NO;
    for (NSString *arg in arguments) {
        if ([arg isEqualToString:@"--daemon"]) is_daemon = YES;
        if ([arg isEqualToString:@"-NSDocumentRevisionsDebugMode"]) is_debug = YES;
    }
    
    if (!is_daemon) {
        // Mark for pop up
        [[ClientProperties getSingleton] setObject:@"yes" forKey:@"showPopUp"];
        [[ClientProperties getSingleton] saveProperties];
    }
    
#ifndef DEBUG
    
        NSString *file = [@"~/Library/LaunchAgents/ViewGen.plist" stringByExpandingTildeInPath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:file]) {
            
            // Check if program is loaded into launchctl.
            
            NSTask *task1 = [[NSTask alloc] init];
            [task1 setLaunchPath:@"/bin/launchctl"];
            
            NSArray *arg1 = [NSArray arrayWithObjects: @"list", @"ViewGen", nil];
            [task1 setArguments: arg1];
            
            [task1 launch];
            [task1 waitUntilExit];
            int status = [task1 terminationStatus];
            
            NSLog(@"STATUS: %d", status);
            
            if (status != 0) {
                NSTask *task2;
                task2 = [[NSTask alloc] init];
                [task2 setLaunchPath: @"/bin/launchctl"];
    
                NSArray *arguments = [NSArray arrayWithObjects: @"load", file, nil];
                [task2 setArguments: arguments];
                [task2 waitUntilExit];
                [task2 launch];
                [[NSApplication sharedApplication] terminate:nil];
            }
        }
#endif
    
    // Make the window invisible
    
    [mWindow setDelegate:self];

    [mWindow setOpaque:YES];
    [mWindow setHasShadow:NO];
    [mWindow setBackgroundColor:[NSColor colorWithSRGBRed:0 green:0 blue:0 alpha:0]];
    [mWindow setStyleMask:NSBorderlessWindowMask];
    [mWindow setBackingType:NSBackingStoreBuffered];
    [mWindow setFrame:CGRectMake(0, 0, 1280, 800) display:NO];
    [mWindow orderOut:self];

    mWebView = [[ProxyWebView alloc] init];
    [mWebView setDelegate:self];
    NSLog(@"%@", [[ClientProperties getSingleton] objectForKey:@"username"]);

    [mWindow setContentView:mWebView];
    
    // Schedule the first probe.
    [mWebView scheduleProbe:1];
    
    mMenuController = [[MenuController alloc] init];
    mPanelController = [[PanelController alloc] initWithDelegate:self];
    [mPanelController addObserver:self forKeyPath:@"hasActivePanel" options:0 context:kContextActivePanel];
    
    [mPanelController updateInfoObject:[[NSDictionary alloc] initWithObjectsAndKeys:
        [[ClientProperties getSingleton] objectForKey:@"username"], @"username",
        [[ClientProperties getSingleton] objectForKey:@"company_name"], @"company_name", nil]];

    mTerminationTimer = [NSTimer scheduledTimerWithTimeInterval:TERMINATION_INTERVAL target:self selector:@selector(executeTerminationTimer:) userInfo:nil repeats:NO];
    
    NSString *linkedInUsername = [[ClientProperties getSingleton] objectForKey:@"LinkedInUsername"];
    if (linkedInUsername == nil || [linkedInUsername isEqualToString:@""]) {
        [mPanelController openLinkedInCredentialsWindow];
    } else {
        NSString *showPopUp = [[ClientProperties getSingleton] objectForKey:@"showPopUp"];
        if (is_debug || (is_daemon && ([showPopUp isEqualToString:@"yes"] || showPopUp == nil))) {
            [[ClientProperties getSingleton] setObject:@"no" forKey:@"showPopUp"];
            [[ClientProperties getSingleton] saveProperties];
            [self showManageWindow];
        }
    }
}

- (void) setInfoObject: (NSDictionary *) info {
    [mPanelController updateInfoObject:info];
}

- (void)dealloc {
    [mPanelController removeObserver:self forKeyPath:@"hasActivePanel"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (context == kContextActivePanel) {
        mMenuController.hasActiveIcon = mPanelController.hasActivePanel;
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Explicitly remove the icon from the menu bar
    mMenuController = nil;
    return NSTerminateNow;
}

#pragma mark - Actions

- (IBAction)togglePanel:(id)sender {
    mMenuController.hasActiveIcon = !mMenuController.hasActiveIcon;
    mPanelController.hasActivePanel = mMenuController.hasActiveIcon;
}

- (StatusItemView *)statusItemViewForPanelController:(PanelController *)controller {
    return mMenuController.mStatusItemView;
}

- (void) executeTerminationTimer: (NSTimer *) timer {
    [[NSApplication sharedApplication] terminate:nil];
}

- (void) hideMainWindow {
    [mWindow setOpaque:YES];
    [mWindow setHasShadow:NO];
    [mWindow setBackgroundColor:[NSColor colorWithSRGBRed:0 green:0 blue:0 alpha:0]];
    [mWindow setStyleMask:NSBorderlessWindowMask];
    [mWindow setBackingType:NSBackingStoreBuffered];
    [mWindow setFrame:CGRectMake(0, -8000, 1280, 800) display:NO];
    [mWindow orderOut:self];
}

- (void) showMainWindow {
    [mWindow setOpaque:NO];
    [mWindow setHasShadow:YES];
    [mWindow setBackgroundColor:[NSColor colorWithSRGBRed:255 green:255 blue:255 alpha:255]];
    [mWindow setStyleMask:NSMiniaturizableWindowMask | NSTitledWindowMask | NSResizableWindowMask | NSClosableWindowMask];
    [mWindow setBackingType:NSBackingStoreBuffered];
    [mWindow setFrame:CGRectMake(([[NSScreen mainScreen] frame].size.width-1280.0)/2.0, 50.0, 1280.0, [[NSScreen mainScreen] frame].size.height-200) display:YES];
    [mWindow orderBack:self];
    
    [mWindow makeKeyWindow];
    [mWindow setLevel:NSScreenSaverWindowLevel];
    [mWindow orderFront:self];
    [mWindow setOpaque:NO];
    [mWindow setLevel:NSNormalWindowLevel];
}

- (void) showManageWindow {
    mPopUpWindow = [[PopUpWindow alloc] init];
    NSWindow *wnd = [mPopUpWindow window];
    [wnd makeKeyWindow];
    [wnd setLevel:NSPopUpMenuWindowLevel];
    [wnd orderFront:self];
    [wnd setOpaque:NO];
}

- (void) reschedule {
    // Make main web view load request directly.
    [mWebView reschedule];
}

- (BOOL)windowShouldClose:(id) sender {
    //[self hideMainWindow];
    [self hideMainWindow];
    return NO;
}

- (void) quitAppNicely {
    
    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/launchctl"];
    
    NSArray *arguments;
    arguments = [NSArray arrayWithObjects: @"unload", [NSHomeDirectory() stringByAppendingPathComponent:@"Library/LaunchAgents/ViewGen.plist"], nil];
    [task setArguments: arguments];
    
    [task launch];
    
    [[NSApplication sharedApplication] terminate:nil];
}

- (void) setStatus: (int) status {
    if (mPopUpWindow) {
        [mPopUpWindow setStatus: status];
    }
}

- (NSString *) getCrawledBy {
    return [mWebView getCrawledBy];
}

@end
