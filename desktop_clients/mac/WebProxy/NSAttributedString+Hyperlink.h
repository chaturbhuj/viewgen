//
//  NSAttributedString+Hyperlink.h
//  SearchQuant
//
//  Created by macbook on 2014-05-20.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString(Hyperlink)

+(id)hyperlinkFromString:(NSString*)inString withURL:(NSURL*)aURL;

@end
