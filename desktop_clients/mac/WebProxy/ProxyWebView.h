//
//  ProxyWebView.h
//  Test
//
//  Created by macbook on 2014-04-27.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "ClientProperties.h"
#import "StatusBar.h"

#define P_ERROR_TIMEOUT 30
#define P_WEB_TIMEOUT 120
#define P_LOGIN_TIMEOUT 1800
#define P_PROBE_TIMEOUT 10
#define P_REQUEST_TIMEOUT 300

@protocol ProxyWebViewDelegate <NSObject>

@required
    - (void) setInfoObject: (NSDictionary *) info;

@end

@interface ProxyWebView : WebView {
    
    // Delegate
    id <ProxyWebViewDelegate> mDelegate;
    
    // The url of our api.
    NSString *mBaseUrl;
    
    // The current version, should be filled with some string identifying when we released the software. "Mac 2015-10-21"
    NSString *mVersion;
    
    // Company name
    NSString *mCompanyName;
    
    // The current user hash, this is used to identify the user during api requests.
    NSString *mClientHash;
    
    // The current url id.
    NSString *mURLId;
    
    // The current time between requests in seconds.
    int mDelta;

    // The JSON object passed to save.
    NSMutableDictionary *mPayload;
    
    // Timer used for sheduling probes.
    NSTimer *mProbeTimer;
    NSTimer *mScrollTimer;
    
    // Timer used for timing out web browser.
    NSTimer *mWebTimer;
    
    NSTimer *mRequestTimer;
    
    StatusBar *mStatusBar;
}

// Delegate for WebView.
- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame*)frame;

// Handle a loaded html string.
- (void)handleData: (NSString *) html;

// Handle api connection.
- (void)postUri: (NSString *) uri withJson: (NSMutableDictionary *) data onCompleted:(void (^)(NSDictionary *json)) handler;

// Process error message.
- (void)handleError: (NSError *) error;

// Starts probing the api for urls.
- (void)startProbe;

// Tell the browser to navigate to the given URL.
- (void)goToURL: (NSString *) url;

// Schedule a new probe after the provided number of seconds.
- (void)scheduleProbe: (int) sec;

- (void)reschedule;

// Execute start probe timer
- (void)executeTimer:(NSTimer *) timer;

// Execute scroll timer
- (void)executeScrollTimer:(NSTimer *) timer;

// Execute cancel web request timer
- (void)cancelWebRequest:(NSTimer *) timer;

// Set delegate for callbacks.
- (void) setDelegate: (id <ProxyWebViewDelegate>) delegate;

// Checks if the page loaded is a page that indicates that the user is logged out of linkedin.
- (bool) checkForLogout: (NSString *) html;

// Initiates the login process for the user.
- (void) initiateLogin;

// Check if the login form is present and handle it.
- (bool) handleLoginForm: (NSString *) html;

- (NSString *) getCrawledBy;

@end
