//
//  ClientProperties.m
//  WebProxy
//
//  Created by macbook on 2014-05-08.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "ClientProperties.h"

@implementation ClientProperties

ClientProperties *singleton = nil;

+ (ClientProperties *) getSingleton {
    if(singleton == nil){
        singleton = [[self alloc] init];
    }
    
    return singleton;
}

- (ClientProperties *) init {
    
    self = [super init];
    if (self) {
        mPath = [@"~/.sqclient" stringByExpandingTildeInPath];
        mDict = [[NSMutableDictionary alloc] initWithDictionary:[[NSDictionary alloc] initWithContentsOfFile:mPath]];
        
        if ([mDict objectForKey:@"client_hash"] == nil) {
            NSString *bundlePath = [[NSBundle mainBundle]bundlePath];
            NSString *path = [bundlePath stringByAppendingPathComponent:@"Contents/Resources/Client.plist"];
            NSLog(@"PATH:::%@", path);
            NSDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[NSDictionary alloc] initWithContentsOfFile:path]];
            NSLog(@"DICT:::%@", dict);
            [mDict setObject:[dict objectForKey:@"client_hash"] forKey:@"client_hash"];
            [mDict setObject:[dict objectForKey:@"company_name"] forKey:@"company_name"];
            for (NSString *key in dict) {
                [mDict setObject:[dict objectForKey:key] forKey:key];
            }
            [self saveProperties];
        }
    }

    return self;
}

- (NSString *) objectForKey: (NSString *) key {
    NSString *obj = [mDict objectForKey:key];
    if (obj == nil) return nil;
    return obj;
}

- (void) setObject:(NSString *)obj forKey:(NSString *) key {
    [mDict setObject:obj forKey:key];
}

- (void) saveProperties {
    [mDict writeToFile:mPath atomically:YES];
}

@end
