//
//  AppDelegate.h
//  Test
//
//  Created by macbook on 2014-04-27.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebView.h>
#import "ProxyWebView.h"
#import "MenuController.h"
#import "PanelController.h"
#import "StatusItemView.h"
#import "PopUpWindow.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate, ProxyWebViewDelegate, PanelControllerDelegate> {

    ProxyWebView *mWebView;
    
    MenuController *mMenuController;
    @public PanelController *mPanelController;
    NSStatusItem *mStatusItem;
    NSMenu *mStatusMenu;
    
    PopUpWindow *mPopUpWindow;
    
    NSTimer *mTerminationTimer;

}

@property (assign) IBOutlet NSWindow *window;

- (void) setInfoObject: (NSDictionary *) info;
- (void) executeTerminationTimer: (NSTimer *) timer;
- (void) hideMainWindow;
- (void) showMainWindow;
- (void) showManageWindow;
- (void) reschedule;
- (void) quitAppNicely;
- (void) setStatus: (int) status;
- (NSString *) getCrawledBy;

@end
