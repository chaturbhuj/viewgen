//
//  CredentialWindow.m
//  WebProxy
//
//  Created by macbook on 2014-05-07.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "CredentialWindow.h"
#import "AppDelegate.h"

@implementation CredentialWindow
@synthesize mWebView;

- (CredentialWindow *) init {
     self = [super initWithWindowNibName:@"Credentials"];
    
    if (self) {
        
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Make a fully skinned panel
    NSWindow *wnd = (id)[self window];
    [wnd makeKeyWindow];
    [wnd setLevel:NSPopUpMenuWindowLevel];
    [wnd orderFront:self];
    [wnd setOpaque:NO];
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame*)frame {
    if (frame == [sender mainFrame]) {
        // Ping MainWebView
        [(AppDelegate *)[[NSApplication sharedApplication] delegate] reschedule];
    }
}

- (void) openCredentialWindow {
    [[NSApplication sharedApplication] activateIgnoringOtherApps : YES];
    [[self window] setFrame:[[NSScreen mainScreen] visibleFrame] display:YES];
    [[self window] makeKeyWindow];
    [[self window] setLevel:NSScreenSaverWindowLevel];
    [[self window] orderFront:self];
    [[self window] setOpaque:NO];
    [[self window] setLevel:NSNormalWindowLevel];
    [[self window] makeKeyWindow];
    [mWebView setMainFrameURL:@"https://www.linkedin.com/"];
}

@end
