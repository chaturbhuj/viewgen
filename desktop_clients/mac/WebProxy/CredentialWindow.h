//
//  CredentialWindow.h
//  WebProxy
//
//  Created by macbook on 2014-05-07.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <WebKit/WebKit.h>

@interface CredentialWindow : NSWindowController {
    
}

@property (weak) IBOutlet WebView *mWebView;

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame*)frame;
- (void) openCredentialWindow;

@end
