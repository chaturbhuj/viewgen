<?php

chdir(dirname(__FILE__) . '/../public_html');

include('admin/prosper.php');

$list_id = 'a18de8efa0';
$api_key = '09d2a9949980387d779bee312baf9c1d-us15';

include('../public_html/db.php');
include('../public_html/classes/mailchimp/Mailchimp.php');
$mailChimp = new MailChimp($api_key);

$query = DB::prep("SELECT * FROM company WHERE email != '' and email LIKE '%@%' AND company_name NOT LIKE '%NotInUse%' AND id >= 291 AND account_type = 'paid' ORDER BY id DESC");
$query->execute();

require_once ('../public_html/lib/recurly.php');

$unique = [];

$numUpdated = 0;
while ($company = $query->fetch()) {

	if (isset($unique[$company['email']])) {
		continue;
	}

	if (str_replace(".", "", $company['email']) == "josefcullhed@gmailcom" || str_replace(".", "", $company['email']) == "ivanhallbergryman@gmailcom") {
		continue;
	}

	$account = null;
	try {
		$account = Recurly_Account::get($company['email']);
	} catch (Exception $error) {

		if ($company['recurlyAccountCode'] != '') {
			try {
				$account = Recurly_Account::get($company['recurlyAccountCode']);
			} catch (Exception $error) {
			}
		}
	}

	$list = "None";
	$endDate = date('m/d/Y');
	$lastTransactionDate = "";

	if ($company['state'] == 'draft') {
		$list = 'Users who started signing up but never completed';
	}

	$hasRefund = "no";
	$startDate = $company['company_created'];

	if ($account !== null) {
		// This company has a recurly account. Look at the subscriptions.
		$subscriptions = Recurly_SubscriptionList::getForAccount($account->account_code);
		$hasActiveSubscription = false;
		foreach ($subscriptions as $subscription) {
			if ($subscription->state == "active") {
				$hasActiveSubscription = true;
				$list = 'Current customers';
				break;
			}
			if ($subscription->state == "canceled") {
				if (isset($subscription->trial_started_at) && isset($subscription->trial_ends_at)) {

					if ($subscription->trial_started_at->format('Y-m-d H:i:s') != $subscription->trial_ends_at->format('Y-m-d H:i:s') and
						(strtotime($subscription->trial_ends_at->format('Y-m-d H:i:s')) >
						strtotime($subscription->canceled_at->format('Y-m-d H:i:s')))) {

						$endDate = $subscription->canceled_at->format('m/d/Y');
						$list = "Users who signed up and ran the 7 day trial and canceled before their first bill";
					}
				}
			}
			if ($subscription->state == "expired") {
				if (isset($subscription->trial_started_at) && isset($subscription->trial_ends_at)) {

					if ($subscription->trial_started_at->format('Y-m-d H:i:s') != $subscription->trial_ends_at->format('Y-m-d H:i:s') and
						(strtotime($subscription->trial_ends_at->format('Y-m-d H:i:s')) >=
						strtotime($subscription->expires_at->format('Y-m-d H:i:s')))) {

						$endDate = $subscription->expires_at->format('m/d/Y');
						$list = "Users who signed up and ran the 7 day trial and canceled before their first bill";
					}
				}
			}
		}

		if (!$hasActiveSubscription && $list == "None") {
			$list = "Previous customers who canceled";
		}

		$transactions = Recurly_TransactionList::getForAccount($account->account_code);
		$latestTransactionCreated = 0;
		foreach ($transactions as $transaction) {
			if ($transaction->action == "refund") {
				$hasRefund = "yes";
			} elseif ($transaction->action == "purchase") {
				$transactionCreated = strtotime($transaction->created_at->format('Y-m-d H:i:s'));
				if ($transactionCreated > $latestTransactionCreated) {
					$latestTransactionCreated = $transactionCreated;
				}
			} elseif ($transaction->action == "verify") {
				$startDate = $transaction->created_at->format('Y-m-d H:i:s');
			}
		}
		if ($latestTransactionCreated > 0) {
			$lastTransactionDate = date('m/d/Y', $latestTransactionCreated);
		}
	}

	$unique[$company['email']] = 1;

	if ($list != "None") {

		// Check number of actual days viewing.
		$query2 = DB::prep("SELECT COUNT(*) numberOfDays, MAX(cache_date) lastDayOfViewing, MIN(cache_date) firstDayOfViewing
			FROM (SELECT * FROM reports_cache WHERE company_id = :company_id AND number_of_visits > 50 GROUP BY cache_date) TMP;");
		$query2->execute([
			'company_id' => $company['id']
		]);
		$row = $query2->fetch();
		$actualDaysViewing = $row['numberOfDays'];
		$lastDayOfViewing = $row['lastDayOfViewing'];
		$firstDayOfViewing = $row['firstDayOfViewing'];

		$query2 = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0");
		$query2->execute([
			'company_id' => $company['id']
		]);

		$allUsers = $query2->fetchAll();
		$first_user = null;
		if (count($allUsers) > 0) {
			$first_user = $allUsers[0];
		}

		$totalDaysInService = "";
		if ($endDate != "") {
			$totalDaysInService = ceil((strtotime($endDate) - strtotime($startDate))/(3600*24));
		}

		$numberOfCampaigns = $first_user ? ($first_user['paused_campaign_count'] +
			$first_user['finished_campaign_count'] + 
			$first_user['active_campaign_count']) : 0;

		if ($list == "Users who started signing up but never completed") $statusId = 141349;
		if ($list == "Current customers") $statusId = 141346;
		if ($list == "Users who signed up and ran the 7 day trial and canceled before their first bill") $statusId = 141348;
		if ($list == "Previous customers who canceled") $statusId = 141347;

		echo "RUNNING: " . $company['email'] ."\n";
		echo "Updating the company in: PROSPERWORKS\n";
		while (true) {
			$didInsert = updateCompanyInProsper($company['id'], [
				109649 => $totalDaysInService, // Nr of days active,
				109650 => $totalDaysInService * 800, // Target nr of visits,
				109651 => $totalDaysInService == 0 ? 0 : round(100 * $company['people_count'] / ($totalDaysInService * 800), 2), // % Actual vs. Target # of Visits
				109648 => $numberOfCampaigns, // # of Campaigns
				109647 => $statusId, // Status
				109640 => $company['recurlyCoupon'], // Coupon code
				109637 => $first_user ? ($first_user['anonymous'] == 'Private mode' ? 1 : 0) : 0, // Private
				109635 => $first_user ? $first_user['vbr_during_visiting'] : 0, // VBR %
				109634 => 0, // # of View Backs
				93054 => $company['people_count'], // Total Visits
				109631 => $endDate, // End Date
				109630 => $startDate ? date('m/d/Y', strtotime($startDate)) : '', // Start Date
				93011 => $company['id'], // Account ID
				91860 => $company['subscription_plan_code'], // Plan
				92602 => count($allUsers) // # of Users
			]);
			echo $company['company_name'] . "\n";
			$numUpdated++;
			sleep(1);
			if ($didInsert) break;
		}
		echo "Updating the company in: MAILCHIMP\n";
		try {
			if ($company['company_do_not_email'] || $company['is_test']) {
				echo "DELETING FROM MAILCHIMP: " . $company['email'] . "\n";
				$mailChimp->lists->unsubscribe($list_id, array('email' => $company['email']), true, false, false);
			} else {
				$mailChimp->lists->subscribe($list_id, array('email' => $company['email']), [
						'CATEGORY' => $list,
						'C_ID' => $company['id'],
						'C_PHONE' => $company['phone'],
						'URL' => $first_user ? sprintf("http://viewgentools.com/new-ui/campaign_builder.php?company_id=%d", $company['id']) : '',
						'FNAME' => $company["first_name"],
						'LNAME' => $company["last_name"],
						'C_NAME' => $company['company_name'],
						'R_COUPON' => $company['recurlyCoupon'],
						'R_PLAN' => $company['subscription_plan_code'],
						'C_START_D' => date('m/d/Y', strtotime($startDate)),
						'C_END_D' => $endDate,
						'DAYS_TOTAL' => $totalDaysInService,
						'VISITS_IDE' => $totalDaysInService * 800,
						'VISITS_ACT' => $company['people_count'],
						'VBR' => $first_user['vbr_during_visiting'],
						'N_CAMPAIGNS' => $numberOfCampaigns,
						'N_USERS' => count($allUsers),
						'NO_MAIL' => $company['company_do_not_email'],
						"optin_time" => date("Y-m-d H:i:s", time()),
						"MMERGE3" => "Customer",
						"HAS_REFUND" => $hasRefund,
						"LAST_TRANS" => $lastTransactionDate,
						"ACT_VIEW" => $actualDaysViewing,
						"LAST_VIEW" => $actualDaysViewing == 0 ? '' : date('m/d/Y', strtotime($lastDayOfViewing)),
						"FIRST_VIEW" => $actualDaysViewing == 0 ? '' : date('m/d/Y', strtotime($firstDayOfViewing))
					], 'html', false, true);
			}
		} catch (Exception $error) {
			echo "Warning. Could not import email. Continuing...\n";
		}
	}

}

?>
