<?php

//$_SERVER['HTTP_HOST'] = 'jozo.dev.viewgentools.com';

chdir(dirname(__FILE__) . '/../public_html');

include_once('db.php');

$data = file_get_contents("../data/dnc_data.txt");
$lines = explode("\n", $data);

foreach ($lines as $line) {
	$cols = explode("\t", $line);
	if (count($cols) != 2) continue;

	if ($cols[1] != "") {
		$query = DB::prep("UPDATE company SET company_do_not_email = 1 WHERE email = :email");
		$query->execute([
			'email' => $cols[0]
		]);
	}
}

?>
