<?php
ini_set("max_execution_time", 0);
require_once(__DIR__.'/../public_html/api/models/InboundSalesParser.php');
try {
	require_once(__DIR__.'/../public_html/db.php');
} catch (Exception $e) {
	throw new Exception('Database connection error.');
}
$options = getopt("su:");
if(!isset($options['s'])){
    $processes = [];

    $query = DB::prep("
        SELECT
            distinct linked_in_id
        FROM
            li_users
        WHERE
            last_connection >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 3 month)
            AND hidden = 0
            AND have_sales_navigator = 1
            AND last_inbound_check >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 3 month)
        ;
    ");
    $query->execute();
    while ($data = $query->fetch()) {
        $processes[exec("nohup php ".__DIR__."/recalcReturnVisits.php -s -u ".$data['linked_in_id']." > /dev/null 2>&1 & echo $!")] = true;
        $processes = check_wait($processes);
    }

} else {
    include(__DIR__.'/../public_html/api/htmlStorage.php');
    $startDate = "2017-05-01 00:00:00";
    $endDate = "2017-08-01 23:59:59";
    $crawledById = $options['u'];


    $query = DB::prep("
            SELECT
                *
            FROM
                tmp_recalc_inbound_state
            WHERE
                linked_in_id = ?
            ;            
        ");
    $query->execute(array($crawledById));
    if(!$query->fetchColumn()){
        $query = DB::prep("
            INSERT INTO
                tmp_recalc_inbound_state (linked_in_id, started_at)
            VALUES
                (?, NOW())
            ;            
        ");
        $query->execute(array($crawledById));

        recalcInboundVisits($crawledById, $startDate, $endDate);

        $query = DB::prep("
            UPDATE
                tmp_recalc_inbound_state
            SET 
                delete_started_at = NOW()
            WHERE
                linked_in_id = ?
            ;            
        ");
        $query->execute(array($crawledById));
        deleteOldData($crawledById, $startDate, $endDate);

        $query = DB::prep("
            UPDATE
                tmp_recalc_inbound_state
            SET 
                copy_started_at = NOW()
            WHERE
                linked_in_id = ?
            ;            
        ");
        $query->execute(array($crawledById));
        copyNewData($crawledById, $startDate, $endDate);

        $query = DB::prep("
            UPDATE
                tmp_recalc_inbound_state
            SET 
                connection_started_at = NOW()
            WHERE
                linked_in_id = ?
            ;            
        ");
        $query->execute(array($crawledById));
        recalcOutboundInboundConnection($crawledById, $startDate, $endDate);

        $query = DB::prep("
            UPDATE
                tmp_recalc_inbound_state
            SET 
                ended_at = NOW()
            WHERE
                linked_in_id = ?
            ;            
        ");
        $query->execute(array($crawledById));
    }

}

//----------------------------------------------------------------------------------------------------------------------
function check_wait($processes){
    while (count($processes) >= 50){
        sleep(30);
        echo ".";
        $processes = array_filter($processes, "process_status",ARRAY_FILTER_USE_KEY);
    }
    return $processes;
}

function process_status($pid){
    $command = 'ps -p '.$pid;
    exec($command,$op);
    if (!isset($op[1]))return false;
    else return true;
}




/**
 * @param $name
 * @param $guessedVisitDate
 * @param $crawledById
 * @return bool
 */
function check4Duplicate($linkedInId, $intervalCount, $intervalName, $parse_visit_date, $crawledById, $visits)
{
    if(!isset($visits[$linkedInId])) return FALSE;
    switch ($intervalName){
        case "DAY":
            $interval_start = ($intervalCount + 1)."D";
            $interval_end = $intervalCount."D";
            break;
        case "WEEK":
            $interval_start = (($intervalCount + 1)*7)."D";
            $interval_end = ($intervalCount*7)."D";
            break;
        case "MONTH":
            $interval_start = ($intervalCount + 1)."M";
            $interval_end = $intervalCount."M";
            break;
        default:
            $interval_start = ($intervalCount + 1)."D";
            $interval_end = $intervalCount."D";
    }
    $date_from = (new DateTime($parse_visit_date))->sub(new DateInterval("P".$interval_start))->format("Y-m-d");
    $date_to = (new DateTime($parse_visit_date))->sub(new DateInterval("P".$interval_end))->format("Y-m-d");

    foreach($visits[$linkedInId] as $date){
        if($date >= $date_from && $date <= $date_to) return TRUE;
    }
    return FALSE;
}

function getGuessedVisitDate($htmlDumpDate, $intervalCount, $intervalName, $randCorrection){
    $htmlDumpDate = new DateTime($htmlDumpDate);

    switch ($intervalName){
        case "DAY":
            $interval = $intervalCount."D";
            break;
        case "WEEK":
            $interval = ($intervalCount*7 + $randCorrection)."D";
            break;
        case "MONTH":
            $interval = $intervalCount."M".$randCorrection."D";
            break;
        default:
            $interval = $intervalCount."D";
    }
    return $htmlDumpDate->sub(new DateInterval("P".$interval))->format("Y-m-d");
}

function deleteOldData($crawledById, $startDate, $endDate){
    $query = DB::prep("
            DELETE
                v, vc
            FROM
                    inbound_visit v
                LEFT JOIN
                    visit_connection vc
                ON
                    v.inbound_visit_id = vc.inbound_visit_id
            WHERE
                v.crawled_by_id = ?
                AND v.guessed_visit_date BETWEEN ? AND ?
            ;            
        ");
    $query->execute(array($crawledById, $startDate, $endDate));
}

function copyNewData($crawledById, $startDate, $endDate){
    $query = DB::prep("
            INSERT INTO
                inbound_visit (crawled_by_id, linked_in_id, linked_in_hashed_id, name, industry_id, industry_name, headline, guessed_visit_date) 
            SELECT 
                crawled_by_id, linked_in_id, linked_in_hashed_id, name, industry_id, industry_name, headline, guessed_visit_date 
            FROM
                tmp_inbound_visit 
            WHERE
                crawled_by_id = ?
                AND guessed_visit_date BETWEEN ? AND ?
            ;
        ");
    $query->execute(array($crawledById, $startDate, $endDate));
}

function recalcInboundVisits($crawledById, $startDate, $endDate){
    $visits = [];

    $query = DB::prep("
      SELECT
        linked_in_id,
        guessed_visit_date 
      FROM 
        tmp_inbound_visit 
      WHERE 
        crawled_by_id = ? 
    ");
    $query->execute(array($crawledById));
    while ($data = $query->fetch()) {
        $visits[$data['linked_in_id']][] = $data['guessed_visit_date'];
    }

    $query = DB::prep("
  SELECT 
    * 
  FROM 
    htmlDump 
  WHERE 
    crawled_by_id = ? AND
    htmlDumpCreated > ? AND
    dump LIKE '%wvmpUpdate?start=%' 
  ORDER BY 
    htmlDumpCreated ASC
");
    $query->execute(array($crawledById, $startDate));

    while ($htmlDump = $query->fetch()) {
        $html = HtmlStorage::retrieve("htmlDump", $htmlDump['crawled_by_id'], $htmlDump['htmlDumpId']);

        $inboundResults = InboundSalesParser::getJsonFromPage($html);

        $viewers = isset($inboundResults->viewers) ? $inboundResults->viewers : [];
        foreach ($viewers as $inbound) {
            if ($inbound->privacy == "PUBLIC" AND !empty($inbound->fullName) AND !empty($inbound->memberId)) {
                $timeAgo = 0;
                $interval = "DAY";

                if (isset($inbound->timeAgo->days)) {
                    $timeAgo = $inbound->timeAgo->days;
                    $interval = "DAY";
                    $randCorrection = 0;
                } elseif (isset($inbound->timeAgo->weeks)) {
                    $timeAgo = $inbound->timeAgo->weeks;
                    $interval = "WEEK";
                    $randCorrection = rand(0, 6);
                } elseif (isset($inbound->timeAgo->months)) {
                    $timeAgo = $inbound->timeAgo->months;
                    $interval = "MONTH";
                    $randCorrection = rand(0, 30);
                } else {
                    continue;
                }
                if (!check4Duplicate($inbound->memberId, $timeAgo, $interval, $htmlDump['htmlDumpCreated'], $htmlDump['crawled_by_id'], $visits)) {
                    $headline = $inbound->headline ?? '';
                    $guessedVisitDate = getGuessedVisitDate($htmlDump['htmlDumpCreated'], $timeAgo, $interval, $randCorrection);
                    $visits[$inbound->memberId][] = $guessedVisitDate;

//                $guessedVisitDate = "DATE_SUB(DATE_SUB(DATE('".$htmlDump['htmlDumpCreated']."'), INTERVAL ".$timeAgo." ".$interval."), INTERVAL ".$randCorrection." DAY)";

                    $insert = DB::prep("INSERT INTO tmp_inbound_visit (crawled_by_id, linked_in_id, linked_in_hashed_id, name, industry_id, industry_name, headline, guessed_visit_date) VALUES(:crawled_by_id, :linked_in_id, :linked_in_hashed_id, :name, :industry_id, :industry_name, :headline, '".$guessedVisitDate."')");
                    $insert->execute([
                        'crawled_by_id' => $crawledById,
                        'linked_in_id' => $inbound->memberId,
                        'linked_in_hashed_id' => '',
                        'name' => $inbound->fullName,
                        'industry_id' => 0,
                        'industry_name' => '',
                        'headline' => $headline
                    ]);
                }
            }
        }
    }
}

function recalcOutboundInboundConnection($crawledById, $startDate, $endDate){
    $outboundVisits = [];
    $query = DB::prep("
            SELECT 
                id, sent_to_crawler_date, member_id
            FROM 
                people
            where 
                crawled_by_id = ?
                AND sent_to_crawler_date BETWEEN date_sub( ? , interval 14 day) AND ?
            ;
        ");
    $query->execute(array($crawledById, $startDate, $endDate));
    while ($outboundVisit = $query->fetch()) {
        $outboundVisits[$outboundVisit['member_id']][] = [
            'id' => $outboundVisit['id'],
            'sent_to_crawler_date' => $outboundVisit['sent_to_crawler_date'],
        ];
    }

    $query = DB::prep("
            SELECT 
                inbound_visit_id, linked_in_id, guessed_visit_date
            FROM 
                inbound_visit 
            where 
                crawled_by_id = ?
                AND guessed_visit_date BETWEEN ? AND ?
                ;
        ");
    $query->execute(array($crawledById, $startDate, $endDate));

    while ($inboundVisit = $query->fetch()) {
        $twoWeekAgo = (new DateTime($inboundVisit['guessed_visit_date']))->sub(new DateInterval("P14D"))->format("Y-m-d");
        if(isset($outboundVisits[$inboundVisit['linked_in_id']])){
            foreach ($outboundVisits[$inboundVisit['linked_in_id']] as $outVisit){
                if($outVisit['sent_to_crawler_date'] <= $inboundVisit['guessed_visit_date'] && $outVisit['sent_to_crawler_date'] >= $twoWeekAgo){
                    $connectionInsertQuery = DB::prep("
                        INSERT IGNORE INTO visit_connection
                        (people_id, inbound_visit_id, guessed_visit_date)
                        VALUES
                        (?, ?, ?)
                    ");
                    $connectionInsertQuery->execute([
                        $outVisit['id'],
                        $inboundVisit['inbound_visit_id'],
                        $inboundVisit['guessed_visit_date']
                    ]);
                }
            }
        }

    }
}

