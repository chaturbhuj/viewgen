<?php

$_SERVER['HTTP_HOST'] = 'jozo.dev.viewgentools.com';

chdir(dirname(__FILE__) . '/../public_html');

include('db.php');
include('lib/recurly.php');

function generateForCompanyId($companyId) {

	$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
	$query->execute([
		'company_id' => $companyId
	]);

	if ($query->rowCount() == 0) {
		echo "No such company";
		return;
	}

	$company = $query->fetch();
	if ($company['recurlyCoupon']) {
		echo "This company has a coupon: " . $company['recurlyCoupon'] . "\n";
		return;
	}

	$coupon = new Recurly_Coupon();
	$coupon->coupon_code = 'vg-' . rand();
	$coupon->duration = 'single_use';
	$coupon->redemption_resource = 'subscription';
	$coupon->max_redemptions_per_account = 1;
	$coupon->name = 'Referal Coupon company_id:#' . $company['id'];
	$coupon->discount_type = 'dollars';
	$coupon->discount_in_cents->addCurrency('USD', 5000);

	$coupon->create();

	$query = DB::prep("UPDATE company SET recurlyCoupon = :coupon WHERE id = :company_id");
	$query->execute([
		'coupon' => $coupon->coupon_code,
		'company_id' => $company['id']
	]);

	echo "COUPON CREATED: " . $coupon->coupon_code . "\n";
}

$query = DB::prep("SELECT company.id, company.company_name FROM company JOIN li_users
	ON(li_users.company_id = company.id) WHERE last_connection > DATE_SUB(NOW(), INTERVAL 1 MONTH) AND state != 'draft' GROUP BY company.id");
$query->execute();

//var_dump($query->rowCount());
while ($row = $query->fetch()) {
	//var_dump($row);
	generateForCompanyId($row['id']);
}

?>
