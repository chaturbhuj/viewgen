<?php
ini_set("max_execution_time", 0);
$IS_CRON_JOB = true;
require_once(__DIR__.'/../public_html/db.php');

$query = DB::prep("
    SELECT
        admin_daily_statistics_id as `id`,
        admin_daily_statistics_date as `date`
    FROM
        admin_daily_statistics
    ;
");
$query->execute();

while ($data = $query->fetch()) {
    echo "Processing date ".$data['date'].": ";
    $queryVisits = DB::prep("
	SELECT 
		count(*)
	FROM inbound_visit 
	WHERE 
		DATE(visit_created) = ?
	");
    $queryVisits->execute([$data['date']]);
    $inboundCount = $queryVisits->fetchColumn();

    $queryUpdate = DB::prep("
	UPDATE 
		admin_daily_statistics
	SET
	    inbound_visits = :visits
	WHERE 
		admin_daily_statistics_id = :id
	");
    $queryUpdate->execute([
        'id' => $data['id'],
        'visits' => $inboundCount
    ]);

    echo $inboundCount."\r\n";
}



