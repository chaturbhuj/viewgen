#!/bin/bash
  REMOTE_DB_USER='dev'
  REMOTE_DB_PASS='aWQLBXkuNEcq'
  REMOTE_DB='searchquant'
  REMOTE_HOST="108.59.9.177"
  LOCAL_DB_USER='dev'
  LOCAL_DB_PASS='HnkjhhwBkPQP80fAZ'
  LOCAL_DB='searchquant'
  LOCAL_HOST="35.184.11.193"
  TABLES=(
    'admin_daily_statistics'
	'api_log'
	'blog_post'
	'campaign'
	'campaign_statistics'
	'client_log'
	'client_versions'
	'company'
	'company_client_version_history'
	'contactForm'
	'coupon'
	'daily_statistics'
	'email'
	'emailTemplate'
	'emailUnsubscribed'
	'email_log'
	'first_connection'
	'first_degree_connection'
	'freeze_cache'
	'front_page_emails'
	'geonames'
	'graph'
	'graph_data'
	'htmlDump'
	'htmlDump_to_client_version'
	'inbound_visit'
	'li_location'
	'li_problem'
	'li_users'
	'new_campaign_debug'
	'page_views'
	'people'
	'people_indexed'
	'people_list'
	'postal_code'
	'postal_code_aggregate'
	'prospects'
	'rc_account'
	'rc_subscription'
	'rc_subscription_history'
	'referer'
	'referer_email_log'
	'relationship_info'
	'reports_cache'
	'search_url'
	'sent_alert'
	'sent_invite'
	'session'
	'software_license'
	'software_status'
	'ssi'
	'title_company'
	'title_global'
	'title_index'
	'tmp_people'
	'user'
	'visit_connection'
	'visitors'
	'zipCode'
    )
count=0
while [ "x${TABLES[count]}" != "x" ]
do
#  Backup
   CUR_TABLE=${TABLES[count]}
   CUR_DB_DUMP="$CUR_TABLE.sql"
   PRIMARY_INDEX_KEY=$(mysql -h $REMOTE_HOST -u $REMOTE_DB_USER --password=$REMOTE_DB_PASS --database=$REMOTE_DB  -e "SHOW INDEX FROM $CUR_TABLE WHERE Key_name = 'PRIMARY';" | sed -n '2p' |awk '{print $5}');
   if [ -z "$PRIMARY_INDEX_KEY" ];
   then
        if [ "$CUR_TABLE" == "htmlDump_to_client_version" ];
            then
                PRIMARY_INDEX_KEY="html_dump_id"
        elif [ "$CUR_TABLE" == "company_client_version_history" ];
            then
                echo "+++++++++++++++++++++"
                REMOTE_MYSQL_COUNT=$(mysql -h $REMOTE_HOST -u $REMOTE_DB_USER --password=$REMOTE_DB_PASS --database=$REMOTE_DB -e "SELECT COUNT(*) FROM $CUR_TABLE");
                REMOTE_COUNT=${REMOTE_MYSQL_COUNT//[^0-9]/""}
                LOCAL_MYSQL_COUNT=$(mysql -h $LOCAL_HOST -u $LOCAL_DB_USER --password=$LOCAL_DB_PASS --database=$LOCAL_DB -e"SELECT COUNT(*) FROM $CUR_TABLE");
                LOCAL_COUNT=${LOCAL_MYSQL_COUNT//[^0-9]/""}
                if [ "$REMOTE_COUNT" -gt "$LOCAL_COUNT" ];
                    then
                    DB_DIFF=$(($REMOTE_COUNT - $LOCAL_COUNT))
                else
                    DB_DIFF=0
                fi;
                echo "====================== $CUR_TABLE has differences in remote($REMOTE_COUNT) and local($LOCAL_COUNT) in about $DB_DIFF."
                if [ "$DB_DIFF" != "0" ];
                    then
                    echo "====================== Start dump for $CUR_TABLE"
                    mysqldump --skip-add-drop-table --no-create-info -h "$REMOTE_HOST" -u "$REMOTE_DB_USER"  --password="$REMOTE_DB_PASS" "$REMOTE_DB" "$CUR_TABLE" --where="true order by created_at desc limit $DB_DIFF" > $CUR_DB_DUMP
                fi;

        else
            echo "====================== $CUR_TABLE do not have id."
            REMOTE_MYSQL_COUNT=$(mysql -h $REMOTE_HOST -u $REMOTE_DB_USER --password=$REMOTE_DB_PASS --database=$REMOTE_DB -e "SELECT COUNT(*) FROM $CUR_TABLE");
            REMOTE_COUNT=${REMOTE_MYSQL_COUNT//[^0-9]/""}
            LOCAL_MYSQL_COUNT=$(mysql -h $LOCAL_HOST -u $LOCAL_DB_USER --password=$LOCAL_DB_PASS --database=$LOCAL_DB -e"SELECT COUNT(*) FROM $CUR_TABLE");
            LOCAL_COUNT=${LOCAL_MYSQL_COUNT//[^0-9]/""}
            if [ "$REMOTE_COUNT" != "$LOCAL_COUNT" ];
                then
                DB_DIFF=$(($REMOTE_COUNT - $LOCAL_COUNT))
                echo "====================== $CUR_TABLE has differences in remote and local in about $DB_DIFF.  Write entire DB..."
                mysqldump --skip-add-drop-table --no-create-info -h "$REMOTE_HOST" -u "$REMOTE_DB_USER"  --password="$REMOTE_DB_PASS" "$REMOTE_DB" "$CUR_TABLE" > $CUR_DB_DUMP
            else
                echo "====================== $CUR_TABLE is same on remote and local. Continue..."
                count=$(( $count + 1 ))
                continue;
            fi;

        fi;
   fi;
   if [ ! -z "$PRIMARY_INDEX_KEY" ];
    then
    echo "====================== Primary key for $CUR_TABLE is: $PRIMARY_INDEX_KEY";
    MYSQL_LAST_INDEX=$(mysql -h $LOCAL_HOST -u $LOCAL_DB_USER --password=$LOCAL_DB_PASS --database=$LOCAL_DB -e "SELECT MAX($PRIMARY_INDEX_KEY) FROM $CUR_TABLE");
    CUR_LAST_INDEX=${MYSQL_LAST_INDEX//[^0-9]/""}
    if [ -z "$CUR_LAST_INDEX" ];
        then
        CUR_LAST_INDEX=0
    fi
    echo "====================== Start dump for $CUR_TABLE"
    mysqldump --skip-add-drop-table --no-create-info -h "$REMOTE_HOST" -u "$REMOTE_DB_USER"  --password="$REMOTE_DB_PASS" "$REMOTE_DB" "$CUR_TABLE" --where="$PRIMARY_INDEX_KEY>$CUR_LAST_INDEX" > $CUR_DB_DUMP
   fi;
   count=$(( $count + 1 ))
#   Restore
   echo "====================== Write dump for $CUR_TABLE to local DB from file $CUR_DB_DUMP and starting from id $CUR_LAST_INDEX"
   mysql -h $LOCAL_HOST -u "$LOCAL_DB_USER"  --password="$LOCAL_DB_PASS" --database="$LOCAL_DB" <  $CUR_DB_DUMP
done
#mysql -h 108.59.9.177 -u dev --password=aWQLBXkuNEcq --database=searchquant -e "SHOW INDEX FROM company_client_version_history WHERE Key_name = 'PRIMARY';"
#mysql -h 108.59.9.177 -u dev --password=aWQLBXkuNEcq --database=searchquant -e "SELECT COUNT(*) FROM company_client_version_history;"
#mysql -h 35.184.11.193 -u dev --password=HnkjhhwBkPQP80fAZ --database=searchquant -e "SELECT COUNT(*) FROM front_page_emails;"
#mysql -h 35.184.11.193 -u dev --password=HnkjhhwBkPQP80fAZ --database=searchquant -e "SELECT COUNT(*) FROM title_index;"
#mysql -h 108.59.9.177 -u dev --password=aWQLBXkuNEcq --database=searchquant -e "SELECT * FROM company_client_version_history LIMIT 1;"

