<?php

$ACTIVE_HEADER_PAGE = 'referer';
$HTML_TITLE = "Referrals";

include('header.php');
include('lib/recurly_loader.php');
include('classes/refererTracking.php');

if (!isset($_GET['company_id'])) {
	die;
}

if (isset($_POST['action']) and $_POST['action'] == "Save") {
	$query = DB::prep("UPDATE li_users SET referer_paypal_account = :paypal WHERE id = :li_user_id");
	$query->execute([
		'paypal' => $_POST['paypal'] == "" ? null : $_POST['paypal'],
		'li_user_id' => $_POST['li_user_id']
	]);
	redirect('referer.php?company_id=' . $_GET['company_id']);
}

$query = DB::prep("SELECT company.*, referer.*, refered_by.name AS refered_by_name FROM company JOIN referer
	ON (refered_company_id = company.id) JOIN
	li_users AS refered_by ON (refered_by.id = referer.refered_by_li_user) WHERE refered_by_company = :company_id
	AND (is_trial OR referer_first_payment IS NOT NULL)");
$query->execute([
	'company_id' => $_GET['company_id']
]);

$refered = $query->fetchAll();

$totalSum = 0;
foreach ($refered as &$r) {
	$r['referer_signed_up'] = date('Y-m-d', strtotime($r['referer_signed_up']));
	$r['company_name'] = preg_replace("# KILLED.*#is", "", $r['company_name']);
	$r['company_name'] = preg_replace("# DRAFT.*#is", "", $r['company_name']);
	$r['refer_payment_received'] = ($r['paid_as_credit'] or $r['paid_to_paypal']) ? RefererTracking::$creditsForRefer . '$' : '';
	$totalSum += ($r['paid_as_credit'] or $r['paid_to_paypal']) ? RefererTracking::$creditsForRefer : 0;
}

$query = DB::prep("SELECT * FROM rc_account WHERE company_id = :company_id");
$query->execute([
	'company_id' => $_GET['company_id']
]);

$rc_account = null;
$availableCredits = 0;
if ($query->rowCount() > 0) {
	$rc_account = $query->fetch();
	$adjustments = Recurly_AdjustmentList::get($rc_account['account_code']);

	foreach ($adjustments as $adjustment) {
		if ($adjustment->origin == "credit") {
			$availableCredits += -($adjustment->unit_amount_in_cents / 100);
		}
	}
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0");
$query->execute([
	'company_id' => $_GET['company_id']
]);

$li_users = $query->fetchAll();

?>

<div class="container">
	<div class="row referer">
		<div class="col-md-6">
			<h3>Refer others and earn quick cash via PayPal!</h3>
			<div class="referer-info-box">
				<div class="width-control">
					<p>1. To get paid your referral $$ via PayPal, add the email address for your PayPal account below. If you don't add your email, you will get a credit to your next monthly bill.</p>
					<p>2. Send the link to as many people as you want, and when they sign up using that link, you will get $75 for each referral. Make sure they use the referral link! </p>
					<? /*foreach ($li_users as $li_user) { ?>
						<div class="input-group">
							<label><?=$li_user['name']?></label>
						</div>
						<div class="input-group">
							<form method="post">
								<input type="hidden" name="li_user_id" value="<?=$li_user['id']?>">
								<input type="text" name="paypal" value="<?=$li_user['referer_paypal_account']?>"
									placeholder="PayPal Account">
								<button name="action" value="Save">Save</button>
							</form>
						</div>
						<input type="text" class="auto-select" value="<?='http://'.$_SERVER['SERVER_NAME'].'/sign_up.php?r=' . $li_user['id']?>">
					<? }
					<div class="o-input-group">
						<label><?=$li_user['name']?></label>
					</div>*/ ?>
					<div class="o-input-group">
						<form method="post">
							<input type="text" name="paypal" value="<?=$_COMPANY_DATA['company_referer_paypal']?>"
								placeholder="PayPal Account">
							<button name="action" value="Save">Save</button>
						</form>
					</div>
					<input type="text" class="auto-select" value="<?='http://'.$_SERVER['SERVER_NAME'].'/sign_up.php?rc=' . $_GET['company_id']?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<h3>Your Referrals</h3>
			<div class="referer-info-box">
				<div class="width-control">
					<? if (count($refered) == 0) { ?>
						<h4>No referrals</h4>
					<? } else { ?>
						<? foreach ($refered as $company) { ?>
							<div class="refered-company-row">
								<span class="refered-company-name"><?=$company['company_name']?></span>
								<span class="refered-company-date">referred by</span>
								<span class="refered-company-refered-by"><?=$company['refered_by_name']?></span>
								<span class="refered-company-date"><?=$company['referer_signed_up']?></span>
								<span class="refered-company-type"><?=$company['paid_to_paypal'] ? 'PayPal' :
									($company['paid_as_credit'] ? 'Credits' : '')?></span>
								<span class="refered-company-payment"><?=$company['refer_payment_received']?></span>
							</div>
						<? } ?>
					<? } ?>
				</div>
			</div>
			<div class="refered-company-note">
				Note that amounts given as a credit will be applied at the end of your current billing cycle.
			</div>
			<div class="refered-company-total">
				<span class="refered-company-total-text">Total:</span>
				<span class="refered-company-total-amount"><?=$totalSum?>$</span>
			</div>
			<? /*
			<div class="refered-company-total">
				<span class="refered-company-total-text">Credits left:</span>
				<span class="refered-company-total-amount"><?=$availableCredits?>$</span>
			</div>
			  */ ?>
		</div>
	</div>
</div>

<?
include('footer.php');
?>
