<?php

include('../authenticate.php');

$uri = $_SERVER['REQUEST_URI'];

$fileName = "index_public.php";
if (preg_match('#\/([a-zA-Z0-9\.\/_-]*)#', $uri, $match)) {
	$fileName = str_replace('..', '', $match[1]);
}

if (!authenticate()) {
	die;
}

$fileName = $_SERVER['REQUEST_URI'];

$fileName = ".." . $fileName;

header("Content-Type: ". mime_content_type($fileName));

$fp = fopen($fileName, "r");
fpassthru($fp);

?>
