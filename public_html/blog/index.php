<?php

include('../authenticate.php');

$uri = $_SERVER['REQUEST_URI'];

$fileName = "index_public.php";
if (preg_match('#\/([a-zA-Z0-9\.\/_-]*)#', $uri, $match)) {
	$fileName = str_replace('..', '', $match[1]);
}

if (!authenticate()) {
	header('Location: /');
	die;
}
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
