<?php

function copy_campaign($campaign_id, $prefix = '', $setRevisitFromCampaignId = false) {
	// Returns the new campaign id.
	$query = DB::prep("CREATE TEMPORARY TABLE tmp SELECT * FROM campaign WHERE id = ?;");
	$query->execute(array($campaign_id));
	if ($setRevisitFromCampaignId) {
		$query = DB::prep("UPDATE tmp SET revisit_from_campaign_id = id");
		$query->execute(array());
	} else {
		$query = DB::prep("UPDATE tmp SET revisit_from_campaign_id = 0");
		$query->execute(array());
	}
	
	$query = DB::prep("ALTER TABLE tmp drop ID");
	$query->execute(array());
	$query = DB::prep("UPDATE tmp SET name = CONCAT('".qq($prefix)."',name), publish_status='draft'");
	$query->execute(array());
	$query = DB::prep("UPDATE tmp SET status = 'Active' WHERE status = 'Finished'");
	$query->execute(array());
	$query = DB::prep("UPDATE tmp SET created = NOW(), campaign_completed = '0000-00-00 00:00:00', campaign_last_visit = '0000-00-00 00:00:00', campaign_visit_count = 0, visits = 0, total_visits = 0, campaign_vbr = 0, total_visitors = 0, has_sent_campaign_finished_email = 0");
	$query->execute(array());
    $query = DB::prep("INSERT INTO campaign SELECT NULL,tmp.* FROM tmp;");
	$query->execute(array());
	$new_campaign_id = DB::get_instance()->lastInsertId();
	
	$query = DB::prep("DROP TABLE tmp;");
	$query->execute(array());

	$query = DB::prep("select * from campaign where id=?");
	$query->execute(array($new_campaign_id));

	$campaign = $query->fetch();

	// Copy search_urls.
	$query = DB::prep("
	INSERT INTO search_url
		(id,
		campaign_id,
		company_id,
		url,
		finished,
		paused,
		visit_by,
		visit_by_id,
		total_matches,
		name,
		revisit_after,
		visit_after,
		created,
		zip_code_id,
		parent_search_url_id,
		duplicateSkipCount,
		auto_generated)
	(SELECT 
		null, 
		'".qq($new_campaign_id)."', 
		company_id, 
		url, 
		0, 
		0, 
		visit_by, 
		visit_by_id, 
		-1, 
		name, 
		revisit_after, 
		visit_after, 
		NOW(), 
		zip_code_id, 
		parent_search_url_id, 
		0,
		auto_generated
	FROM search_url 
	WHERE campaign_id=? AND parent_search_url_id = 0)");
	$query->execute(array($campaign_id));

	return $new_campaign_id;
}

function make_short($str, $len) {
	if (mb_strlen($str) > $len) {
		return trim(mb_substr($str, 0, $len))."...";
	}
	return $str;
}

function get_os() {
	if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"iphone")){
		return "other";
	}elseif(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"windows")){
		return "windows";
	}else if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"mac")){
		return "mac";
	}
	return "other";
}
function get_mac_client_size($prepend){
	$fileName = dirname(__FILE__) . "/client/SearchQuant.pkg";
	if(file_exists($fileName)){
		return $prepend.round((filesize($fileName)/1024)/1024,1)." MB";
	}else{
		return "";
	}
}
function get_windows_client_size($prepend){
	$fileName = "client/SearchQuantInstaller.exe";
	if(file_exists($fileName)){
		return $prepend.round((filesize($fileName)/1024)/1024,1)." MB";
	}else{
		return "";
	}
}
function db_date_to_format($db_date) {
	$time = strtotime($db_date);
	return date('M j, Y', $time);
}

function db_time_to_format($db_date) {
	$time = strtotime($db_date);
	return date('M j, Y g:i a', $time);
}
function format_date_to_db($format_date) {
	$time = strtotime($format_date);
	return date('Y-m-d', $time);
}

function authenticated() {
	return isset($_SESSION['auth']);
}

$idToIndustry[99] ="Design";
$idToIndustry[98] ="Public Relations and Communications";
$idToIndustry[97] ="Market Research";
$idToIndustry[96] ="Information Technology and Services";
$idToIndustry[95] ="Maritime";
$idToIndustry[94] ="Airlines/Aviation";
$idToIndustry[93] ="Warehousing";
$idToIndustry[92] ="Transportation/Trucking/Railroad";
$idToIndustry[91] ="Consumer Services";
$idToIndustry[90] ="Civic &amp; Social Organization";
$idToIndustry[9] ="Law Practice";
$idToIndustry[89] ="Religious Institutions";
$idToIndustry[88] ="Individual &amp; Family Services";
$idToIndustry[87] ="Package/Freight Delivery";
$idToIndustry[86] ="Environmental Services";
$idToIndustry[85] ="Libraries";
$idToIndustry[84] ="Information Services";
$idToIndustry[83] ="Printing";
$idToIndustry[82] ="Publishing";
$idToIndustry[81] ="Newspapers";
$idToIndustry[80] ="Marketing and Advertising";
$idToIndustry[8] ="Telecommunications";
$idToIndustry[79] ="Public Policy";
$idToIndustry[78] ="Public Safety";
$idToIndustry[77] ="Law Enforcement";
$idToIndustry[76] ="Executive Office";
$idToIndustry[75] ="Government Administration";
$idToIndustry[74] ="International Affairs";
$idToIndustry[73] ="Judiciary";
$idToIndustry[72] ="Legislative Office";
$idToIndustry[71] ="Military";
$idToIndustry[70] ="Research";
$idToIndustry[7] ="Semiconductors";
$idToIndustry[69] ="Education Management";
$idToIndustry[68] ="Higher Education";
$idToIndustry[67] ="Primary/Secondary Education";
$idToIndustry[66] ="Fishery";
$idToIndustry[65] ="Dairy";
$idToIndustry[64] ="Ranching";
$idToIndustry[63] ="Farming";
$idToIndustry[62] ="Railroad Manufacture";
$idToIndustry[61] ="Paper &amp; Forest Products";
$idToIndustry[60] ="Textiles";
$idToIndustry[6] ="Internet";
$idToIndustry[59] ="Utilities";
$idToIndustry[58] ="Shipbuilding";
$idToIndustry[57] ="Oil &amp; Energy";
$idToIndustry[56] ="Mining &amp; Metals";
$idToIndustry[55] ="Machinery";
$idToIndustry[54] ="Chemicals";
$idToIndustry[53] ="Automotive";
$idToIndustry[52] ="Aviation &amp; Aerospace";
$idToIndustry[51] ="Civil Engineering";
$idToIndustry[50] ="Architecture &amp; Planning";
$idToIndustry[5] ="Computer Networking";
$idToIndustry[49] ="Building Materials";
$idToIndustry[48] ="Construction";
$idToIndustry[47] ="Accounting";
$idToIndustry[46] ="Investment Management";
$idToIndustry[45] ="Investment Banking";
$idToIndustry[44] ="Real Estate";
$idToIndustry[43] ="Financial Services";
$idToIndustry[42] ="Insurance";
$idToIndustry[41] ="Banking";
$idToIndustry[40] ="Recreational Facilities and Services";
$idToIndustry[4] ="Computer Software";
$idToIndustry[39] ="Performing Arts";
$idToIndustry[38] ="Fine Art";
$idToIndustry[37] ="Museums and Institutions";
$idToIndustry[36] ="Broadcast Media";
$idToIndustry[35] ="Motion Pictures and Film";
$idToIndustry[34] ="Food &amp; Beverages";
$idToIndustry[33] ="Sports";
$idToIndustry[32] ="Restaurants";
$idToIndustry[31] ="Hospitality";
$idToIndustry[30] ="Leisure";
$idToIndustry[3] ="Computer Hardware";
$idToIndustry[29] ="Gambling &amp; Casinos";
$idToIndustry[28] ="Entertainment";
$idToIndustry[27] ="Retail";
$idToIndustry[26] ="Furniture";
$idToIndustry[25] ="Consumer Goods";
$idToIndustry[24] ="Consumer Electronics";
$idToIndustry[23] ="Food Production";
$idToIndustry[22] ="Supermarkets";
$idToIndustry[21] ="Tobacco";
$idToIndustry[20] ="Sporting Goods";
$idToIndustry[19] ="Apparel &amp; Fashion";
$idToIndustry[18] ="Cosmetics";
$idToIndustry[17] ="Medical Devices";
$idToIndustry[16] ="Veterinary";
$idToIndustry[15] ="Pharmaceuticals";
$idToIndustry[148] ="Government Relations";
$idToIndustry[147] ="Industrial Automation";
$idToIndustry[146] ="Packaging and Containers";
$idToIndustry[145] ="Glass";
$idToIndustry[144] ="Renewables &amp; Environment";
$idToIndustry[143] ="Luxury Goods &amp; Jewelry";
$idToIndustry[142] ="Wine and Spirits";
$idToIndustry[141] ="International Trade and Development";
$idToIndustry[140] ="Graphic Design";
$idToIndustry[14] ="Hospital &amp; Health Care";
$idToIndustry[139] ="Mental Health Care";
$idToIndustry[138] ="Business Supplies and Equipment";
$idToIndustry[137] ="Human Resources";
$idToIndustry[136] ="Photography";
$idToIndustry[135] ="Mechanical or Industrial Engineering";
$idToIndustry[134] ="Import and Export";
$idToIndustry[133] ="Wholesale";
$idToIndustry[132] ="E-Learning";
$idToIndustry[131] ="Philanthropy";
$idToIndustry[130] ="Think Tanks";
$idToIndustry[13] ="Medical Practice";
$idToIndustry[129] ="Capital Markets";
$idToIndustry[128] ="Commercial Real Estate";
$idToIndustry[127] ="Animation";
$idToIndustry[126] ="Media Production";
$idToIndustry[125] ="Alternative Medicine";
$idToIndustry[124] ="Health";
$idToIndustry[123] ="Outsourcing/Offshoring";
$idToIndustry[122] ="Facilities Services";
$idToIndustry[121] ="Security and Investigations";
$idToIndustry[120] ="Alternative Dispute Resolution";
$idToIndustry[12] ="Biotechnology";
$idToIndustry[119] ="Wireless";
$idToIndustry[118] ="Computer &amp; Network Security";
$idToIndustry[117] ="Plastics";
$idToIndustry[116] ="Logistics and Supply Chain";
$idToIndustry[115] ="Music";
$idToIndustry[114] ="Nanotechnology";
$idToIndustry[113] ="Online Media";
$idToIndustry[112] ="Electrical/Electronic Manufacturing";
$idToIndustry[111] ="Arts and Crafts";
$idToIndustry[110] ="Events Services";
$idToIndustry[11] ="Management Consulting";
$idToIndustry[109] ="Computer Games";
$idToIndustry[108] ="Translation and Localization";
$idToIndustry[107] ="Political Organization";
$idToIndustry[106] ="Venture Capital &amp; Private Equity";
$idToIndustry[105] ="Professional Training &amp; Coaching";
$idToIndustry[104] ="Staffing and Recruiting";
$idToIndustry[103] ="Writing and Editing";
$idToIndustry[102] ="Program Development";
$idToIndustry[101] ="Fund-Raising";
$idToIndustry[100] ="Nonprofit Organization Management";
$idToIndustry[10] ="Legal Services";
$idToIndustry[1] ="Defense &amp; Space";
uasort($idToIndustry, function($a, $b) {return $a>$b;});



$newIdToSeniorityLevel[1] ="Unpaid";
$newIdToSeniorityLevel[2] ="Training";
$newIdToSeniorityLevel[3] ="Entry";
$newIdToSeniorityLevel[4] ="Senior";
$newIdToSeniorityLevel[5] ="Manager";
$newIdToSeniorityLevel[6] ="Director";
$newIdToSeniorityLevel[7] ="VP";
$newIdToSeniorityLevel[8] ="CXO";
$newIdToSeniorityLevel[9] ="Partner";
$newIdToSeniorityLevel[10] ="Owner";

$newIdToFunction[1] ="Accounting";
$newIdToFunction[2] ="Administrative";
$newIdToFunction[3] ="Arts and Design";
$newIdToFunction[4] ="Business Development";
$newIdToFunction[5] ="Community and Social Services";
$newIdToFunction[6] ="Consulting";
$newIdToFunction[7] ="Education";
$newIdToFunction[8] ="Engineering";
$newIdToFunction[9] ="Entrepreneurship";
$newIdToFunction[10] ="Finance";
$newIdToFunction[11] ="Healthcare Services";
$newIdToFunction[12] ="Human Resources";
$newIdToFunction[13] ="Information Technology";
$newIdToFunction[14] ="Legal";
$newIdToFunction[15] ="Marketing";
$newIdToFunction[16] ="Media and Communication";
$newIdToFunction[17] ="Military and Protective Services";
$newIdToFunction[18] ="Operations";
$newIdToFunction[19] ="Product Management";
$newIdToFunction[20] ="Program and Project Management";
$newIdToFunction[21] ="Purchasing";
$newIdToFunction[22] ="Quality Assurance";
$newIdToFunction[23] ="Real Estate";
$newIdToFunction[24] ="Research";
$newIdToFunction[25] ="Sales";
$newIdToFunction[26] ="Support";

$idToCompanySize[1] ="1 to 10";
$idToCompanySize[2] ="11 to 50";
$idToCompanySize[3] ="51 to 200";
$idToCompanySize[4] ="201 to 500";
$idToCompanySize[5] ="501 to 1000";
$idToCompanySize[6] ="1001 to 5000";
$idToCompanySize[7] ="5001 to 10000";
$idToCompanySize[8] ="10000+";

$idToRelationship["F"] = "1st Connections";
$idToRelationship["S"] = "2nd Connections";
$idToRelationship["A"] = "Group Members";
$idToRelationship["O"] = "3rd + Everyone Else";

$visitsOptions[] = 50;
$visitsOptions[] = 100;
$visitsOptions[] = 200;
$visitsOptions[] = 300;
$visitsOptions[] = 400;
$visitsOptions[] = 500;
$visitsOptions[] = 600;
$visitsOptions[] = 700;
$visitsOptions[] = 800;
$visitsOptions[] = 900;
$visitsOptions[] = 1000;

function base_url($uri = "") {
	global $BASE_URL;
	if (preg_match('#^http#', $uri)) return $uri;
	return sprintf('%s/%s', $BASE_URL, ltrim($uri, '/'));
}

function redirect($uri = "") {
	header(sprintf('Location: %s', base_url($uri)));
	exit;
}

function format_multi_value($str, $num = 2, $len = 30, $translate = array(), $str_key = false) {
	if (trim($str) === "") return "";
	$kw = explode("\n", $str);
	
	if (!empty($translate)) {
		foreach ($kw as $key => $value) {
			$value = trim($value);
			if (!isset($translate[$str_key ? $value : (int)$value])) continue;
			$kw[$key] = trim($translate[$str_key ? $value : (int)$value]);
		}
	}

	$kwstr = implode(", ", array_slice($kw, 0, $num));
	if (strlen($kwstr) > $len) $kwstr = substr($kwstr, 0, $len)."...";
	if (count($kw) > $num) {
		$kwstr .= sprintf(" + %d more", count($kw)-$num);
	}
	return $kwstr;
}

function property_list_to_array($list) {
	$list = explode("\n", $list);
	$ret = [];
	foreach ($list as $val) {
		$val = trim($val);
		if ($val == "") continue;
		$ret[] = $val;
	}
	return $ret;
}

function make_hash_password($password,$salt){
	return hash("sha256",$password.$salt);
}

function isValidEmail($email){ 
	//eregi is not supported, we take all
	return true;
    /* $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,20})$"; 

     if (@eregi($pattern, $email)){ 
        return true; 
     } 
     else { 
        return false; 
     }    */
}
$graphId = 0;
function makeGraph($data,$width,$height,$labels){
	GLOBAL $graphId;
	echo '<script type="text/javascript">
            var chart;
            var chartData'.$graphId.' = [];
            var chartCursor;



            AmCharts.ready(function () {
                // generate some data first
               

                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.pathToImages = "../amcharts/images/";
				chart.dataDateFormat = "YYYY-MM-DD";
                chart.dataProvider = chartData'.$graphId.';
                chart.categoryField = "date";
                chart.balloon.bulletSize = 5;
				
                // listen for "dataUpdated" event (fired when chart is rendered) and call zoomChart method when it happens
                chart.addListener("dataUpdated", zoomChart);

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
                categoryAxis.dashLength = 1;
                categoryAxis.minorGridEnabled = true;
                categoryAxis.twoLineMode = true;
                categoryAxis.dateFormats = [{
                    period: \'fff\',
                    format: \'JJ:NN:SS\'
                }, {
                    period: \'ss\',
                    format: \'JJ:NN:SS\'
                }, {
                    period: \'mm\',
                    format: \'JJ:NN\'
                }, {
                    period: \'hh\',
                    format: \'JJ:NN\'
                }, {
                    period: \'DD\',
                    format: \'DD\'
                }, {
                    period: \'WW\',
                    format: \'DD\'
                }, {
                    period: \'MM\',
                    format: \'MMM\'
                }, {
                    period: \'YYYY\',
                    format: \'YYYY\'
                }];

                categoryAxis.axisColor = "#DADADA";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.dashLength = 1;
                chart.addValueAxis(valueAxis);';
				
				if ($labels[0] == "Profiles visited (Daily)") {
					echo '
					var guide = new AmCharts.Guide();
					guide.value = 500;
					guide.toValue = 1000;
					guide.fillColor = "#00CC00";
					guide.inside = true;
					guide.fillAlpha = 0.2;
					guide.lineAlpha = 0;
					valueAxis.addGuide(guide);';
				}
				
				$labelNr = 0;
				$colors = array("#0E3D59","#D92525","#F29F05","#8dc63f","#B300B3");
				foreach ($labels as $label) {
					if ($labels[0] != "Profiles visited (Daily)") {
						echo 'var graph = new AmCharts.AmGraph();
							graph.title = "'.$label.'";
							graph.valueField = "field'.$labelNr.'";
							graph.bullet = "round";
							graph.bulletBorderColor = "#FFFFFF";
							graph.bulletBorderThickness = 2;
							graph.bulletBorderAlpha = 1;
							graph.lineThickness = 2;
							graph.lineColor = "'.$colors[$labelNr].'";
							graph.negativeLineColor = "#efcc26";
							graph.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection
							chart.addGraph(graph);
							';
					} else {
						if ($labelNr == 0) {
							echo 'var graph = new AmCharts.AmGraph();
							graph.title = "'.$label.'";
							graph.valueField = "field'.$labelNr.'";
							graph.bullet = "round";
							graph.bulletBorderColor = "#FFFFFF";
							graph.bulletBorderThickness = 2;
							graph.bulletBorderAlpha = 1;
							graph.lineThickness = 2;
							graph.lineColor = "#8dc63f";
							graph.negativeLineColor = "#efcc26";
							graph.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection
							chart.addGraph(graph);';
						} else if ($labelNr == 1) {
							echo ' var graph = new AmCharts.AmGraph();
							graph.title = "Visitors to profile (Weekly)";
							graph.valueField = "field'.$labelNr.'";
							// graph.bullet = "round";
							graph.type = "step"
							graph.bulletBorderColor = "#FF5555";
							graph.bulletBorderThickness = 1;
							graph.bulletBorderAlpha = 1;
							graph.lineThickness = 2;
							graph.lineColor = "#FF5555";
							graph.negativeLineColor = "#FF5555";
							graph.hideBulletsCount = 50; // this makes the chart to hide bullets when there are more than 50 series in selection
							chart.addGraph(graph);';
						
						}
					}
					
					$labelNr++;
				}
echo '
				var legend = new AmCharts.AmLegend();

				legend.position = "bottom";
				legend.align = "center";
				legend.markerType = "square";
				//legend.valueText = "aa";
                chart.addLegend(legend);
				
                // CURSOR
                chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorPosition = "mouse";
                chartCursor.pan = false; // set it to fals if you want the cursor to work in "select" mode
                chart.addChartCursor(chartCursor);

                // SCROLLBAR
                var chartScrollbar = new AmCharts.ChartScrollbar();
                chart.addChartScrollbar(chartScrollbar);

                chart.creditsPosition = "bottom-right";

                // WRITE
                chart.write("chartdiv'.$graphId.'");
            });

       
			var chartData'.$graphId.' = [';
				for($i=0;$i<count($data);$i++){
					if($i> 0){
						echo ",";
					}
					if (count($labels) == 1) {
						echo " {date: '".$data[$i][0]."',field0: '".$data[$i][1]."'}";
					} else {
						/*echo " {
						   date: '".$data[$i][0]."',
						   field0: '".$data[$i][1]."',
						   field1: '".$data[$i][2]."'
						}";*/
						echo " {date: '".$data[$i][0]."'";
						for($j= 1;$j<count($data[$i]);$j++){
							if ($data[$i][$j] === null) continue;
							echo ",field".($j-1).": '".$data[$i][$j]."'";
						}
						echo "}";
					}
					
				}
            echo '];

            // this method is called when chart is first inited as we listen for "dataUpdated" event
            function zoomChart() {
                // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
                //chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
            }

            // changes cursor mode from pan to select
            function setPanSelect() {
                if (document.getElementById("rb1").checked) {
                    chartCursor.pan = false;
                    chartCursor.zoomable = true;
                } else {
                    chartCursor.pan = true;
                }
                chart.validateNow();
            }

        </script>
		<div id="chartdiv'.$graphId.'" style="width: '.$width.'; height: '.$height.';"></div>
		';
	$graphId++;
}

function distanceLatLong($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist2 = acos($dist);
    $dist3 = rad2deg($dist2);
    $miles = $dist3 * 60 * 1.1515;
	if (is_nan($miles)) {
		return 0;
	}
    return $miles;
}

function getGlobalAverageVisitsPerDay($number_of_days) {

	return 600;

	$query = DB::prep("SELECT
			sent_to_crawler_date,
			COUNT(*) AS volume_per_day,
			COUNT(DISTINCT crawled_by_id) num_active
		FROM people
		WHERE
			sent_to_crawler_date >= DATE_SUB(DATE(NOW()), INTERVAL :number_of_days DAY)
		GROUP BY sent_to_crawler_date");
	$query->execute([
		'number_of_days' => $number_of_days
	]);

	$stats = $query->fetchAll();

	$global_volume_per_day = 0;
	for ($i = 0; $i < $number_of_days; $i++) {
		$global_volume_per_day += $stats[$i]['volume_per_day'] / $stats[$i]['num_active'];
	}
	$global_volume_per_day = ceil(($global_volume_per_day / $number_of_days) / 100) * 100;

	return $global_volume_per_day;
}

function substituteEmailVariables($text, $variables) {
	if (preg_match_all('#\{([a-zA-Z0-9 -]+)\}#is', $text, $matches)) {
		foreach ($matches[0] as $i => $match) {
			if (!isset($variables[$matches[1][$i]])) {
				throw new Exception("Could not find variable \"" . $matches[1][$i] . "\"");
			}
			$text = str_replace($match, $variables[$matches[1][$i]], $text);
		}
	}

	return $text;
}


function readable_parameters($url){
	$returnValues = array();
	$idToIndustry[99] ="Design";
	$idToIndustry[98] ="Public Relations and Communications";
	$idToIndustry[97] ="Market Research";
	$idToIndustry[96] ="Information Technology and Services";
	$idToIndustry[95] ="Maritime";
	$idToIndustry[94] ="Airlines/Aviation";
	$idToIndustry[93] ="Warehousing";
	$idToIndustry[92] ="Transportation/Trucking/Railroad";
	$idToIndustry[91] ="Consumer Services";
	$idToIndustry[90] ="Civic &amp; Social Organization";
	$idToIndustry[9] ="Law Practice";
	$idToIndustry[89] ="Religious Institutions";
	$idToIndustry[88] ="Individual &amp; Family Services";
	$idToIndustry[87] ="Package/Freight Delivery";
	$idToIndustry[86] ="Environmental Services";
	$idToIndustry[85] ="Libraries";
	$idToIndustry[84] ="Information Services";
	$idToIndustry[83] ="Printing";
	$idToIndustry[82] ="Publishing";
	$idToIndustry[81] ="Newspapers";
	$idToIndustry[80] ="Marketing and Advertising";
	$idToIndustry[8] ="Telecommunications";
	$idToIndustry[79] ="Public Policy";
	$idToIndustry[78] ="Public Safety";
	$idToIndustry[77] ="Law Enforcement";
	$idToIndustry[76] ="Executive Office";
	$idToIndustry[75] ="Government Administration";
	$idToIndustry[74] ="International Affairs";
	$idToIndustry[73] ="Judiciary";
	$idToIndustry[72] ="Legislative Office";
	$idToIndustry[71] ="Military";
	$idToIndustry[70] ="Research";
	$idToIndustry[7] ="Semiconductors";
	$idToIndustry[69] ="Education Management";
	$idToIndustry[68] ="Higher Education";
	$idToIndustry[67] ="Primary/Secondary Education";
	$idToIndustry[66] ="Fishery";
	$idToIndustry[65] ="Dairy";
	$idToIndustry[64] ="Ranching";
	$idToIndustry[63] ="Farming";
	$idToIndustry[62] ="Railroad Manufacture";
	$idToIndustry[61] ="Paper &amp; Forest Products";
	$idToIndustry[60] ="Textiles";
	$idToIndustry[6] ="Internet";
	$idToIndustry[59] ="Utilities";
	$idToIndustry[58] ="Shipbuilding";
	$idToIndustry[57] ="Oil &amp; Energy";
	$idToIndustry[56] ="Mining &amp; Metals";
	$idToIndustry[55] ="Machinery";
	$idToIndustry[54] ="Chemicals";
	$idToIndustry[53] ="Automotive";
	$idToIndustry[52] ="Aviation &amp; Aerospace";
	$idToIndustry[51] ="Civil Engineering";
	$idToIndustry[50] ="Architecture &amp; Planning";
	$idToIndustry[5] ="Computer Networking";
	$idToIndustry[49] ="Building Materials";
	$idToIndustry[48] ="Construction";
	$idToIndustry[47] ="Accounting";
	$idToIndustry[46] ="Investment Management";
	$idToIndustry[45] ="Investment Banking";
	$idToIndustry[44] ="Real Estate";
	$idToIndustry[43] ="Financial Services";
	$idToIndustry[42] ="Insurance";
	$idToIndustry[41] ="Banking";
	$idToIndustry[40] ="Recreational Facilities and Services";
	$idToIndustry[4] ="Computer Software";
	$idToIndustry[39] ="Performing Arts";
	$idToIndustry[38] ="Fine Art";
	$idToIndustry[37] ="Museums and Institutions";
	$idToIndustry[36] ="Broadcast Media";
	$idToIndustry[35] ="Motion Pictures and Film";
	$idToIndustry[34] ="Food &amp; Beverages";
	$idToIndustry[33] ="Sports";
	$idToIndustry[32] ="Restaurants";
	$idToIndustry[31] ="Hospitality";
	$idToIndustry[30] ="Leisure";
	$idToIndustry[3] ="Computer Hardware";
	$idToIndustry[29] ="Gambling &amp; Casinos";
	$idToIndustry[28] ="Entertainment";
	$idToIndustry[27] ="Retail";
	$idToIndustry[26] ="Furniture";
	$idToIndustry[25] ="Consumer Goods";
	$idToIndustry[24] ="Consumer Electronics";
	$idToIndustry[23] ="Food Production";
	$idToIndustry[22] ="Supermarkets";
	$idToIndustry[21] ="Tobacco";
	$idToIndustry[20] ="Sporting Goods";
	$idToIndustry[19] ="Apparel &amp; Fashion";
	$idToIndustry[18] ="Cosmetics";
	$idToIndustry[17] ="Medical Devices";
	$idToIndustry[16] ="Veterinary";
	$idToIndustry[15] ="Pharmaceuticals";
	$idToIndustry[148] ="Government Relations";
	$idToIndustry[147] ="Industrial Automation";
	$idToIndustry[146] ="Packaging and Containers";
	$idToIndustry[145] ="Glass";
	$idToIndustry[144] ="Renewables &amp; Environment";
	$idToIndustry[143] ="Luxury Goods &amp; Jewelry";
	$idToIndustry[142] ="Wine and Spirits";
	$idToIndustry[141] ="International Trade and Development";
	$idToIndustry[140] ="Graphic Design";
	$idToIndustry[14] ="Hospital &amp; Health Care";
	$idToIndustry[139] ="Mental Health Care";
	$idToIndustry[138] ="Business Supplies and Equipment";
	$idToIndustry[137] ="Human Resources";
	$idToIndustry[136] ="Photography";
	$idToIndustry[135] ="Mechanical or Industrial Engineering";
	$idToIndustry[134] ="Import and Export";
	$idToIndustry[133] ="Wholesale";
	$idToIndustry[132] ="E-Learning";
	$idToIndustry[131] ="Philanthropy";
	$idToIndustry[130] ="Think Tanks";
	$idToIndustry[13] ="Medical Practice";
	$idToIndustry[129] ="Capital Markets";
	$idToIndustry[128] ="Commercial Real Estate";
	$idToIndustry[127] ="Animation";
	$idToIndustry[126] ="Media Production";
	$idToIndustry[125] ="Alternative Medicine";
	$idToIndustry[124] ="Health";
	$idToIndustry[123] ="Outsourcing/Offshoring";
	$idToIndustry[122] ="Facilities Services";
	$idToIndustry[121] ="Security and Investigations";
	$idToIndustry[120] ="Alternative Dispute Resolution";
	$idToIndustry[12] ="Biotechnology";
	$idToIndustry[119] ="Wireless";
	$idToIndustry[118] ="Computer &amp; Network Security";
	$idToIndustry[117] ="Plastics";
	$idToIndustry[116] ="Logistics and Supply Chain";
	$idToIndustry[115] ="Music";
	$idToIndustry[114] ="Nanotechnology";
	$idToIndustry[113] ="Online Media";
	$idToIndustry[112] ="Electrical/Electronic Manufacturing";
	$idToIndustry[111] ="Arts and Crafts";
	$idToIndustry[110] ="Events Services";
	$idToIndustry[11] ="Management Consulting";
	$idToIndustry[109] ="Computer Games";
	$idToIndustry[108] ="Translation and Localization";
	$idToIndustry[107] ="Political Organization";
	$idToIndustry[106] ="Venture Capital &amp; Private Equity";
	$idToIndustry[105] ="Professional Training &amp; Coaching";
	$idToIndustry[104] ="Staffing and Recruiting";
	$idToIndustry[103] ="Writing and Editing";
	$idToIndustry[102] ="Program Development";
	$idToIndustry[101] ="Fund-Raising";
	$idToIndustry[100] ="Nonprofit Organization Management";
	$idToIndustry[10] ="Legal Services";
	$idToIndustry[1] ="Defense &amp; Space";

	

	$idToLanguage["ab"] ="Abkhaz";
$idToLanguage["aa"] ="Afar";
$idToLanguage["af"] ="Afrikaans";
$idToLanguage["ak"] ="Akan";
$idToLanguage["sq"] ="Albanian";
$idToLanguage["am"] ="Amharic";
$idToLanguage["ar"] ="Arabic";
$idToLanguage["an"] ="Aragonese";
$idToLanguage["hy"] ="Armenian";
$idToLanguage["as"] ="Assamese";
$idToLanguage["av"] ="Avaric";
$idToLanguage["ae"] ="Avestan";
$idToLanguage["ay"] ="Aymara";
$idToLanguage["az"] ="Azerbaijani";
$idToLanguage["bm"] ="Bambara";
$idToLanguage["ba"] ="Bashkir";
$idToLanguage["eu"] ="Basque";
$idToLanguage["be"] ="Belarusian";
$idToLanguage["bn"] ="Bengali, Bangla";
$idToLanguage["bh"] ="Bihari";
$idToLanguage["bi"] ="Bislama";
$idToLanguage["bs"] ="Bosnian";
$idToLanguage["br"] ="Breton";
$idToLanguage["bg"] ="Bulgarian";
$idToLanguage["my"] ="Burmese";
$idToLanguage["ca"] ="Catalan";
$idToLanguage["ch"] ="Chamorro";
$idToLanguage["ce"] ="Chechen";
$idToLanguage["ny"] ="Chichewa, Chewa, Nyanja";
$idToLanguage["zh"] ="Chinese";
$idToLanguage["cv"] ="Chuvash";
$idToLanguage["kw"] ="Cornish";
$idToLanguage["co"] ="Corsican";
$idToLanguage["cr"] ="Cree";
$idToLanguage["hr"] ="Croatian";
$idToLanguage["cs"] ="Czech";
$idToLanguage["da"] ="Danish";
$idToLanguage["dv"] ="Divehi, Dhivehi, Maldivian";
$idToLanguage["nl"] ="Dutch";
$idToLanguage["dz"] ="Dzongkha";
$idToLanguage["en"] ="English";
$idToLanguage["eo"] ="Esperanto";
$idToLanguage["et"] ="Estonian";
$idToLanguage["ee"] ="Ewe";
$idToLanguage["fo"] ="Faroese";
$idToLanguage["fj"] ="Fijian";
$idToLanguage["fi"] ="Finnish";
$idToLanguage["fr"] ="French";
$idToLanguage["ff"] ="Fula, Fulah, Pulaar, Pular";
$idToLanguage["gl"] ="Galician";
$idToLanguage["ka"] ="Georgian";
$idToLanguage["de"] ="German";
$idToLanguage["el"] ="Greek (modern)";
$idToLanguage["gn"] ="Guaraní";
$idToLanguage["gu"] ="Gujarati";
$idToLanguage["ht"] ="Haitian, Haitian Creole";
$idToLanguage["ha"] ="Hausa";
$idToLanguage["he"] ="Hebrew (modern)";
$idToLanguage["hz"] ="Herero";
$idToLanguage["hi"] ="Hindi";
$idToLanguage["ho"] ="Hiri Motu";
$idToLanguage["hu"] ="Hungarian";
$idToLanguage["ia"] ="Interlingua";
$idToLanguage["id"] ="Indonesian";
$idToLanguage["ie"] ="Interlingue";
$idToLanguage["ga"] ="Irish";
$idToLanguage["ig"] ="Igbo";
$idToLanguage["ik"] ="Inupiaq";
$idToLanguage["io"] ="Ido";
$idToLanguage["is"] ="Icelandic";
$idToLanguage["it"] ="Italian";
$idToLanguage["iu"] ="Inuktitut";
$idToLanguage["ja"] ="Japanese";
$idToLanguage["jv"] ="Javanese";
$idToLanguage["kl"] ="Kalaallisut, Greenlandic";
$idToLanguage["kn"] ="Kannada";
$idToLanguage["kr"] ="Kanuri";
$idToLanguage["ks"] ="Kashmiri";
$idToLanguage["kk"] ="Kazakh";
$idToLanguage["km"] ="Khmer";
$idToLanguage["ki"] ="Kikuyu, Gikuyu";
$idToLanguage["rw"] ="Kinyarwanda";
$idToLanguage["ky"] ="Kyrgyz";
$idToLanguage["kv"] ="Komi";
$idToLanguage["kg"] ="Kongo";
$idToLanguage["ko"] ="Korean";
$idToLanguage["ku"] ="Kurdish";
$idToLanguage["kj"] ="Kwanyama, Kuanyama";
$idToLanguage["la"] ="Latin";
$idToLanguage["lb"] ="Luxembourgish, Letzeburgesch";
$idToLanguage["lg"] ="Ganda";
$idToLanguage["li"] ="Limburgish, Limburgan, Limburger";
$idToLanguage["ln"] ="Lingala";
$idToLanguage["lo"] ="Lao";
$idToLanguage["lt"] ="Lithuanian";
$idToLanguage["lu"] ="Luba-Katanga";
$idToLanguage["lv"] ="Latvian";
$idToLanguage["gv"] ="Manx";
$idToLanguage["mk"] ="Macedonian";
$idToLanguage["mg"] ="Malagasy";
$idToLanguage["ms"] ="Malay";
$idToLanguage["ml"] ="Malayalam";
$idToLanguage["mt"] ="Maltese";
$idToLanguage["mi"] ="Māori";
$idToLanguage["mr"] ="Marathi (Marāṭhī)";
$idToLanguage["mh"] ="Marshallese";
$idToLanguage["mn"] ="Mongolian";
$idToLanguage["na"] ="Nauruan";
$idToLanguage["nv"] ="Navajo, Navaho";
$idToLanguage["nd"] ="Northern Ndebele";
$idToLanguage["ne"] ="Nepali";
$idToLanguage["ng"] ="Ndonga";
$idToLanguage["nb"] ="Norwegian Bokmål";
$idToLanguage["nn"] ="Norwegian Nynorsk";
$idToLanguage["no"] ="Norwegian";
$idToLanguage["ii"] ="Nuosu";
$idToLanguage["nr"] ="Southern Ndebele";
$idToLanguage["oc"] ="Occitan";
$idToLanguage["oj"] ="Ojibwe, Ojibwa";
$idToLanguage["cu"] ="Old Church Slavonic, Church Slavonic, Old Bulgarian";
$idToLanguage["om"] ="Oromo";
$idToLanguage["or"] ="Oriya";
$idToLanguage["os"] ="Ossetian, Ossetic";
$idToLanguage["pa"] ="Eastern Punjabi, Eastern Panjabi";
$idToLanguage["pi"] ="Pāli";
$idToLanguage["fa"] ="Persian (Farsi)";
$idToLanguage["pl"] ="Polish";
$idToLanguage["ps"] ="Pashto, Pushto";
$idToLanguage["pt"] ="Portuguese";
$idToLanguage["qu"] ="Quechua";
$idToLanguage["rm"] ="Romansh";
$idToLanguage["rn"] ="Kirundi";
$idToLanguage["ro"] ="Romanian";
$idToLanguage["ru"] ="Russian";
$idToLanguage["sa"] ="Sanskrit (Saṁskṛta)";
$idToLanguage["sc"] ="Sardinian";
$idToLanguage["sd"] ="Sindhi";
$idToLanguage["se"] ="Northern Sami";
$idToLanguage["sm"] ="Samoan";
$idToLanguage["sg"] ="Sango";
$idToLanguage["sr"] ="Serbian";
$idToLanguage["gd"] ="Scottish Gaelic, Gaelic";
$idToLanguage["sn"] ="Shona";
$idToLanguage["si"] ="Sinhalese, Sinhala";
$idToLanguage["sk"] ="Slovak";
$idToLanguage["sl"] ="Slovene";
$idToLanguage["so"] ="Somali";
$idToLanguage["st"] ="Southern Sotho";
$idToLanguage["es"] ="Spanish";
$idToLanguage["su"] ="Sundanese";
$idToLanguage["sw"] ="Swahili";
$idToLanguage["ss"] ="Swati";
$idToLanguage["sv"] ="Swedish";
$idToLanguage["ta"] ="Tamil";
$idToLanguage["te"] ="Telugu";
$idToLanguage["tg"] ="Tajik";
$idToLanguage["th"] ="Thai";
$idToLanguage["ti"] ="Tigrinya";
$idToLanguage["bo"] ="Tibetan Standard, Tibetan, Central";
$idToLanguage["tk"] ="Turkmen";
$idToLanguage["tl"] ="Tagalog";
$idToLanguage["tn"] ="Tswana";
$idToLanguage["to"] ="Tonga (Tonga Islands)";
$idToLanguage["tr"] ="Turkish";
$idToLanguage["ts"] ="Tsonga";
$idToLanguage["tt"] ="Tatar";
$idToLanguage["tw"] ="Twi";
$idToLanguage["ty"] ="Tahitian";
$idToLanguage["ug"] ="Uyghur";
$idToLanguage["uk"] ="Ukrainian";
$idToLanguage["ur"] ="Urdu";
$idToLanguage["uz"] ="Uzbek";
$idToLanguage["ve"] ="Venda";
$idToLanguage["vi"] ="Vietnamese";
$idToLanguage["vo"] ="Volapük";
$idToLanguage["wa"] ="Walloon";
$idToLanguage["cy"] ="Welsh";
$idToLanguage["wo"] ="Wolof";
$idToLanguage["fy"] ="Western Frisian";
$idToLanguage["xh"] ="Xhosa";
$idToLanguage["yi"] ="Yiddish";
$idToLanguage["yo"] ="Yoruba";
$idToLanguage["za"] ="Zhuang, Chuang";
$idToLanguage["zu"] ="Zulu";


$idToCountry["af"] ="Afghanistan";
$idToCountry["al"] ="Albania";
$idToCountry["dz"] ="Algeria";
$idToCountry["as"] ="American Samoa";
$idToCountry["ad"] ="Andorra";
$idToCountry["ao"] ="Angola";
$idToCountry["ai"] ="Anguilla";
$idToCountry["aq"] ="Antarctica";
$idToCountry["ag"] ="Antigua and Barbuda";
$idToCountry["ar"] ="Argentina";
$idToCountry["am"] ="Armenia";
$idToCountry["aw"] ="Aruba";
$idToCountry["au"] ="Australia";
$idToCountry["at"] ="Austria";
$idToCountry["az"] ="Azerbaijan";
$idToCountry["bs"] ="Bahamas";
$idToCountry["bh"] ="Bahrain";
$idToCountry["bd"] ="Bangladesh";
$idToCountry["bb"] ="Barbados";
$idToCountry["by"] ="Belarus";
$idToCountry["be"] ="Belgium";
$idToCountry["bz"] ="Belize";
$idToCountry["bj"] ="Benin";
$idToCountry["bm"] ="Bermuda";
$idToCountry["bt"] ="Bhutan";
$idToCountry["bo"] ="Bolivia";
$idToCountry["bq"] ="Bonaire";
$idToCountry["ba"] ="Bosnia and Herzegovina";
$idToCountry["bw"] ="Botswana";
$idToCountry["bv"] ="Bouvet Island";
$idToCountry["br"] ="Brazil";
$idToCountry["io"] ="British Indian Ocean Territory";
$idToCountry["bn"] ="Brunei Darussalam";
$idToCountry["bg"] ="Bulgaria";
$idToCountry["bf"] ="Burkina Faso";
$idToCountry["bi"] ="Burundi";
$idToCountry["kh"] ="Cambodia";
$idToCountry["cm"] ="Cameroon";
$idToCountry["ca"] ="Canada";
$idToCountry["cv"] ="Cape Verde";
$idToCountry["ky"] ="Cayman Islands";
$idToCountry["cf"] ="Central African Republic";
$idToCountry["td"] ="Chad";
$idToCountry["cl"] ="Chile";
$idToCountry["cn"] ="China";
$idToCountry["cx"] ="Christmas Island";
$idToCountry["cc"] ="Cocos (Keeling) Islands";
$idToCountry["co"] ="Colombia";
$idToCountry["km"] ="Comoros";
$idToCountry["cg"] ="Congo";
$idToCountry["cd"] ="Democratic Republic of the Congo";
$idToCountry["ck"] ="Cook Islands";
$idToCountry["cr"] ="Costa Rica";
$idToCountry["hr"] ="Croatia";
$idToCountry["cu"] ="Cuba";
$idToCountry["cw"] ="CuraÃ§ao";
$idToCountry["cy"] ="Cyprus";
$idToCountry["cz"] ="Czech Republic";
$idToCountry["ci"] ="CÃ´te d'Ivoire";
$idToCountry["dk"] ="Denmark";
$idToCountry["dj"] ="Djibouti";
$idToCountry["dm"] ="Dominica";
$idToCountry["do"] ="Dominican Republic";
$idToCountry["ec"] ="Ecuador";
$idToCountry["eg"] ="Egypt";
$idToCountry["sv"] ="El Salvador";
$idToCountry["gq"] ="Equatorial Guinea";
$idToCountry["er"] ="Eritrea";
$idToCountry["ee"] ="Estonia";
$idToCountry["et"] ="Ethiopia";
$idToCountry["fk"] ="Falkland Islands (Malvinas)";
$idToCountry["fo"] ="Faroe Islands";
$idToCountry["fj"] ="Fiji";
$idToCountry["fi"] ="Finland";
$idToCountry["fr"] ="France";
$idToCountry["gf"] ="French Guiana";
$idToCountry["pf"] ="French Polynesia";
$idToCountry["tf"] ="French Southern Territories";
$idToCountry["ga"] ="Gabon";
$idToCountry["gm"] ="Gambia";
$idToCountry["ge"] ="Georgia";
$idToCountry["de"] ="Germany";
$idToCountry["gh"] ="Ghana";
$idToCountry["gi"] ="Gibraltar";
$idToCountry["gr"] ="Greece";
$idToCountry["gl"] ="Greenland";
$idToCountry["gd"] ="Grenada";
$idToCountry["gp"] ="Guadeloupe";
$idToCountry["gu"] ="Guam";
$idToCountry["gt"] ="Guatemala";
$idToCountry["gg"] ="Guernsey";
$idToCountry["gn"] ="Guinea";
$idToCountry["gw"] ="Guinea-Bissau";
$idToCountry["gy"] ="Guyana";
$idToCountry["ht"] ="Haiti";
$idToCountry["hm"] ="Heard Island and McDonald Mcdonald Islands";
$idToCountry["va"] ="Holy See (Vatican City State)";
$idToCountry["hn"] ="Honduras";
$idToCountry["hk"] ="Hong Kong";
$idToCountry["hu"] ="Hungary";
$idToCountry["is"] ="Iceland";
$idToCountry["in"] ="India";
$idToCountry["id"] ="Indonesia";
$idToCountry["ir"] ="Iran, Islamic Republic of";
$idToCountry["iq"] ="Iraq";
$idToCountry["ie"] ="Ireland";
$idToCountry["im"] ="Isle of Man";
$idToCountry["il"] ="Israel";
$idToCountry["it"] ="Italy";
$idToCountry["jm"] ="Jamaica";
$idToCountry["jp"] ="Japan";
$idToCountry["je"] ="Jersey";
$idToCountry["jo"] ="Jordan";
$idToCountry["kz"] ="Kazakhstan";
$idToCountry["ke"] ="Kenya";
$idToCountry["ki"] ="Kiribati";
$idToCountry["kp"] ="Korea, Democratic People's Republic of";
$idToCountry["kr"] ="Korea, Republic of";
$idToCountry["kw"] ="Kuwait";
$idToCountry["kg"] ="Kyrgyzstan";
$idToCountry["la"] ="Lao People's Democratic Republic";
$idToCountry["lv"] ="Latvia";
$idToCountry["lb"] ="Lebanon";
$idToCountry["ls"] ="Lesotho";
$idToCountry["lr"] ="Liberia";
$idToCountry["ly"] ="Libya";
$idToCountry["li"] ="Liechtenstein";
$idToCountry["lt"] ="Lithuania";
$idToCountry["lu"] ="Luxembourg";
$idToCountry["mo"] ="Macao";
$idToCountry["mk"] ="Macedonia, the Former Yugoslav Republic of";
$idToCountry["mg"] ="Madagascar";
$idToCountry["mw"] ="Malawi";
$idToCountry["my"] ="Malaysia";
$idToCountry["mv"] ="Maldives";
$idToCountry["ml"] ="Mali";
$idToCountry["mt"] ="Malta";
$idToCountry["mh"] ="Marshall Islands";
$idToCountry["mq"] ="Martinique";
$idToCountry["mr"] ="Mauritania";
$idToCountry["mu"] ="Mauritius";
$idToCountry["yt"] ="Mayotte";
$idToCountry["mx"] ="Mexico";
$idToCountry["fm"] ="Micronesia, Federated States of";
$idToCountry["md"] ="Moldova, Republic of";
$idToCountry["mc"] ="Monaco";
$idToCountry["mn"] ="Mongolia";
$idToCountry["me"] ="Montenegro";
$idToCountry["ms"] ="Montserrat";
$idToCountry["ma"] ="Morocco";
$idToCountry["mz"] ="Mozambique";
$idToCountry["mm"] ="Myanmar";
$idToCountry["na"] ="Namibia";
$idToCountry["nr"] ="Nauru";
$idToCountry["np"] ="Nepal";
$idToCountry["nl"] ="Netherlands";
$idToCountry["nc"] ="New Caledonia";
$idToCountry["nz"] ="New Zealand";
$idToCountry["ni"] ="Nicaragua";
$idToCountry["ne"] ="Niger";
$idToCountry["ng"] ="Nigeria";
$idToCountry["nu"] ="Niue";
$idToCountry["nf"] ="Norfolk Island";
$idToCountry["mp"] ="Northern Mariana Islands";
$idToCountry["no"] ="Norway";
$idToCountry["om"] ="Oman";
$idToCountry["pk"] ="Pakistan";
$idToCountry["pw"] ="Palau";
$idToCountry["ps"] ="Palestine, State of";
$idToCountry["pa"] ="Panama";
$idToCountry["pg"] ="Papua New Guinea";
$idToCountry["py"] ="Paraguay";
$idToCountry["pe"] ="Peru";
$idToCountry["ph"] ="Philippines";
$idToCountry["pn"] ="Pitcairn";
$idToCountry["pl"] ="Poland";
$idToCountry["pt"] ="Portugal";
$idToCountry["pr"] ="Puerto Rico";
$idToCountry["qa"] ="Qatar";
$idToCountry["ro"] ="Romania";
$idToCountry["ru"] ="Russian Federation";
$idToCountry["rw"] ="Rwanda";
$idToCountry["re"] ="Reunion";
$idToCountry["bl"] ="Saint Barthelemy";
$idToCountry["sh"] ="Saint Helena";
$idToCountry["kn"] ="Saint Kitts and Nevis";
$idToCountry["lc"] ="Saint Lucia";
$idToCountry["mf"] ="Saint Martin (French part)";
$idToCountry["pm"] ="Saint Pierre and Miquelon";
$idToCountry["vc"] ="Saint Vincent and the Grenadines";
$idToCountry["ws"] ="Samoa";
$idToCountry["sm"] ="San Marino";
$idToCountry["st"] ="Sao Tome and Principe";
$idToCountry["sa"] ="Saudi Arabia";
$idToCountry["sn"] ="Senegal";
$idToCountry["rs"] ="Serbia";
$idToCountry["sc"] ="Seychelles";
$idToCountry["sl"] ="Sierra Leone";
$idToCountry["sg"] ="Singapore";
$idToCountry["sx"] ="Sint Maarten (Dutch part)";
$idToCountry["sk"] ="Slovakia";
$idToCountry["si"] ="Slovenia";
$idToCountry["sb"] ="Solomon Islands";
$idToCountry["so"] ="Somalia";
$idToCountry["za"] ="South Africa";
$idToCountry["gs"] ="South Georgia and the South Sandwich Islands";
$idToCountry["ss"] ="South Sudan";
$idToCountry["es"] ="Spain";
$idToCountry["lk"] ="Sri Lanka";
$idToCountry["sd"] ="Sudan";
$idToCountry["sr"] ="Suriname";
$idToCountry["sj"] ="Svalbard and Jan Mayen";
$idToCountry["sz"] ="Swaziland";
$idToCountry["se"] ="Sweden";
$idToCountry["ch"] ="Switzerland";
$idToCountry["sy"] ="Syrian Arab Republic";
$idToCountry["tw"] ="Taiwan, Province of China";
$idToCountry["tj"] ="Tajikistan";
$idToCountry["tz"] ="United Republic of Tanzania";
$idToCountry["th"] ="Thailand";
$idToCountry["tl"] ="Timor-Leste";
$idToCountry["tg"] ="Togo";
$idToCountry["tk"] ="Tokelau";
$idToCountry["to"] ="Tonga";
$idToCountry["tt"] ="Trinidad and Tobago";
$idToCountry["tn"] ="Tunisia";
$idToCountry["tr"] ="Turkey";
$idToCountry["tm"] ="Turkmenistan";
$idToCountry["tc"] ="Turks and Caicos Islands";
$idToCountry["tv"] ="Tuvalu";
$idToCountry["ug"] ="Uganda";
$idToCountry["ua"] ="Ukraine";
$idToCountry["ae"] ="United Arab Emirates";
$idToCountry["gb"] ="United Kingdom";
$idToCountry["us"] ="United States";
$idToCountry["um"] ="United States Minor Outlying Islands";
$idToCountry["uy"] ="Uruguay";
$idToCountry["uz"] ="Uzbekistan";
$idToCountry["vu"] ="Vanuatu";
$idToCountry["ve"] ="Venezuela";
$idToCountry["vn"] ="Viet Nam";
$idToCountry["vg"] ="British Virgin Islands";
$idToCountry["vi"] ="US Virgin Islands";
$idToCountry["wf"] ="Wallis and Futuna";
$idToCountry["eh"] ="Western Sahara";
$idToCountry["ye"] ="Yemen";
$idToCountry["zm"] ="Zambia";
$idToCountry["zw"] ="Zimbabwe";
$idToCountry["ax"] ="Aland Islands";

$idToFunctionNEW[1] ="Accounting";
$idToFunctionNEW[2] ="Administrative";
$idToFunctionNEW[3] ="Arts and Design";
$idToFunctionNEW[4] ="Business Development";
$idToFunctionNEW[5] ="Community and Social Services";
$idToFunctionNEW[6] ="Consulting";
$idToFunctionNEW[7] ="Education";
$idToFunctionNEW[8] ="Engineering";
$idToFunctionNEW[9] ="Entrepreneurship";
$idToFunctionNEW[10] ="Finance";
$idToFunctionNEW[11] ="Healthcare Services";
$idToFunctionNEW[12] ="Human Resources";
$idToFunctionNEW[13] ="Information Technology";
$idToFunctionNEW[14] ="Legal";
$idToFunctionNEW[15] ="Marketing";
$idToFunctionNEW[16] ="Media and Communication";
$idToFunctionNEW[17] ="Military and Protective Services";
$idToFunctionNEW[18] ="Operations";
$idToFunctionNEW[19] ="Product Management";
$idToFunctionNEW[20] ="Program and Project Management";
$idToFunctionNEW[21] ="Purchasing";
$idToFunctionNEW[22] ="Quality Assurance";
$idToFunctionNEW[23] ="Real Estate";
$idToFunctionNEW[24] ="Research";
$idToFunctionNEW[25] ="Sales";
$idToFunctionNEW[26] ="Support";

$idToSeniorityLevelNEW[1] ="Unpaid";
$idToSeniorityLevelNEW[2] ="Training";
$idToSeniorityLevelNEW[3] ="Entry";
$idToSeniorityLevelNEW[4] ="Senior";
$idToSeniorityLevelNEW[5] ="Manager";
$idToSeniorityLevelNEW[6] ="Director";
$idToSeniorityLevelNEW[7] ="VP";
$idToSeniorityLevelNEW[8] ="CXO";
$idToSeniorityLevelNEW[9] ="Partner";
$idToSeniorityLevelNEW[10] ="Owner";

$idToInterestedIn[1] ="Potential employees";
$idToInterestedIn[2] ="Consultants/contractors";
$idToInterestedIn[4] ="Entrepreneurs";
$idToInterestedIn[8] ="Hiring managers";
$idToInterestedIn[16] ="Industry experts";
$idToInterestedIn[32] ="Deal-making contacts";
$idToInterestedIn[64] ="Reference check";
$idToInterestedIn[128] ="Reconnect";


	$parts = explode("?",$url);
	$parameters = explode("&",str_replace($parts[0]."?","",$url));
	$output = array();
	$output[] = "<a href='$url'>$url</a><br>";
	$score = 0;
	$postalCode = "";
	$countryCode = "";
	foreach($parameters as $parameter){
		$p = explode("=",$parameter);
		$p[1] = urldecode($p[1]);
		if($p[0] == "f_N"){
			$connections = array();
			if(strpos($p[1],"F") !== false){//first
				$connections[] = "1st";
			}
			$connectionTypesCount = 0;
			if(strpos($p[1],"S") !== false){//second

				$connections[] = "2nd";
			}
			
			if(strpos($p[1],"A") !== false){//group

				$connections[] = "Group";
			}
			
			if(strpos($p[1],"O") !== false){//third
			
				$connections[] = "3rd + everyone";
			}
			
			$returnValues["Relationship"] = $connections;
			//echo "Connections: ".implode(", ",$connections)."; ";
			
		}else if($p[0] == "f_P"){
			$readableInterestedIns = array();
			$interestes = explode(",",$p[1]);
			foreach($interestes as $intereste){
				$readableInterestedIns[] = $idToInterestedIn[$intereste];
			}
			$returnValues["Interested In"] = $readableInterestedIns;
			//echo "Function: ".implode(",",$readableFunctions)."; ";
		}else if($p[0] == "f_CN"){
			$readableFunctions = array();
			$functions = explode(",",$p[1]);
			foreach($functions as $function){
				$readableFunctions[] = $idToFunctionNEW[$function];
			}
			$returnValues["Function"] = $readableFunctions;
			//echo "Function: ".implode(",",$readableFunctions)."; ";
		}else if($p[0] == "f_I"){
			
			
			$readableIndustries = array();
			$industries = explode(",",$p[1]);
			foreach($industries as $industriy){
				$readableIndustries[] = $idToIndustry[$industriy];
			}
			$returnValues["Industry"] = $readableIndustries;
			//echo "Industry: ".implode(",",$readableIndustries);
			
		}else if($p[0] == "f_CE"){
			//echo "Seniority Level: ".$p[1]."; ";
			$readableSeniorityLevels = array();
			$seniorityLevels = explode(",",$p[1]);
			foreach($seniorityLevels as $seniorityLevel){
				$readableSeniorityLevels[] = $idToSeniorityLevelNEW[$seniorityLevel];
			}
			$returnValues["Seniority Level"] = $readableSeniorityLevels;
			//echo "Seniority Level: ".implode(",",$readableSeniorityLevels);
		}else if($p[0] == "keywords"){
			$returnValues["Keyword"][] = $p[1];
			//echo "Keyword(s): ".$p[1]."; ";
		}else if($p[0] == "title"){
			$returnValues["Title"][] = $p[1];
			//echo "Title: ".$p[1]."; ";
		}else if($p[0] == "titleScope"){
			if($p[1] == "C"){
				$returnValues["Title"][] = "Current";
			}elseif($p[1] == "CP"){
				$returnValues["Title"][] = "Current or Past";
			}elseif($p[1] == "p"){
				$returnValues["Title"][] = "Past";
			}elseif($p[1] == "PNC"){
				$returnValues["Title"][] = "Past Not Current";
			}
		}else if($p[0] == "f_CS"){
			$formatSize = array();
			$sizes = explode(",",$p[1]);
			foreach($sizes as $size){
				if($size == 1){
					$formatSize[] = "1-10";
				}elseif($size == 2){
					$formatSize[] = "11-50";
				}elseif($size == 3){
					$formatSize[] = "51-200";
				}elseif($size == 4){
					$formatSize[] = "201-500";
				}elseif($size == 5){
					$formatSize[] = "501-1000";
				}elseif($size == 6){
					$formatSize[] = "1001-5000";
				}elseif($size == 7){
					$formatSize[] = "5001-10000";
				}elseif($size == 8){
					$formatSize[] = "10000+";
				}
				if($size == "A"){
					$formatSize[] = "Self-employed";
				}elseif($size == "B"){
					$formatSize[] = "1-10";
				}elseif($size == "C"){
					$formatSize[] = "11-50";
				}elseif($size == "D"){
					$formatSize[] = "51-200";
				}elseif($size == "E"){
					$formatSize[] = "201-500";
				}elseif($size == "F"){
					$formatSize[] = "501-1000";
				}elseif($size == "G"){
					$formatSize[] = "1001-5000";
				}elseif($size == "H"){
					$formatSize[] = "5001-10000";
				}elseif($size == "I"){
					$formatSize[] = "10000+";
				}
			}
			$returnValues["Company Size"] = $formatSize;
			//echo "Company sizes : ".implode(", ",$formatSize)."; ";
		}else if($p[0] == "f_F"){
			$formatSize = array();
			$sizes = explode(",",$p[1]);
			foreach($sizes as $size){
				if($size == 1){
					$formatSize[] = "50";
				}elseif($size == 2){
					$formatSize[] = "51-100";
				}elseif($size == 3){
					$formatSize[] = "101-250";
				}elseif($size == 4){
					$formatSize[] = "250-500";
				}elseif($size == 5){
					$formatSize[] = "501-1000";
				}
			}
			$returnValues["Fortune"] = $formatSize;
			//echo "Fortune : ".implode(", ",$formatSize)."; ";
		}else if($p[0] == "f_TE"){
			$yearsOfExperience = array();
			$years = explode(",",$p[1]);
			foreach($years as $year){
				if($year == 1){
					$yearsOfExperience[] = "Less than 1 year";
				}elseif($year == 2){
					$yearsOfExperience[] = "1 to 2 years";
				}elseif($year == 3){
					$yearsOfExperience[] = "3 to 5 years";
				}elseif($year == 4){
					$yearsOfExperience[] = "6 to 10 years";
				}elseif($year == 5){
					$yearsOfExperience[] = "More than 10 years";
				}
			}
			$returnValues["Years of Experience"] = $yearsOfExperience;
			//echo "Fortune : ".implode(", ",$formatSize)."; ";
		}else if($p[0] == "f_L"){
			$profilesLanguages = explode(",",$p[1]);
	
			$returnValues["Profile Language"] = array();
			foreach ($profilesLanguages as $profilesLanguage) {
				$returnValues["Profile Language"][] = $idToLanguage[$profilesLanguage];
			}
			
		}else if($p[0] == "postalCode"){
			
			$postalCode = $p[1];
			$returnValues["Postal Code"][] = $p[1];
			asort($returnValues["Postal Code"]);
			//echo "Postal Code: ".$p[1]."; ";
		}else if($p[0] == "f_G"){
			$locationCodes = explode(",",$p[1]);
			foreach ($locationCodes as $locationCode) {
				$locationQuery = DB::prep("select * from li_location where linked_in_id = ?");
				$locationQuery->execute(array(urldecode($locationCode)));

				if ($location = $locationQuery->fetch()) {
					$locations[] = $location["location_name"];
				} else if (isset($idToCountry[str_replace(":0","",$locationCode)])) {
					$locations[] = $idToCountry[str_replace(":0","",$locationCode)];
				} else {
					$locations[] = "Unknown Location (".urldecode($locationCode).")";
				}
			}
			$returnValues["Location"] = $locations;
			//echo "Location ID: ".$p[1]."; ";
		}else if($p[0] == "countryCode"){

			$countryCode = $p[1];
			$countries = explode(",",$p[1]);
			
			$returnValues["Country"] = array();
			foreach ($countries as $country) {
				$returnValues["Country"][] = $idToCountry[$country];
			}
			
			//echo "Country Code: ".$p[1]."; ";
		}else if($p[0] == "f_CC"){
			$returnValues["Current Company"] = explode(",",$p[1]);
		}else if($p[0] == "f_ED"){
			$values = explode(",",$p[1]);
			foreach ($values as $value) {
				$returnValues["School"][] = $value;
			}
			
		}else if($p[0] == "f_PC"){
			$returnValues["Past Company"] = explode(",",$p[1]);
			//echo "Current Company ID: ".$p[1]."; ";
		}else if($p[0] == "companyScope"){
			if($p[1] == "C"){
				$returnValues["Company"][] = "Current";
			}elseif($p[1] == "CP"){
				$returnValues["Company"][] =  "Current or Past";
			}elseif($p[1] == "p"){
				$returnValues["Company"][] = "Past";
			}elseif($p[1] == "PNC"){
				$returnValues["Company"][] =  "Past Not Current";
			}
		}else if($p[0] == "company" ){
			$returnValues["Company"][] = $p[1];
			//echo "Company: ".$p[1]."; ";
		}else if($p[0] == "f_FG"){
			$returnValues["Group"] = explode(",",$p[1]);
			//echo "Group ID: ".$p[1]."; ";
		}else if($p[0] == "firstName" ){
			$returnValues["First Name"][] = $p[1];
			//echo "First Name: ".$p[1]."; ";
		}else if($p[0] == "lastName" ){
			$returnValues["Last Name"][] = $p[1];
			//echo "Last Name: ".$p[1]."; ";
		}else if($p[0] == "school" ){
			$returnValues["School"] = explode(",",$p[1]);
			//echo "School: ".$p[1]."; ";
		}else if($p[0] == "distance" ){
			$returnValues["Postal Code"][] = "___".$p[1]." miles";
			asort($returnValues["Postal Code"]);
		}else if($p[0] == "pid" ){
			$returnValues["Connections of"][] = $p[1];
		}
	}
	if ($postalCode != "" and $countryCode != "") {
		$locationQuery = DB::prep("select * from postal_code where `postal code` = ? and `country code` LIKE ?");
		$locationQuery->execute(array(urldecode($postalCode),$countryCode));

		if ($postalCodeObject = $locationQuery->fetch()) {
			$returnValues["Postal Code"][] = "_".$postalCodeObject["place name"].", ".$postalCodeObject["admin name1"].", ".$returnValues["Country"][0];
			unset($returnValues["Country"]);
		}
	}
	
	if (!isset($returnValues["Relationship"])) {
		$returnValues["Relationship"] = array("All");
	} else if (count($returnValues["Relationship"]) == 4) {
		$returnValues["Relationship"] = array("All");
	}
	return $returnValues;

}

?>
