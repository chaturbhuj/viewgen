<?php

$IGNORE_AUTH = true;
$STOPMENU = true;

require_once('db.php');

require_once('shared_functions.php');

require_once("header.php");
require_once("header_index.php");

/*$redirect = base_url("dashboard.php");
if (isset($_GET['redirect'])) {
	$redirect = $_GET['redirect'];
}*/

$error_msg = "";
$msg = "";

if (isset($_GET["hash"]) && $_GET["hash"]) {
    $hash = $_GET["hash"];
    $query = DB::prep("select * from company where reset_password_hash = ?");
    $query->execute(array($hash));
    if ($query->rowCount() > 0) {
        $company = $query->fetch();
        if (strtotime($company['reset_password_date']) > strtotime('-3 day')) {
            if (isset($_POST["password"]) && $_POST["password"] != "") {
                if ($_POST["password"] == $_POST["confirm_password"]) {
                    if ($company['password_salt'] == "") {
                        $hash_password = md5($_POST["password"]."j567cdJKHL@@87g");
                        $query = DB::prep("UPDATE company SET reset_password_hash = null, reset_password_date = null, company_password = :pass WHERE id = :company_id");
                        $query->execute([
                            'pass' => $_POST["password"],
                            'company_id' => $company['id']
                        ]);
                    } else {
                        $hash_password = hash("sha256",$_POST["password"].$company['password_salt']);
                        $query = DB::prep("UPDATE company SET reset_password_hash = null, reset_password_date = null, password_hash = :pass WHERE id = :company_id");
                        $query->execute([
                            'pass' => $hash_password,
                            'company_id' => $company['id']
                        ]);
                    }
					$_SESSION['auth'] = array(
						'user_id' => 0,
						'company_id' => $company['id']
					);
                    header(sprintf('Location: %s',  base_url("dashboard.php")));
                    exit;
                } else {
                    $error_msg = "Wrong Confirm Password value";
                }
            }
        } else {
            $error_msg = "Link is expired";
        }
    } else {
        $error_msg = "Wrong Reset Password link";
    }
    if(isset($_POST["email"]) && $_POST["email"] != ""){
        // Login code
        $email = $_POST['email'];
        $query = DB::prep("select * from company where company_name=? or email = ?");
        $query->execute(array($email, $email));
        if ($query->rowCount() > 0) {
            $company = $query->fetch();
            if ($company['email']) {
                $hash = md5($company['company_name'] . time());
                $url = base_url('reset_password.php?hash=' . $hash);
                $query = DB::prep("UPDATE company SET reset_password_hash = :hash, reset_password_date = now() WHERE id = :company_id");
                $query->execute([
                    'hash' => $hash,
                    'company_id' => $company['id']
                ]);
                $emailTemplate = new EmailTemplate($company['id']);
                $param = [
                    'company name' => $company['company_name'],
                    'reset password url' => $url
                ];
                $emailTemplate->sendEmail($company['email'], "Reset Password", $param);
                $error_msg = $url;
                $msg = "Email sent.<br> Check your email.";
            } else {
                $error_msg = "Email not set";
            }
        } else {
            $error_msg = "Wrong Email or Account Name";
        }


    }
} else {
    $error_msg = "Wrong Reset Password link";
}

?>
<div class="segment sign_up_title">
	<div class="segment_inner">
		<h1>Set New Password</h1>
	</div>
</div>
<div class="segment sign_up_step_one">
	<div class="segment_inner">
        <span style="color:red;"><?=$error_msg;?></span>
			<?=$msg;?>
		<form  method="post">
		<input type="submit" class="removed" value="Save my Password">
		<div class="form_section login">
			<h2>Please enter new password</h2>
			<input type="password" name="password" placeholder="password"><br>
			<h2>Confirm new password</h2>
			<input type="password" name="confirm_password" placeholder="confirm password"><br>
			
			<a href="<?=base_url('reset_password.php')?>" class="button medium green" id="form_submit"  style="width: 240px;">Save my Password</a>
			
		</div>
		</form>
	</div>
</div>
<div class="segment bottom">
	<div class="segment_inner">
	</div>
</div>
<?php
require_once('footer.php');
?>
