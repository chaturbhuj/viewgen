/**
 * Created by mihael on 25.05.20.
 */
$(function(){
    $('button.get-email').on('click', function (event) {
        var people_id = this.dataset.people_id;
        var button = $(this);
        var old_html = button.html();
        button.html(old_html + ' <div class="loader"></div>');
        ajaxPostJSON('/new-ui/api/numeria.php', {action: 'get-email', data: {people_id: people_id}}, function(payload){
            if(payload == 'Limit reached'){
                alert('Limit reached');
                button.html(old_html);
                return;
            }
            button.parent().html('<b>'+payload+'</b>');
        })
    });

    $('button.get-all-emails').on('click', function (event) {
        var campaign_id = this.dataset.campaign_id;
        var visited_profiles = this.dataset.visited;
        var button = $(this);
        var old_html = button.html();
        button.html(old_html + ' <div class="loader"></div>');
        ajaxPostJSON('/new-ui/api/numeria.php', {action: 'get-all', data: {campaign_id: campaign_id, visited: visited_profiles}}, function(payload){
            if(payload && payload.status !== 'success'){
                if(payload.status == 'limit'){
                    if(confirm('Requsted amount of ' + payload.data.requested + ' emails is above of available ' + payload.data.limit + ' requests. Do you want proceed with available amount?')){
                        return ajaxPostJSON('/new-ui/api/numeria.php', {action: 'get-all', data: {campaign_id: campaign_id, ignore_limit: 1, visited: visited_profiles}}, function(payload){
                            if(payload && payload.status !== 'success'){
                                alert(payload.message);
                                button.html(old_html);
                                return;
                            }
                            location.reload();
                        })
                    }
                }else{
                    alert(payload.message);
                }
                button.html(old_html);
                return;
            }
            location.reload();
        })
    });
});
