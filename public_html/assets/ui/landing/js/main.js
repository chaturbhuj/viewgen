$(function(){	
	if ($(".s9-stories>div").length > 3){
		var btn = "<button class='s9-stories-btn'><img src='/assets/ui/landing/images/s9-btn.png'></button>";
		$(".s9-stories").slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 7000,
			prevArrow: btn,
			nextArrow: btn,
			responsive: [{
				breakpoint: 768,
				settings: {	slidesToShow: 1	}
			}]
		});
	}
	
	$(window).on("load resize", function(){
		$("header").css("height", $("header>nav").outerHeight());
	});
	
	$("a[href^='#scr']").on("click", function(e){
		e.preventDefault();		
		var link = $(this), target = $(link.attr("href")),
			headerHeight = $("header>nav").outerHeight(),
			targetPadding = parseInt(target.css("padding-top")) - 20,
			top = target.offset().top - headerHeight + targetPadding;
		$("html, body").stop().animate({scrollTop: top}, 800);
	});	
	$(window).on("scroll", function(){
		$("[id^='scr']").each(function(i,el){
			var top  = $(el).offset().top - 120,
				bottom = top + $(el).height(),
				scroll = $(window).scrollTop(),
				id = $(el).attr('id');
			if (scroll > top && scroll < bottom){
				$(".header-menu .active").removeClass("active");
				if($(window).scrollTop() + $(window).height() > $(document).height() - 100){
					$(".header-menu a[href='#scr5']").parent().addClass("active");
				} else {
					$(".header-menu a[href='#"+id+"']").parent().addClass("active");
				}
			}
		});
	});

    $("form.contact_form").submit(function (event) {
        event.preventDefault();
        var $form = $(this);
        var $submitBtn = $(this).find(".submit_contact");
        $submitBtn.prop('disabled', true);
        $.ajax({
            url: '/ui/api/catch_email_captcha.public.php',
            type: 'POST',
            data: $form.serialize(),
            success: function (response) {
                $submitBtn.remove();
                $form.append("<h2 class='contact-response'>" + response.payload + "</h2>");
            }
        });
        return false;
    });
    
    // $form.validate({
    //     submitHandler: function(form) {
    //         $submitBtn.prop('disabled', true);
    //         $.ajax({
    //             url: '/ui/api/catch_email_captcha.php',
    //             type: 'POST',
    //             data: jQuery(form).serialize(),
    //             success: function(response) {
    //                 $submitBtn.remove();
    //                 $form.append("<h2 style='text-align:center;'>" + response.payload + "</h2>");
    //             }
    //         });
    //         return false;
    //     }		
    // });

    function setupVideo() {

        var video = $('.video-popup video');

        setupMediaAutoSize(video[0], 40);
        
        // video.height(h).width(w);
        
        $('.play-main-video').on('click', function (event) {
            $('.video-popup').show();
            video[0].play();
        });
        $('.video-popup .close-video').on('click', function (event) {
            $('.video-popup').hide();
            video[0].pause();
        })
    }
    
    setupVideo();
    
    
});