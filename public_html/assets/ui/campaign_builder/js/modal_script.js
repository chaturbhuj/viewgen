//**************************************************************************************************************


function CampaignBuilderDialogBase(companyId, userId){
    
    var self = this;
    var currentTabLabel = 'Title';
    
    self.companyId = companyId;
    self.userId = userId;
    
    self.campaignModalIsDataPresent = false;
    
    var $modal = self.$modal = $('#buildCampaignModal');

    self.$errors = self.$modal.find('.modal-errors');
    self.$warnings = self.$modal.find('.modal-warnings');
    self.$info = self.$modal.find('.modal-info');
    
    self.$modal.on('shown.bs.modal', function () {
      self.refresh();
    });
    
    self.$modal.find('ul.nav a').click(function (event) {
        event.preventDefault();
        $(this).tab('show');
        var cbp = $('.campaign-builder-parameters');
        var cbu = $('.campaign-builder-urls');
        if ($(this).hasClass('builder-nav-parameter')) {
            cbp.show();
            cbu.hide();
        } else {
            cbp.show();
            cbu.hide();
            $('.campaign-builder-parameters').hide();
            $('.campaign-builder-urls').show();
        }
    });

    var boxes = self.boxes = {};
    
	boxes.geographyBox = new LocationBox(
        $('input[data-action="fetch-locations"]').parent().parent(), 
        "geography"
    );

    //TODO may be in the future version
    // if ($('input[data-action="fetch-industries"]').length > 0) {
    // 	var $box = $('input[data-action="fetch-industries"]').parent().parent();
    // 	this.box = new LocationBox($box, "industry", '/new-ui/ajax_get_industries.php');
    // }
	
    self.$modal.find('.campaign-builder-parameter-box .more-arrow').on('click', 
        self.handleParameterBoxClick.bind(self)
    );

    $('#saveNewCampaign').click(self.handleSaveCampaign.bind(self));
    
    self.setupURLValidation();
    
    self.setupManualURLToggle();
    
	$( 'body' ).on( 'click', 'nav.tab_fields div.tab', function(){
		
		currentTabLabel = $( this ).attr( 'tab-for' );
		$( 'nav.tab_fields div.tab' ).removeClass( 'showing' );
		$( this ).addClass( 'showing' );
		$( 'div.tab_field.showing' ).addClass( 'hiding' );
		$( 'div.tab_field.showing' ).removeClass( 'showing' );
		$( 'div.tab_field[tab-label="'+currentTabLabel+'"]' ).addClass( 'showing' );
		$( 'div.tab_field[tab-label="'+currentTabLabel+'"]' ).removeClass( 'hiding' );
		
        self.renderSummaryTree(null);
		
	});
	
	self.buildTabFields();

    self.setDefaultData(); //tree render should go after this

    self._renderSummaryTreeListeners = [];
    
    self.renderSummaryTree();
    
    self.$modal.find('.hideTitleTips').on('click', function(){
        $modal.addClass('hide-tips')
    });
    $('#showTitleTips').on('click', function(){
        $modal.removeClass('hide-tips')
    });
    
    $('.modalCampaignBuilderClear').on('click', function(){
        self.clearIssues();
        self.cleanManualURLs();
        $modal.find('input[type="text"]').val('');
        $modal.find('textarea').val('');
        $modal.find('input[type="checkbox"]').prop('checked', false);
        self.boxes.geographyBox.clear();
        self.renderSummaryTree(null);
    });
    
    self.$modal.find('input.cb-header[data-group]').on('click', function(){
        var $headerCheckbox = $(this);
        self.$modal.find('input.cb-leaf[data-group="' + $headerCheckbox.data('group') + '"]').prop('checked', $headerCheckbox.prop('checked'));
    });

    self.$modal.find('input.cb-leaf[data-group]').on('click', function(){
        var leafCheckbox = $(this);
        self.$modal.find('input.cb-header[data-group="' + leafCheckbox.data('group') + '"]').prop('checked', false);
    });

    $(document).on('new-ui:trigger_tree', function (event) {
        self.renderSummaryTree(null);
    });

    self.regTreeNodes();
    
    //TODO: when_campaigns_query_ready
    // ajaxGet('/new-ui/api/title_queries.php', {
    //      action: 'campaigns',
    //      companyId: URL_PARAMS.companyId,
    // }, function(payload){
    //     var group = $('#myCampaignsOptgroup');
    //     _.each(payload, function(campaign){
    //         $('<option />')
    //             .html(campaign.name).val(campaign.id).appendTo(group);
    //     });
    //    
    // });
    
    //-----------------------------------------------------------------
    var $titleList = $('#campaign_title_tool_list');
    var $titleListTag = $titleList.find('ul');
    var $titleListLoading = $titleList.find('.loading');

    var STOP_WORD_TITLES = /(Please try again|Sorry)/ig;
    
    function titlesListCB(payload, isAppend){
        var $frag;
        var items = _(payload).filter(function (title) {
            //filter numbers
            return isNaN(Number(title))
        })
        .filter(function(title){
            return ! STOP_WORD_TITLES.test(title);
        })
        .map(htmlDecodeDropTags)
        .value();
        
        if (items && items.length){
            $frag = $(document.createDocumentFragment());
            _.each(items, function(item){
                $('<li></li>').text(item).appendTo($frag);
            });
            if (! isAppend) $titleListTag.html('');
            $titleListTag.append($frag)
        }
        $titleListLoading.hide();
    }
    
    //populate initial top 10 titles on the Titles List
    makeTitleQuery('titles', null, titlesListCB);

    var $search = $('#campaign_builder_title_tool_searchSimilar');
    
    //TODO TODO_title_select 
    //var $titleSelect = $('#campaign_builder_title_tool_select');
    
    $search.on('keyup change', function (event) {
        if (event.which in {13:0, 32:0, 9:0} || 
            event.type in {blur:0, change:0}
        ) {
            $titleListTag.html('');
            $titleListLoading.show();
            makeTitleQuery('titles' /*$titleSelect.val() TODO_title_select*/, 
                $search.val(), titlesListCB);
        }
    });

    //TODO TODO_title_select 
    // $titleSelect.on('change', function (event) {
    //     $titleListTag.html('');
    //     $titleListLoading.show();
    //     makeTitleQuery($titleSelect.val(), $search.val(), titlesListCB);
    // });
    
    setupTitlesListScroll($titleList, function(cbb){
        $titleListLoading.show();
        makeTitleQuery('titles' /*$titleSelect.val() TODO_title_select */, $search.val(), function(payload, isAppend /*=== true*/){
            titlesListCB(payload, true);
            cbb();
        }, true);
    });
    
    $('input, textarea').on('keyup', function () {
        self.clearIssues();
    });
    
    $('input[type="checkbox"], input[type="radio"], select').on('mouseup, keyup', function () {
        self.clearIssues();
    });
    
    self.setupCampaignPreview();
}

CampaignBuilderDialogBase.prototype.isLISearchURL = function (url) {
    throw Error("not implemented");
};

CampaignBuilderDialogBase.prototype.isAnyLISearchURL = function (url) {
    //TODO add Recruiter when ready
    return isLinkedInSearchUrl(url) || isLinkedInSimpleSearchUrl(url);
};

CampaignBuilderDialogBase.prototype.setupManualURLToggle = function () {
    var self = this;
    var $urlToggle = self.$modal.find(".campaign-builder-url-toggle");
    $urlToggle.click(function () {
        var $navParameter = self.$modal.find('.nav-link.builder-nav-parameter');
        if ($navParameter.hasClass('active')) {
            $navParameter.removeClass('active');
            $urlToggle.find('.manual').hide();
            $urlToggle.find('.back').show();
        } else {
            $navParameter.addClass('active');
            $urlToggle.find('.manual').show();
            $urlToggle.find('.back').hide();
        }
        self.$modal.find("div.campaign-builder-parameters").toggle();
        self.$modal.find("div.campaign-builder-urls").toggle();
    });

};

CampaignBuilderDialogBase.prototype.setupURLValidation = function () {
    var self = this;
    
    self.manualUrls = {};
    self.manualUrls.$urlAdd = self.$modal.find('.campaign-builder-urls .query-enter-url');
    self.manualUrls.$urlList = self.$modal.find('.campaign-builder-urls .query-urls-list');
    self.manualUrls.$urlTextarea = self.$modal.find('.campaign-builder-urls .query-urls');

    self.manualUrls.validUrls = [];

    /*$('#add_li_url').click(*/
    self.manualUrls.$urlAdd.on('change keyup', function (event) {
        self.clearIssues();
        event.preventDefault();
        event.stopImmediatePropagation(); //skip other event handlers
        var urlPack = self.manualUrls.$urlAdd.val().trim();
        if (urlPack === "") return;
        
        var urls = getAllUrlsFromString(urlPack);
        var hasLIUrls = false;
        var numberOfAdded = 0;
        var duplications = [];
        var alienUrls = [];
        var notSupportedUrls = [];
        var savedSearchURLs = [];
        
        if (urls && urls.length > 0) {
            _.each(urls, function (url, index) {
                if (! self.isLISearchURL(url)) {
                    if (self.isAnyLISearchURL(url)){
                        hasLIUrls = true;
                        notSupportedUrls.push(index + 1);
                    }else{
                        alienUrls.push(index + 1);
                    }
                    return true;
                }
                
                hasLIUrls = true; //duplication also counted to not show "no LI urls" error

                if (isSavedSearchLinkedInUrl(url)) {
                    savedSearchURLs.push(index + 1);
                    return true;
                }
                
                //check already added duplications
                var existing = self.manualUrls.validUrls.indexOf(url);
                if (existing !== -1) {
                    self.addWarningMarker(self.manualUrls.$urlList.find('a[href="' + url + '"]'));
                    duplications.push(index + 1);
                    return true;
                } 

                //now this is a real LI Sales Nav url
                numberOfAdded++;
                var len = self.manualUrls.validUrls.push(url);
                self.manualUrls.$urlList.prepend(
                    '<div class="row">' +
                        '<div class="number col-1"><button type="button" class="close close-added-url" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span></button></div>' +
                        '<div class="col-11"><a href="' + url + '" target="_blank" class="added-url-item">' + url + '</a><hr/></div>' +
                    '</div>'
                );

            });
        }
        if (! hasLIUrls){
            self.addError('There are no valid LinkedIn search URLs in your input.');
        }
        
        if(duplications.length){
            self.addWarning("URLs # " + duplications.join(', ') + " is/are duplications of already added ones (highlighted below). Skipped.");
        }
        
        if (alienUrls.length){
            self.addError("URLs # " + alienUrls.join(', ') + 
                " is/are not valid LinkedIn search URLs");
        }
        
        if (notSupportedUrls.length){
            self.addError("URLs # " + notSupportedUrls.join(', ') + 
                " is/are not supported in this LinkedIn account (e.g. Sales Navigator URLs are not supported under Premium account). Try to take a correct URL or switch to a proper LinkedIn account.");
        }
        
        if (savedSearchURLs.length){
            self.addError("URLs # " + savedSearchURLs.join(', ') + 
                " is/are Saved Search results rom LinkedIn. We do not support Saved Search URLs, please recreate it as straight search URL");
        }
        
        if (numberOfAdded){
            self.addInfo(numberOfAdded + ' urls added.');
        }
        
        if (! self.hasIssues){
            //clean input only if no issues
            self.manualUrls.$urlAdd.val('');
        }
        
        self.manualUrls.$urlTextarea.val(self.manualUrls.validUrls.join('\n'));
        
        if (self.manualUrls.validUrls.length){
            //show hidden Added box
            $('.show-on-added-urls').slideDown();
        }
    });
    
    self.manualUrls.$urlList.on('click', 'button.close', function(){
        self.clearIssues();
        var $t = $(this);
        var $parent = $t.parent().parent();
        var url = $parent.find('a')[0].href;
        var index = self.manualUrls.validUrls.indexOf(url);
        self.manualUrls.validUrls.splice(index, 1);
        self.manualUrls.$urlTextarea.val(self.manualUrls.validUrls.join('\n'));


        $parent.remove();
    });
    
    $('.clear-query-enter-url').on('click', function(event){
        self.clearIssues();
        self.manualUrls.$urlAdd.val('');
    });

    // $('.clear-added-urls').on('click', function(event){
    //     self.clearIssues();
    //     self.manualUrls.validUrls = [];
    //     self.manualUrls.$urlTextarea.val();
    //     self.manualUrls.$urlList.html('');
    // });

};

CampaignBuilderDialogBase.prototype.cleanManualURLs = function () {
    var self = this;
    self.manualUrls.$urlAdd.val('');
    self.manualUrls.validUrls = [];
    self.manualUrls.$urlTextarea.val();
    self.manualUrls.$urlList.html('');
    //TODO single_url_logic
    self.manualUrls.$urlAdd.prop('disabled', false);

};


CampaignBuilderDialogBase.prototype.clearIssues = function () {
    var self = this;
    self.hasErrors = false;
    self.hasWarnings = false;
    self.hasIssues = false;
    self.$errors.html('');
    self.$errors.hide();
    self.$warnings.html('');
    self.$warnings.hide();
    self.$info.html('');
    self.$info.hide();
    self.$modal.find('.redErrorMarker').removeClass('redErrorMarker');
    self.$modal.find('.theWarningMarker').removeClass('theWarningMarker');
};

CampaignBuilderDialogBase.prototype.addWarningMarker = function ($elementToMark) {
    $elementToMark.addClass('theWarningMarker');
};

CampaignBuilderDialogBase.prototype.addErrorMarker = function ($elementToMark) {
    $elementToMark.addClass('redErrorMarker');
};

CampaignBuilderDialogBase.prototype.addInfo = function (text) {
    var self = this;
    self.$info.show();
    $('<p></p>').text(text).appendTo(self.$info);
};

CampaignBuilderDialogBase.prototype.addWarning = function (text, $elementToMark) {
    var self = this;
    self.hasWarnings = true;
    self.hasIssues = true;
    self.$warnings.show();
    $('<p></p>').text('Warning: ' + text).appendTo(self.$warnings);
    if ($elementToMark) $elementToMark.addClass('theWarningMarker');
};

CampaignBuilderDialogBase.prototype.addError = function (text, $elementToMark) {
    var self = this;
    self.hasErrors = true;
    self.hasIssues = true;
    self.$errors.show();
    $('<p></p>').text('Error: ' + text).appendTo(self.$errors);
    if ($elementToMark) $elementToMark.addClass('redErrorMarker');
};


CampaignBuilderDialogBase.prototype.refresh = function () {
    this.setDefaultData();
    this.renderSummaryTree(null);
};

	
CampaignBuilderDialogBase.prototype.setDefaultData = function(){
    if(!useDefaultNewCompaignbuilderData && 'undefined' !== defaultCompaignbuilderData){
        var that = this;
        defaultCompaignbuilderData.geography.forEach(function(item){
            that.boxes.geographyBox.addRawLocation(item);
        });

        defaultCompaignbuilderData.relation.forEach(function(item){
            $('input[name="relation['+item+']"]').prop('checked', true);
        });

        $('textarea[name="titles"]').prop('value', defaultCompaignbuilderData.titles.join('\n'));
        $('textarea[name="skipTitles"]').prop('value', defaultCompaignbuilderData.skipTitles.join('\n'));

        $('textarea[name="keywords"]').prop('value', defaultCompaignbuilderData.keywords.join('\n'));
        $('textarea[name="skipKeywords"]').prop('value', defaultCompaignbuilderData.skipKeywords.join('\n'));

        defaultCompaignbuilderData.industry_location.forEach(function(item){
            $('input[name="industry_location['+item+']"]').prop('checked', true);
        });

        $('textarea[name="companies"]').prop('value', defaultCompaignbuilderData.companies.join('\n'));

        defaultCompaignbuilderData.companySize.forEach(function(item){
            $('input[name="companySize['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.function.forEach(function(item){
            $('input[name="function['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.seniorityLevel.forEach(function(item){
            $('input[name="seniorityLevel['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.yearsInCurrentPosition.forEach(function(item){
            $('input[name="yearsInCurrentPosition['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.yearsAtCurrentCompany.forEach(function(item){
            $('input[name="yearsAtCurrentCompany['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.yearsOfExperience.forEach(function(item){
            $('input[name="yearsOfExperience['+item+']"]').prop('checked', true);
        });

        defaultCompaignbuilderData.groups.forEach(function(item){
            $('input[name="groups['+item+']"]').prop('checked', true);
        });

        $('textarea[name="firstName"]').prop('value', defaultCompaignbuilderData.firstName.join('\n'));
        $('textarea[name="lastName"]').prop('value', defaultCompaignbuilderData.lastName.join('\n'));

        $('select[name="postalCodeWithin"]').prop('value', defaultCompaignbuilderData.postalCodeWithin);
        $('select[name="postalCodeCountry"]').prop('value', defaultCompaignbuilderData.postalCodeCountry);
        $('input[name="postalCode"]').prop('value', defaultCompaignbuilderData.postalCode);
        $('#mainItcField').prop('checked', (defaultCompaignbuilderData.itc > 0 ? 1 : 0));
        $('#mainItcMessageField').prop('value', (defaultCompaignbuilderData.itc_message));
        if($('#counter').length) {
            $('#counter').text($('#mainItcMessageField').attr('maxlength') - $('#mainItcMessageField').val().length)
        }
    } else {
        // this.boxes.geographyBox.addRawLocation({
        //     n: 'United States',
        //     id : 'us:0'
        // });
        $('input[name="relation[S]"]').prop('checked', true);
        $('input[name="relation[A]"]').prop('checked', true);
        $('#mainItcMessageField').prop('value', 'Hello');
        if($('#counter').length) { 
            $('#counter').text($('#mainItcMessageField').attr('maxlength') - $('#mainItcMessageField').val().length)
        }
    }


};

CampaignBuilderDialogBase.prototype.handleSaveCampaign = function (event) {
    var self = this;
    //var $button = $(event.currentTarget);
    // Check the urls.
    //$button.button('loading');
    var $form;
    self.clearIssues();

    if (!self.campaignModalIsDataPresent) {
        self.addError("You need to fill at least one parameter");
    }

    var $campaignName = $('#mainCampaignNameField'),
        campaignName = $campaignName.val().trim();

    var $itc = $('#mainItcField'),
        itc = $itc.length ? $itc.is(':checked') : false;
    var $itc_message = $('#mainItcMessageField'),
        itc_message = $itc_message.length ? $itc_message.val().trim() : '';

    if (itc && itc_message == "") {
        self.addError("You need to specify a invite message", $itc_message);
    }

    if (campaignName === "") {
        self.addError("You need to specify a campaign name", $campaignName);
    }

    if (self.hasErrors) return;

    if ($('.nav-link.builder-nav-parameter').hasClass('active')) {
        if(!confirm('Have you entered all of the search parameters you want to use? Once Active, the campaign parameters cannot be modified. Click Ok to start campaign or Cancel to continue editing.')){
            return;
        }

        $form = $('#autoCampaignForm');
        //TODO: temporary hack: copy this field into the hidden field inside the postable form
        $form.find('input[name="campaign_name"]').val(campaignName);
        $form.find('input[name="itc"]').val(itc ? 1 : 0);
        $form.find('input[name="itc_message"]').val(itc_message);
        $form.submit();
    } else {
        $form = $('#queryCampaignForm');
        //TODO: temporary hack: copy this field into the hidden field inside the postable form
        $form.find('input[name="campaign_name"]').val(campaignName);
        $form.find('input[name="itc"]').val(itc ? 1 : 0);
        $form.find('input[name="itc_message"]').val(itc_message);

        $.ajax({
            'url': '/new-ui/ajax.php?company_id=' + self.companyId + '&user_id=' + self.userId,
            'type': 'post',
            'data': {'action': 'checkUrls', 'query_urls': self.$modal.find('*[name="query_urls"]').val()},
            'success': function (response) {
                //$button.reset();
                if (response === "") {
                    $form.submit();
                } else {
                    self.addError(response);
                }
            }
        });
    }
};

CampaignBuilderDialogBase.prototype.setupCampaignPreview = function (event) {
    var self = this;
    var didTick = false;
    
    $('#show-assembled-url').on('click', function (event) {
        var url = $('#show-assembled-url').data('url');
        
        if (!self.campaignModalIsDataPresent) {
            self.addError("You need to fill at least one parameter");
        }
    
        if (! url) {
            self.addError("Campaign URL is not generated yet, check other errors or wait few seconds");
        }
    
        if (self.hasErrors) {
            self.addWarning("You have errors, please clean up first");
            return false;
        }
        
        var win = window.open(url, "blank");
        win.focus();
    });

    /*asyncRecurrentSequence*/ self._renderSummaryTreeListeners.push(function(tickCallback) {
        if (!self.campaignModalIsDataPresent || self.hasErrors) {
            return;
        }

        var $form = $('#autoCampaignForm');

        $form.find('input[name="campaign_name"]').val('__GET_URL'); //should not apply

        ajaxPostForm($form, {get_url: true}, {
            success: function (payload) {
                if (payload && payload.url) {
                    didTick = true;
                    $('#assembled-url')
                        .html(escapeHTML(payload.url).replace(/(&amp;\w+|&#x3D;|=)/, '<strong>$1</strong>'))
                        .fadeOut(200, function(){$(this).fadeIn(200, function(){})});
                    $('#show-assembled-url').data('url', payload.url);
                    $('#show-assembled-url').show(); //show it first time
                }
            },
            timeout: 3000,
            //complete: tickCallback
        });
    }); //, 5000, false);
    
};

CampaignBuilderDialogBase.prototype.handleParameterBoxClick = function (event) {
    var $box = $(event.currentTarget);

    if ($box.data('expanded')) {
        $box.parent().css({'max-height': '200px'});
        $box.html('more');
        $box.data('expanded', 0);
    } else {
        $box.parent().css({'max-height': 'none'});
        $box.html('less');
        $box.data('expanded', 1);
    }
};

//**************************************************************************************************************


function setupTitlesListScroll($titleList, callback){
    var $ul = $titleList.find('ul');
    var lock = false;
    $titleList.on('scroll', function() {
        // End of the document reached?
        if (lock) return;
        if ($ul[0].clientHeight - $titleList[0].clientHeight + 40 <= $titleList.scrollTop()) {
            lock = true;
            callback(function(){
                lock = false;
            });
        }
    });
}

function makeTitleQuery(option, search, callback, isAppend) {
    var OFFSET_STEP = 30;
    
    var endpoint, params = {
        limit: OFFSET_STEP,
        search: search || null,
        linked_in_id: URL_PARAMS.linked_in_id //TODO is it?
    };
    if(option in {titles:0, 'titles-company':0, 'titles-linkedin':0}) {
        endpoint = option;
    }else if (! isNaN(Number(option))){
        endpoint = 'titles-campaign';
        params.campaign_id = option;
    }else{
        console.error("makeTitleQuery: Invalid option: ", option);
        return;
    }
    
    if (isAppend){
        params.offset = makeTitleQuery.currentOffset;
    }
    params.action = endpoint;
    ajaxGet('/new-ui/api/title_queries.php', params, 
        function (payload) {
            makeTitleQuery.currentOffset += OFFSET_STEP;
            callback(payload, isAppend);
        }
    );
}

makeTitleQuery.currentOffset = 0;

CampaignBuilderDialogBase.prototype.generateSummaryTreeWrapper = function(){
    var self = this;
    var data = self.generateSummaryTree();
    $('.tab[tab-for]').each(function(idx){
        var name = this.attributes['tab-for'].value;
        var datum = _.find(data, {name: name});
        $(this).find('.modal_menu_badge').remove();
        if (datum) {
            $(this).addClass('has-data');
            $(this).append('<span class="modal_menu_badge">' + datum._totalEntries + '</span>')
        }else{
            $(this).removeClass('has-data');
        }
    });
    //the functionality below hides whole the tree, if there is no members for currently selected item
    //
    // var body = _.find(data, {name: currentTabLabel});
    // if (! body || ! body.children || _.filter(body.children, 
    //         function(item){ return item.children && item.children.length > 0;}).length === 0){
    //     return []; //empty the tree
    // }
    return data;
};

CampaignBuilderDialogBase.prototype.renderSummaryTree = function(id){
    var self = this;
    var $tree = $('#campaignSummaryTree');

    var data = self.generateSummaryTree();

    $('.tab[tab-for]').each(function(idx){
        var name = this.attributes['tab-for'].value;
        var datum = _.find(data, {name: name});
        $(this).find('.modal_menu_badge').remove();
        if (datum) {
            $(this).addClass('has-data');
            $(this).append('<span class="modal_menu_badge">' + datum._totalEntries + '</span>')
        }else{
            $(this).removeClass('has-data');
        }
    });
    //the functionality below hides whole the tree, if there is no members for currently selected item
    //
    // var body = _.find(data, {name: currentTabLabel});
    // if (! body || ! body.children || _.filter(body.children, 
    //         function(item){ return item.children && item.children.length > 0;}).length === 0){
    //     return []; //empty the tree
    // }

    for (var i in self._renderSummaryTreeListeners){
        self._renderSummaryTreeListeners[i]();
    }
    
    if (! self._summaryTreeRendered){
        $tree.tree({
            data:data,
            closedIcon: ' ',
            openedIcon: ' ',
            selectable: false,
            autoOpen: true
        });
        self._summaryTreeRendered = true;
    }else{
        $tree.tree('loadData', data);
        if (id){
            var node = $tree.tree('getNodeById', id);
            $tree.tree('openNode', node);
        }
    }
};

CampaignBuilderDialogBase.prototype.buildTabFields = function(){
    var self = this;
    var $fields = self.$modal.find( 'div.tab_field' );
	var n_fields = $fields.length;
	var tab_fields = '';
	if( n_fields > 1 ){
		var i = 0;
		$fields.each( function(){
			var extraClass = '';
			if( i === 0 )
				extraClass = 'showing';
			$( this ).addClass( extraClass );
			tab_fields += '<div class="tab '+extraClass+'" tab-for="'+$( this ).attr( 'tab-label' )+'">'+$( this ).attr( 'tab-label' )+'</div>';
			i++;
		});
        self.$modal.find( 'div.campaign_modal_tabs' ).prepend( '<nav class="tab_fields">'+tab_fields+'</nav>' );
	}

};


CampaignBuilderDialogBase.prototype.getTextareaValues = function(selectorOrElement, end){
    var val = $(selectorOrElement).val();
    if (end) val = val.slice(0, end);
    if (! _.trim(val)) return [];
    var rawData = val.split(/[\s\n]*\n[\s\n]*/);
    return _.reduce(rawData, function(acc, entry){
        entry = _.trim(entry);
        if (entry !== '') acc.push(entry);
        return acc;
    }, []);
};


CampaignBuilderDialogBase.prototype.getItems = function() {
    throw Error('not implemented');
};


CampaignBuilderDialogBase.prototype.getCheckedLabels = function(baseSelector, checkboxSubSelector, labelSubSelector){
    checkboxSubSelector = checkboxSubSelector === 'std' ? 
        'input[type="checkbox"]' : checkboxSubSelector;
    var label, labels = [];
    $(baseSelector).each(function(){
        var t = $(this);
        if (t.find(checkboxSubSelector)[0].checked){
            label = (
                labelSubSelector ? 
                    t.find(labelSubSelector) :
                    t //get the inner text from this root element
            ).text().trim();
            if (label){
                labels.push(label);
            }
        }
    });
    return labels;
};


CampaignBuilderDialogBase.prototype.getSingleValue = function(selector){
    var val = $(selector).val().trim();
    return val ? [val] : [];
};


CampaignBuilderDialogBase.prototype.generateSummaryTree = function(){
    var self = this;

    function addItem(tree, name, values) {
        if (_.isPlainObject(values)){
            return _addCompoundItem(true, tree, name, values.children);
        }
        return _addCompoundItem(false, tree, name, values);
    }
    
    function _addCompoundItem(isCompound, tree, name, children){
        if (!children || children.length === 0) return 0;
        var id = name.trim().replace(/\s/g, '_'),
            item, cName, cId, leafChildren, totalEntries = 0;
        var itemChildren = [];
        if (isCompound) {
            for (var i in children) {
                leafChildren = children[i][1];
                if (!leafChildren || leafChildren.length === 0) continue;
                cName = children[i][0];
                cId = id + '--' + cName.replace(/\s/g, '_');
                totalEntries += leafChildren.length;
                _addSingleItem(itemChildren, cName, cId, leafChildren)
            }
        }else{
            itemChildren = children;
            totalEntries = children.length;
        }
        if (totalEntries > 0){
            item = _addSingleItem(tree, name, id, itemChildren);
            item._totalEntries = totalEntries;
        }
        return totalEntries;
    }

    function _addSingleItem(parent, name, id, children){
        var item = {
            name: name,
            children: children || []
        };
        if (id) item.id = id;
        parent.push(item);
        return item;
    }
    
    
    //----------------------------------------------------------------------------------------------------------
    var tree = [], total = 0;

    var items = self.getItems();
    for (var i in items){
        var data = items[i].data;
        if (data) total += addItem(tree, data[0], data[1]);
    }

    self.campaignModalIsDataPresent = total > 0;
    
    return tree;
};


CampaignBuilderDialogBase.prototype.regTreeNodes = function(){
    var self = this;
    function common(element, event){
        if (event.data.subHandler) event.data.subHandler.call(element);
        self.renderSummaryTree(event.data.id);
    }
    
    var treeChangeHandlers = {
        bykey: [
            function (event){
                if (event.which in {13:0, 32:0, 9:0} || event.type === 'blur') {
                    common(this, event);
                }
            },
            'keyup blur'
        ],    
        change: [
            function (event){
                common(this, event);
            },
            'change'
        ],    
    };
    
    var subHandlers = {
        triggerSearch: function (){
            var wordsTillTheCursor = self.getTextareaValues(this, getInputSelection(this).start);
            $search.val(wordsTillTheCursor[wordsTillTheCursor.length - 1]);
            $search.trigger("change");
        },
        triggerClearRelationship: function (){
            $('input[name^="relation"]').prop('checked', false);
        }
    };
    
    function regTreeNode(selector, handlerName, id, subHandlerName){
        var handler = treeChangeHandlers[handlerName];
        var isComplexSelector = _.isArray(selector);
        $(isComplexSelector ? selector[0] : selector)
            .on(handler[1], //events
                isComplexSelector ? selector[1] : null, 
                {id: id,
                subHandler: subHandlers[subHandlerName]}, 
                handler[0] //handler function
            );
    }
    
    var $search = $('#campaign_builder_title_tool_searchSimilar');
    
    var items = self.getItems();
    for (var i in items){
        var nodes = items[i].nodes;
        if (nodes) {
            for (var n in nodes) if (nodes[n]) regTreeNode.apply(self, nodes[n]);
        }
    }
};

//**************************************************************************************************************************
//**************************************************************************************************************************

var FullCampaignBuilderDialog = inherit(CampaignBuilderDialogBase, function(companyId, userId){
    this.__super(companyId, userId);
});

FullCampaignBuilderDialog.prototype.isLISearchURL = function (url) {
    //Sales Nav accounts recognizes Premium/Free URL formats as well
    return isLinkedInSearchUrl(url); //.. TODO: || isLinkedInSimpleSearchUrl(url);
    //TODO to make this work Premium URLs in Sales Nav should be handled on API side as well 
};

FullCampaignBuilderDialog.prototype.getItems = function () {
    var self = this;

    return [
        {
            data: ['Title', {
                children: [
                    ['VISIT', self.getTextareaValues('#campaign_field_TitlesToVisit')],
                    ['SKIP', self.getTextareaValues('#campaign_field_TitlesToSkip')]
                ]
            }], nodes: [['#campaign_field_TitlesToVisit', 'bykey', 'Title--VISIT', 'triggerSearch'],
            ['#campaign_field_TitlesToSkip', 'bykey', 'Title--SKIP', 'triggerSearch']]
        },

        {
            data: ['Keywords', {
                children: [
                    ['VISIT', self.getTextareaValues('#campaign_field_KeywordsToVisit')],
                    ['SKIP', self.getTextareaValues('#campaign_field_KeywordsToSkip')]
                ]
            }], nodes: [['#campaign_field_KeywordsToVisit', 'bykey', 'Keywords--VISIT'],
            ['#campaign_field_KeywordsToSkip', 'bykey', 'Keywords--SKIP']]
        },

        {
            data: ['Geography',
                self.boxes.geographyBox.getTitles().concat(self.getCheckedLabels(
                    '#campaign_field_Geography>label.leaf',
                    'std',
                    '.label-name'
                ))
            ], nodes: [['#campaign_field_Geography>label>input', 'change', 'Geography']]
        }, //this not working for geography, see 'new-ui:trigger_tree'

        {
            data: ['Relationship', self.getCheckedLabels(
                '#campaign_field_Relationship>label',
                'std',
                null
            )], nodes: [['#campaign_field_Relationship>label>input', 'change', 'Relationship']]
        },
        {
            data: ['Industry', self.getCheckedLabels(
                '#campaign_field_Industry>label.leaf',
                'std',
                '.label-name'
            )], nodes: [['#campaign_field_Industry>label>input', 'change', 'Industry']]
        },

        {
            data: ['Company', self.getTextareaValues('#campaign_field_Company')],
            nodes: [['#campaign_field_Company', 'bykey', 'Company']]
        },

        {
            data: ['Company Size', self.getCheckedLabels(
                '#campaign_field_CompanySize>label',
                'std',
                null
            )], nodes: [['#campaign_field_Relationship>label>input', 'change', 'Relationship']]
        },

        {
            data: ['Function', self.getCheckedLabels(
                '#campaign_field_Function>label',
                'std',
                null
            )], nodes: [['#campaign_field_Function>label>input', 'change', 'Function']]
        },

        {
            data: ['Seniority Level', self.getCheckedLabels(
                '#campaign_field_SeniorityLevel>label',
                'std',
                null
            )], nodes: [['#campaign_field_SeniorityLevel>label>input', 'change', 'Seniority_Level']]
        },

        {
            data: ['Years in Current Position', self.getCheckedLabels(
                '#campaign_field_YearsInCurrentPosition>label',
                'std',
                null
            )], nodes: [['#campaign_field_YearsInCurrentPosition>label>input', 'change', 'Years_in_Current_Position']]
        },

        {
            data: ['Years at Current Company', self.getCheckedLabels(
                '#campaign_field_YearsAtCurrentCompany>label',
                'std',
                null
            )], nodes: [['#campaign_field_YearsAtCurrentCompany>label>input', 'change', 'Years_at_Current_Company']]
        },

        {
            data: ['Years of Experience', self.getCheckedLabels(
                '#campaign_field_YearsOfExperience>label',
                'std',
                null
            )], nodes: [['#campaign_field_YearsOfExperience>label>input', 'change', 'Years_of_Experience']]
        },

        {
            data: ['Postal Code', self.getSingleValue('#campaign_field_PostalCode')],
            nodes: [['#campaign_field_PostalCode', 'bykey', 'Postal_Code']]
        },
        {
            data: ['Your Groups', self.getCheckedLabels(
                '#campaign_field_Groups>label',
                'std',
                null
            )], nodes: [['#campaign_field_Groups>label>input', 'change', 'Your_Groups', 'triggerClearRelationship']]
        },

        {
            data: ['First Name', self.getTextareaValues('#campaign_field_FirstName')],
            nodes: [['#campaign_field_FirstName', 'bykey', 'First_Name']]
        },

        {
            data: ['Last Name', self.getTextareaValues('#campaign_field_LastName')],
            nodes: [['#campaign_field_LastName', 'bykey', 'Last_Name']]
        },
    ];
};

//****************************************************************************************************************************

var SimpleCampaignBuilderDialog = inherit(CampaignBuilderDialogBase, function(companyId, userId){
    this.__super(companyId, userId);
});

SimpleCampaignBuilderDialog.prototype.isLISearchURL = function (url) {
    return isLinkedInSimpleSearchUrl(url);
};

SimpleCampaignBuilderDialog.prototype.getItems = function () {
    var self = this;
    return [
        {
            data: ['Title', {
                children: [
                    ['VISIT', self.getTextareaValues('#campaign_field_TitlesToVisit')],
                    ['SKIP', self.getTextareaValues('#campaign_field_TitlesToSkip')]
                ]
            }], nodes: [['#campaign_field_TitlesToVisit', 'bykey', 'Title--VISIT', 'triggerSearch'],
            ['#campaign_field_TitlesToSkip', 'bykey', 'Title--SKIP', 'triggerSearch']]
        },

        {
            data: ['Keywords', {
                children: [
                    ['VISIT', self.getTextareaValues('#campaign_field_KeywordsToVisit')],
                    ['SKIP', self.getTextareaValues('#campaign_field_KeywordsToSkip')]
                ]
            }], nodes: [['#campaign_field_KeywordsToVisit', 'bykey', 'Keywords--VISIT'],
            ['#campaign_field_KeywordsToSkip', 'bykey', 'Keywords--SKIP']]
        },

        {
            data: ['Geography',
                self.boxes.geographyBox.getTitles().concat(self.getCheckedLabels(
                    '#campaign_field_Geography>label.leaf',
                    'std',
                    '.label-name'
                ))
            ], nodes: [['#campaign_field_Geography>label>input', 'change', 'Geography']]
        }, //this not working for geography, see 'new-ui:trigger_tree'

        {
            data: ['Relationship', self.getCheckedLabels(
                '#campaign_field_Relationship>label',
                'std',
                null
            )], nodes: [['#campaign_field_Relationship>label>input', 'change', 'Relationship']]
        },
        {
            data: ['Industry', self.getCheckedLabels(
                '#campaign_field_Industry>label.leaf',
                'std',
                '.label-name'
            )], nodes: [['#campaign_field_Industry>label>input', 'change', 'Industry']]
        },

        {
            data: ['Company', self.getTextareaValues('#campaign_field_Company')],
            nodes: [['#campaign_field_Company', 'bykey', 'Company']]
        },

        {
            data: ['First Name', self.getTextareaValues('#campaign_field_FirstName')],
            nodes: [['#campaign_field_FirstName', 'bykey', 'First_Name']]
        },

        {
            data: ['Last Name', self.getTextareaValues('#campaign_field_LastName')],
            nodes: [['#campaign_field_LastName', 'bykey', 'Last_Name']]
        },
    ];
};
