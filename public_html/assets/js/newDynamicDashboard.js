//**********************************************************************************************************************

function LocationBox($box, prefix, url) {

    if (typeof prefix === "undefined") prefix = "";

    var self = this;

    if (typeof url !== 'undefined') {
        self.url = url;
    } else {
        self.url = '/new-ui/ajax_get_li_locations.php';
    }

    self.locations = {};
    self.selectedLocations = {};
    self.$box = $box;
    self.prefix = prefix;

    self.$suggestions = self.$box.find('.suggestions');
    self.$add_location = self.$box.find('.add_location');
    self.$s_box = self.$box.find('.suggestions .box');
    self.$s_inp = self.$box.find('.suggestions input');

    self.$box.on('click', '.location_box_item_close', function () {
        var el = $(this);
        self.removeLocation(el.data('loc'));
        el.parent().remove();
    });

    self.$s_box.on('click', 'li', function () {
        self.addLocation($(this).index());
    });

    self.$s_inp.val('');
    self.$s_inp.unbind('keyup');
    self.$s_inp.bind('keyup', self.handleSuggestions.bind(self));
    self.$s_inp.bind('keydown', function (evt) {
        if (evt.keyCode === 13) {
            evt.stopPropagation();
            evt.preventDefault();
        }
    });
    self.$suggestions.show();
    self.$s_inp.focus();
}

LocationBox.prototype.getTitles = function () {
    return _.map(_.values(this.selectedLocations), function(val){ return val.n });
};

//on item 'close' click
LocationBox.prototype.removeLocation = function (locId) {
    var self = this;
    var $hiddenInput = $('#' + self.prefix + '__hidden_location_facets');
    var newVal = _($hiddenInput.val().split('--'))
    .filter(function (val) {
        return val !== locId;
    })
    .value()
    .join('--');
    $hiddenInput.val(newVal);
    delete self.selectedLocations[locId];
    $(document).trigger('new-ui:trigger_tree');
};

LocationBox.prototype.clear = function () {
    var self = this;
    $('#' + self.prefix + '__hidden_location_facets').val('');
    self.selectedLocations = {};
    self.$box.find('.location_box_item').remove();
    $(document).trigger('new-ui:trigger_tree');
};

LocationBox.prototype.addLocation = function (idx) {
    var self = this;
    var loc = self.getActiveSuggestion(idx);
    self.addRawLocation(loc);
};

LocationBox.prototype.addRawLocation = function (loc) {
    var self = this;
    var $hiddenInput = $('#' + self.prefix + '__hidden_location_facets');
    var $listItem, oldVal = $hiddenInput.val();

    var predefinedItem = $('input[name="predefinedRegions['+loc.id+']"]');
    if(predefinedItem.length > 0){
        predefinedItem.prop('checked', true);

        //trigger a cross-component event
        $(document).trigger('new-ui:trigger_tree');
    }else if (loc && oldVal.indexOf(loc.id) === -1) {
        self.selectedLocations[loc.id] = loc;
        $hiddenInput.val(oldVal + '--' + loc.id);
        $listItem = $('<li class="location_box_item"><span class="location_box_item_close" data-loc="' + loc.id +
            '">&#10005;</span><span class="text">' + loc.n +
            '</span></li>');
        self.$add_location.append($listItem);

        //trigger a cross-component event
        $(document).trigger('new-ui:trigger_tree');
    }
    self.$s_inp.val('');
    self.$s_box.html('');
};

LocationBox.prototype.handleSuggestions = function (evt) {
    var self = this;
    if (evt.keyCode === 13) {
        // Enter was pressed.
        evt.preventDefault();
        evt.stopPropagation();
        self.addLocation(self.active_row_index);
        return;
    } else if (evt.keyCode === 40) {
        self.active_row_index = self.setActiveSuggestion(self.active_row_index + 1);
        return;
    } else if (evt.keyCode === 38) {
        self.active_row_index = self.setActiveSuggestion(self.active_row_index - 1);
        return;
    }

    var i;
    // Request suggestions.
    $.ajax({
        'url': self.url,
        'type': 'post',
        'dataType': 'json',
        'data': {action: 'get_location', user_id: window.campaignBuilder.user_id, str: self.$s_inp.val()},
        'success': function (res) {
            self.$s_box.css({'z-index': self.$s_box.css('z-index') + 1});
            var allIsBad = true;
            if (res.status === "success") {
                var val = self.$s_inp.val().toLowerCase();
                for (i in res.payload) {
                    if (res.payload[i].n.toLowerCase().search(val) !== -1) {
                        allIsBad = false;
                    }
                }
            }
            if (allIsBad) {
                //just smart faking
                res.payload = []
            }

            self.$s_box.html('');
            for (i in res.payload) {
                var $row = $('<li></li>').html(res.payload[i].n);
                $row.attr({'data-id': res.payload[i].id});
                self.locations[res.payload[i].id] = res.payload[i];
                self.$s_box.append($row);
            }
            self.active_row_index = 0;
            self.setActiveSuggestion(self.active_row_index);
        }
    });
};

LocationBox.prototype.setActiveSuggestion = function (num) {
    var self = this;
    if (num < 0) return 0;
    self.$lis = self.$s_box.children('li');
    if (num >= self.$lis.length) return self.$lis.length - 1;
    self.$s_box.find('li.active').removeClass('active');
    self.$lis.eq(num).addClass('active');
    return num;
};

LocationBox.prototype.getActiveSuggestion = function (idx) {
    return this.locations[this.$lis.eq(idx).attr('data-id')];
};

//***************************************************************************************************************************

function CampaignBuilder($container) {
    var self = this;
    self.$container = $container;
    self.company_id = $container.data('company-id');
    self.user_id = $container.data('user-id');
    self.$campaigns = $container.find('.campaign-item');

    self.$campaigns.bind('click', self.handleCampaignClick.bind(self));
    self.$container.find('#change_user').bind('change', self.handleChangeUser.bind(self));

    self.setupCampaignSorting();
    
    self.campaignBuilderDialog = new window[window.CampaignBuilderDialogImpl](self.company_id, self.user_id);
}        


CampaignBuilder.prototype.handleCampaignClick = function (event) {
    var $campaignItem = $(event.currentTarget);
    var file = "campaign_builder.php";
    if (this.$container.data('file')) {
        file = this.$container.data('file');
    }
    document.location.href = "/new-ui/" + file + "?company_id=" + this.company_id + "&user_id=" + this.user_id +
        '&campaign_id=' + $campaignItem.data('id');
};

CampaignBuilder.prototype.handleChangeUser = function (event) {
    var $select = $(event.currentTarget);
    var file = "campaign_builder.php";
    if (this.$container.data('file')) {
        file = this.$container.data('file');
    }
    document.location.href = "/new-ui/" + file + "?company_id=" + this.company_id + "&user_id=" + $select.val();
};

CampaignBuilder.prototype.setupCampaignSorting = function () {
    var self = this;
    self.$sortable_active_campaign = $('#sortable_active_campaigns');
    function handler() {
        var order = [];
        self.$sortable_active_campaign.find('.campaign-item').each(function () {
            order.push($(this).data('id'));
        });
        $.ajax({
            'url': '/new-ui/ajax.php?company_id=' + self.company_id + '&user_id=' + self.user_id,
            'type': 'post',
            'data': {'action': 'updateCampaignOrder', 'order': order},
            'success': function (response) {
            }
        });
    }
    
    self.$sortable_active_campaign.sortable({
        receive: function () {
        }, //TODO this was NOT implemented: $.proxy(this.sortableReceived, this),
        update: handler
    }).disableSelection();

    
    movableListNodes(self.$sortable_active_campaign, 
        '.campaign-item', '.ctl__position_up', '.ctl__position_down', handler);
};


function DynamicDashboard($container){
    
    var t = this;
    t.$container = $container;
    t.companyId = t.$container.data('company-id');
    t.userId = t.$container.data('user-id');

    var handleData = function (data) {
   		// Update campaigns.
   		for (var i in data) {
   			t.$container.find('*[data-dynamic="'+i+'"]').html(data[i]);
   		}
   	};

	var tick = function (callback) {
		$.ajax({
			'url':'/new_dynamic_dashboard.php?company_id=' + t.companyId + '&user_id=' + t.userId,
			'type': 'post',
			'data': {'action': 'get'},
			'dataType': 'json',
			'success': function (resp) {
			    if (resp.status === "success") {
					handleData(resp.payload);
				}
			},
            timeout: 3000,
            complete: function () {
                callback();
            }
		});
	};

    asyncRecurrentSequence(tick, 10000, false);
}

//*****************************************************************************************************************************
$(function () {
    
    window.campaignBuilder = new CampaignBuilder($('#campaign_builder'));
    
	if ($('*[data-function="dynamic-dashboard"]').length > 0) {
        window.dynamicDashboard = new DynamicDashboard($('*[data-function="dynamic-dashboard"]'));
	}
    
});

//*****************************************************************************************************************************

$(document).ready(function() {
    
    var url = /http(s?):\/\//gi;
    var isMaxLength = false;
    var textarea = $('#mainItcMessageField');
    var counter = $('#counter');

    $(textarea)
        .unbind('keyup change input paste')
        .bind('keyup change input paste', function(e) {
    
            var $this = $(this),
                valLength = $this.val().length,
                maxCount = $this.attr('maxlength');

            if(!$this.hasClass('changed')) {
                $this.addClass('changed')
            }
    
            $(counter).text(maxCount - valLength);
    
            if (e.keyCode == 8 && e.keyCode !== 9 && e.keyCode !== 13 && e.keyCode !== 32 && e.keyCode !== 46) {
                return;
            }
    
            if (e.keyCode == 17) {
                return;
            }
    
            if ($(textarea).val !="") { 
                if($(textarea).val().match(url)) {
                    alert('Please remove external URL from the message as LinkedIn does not allow external URLs within connection requests.')
                }
                $(textarea).val($(textarea).val().replace(url,""));
            }
            
            if( valLength < maxCount && isMaxLength ) {
                isMaxLength = false;
            }

            if( (valLength == maxCount || valLength > maxCount) && !isMaxLength ) {
                alert('The message exceeds the maximum supported size.')
                isMaxLength = true;
            }
    
            if( valLength > maxCount ) {
                $this.val($this.val().substring(0,maxCount));
                $(counter).text('0')
            }
            
        }); 
    
    
    $('.btn-add-param').on('click', function(){
        var varLength = $(this).data('name').length + 4;
        if (($(textarea).attr('maxlength') - $(textarea).val().length) < varLength ) {
            alert('The message exceeds the maximum supported size.')
            return;
        }

        $(textarea).val($(textarea).val() + '{{' + $(this).data('name') + '}}')
        $(textarea).keyup();
    })
})