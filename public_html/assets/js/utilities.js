var URL_PARAMS = (function (rawParameters) {
    if (rawParameters.length === 1 && rawParameters[0] === "") return {};
    var params = {};
    for (var i = 0; i < rawParameters.length; ++i) {
        var parts = rawParameters[i].split('=', 2);
        if (parts.length === 1) {
            params[parts[0]] = "";
        } else {
            params[parts[0]] = decodeURIComponent(
                parts[1].replace(/\+/g, " ")
            );
        }
    }
    return params;
})(window.location.search.substr(1).split('&'));


function joinParamsToUrl(url, params){
    return url + (url.indexOf('?') === -1 ? '?' : '&') + params;
}


function _ajax(method, url, params, ajaxParams) {
    if (typeof ajaxParams === 'function'){ //success callback shortcut
        ajaxParams = {success: ajaxParams};
    }
    var data = params || {};
    var config = {
        url: url,
        type: method.method,
        data: data,
        success: function (res) {
            if (res.status !== "success") {
                console.error('AJAX error', url, res.status, res.payload);
                return false;
            }
            if (!res.payload) {
                console.error('AJAX empty payload', url);
                return false;
            }
            ajaxParams.success(res.payload);
        },
        error: function (error) {
            console.error('AJAX unknown server error: ', error);
            if (ajaxParams.error) ajaxParams.error(res.payload);
        }
    };
    
    if (ajaxParams.timeout) config.timeout = ajaxParams.timeout;
    if (ajaxParams.complete) config.complete = ajaxParams.complete;
    
    if (method.json){
        config.dataType = 'json';
        config.processData = false;
        config.data = JSON.stringify(data); 
            
    }
    $.ajax(config);
}

function ajaxGet(url, params, ajaxParamsOrCallback) {
    _ajax({method: 'GET'}, url, params, ajaxParamsOrCallback);
}

function ajaxPostForm($form, additionalParams, ajaxParamsOrCallback) {
    var url = $form.attr('action');
    var params = $.param(additionalParams || {});
    _ajax({method: 'POST'}, params ? joinParamsToUrl(url, params) : url, $form.serialize(), ajaxParamsOrCallback);
}

function ajaxPostJSON(url, params, ajaxParamsOrCallback) {
    _ajax({method: 'POST', json: true}, url, params, ajaxParamsOrCallback);
}

function htmlDecode(htmlString) {
    htmlDecode.el = document.createElement('div');
    htmlDecode.el.innerHTML = htmlString;
    //return htmlDecode.el.innerHTML;
    return htmlDecode.el.childNodes.length === 0 ? "" : htmlDecode.el.childNodes[0].nodeValue;
}
htmlDecode.el = null; //single thread

function htmlDecodeDropTags(htmlString) {
    return htmlDecode(htmlString.replace(/<\s*[\w\-]+.*>/g, ''));
}

function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart === "number" && typeof el.selectionEnd === "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() === el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start, //this can be used to determine caret position if no actual selection
        end: end
    };
}

//INFO 
//LinkedIn Lead builder uses unquoted parentheses and replace quoted fro mpasted URL if rebuild
//so, \(\) is mandatory!!!

function getAllUrlsFromString(s){
    return s.match(/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#\-\(\)]*[\w@?^=%&/~+#\-\(\)])?/gi);
}

function isLinkedInSearchUrl(s){
    return /^https?:\/\/(www\.)?linkedin\.com\/sales\/search([\w.,@?^=%&:/~+#\-\(\)]*[\w@?^=%&/~+#\-\(\)])?$/.test(s) &&
          s.indexOf('leadBuilder=true') === -1;  //this is not the filters dialog but a real search URL
}

function isLinkedInSimpleSearchUrl(s){
    return /^https?:\/\/(www\.)?linkedin\.com\/search\/results\/(index|people)([\w.,@?^=%&:/~+#\-\(\)]*[\w@?^=%&/~+#\-\(\)])?$/.test(s) &&
          s.indexOf('leadBuilder=true') === -1;  //this is not the filters dialog but a real search URL
}


function isSavedSearchLinkedInUrl(s){
    return /^https?:\/\/(www\.)?linkedin\.com\/[\w\/]+.*\?.*savedSearchId=\d+/.test(s);
}


function screenDims() {
    return {
        width: window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width,
        height: window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height
    }
}


function modalLikePopup(url, title, content, w, h) {
    // Fixes dual-screen position Most browsers Firefox
    var dualScreenLeft = window.screenLeft != null ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != null ? window.screenTop : screen.top;

    var dims = screenDims();

    w = w || Math.floor(dims.width * 0.7);
    h = h || Math.floor(dims.height * 0.7);
    
    var left = ((dims.width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((dims.height / 2) - (h / 2)) + dualScreenTop;

    var newWindow = window.open(
        content ? '' : url,
        title || '',
        'location=' + (content ? 'no' : 'yes') +
        ', menubar=no, personalbar=no, toolbar=no, status=no, resizable=yes, scrollbars=yes, width=' + w + ', height=' + h +
        ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    newWindow.focus();
    
    if (content){
        $(newWindow.document.body).html(content);
    }
}

function escapeHTML(string) {
    return String(string).replace(/[&<>"'`=\/\n]/g, function (s) {
        return escapeHTML.entityMap[s];
    });
}

escapeHTML.entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;',
    '\n': '<br/>'
};

function addScript(src, appentTo /*body, head*/, attrs) {
    var s = document.createElement('script');
    s.setAttribute('src', src);
    for (var i in (attrs || '').trim().split(/\s+/)) s.setAttribute(attrs[i], '');
    document[appentTo || 'body'].appendChild(s);
}

function setupMediaAutoSize(video, margin) {
    var resize = function() {
        var videoRatio = video.height / video.width;

        var dims = screenDims();
        var h = dims.height;
        var w = dims.width;

        var windowRatio = h / w;
        /* browser size */

        if ( videoRatio > windowRatio) {
            /* smallest video height */
            video.height = h > (50 + margin) ? (h - margin) : 50;
        } else {
            video.width = w > (50 + margin) ? (w - margin) : 50;
        }
    };

    window.addEventListener('resize', resize, false);

    video.height = 100;
    /* to get an initial width to work with*/
    resize();
}

/**
 *
 * @param handler - interval handler with possibly async calls
 *   - @param Object handlerArgs
 *   - @param function callback - callback with no args
 * @param msWait - waiting interval between calls
 * @param adjustWaitingTime bool - do correction of interval
 *      (next call would happen in (msWait - previous_request_running_time) instead of msWait
 * @param minimumWaitingTime
 */
function asyncRecurrentSequence(handler, msWait, adjustWaitingTime, minimumWaitingTime) {
    var timeout;
    var start, stop;
    
    var callback = function(){
        stop = new Date();
        var difference = stop - start;
        var delay = adjustWaitingTime ? 
            Math.max(msWait - difference, minimumWaitingTime || 0) : //no negative
            msWait; 
        
        timeout = setTimeout(wrapped, delay);
    };
    
    var wrapped = function () {
        start = new Date();
        handler(callback);
    };
    
    wrapped();
}

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

    
//------>> WARNING!!! this trivial implementation of "super" works properly ONLY for ONE level of inheritance
//------->> it is not anyway recommended to have more than one level.
function inherit(ParentClass, NewClass) {
    NewClass.prototype = Object.create(ParentClass.prototype);
    NewClass.prototype.constructor = NewClass;
    NewClass.prototype.__super = ParentClass;
    NewClass.prototype.__supermember = ParentClass.prototype;  //access super methods like this: this.__supermember.someMethod(...)
    return NewClass;
}

function movableListNodes($container, selNode, selUp, selDown, onMoved){
    var $nodes = $container.find(selNode);
    if ($nodes.length === 0) console.error('No sortable nodes found!');
    $nodes.find(selUp).on('click', function(event){
        event.stopPropagation();
        var $node = $(this).closest(selNode); // get closest item div
        $node.prev(selNode).before($node);
        onMoved($node, 'up');
    });
    $nodes.find(selDown).on('click', function(event){
        event.stopPropagation();
        var $node = $(this).closest(selNode); // get closest item div
        $node.next(selNode).after($node); // if it is move after next
        onMoved($node, 'down');
    });
}