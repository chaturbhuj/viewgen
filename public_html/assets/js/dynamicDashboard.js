
function handleIncomingClientStatus(data) {

	var $box = $('<div></div>').addClass('client_software_box');

	for (var i in data.statuses) {
		var stat = data.statuses[i];
		var $row = $('<div></div>').addClass('row');
		var $software = $('<i></i>').addClass('software_status').addClass(stat.software_status);
		var $version = $('<b></b>').addClass('bold').html(stat.client_version);
		var $message = $('<span></span>').html(stat.message);

		var name = stat.name;

		$row.append($software);
		$row.append(name);
		$row.append(' &nbsp;');
		$row.append($version);
		$row.append($message);
		$box.append($row);

		if (typeof stat.newRowAlert != 'undefined') {
			$box.append('<div class="row" style="margin-bottom: 8px;">'+stat.newRowAlert+'</div>');
		}
	}

	$('.client_software_box').html($box.html());

	for (var i in data.campaigns) {
		var visits = $('.num_visits span[data-id="'+data.campaigns[i].campaign_id+'"]').html();
		var new_visits = data.campaigns[i].visits;
		var $box = $('.num_visits span[data-id="'+data.campaigns[i].campaign_id+'"]');
		$box.html(new_visits);
		if (visits != new_visits) {
			$box.fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
		}
	}
}

$(function () {

	if (document.location.pathname != "/dashboard.php") return;

	var timer = setInterval(function () {
		$.ajax({
			'url':'/dynamic_dashboard.php?company_id=' + window.company_id,
			'type': 'post',
			'data': {'action': 'get_client_status'},
			'dataType': 'json',
			'success': function (resp) {
				if (resp.status == "success") {
					handleIncomingClientStatus(resp.payload);
				}
			}
		});
	}, 10000);

});

