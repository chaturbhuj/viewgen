
function LocationBox($box, prefix) {

	if (typeof prefix == "undefined") prefix = "";

	var self = this;

	self.locations = {};
	self.$box = $box;
	self.prefix = prefix;
	
	self.$box.delegate('li', 'click', function () {
		self.addLocation($(this).index())
	});

	self.addLocation = function (idx) {
		self.$s_inp.val('');
		var loc = self.getActiveSuggestion(idx);
		var $inp = $('<input type="checkbox" name="'+self.prefix+'location['+loc.id+']" id="loc_'+loc.id+'">').attr({checked: 'checked'});
		var $label = $('<label class="radio" for="loc_'+loc.id+'"></label>').html(loc.n);
		self.$add_location.before($inp, $label);

		self.$s_box.html('');
	};

	self.handleSuggestions = function (evt) {

		if (evt.keyCode == 13) {
			// Enter was pressed.
			evt.preventDefault();
			evt.stopPropagation();
			self.addLocation(self.active_row_index);
			return;
		} else if (evt.keyCode == 40) {
			self.active_row_index = self.setActiveSuggestion(self.active_row_index+1);
			return;
		} else if (evt.keyCode == 38) {
			self.active_row_index = self.setActiveSuggestion(self.active_row_index-1);
			return;
		}

		// Request suggestions.
		$.ajax({
			'url': 'ajax.php',
			'type': 'post',
			'dataType': 'json',
			'data': {action: 'get_location', str: self.$s_inp.val()},
			'success': function (res) {
				if (res.status != "success") return;
				var allIsBad = true;
				for (var i in res.payload) {
					if (res.payload[i].n.toLowerCase().search(self.$s_inp.val().toLowerCase()) == 0) {
						allIsBad = false;
					}
				}
				if (allIsBad) return;
				self.$s_box.html('');
				for (var i in res.payload) {
					var $row = $('<li></li>').html(res.payload[i].n);
					$row.attr({'data-id': res.payload[i].id});
					self.locations[res.payload[i].id] = res.payload[i];
					self.$s_box.append($row);
				}
				self.active_row_index = 0;
				self.setActiveSuggestion(self.active_row_index);
			}
		});
	};

	self.setActiveSuggestion = function (num) {
		if (num < 0) return 0;
		$lis = self.$s_box.children('li');
		if (num >= $lis.length) return $lis.length-1;
		self.$s_box.find('li.active').removeClass('active');
		$lis.eq(num).addClass('active');
		return num;
	};

	self.getActiveSuggestion = function (idx) {
		return self.locations[$lis.eq(idx).attr('data-id')];
	};

	
	self.$suggestions = self.$box.find('.suggestions');
	self.$add_location = self.$box.find('.add_location');
	self.$s_box = self.$box.find('.suggestions .box');
	self.$s_inp = self.$box.find('.suggestions input');
	self.$s_inp.val('');
	self.$s_inp.unbind('keyup');
	self.$s_inp.bind('keyup', self.handleSuggestions);
	self.$s_inp.bind('keydown', function (evt) {if (evt.keyCode == 13) {evt.stopPropagation(); evt.preventDefault();}});
	self.$suggestions.show();
	self.$s_inp.focus();

}

$(function () {

window.App = new (function () {

	var self = this;

	self.boxes = [];

	// Bind events
	$('.add_location').click(function (evt) {
		evt.stopPropagation();
		evt.preventDefault();

		self.register_suggestion_box($('.editor_box.collapsee'));
	});

	self.register_suggestion_box = function ($box, prefix) {
		self.boxes.push(new LocationBox($box, prefix));
	};

	$("input[name='campaign_type']").change(function () {
		if ($(this).val() == 'query') {
			$('.editor_box.collapsee').hide();
		} else {
			$('.editor_box.collapsee').show();
		}
	});

	$("input[name='other_locations']").change(function () {
		if ($('#wizard_location_n4').is(':checked')) {
			$('.condensed_location').css({'display': 'inline-block'});
		} else {
			$('.condensed_location').css({'display': 'none'});
		}
	});

	$('#priority').change(function () {
		$('#priority_indicator').removeClass('p1 p2 p3').addClass('p'+$(this).val());
	});

	$('#create_campaign_submit').click(function (evt) {
		evt.preventDefault();
		evt.stopPropagation();
		if ($(this).hasClass("campaign_validation")) {
			// Do validation.

			$("#errors").html('');
			var ok = true;

			if ($('select[name="li_user"]').val() == 0) {
				self.addError("You must assign a user to the campaign");
				ok = false;
			}

			if ($('input[name="campaign_name"]').val() == "") {
				self.addError("Campaign name can not be empty");
				ok = false;
			}

			var num_loc = 0;
			$('input[name^="location"]').each(function () {
				if ($(this).is(':checked')) num_loc++;
			});

			if (!$("#campaign_type_query").is(":checked") && num_loc == 0) {
				self.addError("Campaign needs at least one location");
				ok = false;
			}

			if (!ok) {
				return;
			}
		}
		if ($(this).hasClass("query_campaign_validation")) {
			// Do validation.

			$("#errors").html('');
			var ok = true;

			if ($('select[name="li_user"]').val() == 0) {
				self.addError("You must assign a user to the campaign");
				ok = false;
			}

			if ($('input[name="campaign_name"]').val() == "") {
				self.addError("Campaign name can not be empty");
				ok = false;
			}

			if (!ok) {
				return;
			}
		}
		if ($(this).hasClass("edit_builder_campaign_validation")) {

			$("#errors").html('');
			var ok = true;

			// Count selected properties.
			var num_functions = $('input[name^="functions"]:checked').length;
			var num_seniority_levels = $('input[name^="seniority_levels"]:checked').length;
			var num_company_sizes = $('input[name^="company_sizes"]:checked').length;
			var num_industries = $('input[name^="industries"]:checked').length;
			var has_titles = $('textarea[name^="titles"]').val().length;
			var has_companies = $('textarea[name^="companies"]').val().length;
			var has_keywords = $('textarea[name^="keywords"]').val().length;

			if (num_functions + num_seniority_levels + num_company_sizes + num_industries + has_titles + has_companies + has_keywords == 0) {
				self.addError("You must specify at least one of the following: Functions, Seniority Levels, Company Size, Industries, Titles, Companies or Keywords");
				ok = false;
			}

			if (!ok) {
				return;
			}
		}
		$(this).parents('form').submit();
	});
	
	self.addError = function (str) {
		$("#errors").append($('<h1></h1>').append(str));
	}

	$('#add_query_urls').click(function (evt) {
		evt.preventDefault();
		evt.stopPropagation();
		$("#redirect").val($('#redirect').attr('data-alt'));
		$(this).parents('form').submit();
	});

	$('#form_submit').click(function (evt) {
		evt.preventDefault();
		evt.stopPropagation();
		$(this).parents('form').submit();
	});

	if ($('#start_date').length > 0) {
		new datepickr('start_date', {
			'dateFormat': 'M j, Y'
		});
	}

	if ($('#end_date').length > 0) {
		new datepickr('end_date', {
			'dateFormat': 'M j, Y'
		});
	}

	

	$('#end_date_never').change(function () {
		if($(this).is(':checked')) {
			$('#end_date').val('No end date');
		}
	});

	$('#end_date').change(function () {
		if ($(this).val().toLowerCase() == "no end date" ) {
			$('#end_date_never').prop('checked', true);
		} else {
			$('#end_date_never').prop('checked', false);
		}
	});

	$(".counted").delegate('input', 'change', function () {
		// find previous <h4>
		if ($(this).hasClass('all_industry')) return;
		var on_off = $(this).is(':checked') ? 1 : -1;

		self.incrCounter($(this), on_off, true);
	});

	$("#add_campaign.no_users").click(function (evt) {
		evt.preventDefault();
		evt.stopPropagation();
		alert("You need to download the client software before creating campaigns");
	});

	$('input[name="subscription_type"]').change(function () {
		if ($(this).val() == "free") $("#subscription_plan_code").prop("disabled", true);
		else $("#subscription_plan_code").prop("disabled", false);
	});

	$('#subscription_plan_code').change(function () {
		var cost = $(this).find(':selected').attr('data-cost');
		$('label[for="paid"]').html(cost);
	});

	self.incrCounter = function ($elem, count, overwrite) {
		// Find h4
		var $h4 = null;
		var $inp = $elem;
		$elem.parent().children().each(function () {
			if ($(this).index() == $inp.index()) return false;
			if ($(this).is('h4')) $h4 = $(this);
		});
		if ($h4 == null) {
			$elem.parent().parent().parent().children().each(function () {
				if ($(this).index() == $elem.parent().parent().index()) return false;
				if ($(this).is('h4')) $h4 = $(this);
			});
		}
		var str = $h4.html();
		var num = /\(([0-9]+)/.exec(str);
		if (num == null) return;
		num = parseInt(num[1]);
		if (num>147){ num = 147; count = 0};
		if (num < 0) num = 0;
		if (num == 0 && count < 0) count = 0;
		$h4.html(str.replace(/\(([0-9]+)/, '('+(overwrite ? num+count : count)));
	};

	$('body').delegate('.show_industries', 'click', function (evt) {
		evt.preventDefault();
		evt.stopPropagation();
		var $box = $('#hidden_industry_box');
		if ($box.is(':visible')) {
			$('.show_industries').removeClass('shown');
			$('.more.expand').removeClass('is_hidden');
			$box.hide();
		} else {
			$('.show_industries').addClass('shown');
			$('.more.expand').addClass('is_hidden');
			$box.show();
		}
	});

	$('.all_industries').change(function () {
		var $industries = $(this).parent().parent().find('input[name^="industries"]');
		if ($(this).is(':checked')) {
			$industries.prop('checked', true);
			$industries.each(function () {
				self.industry_change($(this));
			});
			self.incrCounter($(this), $industries.length, false);
		} else {
			$industries.prop('checked', false);
			$industries.each(function () {
				self.industry_change($(this));
			});
			self.incrCounter($(this), 0, false);
		}
	});

	self.industry_change = function ($obj) {
		if ($obj.prop('checked')) {
			var all = true;
			$('input[name^="industries"]').each(function () {
				if (!$(this).is(':checked')) {
					all = false;
					return false;
				}
			});
			// Sync with others.
	//		$('#industries_fp_'+$(this).attr('data-id')).prop('checked', true);
//<input data-id="<?=$id?>" type="checkbox" name="tmp_industries[<?=$id?>]" id="industries_fp_<?=$id?>" checked="checked"><label for="industries_fp_<?=$id?>" class="checkbox"><?=$idToIndustry[$id]?></label>
			if ($('.builder_col.industries').find('input[name^="tmp_industries"]').length < 10) {
				
				var id = $obj.attr('data-id');
				if ($('#industries_fp_'+id).length == 0) {
					$inp = $('<input type="checkbox" checked="checked">');
					$inp.addClass('tmp_industry');
					$label = $('<label class="checkbox"></label>').html($obj.next().html());
					$inp.attr({'data-id': id, 'id': 'industries_fp_'+id, 'name': 'tmp_industries['+id+']'});
					$label.attr({'for': 'industries_fp_'+$obj.attr('data-id')});
					$('#hidden_industry_box').before($inp, $label);
				}
			}
			
			if (all) $('#all_industries').prop('checked', true);
		} else {
			$('#all_industries').prop('checked', false);
			$('#industries_fp_'+$obj.attr('data-id')).next().remove();
			$('#industries_fp_'+$obj.attr('data-id')).remove();
		}
	};

	$('input[name^="industries"]').change(function () {
		self.industry_change($(this));
	});

	$('.builder_col.industries').delegate('input[name^="tmp_industries"]', 'change', function () {
		var id = $(this).attr('data-id');
		$('#hidden_industry_box').find('#industries_'+id).prop('checked', false);
		$(this).next().remove();
		$(this).remove();
		$('#all_industries').prop('checked', false);
	});

	$('.all_functions').change(function () {
		var $industries = $(this).parent().find('input[name^="functions"]');
		if ($(this).is(':checked')) {
			$industries.prop('checked', true);
			self.incrCounter($(this), $industries.length, false);
		} else {
			$industries.prop('checked', false);
			self.incrCounter($(this), 0, false);
		}
	});

	$('input[name^="functions"]').change(function () {
		if ($(this).is(':checked')) {
			var all = true;
			$('input[name^="functions"]').each(function () {
				if (!$(this).is(':checked')) {
					all = false;
					return false;
				}
			});
			if (all) $(this).parent().parent().find('.all_functions').prop('checked', true);
		} else {
			$(this).parent().parent().find('.all_functions').prop('checked', false);
		}
	});

	$('#all_company_sizes').change(function () {
		var $industries = $('input[name^="company_sizes"]');
		if ($(this).is(':checked')) {
			$industries.prop('checked', true);
			self.incrCounter($(this), $industries.length, false);
		} else {
			$industries.prop('checked', false);
			self.incrCounter($(this), 0, false);
		}
	});

	$('input[name^="company_sizes"]').change(function () {
		if ($(this).is(':checked')) {
			var all = true;
			$('input[name^="company_sizes"]').each(function () {
				if (!$(this).is(':checked')) {
					all = false;
					return false;
				}
			});
			if (all) $('#all_company_sizes').prop('checked', true);
		} else {
			$('#all_company_sizes').prop('checked', false);
		}
	});

	$('.all_seniority_levels').change(function () {
		var $industries = $(this).parent().parent().find('input[name^="seniority_levels"]');
		if ($(this).is(':checked')) {
			$industries.prop('checked', true);
			self.incrCounter($(this), $industries.length, false);
		} else {
			$industries.prop('checked', false);
			self.incrCounter($(this), 0, false);
		}
	});

	$('input[name^="seniority_levels"]').change(function () {
		if ($(this).is(':checked')) {
			var all = true;
			$('input[name^="seniority_levels"]').each(function () {
				if (!$(this).is(':checked')) {
					all = false;
					return false;
				}
			});
			if (all) $(this).parent().parent().find('.all_seniority_levels').prop('checked', true);
		} else {
			$(this).parent().parent().find('.all_seniority_levels').prop('checked', false);
		}
	});

	$('.campaign_name.edit').click(function () {
		$(this).hide();
		$(this).parent().find('.name').hide();
		$(this).next().css({'display': 'block'});
	});

	/*
	$('#hidden_industry_box, #hidden_industry_box label').click(function (evt) {
		evt.stopPropagation();
		window.event.cancelBubble = true;
	});

	$('body').click(function () {
		$('#hidden_industry_box').hide();
	});*/

	$('#end_date').change(function () {
		var end_date = new Date($(this).val());
		var start_date = new Date($('#start_date').val());
		if (end_date < start_date) $(this).val($('#start_date').val());
	});
	$('#start_date').change(function () {
		var start_date = new Date($(this).val());
		var end_date = new Date($('#end_date').val());
		if (end_date < start_date) $('#end_date').val($('#start_date').val());
	});

	$("input[name='other_industries']").change(function () {
		if ($('#wizard_industries_n1').is(':checked')) {
			$('.wizard_industries.industries').show();
		} else {
			$('.wizard_industries.industries').hide();
		}
	});

	$("input[name='other_company_sizes']").change(function () {
		if ($('#wizard_company_size_n1').is(':checked')) {
			$('.wizard_industries.company_size').show();
		} else {
			$('.wizard_industries.company_size').hide();
		}
	});

	$('a.button.delete').click(function (evt) {
		var del = confirm('Are you sure you want to archive '+$(this).attr('data-campaign-name'));
		if (!del) evt.preventDefault();
	});
	$('button.button.delete').click(function (evt) {
		var del = confirm('Are you sure you want to archive '+$(this).attr('data-campaign-name'));
		if (!del) evt.preventDefault();
	});

	if ($('.editor_box.collapsee').hasClass('opened')) {
		self.register_suggestion_box($('.editor_box.collapsee'));
	}

});

$('input.auto-select').on('focus', function (event) {
	var $input = $(event.currentTarget);
	$input.select();
});

$('.modal a.close-modal').click(function () {
	closeModal($(this).parents('.modal'));
});
$('.modal').click(function () {
	closeModal($(this));
});
$('.modal-inner').click(function (event) {
	event.stopPropagation();
});

});

function openModal($target) {
	var windowHeight = $(document).outerHeight();
	var $inner = $target.find('.modal-inner');

	$target.height(windowHeight < $inner.height() ? $inner.height() + 100 : windowHeight);

	$target.fadeIn(200, function () {
		$target.height(windowHeight < $inner.height() ? $inner.height() + 200 : windowHeight + 400);
	});
}

function closeModal($target) {
	$target.fadeOut(200);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function setCookieHours(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function setCookieV2(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

$(function () {
	$('.open-maximizer-settings').click(function () {
		openModal($('.visitMaximizer[data-id="'+$(this).data('id')+'"]'));
	});

	$('.display-option-dropdown').change(function () {
		if ($('.display-option-dropdown').val() == -1) {
			$('#hiddenDiv').css({"opacity":1});
				} else {
			$('.display-option-button').trigger('click');
		}
	});

	$('div.priority a').on('mouseover', function () {
		var $a = $(this);
		$(this).parent().find('a').each(function () {
			if ($(this).data('priority') <= $a.data('priority')) {
				$(this).removeClass('gray').addClass('green');
			} else {
				$(this).removeClass('green').addClass('gray');
			}
		});
	});
	$('div.priority a').on('mouseout', function () {
		var $a = $(this);
		$(this).parent().find('a').each(function () {
			if ($(this).data('priority') <= $a.data('original-priority')) {
				$(this).removeClass('gray').addClass('green');
			} else {
				$(this).removeClass('green').addClass('gray');
			}
		});
	});

	if ($('.follow-scroll').length > 0) {
		var inScrollMode = false;
		$(window).scroll(function (event) {
			if ($(window).scrollTop() > 200 && !inScrollMode) {
				$('.follow-scroll').css({
					'position': 'fixed',
					'top': $('.follow-scroll').offset().top-310
				});
				inScrollMode = true;
			}
			if ($(window).scrollTop() <= 200 && inScrollMode) {
				$('.follow-scroll').css({
					'position': 'static'
				});
				inScrollMode = false;
			}
		});
	} 

});
