$(document).ready(function () {
    $('body').on('click', 'span#float-right-menu', function () {
        if (!( $('span#float-right-menu ul:hover').length > 0 ) && !( $('span#float-right-menu ul:visible').length > 0 ))
            $(this).children('ul').slideToggle();
    })
    .on('mousedown', function () {
        if ($('span#float-right-menu ul:visible').length > 0) {
            if (!( $('span#float-right-menu ul:hover').length > 0 ))
                $('span#float-right-menu ul').slideToggle();
        }
    });

    if(typeof $('.dropdown_menu').dropit === "function"){
        $('.dropdown_menu').dropit();
    }
});

