
$(function () {

	$('.sent-automated-email').click(function () {
		var $link = $(this);
		var emailTo = $(this).data('email');
		var type = $(this).data('type');
		var accountType = $(this).data('account-type');
		var companyId = $(this).data('company-id');
		var name = $(this).data('name');
		$.ajax({
			'url': '/admin/ajax_send_email.php',
			'type': 'post',
			'data': {
				'emailTo': emailTo,
				'type': type,
				'accountType': accountType,
				'company_id': companyId,
				'name': name
			},
			'dataType': 'json',
			'success': function (response) {
				$link.before('SENT');
				$link.remove();
			}
		});
	});
	
});

