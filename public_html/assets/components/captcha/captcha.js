var pageCaptchas = [];

var reCaptchaCallback = function () {
    var captchaClientKey = "6Ld-oyUUAAAAAHCllfF0I3KV7FTye0U0UVX9vQ76";
    $('.g-recaptcha').each(function (index, el) {
        pageCaptchas.push(
            grecaptcha.render(el, {'sitekey': $(this).data('sitekey') || captchaClientKey})
        );
    });
};

addScript('//www.google.com/recaptcha/api.js?onload=reCaptchaCallback&render=explicit', 'head', 'async defer');

