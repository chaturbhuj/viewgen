<?php

require_once('db.php');

function authenticate() {

	global $AUTHENTICATED;
	
	if (isset($AUTHENTICATED) && $AUTHENTICATED) return true;
	
    global $BASE_URI;
	global $_USER_DATA;
	global $_COMPANY_DATA;

	if (isset($_SESSION['auth']) && ($_SESSION['auth']['user_id'] == 0 && $_SESSION['auth']['company_id'] == 0)) {
		unset($_SESSION['auth']);
		session_destroy();
		return false;
	}

	if (!isset($_SESSION['auth']) || ($_SESSION['auth']['user_id'] == 0 && $_SESSION['auth']['company_id'] == 0)) {
		return false;
	}

	$_USER_DATA = array();
	$_COMPANY_DATA = array();
	if ($_SESSION['auth']['user_id'] > 0) {
		$query = DB::prep('select * from user where id=?');
		$query->execute(array($_SESSION['auth']['user_id']));

		if ($query->rowCount() == 0) return false;

		$_USER_DATA = $query->fetch();

		if(isset($_GET["company_id"]) AND $_USER_DATA["admin"]){
			$query = DB::prep('select * from company where id=?');
			$query->execute(array($_GET["company_id"]));

			if ($query->rowCount() == 0) return false;

			$_COMPANY_DATA = $query->fetch();

			$query = DB::prep("select * from rc_account where company_id=?");
			$query->execute(array($_GET['company_id']));

			if ($query->rowCount() > 0) {
				$_COMPANY_DATA['account'] = $query->fetch();
			}/* else {
				$_COMPANY_DATA['account_type'] = 'free';
			}*/
		}
		if ($_SERVER['REQUEST_URI'] == $BASE_URI."/") {
			redirect("adminDashboard.php");
		}
	}

	$companyIsAutenticated = false;
	if ($_SESSION['auth']['company_id'] > 0) {
		$query = DB::prep('select * from company where id=?');
		$query->execute(array($_SESSION['auth']['company_id']));

		if ($query->rowCount() == 0) return false;

		$_COMPANY_DATA = $query->fetch();

		$_USER_DATA["user_name"] = $_COMPANY_DATA["company_name"];
		$_USER_DATA["company_id"] = $_COMPANY_DATA["id"];
		$_USER_DATA["admin"] = 0;

		// Fetch billing information.
		$query = DB::prep("select * from rc_account where company_id=?");
		$query->execute(array($_SESSION['auth']['company_id']));

		if ($query->rowCount() > 0) {
			$_COMPANY_DATA['account'] = $query->fetch();
		} else {
			$_COMPANY_DATA['account_type'] = 'free';
		}
		
		$companyIsAutenticated = true;

		if ($_COMPANY_DATA['state'] == "draft" && !isset($AUTH_DRAFT_OK)) {
			return false;
		}

		$_GET['company_id'] = $_SESSION['auth']['company_id'];

		if ($_SERVER['REQUEST_URI'] == $BASE_URI."/") {
			redirect("dashboard.php?company_id=".$_USER_DATA['company_id']);
		} 
	}

	if (isset($_GET['campaign_id']) and $_GET['campaign_id'] != '' and !$_USER_DATA['admin']) {
		// Check that the company owns the campaign.
		$query = DB::prep("select * from campaign where id=?");
		$query->execute(array($_GET['campaign_id']));

		if ($query->rowCount() > 0) {
			$campaign = $query->fetch();
			if ($campaign['company_id'] != $_COMPANY_DATA['id']) {
				return false;
			}
		} else {
			return false;
		}
	}

	if (isset($_GET['company_id']) and $_GET['company_id'] != $_SESSION['auth']['company_id'] and $_SESSION['auth']['company_id'] > 0) {
		if (isset($WRONG_COMPANY_ERROR)) echo $WRONG_COMPANY_ERROR;
		else return false;
		die;
	}

    $AUTHENTICATED = true;
	
	return true;
}

?>
