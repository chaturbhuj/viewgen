<?php


require_once('../header.php');
require_once("../shared_functions.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$query = "
	SELECT
		campaign.created,
		DATE_FORMAT(campaign.created,'%Y-%m') month,
		count(*) cnt
	FROM search_url
	JOIN campaign ON campaign.id = search_url.campaign_id
	WHERE 
		campaign.status = 'Finished'
	GROUP BY campaign_id
	ORDER BY campaign.created
	;";
$result = mysql_query($query);

$totalVisits = 0;
$totalQueries = 0;
while($data = mysql_fetch_array($result)){
	$data["month"] = $data["month"]."-01";
	
	$totalVisits +=$data["cnt"];
	$totalQueries ++;
	if (!isset($queriesPerMonth[$data["month"]])) {
		$visitsPerMonth[$data["month"]] = 0;
		$queriesPerMonth[$data["month"]] = 0;
	}
	$visitsPerMonth[$data["month"]] += $data["cnt"];
	$queriesPerMonth[$data["month"]] ++;
}
$graphData = array();
foreach ($visitsPerMonth as $key => $value) {
	$graphData[] = array($key,$queriesPerMonth[$key],round($visitsPerMonth[$key]/$queriesPerMonth[$key],2));
}
echo makeGraph($graphData,"100%","400px",array("Campaign Count","URL Queries per Campaign"));
?>