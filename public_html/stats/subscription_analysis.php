<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once('../autoLoader.php');
require_once('../header.php');
require_once("../shared_functions.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$graph = new SubscriptionAnalysis1Graph();
$graph->recalculate(date("Y-m-d", time() - 3600 * 24 * 100), date("Y-m-d"));
echo "<h1>" . $graph->getTitle() . "</h1>";
echo $graph->getOutput();


?>
