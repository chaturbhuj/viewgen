<?php


require_once('../header.php');
require_once("../shared_functions.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$query = "
	SELECT
		search_url.created,
		DATE_FORMAT(search_url.created,'%Y-%m') month,
		count(*) cnt
	FROM people
	JOIN search_url ON search_url.id = people.search_url_id
	WHERE 
		crawled_by_id != 0 AND
		search_url.finished = 1 AND
		search_url.created > '0000-00-00 00:00:00'
	GROUP BY search_url_id
	ORDER BY search_url.created
	;";
$result = mysql_query($query);

$totalVisits = 0;
$totalQueries = 0;
while($data = mysql_fetch_array($result)){
	$data["month"] = $data["month"]."-01";
	
	$totalVisits +=$data["cnt"];
	$totalQueries ++;
	if (!isset($queriesPerMonth[$data["month"]])) {
		$visitsPerMonth[$data["month"]] = 0;
		$queriesPerMonth[$data["month"]] = 0;
	}
	$visitsPerMonth[$data["month"]] += $data["cnt"];
	$queriesPerMonth[$data["month"]] ++;
}
$graphData = array();
foreach ($visitsPerMonth as $key => $value) {
	$graphData[] = array($key,$queriesPerMonth[$key],round($visitsPerMonth[$key]/$queriesPerMonth[$key],2));
}
echo makeGraph($graphData,"100%","400px",array("Query URL Count","Visits per Query"));
?>