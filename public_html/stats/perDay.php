<?php


require_once('../header.php');
require_once("../shared_functions.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$query = "
	SELECT
		DATE_FORMAT(sent_to_crawler_date,'%a') dayOfWeek,
		count(*) cnt
	FROM people 
	WHERE crawled_by_id != 0
	GROUP BY DATE_FORMAT(sent_to_crawler_date,'%a')
	ORDER BY DATE_FORMAT(sent_to_crawler_date,'%w')
	;";
$result = mysql_query($query);

$totalVisits = 0;
while($data = mysql_fetch_array($result)){
	$totalVisits +=$data["cnt"];
	$valuesPerDay[$data["dayOfWeek"]] = $data["cnt"];
}


?>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
  "theme": "light",
  "dataProvider": [ 
<?
	$first = true;
	foreach ($valuesPerDay as $key => $value) {
		if ($first) {
			$first = false;
		} else {
			echo ",\n";
		}
		echo '{
		"country": "'.$key.'",
		"visits": '.round($value/$totalVisits,4)*100 .'
		}';
	}
?>
  ],
  "valueAxes": [ {
    "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
  } ],
  "gridAboveGraphs": true,
  "startDuration": 1,
  "graphs": [ {
    "balloonText": "[[category]]: <b>[[value]]%</b>",
    "fillAlphas": 0.8,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "visits"
  } ],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "country",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "tickPosition": "start",
    "tickLength": 20
  },
  "export": {
    "enabled": true
  }

} );
</script>

<style>
#chartdiv {#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}	
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}					
</style>
<div id="chartdiv"></div>		