<?php

include('../db.php');

$query = DB::prep("SELECT name, location, premium FROM people WHERE sent_to_crawler_date >= '2016-01-01' AND
	sent_to_crawler_date < '2016-02-01';");
$query->execute();

$visitsThisPeriod = [];
$visitsWithPremiumThisPeriod = [];
while ($row = $query->fetch()) {
	$hash = md5($row['name'] . $row['location']);
	
	if (!isset($visitsThisPeriod[$hash])) {
		$visitsThisPeriod[$hash] = $row['name'];
	}
	if ($row['premium']) {
		$visitsWithPremiumThisPeriod[$hash] = $row['name'];
	}
}

echo "Got " . count($visitsThisPeriod) . " people this period\n";

$query = DB::prep("SELECT name, location, premium FROM people WHERE sent_to_crawler_date >= '2015-01-01' AND
	sent_to_crawler_date < '2015-02-01';");
$query->execute();

$visitsLastPeriod = [];
$visitsWithPremiumLastPeriod = [];
while ($row = $query->fetch()) {
	$hash = md5($row['name'] . $row['location']);
	if (!isset($visitsLastPeriod[$hash])) {
		$visitsLastPeriod[$hash] = $row['name'];
	}
	if ($row['premium']) {
		$visitsWithPremiumLastPeriod[$hash] = $row['name'];
	}
}

$query = DB::prep("SELECT name, location, premium FROM people WHERE sent_to_crawler_date >= '2015-01-01' AND
	sent_to_crawler_date < '2016-02-01';");
$query->execute();

$visitsTotalPeriod = [];
while ($row = $query->fetch()) {
	$hash = md5($row['name'] . $row['location']);
	if (!isset($visitsTotalPeriod[$hash])) {
		$visitsTotalPeriod[$hash] = 0;
	}
	$visitsTotalPeriod[$hash]++;
}
echo "Got " . count($visitsTotalPeriod) . " people total period\n";

$diff = array_intersect_assoc($visitsThisPeriod, $visitsLastPeriod);

echo "Sample size: " . count($diff) . "\n";

// Divide in to three groups.
$groups = [];

/*$groups = [
	1 : [],
	2 : [],
	3 : []
];

foreach ($diff as $hash => $name) {
	if ($visitsTotalPeriod[$hash] <= 2 and $visitsTotalPeriod[$hash] > 1) {
		$groups[1][] = $hash;
	} elseif ($visitsTotalPeriod[$hash] <= 10) {
		$groups[2][] = $hash;
	} else {
		$groups[3][] = $hash;
	}
}*/

for ($i = 2; $i <= 11 ; $i++) {
	$groups[$i] = [];
}

foreach ($diff as $hash => $name) {
	if ($visitsTotalPeriod[$hash] > 30) continue;
	$groups[1 + ceil($visitsTotalPeriod[$hash] / 3)][] = $hash;
}

foreach ($groups as $groupId => $group) {
	echo "Group $groupId: " . count($group) . "\n";
}

// generate report.

foreach ($groups as $groupId => $group) { 
	$numberThatGotPremiumDuringPeriod = 0;
	$numWithSubscriptionLastPeriod = 0;
	$numWithSubscriptionThisPeriod = 0;
	foreach ($group as $hash) {
		if (isset($visitsWithPremiumLastPeriod[$hash])) {
			$numWithSubscriptionLastPeriod++;
		}
		if (isset($visitsWithPremiumThisPeriod[$hash])) {
			$numWithSubscriptionThisPeriod++;
		}
		if (!isset($visitsWithPremiumLastPeriod[$hash]) && isset($visitsWithPremiumThisPeriod[$hash])) {
			$numberThatGotPremiumDuringPeriod++;
		}
	}
	//echo "\nnumWithSubscriptionLastPeriod = $numWithSubscriptionLastPeriod\n";
	//echo "numWithSubscriptionThisPeriod = $numWithSubscriptionThisPeriod\n";
	//echo "numberThatGotPremiumDuringPeriod = $numberThatGotPremiumDuringPeriod\n";
	//var_dump($numberThatGotPremiumDuringPeriod / count($group));
	echo ($numberThatGotPremiumDuringPeriod / count($group)) . "," . "\n";
}

?>
