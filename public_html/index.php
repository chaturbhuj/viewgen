<?php
require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/services/text.php';
use function \services\text\join_paths;

require __DIR__ . '/config.php';

include('force_https.php');
include('authenticate.php');

function content_map(string $extension){
  return [
      'txt' => 'Content-Type: text/plain',
      'html' => 'Content-Type: text/html',
      
      'css' => 'Content-Type: text/css',
      "mp4" => 'Content-Type: video/mp4',
      "webm" => 'Content-Type: video/webm',
      "js" => 'Content-Type: application/js',
      "zip" => 'Content-Type: application/zip',
      "xml" => 'Content-Type: application/xml',
      'jpg' => 'Content-Type: image',
      'png' => 'Content-Type: image',
      'gif' => 'Content-Type: image',
      
      'svg' => 'image/svg+xml',
      
      'woff' => 'application/font-woff',
      'ttf' => 'application/font-sfnt',
      'eot' => 'application/vnd.ms-fontobjec',
          
  ][$extension] ?? 'application/octet-stream';  
} 


$ui_modules_mapping = [
    //                 dir          redirect on authenticatied
    'sign_up' => ['ui/sign_up', false],
    'login' => ['ui/login', true]
];

    
$uri = $_SERVER['REQUEST_URI'];

$fileAbsolutePath = "index_public.php";
if (preg_match('#\/([a-zA-Z0-9\.\/_-]+)#', $uri, $match)) {
	//security sanitization, to not allow step up to system folders
    $fileAbsolutePath = str_replace('..', '', $match[1]);
}
$fileAbsolutePath = __DIR__ . ($fileAbsolutePath[0] === '/' ? '' : '/') . $fileAbsolutePath;

$path_parts = pathinfo($fileAbsolutePath);
$extension = $path_parts['extension'] ?? '';
$name = $path_parts['filename'] ?? '';
$baseName = $path_parts['basename'] ?? '';
$path = $path_parts['dirname'] ?? '';

if (strpos($uri, "/client/installers") === 0) {
    $clientFilePath = $CONFIG['path']['client_files_base'] . $baseName;

    if (in_array($extension, ['exe', 'pkg'])){
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header(sprintf('Content-Disposition: attachment; filename="%s"', $baseName)); //<<< Note the " " surrounding the file name
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
    }else{
        header(content_map($extension));
    }
    $content = file_get_contents($clientFilePath);
    header('Content-Length: ' . strlen($content));
    echo $content;
    die;
}

if (
    //assets should be no-auth
    strpos($uri, "/assets/") === 0 || 
    strpos($uri, "/ui/assets/") === 0 || 
    strpos($uri, "/client_ui/") === 0 ||
    
    //installer binaries should be no-auth. Matters only for Mac at the moment, windows client use should_update.php
    strpos($uri, "/client/installers/") === 0 ||

    //you can include .public. in the PHP filename to denote it is not needed authorization
    ($extension === 'php' && strpos($name, ".public") !== false)  ||
    
    authenticate()
) {
    
    if ($extension === '') {
        //this might be a route
        $basePath = $fileAbsolutePath;
        $file = join_paths($basePath, 'index.php');
        
        if (file_exists($file)){
            chdir($basePath);
            include($file);
            die;
        }
    } else if (file_exists($fileAbsolutePath) && $name[0] != '.') {
        if ($extension === 'php') {
            // Set include path.
            chdir($path);
            include($fileAbsolutePath);
        } else {

            header(content_map($extension));

			$fp = fopen($fileAbsolutePath, "r");
			fpassthru($fp);
		}
		die;
	};

	header("HTTP/1.0 404 Not Found");
	echo "<h1>404 Not Found</h1>";
	die;
}

//global $AUTHENTICATED;

$uri = preg_replace('#\?.*#is', '', $uri);

if ($uri == "/") {
	include(__DIR__ . '/ui/landing/index.php');

//ui routes handling
} elseif (strpos($uri, "/about") === 0) {
    include(__DIR__ . '/ui/landing/about.php');
} elseif (strpos($uri, "/terms") === 0) {
    include(__DIR__ . '/ui/landing/terms.php');
} elseif (strpos($uri, "/gdpr") === 0) {
    include(__DIR__ . '/ui/landing/gdpr.php');

//ui routes handling
} elseif (strpos($uri, "/ui/") === 0) {
    preg_match('/^\/ui\/([\w\-]+)\/?([\w\-]+)?/', $uri, $matches);
    $module = $matches[1];
    $file = (isset($matches[2]) && $matches[2] !== '') ? 
        ($matches[2] . '.php') : 'index.php';
    
	if (! isset($ui_modules_mapping[$module])){
        header("HTTP/1.0 404 Not Found");
       	echo "<h1>404 Not Found</h1>";
       	die;
    }

//    if ($AUTHENTICATED && $ui_modules_mapping[$url][1]){
//	    redirect("dashboard.php?company_id=".$_USER_DATA['company_id']);
//    };
    
    chdir($ui_modules_mapping[$module][0]);
	
	include($file);
	
} elseif (strpos($uri, "/login.php") === 0) {
	include("login.php");
} elseif (strpos($uri, "/requestNewPassword.php") === 0) {
	include("requestNewPassword.php");
} elseif (strpos($uri, "/reset_password.php") === 0) {
	include("reset_password.php");
} elseif (strpos($uri, "/alert_unsubscribe.php") === 0) {
    include("alert_unsubscribe.php");
} elseif (strpos($uri, "/client/download.php") === 0) {
	chdir ('client');
	include("download.php");
} elseif (strpos($uri, "/api/") === 0) {
	chdir ('api');
    $parts = explode("/", $uri);
	include($parts[count($parts)-1]);
} elseif (strpos($uri, "/assets/js/app.js") === 0) {
	echo file_get_contents("assets/js/app.js");
} elseif (strpos($uri, "/assets/images/cpt.png") === 0) {
	echo file_get_contents("assets/images/cpt.png");
} elseif (strpos($uri, "/sign_up.php") === 0) {
	include("sign_up.php");
} elseif (strpos($uri,"payment/confirm.php") !== false) {
	include("payment/confirm.php");
} elseif ($uri == "/assets/js/recurly.min.js") {
	include("assets/js/recurly.min.js");
} elseif ($uri == "/lib/recurly-theme/default/recurly.css") {
	header("Content-type: text/css");
	include("lib/recurly-theme/default/recurly.css");	
} elseif ($uri == "/assets/js/jquery.min.js") {
	include("assets/js/jquery.min.js");	
} elseif ($uri == "/assets/vendor/bootstrap3/css/bootstrap.min.css") { // behövs inte?
header("Content-type: text/css");
	include("assets/vendor/bootstrap3/css/bootstrap.min.css");	
} elseif ($uri == "/lib/owl/owl-carousel/owl.carousel.css") { // behövs inte?
header("Content-type: text/css");
	include("lib/owl/owl-carousel/owl.carousel.css");	
} elseif ($uri == "/lib/owl/owl-carousel/owl.theme.css") { // behövs inte?
header("Content-type: text/css");
	include("lib/owl/owl-carousel/owl.theme.css");
} elseif ($uri == "/assets/css/grid.css") { // behövs inte?
header("Content-type: text/css");
	include("assets/css/grid.css");	
} elseif ($uri == "/assets/js/app.js?46") { // behövs inte?
	include("assets/js/app.js");	
} elseif ($uri == "/assets/js/slider.js?46") { // behövs inte?
	include("assets/js/slider.js");	
} elseif ($uri == "/lib/owl/owl-carousel/owl.carousel.js") { // behövs inte?
	include("lib/owl/owl-carousel/owl.carousel.js");	
} elseif ($uri == "/assets/css/index.css?46") { // behövs inte?
	header("Content-type: text/css");
	include("assets/css/index.css");	
	
} elseif ($uri == "/robots.txt") {
	include('robots.txt');
} elseif (strpos($uri,"payment/push.php") !== false) {
    include("payment/push.php");
} else {
	$uriWithQueryString = $uri;
	if ($_SERVER['QUERY_STRING']) {
		$uriWithQueryString .= '?' . $_SERVER['QUERY_STRING'];
	}
	header("Location: /ui/login/?redirect=" . urlencode($uriWithQueryString));
	die;
}

// Check if the user was authenticated.

?>
