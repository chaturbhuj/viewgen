<?php
try {
    require_once(__DIR__.'/db.php');
} catch (Exception $e) {
    throw new Exception('Database connection error.');
}
function ajax_response($status, $payload = array()) {
	echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

require_once("shared_functions.php");
if (!isset($_GET['company_id'])) {
	ajax_response("error");
}

if (!isset($_GET['user_id'])) {
	ajax_response("error");
}

if ($_POST['action'] != "get") {
	ajax_response("error");
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND id = :user_id");
$query->execute([
	'company_id' => $_GET['company_id'],
	'user_id' => $_GET['user_id']
]);
if ($query->rowCount() == 0) {
	ajax_response("error");
}
$user = $query->fetch();

$query = DB::prep("SELECT * FROM campaign WHERE company_id = :company_id AND visit_by_id = :li_user_id");
$query->execute([
	'company_id' => $_GET['company_id'],
	'li_user_id' => $user['linked_in_id']
]);

$payload = [];
while ($campaign = $query->fetch()) {
	$payload['campaign_visits_' . $campaign['id']] = $campaign['campaign_visit_count'];
}

$payload['software_status'] = '<i class="status-icon ' . $user['software_status'] . '"></i>';

$query = DB::prep("select 
	*,
	(SELECT name FROM campaign WHERE campaign.id=li_users.campaign_id) as campaignName,
	visitsLast24Hours AS visitCount,
	invitesLast24Hours AS invitesCount,
	(limit_reached_at > '0000-00-00 00:00:00' AND limit_reached_at > date_sub(NOW(), interval 1 hour)) limitReached,
	(invitesLast24Hours >= invites24HoursLimit) invitesLimitReached
	from li_users 
	where company_id=?
	AND li_users.id = ?
	and hidden = 0");
	$query->execute(array($_GET['company_id'], $_GET['user_id']));

$status = $query->fetch();

$payload['software_status_text'] =
	($status["visitCount"] > 0 ? "($status[visitCount] visits last 24h) &nbsp;": "")
	.($status["invitesCount"] > 0 ? "($status[invitesCount] invites last 24h ".($status['invitesLimitReached'] ? 'limit reached':'').") &nbsp;": "")
	.ucfirst(
		$status['software_status'] == 'waiting' ? (
			($status['visitCount'] >= $status['max_visits_per_day'] or $status['limitReached']) ? '24h limit reached' : 'Waiting for Campaign'
		) : $status['software_status']
	)
	.(trim($status['software_status'])=='running' ? ' "'.make_short($status['campaignName'],20).'"': "");

ajax_response("success", $payload);

?>
