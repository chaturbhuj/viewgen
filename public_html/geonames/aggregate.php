<?php

include("../db.php");

//$query = DB::prep("truncate table postal_code_aggregate");
//$query->execute();

$insert = DB::prep("INSERT INTO postal_code_aggregate (`country code`, `admin name1`, `admin name2`, `admin name3`, `place name`, latitude, longitude, zipHits, aggregateType) values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
/*
$query = DB::prep("select *, `country code`,count(*) as zipHits from postal_code where postal_code_id > 2238442 GROUP BY `country code` ");
$query->execute();

while ($row = $query->fetch()) {
	if (trim($row['country code']) == "") continue;
	$insert->execute([
		$row['country code'],
		"",
		"",
		"",
		"",
		$row['longitude'],
		$row['latitude'],
		$row['zipHits'],
		"country"
	]);
}

$query = DB::prep("select *, `country code`,count(*) as zipHits from postal_code where postal_code_id > 2238442 GROUP BY `admin name1`, `country code`");
$query->execute();

while ($row = $query->fetch()) {
	if (trim($row['admin name1']) == "") continue;
	$insert->execute([
		$row['country code'],
		$row['admin name1'],
		"",
		"",
		"",
		$row['longitude'],
		$row['latitude'],
		$row['zipHits'],
		"admin1"
	]);
}

$query = DB::prep("select *,count(*) as zipHits from postal_code where postal_code_id > 2238442 GROUP BY `admin name2`,`admin name1`, `country code` HAVING zipHits>1");
$query->execute();

while ($row = $query->fetch()) {
	if (trim($row['admin name2']) == "") continue;
	$insert->execute([
		$row['country code'],
		$row['admin name1'],
		$row['admin name2'],
		"",
		"",
		$row['longitude'],
		$row['latitude'],
		$row['zipHits'],
		"admin2"
	]);
}

$query = DB::prep("select *,count(*) as zipHits from postal_code where postal_code_id > 2238442 GROUP BY `admin name3`,`admin name2`,`admin name1`, `country code`  HAVING zipHits>1");
$query->execute();

while ($row = $query->fetch()) {
	if (trim($row['admin name3']) == "") continue;
	$insert->execute([
		$row['country code'],
		$row['admin name1'],
		$row['admin name2'],
		$row['admin name3'],
		"",
		$row['longitude'],
		$row['latitude'],
		$row['zipHits'],
		"admin3"
	]);
}
*/
$query = DB::prep("select *,count(*) as zipHits from postal_code where postal_code_id > 2238442 GROUP BY `place name`,`admin name3`,`admin name2`,`admin name1`, `country code`  HAVING zipHits>1");
$query->execute();

while ($row = $query->fetch()) {
	if (trim($row['place name']) == "") continue;
	$insert->execute([
		$row['country code'],
		$row['admin name1'],
		$row['admin name2'],
		$row['admin name3'],
		$row['place name'],
		$row['longitude'],
		$row['latitude'],
		$row['zipHits'],
		"place"
	]);
}

?>
