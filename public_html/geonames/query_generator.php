<?

ini_set('display_errors', 1);

if (!isset($_GET['company_id'])) {
	die;
}

function unparse_url($parsed_url) { 
  $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
  $host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
  $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
  $user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
  $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
  $pass     = ($user || $pass) ? "$pass@" : ''; 
  $path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
  $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
  $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
  return "$scheme$user$pass$host$port$path$query$fragment"; 
} 

if (!isset($_POST['type'])) {
	$_POST['type'] = 'Keywords';
}

require_once('../authenticate.php');

DB::get_instance()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$outputUrls = [];
$resultingCodes = [];
$rawLocations = [];
$maxDistance = 0;
$centerLat = 0;
$centerLong = 0;

if (isset($_POST['action']) && $_POST['action'] == "generate" && @$_POST['baseUrl'] != "") {

	$items = explode("\n", trim($_POST['items']));

	$urlParts = parse_url($_POST['baseUrl']);
	if ($urlParts === false) {
		echo "Invalid base url";
		die;
	}

	foreach ($items as $item) {
		$item = trim($item);
		$queryStr = parse_str($urlParts['query'], $arr);
		if ($_POST['type'] == "Keywords") {
			$arr['keywords'] = $item;
		} elseif ($_POST['type'] == "Title") {
			$arr['title'] = $item;
		} elseif ($_POST['type'] == "First Name") {
			$arr['firstName'] = $item;
		} elseif ($_POST['type'] == "School") {
			$arr['school'] = $item;
		}
		$urlParts['query'] = http_build_query($arr);
		$outputUrls[] = unparse_url($urlParts);
	}
}


require_once('../header.php');

?>
</div>
<div class="outer-page" style="padding-left: 20px; font-size: 19px;">
<style>

.geobuilder {
margin-top: 30px;
width: 100%;
}

.geobuilder .base_url {
	width: 98%;
	display: block;
}

.geobuilder .editor_box {
margin: 40px 0 0 0;
padding-bottom: 52px;
width: 800px;
}

.geobuilder .suggestions {
display: block;
margin-top: -52px;
width: 800px
}

.geobuilder .suggestions input {
width: 770px;
}

.geobuilder .suggestions ul.box {
width: 800px;
}


.geobuilder .select-size-label { 
margin: 40px 0 0 30px;
display: inline-block;
float: none;
}
.geobuilder .select-name {
margin: 0 0 0 30px;
float: none;
display: block;
}

.geobuilder .select-url-limit-label { 
margin: 40px 0 0 30px;
display: inline-block;
float: none;
}
.geobuilder .select-url-limit {
margin: 0 0 0 30px;
float: none;
display: block;
}

.geobuilder .geo-section {
float: left;
width: 167px;
}

.geobuilder button {
float: left;
margin-top: 37px;
width: 145px;
height: 60px;
font-size: 22px;
margin-left: 15px;
}

.geobuilder textarea {
margin-top: 20px;
width: 100%;
}

#map-canvas {
height: 800px;
}

.geobuilder .editor_box h3 {
display: none;
}

.geobuilder .editor_box .add_location {
display: none;
}

.geobuilder button.reset-settings {
font-size: 14px;
width: 120px;
height: 30px;
margin-top: 50px;
opacity: 0.6;
}

.geobuilder button.create-camp {
width: 310px;
font-size: 14px;
margin: 15px 0;
float: right;
}

.geobuilder textarea.generate {
width: 50%;
height: 300px;
float: left;
}

</style>
<div class="geobuilder">
	<form method="post">
		<input class="base_url" type="text" name="baseUrl" placeholder="URL from LinkedIn" value="<?=@$_POST['baseUrl']?>">
		<textarea name="items" class="generate"></textarea>
		
		<div class="geo-section">
				<label for="selectSize" class="select-size-label">Type</label>
				<select class="select-name" id="selectSize" name="type">
					<?php
						$types = ['Keywords', 'Company', 'Title', 'First Name', 'Last Name', 'School'];
						foreach ($types as $type) {
							printf('<option value="%s"%s>%s</option>', $type, $type == $_POST['type'] ? ' selected' : '', $type);
						}
					?>
				</select>
		</div>
		<button type="submit" name="action" value="generate">Generate</button>
	</form>
	<div class="clear"></div>
	<? if (count($outputUrls) > 0) { ?>
		<form method="post" action="/create_campaign.php?company_id=<?=$_GET['company_id']?>">
			<textarea name="query_urls" style="height: 400px"><?=implode("\n", $outputUrls)?></textarea>
			<button type="submit" formtarget="_blank" class="create-camp">Create Campaign Based on these URLs</button>
		</form>
	<? } ?>

</div>
