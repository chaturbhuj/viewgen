<?
$HTML_TITLE = "Query Slicer";
ini_set('memory_limit', '2048M');
$ACTIVE_HEADER_PAGE = "dashboard";
//ini_set('display_errors', 1);
$freeVersion = false;
if (!isset($_GET['company_id'])) {
	$freeVersion = true;
}

function unparse_url($parsed_url) { 
  $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
  $host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
  $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
  $user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
  $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
  $pass     = ($user || $pass) ? "$pass@" : ''; 
  $path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
  $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
  $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
  return "$scheme$user$pass$host$port$path$query$fragment"; 
} 

if (!isset($_POST['size'])) {
	$_POST['size'] = 50;
}

if (!isset($_POST['urlLimit'])) {
	$_POST['urlLimit'] = 200;
}



DB::get_instance()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$outputUrls = [];
$resultingCodes = [];
$rawLocations = [];
$maxDistance = 0;
$centerLat = 0;
$centerLong = 0;
if (isset($_POST['action']) && $_POST['action'] == "generate") {

	$rawLocations = array_keys(isset($_POST['location']) ? $_POST['location'] : []);
	$locations = [];
	foreach ($rawLocations as $loc) {
		$locations[] = (int)$loc;
	}

	if (count($locations) > 0 and @$_POST['baseUrl'] != "") {

		$postalCodes = [];
		foreach ($locations as $i => $locationId) {
			$query = DB::prep("SELECT * FROM postal_code_aggregate WHERE postal_code_aggregate_id = ?");
			$query->execute([$locationId]);

			if ($query->rowCount() == 0) continue;
			$row = $query->fetch();

			$parts = [];
			if ($row['aggregateType'] == "country") {
				$parts[] = $row['countryName'];
				$parts[] = "(" . $row['country code'] . ")";
			}
			if ($row['aggregateType'] == "huge_region") {
				$parts[] = $row['place name'];
			}
			if ($row['aggregateType'] == "admin1") {
				$parts[] = $row['admin name1'];
				$parts[] = $row['country code'];
			}
			if ($row['aggregateType'] == "admin2") {
				$parts[] = $row['admin name2'];
				$parts[] = $row['admin name1'];
				$parts[] = $row['country code'];
			}
			if ($row['aggregateType'] == "admin3") {
				$parts[] = $row['admin name3'];
				$parts[] = $row['admin name2'];
				$parts[] = $row['admin name1'];
				$parts[] = $row['country code'];
			}
			if ($row['aggregateType'] == "place") {
				$parts[] = $row['place name'];
				$parts[] = $row['admin name3'];
				$parts[] = $row['admin name2'];
				$parts[] = $row['admin name1'];
				$parts[] = $row['country code'];
			}
			$nameParts = [];
			foreach ($parts as $part) {
				if (trim($part) == "") continue;
				$nameParts[] = $part;
			}
			$name = implode(", ", $nameParts);
			$rawLocations[$i] = ["name" => $name, "id" => $locationId, "zipCount" => $row['zipHits']];

			if ($row['aggregateType'] == "country") {
				$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` FROM postal_code WHERE `country code` = :country GROUP BY `postal code` ORDER BY `longitude`");
				$query->execute([
					'country' => $row['country code']
				]);
			}
			if ($row['aggregateType'] == "huge_region") {
				if ($row['place name'] == "Western United States") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` = 'US' AND 
						`admin name1` IN ('California', 'Washington', 'Arizona', 'Colorado', 'Utah', 'Oregon', 'Nevada', 'Idaho', 'Montana', 'Wyoming', 'New Mexico') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Midwest United States") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` = 'US' AND 
						`admin name1` IN ('Illinois', 'Indiana', 'Michigan', 'Ohio', 'Minnesota', 'Wisconsin', 'Missouri', 'Kansas', 'Iowa', 'North Dakota', 'South Dakota', 'Nebraska') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Northeast United States") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` = 'US' AND 
						`admin name1` IN ('New York', 'Massachusetts', 'Pennsylvania', 'New Jersey', 'Connecticut', 'Rhode Island', 'Vermont', 'Maine', 'New Hampshire') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				
				if ($row['place name'] == "Central United States") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` = 'US' AND 
						`admin name1` IN ('Texas', 'Oklahoma', 'Arkansas', 'Louisiana', 'Mississippi', 'Alabama', 'Tennessee', 'Kentucky') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Southeast United States") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` = 'US' AND 
						`admin name1` IN ('Florida', 'Georgia', 'Virginia', 'North Carolina', 'Washington D.C.', 'Maryland', 'Delaware', 'South Carolina', 'West Virginia') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Western Europe") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` IN ('DE', 'GB', 'FR', 'IT', 'ES', 'NL', 'CZ', 'SE', 'BE', 'NO', 'AT', 'DK', 'FI', 'IE', 'PT', 'GR') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Eastern Europe") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` IN ('AM', 'BY', 'BA', 'BG', 'CY', 'EE', 'GE', 'HU', 'HR', 'LT', 'LV', 'MK', 'PO', 'RO', 'SI', 'SK', 'UA', 'PL', 'MD') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Africa") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` IN ('DZ', 'AO', 'SH', 'BJ', 'BW', 'BF', 'BI', 'CM', 'CV', 'CF', 'TD', 'KM', 'CG', 'DJ', 'EG', 'GQ', 'ER', 'ET', 'GA', 'GM', 'GH', 'GW', 'GN', 'CI', 'KE', 'LS', 'LR', 'LY', 'MG', 'MW', 'ML', 'MR', 'MU', 'YT', 'MA', 'MZ', 'NA', 'NE', 'NG', 'ST', 'RE', 'RW', 'ST', 'SN', 'SC', 'SL', 'SO', 'ZA', 'SH', 'SD', 'SZ', 'TZ', 'TG', 'TN', 'UG', 'CD', 'ZM', 'TZ', 'ZW', 'SS', 'CD') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "Asia") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` IN ('AF', 'AM', 'AZ', 'BH', 'BD', 'BT', 'BN', 'KH', 'CN', 'CX', 'CC', 'IO', 'GE', 'HK', 'IN', 'ID', 'IR', 'IQ', 'IL', 'JP', 'JO', 'KZ', 'KP', 'KR', 'KW', 'KG', 'LA', 'LB', 'MO', 'MY', 'MV', 'MN', 'MM', 'NP', 'OM', 'PK', 'PH', 'QA', 'SA', 'SG', 'LK', 'SY', 'TW', 'TJ', 'TH', 'TR', 'TM', 'AE', 'UZ', 'VN', 'YE', 'PS') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				if ($row['place name'] == "South America") {
					$query = DB::prep("SELECT latitude, longitude, `country code`, `postal code`, `admin name1` 
					FROM postal_code 
					WHERE 
						`country code` IN ('AR', 'BO', 'BR', 'CL', 'CO', 'EC', 'FK', 'GF', 'GY', 'GY', 'PY', 'PE', 'SR', 'UY', 'VE') 
					GROUP BY `postal code` ORDER BY `longitude`");
					$query->execute();
				}
				
			}
			if ($row['aggregateType'] == "admin1") {
				$query = DB::prep("SELECT * FROM postal_code WHERE `admin name1` = :admin1 and `country code` = :country GROUP BY `postal code` ORDER BY `longitude`");
				$query->execute([
					'admin1' => $row['admin name1'],
					'country' => $row['country code']
				]);
			}
			if ($row['aggregateType'] == "admin2") {
				$query = DB::prep("SELECT * FROM postal_code WHERE `admin name2` = :admin2 and `admin name1` = :admin1  and `country code` = :country GROUP BY `postal code` ORDER BY `longitude`");
				$query->execute([
					'admin2' => $row['admin name2'],
					'admin1' => $row['admin name1'],
					'country' => $row['country code']
				]);
			}
			if ($row['aggregateType'] == "admin3") {
				$query = DB::prep("SELECT * FROM postal_code WHERE `admin name3` = :admin3 and `admin name2` = :admin2 and `admin name1` = :admin1  and `country code` = :country GROUP BY `postal code` ORDER BY `longitude`");
				$query->execute([
					'admin3' => $row['admin name3'],
					'admin2' => $row['admin name2'],
					'admin1' => $row['admin name1'],
					'country' => $row['country code']
				]);
			}
			if ($row['aggregateType'] == "place") {
				$query = DB::prep("SELECT * FROM postal_code WHERE `place name` = :placeName and `admin name3` = :admin3 and `admin name2` = :admin2 and `admin name1` = :admin1  and `country code` = :country GROUP BY `postal code` ORDER BY `longitude`");
				$query->execute([
					'placeName' => $row['place name'],
					'admin3' => $row['admin name3'],
					'admin2' => $row['admin name2'],
					'admin1' => $row['admin name1'],
					'country' => $row['country code']
				]);
			}
		
			while ($row2 = $query->fetch()) {
				$postalCodes[] = $row2;
			}
		}

		$sizeModifier = 1;
		/*if ($_POST['size'] <= 25 and count($postalCodes) > 10000) {
			$sizeModifier = 4;
		}*/

		$currentCode = null;
		$eatenCodes = [];
		$numLoops = 0;
		$numCodes = count($postalCodes);
		$start = microtime(true);
		for ($i = 0; $i < $numCodes; $i++) {
			if (isset($eatenCodes[$i])) continue;
			$currentCode = $postalCodes[$i];

			if (!isset($currentCode['count'])) {
				$currentCode['count'] = 0;
				$currentCode['circleSize'] = $_POST['size'];
			}
			$debugCount = 0;
			for ($j = $i + 1; $j < $numCodes; $j++) {
				if (isset($eatenCodes[$j])) continue;
				if (abs($postalCodes[$j]['longitude'] - $currentCode['longitude']) > $_POST['size']*0.03) break;
				
				if (abs($postalCodes[$j]['latitude'] - $currentCode['latitude']) < $_POST['size']* .03 and abs($postalCodes[$j]['longitude'] - $currentCode['longitude']) < $_POST['size'] *.03) {
					
					if (abs($postalCodes[$j]['latitude'] - $currentCode['latitude']) + abs($postalCodes[$j]['longitude'] - $currentCode['longitude']) == 0) {
						$distance = 0;
					} else {
						
						$distance = distanceLatLong($postalCodes[$j]['latitude'], $postalCodes[$j]['longitude'], $currentCode['latitude'], $currentCode['longitude']);
						//echo "Lat:".abs($postalCodes[$j]['latitude'] - $currentCode['latitude']). " Long:".abs($postalCodes[$j]['longitude'] - $currentCode['longitude'])." Actual Distance:".$distance."<br>";
						//if ($debugCount>100) die;
						$debugCount++;
					}
					
					
					
					if ($distance <= $_POST['size'] * 1.5 * $sizeModifier) {
						// Eat this postal code.
						$currentCode['count']++;
						$eatenCodes[$j] = 1;
					}
				}
			}
			// Cannot eat any more.
			$resultingCodes[] = $currentCode;
		}
		usort($resultingCodes, function ($a, $b) {
			if ($a['count'] == $b['count']) return 0;
			return ($a['count'] < $b['count']) ? 1 : -1;
		});
		$resultingCodes = array_slice($resultingCodes, 0, $_POST['urlLimit']);

		// Find max distance
		foreach ($resultingCodes as $code1) {
			foreach ($resultingCodes as $code2) {
				//$distance = distanceLatLong($code1['latitude'], $code1['longitude'], $code2['latitude'], $code2['longitude']);
				$distance = abs($code1['longitude']-$code2['longitude'])+abs($code1['latitude']-$code2['latitude']);
				if ($distance > $maxDistance) {
					$centerLat = $code1['latitude'] + ($code2['latitude'] - $code1['latitude']) / 2;
					$centerLong = $code1['longitude'] + ($code2['longitude'] - $code1['longitude']) / 2;
					$maxDistance = $distance;
				}
			}
		}

		$urlParts = parse_url(trim($_POST['baseUrl']));
		if ($urlParts === false) {
			echo "Invalid base url";
			die;
		}

		foreach ($resultingCodes as $code) {
			$queryStr = parse_str($urlParts['query'], $arr);
			
			if (strpos($_POST['baseUrl'],'/sales/')) {
				$arr['countryCode'] = strtolower($code['country code']);
				$arr['radiusMiles'] = $_POST['size'];
				$arr['postalCode'] = $code['postal code'];
			} else {
				$arr['countryCode'] = strtolower($code['country code']);
				$arr['distance'] = $_POST['size'];
				$arr['postalCode'] = $code['postal code'];
			}
			
			
			if (isset($arr['f_G'])) {
				unset($arr['f_G']);
			}
			$arr['locationType'] = 'I';
			$urlParts['query'] = http_build_query($arr);
			$outputUrls[] = unparse_url($urlParts);
		}
	}
}
if ($freeVersion) {
	
	$IGNORE_AUTH = true;
	$STOPMENU = true;

	require_once(dirname(__FILE__) . "/../header.php");
	require_once(dirname(__FILE__) . "/../header_index.php");
} else {
	require_once(dirname(__FILE__) . '/../header.php');
}


?>
</div>
<div class="container geobuilder responsive">
	<div class="row">
		<div class="col-xs-12">
			<div class="subnav">
				<ul id="nav" class="secondary">
					<li>
						<a class="button small active" href="/geonames/find_location.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Query Slicer
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help" >
								<div>
									The Query Slicer cuts your large LinkedIn query into 25-200 smaller queries by city, county state or country, so that you spend less time building <1000-profile queries, and more time visiting profiles.
								</div>
							</i>
						</a>
					</li>
					<li>
						<a class="button small" href="/geonames/company_generator.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Company Query Generator
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									Company Query Generator turns lists of companies you provide into hundreds of well-targeted LinkedIn queries in just seconds
								</div>
							</i>
						</a>
					</li>
					
					<li>
						<a class="button small" href="/connections_of_connections.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Connections of Connections
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									Targeting the 1st-degree connections of specific people you’re already connected to - along with your core targeting parameters
								</div>
							</i>
						</a>
					</li>
					
					<li>
						<a class="button small" href="/title_suggestions.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Title Suggestions
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									On this page you find all the most common titles of the people you have visited. You can combine these titles with one or more URLs from LinkedIn
								</div>
							</i>
						</a>
					</li>
				
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">

			<h1 class="query-slicer-h1"><b>Q: What is the Query Slicer?</b></h1>
			<p>LinkedIn only lets you see and visit the first 1000 profiles for any one query. Query Slicer gets around that by automatically cutting large (1001-100000 profiles) queries into 25-200 smaller queries AND lets you target by any city, county, state or multiple non-U.S. countries.</p>
		</div>

			<form method="post">
			<div class="col-xs-8">
				<div class="editor_box collapsee opened">
					<input class="base_url" type="text" name="baseUrl" placeholder="Paste large LinkedIn Query URL here (one URL only)" value="<?=@$_POST['baseUrl']?>">
			
					<h3>Location:<i>*</i></h3>
					<? foreach ($rawLocations as $location) { ?>
						<input type="checkbox" name="location[<?=$location['id']?>]" id="loc_<?=$location['id']?>" checked="checked">
						<label class="radio" for="loc_<?=$location['id']?>"><?=$location['name']?> (<?=$location['zipCount']?>)</label>
					<? } ?>
					
					<span class="add_location"><i></i>Add location</span>
					<div class="suggestions">
						<input type="text" placeholder="Start typing location(s)">
						<i></i>
						<ul class="box"></ul>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="row">
					<div class="col-xs-6">
						<label for="selectSize" class="select-size-label">Within</label>
						<select class="select-name form-control" id="selectSize" name="size">
							<? $miles = [5, 10, 25, 35, 50, 75, 100]; ?>
							<? foreach ($miles as $mile) { ?>
								<option value="<?=$mile?>"<?=($mile == @$_POST['size']) ? ' selected' : ''?>><?=$mile?> mile<?=$mile == 1 ? '' : 's'?></option>
							<? } ?>
						</select>
					</div>
					<div class="col-xs-6">
						<label for="selectLimit" class="select-url-limit-label">URLs To Create</label>
						<select class="select-url-limit form-control" id="selectLimit" name="urlLimit">
							<? $numUrls = [10, 25, 50, 100, 200]; ?>
							<? foreach ($numUrls as $numUrl) { ?>
								<option value="<?=$numUrl?>"<?=($numUrl == @$_POST['urlLimit']) ? ' selected' : ''?>><?=$numUrl?></option>
							<? } ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">	
						<br/>
						<button type="submit" name="action" value="generate" class="btn btn-success btn-lg">Generate URLs</button>
					</div>
					<div class="col-xs-6">	
						<br/>
						<button type="button" onclick="document.location.href = document.location.href" class="btn btn-warning btn-lg">Reset</button>
					</div>
				</div>
			</div>
			</form>

	<form method="post" action="/create_campaign.php?company_id=<?=$_GET['company_id']?>&urls_created_with=query_slicer">
	<? if (count($outputUrls) > 0) { ?>

		<div class="col-xs-12">
			<? if(!$freeVersion){ ?>
				<div style = "width: 100%; float:left;">
					<br/>
					<br/>
					<button type="submit" formtarget="_blank" class="button green medium " style="border: none; width: 370px; font-size: 16px; margin-top:10px;margin-bottom:0;">Create Campaign Based on these URLs</button>
				</div>
			<? } ?>
		</div>
		<div class="col-xs-6">
			<input type="hidden" name="original_url" value="<?=$_POST["baseUrl"]?>">
			<textarea name="query_urls" class="form-control"><?=implode("\n", $outputUrls)?></textarea>
		</div>
		<div class="col-xs-6">

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMlNstUMKOamlydmb0uW6F7yPj576Zse8&v=3.exp&sensor=false"></script>
<script>
// This example creates circles on the map, representing
// populations in the United States.

// First, create an object containing LatLng and population for each city.
var citymap = {};
<?php

	$circleCount = count($resultingCodes);
	$nr = 0;
	foreach($resultingCodes as $code) {
		if ($code['circleSize'] == 0) {
			$code['circleSize'] = 0.5;
		}
		echo "
		citymap[".$nr."] = {
		  center: new google.maps.LatLng(".$code['latitude'].", ".$code['longitude']."),
		  radius: ".$code['circleSize']."
		};
		";
		$nr ++;
	}


$zoom = 10;
if ($maxDistance > 0) {
	$zoom = round(10/log($maxDistance+1));
} else {
	$centerLat = $resultingCodes[0]['latitude'];
	$centerLong = $resultingCodes[0]['longitude'];
}

if ($zoom < 3) $zoom = 3;
if ($zoom > 11) $zoom = 11;

echo 'var cityCircle;
var map;
var circles = [];
var myColor = "#00FF00";
function initialize() {
  // Create the map.
  var mapOptions = {
    zoom: '.$zoom.',
    center: new google.maps.LatLng('.$centerLat.', '.$centerLong.'),
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

   map = new google.maps.Map(document.getElementById("map-canvas"),
      mapOptions);


';

echo '	  


';
?>

for (var city in citymap) {
    var populationOptions = {
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      map: map,
	  draggable: true,
      center: citymap[city].center,
      radius: citymap[city].radius*1600
    };
	
	var circle = new google.maps.Circle(populationOptions);
	
	
    circles.push(circle);
    // Add the circle for this city to the map.
  }
};

google.maps.event.addDomListener(window, "load", initialize);
function output(){
	textOutput.value ="";
	for (var i=0;i<circles.length;i++) {
		textOutput.value += circles[i]["center"]+","+circles[i]["radius"]+"\n";
	}
	
}
function select(nr){
	for(i=0;i<<?=$circleCount?>;i++){
		if(i == nr){
			circles[i].set("fillColor","green");
			circles[i].set("strokeColor","green");
		}else{
			circles[i].set("fillColor","red");
			circles[i].set("strokeColor","red");
		}
	}
	
}
function createBall(size){
	var tmpOptions = {
      strokeColor: "#0000FF",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#0000FF",
      fillOpacity: 0.35,
      map: map,
	  draggable: true,
      center: map.getCenter(),
      radius: size*1600
    };
	
	var circle = new google.maps.Circle(tmpOptions);
    circles.push(circle);
}
function createGrayBall(size,lat,lng){
	var tmpOptions = {
      strokeColor: "#000000",
      strokeOpacity: 0.2,
      strokeWeight: 2,
      fillColor: "#FFFFFF",
      fillOpacity: 0.2,
	  clickable: false,
      map: map,
	  draggable: false,
      center: new google.maps.LatLng(lat, lng),
      radius: size*1600
    };
	
	var circle = new google.maps.Circle(tmpOptions);

}
</script>
<!-- http://ws.geonames.org/findNearbyPostalCodesJSON?formatted=true&lat=36&lng=-79.08&username=ivan -->
<div style="background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0px);height: 400px;margin-top: 20px;"><div id="map-canvas"></div></div>
		</div>
		<div class="col-xs-12">

	<? if(!$freeVersion){ ?>
			
			<button type="submit" formtarget="_blank" class="button green medium " style="border: none; width: 370px; font-size: 16px; margin-top:10px;">Create Campaign Based on these URLs</button>
			<br><br>
	<? } ?>
		</div>
<? } ?>
		</form>
	</div>

<? if($freeVersion){ ?>
	<div class="row">
		<div class="col-xs-12">
			<br><br>
			<h1 style="display: inline-block;width: 100%;">What is the Query Slicer?</h1>
			<br>
			<br>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			Even though LinkedIn itself doesn't let you target by smaller cities, county, state or multiple countries, <u>the Query Slicer does</u>. The Query Slicer cuts any large LinkedIn Advanced People Search query - typically ones with 1001-100000 profiles - into 25-200 smaller queries by city, county state or country, so that you spend less time building <1000-profile queries, and more time visiting profiles. Of particular note, with the SQ Query Slicer you can
			<br><br>
			<ul class="bulletPoints">
				<li>
					Find all the prospects in your particular geographic or named account patch
				</li>
				<li>
					Apply email/phone append tools like RingLead Capture, SalesLoft and Prophet to those lists
				</li>
				<li>	
					See a Google Map showing what coverage over any city, state, county or country territory your queries result in
				</li>
			</ul>
			<br>
			<b>How to use the Query Slicer</b>
			<ol>
				<li>
					Paste a large (1001-100000 profiles) query into the URL box; 
				</li>
				<li>
					Type in the cities, counties, states or countries you want to target; 
				</li>
				<li>
				
					Specify Distance (radius) and Number of URLs (default is 10 miles and 25 URLs), using the map to choose a Distance that fully covers the region;
				</li>
				<li>		
					Click the Create button; 
				</li>
				<li>		
					Copy and paste the query URLs into LinkedIn and you'll be able to see all the results you wanted to,
				</li>
			</ol>
		</div>
		<div class="col-xs-6">
			Smaller queries (red circles) mapped to chosen states
			<img src=splicermap.png>
		</div>
	</div>
<? } else { ?>
	<div class="row">
		<div class="col-xs-12">
			<b><u>Instructions</u></b><br>
			1. Paste a large (1001-100K profiles) query into the URL box;<br>
			2. Type in the cities, counties, states or countries you want to target and then click the Generate URLs button; <br>
			3. If you are targeting a smaller geographical area you may want to decrease the radius (Distance);<br>
			4. Click the Create button; <br>
			5. Either create a new Campaign from the resulting multiple smaller query URLs, or copy and paste the query URLs into an existing Campaign; <br><br>
		</div>
		
	</div>
<? } ?>
</div>
