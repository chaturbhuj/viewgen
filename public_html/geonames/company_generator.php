<?
$HTML_TITLE = "Company Query Generator";
ini_set('display_errors', 1);
$ACTIVE_HEADER_PAGE = "dashboard";
$freeVersion = false;
if (!isset($_GET['company_id'])) {
	$freeVersion = true;
	$IGNORE_AUTH = true;
	
}
if ($freeVersion) {
	
	$IGNORE_AUTH = true;
	$STOPMENU = true;
require_once('../db.php');
	require_once(dirname(__FILE__) . "/../header.php");
	require_once(dirname(__FILE__) . "/../header_index.php");
} else {
	require_once(dirname(__FILE__) . '/../authenticate.php');
	require_once(dirname(__FILE__) . '/../header.php');
}



function unparse_url($parsed_url) { 
  $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
  $host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
  $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
  $user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
  $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
  $pass     = ($user || $pass) ? "$pass@" : ''; 
  $path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
  $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
  $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
  return "$scheme$user$pass$host$port$path$query$fragment"; 
} 

if (!isset($_POST['type'])) {
	$_POST['type'] = 'Keywords';
}




DB::get_instance()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$outputUrls = [];
$resultingCodes = [];
$rawLocations = [];
$maxDistance = 0;
$centerLat = 0;
$centerLong = 0;

if (isset($_POST['action']) && $_POST['action'] == "generate" && @$_POST['baseUrl'] != "") {

	$items = explode("\n", trim($_POST['items']));

	$urlParts = parse_url($_POST['baseUrl']);
	if ($urlParts === false) {
		echo "Invalid base url";
		die;
	}

	foreach ($items as $item) {
		$queryStr = parse_str($urlParts['query'], $arr);
		$item = trim($item);
		if (strpos($item," ")) {
			$item = '"'.$item.'"';
		}
		if (strpos($_POST['baseUrl'],'/sales/')) {
			$arr['companyEntities'] = trim($item);
			$arr['companyScope'] = 'CURRENT';
		} else {
			$arr['company'] = trim($item);
			$arr['companyScope'] = 'C';
		}
		
		$urlParts['query'] = http_build_query($arr);
		$outputUrls[] = str_replace("%20","+",unparse_url($urlParts));
	}
}




?>
<div class="container geobuilder">
	<div class="row">
		<div class="col-xs-12">
			<div class="subnav">
				<ul id="nav" class="secondary">
					<li>
						<a class="button small" href="/geonames/find_location.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Query Slicer
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help" >
								<div>
									The Query Slicer cuts your large LinkedIn query into 25-200 smaller queries by city, county state or country, so that you spend less time building <1000-profile queries, and more time visiting profiles.
								</div>
							</i>
						</a>
					</li>
					<li>
						<a class="button small active" href="/geonames/company_generator.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Company Query Generator
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									Company Query Generator turns lists of companies you provide into hundreds of well-targeted LinkedIn queries in just seconds
								</div>
							</i>
						</a>
					</li>
					
					<li>
						<a class="button small" href="/connections_of_connections.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Connections of Connections
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									Targeting the 1st-degree connections of specific people you’re already connected to - along with your core targeting parameters
								</div>
							</i>
						</a>
					</li>
					
					<li>
						<a class="button small" href="/title_suggestions.php?company_id=<?=$_COMPANY_DATA['id']?>" style="padding-right: 1px;padding-left:6px;">
							Title Suggestions
						</a>
						<a class="help" style="float: right; margin-top: 8px;">
							<i class="help">
								<div>
									On this page you find all the most common titles of the people you have visited. You can combine these titles with one or more URLs from LinkedIn
								</div>
							</i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">

			<form method="post">
				<div class="row">
					<div class="col-xs-12">
						<input class="base_url" type="text" name="baseUrl" placeholder="URL from LinkedIn" value="<?=@$_POST['baseUrl']?>">
					</div>
					<div class="col-xs-6">
						<div class="form-group">
							<label>Company Names</label>
							<textarea placeholder="Type in or cut and paste your list of Company Names (one per line)" name="items" class="form-control" style="height: 200px;"></textarea>
						</div>
					</div>
					<div class="col-xs-6 text-center" style="padding-top: 100px;">
						<button type="submit" name="action" value="generate" class="btn btn-success btn-lg">Generate URLs</button>
						<button type="button" onclick="document.location.href = document.location.href" class="btn btn-info btn-lg">Reset</button>
					</div>
				</div>
			</form>
			<? if (count($outputUrls) > 0) { ?>
				<div class="row">
					<div class="col-xs-12">
						<? if ($freeVersion) {?>
							<textarea placeholder="Type in or cut and paste your list of Company Names (one Company Name per line)" name="query_urls" style="height: 400px"><?=implode("\n", $outputUrls)?></textarea>
						<? } else { ?>
							<form method="post" action="/create_campaign.php?company_id=<?=$_GET['company_id']?>&urls_created_with=company_query_generator">
								<div class="row">
									<div class="col-xs-12">
										<br>
										<button type="submit" formtarget="_blank" class="btn btn-success btn-lg">Create Campaign Based on these URLs</button>
										<br>
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<textarea name="query_urls" style="height: 400px" class="form-control"><?=implode("\n", $outputUrls)?></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<br>
										<button type="submit" formtarget="_blank"  class="btn btn-success btn-lg">Create Campaign Based on these URLs</button>
									</div>
								</div>
							</form>
						<? } ?>
					</div>
				</div>
			<? } ?>
		<br><br>
			<h1>What is the Company Generator?</h1>
				The Company Generator allows you to take a list of companies that you want to visit and turn that list into a 25-200 LinkedIn queries in just a few seconds. <b>For those with a Named Company focus, this feature gets rid of 95% of the work needed to get value from this service. </b>
			<br><br> 
			<b><u>Instructions</u></b><br>
			1. Build a search in LinkedIn with all desired targeting parameters <b>except company names</b>, and then paste the query result URL from LinkedIn into the URL box below; <br>
			2. Type in or cut and paste your list of Company Names (one per line) into the Company Name box below<br>
			3. Click the Generate URLs button; <br>
			4. Either create a new Campaign from the resulting multiple query URLs, or copy and paste the query URLs into an existing Campaign; 
		</div>
	</div>
</div>
