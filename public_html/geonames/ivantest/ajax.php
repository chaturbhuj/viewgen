<?php

require_once('../../authenticate.php');

function ajax_response($status, $payload = array()) {
	echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

if (!isset($_POST['action'])) ajax_response("error");

$action = $_POST['action'];

if ($action == "get_location") {
	if (!isset($_POST['str'])) ajax_response("error");

	$str = $_POST['str'];

	if ($str == "") ajax_response("success", array());

	$query = DB::prep("select `admin name1`, `country code`,count(*) as zipHits from postal_code where `admin name1` like ? GROUP BY `admin name1` ORDER BY zipHits DESC");
	$query->execute(array($str."%"));
	$res = array();
	while ($row = $query->fetch()) {
		$res[] = array(
			'n' => $row['admin name1'].", ".$row["country code"]." (".($row['zipHits']).')',
			'c' => $row['country code'],
			't' => "admin name1",
			'id' => "admin name1=".$row['admin name1']
		);
	}
	
	$query = DB::prep("select `admin name2`,`admin name1`, `country code`,count(*) as zipHits from postal_code where `admin name2` like ? GROUP BY concat(`admin name2`,`admin name1`) ORDER BY zipHits DESC");
	$query->execute(array($str."%"));
	
	while ($row = $query->fetch()) {
		$res[] = array(
			'n' => $row['admin name2'].", ".$row['admin name1'].", ".$row["country code"]." (".($row['zipHits']).')',
			'c' => $row['country code'],
			't' => "admin name2",
			'id' => "admin name2=".$row['admin name2'].";admin name1=".$row['admin name1']
		);
	}
	
	$query = DB::prep("select `admin name3`,`admin name2`,`admin name1`, `country code`,count(*) as zipHits from postal_code where `admin name3` like ? GROUP BY concat(`admin name3`,`admin name2`,`admin name1`) ORDER BY zipHits DESC");
	$query->execute(array($str."%"));
	
	while ($row = $query->fetch()) {
		$res[] = array(
			'n' => $row['admin name3'].", ".$row['admin name2'].", ".$row['admin name1'].", ".$row["country code"]." (".($row['zipHits']).')',
			'c' => $row['country code'],
			't' => "admin name3",
			'id' => "admin name3=".$row['admin name3'].";admin name2=".$row['admin name2'].";admin name1=".$row['admin name1']
		);
	}
	if (strlen($str) > 3) {
		$query = DB::prep("select `place name`,`admin name3`,`admin name2`,`admin name1`, `country code`,count(*) as zipHits from postal_code where `place name` like ? GROUP BY concat(`place name`,`admin name3`,`admin name2`,`admin name1`) ORDER BY zipHits DESC");
		$query->execute(array($str."%"));
		
		while ($row = $query->fetch() and count($res) < 10) {
			$res[] = array(
				'n' => $row['place name'].", ".$row['admin name3'].", ".$row['admin name2'].", ".$row['admin name1'].", ".$row["country code"]." (".($row['zipHits']).')',
				'c' => $row['country code'],
				't' => "place name",
				'id' => "place name=".$row['place name'].";admin name3=".$row['admin name3'].";admin name2=".$row['admin name2'].";admin name1=".$row['admin name1']
			);
		}
	}
	ajax_response("success", $res);
} else {
	ajax_response("error");
}

?>
