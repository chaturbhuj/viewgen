<?


require_once('../../authenticate.php');

$outputUrls = [];
$resultingCodes = [];
if (isset($_POST['action']) && $_POST['action'] == "generate") {

	$locations = array_keys($_POST['location']);
	
	$filter = array();
	foreach($locations as $location) {
		$parts = explode(";",$location);
		$andFilter = array();
		foreach($parts as $part) {
			$p = explode("=",$part);
			$andFilter[] = "`".mysql_real_escape_string($p[0])."` = '".mysql_real_escape_string($p[1])."'";
		}
		$filter[] = "(".implode(" AND ",$andFilter).")";
	}
	$queryString ="SELECT * FROM postal_code WHERE (".implode(" OR ",$filter).") GROUP BY `postal code` ORDER BY `postal code`";
	
	$query = DB::prep($queryString);
	$query->execute(array());
	
	$postalCodes = $query->fetchAll();
	//print_r($postalCodes);
	
	//die($queryString);
	$currentCode = null;
	$eatenCodes = [];
	for ($i = 0; $i < count($postalCodes); $i++) {
		if (isset($eatenCodes[$i])) continue;
		$currentCode = $postalCodes[$i];

		if (!isset($currentCode['count'])) {
			$currentCode['count'] = 0;
			$currentCode['circleSize'] = $_POST['size'];
		}

		for ($j = $i + 1; $j < count($postalCodes); $j++) {
			$distance = distanceLatLong($postalCodes[$j]['latitude'], $postalCodes[$j]['longitude'], $currentCode['latitude'], $currentCode['longitude']);
			if ($distance <= $_POST['size'] * 1) {
				// Eat this postal code.
				$currentCode['count']++;
				$eatenCodes[$j] = 1;
			}
		}
		// Cannot eat any more.
		$resultingCodes[] = $currentCode;
	}
	usort($resultingCodes, function ($a, $b) {
		if ($a['count'] == $b['count']) return 0;
		return ($a['count'] < $b['count']) ? 1 : -1;
	});
	$resultingCodes = array_slice($resultingCodes, 0, $_POST['urlLimit']);
	
	$parts = explode("&",$_POST["baseUrl"]);
	$newParts = array();
	foreach($parts as $part) {
		$p = explode("=",$part);
		if ($p[0] == "locationType") {
		} else if ($p[0] == "countryCode") {
		} else if ($p[0] == "distance") {
		} else if ($p[0] == "postalCode") {
		} else if ($p[0] == "f_G") {
		} else {
			$newParts[] = $part;
		}
	}
	$baseUrl = implode("&",$newParts);
	foreach ($resultingCodes as $code) {
		$outputUrls[] = $baseUrl."&postalCode=".urlencode($code['postal code'])."&locationType=I&countryCode=". strtolower($code['country code']) ."&distance=".urlencode($_POST['size']);
	}
}

require_once('../../header.php');

?>
<style>

.geobuilder {
margin-top: 30px;
}

.geobuilder .base_url {
	width: 500px;
	display: block;
}

.geobuilder .editor_box {
margin: 15px 0 0 0;
}

.geobuilder .suggestions {
display: block;
}


.geobuilder .select-size-label { 
margin: 20px 0 0 30px;
display: inline-block;
float: none;
}
.geobuilder .select-name {
margin: 0 0 0 30px;
float: none;
}

.geobuilder .select-url-limit-label { 
margin: 20px 0 0 30px;
display: inline-block;
float: none;
}
.geobuilder .select-url-limit {
margin: 0 0 0 30px;
float: none;
}

</style>
<div class="geobuilder">
	<form method="post">
		<input class="base_url" type="text" name="baseUrl" placeholder="URL from LinkedIn">
		<div class="editor_box collapse opened">
			<h3>Location:<i>*</i></h3>
			
			<span class="add_location"><i></i>Add location</span>
			<div class="suggestions">
				<input type="text" placeholder="Start typing location">
				<i></i>
				<ul class="box"></ul>
			</div>
		</div>
		<label for="selectSize" class="select-size-label">Distance</label>
		<select class="select-name" id="selectSize" name="size">
			<option value="0">0 miles</option>
			<option value="1">1 mile</option>
			<option value="3">3 miles</option>
			<option value="5">5 miles</option>
			<option selected=selected value="10">10 miles</option>
			<option value="25">25 miles</option>
			<option value="35">35 miles</option>
			<option value="50">50 miles</option>
			<option value="75">75 miles</option>
			<option value="100">100 miles</option>
		</select>
		<label for="selectLimit" class="select-url-limit-label">Number of Urls</label>
		<select class="select-url-limit" id="selectLimit" name="urlLimit">
			<option value="10">10</option>
			<option selected=selected value="50">50</option>
			<option value="100">100</option>
			<option value="200">200</option>
		</select>
		<button type="submit" name="action" value="generate">Generate</button>
	</form>
	<? if (count($outputUrls) > 0) { ?>
		<textarea style="width: 800px; height: 400px"><?=implode("\n", $outputUrls)?></textarea>
		<form method="post" action="/geonames/preview_on_map.php">
			<? foreach ($resultingCodes as $i => $code) { ?>
				<input type="hidden" name="circles[<?=$i?>][0]" value="<?=$code['latitude']?>">
				<input type="hidden" name="circles[<?=$i?>][1]" value="<?=$code['longitude']?>">
				<input type="hidden" name="circles[<?=$i?>][2]" value="<?=$code['circleSize']?>">
			<? } ?>
			<button type="submit" formtarget="_blank">Preview on Map</button>
		</form>
	<? } ?>
</div>
