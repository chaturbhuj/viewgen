
server {
	listen 80;

	server_name viewgentools.com www.viewgentools.com;

	root /var/www/live;
	index index.php index.html index.htm;

	port_in_redirect off;
	
	location ~ /wordpress/.*.php {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
		include fastcgi_params2;
	}

	location ~ /wordpress {
	}

	location ~ /blog/.*.php {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
		include fastcgi_params2;
	}

	location ~ /blog/wp-content/uploads/.* {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root/blog/proxy.php;
		include fastcgi_params2;
	}

	location ~ /blog {
	}

	location ~ /help/.*.php {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
		include fastcgi_params2;
	}

	location ~ /help/wp-content/uploads/.* {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root/help/proxy.php;
		include fastcgi_params2;
	}

	location ~ /help {
	}

	location / {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root/index.php;
		include fastcgi_params;
	}

}

server {
	listen 443;

	root /var/www/live;
	index index.php index.html index.htm;

	server_name viewgentools.com www.viewgentools.com;

	#ssl on;
	#ssl_certificate /etc/apache2/ssl/searchquant.net/chained.crt;
	#ssl_certificate_key /etc/apache2/ssl/searchquant.net/searchquant.net.key;

	port_in_redirect off;

	location / {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_index index.php;
		fastcgi_param   SCRIPT_FILENAME  $document_root/index.php;
		include fastcgi_params;
	}

}

