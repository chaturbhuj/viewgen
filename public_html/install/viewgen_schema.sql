-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: searchquant
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_daily_statistics`
--

DROP TABLE IF EXISTS `admin_daily_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_daily_statistics` (
  `admin_daily_statistics_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_daily_statistics_date` date NOT NULL,
  `visits` int(11) NOT NULL,
  `visits_to_premium` int(11) NOT NULL,
  `active_users` int(11) NOT NULL,
  `active_companies` int(11) NOT NULL,
  `active_windows` int(11) NOT NULL,
  `active_mac` int(11) NOT NULL,
  `visits_windows` int(11) NOT NULL,
  `visits_mac` int(11) NOT NULL,
  PRIMARY KEY (`admin_daily_statistics_id`),
  UNIQUE KEY `admin_daily_statistics_date` (`admin_daily_statistics_date`)
) ENGINE=InnoDB AUTO_INCREMENT=3209 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blog_post`
--

DROP TABLE IF EXISTS `blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_post` (
  `blog_post_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `status` enum('active','deleted','','') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`blog_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `publish_status` enum('draft','published','deleted','','archived') NOT NULL DEFAULT 'draft',
  `is_query_campaign` tinyint(1) NOT NULL,
  `priority` bigint(12) NOT NULL,
  `secondary_priority` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `name` varchar(200) NOT NULL,
  `visit_by` varchar(200) NOT NULL,
  `visit_by_id` bigint(12) NOT NULL,
  `status` varchar(50) NOT NULL,
  `visits` bigint(12) NOT NULL,
  `total_visits` bigint(12) NOT NULL,
  `revisit_after` bigint(12) NOT NULL,
  `keywords` text NOT NULL,
  `companies` text NOT NULL,
  `company_scope` varchar(100) NOT NULL,
  `titles` text NOT NULL,
  `title_scope` varchar(100) NOT NULL,
  `locations` text NOT NULL,
  `seniority_levels` text NOT NULL,
  `company_sizes` text NOT NULL,
  `relationships` text NOT NULL,
  `functions` text NOT NULL,
  `industries` text NOT NULL,
  `campaign_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `campaign_last_visit` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `campaign_visit_count` bigint(12) NOT NULL,
  `revisit_from_campaign_id` bigint(12) NOT NULL,
  `issue_message` varchar(150) NOT NULL,
  `campaign_vbr` float DEFAULT NULL,
  `campaign_vbr_date` timestamp NULL DEFAULT NULL,
  `original_url` varchar(200) NOT NULL,
  `total_visitors` int(11) NOT NULL,
  `has_sent_campaign_finished_email` tinyint(1) NOT NULL DEFAULT '0',
  `campaign_comment` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `publish_status` (`publish_status`),
  KEY `status` (`status`),
  KEY `secondary_priority` (`secondary_priority`)
) ENGINE=InnoDB AUTO_INCREMENT=52523 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign_statistics`
--

DROP TABLE IF EXISTS `campaign_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_statistics` (
  `campaign_statistics_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) unsigned NOT NULL,
  `campaign_id` bigint(12) unsigned NOT NULL,
  `li_user_id` bigint(12) NOT NULL,
  `date` date NOT NULL,
  `visited` int(11) NOT NULL,
  `visited_unique` int(11) NOT NULL,
  `visitors` int(11) NOT NULL,
  PRIMARY KEY (`campaign_statistics_id`),
  UNIQUE KEY `Unique Campaign And Date` (`company_id`,`campaign_id`,`li_user_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=19226236 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `account_type` enum('paid','free','pending_free','') NOT NULL DEFAULT 'paid',
  `subscription_plan_code` varchar(64) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `password_salt` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_password` varchar(100) NOT NULL,
  `client_hash` varchar(64) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `wait_between_connections` bigint(12) NOT NULL DEFAULT '10',
  `last_visit` datetime NOT NULL,
  `people_count` bigint(12) NOT NULL,
  `assignedTo` bigint(12) NOT NULL,
  `after_n_visits` bigint(12) NOT NULL DEFAULT '200',
  `wait_or_stop` bigint(12) NOT NULL DEFAULT '0',
  `industry_spec` text NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `referer` varchar(200) NOT NULL,
  `track` tinyint(1) NOT NULL DEFAULT '1',
  `frozen` tinyint(1) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isPremium` tinyint(1) NOT NULL,
  `send_automatic_alerts` tinyint(1) NOT NULL DEFAULT '1',
  `automatic_alerts_email` text NOT NULL,
  `alert_unsubscribe_token` varchar(32) NOT NULL,
  `new_add_user_page` tinyint(1) NOT NULL DEFAULT '1',
  `company_referer_paypal` varchar(200) DEFAULT NULL,
  `company_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_comment` varchar(250) NOT NULL,
  `discourse_username` varchar(100) DEFAULT NULL,
  `prosperworks_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_name` (`company_name`),
  UNIQUE KEY `client_hash` (`client_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=2226 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contactForm`
--

DROP TABLE IF EXISTS `contactForm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactForm` (
  `contactFormId` int(11) NOT NULL AUTO_INCREMENT,
  `contactFormCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `howDidYouHearAboutUs` varchar(250) NOT NULL,
  `whatAreYouInterestedIn` text NOT NULL,
  PRIMARY KEY (`contactFormId`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `daily_statistics`
--

DROP TABLE IF EXISTS `daily_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `daily_statistics` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `li_user_id` bigint(12) NOT NULL,
  `date` date NOT NULL,
  `visited` bigint(12) NOT NULL,
  `visitors` bigint(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userAndDate` (`li_user_id`,`date`),
  KEY `li_user_id` (`li_user_id`),
  KEY `date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=85708 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `emailId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailTemplateId` int(11) unsigned NOT NULL,
  `emailCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emailSubject` varchar(100) NOT NULL,
  `emailBody` text NOT NULL,
  `recipient` varchar(100) NOT NULL,
  `usedVariables` text NOT NULL,
  PRIMARY KEY (`emailId`),
  KEY `emailTemplateId` (`emailTemplateId`)
) ENGINE=InnoDB AUTO_INCREMENT=11525 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailTemplate`
--

DROP TABLE IF EXISTS `emailTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailTemplate` (
  `emailTemplateId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailTemplateStatus` enum('inactive','active','deleted','') NOT NULL DEFAULT 'inactive',
  `emailTemplateName` varchar(100) NOT NULL,
  `emailTemplateSubject` varchar(100) NOT NULL,
  `emailTemplateBody` text NOT NULL,
  `availableVariables` text NOT NULL,
  `unsubscribeType` varchar(100) DEFAULT NULL,
  `emailTemplateOrder` float NOT NULL,
  `numberOfSentEmails` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emailTemplateId`),
  UNIQUE KEY `emailTemplateName` (`emailTemplateName`),
  KEY `emailTemplateStatus` (`emailTemplateStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailUnsubscribed`
--

DROP TABLE IF EXISTS `emailUnsubscribed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailUnsubscribed` (
  `emailUnsubscribedId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailUnsubscribedCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) unsigned NOT NULL,
  `unsubscribeType` varchar(100) NOT NULL,
  PRIMARY KEY (`emailUnsubscribedId`),
  UNIQUE KEY `company_id_2` (`company_id`,`unsubscribeType`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_log`
--

DROP TABLE IF EXISTS `email_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_log` (
  `email_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `subject` varchar(200) NOT NULL,
  `body` text NOT NULL,
  `company_id` int(11) NOT NULL,
  `email_type` varchar(100) NOT NULL,
  PRIMARY KEY (`email_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `freeze_cache`
--

DROP TABLE IF EXISTS `freeze_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freeze_cache` (
  `company_id` int(11) unsigned NOT NULL,
  `is_zero_account` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_date` varchar(100) NOT NULL,
  `invoice_title` varchar(100) NOT NULL,
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geonames`
--

DROP TABLE IF EXISTS `geonames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geonames` (
  `geonameid` bigint(15) NOT NULL,
  `name` varchar(200) NOT NULL,
  `asciiname` varchar(200) NOT NULL,
  `alternatenames` varchar(10000) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `feature_class` varchar(10) NOT NULL,
  `feature_code` varchar(10) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `cc2` varchar(200) NOT NULL,
  `admin1` varchar(20) NOT NULL,
  `admin2` varchar(80) NOT NULL,
  `admin3` varchar(20) NOT NULL,
  `admin4` varchar(20) NOT NULL,
  `population` bigint(10) NOT NULL,
  `elevation` bigint(10) NOT NULL,
  `dem` varchar(100) NOT NULL,
  `timezone` varchar(40) NOT NULL,
  `modification_date` date NOT NULL,
  UNIQUE KEY `geonameid` (`geonameid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `graph_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `graph_name` varchar(128) NOT NULL,
  PRIMARY KEY (`graph_id`),
  UNIQUE KEY `graph_name` (`graph_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `graph_data`
--

DROP TABLE IF EXISTS `graph_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_data` (
  `graph_data_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `graph_id` bigint(12) unsigned NOT NULL,
  `x_value` varchar(32) NOT NULL,
  `y_value` bigint(12) NOT NULL,
  PRIMARY KEY (`graph_data_id`),
  KEY `graph_id` (`graph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `htmlDump`
--

DROP TABLE IF EXISTS `htmlDump`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `htmlDump` (
  `htmlDumpId` bigint(12) NOT NULL AUTO_INCREMENT,
  `htmlDumpCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dump` longtext NOT NULL,
  `companyId` bigint(12) NOT NULL,
  `crawled_by` varchar(250) NOT NULL,
  `crawled_by_id` varchar(250) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `client_hash` varchar(250) NOT NULL,
  `html_people_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`htmlDumpId`),
  KEY `companyId` (`companyId`),
  KEY `htmlDumpCreated` (`htmlDumpCreated`),
  KEY `crawled_by_id` (`crawled_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50722673 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inbound_visit`
--

DROP TABLE IF EXISTS `inbound_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_visit` (
  `inbound_visit_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `crawled_by_id` bigint(12) unsigned NOT NULL,
  `linked_in_id` bigint(12) unsigned DEFAULT NULL,
  `linked_in_hashed_id` varchar(100) NOT NULL,
  `visit_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(100) NOT NULL,
  `industry_id` bigint(12) unsigned NOT NULL,
  `industry_name` varchar(100) NOT NULL,
  `headline` varchar(100) NOT NULL,
  `guessed_visit_date` date DEFAULT NULL,
  PRIMARY KEY (`inbound_visit_id`),
  KEY `crawled_by_id` (`crawled_by_id`,`linked_in_id`),
  KEY `linked_in_hashed_id` (`linked_in_hashed_id`),
  KEY `name` (`name`),
  KEY `guessed_visit_date` (`guessed_visit_date`),
  KEY `linked_in_id` (`linked_in_id`),
  KEY `crawled_by_id_2` (`crawled_by_id`),
  KEY `crawled_by_id_3` (`crawled_by_id`,`guessed_visit_date`)
) ENGINE=InnoDB AUTO_INCREMENT=8352655 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `li_location`
--

DROP TABLE IF EXISTS `li_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `li_location` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(200) NOT NULL,
  `linked_in_id` varchar(200) NOT NULL,
  `hits` bigint(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `location_name` (`location_name`),
  UNIQUE KEY `linked_in_id` (`linked_in_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2495 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `li_problem`
--

DROP TABLE IF EXISTS `li_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `li_problem` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `people_id` bigint(12) NOT NULL,
  `people_list_id` bigint(12) NOT NULL,
  `company_id` bigint(12) NOT NULL,
  `html` longtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `people_list_id` (`people_list_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8833 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `li_users`
--

DROP TABLE IF EXISTS `li_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `li_users` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(100) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `getVisitorsAfter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `linked_in_id` bigint(12) NOT NULL,
  `wizard_data` text NOT NULL,
  `wizard_finished` tinyint(1) NOT NULL DEFAULT '0',
  `max_visits_per_day` bigint(12) NOT NULL DEFAULT '800',
  `software_status` enum('running','waiting','offline','') NOT NULL DEFAULT 'offline',
  `campaign_id` bigint(22) NOT NULL,
  `last_connection` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `limit_reached_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `limit_reached_of` bigint(5) NOT NULL,
  `last_inbound_check` timestamp NULL DEFAULT NULL,
  `client_version` varchar(100) NOT NULL,
  `referer_paypal_account` varchar(200) NOT NULL,
  `enableVisitMaximization` tinyint(1) NOT NULL DEFAULT '1',
  `enableAutoVisitParameters` tinyint(1) NOT NULL DEFAULT '1',
  `account_vbr` float DEFAULT NULL,
  `account_volume` bigint(12) DEFAULT NULL,
  `visitsLast24Hours` bigint(12) NOT NULL DEFAULT '0',
  `anonymous` varchar(100) NOT NULL,
  `lastAnonymusCheck` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `anonymousHtml` longtext NOT NULL,
  `subscriptions` varchar(100) NOT NULL,
  `active_campaign_count` int(11) NOT NULL,
  `finished_campaign_count` int(11) NOT NULL,
  `archived_campaign_count` int(11) NOT NULL,
  `paused_campaign_count` int(11) NOT NULL,
  `total_visited` int(11) NOT NULL,
  `total_days_visiting` int(11) NOT NULL,
  `first_visit` date NOT NULL,
  `last_visit` date NOT NULL,
  `first_visitor` date NOT NULL,
  `last_visitor` date NOT NULL,
  `visitors_before_first_visit` int(11) NOT NULL,
  `visitors_after_last_visit` int(11) NOT NULL,
  `visitors_during_visiting` int(11) NOT NULL,
  `avg_views_before_sq` int(11) NOT NULL,
  `avg_views_with_sq` int(11) NOT NULL,
  `vbr_during_visiting` decimal(10,2) NOT NULL,
  `last_ssi_check` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `have_new_interface` tinyint(1) NOT NULL DEFAULT '0',
  `new_interface_detected` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `have_sales_navigator` tinyint(1) NOT NULL DEFAULT '0',
  `prosperworks_people_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `linked_in_id` (`linked_in_id`),
  KEY `company_id` (`company_id`),
  KEY `wizard_finished` (`wizard_finished`)
) ENGINE=InnoDB AUTO_INCREMENT=1946 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_views`
--

DROP TABLE IF EXISTS `page_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_views` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `page` varchar(200) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `server_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `local_time` varchar(100) NOT NULL,
  `timezone_offset` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3050 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `campaign_id` bigint(12) NOT NULL,
  `search_url_id` bigint(12) NOT NULL,
  `people_list_id` bigint(12) NOT NULL,
  `linked_in_id` varchar(400) NOT NULL,
  `url` varchar(200) NOT NULL,
  `sent_to_crawler` datetime NOT NULL,
  `sent_to_crawler_date` date NOT NULL,
  `sent_to_crawler_date_utc` date NOT NULL,
  `crawled_by` varchar(100) NOT NULL,
  `crawled_by_id` bigint(12) NOT NULL,
  `html` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `employer` varchar(100) NOT NULL,
  `employer_url` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `premium` tinyint(1) NOT NULL DEFAULT '0',
  `member_id` bigint(30) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `is_connected` tinyint(1) NOT NULL DEFAULT '0',
  `industry` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `linked_in_id` (`linked_in_id`(20)),
  KEY `campaign_id` (`campaign_id`),
  KEY `search_url_id` (`search_url_id`),
  KEY `people_list_id` (`people_list_id`),
  KEY `sent_to_crawler` (`sent_to_crawler`),
  KEY `sent_to_crawler_date` (`sent_to_crawler_date`),
  KEY `crawled_by_id` (`crawled_by_id`),
  KEY `is_connected` (`is_connected`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=53636203 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `people_list`
--

DROP TABLE IF EXISTS `people_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_list` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `search_url_id` bigint(12) NOT NULL,
  `page_nr` bigint(12) NOT NULL,
  `url` varchar(11000) NOT NULL,
  `html` longtext NOT NULL,
  `crawled_by` varchar(100) NOT NULL,
  `crawled_by_id` bigint(12) NOT NULL,
  `last_list` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `people_list_completed` timestamp NULL DEFAULT NULL,
  `people_list_last_visit` timestamp NULL DEFAULT NULL,
  `retry` bigint(12) NOT NULL DEFAULT '0',
  `retry_count` bigint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `search_url_id` (`search_url_id`),
  KEY `retry` (`retry`),
  KEY `people_list_last_visit` (`people_list_last_visit`),
  KEY `crawled_by_id` (`crawled_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7424649 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postal_code`
--

DROP TABLE IF EXISTS `postal_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postal_code` (
  `postal_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `country code` varchar(2) NOT NULL,
  `postal code` varchar(20) NOT NULL,
  `place name` varchar(180) NOT NULL,
  `admin name1` varchar(100) NOT NULL,
  `admin code1` varchar(20) NOT NULL,
  `admin name2` varchar(100) NOT NULL,
  `admin code2` varchar(20) NOT NULL,
  `admin name3` varchar(100) NOT NULL,
  `admin code3` varchar(20) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `accuracy` bigint(1) NOT NULL,
  PRIMARY KEY (`postal_code_id`),
  KEY `place name` (`place name`),
  KEY `admin name1` (`admin name1`),
  KEY `admin name2` (`admin name2`),
  KEY `admin name3` (`admin name3`),
  KEY `longitude` (`longitude`),
  KEY `country code` (`country code`),
  KEY `postal code` (`postal code`)
) ENGINE=InnoDB AUTO_INCREMENT=3202302 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postal_code_aggregate`
--

DROP TABLE IF EXISTS `postal_code_aggregate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postal_code_aggregate` (
  `postal_code_aggregate_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `country code` varchar(2) NOT NULL,
  `postal code` varchar(20) NOT NULL,
  `place name` varchar(180) NOT NULL,
  `admin name1` varchar(100) NOT NULL,
  `admin code1` varchar(20) NOT NULL,
  `admin name2` varchar(100) NOT NULL,
  `admin code2` varchar(20) NOT NULL,
  `admin name3` varchar(100) NOT NULL,
  `admin code3` varchar(20) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `accuracy` bigint(1) NOT NULL,
  `zipHits` bigint(12) unsigned NOT NULL,
  `aggregateType` varchar(30) NOT NULL,
  `countryName` varchar(200) NOT NULL,
  PRIMARY KEY (`postal_code_aggregate_id`),
  KEY `place name` (`place name`),
  KEY `admin name1` (`admin name1`),
  KEY `admin name2` (`admin name2`),
  KEY `admin name3` (`admin name3`),
  KEY `zipHits` (`zipHits`),
  KEY `countryName` (`countryName`),
  KEY `aggregateType` (`aggregateType`)
) ENGINE=InnoDB AUTO_INCREMENT=64937 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rc_account`
--

DROP TABLE IF EXISTS `rc_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rc_account` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `state` varchar(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_code` (`account_code`)
) ENGINE=InnoDB AUTO_INCREMENT=993 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rc_subscription`
--

DROP TABLE IF EXISTS `rc_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rc_subscription` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) unsigned NOT NULL,
  `rc_account_id` bigint(12) NOT NULL,
  `state` enum('active','canceled','expired','future','in_trial','live','past_due') NOT NULL,
  `plan_code` varchar(32) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uuid` varchar(64) NOT NULL,
  `currency` varchar(8) NOT NULL,
  `quantity` bigint(12) NOT NULL,
  `current_period_started_at` datetime NOT NULL,
  `current_period_ends_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=992 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rc_subscription_history`
--

DROP TABLE IF EXISTS `rc_subscription_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rc_subscription_history` (
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) unsigned NOT NULL,
  `rc_account_id` bigint(12) NOT NULL,
  `state` enum('active','canceled','expired','future','in_trial','live','past_due') NOT NULL,
  `plan_code` varchar(32) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uuid` varchar(64) NOT NULL,
  `currency` varchar(8) NOT NULL,
  `quantity` bigint(12) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `company_id` (`company_id`,`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `referer`
--

DROP TABLE IF EXISTS `referer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referer` (
  `referer_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `refered_by_company` bigint(12) NOT NULL,
  `refered_by_li_user` bigint(12) NOT NULL,
  `refered_company_id` bigint(12) NOT NULL,
  `referer_signed_up` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `referer_first_payment` timestamp NULL DEFAULT NULL,
  `is_trial` bigint(12) NOT NULL DEFAULT '0',
  `paid_as_credit` tinyint(1) NOT NULL DEFAULT '0',
  `paid_to_paypal` bigint(12) NOT NULL,
  `payment_date` timestamp NULL DEFAULT NULL,
  `paid_to_paypal_account` varchar(200) NOT NULL,
  PRIMARY KEY (`referer_id`),
  KEY `refered_by_company` (`refered_by_company`,`refered_company_id`,`referer_signed_up`),
  KEY `refered_by_li_user` (`refered_by_li_user`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `referer_email_log`
--

DROP TABLE IF EXISTS `referer_email_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referer_email_log` (
  `referer_email_log_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `email` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emailSubject` varchar(200) NOT NULL,
  `emailBody` text NOT NULL,
  PRIMARY KEY (`referer_email_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationship_info`
--

DROP TABLE IF EXISTS `relationship_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_info` (
  `relationship_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `html_content` text NOT NULL,
  `image_url` varchar(150) NOT NULL,
  PRIMARY KEY (`relationship_info_id`),
  UNIQUE KEY `people_id` (`people_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16847435 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports_cache`
--

DROP TABLE IF EXISTS `reports_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_cache` (
  `reports_cache_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `li_users_id` bigint(12) NOT NULL,
  `number_of_visits` bigint(12) NOT NULL,
  `cache_date` date NOT NULL,
  PRIMARY KEY (`reports_cache_id`),
  UNIQUE KEY `li_users_id_2` (`li_users_id`,`cache_date`),
  KEY `company_id` (`company_id`),
  KEY `cache_date` (`cache_date`),
  KEY `li_users_id` (`li_users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247531 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_url`
--

DROP TABLE IF EXISTS `search_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_url` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint(12) NOT NULL,
  `company_id` bigint(12) NOT NULL,
  `url` varchar(10000) NOT NULL,
  `finished` tinyint(1) NOT NULL DEFAULT '0',
  `paused` tinyint(1) NOT NULL,
  `visit_by` varchar(150) NOT NULL,
  `visit_by_id` bigint(12) NOT NULL,
  `total_matches` bigint(12) NOT NULL DEFAULT '-1',
  `name` varchar(255) NOT NULL,
  `revisit_after` bigint(12) NOT NULL,
  `visit_after` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `zip_code_id` bigint(12) DEFAULT NULL,
  `parent_search_url_id` bigint(12) NOT NULL,
  `duplicateSkipCount` bigint(12) NOT NULL DEFAULT '0',
  `auto_generated` varchar(32) NOT NULL,
  `search_url_vbr` float DEFAULT NULL,
  `search_url_vbr_date` timestamp NULL DEFAULT NULL,
  `search_url_visits` bigint(12) DEFAULT NULL,
  `search_url_viewbacks` bigint(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `finished` (`finished`),
  KEY `campaign_id` (`campaign_id`),
  KEY `created` (`created`),
  KEY `visit_by_id` (`visit_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2392519 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sent_alert`
--

DROP TABLE IF EXISTS `sent_alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sent_alert` (
  `sent_alert_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) unsigned NOT NULL,
  `email` varchar(200) NOT NULL,
  `emailSubject` varchar(120) NOT NULL,
  `emailBody` text NOT NULL,
  `sent_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alert_type` varchar(120) NOT NULL,
  `parameters` text NOT NULL,
  PRIMARY KEY (`sent_alert_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sent_invite`
--

DROP TABLE IF EXISTS `sent_invite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sent_invite` (
  `sent_invite_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `email` varchar(200) NOT NULL,
  `sent_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sent_invite_id`),
  UNIQUE KEY `company_id_2` (`company_id`,`email`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `sessionId` varchar(255) NOT NULL,
  `sessionData` text NOT NULL,
  `lastUsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sessionId`),
  KEY `lastUsed` (`lastUsed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `software_license`
--

DROP TABLE IF EXISTS `software_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_license` (
  `software_license_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `product_key` varchar(50) NOT NULL,
  `company_id` bigint(12) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`software_license_id`),
  UNIQUE KEY `product_key` (`product_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12025 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `software_status`
--

DROP TABLE IF EXISTS `software_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_status` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `campaign_id` bigint(12) NOT NULL,
  `visit_by_id` bigint(12) NOT NULL,
  `campaign_name` varchar(128) NOT NULL,
  `visit_by` varchar(128) NOT NULL,
  `status` enum('running','waiting','offline','') NOT NULL DEFAULT 'offline',
  `last_connection` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`,`visit_by_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ssi`
--

DROP TABLE IF EXISTS `ssi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ssi` (
  `ssi_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `crawled_by_id` int(11) NOT NULL,
  `crawled_by` varchar(100) NOT NULL,
  `ssi_crawled_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ssi_html` longtext NOT NULL,
  PRIMARY KEY (`ssi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=894 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_people`
--

DROP TABLE IF EXISTS `tmp_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_people` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `campaign_id` bigint(12) NOT NULL,
  `search_url_id` bigint(12) NOT NULL,
  `people_list_id` bigint(12) NOT NULL,
  `linked_in_id` varchar(400) NOT NULL,
  `url` varchar(200) NOT NULL,
  `sent_to_crawler` datetime NOT NULL,
  `sent_to_crawler_date` date NOT NULL,
  `sent_to_crawler_date_utc` date NOT NULL,
  `crawled_by` varchar(100) NOT NULL,
  `crawled_by_id` bigint(12) NOT NULL,
  `html` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `employer` varchar(100) NOT NULL,
  `employer_url` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `premium` tinyint(1) NOT NULL DEFAULT '0',
  `member_id` bigint(30) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `is_connected` tinyint(1) NOT NULL DEFAULT '0',
  `industry` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `company_id` bigint(12) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visit_connection`
--

DROP TABLE IF EXISTS `visit_connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visit_connection` (
  `visit_connection_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `people_id` bigint(12) NOT NULL,
  `inbound_visit_id` bigint(12) NOT NULL,
  `guessed_visit_date` date NOT NULL,
  PRIMARY KEY (`visit_connection_id`),
  UNIQUE KEY `Unique People And Inbound` (`people_id`,`inbound_visit_id`),
  KEY `people_id` (`people_id`),
  KEY `inbound_visit_id` (`inbound_visit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=329599453 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitors` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12) NOT NULL,
  `linked_in_user` varchar(100) NOT NULL,
  `linked_in_user_id` bigint(12) NOT NULL,
  `date` date NOT NULL,
  `visitor_count` bigint(12) NOT NULL,
  `epoch` varchar(50) NOT NULL,
  `profiles_visited` bigint(12) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueCompanyUserDay` (`company_id`,`linked_in_user`,`date`),
  KEY `company_id` (`company_id`),
  KEY `linked_in_user` (`linked_in_user`),
  KEY `date` (`date`),
  KEY `linked_in_user_id` (`linked_in_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=679686 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zipCode`
--

DROP TABLE IF EXISTS `zipCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zipCode` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(12) NOT NULL,
  `status` varchar(20) NOT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `code` varchar(20) NOT NULL,
  `hits` bigint(12) NOT NULL DEFAULT '-1',
  `radius` bigint(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `admin1` varchar(150) NOT NULL,
  `admin2` varchar(150) NOT NULL,
  `countryCode` varchar(20) NOT NULL,
  `url` varchar(300) NOT NULL,
  `type` varchar(100) NOT NULL,
  `area` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniquePoint` (`lat`,`lng`),
  KEY `type` (`type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8658 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-20  2:12:26
