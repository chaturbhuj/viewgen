<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
	ini_set('memory_limit', '1024M');
/** Error reporting */
error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
/*$objPHPExcel->getProperties()->setCreator("No one")
							 ->setLastModifiedBy("No one")
							 ->setTitle("Visited Profiles")
							 ->setSubject("Visited Profiles")
							 ->setDescription("Visited Profiles");
*/


$STOPHEAD = true;
$STOPMENU = true;

include('header.php');

if (!isset($_GET['company_id'])) {
	die;
}

function myJsonDecode($jsonText){
	$jsonObject = json_decode('["'.$jsonText.'"]',true);
	return $jsonObject[0];
}




$direction = "asc";
if (isset($_GET["direction"]) and $_GET["direction"] == 'desc') {
	$direction = "desc";
}
$orderBy = "people.id";
if (isset($_GET["order_by"]) and $_GET["order_by"] != "") {
	$orderBy = str_replace("timestamp","people.id",mysql_real_escape_string($_GET["order_by"]));
} else {
	$orderBy = "people.id";
	$direction = "desc";
}
$limit = "LIMIT 1000000";


$query = DB::prep("SELECT people.*,people_list.page_nr,people_list.url FROM people 
JOIN people_list ON people_list.id = people.people_list_id
WHERE people.name != '' AND
:company_id = people.company_id AND
(:search_url_id = people.search_url_id OR :search_url_id = 0) AND
(:user_id = people.crawled_by_id OR :user_id = 0)
ORDER BY IF(".$orderBy." = '','zzz',".$orderBy.") ".$direction."
".$limit);

if (!isset($_GET['search_url_id']) or $_GET['search_url_id'] == '') {
	$_GET['search_url_id'] = 0;
}
if (!isset($_GET['user']) or $_GET['user'] == '') {
	$_GET['user'] = 0;
}
$query->execute(array("company_id"=>$_GET['company_id'],"search_url_id"=>$_GET['search_url_id'],"user_id"=>$_GET['user']));



$queries = $query->fetchAll();




	//$row = ["Name","Title","Company (based on title)","Company (first listed)","Location","Timestamp","LinkedIn Profile"];
	//echo "sep=,\r\n";
	//echo implode(",",$row)."\r\n";
	$rowNr = 1;
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$rowNr, "Name")
            ->setCellValue('B'.$rowNr, "Title")
            ->setCellValue('C'.$rowNr, "Company (based on title)")
            ->setCellValue('D'.$rowNr, "Company (first listed)")
            ->setCellValue('E'.$rowNr, "Location")
            ->setCellValue('F'.$rowNr, "Timestamp")
            ->setCellValue('G'.$rowNr, "LinkedIn Profile");
	foreach ($queries as $query) {
		$rowNr++;
		$workAt = "";
		$atSplit = explode(" at ",myJsonDecode($query["title"]));
		if(count($atSplit) == 2) {
			$workAt = $atSplit[1];
		}
		$linkedInId = utf8_decode(html_entity_decode(myJsonDecode($query["linked_in_id"])));
		/*if (true) { // short version
			$p = explode("&",$linkedInId);
			$linkedInId = $p[0];
		}*/
		
		for ($i=0; $i<count($row); $i++) {
			if (strpos($row[$i],"\n") !== false) {
				$row[$i] = str_replace("\n",'',$row[$i]);
			}
			if (strpos($row[$i],"\r") !== false) {
				$row[$i] = str_replace("\r",'',$row[$i]);
			}
			if (strpos($row[$i],'"') !== false) {
				$row[$i] = str_replace('"','',$row[$i]);
			}
			if (strpos($row[$i],",") !== false) {
				$row[$i] = '"'.$row[$i].'"';
			}
		}
		$objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$rowNr, html_entity_decode($query['name']))
            ->setCellValue('B'.$rowNr, html_entity_decode($query["title"]))
            ->setCellValue('C'.$rowNr, html_entity_decode($workAt))
            ->setCellValue('D'.$rowNr, html_entity_decode(myJsonDecode($query["employer"])))
            ->setCellValue('E'.$rowNr, html_entity_decode(myJsonDecode($query["location"])))
            ->setCellValue('F'.$rowNr, $query["sent_to_crawler"])
            ->setCellValue('G'.$rowNr, "https://www.linkedin.com/profile/view?id=".$linkedInId);
		//echo implode(",",$row)."\r\n";
	}


/*// Add some data
for ($i = 0;$i<100000;$i++) {
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'Brian Crabtree')
            ->setCellValue('B'.$i, 'Director of Market Practice, Standards and SWIFT at Citibank')
            ->setCellValue('C'.$i, 'Citibank')
            ->setCellValue('D'.$i, 'Special Projects Inc')
            ->setCellValue('D'.$i, 'Greater New York City Area')
            ->setCellValue('D'.$i, '2014-11-12 17:38')
            ->setCellValue('D'.$i, 'https://www.linkedin.com/profile/view?id=20811199&authType=OUT_OF_NETWORK&authToken=9cFI&trk=vsrp_people_res_sec_act&trkInfo=VSRPsearchId%3A2581821415842695703%2CVSRPtargetId%3A20811199%2CVSRPcmpt%3Aprimary');
}*/


// Miscellaneous glyphs, UTF-8


// Rename worksheet
//$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Profiles Visited.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
