<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 23.08.17
 * Time: 18:37
 */

include_once __DIR__."/lib/jwt/src/JWT.php";

use Firebase\JWT;

global $CONFIG;
global $_USER_DATA;
global $_COMPANY_DATA;

$name = !empty($_USER_DATA['admin']) ? 'admin' : $_COMPANY_DATA['company_name'];
$email = !empty($_USER_DATA['admin']) ? 'admin@viewgentools.com' : $_COMPANY_DATA['email'];

$key       = $CONFIG['zendesk_key'];
$subdomain = $CONFIG['zendesk_subdomain'];

$now       = time();
$token = array(
    "jti"   => md5($now . rand()),
    "iat"   => $now,
    "name"  => $name,
    "email" => $email
);
$jwt = JWT\JWT::encode($token, $key);

$location = "https://" . $subdomain . ".zendesk.com/access/jwt?jwt=" . $jwt;
if(isset($_GET["return_to"])) {
    $location .= "&return_to=" . urlencode($_GET["return_to"]);
}
// Redirect
header("Location: " . $location);