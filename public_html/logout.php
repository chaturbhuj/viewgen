<?php

$STOPMENU = true;
$AUTH_DRAFT_OK = true;

require_once('db.php');

require_once('shared_functions.php');


// Destroy the session.
unset($_SESSION['auth']);
session_destroy();

redirect('/');

?>
