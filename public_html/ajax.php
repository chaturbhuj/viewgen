<?php

require_once('authenticate.php');

function ajax_response($status, $payload = array()) {
    header('Content-Type: application/json');
    echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

if (!isset($_POST['action'])) ajax_response("error");

$action = $_POST['action'];

if ($action == "get_location") {
	if (!isset($_POST['str'])) ajax_response("error");

	$str = $_POST['str'];

	if ($str == "") ajax_response("success", array());

	$query = DB::prep("select * from zipCode where name like ? and type in ('Cities', 'States', 'Countries') and status='active' order by hits desc limit 10");
	$query->execute(array($str."%"));

	$res = array();
	while ($row = $query->fetch()) {

		$tt = "City";
		if ($row['type'] == "States") $tt = "State";
		if ($row['type'] == "Countries") $tt = "Country";

		$res[] = array(
			'n' => $row['name']." (".($tt).')',
			'c' => $row['countryCode'],
			't' => $row['type'],
			'id' => $row['id']
		);
	}
	ajax_response("success", $res);
} else {
	ajax_response("error");
}

?>
