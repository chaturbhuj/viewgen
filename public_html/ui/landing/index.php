<?php 
include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;
?>
<html>
	<head>
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="stylesheet" href="/assets/ui/landing/css/styles.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header class="navbar">
				<a href="/" class="logo"></a>
				<ul class="header-menu" id="myTopnav">
					<li class="menu-item"><a class="item-link" href="/">home</a></li>
					<li class="menu-item"><a class="item-link" href="/about">about us</a></li>
					<li class="menu-item"><a class="item-link" href="/ui/login">login</a></li>
				</ul>
				<button onclick="myFunction()" class="toggle-btn">
					<span></span>
					<span></span>
					<span></span>
				</button>
			</header>
		</div>
		<div class="main-container">
			<div class="top-block">
				<h1 class="top-block__title">Social Media Consulting Services</h1>
				<p class="top-block__description">Our team leverages your brand to find well-targeted prospects</p>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
			<div class="customer-wr">
				<div class="container">
					<p class="customer-title">Some of our satisfied customers</p>
					<ul class="customer-list">
						<li class="customer-item"><img src="/assets/ui/landing/images/wells-fargo.png" alt=""></li>
						<li class="customer-item"><img src="/assets/ui/landing/images/domo.png" alt=""></li>
						<li class="customer-item"><img src="/assets/ui/landing/images/disco.png" alt=""></li>
						<li class="customer-item"><img src="/assets/ui/landing/images/badger.png" alt=""></li>
						<li class="customer-item"><img src="/assets/ui/landing/images/highspot.png" alt=""></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="direction">
					<div class="direction-item item_1">
						<header class="diration-title">Precision Targeting</header>
						<p class="diraction-description">
							Whether you're selling to SMB or Fortune 500, focus on the right people at the right company.
						</p>
					</div>
					<div class="direction-item item_2">
						<header class="diration-title">Defiined Messaging</header>
						<p class="diraction-description">
							Every engagement counts. We make sure that your brand will present the best messaging.
						</p>
					</div>
					<div class="direction-item item_3">
						<header class="diration-title">Qualified Inbound</header>
						<p class="diraction-description">
							Your time is valuable which is why our team  will help generate only well-targeted prospects.
						</p>
					</div>
				</div>
			</div>
			<div class="footer-block">
				<h2 class="footer-block__title">Join Hundreds of Sales Teams Using ViewGen to Grow Their Business</h2>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
		</div>
		<footer>
			<div class="container">
				<a href="/" class="footer-logo"></a>
				<hr>
				<div class="contacts">
					<ul class="contact-list">
						<li class="contact-item">viewgentools.com</li>
						<li class="contact-item">+1 (888) 901-8532</li>
						<li class="contact-item">info@viewgentools.com</li>
						<li class="contact-item"><a href="/gdpr">GDPR</a></li>
						<li class="contact-item"><a href="/terms">Terms</a></li>
					</ul>
					<div class="footer-form-wr">
						<header class="subscribe-form-title">join our newsletter</header>
						<form class="subscribe-form" action="">
							<input class="field-email" type="text" placeholder="Your email here">
							<input class="btn" type="submit" value="subscribe">
						</form>
					</div>
				</div>
<!--				<p class="copyright">&copy;2018 Search Quant LLC. All rights reserved</p>-->
			</div>
		</footer>
		<script>
			function myFunction() {
				var x = document.getElementById("myTopnav");
				if (x.className === "header-menu") {
					x.className += " open";
				} else {
					x.className = "header-menu";
				}
			}
		</script>
	</body>
</html>