<?php 
include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;
?>
<html>
	<head>
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="stylesheet" href="/assets/ui/landing/css/styles.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header class="navbar">
				<a href="/" class="logo"></a>
				<ul class="header-menu" id="myTopnav">
					<li class="menu-item"><a class="item-link" href="/">home</a></li>
					<li class="menu-item"><a class="item-link" href="">about us</a></li>
					<li class="menu-item"><a class="item-link" href="/ui/login">login</a></li>
				</ul>
				<button onclick="myFunction()" class="toggle-btn">
					<span></span>
					<span></span>
					<span></span>
				</button>
			</header>
		</div>
		<div class="main-container">
			<div class="top-block about-us">
				<h1 class="top-block__title">About Us</h1>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
			<div class="container">
				<div class="about-us__description">
					<div class="img-wr"><img src="/assets/ui/landing/images/about-us-desc.png"></div>
					<p class="description">We are marketers on a mission to guide you in the right direction. We are a team of tech rebels building tools that help our customers grow more and connect with their customers. We collaborate, learn, and strive to achieve great things.</p>
				</div>
			</div>
			<div class="footer-block">
				<h2 class="footer-block__title">Join Hundreds of Sales Teams Using ViewGen to Grow Their Business</h2>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
		</div>
		<footer>
			<div class="container">
				<a href="/" class="footer-logo"></a>
				<hr>
				<div class="contacts">
					<ul class="contact-list">
						<li class="contact-item">viewgentools.com</li>
						<li class="contact-item">+1 (888) 901-8532</li>
						<li class="contact-item">info@viewgentools.com</li>
						<li class="contact-item"><a href="/gdpr">GDPR</a></li>
						<li class="contact-item"><a href="/terms">Terms</a></li>
					</ul>
					<div class="footer-form-wr">
						<header class="subscribe-form-title">join our newsletter</header>
						<form class="subscribe-form" action="">
							<input class="field-email" type="text" placeholder="Your email here">
							<input class="btn" type="submit" value="subscribe">
						</form>
					</div>
				</div>
<!--				<p class="copyright">&copy;2018 Search Quant LLC. All rights reserved</p>-->
			</div>
		</footer>
		<script>
			function myFunction() {
				var x = document.getElementById("myTopnav");
				if (x.className === "header-menu") {
					x.className += " open";
				} else {
					x.className = "header-menu";
				}
			}
		</script>
	</body>
</html>