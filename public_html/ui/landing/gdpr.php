<?php 
include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;
?>
<html>
	<head>
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="stylesheet" href="/assets/ui/landing/css/styles.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header class="navbar">
				<a href="/" class="logo"></a>
				<ul class="header-menu" id="myTopnav">
					<li class="menu-item"><a class="item-link" href="/">home</a></li>
					<li class="menu-item"><a class="item-link" href="/about">about us</a></li>
					<li class="menu-item"><a class="item-link" href="/ui/login">login</a></li>
				</ul>
				<button onclick="myFunction()" class="toggle-btn">
					<span></span>
					<span></span>
					<span></span>
				</button>
			</header>
		</div>
		<div class="main-container">
			<div class="top-block about-us">
				<h1 class="top-block__title">GDPR Compliance</h1>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
			<div class="container">
				<div class="">
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%"><font face="Times New Roman, serif"><font size="5" style="font-size: 18pt"><b>GDPR
									Compliance</b></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">The
									EU General Data Protection Regulation (GDPR) is a comprehensive
									change to EU data privacy law. It takes effect on the 25th May 2018.
									The ViewGen team is working hard to ensure our fully compliant.</font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>What
										is the GDPR?</b></font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%"><a name="_GoBack"></a>
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">The
									General Data Protection Regulation (GDPR)
									(https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A32016R0679)
									is a regulation in EU law on data protection and privacy for all
									individuals within the European Union. For EUR residents, the
									regulation aims to increase their control over their personal data.
									For businesses, the GDPR becomes a unifying regulation across the EU.
									Once the GDPR takes effect on the 25th of May, it will replace the
									1995 Data Protection Directive.</font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Does
										this affect me?</b></font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">The
									GDPR regulation applies to any EU residents' data, regardless of
									where the processor or controller is located. This means that if
									you&rsquo;re using ViewGen from the US to reach out to other US
									corporations, the regulation doesn&rsquo;t affect you. But if some of
									your customers or leads are in the EU, you are required to look into
									it. In practice, most companies need to take the GDPR into
									consideration.</font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>How
										ViewGen is complying with the GDPR</b></font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Even
									though the GDPR only applies to data from EU residents, we took the
									decision to apply broadly the requirement of the regulation. This
									means we don&rsquo;t restrict any privacy related feature based on
									the geographical location of a data subject. Here are some of the
									actions we&rsquo;ve taken to ensure we&rsquo;re compliant:</font></font></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Security</b></font></font></font></p>
					</ul>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">We
									take the security of the data we manage seriously.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">The
									ViewGen servers are physically secure and may only be accessed by
									ViewGen's technical or support personnel whose jobs specifically
									relate to maintaining the integrity of the ViewGen servers or
									supporting product-related functions. Such individuals are required
									to maintain the security of the servers and the confidentiality of
									the information contained in the servers. ViewGen takes all
									reasonable and appropriate steps to protect your personal
									information, including by encrypting such information, maintaining
									readily accessible steps to limit access, working to detect
									unauthorized access, and not storing such information any longer than
									we need to.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Our
									privacy team has analyzed the requirements of the GDPR and is working
									to enhance our policies, procedures, contracts and platform features
									to ensure we comply with the GDPR and enable compliance for our
									customers.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Your
									data is protected between you and our systems. We take multiple steps
									to prevent eavesdropping between you and our systems, as well as
									within our infrastructure. All network traffic runs over SSL/HTTPS,
									the most common and trusted communications protocol on the Internet.
									Internal infrastructure is isolated using strict firewalls and
									network access lists. Each system is designated to a firewall
									security group by its function. By default, all access is denied and
									only explicitly allowed ports are exposed.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">If
									we see something, we&rsquo;ll react quickly and remedy the issue.
									We&rsquo;re not resting on our laurels. We&rsquo;re always looking
									for potential system interruptions. If we do find something out of
									place, we&rsquo;ll address the issue in a manner that it won&rsquo;t
									be an issue in the future. We&rsquo;ve invested in ensuring we can
									detect and respond to security events and incidents that impact its
									infrastructure.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">We&rsquo;re
									relentlessly updating our systems to protect your data. Our virtual
									systems are replaced on a regular basis with new, patched systems.
									System configuration and consistency are maintained using a
									combination of configuration management, up-to-date images and
									continuous deployment. Through continuous deployment, existing
									systems are decommissioned and replaced by up-to-date images at a
									regular interval.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Only
									people who need access, get access. Production system access is
									limited to key members of the ViewGen engineering team and passwords
									are expressly forbidden. At a minimum, authentication requires two
									factors including asymmetric RSA public/private keys and a time-based
									crypto token.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Don&rsquo;t
									just take our word that our systems are secure. We don&rsquo;t. Even
									though we&rsquo;ve designed secure systems and procedures, we
									regularly perform security tests to identify and remediate potential
									vulnerabilities. We also conduct periodic penetration tests with
									expert third-party vendors to help keep our applications safe and
									secure. These tests cover network, server, database and in-depth
									testing for vulnerabilities inside ViewGen applications.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">We
									prevent single points of failure. Even if there is an interruption to
									one system, the rest of our services stay up and secure. We
									physically separate the database instances from application servers
									and heartily believe in the mantra of single&shy; function servers.
									All login pages pass data via SSL/TLS for public and private
									networks, and only support certificates signed by well&shy; known
									Certificate Authorities (CAs). All email and CRM credential&shy;
									related data is encrypted while in transit as well as at&shy; rest
									using military grade encryption to ensure the security of user IDs
									and passwords. ViewGen application passwords are hashed and even our
									own staff can&rsquo;t retrieve them. If lost the password must be
									reset.</font></font></font></p>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">You
									can learn more about this topic on our Terms page.</font></font></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Log
											retention</b></font></font></font></p>
					</ul>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">To
									improve, debug or prevent fraud on the service, we keep a variety of
									logs. We now make sure logs are destroyed at most 3 months after
									there collection date. We never use those logs of anything else than
									monitoring and debugging.</font></font></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Data
											portability</b></font></font></font></p>
					</ul>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">The
									GDPR gives the right to any user to download any data that he
									provides to a service. This allows for easier migration to other
									services. We think this is a great idea and ViewGen has always made
									it possible for user to download their data.</font></font></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Systematic
											pseudonymisation of non-public data</b></font></font></font></p>
					</ul>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Our
									applications heavily pseudonymise data to ensure the privacy of data
									subjects. Any attributes that doesn&rsquo;t need to remain in it&rsquo;s
									original form is truncated to remove any possibility to be linked
									back to a specific data subject.</font></font></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Right
											of erasure</b></font></font></font></p>
					</ul>
					<p style="margin-left: 0.5in; margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Because
									we deal with publicly available web data, information removed from a
									website are also removed from our database. But if a data subject
									wishes to speed up the removal of any in our index, we offer a simple
									an efficient way to claim email addresses. It is then possible to
									either update the data or entirely remove it.</font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt"><b>Any
										other questions?</b></font></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 13pt">Our
									work related to the GDPR is still in progress and you can expect this
									and related pages to be updated regularly. Should you have any other
									question, we&rsquo;re here to help: info@viewgentools.com</font></font></font></p>
					<p style="margin-bottom: 0in; line-height: 100%"><br/>

					</p>

				</div>
			</div>
			<div class="footer-block">
				<h2 class="footer-block__title">Join Hundreds of Sales Teams Using ViewGen to Grow Their Business</h2>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
		</div>
		<footer>
			<div class="container">
				<a href="/" class="footer-logo"></a>
				<hr>
				<div class="contacts">
					<ul class="contact-list">
						<li class="contact-item">viewgentools.com</li>
						<li class="contact-item">+1 (888) 901-8532</li>
						<li class="contact-item">info@viewgentools.com</li>
						<li class="contact-item"><a href="/gdpr">GDPR</a></li>
						<li class="contact-item"><a href="/terms">Terms</a></li>
					</ul>
					<div class="footer-form-wr">
						<header class="subscribe-form-title">join our newsletter</header>
						<form class="subscribe-form" action="">
							<input class="field-email" type="text" placeholder="Your email here">
							<input class="btn" type="submit" value="subscribe">
						</form>
					</div>
				</div>
<!--				<p class="copyright">&copy;2018 Search Quant LLC. All rights reserved</p>-->
			</div>
		</footer>
		<script>
			function myFunction() {
				var x = document.getElementById("myTopnav");
				if (x.className === "header-menu") {
					x.className += " open";
				} else {
					x.className = "header-menu";
				}
			}
		</script>
	</body>
</html>