<?php 
include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;
?>
<html>
	<head>
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link rel="stylesheet" href="/assets/ui/landing/css/styles.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header class="navbar">
				<a href="/" class="logo"></a>
				<ul class="header-menu" id="myTopnav">
					<li class="menu-item"><a class="item-link" href="/">home</a></li>
					<li class="menu-item"><a class="item-link" href="/about">about us</a></li>
					<li class="menu-item"><a class="item-link" href="/ui/login">login</a></li>
				</ul>
				<button onclick="myFunction()" class="toggle-btn">
					<span></span>
					<span></span>
					<span></span>
				</button>
			</header>
		</div>
		<div class="main-container">
			<div class="top-block about-us">
				<h1 class="top-block__title">Terms of Service & Privacy Policy</h1>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
			<div class="container">
				<div class="">
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%"><font face="Times New Roman, serif"><font size="5" style="font-size: 18pt"><b>Terms
								of Service &amp; Privacy Policy</b></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">The following terms govern all
							use of the ViewGenTools.com website and all content, services and
							products available at, or through, the website, including, but not
							limited to, the ViewGenTools.com user management service (&quot;ViewGen&quot;),
							(taken together, the Services). The Services offered is subject to
							your acceptance without modification of all of the terms and
							conditions contained herein and all other operating rules, policies,
							and procedures that may be published from time to time on this Site
							by ViewGenTools.com (collectively, the &quot;Agreement&quot;). </font>
					</p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Please read this Agreement
							carefully before accessing or using the Services. By accessing or
							using any part of the website, you agree to become bound by the terms
							and conditions of this agreement. If you do not agree to all the
							terms and conditions of this agreement, then you may not access or
							use the Services. If these terms and conditions are considered an
							offer by ViewGen, acceptance is expressly limited to these terms. The
							Services are available only to individuals who are at least 18 years
							old. </font>
					</p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Your ViewGen Account</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">If you create an account on the
							Services, you are responsible for maintaining the security of your
							account, and you are fully responsible for all activities that occur
							under the account and any other actions taken in connection with the
							account. You must immediately notify ViewGen of any unauthorized uses
							of your account or any other breaches of security. ViewGen will not
							be liable for any acts or omissions by You, including any damages of
							any kind incurred as a result of such acts or omissions. You must be
							a human. Accounts registered by &quot;bots&quot; or other automated
							methods are not permitted. You must provide your legal full name, a
							valid email address, and any other information requested in order to
							complete the sign-up process. You must not, in the use of the
							Services, violate any laws in your jurisdiction and in United States
							of America (including but not limited to copyright or trademark
							laws). </font>
					</p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Privacy Policy</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen (&quot;ViewGen&quot; or
							&quot;We&quot;) is committed and determined in protecting the privacy
							of visitors to the ViewGen website and users of the ViewGen service
							(the &quot;Service&quot;). This privacy policy (the &quot;Policy) was
							prepared because we believe you (the &quot;User&quot;) should know as
							much as possible about the personal information we collect about you,
							why we collect it, how we use it, and when we share it with third
							parties.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><br/>
							ViewGen uses the personal
							information provided by the User to operate and enable the Service,
							to improve and customize your experience with the Service, and to
							send you Service related updates, notices and announcements. We may
							also use that information to notify the User about our new products,
							services, updates and other ViewGen information we think the User may
							find to be of interest.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><br/>
							In addition, this Policy
							describes your choices with respect to how we collect and use your
							information or data. You may exercise your choices about how we
							collect and use your information or data, consistent with this Policy
							at any time. Your continued use of the website (the &quot;Website&quot;)
							and our services, constitute your acceptance of this Policy and any
							updates thereto.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><br/>
							We are very conscious of
							the User's right to privacy regarding the information the User
							supplies to us. At ViewGen, our policy is to use the information
							exclusively to provide the User with better service. If you have
							questions or concerns regarding this Privacy Policy, you are welcome
							to contact us as provided in the Contacting Us at ViewGen section of
							this Privacy Policy.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><font size="4" style="font-size: 13pt"><b>The
									Information We collect for our Service.</b></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Aggregation of User Data</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen may aggregate the
							information you provide or capture with similar information collected
							from other visitors to the Website or users of the Service in order
							to evaluate or enhance the Website or its Services. Specifically,
							ViewGen may aggregate data to determine the usage patterns or
							interests of visitors to the Website or users of the Service, or for
							purposes related to the technical support or security of the Website
							and our computer systems. For example, we collect traffic data that
							is regularly generated with Users' online activities such as IP
							addresses, the name of the website from which you entered our
							website, session durations, viewed pages, type of mobile device and
							computer operating system. We use all of this information to analyze
							trends among our Users to help improve our Website. The aggregated
							data that we evaluate, contains no personal information or other
							information that could identify an individual.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Usage Tracking Within The
							</b></font><font face="Helvetica, serif"><b>&ldquo;</b></font><font face="Times New Roman, serif"><b>Service</b></font><font face="Helvetica, serif"><b>&rdquo;</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen may aggregate the
							information you provide with similar information collected from other
							visitors to the Website or users of the Service in order to evaluate
							or enhance the Website or its Services. Specifically, ViewGen may
							aggregate data to determine the usage patterns or interests of
							visitors to the Website or users of the Service, or for purposes
							related to the technical support or security of the Website and our
							computer systems. For example, we collect traffic data that is
							regularly generated with Users' online activities such as IP
							addresses, the name of the website from which you entered our
							website, session durations, viewed pages, type of mobile device and
							computer operating system. We use all of this information to analyze
							trends among our Users to help improve our Website. The aggregated
							data that we evaluate, contains no personal information or other
							information that could identify an individual.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Cookies. Yum.</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Not the ones you eat. Our Website
							uses &quot;Cookies&quot; to identify the areas of our Website that
							you have visited. A Cookie is a small piece of data stored on your
							computer or mobile device by your web browser. We use Cookies to
							personalize the Content that you see on our Website. Most web
							browsers can be set to disable the use of Cookies. However, if you
							disable Cookies, you may not be able to access functionality on our
							Website correctly or at all.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Information to Certain Third
								Parties</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen, on occasion, may provide
							some general usage information from the Products to our data
							suppliers for royalty-related purposes. Any information that we may
							provide to our data suppliers contains no personal details about
							individual users. Additionally, no information that we collect from
							the Website that you provide to us is ever sold to third parties.
							When appropriate or required we may disclose information, including
							personal information, collected from the Website to protect our legal
							rights or in response to a court order, or in connection with a sale
							or merger of ViewGen or its assets. ViewGen will endeavor to make
							such disclosures subject to protective orders or confidentiality
							agreements. With respect to information collected in the course of
							our marketing activities, ViewGen may exchange business leads with
							our affiliates from time to time. Additionally, we may, on limited
							occasions, share your personal information with third-party marketing
							partners. These partners generally consist of persons or entities
							with whom we maintain a relationship and which offer certain
							complementary products and services to those Services that we offer.
							We will take commercially reasonable efforts to make sure that such
							third parties safeguard your personal information. Further, you are
							permitted at any time to request that we refrain from sharing your
							personal information with such third parties, or that we disclose who
							we shared your personal information with. In order to make such
							requests, please follow the instructions set forth below with regard
							to requests made in connection with your personal information.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Receipt of Communications</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">If you decide at any time that
							you no longer wish to receive e-mails from us you may opt out by
							updating your ViewGen account. You may still receive certain
							communications from us at your request or at the request of your
							organization.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Server Security </b></font><font face="Helvetica, serif"><b>&ndash;</b></font><font face="Times New Roman, serif"><b>
								Click here to read GDPR Compliance</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">The ViewGen servers are
							physically secure and may only be accessed by ViewGen's technical or
							support personnel whose jobs specifically relate to maintaining the
							integrity of the ViewGen servers or supporting product-related
							functions. Such individuals are required to maintain the security of
							the servers and the confidentiality of the information contained in
							the servers. ViewGen takes all reasonable and appropriate steps to
							protect your personal information, including by encrypting such
							information, maintaining readily accessible steps to limit access,
							working to detect unauthorized access, and not storing such
							information any longer than we need to.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Third Party Sites</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">As a visitor to the Website or a
							subscriber to our Services, you may decide to visit a third party's
							website through links available on our Website. Although the ViewGen
							branding may still be visible, you will be subject to that
							third-party's privacy policy, if one exists. We encourage you to
							review the privacy policies of third-party websites you choose to
							visit. ViewGen is not responsible for how that third-party may
							collect or use your personal information.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><font size="4" style="font-size: 13pt"><b>Payment
									and Renewal.</b></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>General Terms</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">By upgrading to a paid account,
							you agree to pay ViewGen the monthly or annual subscription fees
							indicated for that service (additional payment terms are described
							below). Payments will be charged on a pre-pay basis on the day you
							sign up and after your trial period. Paid accounts will cover the use
							of that service for a monthly or annual subscription period as
							indicated. Any payments made to ViewGen are not refundable.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Automatic Renewal</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Unless you notify ViewGen before
							the end of the applicable subscription period that you want to cancel
							a paid account, your subscription will automatically renew and you
							authorize us to collect the then-applicable annual or monthly
							subscription fee for such subscription (as well as any taxes) using
							any credit card or other payment mechanism we have on record for you.
							Paid accounts can be canceled at any time in the Settings section of
							your account's dashboard or contacting our support team at
							info@viewgentools.com</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>ViewGen Fees and payments</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">By signing up for an ViewGen
							account you agree to pay ViewGen the monthly fees indicated to you
							when you registered for the service in exchange for the ViewGen
							services. Applicable fees will be invoiced starting from the day your
							paid account is established and in advance of using such services.
							ViewGen reserves the right to change the payment terms and fees upon
							thirty (30) days prior written notice to you. ViewGen accounts can be
							canceled by you at anytime with 30 days written notice to ViewGen.
							Written notices refer to, but are not limited to, emails.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Support</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen includes access to email
							support. Email support means the ability to make requests for
							technical support assistance by email at any time (with reasonable
							efforts by ViewGen to respond within three business days) concerning
							the use of ViewGen. All ViewGen support will be provided in
							accordance with ViewGen standard ViewGen practices, procedures, and
							policies.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><font size="4" style="font-size: 13pt"><b>ViewGen
									API</b></font></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Any use of the API, including use
							of the API through a third-party product that accesses ViewGen, is
							bound by these Terms of Service plus the following specific terms:</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Damage and liability</b></font></p>
					<ul>
						<li/>
						<p style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%">
							<font face="Times New Roman, serif">No one has been banned for using
								ViewGen, but we wouldn't be responsible in case this happens. We
								will never sell any of your information to third parties.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">ViewGen
								has no affiliation with LinkedIn or any third-party partners in any
								way.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">It
								is your sole responsibility to comply with LinkedIn rules and any
								legislation that you are subject to. You use ViewGen at your own
								risk.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">We
								are not responsible for your actions and their consequences. We are
								not to blame in the unlikely case of your accounts getting banned
								for any reason.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">We
								require your ViewGen API access to obtain required information for
								interacting with the APIs. We don't store, give away, or otherwise
								distribute your information to any third parties.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">We
								can't guarantee any specific outcome from using ViewGen.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">We
								can't guarantee the continuous, uninterrupted or error-free
								operation of the service.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">Before
								you make a purchase decision you are advised to try ViewGen with our
								Free Trial.</font></p>
						<li/>
						<p style="margin-bottom: 0in; line-height: 100%"><font face="Times New Roman, serif">We
								reserve the right to modify, suspend or withdraw the whole or any
								part of our service or any of its content at any time without notice
								and without incurring any liability.</font></p>
						<li/>
						<p style="margin-bottom: 0.19in; line-height: 100%"><font face="Times New Roman, serif">You
								expressly understand and agree that ViewGen shall not be liable for
								any direct, indirect, incidental, special, consequential or
								exemplary damages, including but not limited to: damages for loss of
								profits, goodwill, use, data or other intangible losses (even if
								ViewGen has been advised of the possibility of such damages),
								resulting from your use of the API or third-party products that
								access data via the API.</font></p>
					</ul>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Rate limiting</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Abuse or excessively frequent
							requests to ViewGen via the API may result in the temporary or
							permanent suspension of your account's access to the API. ViewGen, in
							its sole discretion, will determine abuse or excessive usage of the
							API. ViewGen, in its sole discretion, may make an attempt via email
							to warn the account owner prior to suspension.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Discontinuation</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen reserves the right at any
							time to modify or discontinue, temporarily or permanently, your
							access to the API (or any part thereof) with or without notice.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Marketing</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">As a user, you give ViewGen a
							perpetual world-wide license to use your company's assets and logos,
							unless ViewGen agrees in writing otherwise. These assets and logos
							will be used purely for marketing and sales efforts, such as being
							displayed on the homepage and potentially e-mail marketing campaigns.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Copyright Infringement</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">As ViewGen asks others to respect
							its intellectual property rights, it respects the intellectual
							property rights of others. If you believe that material located on or
							linked to by ViewGen violates your copyright, you are encouraged to
							notify ViewGen. ViewGen will respond to all such notices, including
							as required or appropriate by removing the infringing material or
							disabling all links to the infringing material. ViewGen will
							terminate a visitor's access to and use of the Services if, under
							appropriate circumstances, the visitor is determined to be a repeat
							infringer of the copyrights or other intellectual property rights of
							ViewGen or others. In the case of such termination, ViewGen will have
							no obligation to provide a refund of any amounts previously paid to
							ViewGen. You agree that upon purchasing our service, that you clearly
							understand and agree what you are purchasing and will not file a
							fraudulent dispute.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Intellectual Property</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">This Agreement does not transfer
							from ViewGen to you any ViewGen or third party intellectual property,
							and all right, title and interest in and to such property will remain
							(as between the parties) solely with ViewGen. ViewGen, the ViewGen
							logo, and all other trademarks, service marks, graphics and logos
							used in connection with ViewGen, or the Services are trademarks or
							registered trademarks of ViewGen or ViewGen licensors. Other
							trademarks, service marks, graphics and logos used in connection with
							the Services may be the trademarks of other third parties. Your use
							of the Services grants you no right or license to reproduce or
							otherwise use any ViewGen or third-party trademarks.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Use License</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Under this license you may not:</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Attempt to decompile or reverse
							engineer any software contained on ViewGen's web site or API.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Access the Site or Services in
							order to build a similar or competitive service.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">This license shall automatically
							terminate if you violate any of these restrictions and may be
							terminated by ViewGen at any time.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Changes</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen reserves the right, at
							its sole discretion, to modify or replace any part of this Agreement.
							It is your responsibility to check this Agreement periodically for
							changes. Your continued use of or access to the Services following
							the posting of any changes to this Agreement constitutes acceptance
							of those changes. ViewGen may also, in the future, offer new services
							and/or features through the Services (including, the release of new
							tools and resources). Such new features and/or services shall be
							subject to the terms and conditions of this Agreement.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Termination</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">ViewGen may terminate your access
							to all or any part of the Services at any time, with or without
							cause, with or without notice, effective immediately. If you wish to
							terminate this Agreement or your ViewGen account (if you have one),
							you may simply discontinue using the Services. Notwithstanding the
							foregoing, if you have a paid ViewGen account, such account can only
							be terminated by ViewGen if you materially breach this Agreement and
							fail to cure such breach within thirty (30) days from ViewGen's
							notice to you thereof; provided that, ViewGen can terminate the
							Services immediately as part of a general shut down of our service.
							All provisions of this Agreement which by their nature should survive
							termination shall survive termination, including, without limitation,
							ownership provisions, warranty disclaimers, indemnity and limitations
							of liability.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Disclaimer of Warranties</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">The Services is provided &quot;as
							is&quot;. ViewGen and its suppliers and licensors hereby disclaim all
							warranties of any kind, express or implied, including, without
							limitation, the warranties of merchantability, fitness for a
							particular purpose and non-infringement. Neither ViewGen nor its
							suppliers and licensors, makes any warranty that the Services will be
							error free or that access thereto will be continuous or
							uninterrupted. You understand that you download from, or otherwise
							obtain content or services through, the Services at your own
							discretion and risk.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Limitation of Liability</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">In no event will ViewGen, or its
							suppliers or licensors, be liable with respect to any subject matter
							of this agreement under any contract, negligence, strict liability or
							other legal or equitable theory for: (i) any special, incidental or
							consequential damages; (ii) the cost of procurement for substitute
							products or services; (iii) for interruption of use or loss or
							corruption of data; or (iv) for any amounts that exceed the fees paid
							by you to ViewGen under this agreement during the twelve (12) month
							period prior to the cause of action. ViewGen shall have no liability
							for any failure or delay due to matters beyond or not within their
							reasonable control. The foregoing shall not apply to the extent
							prohibited by applicable law.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>General Representation and
								Warranty</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">You represent and warrant that
							(i) your use of the Services will be in strict accordance with the
							ViewGen Privacy Policy, with this Agreement and with all applicable
							laws and regulations (including without limitation any local laws or
							regulations in your country, state, city, or other governmental area,
							regarding online conduct and acceptable content, and including all
							applicable laws regarding the transmission of technical data exported
							from United State of America or the country in which you reside) and
							(ii) your use of the Services will not infringe or misappropriate the
							intellectual property rights of any third party.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Indemnification</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">You agree to indemnify and hold
							harmless ViewGen, its contractors, its licensors, and their
							respective directors, officers, employees and agents from and against
							any and all claims and expenses, including attorneys' fees, arising
							out of your use of the Services, including but not limited to your
							violation of this Agreement.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif"><b>Miscellaneous</b></font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%"><a name="_GoBack"></a>
						<font face="Times New Roman, serif">This Agreement constitutes the
							entire agreement between ViewGen and you concerning the subject
							matter hereof, and they may only be modified by a written amendment
							signed by an authorized executive of ViewGen, or by the posting by
							ViewGen of a revised version. Except to the extent applicable law, if
							any, provides otherwise, this Agreement, any access to or use of the
							Services will be governed by the laws of United State of America,
							excluding its conflict of law provisions, and the proper venue for
							any disputes arising out of or relating to any of the same will be
							the Santa Cruz County Court, Santa Cruz, CA United States of America.
							Any legal action or arbitration shall take place in United State of
							America, in the English language. The prevailing party in any action
							or proceeding to enforce this Agreement shall be entitled to costs
							and attorneys' fees. If any part of this Agreement is held invalid or
							unenforceable, that part will be construed to reflect the parties'
							original intent, and the remaining portions will remain in full force
							and effect. A waiver by either party of any term or condition of this
							Agreement or any breach thereof, in any one instance, will not waive
							such term or condition or any subsequent breach thereof. You may
							assign your rights under this Agreement to any party that consents
							to, and agrees to be bound by, its terms and conditions; ViewGen may
							assign its rights under this Agreement without condition. This
							Agreement will be binding upon and will inure to the benefit of the
							parties, their successors and permitted assigns.</font></p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						&nbsp;</p>
					<p style="margin-top: 0.19in; margin-bottom: 0.19in; line-height: 100%">
						<font face="Times New Roman, serif">Any questions about this Terms of
							Service &amp; Privacy Policy should be addressed to
						</font><font color="#0000ff"><u><a href="mailto:info@viewgentools.com"><font face="Times New Roman, serif">info@viewgentools.com</font></a></u></font><font face="Times New Roman, serif">
						</font>
					</p>
					<p style="margin-bottom: 0in; line-height: 100%"><br/></p>
				</div>
			</div>
			<div class="footer-block">
				<h2 class="footer-block__title">Join Hundreds of Sales Teams Using ViewGen to Grow Their Business</h2>
				<a class="btn-consultation" href="mailto:info@viewgentools.com">request consultation</a>
			</div>
		</div>
		<footer>
			<div class="container">
				<a href="/" class="footer-logo"></a>
				<hr>
				<div class="contacts">
					<ul class="contact-list">
						<li class="contact-item">viewgentools.com</li>
						<li class="contact-item">+1 (888) 901-8532</li>
						<li class="contact-item">info@viewgentools.com</li>
                        <li class="contact-item"><a href="/gdpr">GDPR</a></li>
                        <li class="contact-item"><a href="/terms">Terms</a></li>
                    </ul>
					<div class="footer-form-wr">
						<header class="subscribe-form-title">join our newsletter</header>
						<form class="subscribe-form" action="">
							<input class="field-email" type="text" placeholder="Your email here">
							<input class="btn" type="submit" value="subscribe">
						</form>
					</div>
				</div>
<!--				<p class="copyright">&copy;2018 Search Quant LLC. All rights reserved</p>-->
			</div>
		</footer>
		<script>
			function myFunction() {
				var x = document.getElementById("myTopnav");
				if (x.className === "header-menu") {
					x.className += " open";
				} else {
					x.className = "header-menu";
				}
			}
		</script>
	</body>
</html>