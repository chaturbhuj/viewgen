<?php

namespace ui\api\catch_email_captcha;

include_once(__DIR__.'/../../services/ajax_svc.php');
include_once(__DIR__.'/../../services/db_svc.php');
include_once(__DIR__.'/../../services/captcha_svc.php');

use function \services\ajax\{json_fail, json_ok, get_request_data};
use \services\db\DBException;
use function \services\captcha\validate;

function run(){
    $contact_types = ['contact_us' => 'contact_us', 'lead_in' => 'lead_in', 'download' => 'download'];
    
    $data = get_request_data([
        'POST' => [['contact_type'], ['email'], ['name'], ['phone', null], ['message', null]]
    ]);
    
    if(validate($_POST)) {
        if (! array_key_exists($data['contact_type'], $contact_types)){
            json_fail('Invalid contact_type: ' . $data['contact_type']);
        }
        
        try {
            
            $toEmail = 'info@viewgentools.com'; 
           	$theSubject = 'Contact request [' . $data["contact_type"] .'] from ' . $data['name'];
           	$messageToSend = "Your Name: ".$data['name']."\r\n";
           	$messageToSend .= "Your Email: ".$data['email']."\r\n";
           	$messageToSend .= "Your Message:\r\n".$data['message']."\r\n";
           	$theHeaders = "From: " . $data['name'] . " <no-reply@viewgentools.com>\r\n" .
           		"Reply-To: " . $data['email'] . "\r\n" .
           		"X-Mailer: PHP/" . phpversion();
           
           	mail($toEmail, $theSubject, $messageToSend, $theHeaders);

            $query = \DB::prep("INSERT INTO front_page_emails (contact_type, name, email, phone, message) VALUES(?, ?, ?, ?, ?)");
           	$query->execute([
                $data['contact_type'],
                $data['name'],
                $data['email'],
                $data['phone'],
           		$data['message'],
           	]);
           
           	include(__DIR__ . '/../../classes/mailchimp/Mailchimp.php');
           	$mailChimp = new \MailChimp("4ebb3ecef8c8440befdf8860f534292d-us8");
           
           	$listId = "7c8831719a";
           
           	try {
           		$mailChimp->lists->subscribe($listId, ['email' => $data["email"]], [], 'html', true, true);
           	} catch (\Exception $error) {
           	}
           
            json_ok('Your response is saved');
        }catch (DBException $e){
            json_fail($e->toArray());
        } catch (\Exception $e) {
            json_fail($e->getMessage());
        }
    } else {
        json_fail('Wrong Captcha!');
    }
    
}

run();

?>

