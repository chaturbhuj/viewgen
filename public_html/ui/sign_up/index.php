<?php

require_once(__DIR__ . '/../common/render_page.php');
use function ui\common\render_page;

require_once __DIR__.'/../../intercom/helper.php';
require_once __DIR__ . '/parts/sign_up_init.php';
require_once(__DIR__ . '/../../lib/recurly.php');

require_once __DIR__.'/../../classes/Referrer.php';

global $errors;

function getCompanyById($id){
    $query = DB::prep("SELECT * FROM company WHERE id = ?");
    $query->execute(array($id));
    return $query->fetch();
}

function processPost(){
    global $CONFIG;
    $email = trim($_POST["email"]);
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $phone = $_POST['phone'];
    $company_name = trim($_POST['company_name']);
    $company_ = trim($_POST['company']);
    $password1 = $_POST["password1"];
    $password2 = $_POST["password2"];
    $coupon = $_POST["coupon"];

    if($first_name == ""){
        $errors['first_name'] = "First Name is missing";
    }
    if($last_name == ""){
        $errors['last_name'] ="Last Name is missing";
    }
    if($email == ""){
        $errors['email'] = "Email is missing";
    }
    if(!isValidEmail($email)){
        $errors['email'] = "\"".$email."\" is not a valid email";
    }
    if ($password1 == "") {
        $errors['password1'] = "Password is missing";
    }
    if (strlen($password1) < 6) {
        $errors['password1'] = "Password is too short - must be at least 6 characters";
    }
    if ($password1 != $password2) {
        $errors['password2'] = "Password and repeat password did not match";
    }
    if ($company_name == "") {
        $errors['company_name'] = 'Missing account name';
    }
    if (strlen($company_name) < 4) {
        $errors['company_name'] = 'Account Name is too short - must be at least 4 characters';
    }

    $query = DB::prep("SELECT * FROM company WHERE email LIKE ?");
    $query->execute(array($email));
    $account = $query->fetch();

    if(!empty($account) && $account['state'] !== "draft"){
        $errors['email'] = 'The email is already in use, try to <a href="/ui/login/">Login</a>';
    }

    $query = DB::prep("
        SELECT 
            * 
        FROM 
            company 
        WHERE 
            company_name LIKE ?
            AND id <> ?
    ");
    $query->execute(array($company_name, !empty($account) ? intval($account['id']) : 0));
    if ($query->rowCount() > 0) {
        $errors['company_name'] = "Account Name is already taken";
    }

    if(!empty($errors)){
        return $errors;
    }

    if($account) {
        // Update draft company
        $query = DB::prep("
            UPDATE 
                company
            SET
                first_name = :first_name,
                last_name = :last_name, 
                phone = :phone,
                company_name = :company_name,
                company = :company
            WHERE
                id = :company_id
        ");
        $query->execute([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'phone' => $phone,
            'company_name' => $company_name,
            'company' => $company_,
            'company_id' => $account['id']
        ]);

        $_SESSION['viewgen_signup']['company_id'] = $account['id'];
        $company = getCompanyById($_SESSION['viewgen_signup']['company_id']);
    } else {
        // Create the company as a draft.
        $query = DB::prep("INSERT INTO company (state, company_name, company, first_name, last_name, email, phone, client_hash) VALUES('draft', :company_name, :company, :first_name, :last_name, :email, :phone, :client_hash)");
        $query->execute([
            'client_hash' => uniqid(),
            'company_name' => $company_name,
            'company' => $company_,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone
        ]);

        $_SESSION['viewgen_signup']['company_id'] = DB::get_instance()->lastInsertId();
        $company = getCompanyById($_SESSION['viewgen_signup']['company_id']);

        try{
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->pushCompanyData($company);
            $intercomHelper->pushSignUpStarted($company);
        } catch (Exception $error) {

        }
    }


    try {
        $subscription = new Recurly_Subscription();
        $subscription->plan_code = 'vg-basic';
        $subscription->currency = 'USD';
        $subscription->quantity = 1;

        if (isset($coupon) && $coupon != '') {
            $subscription->coupon_code = $coupon;
        }

        $newRecurlyId = uniqid();

        $subscription->account = new Recurly_Account();
        $subscription->account->account_code = $newRecurlyId;
        $subscription->account->first_name = $company['first_name'];
        $subscription->account->last_name = $company['last_name'];
        $subscription->account->email = $company['email'];
        $subscription->account->company_name = $company['company_name'];

        $subscription->create();
    } catch (Exception $error) {
        $errors['coupon'] = ' Billing provider: ' .  $error->getMessage();
        return $errors;
    }

    $salt = MD5($company["first_name"] . $company["last_name"] . time());
    $hash_password = make_hash_password($password1,$salt);
    $client_hash = substr($company["company_name"].MD5($company["first_name"].time().time()),0,30);

    // Just update the company that is a draft.
    $query = DB::prep("
        UPDATE 
            company 
        SET
            state = '',
            account_type = 'paid',
            subscription_plan_code = :subscription_plan_code,
            password_salt = :password_salt,
            password_hash = :password_hash,
            client_hash = :client_hash,
            referer = :referer,
            company_runs_query_slicer = 1,
            recurlyAccountCode = :recurlyAccountCode
        WHERE 
            id = :company_id 
        LIMIT 1
    ");
    $query->execute([
        'subscription_plan_code' => 'vg-basic',
        'password_salt' => $salt,
        'password_hash' => $hash_password,
        'client_hash' => $client_hash,
        'referer' => "",
        'recurlyAccountCode' => $newRecurlyId,
        'company_id' => $company['id']
    ]);

    unset($_SESSION['viewgen_signup']);

    // Send out email.
    try {
        include_once(__DIR__.'/../../classes/Email.php');
        include_once(__DIR__.'/../../classes/EmailTemplate.php');
        $email = new Email();
        $emailTemplate = new EmailTemplate($company['id']);
        $email->setHtmlBody(' ');
        $email->setTextBody(' ');
        $email->setSubject(' ');
        $email->setTemplateId('4c483055-fa49-423b-85ae-8aea85310452');
        $email->addTo($company['email']);
        $email->addSubstitution('{{unsubscribe_link}}', [$emailTemplate->generateUnsubscrubeLink()]);

        $email->send();
    } catch (Exception $error) {

    }

    try{
        $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
        $company = getCompanyById($company['id']);
        $intercomHelper->pushCompanyData($company);
        $intercomHelper->pushSignUpFinished($company);
    } catch (Exception $error) {

    }

    if(!empty($_GET['referrer'])){
        try{
            $Referrer = new Referrer();
            $Referrer->connect($_GET['referrer'], $company['email'], $company['id']);
        } catch (Exception $e){

        }
    }



    // Set session.
    $_SESSION['auth'] = array(
        'user_id' => 0,
        'company_id' => $company['id']
    );

    return [];
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postErrors = processPost();
    if(empty($postErrors)){
        redirect("/new-ui/download.php");
//        header('Location: /ui/sign_up/billing-information.php');
        die;
    }
    $errors = array_merge($errors, $postErrors);
}

//first time GEt or reload with errors
echo render_page([__DIR__ . '/templates'],'index', [
    'title'=>'START YOUR FREE TRIAL TODAY',
    'errors' => $errors,
    'data' => $_SESSION['viewgen_signup']
]);


