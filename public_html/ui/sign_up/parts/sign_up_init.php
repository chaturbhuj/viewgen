<?php
$errors = [];

if (isset($_SESSION['error_message'])) {
	$errors['form'] = $_SESSION['error_message'];
	unset($_SESSION['error_message']);
}

if (!isset($_SESSION['viewgen_signup'])) $_SESSION['viewgen_signup'] = [];
foreach ($_POST as $name => $value) {
	$_SESSION['viewgen_signup'][$name] = $value;
}

?>