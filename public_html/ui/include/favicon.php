<link rel="icon" type="image/png" href="/assets/images/favicons/fav16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="/assets/images/favicons/fav36x36.png" sizes="36x36">
<link rel="icon" type="image/png" href="/assets/images/favicons/fav48x48.png" sizes="48x48">
