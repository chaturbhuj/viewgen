<?php
namespace ui\common;

include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;

require(__DIR__ . '/../../services/template_svc.php');

use function services\template\get_twig;

function render_page($templates_root_folders, $index_template_name, $template_args){
    $common_data = [
        'assets_version' => ASSETS_VERSION
    ];
    
    if (! is_array($templates_root_folders)){
        $templates_root_folders = [$templates_root_folders];
    }
    
    $templates_root_folders[] = __DIR__ . '/..';
    
    $twig = get_twig($templates_root_folders);
    return $twig($index_template_name, array_merge($common_data, $template_args));
}

?>