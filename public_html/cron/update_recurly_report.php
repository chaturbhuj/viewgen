<?php
/**
 * User: mihael
 * Date: 26.06.17
 * Time: 16:25
 */
$IS_CRON_JOB = true;

require_once (__DIR__.'/../db.php');

require_once (__DIR__.'/../lib/recurly.php');

echo "\r\n Sync accounts \r\n";

$accountsIds = [];
$accounts = Recurly_AccountList::get();
foreach ($accounts as $account){
    echo ".";
    $accountsIds[] = $account->account_code;
    $query = DB::prep("SELECT id FROM rc_account_report WHERE account_code = ?");
    $storedAccountId = $query->execute([$account->account_code]) ? $query->fetchColumn() : null;
    if($storedAccountId) {
        DB::prep("
          UPDATE rc_account_report 
          SET state = :state, 
              email = :email, 
              first_name = :first_name, 
              last_name = :last_name,
              company_name = :company_name,
              created_at = :created_at, 
              updated_at = :updated_at, 
              closed_at = :closed_at,
              has_past_due_invoice = 0,
              has_active_subscription = 1
          WHERE id = :id
        ")->execute([
            'state'         => $account->state,
            'email'         => $account->email,
            'first_name'    => $account->first_name,
            'last_name'     => $account->last_name,
            'company_name'  => $account->company_name,
            'created_at'    => $account->created_at ? date_format($account->created_at, 'Y-m-d H:i:s') : null,
            'updated_at'    => $account->updated_at ? date_format($account->updated_at, 'Y-m-d H:i:s') : null,
            'closed_at'     => $account->closed_at ? date_format($account->closed_at, 'Y-m-d H:i:s') : null,
            'id'            => $storedAccountId
        ]);
    }else{
        DB::prep("
          INSERT INTO rc_account_report (
            account_code,
            state,
            email,
            first_name,
            last_name,
            company_name,
            created_at,
            updated_at,
            closed_at,
            has_active_subscription,
            has_past_due_invoice
          ) VALUES (
            :account_code,
            :state,
            :email,
            :first_name,
            :last_name,
            :company_name,
            :created_at,
            :updated_at,
            :closed_at,
            1,
            0
          ) 
        ")->execute([
            'account_code'  => $account->account_code,
            'state'         => $account->state,
            'email'         => $account->email,
            'first_name'    => $account->first_name,
            'last_name'     => $account->last_name,
            'company_name'  => $account->company_name,
            'created_at'    => $account->created_at ? date_format($account->created_at, 'Y-m-d H:i:s') : null,
            'updated_at'    => $account->updated_at ? date_format($account->updated_at, 'Y-m-d H:i:s') : null,
            'closed_at'     => $account->closed_at ? date_format($account->closed_at, 'Y-m-d H:i:s') : null,
        ]);
    }
}

$accounts = Recurly_AccountList::get(['state' => 'past_due']);
foreach ($accounts as $account){
    DB::prep("
          UPDATE rc_account_report 
          SET has_past_due_invoice = 1 
          WHERE account_code = :account_code
        ")->execute([
        'account_code'         => $account->account_code
    ]);
}

$accounts = Recurly_AccountList::get(['state' => 'non_subscriber']);
foreach ($accounts as $account){
    DB::prep("
          UPDATE rc_account_report 
          SET has_active_subscription = 0 
          WHERE account_code = :account_code
        ")->execute([
        'account_code'         => $account->account_code
    ]);
}

//----------------------------------------------------------------------------------------------------------------------
echo "\r\n Sync subscriptions \r\n";

$subscriptionsIds = [];
$subscriptions = Recurly_SubscriptionList::get();
foreach ($subscriptions as $subscription){
    echo ".";
    $subscriptionsIds[] = $subscription->uuid;

    $p = explode("/",$subscription->account->getHref());
    $accountCode = $p[count($p)-1];
    $query = DB::prep("SELECT id FROM rc_account_report WHERE account_code = ?");
    $rcAccountReportId = $query->execute([$accountCode]) ? $query->fetchColumn() : null;

    $query = DB::prep("SELECT uuid FROM rc_subscription_report WHERE uuid = ?");
    $storedSubscriptionId = $query->execute([$subscription->uuid]) ? $query->fetchColumn() : null;
    if($storedSubscriptionId) {
        DB::prep("
          UPDATE rc_subscription_report 
          SET rc_account_report_id = :rc_account_report_id,
              state = :state,
              plan_code = :plan_code,
              currency = :currency,
              unit_amount_in_cents = :unit_amount_in_cents,
              quantity = :quantity,
              activated_at = :activated_at,
              canceled_at = :canceled_at,
              expires_at = :expires_at,
              current_period_started_at = :current_period_started_at,
              current_period_ends_at = :current_period_ends_at,
              trial_started_at = :trial_started_at,
              trial_ends_at = :trial_ends_at,
              has_past_due_invoice = 0
          WHERE uuid = :uuid
        ")->execute([
            'rc_account_report_id' => $rcAccountReportId,
            'state' => $subscription->state,
            'plan_code' => $subscription->plan->plan_code,
            'currency' => $subscription->currency,
            'unit_amount_in_cents' => $subscription->unit_amount_in_cents,
            'quantity' => $subscription->quantity,
            'activated_at' => $subscription->activated_at ? date_format($subscription->activated_at, 'Y-m-d H:i:s') : null,
            'canceled_at' => $subscription->canceled_at ? date_format($subscription->canceled_at, 'Y-m-d H:i:s') : null,
            'expires_at' => $subscription->expires_at ? date_format($subscription->expires_at, 'Y-m-d H:i:s') : null,
            'current_period_started_at' => $subscription->current_period_started_at ? date_format($subscription->current_period_started_at, 'Y-m-d H:i:s') : null,
            'current_period_ends_at' => $subscription->current_period_ends_at ? date_format($subscription->current_period_ends_at, 'Y-m-d H:i:s') : null,
            'trial_started_at' => $subscription->trial_started_at ? date_format($subscription->trial_started_at, 'Y-m-d H:i:s') : null,
            'trial_ends_at' => $subscription->trial_ends_at ? date_format($subscription->trial_ends_at, 'Y-m-d H:i:s') : null,
            'uuid'            => $storedSubscriptionId
        ]);
    }else{
        DB::prep("
          INSERT INTO rc_subscription_report (
            uuid,
            rc_account_report_id,
            state,
            plan_code,
            currency,
            unit_amount_in_cents,
            quantity,
            activated_at,
            canceled_at,
            expires_at,
            current_period_started_at,
            current_period_ends_at,
            trial_started_at,
            trial_ends_at,
            has_past_due_invoice
          ) VALUES (
            :uuid,
            :rc_account_report_id,
            :state,
            :plan_code,
            :currency,
            :unit_amount_in_cents,
            :quantity,
            :activated_at,
            :canceled_at,
            :expires_at,
            :current_period_started_at,
            :current_period_ends_at,
            :trial_started_at,
            :trial_ends_at,
            0
          ) 
        ")->execute([
            'rc_account_report_id' => $rcAccountReportId,
            'state' => $subscription->state,
            'plan_code' => $subscription->plan->plan_code,
            'currency' => $subscription->currency,
            'unit_amount_in_cents' => $subscription->unit_amount_in_cents,
            'quantity' => $subscription->quantity,
            'activated_at' => $subscription->activated_at ? date_format($subscription->activated_at, 'Y-m-d H:i:s') : null,
            'canceled_at' => $subscription->canceled_at ? date_format($subscription->canceled_at, 'Y-m-d H:i:s') : null,
            'expires_at' => $subscription->expires_at ? date_format($subscription->expires_at, 'Y-m-d H:i:s') : null,
            'current_period_started_at' => $subscription->current_period_started_at ? date_format($subscription->current_period_started_at, 'Y-m-d H:i:s') : null,
            'current_period_ends_at' => $subscription->current_period_ends_at ? date_format($subscription->current_period_ends_at, 'Y-m-d H:i:s') : null,
            'trial_started_at' => $subscription->trial_started_at ? date_format($subscription->trial_started_at, 'Y-m-d H:i:s') : null,
            'trial_ends_at' => $subscription->trial_ends_at ? date_format($subscription->trial_ends_at, 'Y-m-d H:i:s') : null,
            'uuid'            => $subscription->uuid
        ]);
    }
}

$subscriptions = Recurly_SubscriptionList::get(['state' => 'past_due']);
foreach ($subscriptions as $subscription){
    DB::prep("
          UPDATE rc_subscription_report 
          SET has_past_due_invoice = 1 
          WHERE uuid = :uuid
        ")->execute([
        'uuid'         => $subscription->uuid
    ]);
}
//----------------------------------------------------------------------------------------------------------------------
echo "\r\n Clean up subscriptions \r\n";

$query = DB::prep("SELECT uuid FROM rc_subscription_report");
$query->execute();

while($subscriptionId = $query->fetchColumn()){
    echo ".";
    if(!in_array($subscriptionId, $subscriptionsIds)){
        $queryDelete = DB::prep("DELETE FROM rc_subscription_report WHERE uuid = ?");
        $queryDelete->execute([$subscriptionId]);
    }
};

echo "\r\n Clean up accounts \r\n";

$query = DB::prep("SELECT account_code FROM rc_account_report");
$query->execute();

while($accountId = $query->fetchColumn()){
    echo ".";
    if(!in_array($accountId, $accountsIds)){
        $queryDelete = DB::prep("DELETE FROM rc_account_report WHERE account_code = ?");
        $queryDelete->execute([$accountId]);
    }
};
