<?php
$IS_CRON_JOB = true;

chdir(dirname(__FILE__));

include (__DIR__ . '/../db.php');

$query = DB::prep("SELECT * FROM li_users");
$query->execute();

$queryFetch = DB::prep("SELECT
		people.company_id,
		people.crawled_by_id,
		COUNT(*) count,
		people.sent_to_crawler_date date,
		COUNT(people_flags.value) invites_count
	FROM people LEFT JOIN people_flags ON people.id = people_flags.people_id AND people_flags.flag_id = 1 
	WHERE
		people.company_id = :company_id AND
		people.crawled_by_id = :crawled_by_id AND
		people.sent_to_crawler_date >= :last_cache_date
	GROUP BY
		people.sent_to_crawler_date
	ORDER BY
		people.sent_to_crawler_date");

while ($li_user = $query->fetch()) {

	$queryFetch->execute([
		'company_id' => $li_user['company_id'],
		'crawled_by_id' => $li_user["linked_in_id"],
		'last_cache_date' => date('Y-m-d', time() - 24 * 3600 * 3)
	]);

	while ($row = $queryFetch->fetch()) {
		$query2 = DB::prep("INSERT IGNORE INTO reports_cache (company_id, li_users_id, number_of_visits, number_of_invites, cache_date)
			VALUES(:company_id, :li_users_id, :number_of_visits, :number_of_invites, :cache_date)
			ON DUPLICATE KEY UPDATE number_of_visits = :number_of_visits, number_of_invites = :number_of_invites");
		$query2->execute([
			'company_id' => $row['company_id'],
			'li_users_id' => $row['crawled_by_id'],
			'number_of_visits' => $row['count'],
			'number_of_invites' => $row['invites_count'],
			'cache_date' => $row['date']
		]);
	}

	echo $li_user['name'] . "\n";

}

?>
