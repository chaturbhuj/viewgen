<?php
$IS_CRON_JOB = true;

chdir(__DIR__ . "/..");

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../classes/EmailTemplate.php');

//select online user with no active campaign
$users = DB::prep("
	SELECT
		li_users.id,
		li_users.company_id,
		li_users.linked_in_id,
		 max(campaign.id) AS active
	FROM
		li_users
		JOIN company ON li_users.company_id = company.id
		LEFT JOIN campaign ON campaign.visit_by_id = li_users.linked_in_id
		    AND campaign.company_id = li_users.company_id
    		AND campaign.publish_status = 'published'
	    	AND campaign.status = 'Active'
	    	AND (campaign.start_date = 0000-00-00 OR campaign.start_date <= date(NOW()) )
	WHERE
		-- li_users.software_status <> 'offline'
		company.frozen = 0
		AND company.deleted = 0
		AND company.do_not_autorerun = 0
		AND company.is_test = 0
		AND li_users.hidden = 0
	GROUP BY
		li_users.id
	HAVING 
		isnull(active)
	;
");
$users->execute();

$last_finish_date = DB::prep("
	SELECT 
		max(campaign_completed) AS last_finished 
	FROM 
		campaign 
	WHERE 
		publish_status = 'published'
		AND company_id = :company_id
		AND	status = 'Finished'
		AND visit_by_id = :crawled_by_id
	HAVING
	  last_finished <= DATE_SUB(NOW(), INTERVAL 6 HOUR)
	;
");

$campaigns_to_rerun = DB::prep("
	SELECT 
		* 
	FROM 
		campaign 
	WHERE 
		publish_status = 'published'
		AND company_id = :company_id
		AND	status = 'Finished'
		AND visit_by_id = :crawled_by_id
		AND (start_date > '2017-05-01 00:00:00' OR (start_date = 0 AND created > '2017-05-01 00:00:00'))
	ORDER BY
	    campaign_vbr DESC
	LIMIT 3
	;
");

$totalUsersProcessed = 0;
$totalUsersWithRerun = 0;
$totalCampaignRerun = 0;

while ($row = $users->fetch()) {
    $totalUsersProcessed++;
    consoleLog("--------------------------------------------");
    consoleLog("userId:".$row['id']);
    consoleLog("company_id:".$row['company_id']);
    consoleLog("linked_in_id:".$row['linked_in_id']);
    consoleLog(" ");

	$last_finish_date->execute([
		'company_id' => $row['company_id'],
		'crawled_by_id' => $row['linked_in_id']
	]);

	if ($last_finish_date->rowCount() === 0) {
		// Last campaign finish was less than 6 hours, so rerun not needed
        consoleLog("Last campaign finish was less than 6 hours, so rerun not needed");
        consoleLog("--------------------------------------------");
		continue;
	}

    $campaigns_to_rerun->execute([
        'company_id' => $row['company_id'],
        'crawled_by_id' => $row['linked_in_id']
    ]);

    if ($last_finish_date->rowCount() === 0) {
        // no finished campaign, nothing to rerun
        consoleLog("no finished campaign, nothing to rerun");
        consoleLog("--------------------------------------------");
        continue;
    }

    $totalUsersWithRerun++;

    $campaign_to_rerun = null;
	while($campaign_to_rerun = $campaigns_to_rerun->fetch()){
		$new_campaign_id = copy_campaign($campaign_to_rerun['id'], '');

		$query = DB::prep("UPDATE campaign SET publish_status = 'published', priority=3, secondary_priority=1100 WHERE id = :campaign_id");
		$query->execute([
			'campaign_id' => $new_campaign_id
		]);
        consoleLog("campaign to rerun id:".$campaign_to_rerun['id']);
        consoleLog("new campaign id:".$new_campaign_id);
        $totalCampaignRerun++;
	}
    consoleLog("--------------------------------------------");

}
consoleLog("----------------Total-----------------------");
consoleLog("Users processed:".$totalUsersProcessed);
consoleLog("Users with with rerunned campaigns:".$totalUsersWithRerun);
consoleLog("Total campaigns created:".$totalCampaignRerun);
consoleLog("--------------------------------------------");

function consoleLog($message, $success = true, $showStatus = false){
    $logMessage = "\e[1;33mINFO\e[0m ".$message." ";
    $cols = exec('tput cols') - strlen($message);
    echo $logMessage.($showStatus ? sprintf("%".$cols."s\r\n",    ($success ? "\e[32m[  OK  ]\e[0m" : "\e[31m[ FAIL ]\e[0m")) : "\r\n");
}


?>
