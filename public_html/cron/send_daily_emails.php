<?php
$IS_CRON_JOB = true;

chdir(__DIR__ . "/..");

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../classes/Email.php');
include_once(__DIR__.'/../classes/EmailTemplate.php');

$emailIds = [
	1 => '2bd5ce4a-9894-4070-839f-943352206d12',
	2 => 'ffb5394f-4bdb-42a0-983f-3108d2f95cc6',
	3 => 'a91a23a9-b3f1-4a2a-ab99-c8e2a009594b',
	//4 => '83c93569-363f-48a0-95e9-5165e4280294',
	//5 => '0a8ae9d4-be43-401c-a9e5-f7607b43b2d4'
];

foreach ($emailIds as $days => $templateId) {
	$query = DB::prep("
						SELECT
							*
						FROM
							company
							LEFT JOIN emailUnsubscribed ON company.id = emailUnsubscribed.company_id AND emailUnsubscribed.unsubscribeType = 'Introduction Emails' 
						WHERE
							DATE(company_created) = DATE_SUB(DATE(NOW()), INTERVAL $days DAY)
							AND last_page_view_viewgen > 0
							AND company.company_do_not_email = 0
							AND company.frozen = 0
							AND emailUnsubscribed.emailUnsubscribedId IS NULL
					   ");
	$query->execute();
	while ($company = $query->fetch()) {
		$emailTemplate = new EmailTemplate($company['id']);
		$email = new Email();
		$email->setHtmlBody(' ');
		$email->setTextBody(' ');
		$email->setSubject(' ');
		$email->setTemplateId($templateId);
		$email->addTo($company['email']);
		$email->addSubstitution('{{unsubscribe_link}}', [$emailTemplate->generateUnsubscrubeLink()]);
		$email->send();

		echo "Sent email to " . $company['email'] . "\n";
	}
}


?>
