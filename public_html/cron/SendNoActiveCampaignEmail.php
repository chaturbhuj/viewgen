<?php
$IS_CRON_JOB = true;

chdir(__DIR__ . "/..");

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../classes/EmailTemplate.php');

$query = DB::prep("SELECT *, company.email as company_email FROM li_users JOIN company ON (company.id = li_users.company_id) WHERE last_connection > DATE_SUB(NOW(), INTERVAL 7 DAY) AND company.frozen = 0 AND li_users.hidden = 0");
$query->execute();

$select = DB::prep("SELECT * FROM campaign WHERE publish_status = 'published' AND company_id = :company_id AND
	status = 'Active' AND (start_date = 0000-00-00 OR start_date <= date(NOW()) ) AND visit_by_id = :crawled_by_id
	LIMIT 1");

while ($row = $query->fetch()) {

	$select->execute([
		'company_id' => $row['company_id'],
		'crawled_by_id' => $row['linked_in_id']
	]);

	if ($select->rowCount() > 0) {
		// Has active campaign.
		continue;
	}

    $emailTemplate = new EmailTemplate($row['company_id']);
	$name_parts = explode(" ", trim($row['name']));
	//In database: ["linkedin first name","linkedin last name","company name"]
	$variables = [
		'linkedin first name' => $name_parts[0],
		'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
		'company name' => $row['company_name']
	];
    $emailTemplate->sendEmail($row['company_email'], "No Active Campaigns", $variables);
}
?>
