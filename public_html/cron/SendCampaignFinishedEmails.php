<?php
$IS_CRON_JOB = true;

chdir(__DIR__ . "/..");

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../classes/EmailTemplate.php');

$query = DB::prep("SELECT *, campaign.id as campaign_id FROM campaign JOIN company ON(company.id = campaign.company_id)
	WHERE campaign_completed > DATE_SUB(NOW(), INTERVAL 3 HOUR) AND has_sent_campaign_finished_email = 0");
$query->execute();

$update = DB::prep("UPDATE campaign SET has_sent_campaign_finished_email = 1 WHERE id = :campaign_id");

while ($row = $query->fetch()) {
	$emailTemplate = new EmailTemplate($row['company_id']);

	$vbr = $row['campaign_visit_count'] == 0 ? 0 : ($row['total_visitors'] / $row['campaign_visit_count']);
	$name_parts = explode(" ", trim($row['visit_by']));
	$variables = [
		'linkedin first name' => $name_parts[0],
		'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
		'company name' => $row['company_name'],
		'campaign name' => $row['name'],
		'num profiles visited' => $row['campaign_visit_count'],
		'num inbound views' => $row['total_visitors'],
		'view back rate' => round(100*$vbr, 1) . '%',
		'campaign link' => sprintf('<a href="%1$s">%1$s</a>', 'https://searchquant.net/visitedProfiles.php?campaign_id=' . $row['campaign_id'] . '&company_id=' . $row['company_id'])
	];

	$emailTemplate->sendEmail($row['email'], "Campaign Finished", $variables);

	$update->execute([
		'campaign_id' => $row['campaign_id']
	]);
}

?>
