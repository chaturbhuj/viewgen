<?
$IS_CRON_JOB = true;

chdir(__DIR__);

require_once(__DIR__ . '/../db.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');
$dates = array();

$min_date = "2016-01-01";
$query = DB::prep("SELECT max(`date`) last_date FROM `campaign_statistics`");
$query->execute();
if ($data = $query->fetch()) {
	//$min_date = $data["last_date"];
	$min_date = date('Y-m-d', strtotime($data["last_date"].' - 50 days'));;
}

$max_date = date('Y-m-d');
$active_date = $min_date;
while ($active_date <= $max_date) {
	$dates[] = $active_date;
	$active_date = date('Y-m-d', strtotime($active_date.' + 1 days'));
}
//$dates = array("2017-02-21");
$campaigns_to_update = array();
$companies_to_update = array();
foreach ($dates as $date) {
	echo "Starting with $date - ".date("H:i:s")."\n";
	$start_date = $date;
	$end_date = date('Y-m-d', strtotime($date.' + 14 days'));
	
	$query = DB::prep("
	SELECT * 
	FROM inbound_visit 
	WHERE 
		name != 'PRIVATE VIEWBACK' AND
		guessed_visit_date >= :start_date AND
		guessed_visit_date <= :end_date
	");
	
	$query->execute([
		"start_date" => $start_date,
		"end_date" => $end_date
		]);
	$visitors = array();
	while ($data = $query->fetch()) {
		$visitorKey = $data["crawled_by_id"]."|".$data["name"];
		$visitors[$visitorKey] = array(
			"inbound_visit_id" => $data["inbound_visit_id"],
			"guessed_visit_date" => $data["guessed_visit_date"]
		);
	}
	
	$start_date = date('Y-m-d', strtotime($date.' - 14 days'));
	$end_date = $date;
	$query = DB::prep("
	SELECT 
		crawled_by_id,
		CONCAT(name, title) as uniqueText 
	FROM people 
	WHERE 
		name != '' AND
		sent_to_crawler_date >= :start_date AND
		sent_to_crawler_date <= :end_date 
	");
	$query->execute([
		"start_date" => $start_date,
		"end_date" => $end_date
		]);
	$uniquePeople = array();
	while ($data = $query->fetch()) {
		if (!isset($uniquePeople[$data["crawled_by_id"]])) {
			$uniquePeople[$data["crawled_by_id"]] = array();
		}
		if (!isset($uniquePeople[$data["crawled_by_id"]][$data["uniqueText"]])) {
			$uniquePeople[$data["crawled_by_id"]][$data["uniqueText"]] = 0;
		}
		$uniquePeople[$data["crawled_by_id"]][$data["uniqueText"]] ++;
	}
	echo "uniquePeople:".count($uniquePeople)."\n";
	
	$query = DB::prep("
	SELECT * 
	FROM people 
	WHERE 
		name != '' AND
		sent_to_crawler_date = ?
	");

	$query->execute([$date]);
	$aggregates = array();
	while ($data = $query->fetch()) {
		
		$viewed_back = 0;
		$visitorKey = $data["crawled_by_id"]."|".$data["name"];
		if (isset($visitors[$visitorKey])) {
			$viewed_back = 1;
			$connectionInsertQuery = DB::prep("
			INSERT IGNORE INTO visit_connection
			(people_id, inbound_visit_id, guessed_visit_date)
			VALUES
			(?, ?, ?)
			");
			$connectionInsertQuery->execute([
				$data["id"], 
				$visitors[$visitorKey]["inbound_visit_id"],
				$visitors[$visitorKey]["guessed_visit_date"]
				
			]);
		}

		
		$visitedKey = $data["campaign_id"]."|".$data["crawled_by_id"].$data["sent_to_crawler_date"];
		if (!isset($aggregates[$visitedKey])) {
			$aggregates[$visitedKey] = array();
			$aggregates[$visitedKey]["company_id"] = $data["company_id"];
			$aggregates[$visitedKey]["campaign_id"] = $data["campaign_id"];
			$aggregates[$visitedKey]["li_user_id"] = $data["crawled_by_id"];
			$aggregates[$visitedKey]["date"] = $data["sent_to_crawler_date"];
			$aggregates[$visitedKey]["visited"] = 0;
			$aggregates[$visitedKey]["visitors"] = 0;
			$aggregates[$visitedKey]["visited_unique"] = 0;
			
		} 
		$aggregates[$visitedKey]["visited"] += 1;
		$aggregates[$visitedKey]["visitors"] += $viewed_back;
		if (isset($uniquePeople[$data["crawled_by_id"]][$data["name"].$data["title"]])) {
			if ($uniquePeople[$data["crawled_by_id"]][$data["name"].$data["title"]] == 1) {
				$aggregates[$visitedKey]["visited_unique"] += 1;
			}
			$uniquePeople[$data["crawled_by_id"]][$data["name"].$data["title"]]--;
		}
		
	}
	
	
	$query = DB::prep("
	INSERT INTO campaign_statistics
	(company_id, campaign_id, li_user_id, date, visited, visitors, visited_unique)
	VALUES
	(:company_id, :campaign_id, :li_user_id , :date, :visited, :visitors, :visited_unique)
	ON DUPLICATE KEY UPDATE
	visited = :visited,
	visitors = :visitors,
	visited_unique = :visited_unique
	");
	foreach ($aggregates as $aggregate) {
		if ($aggregate["li_user_id"] != 0) {
			$query->execute([
				"company_id" => $aggregate["company_id"],
				"campaign_id" => $aggregate["campaign_id"],
				"li_user_id" => $aggregate["li_user_id"],
				"date" => $aggregate["date"],
				"visited" => $aggregate["visited"],
				"visitors" => $aggregate["visitors"],
				"visited_unique" => $aggregate["visited_unique"]
			]);
			if ($aggregate["visitors"] > 0 or $aggregate["visited"] > 0) {
				$campaigns_to_update[$aggregate["campaign_id"]] = true;
				$companies_to_update[$aggregate["company_id"]] = true;
			}
		}
	}
	
}
echo "Starting with aggregating total_visitors on campaigns - ".date("H:i:s")."\n";

$campaigns_to_update_keys = array_keys($campaigns_to_update);
foreach ($campaigns_to_update_keys as $campaign_to_update) {
	$query = DB::prep("
	UPDATE campaign 
	SET total_visitors = (
		SELECT SUM(visitors) 
		FROM campaign_statistics 
		WHERE campaign_statistics.campaign_id = campaign.id
	)
	WHERE campaign.id = ?
	");
	$query->execute(array($campaign_to_update));
}
echo "Starting with aggregating people_count on company - ".date("H:i:s")."\n";
$companies_to_update_keys = array_keys($companies_to_update);
foreach ($companies_to_update_keys as $company_to_update) {
	$query = DB::prep("
	UPDATE `company` SET  
	people_count = (SELECT SUM(visited) 
					FROM  `campaign_statistics` 
					WHERE campaign_statistics.company_id = company.id
					)
	WHERE id = ?");
	$query->execute(array($company_to_update));
	//echo "UPDATE company $company_to_update ".date("H:i:s")."\n";
}

echo "Finished - ".date("H:i:s")."\n";
?>
