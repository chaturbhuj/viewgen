<?php

chdir(dirname(__FILE__) . "/..");

require_once('db.php');
require('classes/EmailTemplate.php');

$templateName = "Subscription: Basic only";
$targets = EmailTargets::getTargets($templateName);

foreach ($targets as $target) {
	$emailTemplate = new EmailTemplate($target->getCompanyId());
	$emailTemplate->sendEmail($target->getEmail(), $templateName, $target->getVariables());
}

?>
