<?
chdir(dirname(__FILE__));

require_once('../db.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');
$dates = array();


$min_date = "2015-01-01";
$query = DB::prep("SELECT max(`first_connection_created`) last_date FROM `first_connection`");
$query->execute();
if ($data = $query->fetch()) {
	//$min_date = $data["last_date"];
	if ($data["last_date"] != ""){
		$min_date = date('Y-m-d', strtotime($data["last_date"].' - 1 days'));
	}
}

$max_date = date('Y-m-d');
$active_date = $min_date;
while ($active_date <= $max_date) {
	$dates[] = $active_date;
	$active_date = date('Y-m-d', strtotime($active_date.' + 1 days'));
}
//$dates = array("2017-05-17");
$insert = DB::prep("INSERT IGNORE 
	INTO first_connection (user_li_id, first_connection_li_id, first_connection_created,company_id, connection_name) 
	VALUES (:user_li_id, :first_connection_li_id, :first_connection_created, :company_id, :connection_name)");
foreach ($dates as $date) {
	echo "\nStarting with $date - ".date("H:i:s")."\n";
	$query = DB::prep("
	SELECT 
		crawled_by_id,
		company_id,
		name,
		member_id
	FROM people 
	WHERE 
		name != '' AND
		sent_to_crawler_date = :date AND
		is_connected = 1
	");
	$query->execute([
		"date" => $date
		]);
	
	while ($data = $query->fetch()) {
		
		echo "|";
		if ($data["member_id"] != "" and $data["member_id"] != 0) {
			$insert->execute([
				"user_li_id" => $data["crawled_by_id"],
				"first_connection_li_id" => $data["member_id"],
				"first_connection_created" => $date,
				"company_id" => $data["company_id"],
				"connection_name" => $data["name"]
			]);
		}
	}
}





?>