<?
chdir(dirname(__FILE__));
$IS_CRON_JOB = true;

require_once(__DIR__.'/../../db.php');
function locationToCountryCode ($location) {
	if ($location == "") {
		return "";
	} else if (strpos($location, 'Afghanistan') !== false) {return 'af';}
	else if (strpos($location, 'Albania') !== false) {return 'al';}
	else if (strpos($location, 'Algeria') !== false) {return 'dz';}
	else if (strpos($location, 'American Samoa') !== false) {return 'as';}
	else if (strpos($location, 'Andorra') !== false) {return 'ad';}
	else if (strpos($location, 'Angola') !== false) {return 'ao';}
	else if (strpos($location, 'Anguilla') !== false) {return 'ai';}
	else if (strpos($location, 'Antarctica') !== false) {return 'aq';}
	else if (strpos($location, 'Antigua and Barbuda') !== false) {return 'ag';}
	else if (strpos($location, 'Argentina') !== false) {return 'ar';}
	else if (strpos($location, 'Armenia') !== false) {return 'am';}
	else if (strpos($location, 'Aruba') !== false) {return 'aw';}
	else if (strpos($location, 'Australia') !== false) {return 'au';}
	else if (strpos($location, 'Austria') !== false) {return 'at';}
	else if (strpos($location, 'Azerbaijan') !== false) {return 'az';}
	else if (strpos($location, 'Bahamas') !== false) {return 'bs';}
	else if (strpos($location, 'Bahrain') !== false) {return 'bh';}
	else if (strpos($location, 'Bangladesh') !== false) {return 'bd';}
	else if (strpos($location, 'Barbados') !== false) {return 'bb';}
	else if (strpos($location, 'Belarus') !== false) {return 'by';}
	else if (strpos($location, 'Belgium') !== false) {return 'be';}
	else if (strpos($location, 'Belize') !== false) {return 'bz';}
	else if (strpos($location, 'Benin') !== false) {return 'bj';}
	else if (strpos($location, 'Bermuda') !== false) {return 'bm';}
	else if (strpos($location, 'Bhutan') !== false) {return 'bt';}
	else if (strpos($location, 'Bolivia') !== false) {return 'bo';}
	else if (strpos($location, 'Bosnia and Herzegovina') !== false) {return 'ba';}
	else if (strpos($location, 'Botswana') !== false) {return 'bw';}
	else if (strpos($location, 'Brazil') !== false) {return 'br';}
	else if (strpos($location, 'British Indian Ocean Territory') !== false) {return 'io';}
	else if (strpos($location, 'British Virgin Islands') !== false) {return 'vg';}
	else if (strpos($location, 'Brunei') !== false) {return 'bn';}
	else if (strpos($location, 'Bulgaria') !== false) {return 'bg';}
	else if (strpos($location, 'Burkina Faso') !== false) {return 'bf';}
	else if (strpos($location, 'Burundi') !== false) {return 'bi';}
	else if (strpos($location, 'Cambodia') !== false) {return 'kh';}
	else if (strpos($location, 'Cameroon') !== false) {return 'cm';}
	else if (strpos($location, 'Canada') !== false) {return 'ca';}
	else if (strpos($location, 'Cape Verde') !== false) {return 'cv';}
	else if (strpos($location, 'Cayman Islands') !== false) {return 'ky';}
	else if (strpos($location, 'Central African Republic') !== false) {return 'cf';}
	else if (strpos($location, 'Chad') !== false) {return 'td';}
	else if (strpos($location, 'Chile') !== false) {return 'cl';}
	else if (strpos($location, 'China') !== false) {return 'cn';}
	else if (strpos($location, 'Christmas Island') !== false) {return 'cx';}
	else if (strpos($location, 'Cocos Islands') !== false) {return 'cc';}
	else if (strpos($location, 'Colombia') !== false) {return 'co';}
	else if (strpos($location, 'Comoros') !== false) {return 'km';}
	else if (strpos($location, 'Cook Islands') !== false) {return 'ck';}
	else if (strpos($location, 'Costa Rica') !== false) {return 'cr';}
	else if (strpos($location, 'Croatia') !== false) {return 'hr';}
	else if (strpos($location, 'Cuba') !== false) {return 'cu';}
	else if (strpos($location, 'Curacao') !== false) {return 'cw';}
	else if (strpos($location, 'Cyprus') !== false) {return 'cy';}
	else if (strpos($location, 'Czech Republic') !== false) {return 'cz';}
	else if (strpos($location, 'Democratic Republic of the Congo') !== false) {return 'cd';}
	else if (strpos($location, 'Denmark') !== false) {return 'dk';}
	else if (strpos($location, 'Djibouti') !== false) {return 'dj';}
	else if (strpos($location, 'Dominica') !== false) {return 'dm';}
	else if (strpos($location, 'Dominican Republic') !== false) {return 'do';}
	else if (strpos($location, 'East Timor') !== false) {return 'tl';}
	else if (strpos($location, 'Ecuador') !== false) {return 'ec';}
	else if (strpos($location, 'Egypt') !== false) {return 'eg';}
	else if (strpos($location, 'El Salvador') !== false) {return 'sv';}
	else if (strpos($location, 'Equatorial Guinea') !== false) {return 'gq';}
	else if (strpos($location, 'Eritrea') !== false) {return 'er';}
	else if (strpos($location, 'Estonia') !== false) {return 'ee';}
	else if (strpos($location, 'Ethiopia') !== false) {return 'et';}
	else if (strpos($location, 'Falkland Islands') !== false) {return 'fk';}
	else if (strpos($location, 'Faroe Islands') !== false) {return 'fo';}
	else if (strpos($location, 'Fiji') !== false) {return 'fj';}
	else if (strpos($location, 'Finland') !== false) {return 'fi';}
	else if (strpos($location, 'France') !== false) {return 'fr';}
	else if (strpos($location, 'French Polynesia') !== false) {return 'pf';}
	else if (strpos($location, 'Gabon') !== false) {return 'ga';}
	else if (strpos($location, 'Gambia') !== false) {return 'gm';}
	else if (strpos($location, 'Georgia') !== false) {return 'ge';}
	else if (strpos($location, 'Germany') !== false) {return 'de';}
	else if (strpos($location, 'Ghana') !== false) {return 'gh';}
	else if (strpos($location, 'Gibraltar') !== false) {return 'gi';}
	else if (strpos($location, 'Greece') !== false) {return 'gr';}
	else if (strpos($location, 'Greenland') !== false) {return 'gl';}
	else if (strpos($location, 'Grenada') !== false) {return 'gd';}
	else if (strpos($location, 'Guam') !== false) {return 'gu';}
	else if (strpos($location, 'Guatemala') !== false) {return 'gt';}
	else if (strpos($location, 'Guernsey') !== false) {return 'gg';}
	else if (strpos($location, 'Guinea') !== false) {return 'gn';}
	else if (strpos($location, 'Guinea-Bissau') !== false) {return 'gw';}
	else if (strpos($location, 'Guyana') !== false) {return 'gy';}
	else if (strpos($location, 'Haiti') !== false) {return 'ht';}
	else if (strpos($location, 'Honduras') !== false) {return 'hn';}
	else if (strpos($location, 'Hong Kong') !== false) {return 'hk';}
	else if (strpos($location, 'Hungary') !== false) {return 'hu';}
	else if (strpos($location, 'Iceland') !== false) {return 'is';}
	else if (strpos($location, 'India') !== false) {return 'in';}
	else if (strpos($location, 'Indonesia') !== false) {return 'id';}
	else if (strpos($location, 'Iran') !== false) {return 'ir';}
	else if (strpos($location, 'Iraq') !== false) {return 'iq';}
	else if (strpos($location, 'Ireland') !== false) {return 'ie';}
	else if (strpos($location, 'Isle of Man') !== false) {return 'im';}
	else if (strpos($location, 'Israel') !== false) {return 'il';}
	else if (strpos($location, 'Italy') !== false) {return 'it';}
	else if (strpos($location, 'Ivory Coast') !== false) {return 'ci';}
	else if (strpos($location, 'Jamaica') !== false) {return 'jm';}
	else if (strpos($location, 'Japan') !== false) {return 'jp';}
	else if (strpos($location, 'Jersey') !== false) {return 'je';}
	else if (strpos($location, 'Jordan') !== false) {return 'jo';}
	else if (strpos($location, 'Kazakhstan') !== false) {return 'kz';}
	else if (strpos($location, 'Kenya') !== false) {return 'ke';}
	else if (strpos($location, 'Kiribati') !== false) {return 'ki';}
	else if (strpos($location, 'Kosovo') !== false) {return 'xk';}
	else if (strpos($location, 'Kuwait') !== false) {return 'kw';}
	else if (strpos($location, 'Kyrgyzstan') !== false) {return 'kg';}
	else if (strpos($location, 'Laos') !== false) {return 'la';}
	else if (strpos($location, 'Latvia') !== false) {return 'lv';}
	else if (strpos($location, 'Lebanon') !== false) {return 'lb';}
	else if (strpos($location, 'Lesotho') !== false) {return 'ls';}
	else if (strpos($location, 'Liberia') !== false) {return 'lr';}
	else if (strpos($location, 'Libya') !== false) {return 'ly';}
	else if (strpos($location, 'Liechtenstein') !== false) {return 'li';}
	else if (strpos($location, 'Lithuania') !== false) {return 'lt';}
	else if (strpos($location, 'Luxembourg') !== false) {return 'lu';}
	else if (strpos($location, 'Macau') !== false) {return 'mo';}
	else if (strpos($location, 'Macedonia') !== false) {return 'mk';}
	else if (strpos($location, 'Madagascar') !== false) {return 'mg';}
	else if (strpos($location, 'Malawi') !== false) {return 'mw';}
	else if (strpos($location, 'Malaysia') !== false) {return 'my';}
	else if (strpos($location, 'Maldives') !== false) {return 'mv';}
	else if (strpos($location, 'Mali') !== false) {return 'ml';}
	else if (strpos($location, 'Malta') !== false) {return 'mt';}
	else if (strpos($location, 'Marshall Islands') !== false) {return 'mh';}
	else if (strpos($location, 'Mauritania') !== false) {return 'mr';}
	else if (strpos($location, 'Mauritius') !== false) {return 'mu';}
	else if (strpos($location, 'Mayotte') !== false) {return 'yt';}
	else if (strpos($location, 'Mexico') !== false) {return 'mx';}
	else if (strpos($location, 'Micronesia') !== false) {return 'fm';}
	else if (strpos($location, 'Moldova') !== false) {return 'md';}
	else if (strpos($location, 'Monaco') !== false) {return 'mc';}
	else if (strpos($location, 'Mongolia') !== false) {return 'mn';}
	else if (strpos($location, 'Montenegro') !== false) {return 'me';}
	else if (strpos($location, 'Montserrat') !== false) {return 'ms';}
	else if (strpos($location, 'Morocco') !== false) {return 'ma';}
	else if (strpos($location, 'Mozambique') !== false) {return 'mz';}
	else if (strpos($location, 'Myanmar') !== false) {return 'mm';}
	else if (strpos($location, 'Namibia') !== false) {return 'na';}
	else if (strpos($location, 'Nauru') !== false) {return 'nr';}
	else if (strpos($location, 'Nepal') !== false) {return 'np';}
	else if (strpos($location, 'Netherlands') !== false) {return 'nl';}
	else if (strpos($location, 'Netherlands Antilles') !== false) {return 'an';}
	else if (strpos($location, 'New Caledonia') !== false) {return 'nc';}
	else if (strpos($location, 'New Zealand') !== false) {return 'nz';}
	else if (strpos($location, 'Nicaragua') !== false) {return 'ni';}
	else if (strpos($location, 'Niger') !== false) {return 'ne';}
	else if (strpos($location, 'Nigeria') !== false) {return 'ng';}
	else if (strpos($location, 'Niue') !== false) {return 'nu';}
	else if (strpos($location, 'North Korea') !== false) {return 'kp';}
	else if (strpos($location, 'Northern Mariana Islands') !== false) {return 'mp';}
	else if (strpos($location, 'Norway') !== false) {return 'no';}
	else if (strpos($location, 'Oman') !== false) {return 'om';}
	else if (strpos($location, 'Pakistan') !== false) {return 'pk';}
	else if (strpos($location, 'Palau') !== false) {return 'pw';}
	else if (strpos($location, 'Palestine') !== false) {return 'ps';}
	else if (strpos($location, 'Panama') !== false) {return 'pa';}
	else if (strpos($location, 'Papua New Guinea') !== false) {return 'pg';}
	else if (strpos($location, 'Paraguay') !== false) {return 'py';}
	else if (strpos($location, 'Peru') !== false) {return 'pe';}
	else if (strpos($location, 'Philippines') !== false) {return 'ph';}
	else if (strpos($location, 'Pitcairn') !== false) {return 'pn';}
	else if (strpos($location, 'Poland') !== false) {return 'pl';}
	else if (strpos($location, 'Portugal') !== false) {return 'pt';}
	else if (strpos($location, 'Puerto Rico') !== false) {return 'pr';}
	else if (strpos($location, 'Qatar') !== false) {return 'qa';}
	else if (strpos($location, 'Republic of the Congo') !== false) {return 'cg';}
	else if (strpos($location, 'Reunion') !== false) {return 're';}
	else if (strpos($location, 'Romania') !== false) {return 'ro';}
	else if (strpos($location, 'Russia') !== false) {return 'ru';}
	else if (strpos($location, 'Rwanda') !== false) {return 'rw';}
	else if (strpos($location, 'Saint Barthelemy') !== false) {return 'bl';}
	else if (strpos($location, 'Saint Helena') !== false) {return 'sh';}
	else if (strpos($location, 'Saint Kitts and Nevis') !== false) {return 'kn';}
	else if (strpos($location, 'Saint Lucia') !== false) {return 'lc';}
	else if (strpos($location, 'Saint Martin') !== false) {return 'mf';}
	else if (strpos($location, 'Saint Pierre and Miquelon') !== false) {return 'pm';}
	else if (strpos($location, 'Saint Vincent and the Grenadines') !== false) {return 'vc';}
	else if (strpos($location, 'Samoa') !== false) {return 'ws';}
	else if (strpos($location, 'San Marino') !== false) {return 'sm';}
	else if (strpos($location, 'Sao Tome and Principe') !== false) {return 'st';}
	else if (strpos($location, 'Saudi Arabia') !== false) {return 'sa';}
	else if (strpos($location, 'Senegal') !== false) {return 'sn';}
	else if (strpos($location, 'Serbia') !== false) {return 'rs';}
	else if (strpos($location, 'Seychelles') !== false) {return 'sc';}
	else if (strpos($location, 'Sierra Leone') !== false) {return 'sl';}
	else if (strpos($location, 'Singapore') !== false) {return 'sg';}
	else if (strpos($location, 'Sint Maarten') !== false) {return 'sx';}
	else if (strpos($location, 'Slovakia') !== false) {return 'sk';}
	else if (strpos($location, 'Slovenia') !== false) {return 'si';}
	else if (strpos($location, 'Solomon Islands') !== false) {return 'sb';}
	else if (strpos($location, 'Somalia') !== false) {return 'so';}
	else if (strpos($location, 'South Africa') !== false) {return 'za';}
	else if (strpos($location, 'South Korea') !== false) {return 'kr';}
	else if (strpos($location, 'South Sudan') !== false) {return 'ss';}
	else if (strpos($location, 'Spain') !== false) {return 'es';}
	else if (strpos($location, 'Sri Lanka') !== false) {return 'lk';}
	else if (strpos($location, 'Sudan') !== false) {return 'sd';}
	else if (strpos($location, 'Suriname') !== false) {return 'sr';}
	else if (strpos($location, 'Svalbard and Jan Mayen') !== false) {return 'sj';}
	else if (strpos($location, 'Swaziland') !== false) {return 'sz';}
	else if (strpos($location, 'Sweden') !== false) {return 'se';}
	else if (strpos($location, 'Switzerland') !== false) {return 'ch';}
	else if (strpos($location, 'Syria') !== false) {return 'sy';}
	else if (strpos($location, 'Taiwan') !== false) {return 'tw';}
	else if (strpos($location, 'Tajikistan') !== false) {return 'tj';}
	else if (strpos($location, 'Tanzania') !== false) {return 'tz';}
	else if (strpos($location, 'Thailand') !== false) {return 'th';}
	else if (strpos($location, 'Togo') !== false) {return 'tg';}
	else if (strpos($location, 'Tokelau') !== false) {return 'tk';}
	else if (strpos($location, 'Tonga') !== false) {return 'to';}
	else if (strpos($location, 'Trinidad and Tobago') !== false) {return 'tt';}
	else if (strpos($location, 'Tunisia') !== false) {return 'tn';}
	else if (strpos($location, 'Turkey') !== false) {return 'tr';}
	else if (strpos($location, 'Turkmenistan') !== false) {return 'tm';}
	else if (strpos($location, 'Turks and Caicos Islands') !== false) {return 'tc';}
	else if (strpos($location, 'Tuvalu') !== false) {return 'tv';}
	else if (strpos($location, 'U.S. Virgin Islands') !== false) {return 'vi';}
	else if (strpos($location, 'Uganda') !== false) {return 'ug';}
	else if (strpos($location, 'Ukraine') !== false) {return 'ua';}
	else if (strpos($location, 'United Arab Emirates') !== false) {return 'ae';}
	else if (strpos($location, 'United Kingdom') !== false) {return 'gb';}
	else if (strpos($location, 'United States') !== false) {return 'us';}
	else if (strpos($location, 'Uruguay') !== false) {return 'uy';}
	else if (strpos($location, 'Uzbekistan') !== false) {return 'uz';}
	else if (strpos($location, 'Vanuatu') !== false) {return 'vu';}
	else if (strpos($location, 'Vatican') !== false) {return 'va';}
	else if (strpos($location, 'Venezuela') !== false) {return 've';}
	else if (strpos($location, 'Vietnam') !== false) {return 'vn';}
	else if (strpos($location, 'Wallis and Futuna') !== false) {return 'wf';}
	else if (strpos($location, 'Western Sahara') !== false) {return 'eh';}
	else if (strpos($location, 'Yemen') !== false) {return 'ye';}
	else if (strpos($location, 'Zambia') !== false) {return 'zm';}
	else if (strpos($location, 'Zimbabwe') !== false) {return 'zw';}
	else {
		return "us";
	}
}
error_reporting(E_ALL);
ini_set('display_errors', '1');
$dates = array();

$min_date = "2016-05-04";
$query = DB::prep("SELECT max(`firstEncounter`) last_date FROM `prospects`");
$query->execute();
if ($data = $query->fetch()) {
	
	if ($data["last_date"] != "") {
		$min_date = $data["last_date"];
	}
}

$max_date = date('Y-m-d');
$active_date = $min_date;
while ($active_date <= $max_date) {
	$dates[] = $active_date;
	$active_date = date('Y-m-d', strtotime($active_date.' + 1 days'));
}
//$dates = array("2017-02-21");
$campaigns_to_update = array();
$companies_to_update = array();
foreach ($dates as $date) {
	echo "Starting with $date - ".date("H:i:s")."\n";

	
	$query = DB::prep("
	SELECT * 
	FROM people 
	WHERE 
		email != '' AND
		sent_to_crawler_date = :date
	");
	
	$query->execute([
		"date" => $date
		]);
	
	$insert = DB::prep("
	INSERT IGNORE INTO prospects
		(firstEncounter, email, tagline, firstName, lastName, industry, location, premiumServiceLevel, company, countryCode) VALUES
		(:firstEncounter, :email, :tagline, :firstName, :lastName, :industry, :location, :premiumServiceLevel, :company, :countryCode)
	");
	
	while ($data = $query->fetch()) {
		$names = explode(" ", $data["name"]);
		$firstName = $names[0];
		$lastName = trim(str_replace($names[0], "", $data["name"]));
		echo $firstName."\n".$lastName;
		$insert->execute([
		"firstEncounter" => $date,
		"email" => $data["email"],
		"tagline" => $data["title"],
		"firstName" => $firstName,
		"lastName" => $lastName,
		"industry" => $data["industry"],
		"location" => $data["location"],
		"premiumServiceLevel" => $data["premium"],
		"company" => $data["employer"],
		"countryCode" => locationToCountryCode($data["location"])
		
		]);
		//print_r($data);
		//die;
	}
}

?>