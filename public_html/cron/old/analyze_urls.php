<?php

include('../db.php');

$query = DB::prep("SELECT campaign.id, campaign.company_id, url FROM campaign JOIN search_url ON (search_url.campaign_id = campaign.id) WHERE campaign.status = 'Active' AND campaign.publish_status = 'Published'");
$query->execute();

$campaigns_with_old_urls = [];
while ($row = $query->fetch()) {
	if (strpos($row['url'], 'https://www.linkedin.com/vsearch/') === 0) {
		$campaigns_with_old_urls[$row['id']] = $row['company_id'];
	}
}

foreach ($campaigns_with_old_urls as $campaign_id => $company_id) {
	$query = DB::prep("update campaign set publish_status='archived', status='Paused' where id = :campaign_id and
		company_id = :company_id");
	$query->execute([
		'campaign_id' => $campaign_id,
		'company_id' => $company_id
	]);
}

?>
