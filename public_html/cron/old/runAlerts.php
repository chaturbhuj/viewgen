<?php

chdir(dirname(__FILE__) . '/..');

include_once('db.php');
include_once('shared_functions.php');
include_once('classes/AlertSystem.php');

$alertSystem = new AlertSystem();
$alertSystem->run();

echo "\n\n";

?>
