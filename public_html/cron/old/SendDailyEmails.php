<?php

chdir(dirname(__FILE__) . "/..");

require_once('db.php');
require('classes/EmailTemplate.php');

$templates = [
	'Email after 1 day',
	'Email after 2 days',
	'Email after 3 days',
	'Email after 4 days',
	'Email after 5 days',
	'Email after 6 days',
	'Email after 7 days',
	'Email after 14 days'
];

foreach ($templates as $templateName) {

	$targets = EmailTargets::getTargets($templateName);

	foreach ($targets as $target) {
		$emailTemplate = new EmailTemplate($target->getCompanyId());
		$emailTemplate->sendEmail($target->getEmail(), $templateName, $target->getVariables());
	}
}

?>
