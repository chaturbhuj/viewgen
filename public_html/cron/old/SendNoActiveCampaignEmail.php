<?php

chdir(dirname(__FILE__) . "/..");

require_once('db.php');
require('classes/EmailTemplate.php');

$query = DB::prep("SELECT * FROM li_users WHERE last_connection > DATE_SUB(NOW(), INTERVAL 7 DAY)");
$query->execute();

$select = DB::prep("SELECT * FROM campaign WHERE publish_status = 'published' AND company_id = :company_id AND
	status = 'Active' AND (start_date = 0000-00-00 OR start_date <= date(NOW()) ) AND visit_by_id = :crawled_by_id
	LIMIT 1");

$count = 0;
$total = 0;
while ($row = $query->fetch()) {
	$total++;
	$select->execute([
		'company_id' => $row['company_id'],
		'crawled_by_id' => $row['linked_in_id']
	]);

	if ($select->rowCount() > 0) {
		// Has active campaign.
		continue;
	}

	$count++;
	echo $row['name']."\n";
	/*$emailTemplate = new EmailTemplate($row['id']);
	$variables = [
		'first name' => $row['first_name'],
		'last name' => $row['last_name'],
		'company name' => $row['company_name']
	];*/
}

var_dump($count);
var_dump($total);

?>
