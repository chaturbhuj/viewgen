<?php

chdir(dirname(__FILE__) . "/..");

require_once('db.php');
require('classes/EmailTemplate.php');

$targets = EmailTargets::getTargets("Anonymous User");

foreach ($targets as $target) {
	$emailTemplate = new EmailTemplate($target->getCompanyId());
	$emailTemplate->sendEmail($target->getEmail(), "Anonymous User", $target->getVariables());
}

?>
