<?php

chdir(dirname(__FILE__) . "/..");

include('locationmap.php');

require_once('db.php');

	$map = [
		'us:42' => [8829]
	];

function linkedin_location_to_zip_codes($locations) {
	global $lMap;
	global $map;

	$locations = explode(',', $locations);
	$return = [];

	foreach ($locations as $location) {
		if (strpos($location, "us") !== 0) continue;
		if (!isset($map[$location])) {

			$lname = "";
			$lparts = explode(":", $location);
			if (is_numeric($lparts[count($lparts) - 1]) && $lparts[count($lparts) - 1] > 0) {
				foreach ($lMap as $code => $name) {
					if (strpos($code, $lparts[count($lparts) - 1]) !== false) {
						$lname = $name;
					}
				}
			}

			echo "https://www.linkedin.com/vsearch/p?keywords=board%20director&openAdvancedForm=true&locationType=Y&f_CS=G,H,I&rsid=493721231473369390141&orig=FCTD&f_N=S&openFacets=N,G,CC,CS&f_G=" . urlencode($location) . ";$location;$lname\n";
			$map[$location] = [];
			continue;
		}
	
		$return[] = $map[$location];
	}
	return $return;
}

function get_prefered_locations($li_user_id) {
	$query = DB::prep("SELECT * FROM search_url WHERE visit_by_id = :li_user_id");
	$query->execute([
		'li_user_id' => $li_user_id
	]);

	$locations = [];
	while ($row = $query->fetch()) {
		if (!preg_match('#https://www.linkedin.com/vsearch/p\?(.+)#is', $row['url'], $match)) continue;
		parse_str($match[1], $parameters);

		if (!isset($parameters['f_G'])) continue;
		if (!isset($locations[$parameters['f_G']])) $locations[$parameters['f_G']] = 0;
		$locations[$parameters['f_G']]++;
	}
	return $locations;
}

$query = DB::prep("SELECT * FROM search_url JOIN campaign ON (campaign.id = campaign_id) WHERE total_matches > 1000 AND finished = 0 AND campaign.status = 'Active' AND campaign_last_visit > DATE_SUB(NOW(), interval 1 DAY) AND url NOT LIKE '%postalCode%'");
$query->execute();

while ($row = $query->fetch()) {

	if (!preg_match('#https://www.linkedin.com/vsearch/p\?(.+)#is', $row['url'], $match)) continue;
	parse_str($match[1], $parameters);

	if (isset($parameters['f_G'])) {
		
	}
	
	if (isset($parameters['f_G'])) {
		// This one has a location.
		$zipCodes = linkedin_location_to_zip_codes($parameters['f_G']);
	} else {
		$preferedLocations = get_prefered_locations($row['visit_by_id']);
		if ($preferedLocations === null) return;
		$zipCodes = [];
		foreach ($preferedLocations as $location => $weight) {
			$zipCodes = array_merge($zipCodes, linkedin_location_to_zip_codes($location));
		}
	}

}

?>
