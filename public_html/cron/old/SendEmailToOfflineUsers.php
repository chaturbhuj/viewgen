<?php

chdir(dirname(__FILE__) . "/..");

require_once('db.php');
require('classes/EmailTemplate.php');

$query = DB::prep("
	SELECT * FROM (
		(SELECT li_users.*, company.email, company.company_name, 'Desktop Client Offline (4 days)' AS emailTemplate FROM `li_users` JOIN company ON(company.id = li_users.company_id)
		WHERE DATE(`last_connection`) = DATE(DATE_SUB(NOW(), INTERVAL 4 DAY)))
		UNION ALL
		(SELECT li_users.*, company.email, company.company_name, 'Desktop Client Offline (5 days)' AS emailTemplate FROM `li_users` JOIN company ON(company.id = li_users.company_id)
		WHERE DATE(`last_connection`) = DATE(DATE_SUB(NOW(), INTERVAL 5 DAY)))
		UNION ALL
		(SELECT li_users.*, company.email, company.company_name, 'Desktop Client Offline (7 days)' AS emailTemplate FROM `li_users` JOIN company ON(company.id = li_users.company_id)
		WHERE DATE(`last_connection`) = DATE(DATE_SUB(NOW(), INTERVAL 7 DAY)))
	) AS tmp

	GROUP BY company_id");
$query->execute();

while ($row = $query->fetch()) {
	$emailTemplate = new EmailTemplate($row['company_id']);
	$name_parts = explode(" ", trim($row['name']));
	$variables = [
		'linkedin first name' => $name_parts[0],
		'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
		'company name' => $row['company_name'],
		'company_id' => $row['company_id']
	];

	$emailTemplate->sendEmail($row['email'], $row['emailTemplate'], $variables);
}

?>
