<?php

chdir(dirname(__FILE__));

require_once('../db.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

function get_vbr_for_campaign($campaign_id) {

	$query = DB::prep("SELECT * FROM search_url WHERE campaign_id = :campaign_id");
	$query->execute([
		'campaign_id' => $campaign_id
	]);

	$total_visits = 0;
	$returned = 0;
	while ($row = $query->fetch()) {
		$total_visits += $row['search_url_visits'];
		$returned += $row['search_url_viewbacks'];
	}

	if ($total_visits == 0) return null;

	return $returned / $total_visits;
}

function get_vbr_for_search_url($search_url_id) {

	$query = DB::prep("SELECT * FROM search_url WHERE id = :search_url_id");
	$query->execute([
		'search_url_id' => $search_url_id
	]);

	$row = $query->fetch();

	$vbr_period = 7;

	$query = DB::prep("SELECT
			COUNT(*) num_visits,
			SUM(
				IF(guessed_visit_date IS NULL, 0,
					IF(guessed_visit_date >= sent_to_crawler_date AND
						guessed_visit_date <= DATE_ADD(sent_to_crawler_date, INTERVAL $vbr_period DAY), 1, 0)
				)
			) num_return_visits
		FROM
		(
			SELECT
				(
					SELECT
						inbound_visit.guessed_visit_date
					FROM inbound_visit
					WHERE
						inbound_visit.crawled_by_id = :user_id AND
						inbound_visit.name = people.name AND
						SUBSTR(inbound_visit.headline, 1, 40) SOUNDS LIKE SUBSTR(people.title, 1, 40)
					ORDER BY
						guessed_visit_date DESC
					LIMIT 1
				) AS guessed_visit_date,
				people.*
			FROM people
			JOIN people_list ON (
				people_list.id = people.people_list_id
			)
			JOIN campaign ON (
				campaign.id = people.campaign_id
			)
			WHERE
				people.name != '' AND
				people.search_url_id = :search_url_id
		) tmp2");

	$query->execute(array(
		"search_url_id" => $search_url_id,
		"user_id" => $row['visit_by_id']
	));

	$row = $query->fetch();

	// Determine if we have had time to look at all the viewbacks.
	$query = DB::prep("SELECT COUNT(*) numPeopleLeft FROM people WHERE search_url_id = :search_url_id AND
		sent_to_crawler >= DATE_SUB(NOW(), INTERVAL $vbr_period DAY)");
	$query->execute([
		'search_url_id' => $search_url_id
	]);

	$row2 = $query->fetch();
	$tracked_all_viewbacks = $row2['numPeopleLeft'] == 0;

	return [
		$row['num_visits'] == 0 ? null : ($row['num_return_visits'] / $row['num_visits']),
		$row['num_visits'],
		$row['num_return_visits'],
		$tracked_all_viewbacks
	];
}

$query = DB::prep("SELECT * FROM search_url WHERE finished = 1 AND created > '2015-11-01' AND search_url_vbr_date IS NULL AND total_matches > 0");
$query->execute();
while ($search_url = $query->fetch()) {
	list($search_url_vbr, $search_url_visits, $search_url_viewbacks, $tracked_all_viewbacks) =
		get_vbr_for_search_url($search_url['id']);

	echo $search_url['id'] . " $search_url_vbr\n";

	$update = DB::prep("UPDATE search_url SET
			search_url_vbr = :search_url_vbr,
			search_url_vbr_date = :search_url_vbr_date,
			search_url_visits = :search_url_visits,
			search_url_viewbacks = :search_url_viewbacks
		WHERE id = :search_url_id");

	$update->execute([
		'search_url_vbr' => $search_url_vbr,
		'search_url_vbr_date' => $tracked_all_viewbacks ? date('Y-m-d H:i:s', time()) : null,
		'search_url_visits' => $search_url_visits,
		'search_url_viewbacks' => $search_url_viewbacks,
		'search_url_id' => $search_url['id']
	]);
}

$query = DB::prep("SELECT * FROM campaign WHERE DATE(campaign_completed) <= DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND campaign_vbr_date IS NULL AND DATE(campaign_completed) >= '2015-11-01' ORDER BY campaign_completed DESC");
$query->execute();
while ($campaign = $query->fetch()) {
	$campaign_vbr = get_vbr_for_campaign($campaign['id']);
	echo $campaign['id']."\n";

	$update = DB::prep("UPDATE campaign SET campaign_vbr = :campaign_vbr, campaign_vbr_date = NOW() WHERE id = :campaign_id");
	$update->execute([
		'campaign_vbr' => $campaign_vbr,
		'campaign_id' => $campaign['id']
	]);
}

?>
