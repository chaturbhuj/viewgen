<?php
$IS_CRON_JOB = true;

require_once(__DIR__.'/../db.php');

//require_once(__DIR__ . '/../intercom/helper.php');
//use IntercomHelper;

error_reporting(E_ALL);
ini_set('display_errors', '1');
//$dates = array();

$query = DB::prep("SELECT DISTINCT linked_in_id FROM li_users");
$query->execute();
$userToCount = [];
while ($row = $query->fetch()) {
	$userToCount[$row['linked_in_id']] = [
        'visits' => 0,
        'invites' => 0
    ];
}

$query = DB::prep("SELECT people.crawled_by_id, people.sent_to_crawler, people_flags.value AS invite_sent FROM people LEFT JOIN people_flags ON people.id = people_flags.people_id AND people_flags.flag_id = 1 ORDER BY sent_to_crawler DESC LIMIT 200000");
$query->execute();
$oneDayAgo = date('Y-m-d H:i:s', strtotime('-1 days', time()));

while ($data = $query->fetch()) {
    if ($oneDayAgo > $data["sent_to_crawler"]) {
        //echo $data["sent_to_crawler"];
        break;
    }
    //$min_date = $data["last_date"];
    //print_r($data);
    if(!isset($userToCount[$data["crawled_by_id"]])){
        $userToCount[$data["crawled_by_id"]] = [
            'visits' => 0,
            'invites' => 0
        ];
    }

    $userToCount[$data["crawled_by_id"]]['visits'] ++;
    if($data["invite_sent"]){
        $userToCount[$data["crawled_by_id"]]['invites'] ++;
    }

}

//DB::get_instance()->beginTransaction();
$query = DB::prep("UPDATE li_users SET visitsLast24Hours = :visits, invitesLast24Hours = :invites WHERE linked_in_id = :linked_in_id");

foreach ($userToCount as $userId => $count) {
    $query->execute([
        'visits' => $count['visits'],
        'invites' => $count['invites'],
        'linked_in_id' => $userId
    ]);
    //echo $query."\n";
}
//DB::get_instance()->commit();

//global $CONFIG;
//$intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);


$query_off = DB::prep("update li_users set software_status='offline' where UNIX_TIMESTAMP(last_connection)<UNIX_TIMESTAMP(utc_timestamp())-300");
$query = DB::prep("select * from li_users where software_status <> 'offline' and UNIX_TIMESTAMP(last_connection)<UNIX_TIMESTAMP(utc_timestamp())-300");
$query->execute();
$query_off->execute();
while ($data = $query->fetch()) {
    $data['software_status'] = 'offline';
    //$intercomHelper->pushUserMetrics($data); //switch off due to intercom limit exceeded

}

//print_r($userToCount);
?>