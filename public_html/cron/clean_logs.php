<?php
$IS_CRON_JOB = true;

require_once(__DIR__.'/../db.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');
//$dates = array();

$query = DB::prep("DELETE FROM autoslicer_log WHERE created < DATE_SUB(NOW(), INTERVAL 45 DAY) ");
$query->execute();

$query = DB::prep("DELETE FROM autoslicer_run WHERE created < DATE_SUB(NOW(), INTERVAL 45 DAY) ");
$query->execute();

$query = DB::prep("DELETE FROM api_log WHERE created < DATE_SUB(NOW(), INTERVAL 90 DAY) ");
$query->execute();

//TODO remove parent key, conflict during deletion
//$query = DB::prep("DELETE FROM client_log WHERE created < DATE_SUB(NOW(), INTERVAL 90 DAY) ");
//$query->execute();

?>