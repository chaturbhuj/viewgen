<?php

$IS_CRON_JOB = true;

chdir(__DIR__);

include_once(__DIR__ . '/../db.php');

$userStatistics = [];
$daysToCheck = 14;

for ($daysAgo = 1; $daysAgo <= $daysToCheck; $daysAgo++) {
	$date = date('Y-m-d', time() - 24*3600*$daysAgo);

	$query = DB::prep("SELECT * FROM people WHERE sent_to_crawler_date = :sent_to_crawler");
	$query->execute([
		'sent_to_crawler' => $date
	]);

	while ($people = $query->fetch()) {
		if (!isset($userStatistics[$people['crawled_by_id']])) {
			$userStatistics[$people['crawled_by_id']] = [
				'visits' => [],
				'viewbacks' => []
			];
		}
		if (!isset($userStatistics[$people['crawled_by_id']]['visits'][$date])) {
			$userStatistics[$people['crawled_by_id']]['visits'][$date] = 0;
		}
		$userStatistics[$people['crawled_by_id']]['visits'][$date]++;
	}
	echo ".";
}

echo "\n";

$query = DB::prep("SELECT * FROM visitors WHERE `date` >= DATE_SUB(DATE(NOW()), INTERVAL 21 DAY) ORDER BY `date` DESC");
$query->execute();

while ($visitors = $query->fetch()) {

	// Loop over this specific week.
	$weekEndDate = $visitors['date'];
	$weekEndTime = strtotime($weekEndDate);

	for ($time = $weekEndTime; $time > $weekEndTime - 7*24*3600; $time -= 24*3600) {
		$date = date('Y-m-d', $time);
		if ($date > date('Y-m-d', time())) continue;

		if (!isset($userStatistics[$visitors['linked_in_user_id']])) {
			$userStatistics[$visitors['linked_in_user_id']] = [
				'visits' => [],
				'viewbacks' => []
			];
		}
		if (!isset($userStatistics[$visitors['linked_in_user_id']]['visits'][$date])) {
			$userStatistics[$visitors['linked_in_user_id']]['visits'][$date] = 0;
		}
		$userStatistics[$visitors['linked_in_user_id']]['viewbacks'][$date] = $visitors['visitor_count'] / 7;
	}
}

// Do the math.

$reset = DB::prep("UPDATE li_users SET account_vbr = null");

$update = DB::prep("UPDATE li_users SET account_vbr = :vb_rate, account_volume = :account_volume
	WHERE linked_in_id = :linked_in_id");

foreach ($userStatistics as $linked_in_id => $stats) {
	$visits = 0;
	$viewbacks = 0;
	$daysActive = 0;
	for ($i = 1; $i <= $daysToCheck; $i++) {
		$date = date('Y-m-d', time() - 24*3600*$i);
		if (isset($stats['visits'][$date])) $visits += $stats['visits'][$date];
		if (isset($stats['viewbacks'][$date])) $viewbacks += $stats['viewbacks'][$date];
		if (isset($stats['visits'][$date]) && $stats['visits'][$date] > 0) {
			$daysActive++;
		}
	}

	if ($visits > 2000) {
		$update->execute([
			'account_volume' => $visits / $daysActive,
			'vb_rate' => $viewbacks / $visits,
			'linked_in_id' => $linked_in_id
		]);
	}
}

?>
