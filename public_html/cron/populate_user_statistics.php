<?php
$IS_CRON_JOB = true;

chdir(__DIR__);

require_once(__DIR__ . '/../db.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

	
echo "Started campaign aggregation - ".date("H:i:s")."\n";

$query = DB::prep("
SELECT * 
FROM campaign 
WHERE 
	publish_status = 'published' or 
	publish_status = 'archived'
");

$query->execute();
$aggregates = array();
while ($data = $query->fetch()) {
	
	$userKey = $data["company_id"]."-".$data["visit_by_id"];
	$status = $data["status"];
	if ($data["publish_status"] == "archived") {
		$status = "Archived";
	}
	if (!isset($aggregates[$userKey])) {
		$aggregates[$userKey] = array();

		
		$aggregates[$userKey]["company_id"] = $data["company_id"];
		$aggregates[$userKey]["li_user_id"] = $data["visit_by_id"];
		$aggregates[$userKey]["Active_campaign_count"] = 0;
		$aggregates[$userKey]["Paused_campaign_count"] = 0;
		$aggregates[$userKey]["Finished_campaign_count"] = 0;
		$aggregates[$userKey]["Archived_campaign_count"] = 0;
		$aggregates[$userKey]["visitors_during_visiting"] = 0;

		$aggregates[$userKey]["total_visited"] = 0;
		$aggregates[$userKey]["total_days_visiting"] = 0;
		$aggregates[$userKey]["first_visit"] = "";
		$aggregates[$userKey]["last_visit"] = "";

		$aggregates[$userKey]["first_visitor"] = "";
		$aggregates[$userKey]["last_visitor"] = "";

		$aggregates[$userKey]["visitors_before_first_visit"] = 0;
		$aggregates[$userKey]["visitors_after_last_visit"] = 0;
		$aggregates[$userKey]["visitors_during_visiting"] = 0;


		$aggregates[$userKey]["weeks_before"] = 0;
		$aggregates[$userKey]["weeks_after"] = 0;
		$aggregates[$userKey]["weeks_during"] = 0;




	} 
    $aggregates[$userKey]["visitors_during_visiting"] += $data["total_visitors"];
	if ($status != "Deleted") {
		$aggregates[$userKey][$status."_campaign_count"] += 1;
	}
		
	
}

$query = DB::prep("
UPDATE li_users
SET
	active_campaign_count = :active_campaign_count,
	finished_campaign_count = :finished_campaign_count,
	archived_campaign_count = :archived_campaign_count,
	paused_campaign_count = :paused_campaign_count
WHERE 
	linked_in_id = :linked_in_id AND
	company_id = :company_id
");
foreach ($aggregates as $aggregate) {
	if ($aggregate["li_user_id"] != 0) {
		$query->execute([
			"active_campaign_count" => $aggregate["Active_campaign_count"],
			"finished_campaign_count" => $aggregate["Finished_campaign_count"],
			"archived_campaign_count" => $aggregate["Archived_campaign_count"],
			"paused_campaign_count" => $aggregate["Paused_campaign_count"],
			"linked_in_id" => $aggregate["li_user_id"],
			"company_id" => $aggregate["company_id"]
		]);
	}
}

echo "Started daily statistics aggregation - ".date("H:i:s")."\n";

$query = DB::prep("
SELECT * 
FROM reports_cache
ORDER BY cache_date
");

$query->execute();
while ($data = $query->fetch()) {
	
	$userKey = $data["company_id"]."-".$data["li_users_id"];
	
	
	if (!isset($aggregates[$userKey])) {
		$aggregates[$userKey] = array();
		$aggregates[$userKey]["li_user_id"] = $data["li_users_id"];
		$aggregates[$userKey]["company_id"] = $data["company_id"];
		$aggregates[$userKey]["total_visited"] = 0;
		$aggregates[$userKey]["total_days_visiting"] = 0;
		$aggregates[$userKey]["first_visit"] = "";
		$aggregates[$userKey]["last_visit"] = "";
		
		$aggregates[$userKey]["first_visitor"] = "";
		$aggregates[$userKey]["last_visitor"] = "";
		
		$aggregates[$userKey]["visitors_before_first_visit"] = 0;
		$aggregates[$userKey]["visitors_after_last_visit"] = 0;
		$aggregates[$userKey]["visitors_during_visiting"] = 0;
		
		
		$aggregates[$userKey]["weeks_before"] = 0;
		$aggregates[$userKey]["weeks_after"] = 0;
		$aggregates[$userKey]["weeks_during"] = 0;
		
		
	}
	if ($data["number_of_visits"] > 0 and $aggregates[$userKey]["first_visit"] == "") {
		$aggregates[$userKey]["first_visit"] = $data["cache_date"];
	}
	$aggregates[$userKey]["total_visited"] += $data["number_of_visits"];
	$aggregates[$userKey]["total_days_visiting"] += 1;
	if ($aggregates[$userKey]["first_visit"] == "") {
		$aggregates[$userKey]["days_before_first_visit"] += 1;
	}
	if ($data["number_of_visits"] > 0) {
		$aggregates[$userKey]["last_visit"] = $data["cache_date"];
	}
	
}

//
//$query = DB::prep("
//SELECT *
//FROM visitors
//ORDER BY date
//
//");
//
//$query->execute();
//
//
//while ($data = $query->fetch()) {
//
//	$userKey = $data["company_id"]."-".$data["linked_in_user_id"];
//
//
//	if (isset($aggregates[$userKey])) {
//		if ($aggregates[$userKey]["first_visitor"] == "") {
//			$aggregates[$userKey]["first_visitor"] = date('Y-m-d', strtotime($data["date"].' - 6 days'));
//		}
//		$aggregates[$userKey]["last_visitor"] = $data["date"];
//		if ($data["date"] < $aggregates[$userKey]["first_visit"]) {
//			$aggregates[$userKey]["visitors_before_first_visit"] += $data["visitor_count"];
//			$aggregates[$userKey]["weeks_before"] ++;
//		} else if (date('Y-m-d', strtotime($data["date"].' - 7 days')) > $aggregates[$userKey]["last_visit"]) {
//			$aggregates[$userKey]["visitors_after_last_visit"] += $data["visitor_count"];
//			$aggregates[$userKey]["weeks_after"]  ++;
//		} else {
//			$aggregates[$userKey]["visitors_during_visiting"] += $data["visitor_count"];
//			$aggregates[$userKey]["weeks_during"] ++;
//		}
//
//	}
//
//}

$query = DB::prep("
UPDATE li_users
SET
	total_visited = :total_visited,
	total_days_visiting = :total_days_visiting,
	first_visit = :first_visit,
	last_visit = :last_visit,
	first_visitor = :first_visitor,
	last_visitor = :last_visitor,
	visitors_before_first_visit = :visitors_before_first_visit,
	visitors_after_last_visit = :visitors_after_last_visit,
	visitors_during_visiting = :visitors_during_visiting,
	avg_views_before_sq = :avg_views_before_sq,
	avg_views_with_sq = :avg_views_with_sq,
	vbr_during_visiting = :vbr_during_visiting
WHERE 
	linked_in_id = :linked_in_id AND
	company_id = :company_id
");
foreach ($aggregates as $aggregate) {
	if ($aggregate["li_user_id"] != 0) {
		$avgViewsBeforeSQ = 0;
		$avgViewsWithSQ = 0;
		$vbr_during_visiting = 0;
//		if ($aggregate["weeks_before"] > 0) {
//			$avgViewsBeforeSQ = $aggregate["visitors_before_first_visit"]/$aggregate["weeks_before"];
//		}
//		if ($aggregate["weeks_during"] > 0 and $aggregate["total_visited"] > 2000) {
		if ($aggregate["total_visited"] > 2000) {
//			$avgViewsWithSQ = $aggregate["visitors_during_visiting"]/$aggregate["weeks_during"];
			$vbr_during_visiting = ($aggregate["visitors_during_visiting"]/$aggregate["total_visited"])*100;
		}
		/*if ($aggregate["first_visit"] > $aggregate["first_visitor"]) {
			$date1 = new DateTime($aggregate["first_visitor"]);
			$date2 = new DateTime($aggregate["first_visit"]);
			$diff = $date2->diff($date1)->format("%a");
			$avgViewsBeforeSQ = ($aggregate["visitors_before_first_visit"]/$diff)*7;
		}
		if ($aggregate["last_visit"] > $aggregate["first_visit"]) {
			$date1 = new DateTime($aggregate["first_visit"]);
			$date2 = new DateTime($aggregate["last_visit"]);
			$diff = $date2->diff($date1)->format("%a");
			$avgViewsWithSQ = ($aggregate["visitors_during_visiting"]/$diff)*7;
		}*/
		
		$query->execute([
			"total_visited" => $aggregate["total_visited"],
			"total_days_visiting" => $aggregate["total_days_visiting"],
			"first_visit" => $aggregate["first_visit"],
			"last_visit" => $aggregate["last_visit"],
			"first_visitor" => $aggregate["first_visitor"],
			"last_visitor" => $aggregate["last_visitor"],
			"visitors_before_first_visit" => $aggregate["visitors_before_first_visit"],
			"visitors_after_last_visit" => $aggregate["visitors_after_last_visit"],
			"visitors_during_visiting" => $aggregate["visitors_during_visiting"],
			"avg_views_before_sq" => $avgViewsBeforeSQ,
			"avg_views_with_sq" => $avgViewsWithSQ,
			"vbr_during_visiting" => $vbr_during_visiting,
			"linked_in_id" => $aggregate["li_user_id"],
			"company_id" => $aggregate["company_id"]
		]);
	}
}


echo "Finished - ".date("H:i:s")."\n";
?>
