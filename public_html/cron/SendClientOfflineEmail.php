<?php
$IS_CRON_JOB = true;

chdir(__DIR__ . "/..");

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../classes/EmailTemplate.php');

$query = DB::prep("
					SELECT
						li_users.*, company.company_name, company.email as company_email
					FROM
						li_users
						JOIN company ON (company.id = li_users.company_id)
					WHERE
						software_status = 'offline'
						AND last_connection > 0
						AND ABS(DATEDIFF(last_connection, now())) > 0 AND MOD(ABS(DATEDIFF(last_connection, now())), 2) = 0
						AND company.frozen <> 1
						AND li_users.hidden <> 1
						AND company.deleted <> 1
					");
$query->execute();

$companies = [];
while ($row = $query->fetch()) {
	if(!isset($companies[$row['company_id']])){
		$companies[$row['company_id']] = [
			'company name' => $row['company_name'],
			'company email' => $row['company_email'],
			'users' => $row['name'],
            'company_id' => $row['company_id']
		];
	} else {
		$companies[$row['company_id']]['users'] .= ", ".$row['name']."\r\n";
	}

	if(!empty($row['user_email'])){
        $emailTemplate = new EmailTemplate($row['company_id']);
        $variables = [
            'company name' => $row['company_name'],
            'user' => $row['name']
        ];
        $emailTemplate->sendEmail($row['user_email'], "Software offline user", $variables);
        echo "Sent to user: ".$row['id']." ".$row['user_email']." ".$row['name']."\r\n";
    }

}

foreach ($companies as $company){
	$emailTemplate = new EmailTemplate($company['company_id']);
	$variables = [
		'users' => $company['users'],
		'company name' => $company['company name']
	];
	$emailTemplate->sendEmail($company['company email'], "Software offline", $variables);
    echo "Sent to company: ".$company['company_id']." ".$company['company email']." ".$company['users'];
}

