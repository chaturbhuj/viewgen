<?php
$IS_CRON_JOB = true;

chdir(dirname(__FILE__));

include (__DIR__ . '/../db.php');

$query = DB::prep("
	UPDATE 
		vg_settings A
		INNER JOIN vg_settings B 
			ON A.object_id = B.object_id
			   AND A.name = 'email_extractor-limit'
			   AND B.name = 'email_extractor-expiration_date'
	SET 
		A.value = 0
	WHERE 
		DATE(B.value) < DATE(NOW())
;");

$query->execute();


?>
