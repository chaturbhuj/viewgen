<?php
global $CONFIG;
$IS_CRON_JOB = true;

require_once __DIR__ . '/../../vendor/autoload.php';
require_once(__DIR__ . '/../db.php');
require_once (__DIR__ . '/../lib/recurly.php');

require_once __DIR__.'/../intercom/helper.php';

$intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);

$query = DB::prep("
                    SELECT
                      c.*, rc_account.account_code AS rc_account_code 
                    FROM 
                      company c
                      LEFT JOIN rc_account ON c.id = rc_account.company_id
                    WHERE 
                      c.email != ''
                      AND c.company_name NOT LIKE '%NotInUse%' 
                    ORDER BY
                      c.id DESC
                  ");
$query->execute();

$numUpdated = 0;
while ($company = $query->fetch()) {
	$company['email'] = preg_replace('# KILLED DRAFT.*#', '', $company['email']);

	if (str_replace(".", "", $company['email']) == "josefcullhed@gmailcom" || str_replace(".", "", $company['email']) == "ivanhallbergryman@gmailcom") {
		continue;
	}

	echo "Company: ".$company['id']."\r\n";
	try{
        $intercomHelper->pushCompanyData($company, true);
    } catch (Exception $e) {
        var_dump("Error company: ", $e);
    }

    $users = DB::prep("
                SELECT
                    li_users.*, client_versions.name AS version_name 
                FROM 
                    li_users
                    LEFT JOIN client_versions ON (li_users.client_version_id = client_versions.id)
                WHERE 
                    company_id = ?
              ");
    $users->execute([$company['id']]);
    while ($user = $users->fetch()) {
        echo "User: ".$user['id']."\r\n";
        try{
            $intercomHelper->pushUserData($user, $company['company_do_not_email']);
        } catch (Exception $e) {
            var_dump("Error User: ", $e);
        }
    }
}

