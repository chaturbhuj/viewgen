<?php
$HTML_TITLE = "Download Client Software";
require_once('include/session.php');
require_once('shared_functions.php');

$STOPMENU = true;

require_once('header.php');

$HIDE_LOGIN_MENU = true;
require_once('header_index.php');

// Add the customer to the mailchimp list.
//include('classes/mailchimp/Mailchimp.php');

try {
	$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
	$query->execute([
		'company_id' => $_COMPANY_DATA['id']
	]);

	if ($company = $query->fetch()) {
//		$mailChimp = new MailChimp("4ebb3ecef8c8440befdf8860f534292d-us8");
//		$list_id = "a98f09c59c";
//
//		$email = $company['email'];
//		$firstName = $company['first_name'];
//		$lastName = $company['last_name'];
//
//		$mailChimp->lists->subscribe($list_id, array('email' => $email), [
//			'FNAME' => $firstName,
//			'LNAME' => $lastName,
//			"optin_ip" => $_SERVER['REMOTE_ADDR'],
//			"optin_time" => date("Y-m-d H:i:s", time()),
//			"MMERGE3" => "Customer"
//		], 'html', false, true);
	}
} catch (Exception $error) {
	
}

?>
<div class="segment sign_up_title">
	<div class="segment_inner">
		<h1>Sign Up</h1>
	</div>
</div>
<div class="segment sign_up_step_two">
	<div class="segment_inner install">
		<div class="sign_up_top">
			<div class="sign_up_steps">
				<div>
					<h2>Step 1</h2>
					<span>Fill in your details</span>
					<i></i>
				</div>
				<div>
					<h2>Step 2</h2>
					<span>Payment details</span>
					<i></i>
				</div>
				<div class="active">
					<h2>Step 3</h2>
					<span>Download client</span>
					<i></i>
				</div>
			</div>
		</div>
		<h1>Download & Install the Client Software</h1>
		<div class="install_instructions">
			<ol>
				<li>Download the software by clicking download</li>
				<li>Install the software on your computer</li>
				<li>Sign in to LinkedIn inside the software</li>
				<li>Click on Next below to set up your first campaign</li>
			</ol>
			<div class="install_finish">
				<a href="<?=base_url('dashboard.php?company_id='.$_COMPANY_DATA['id'])?>" class="button green medium install_finish">Next</a>
			</div>
		</div>
		<div class="install_links">
			<?php if (get_os() == "mac") { ?>
			<a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=mac')?>" class="button blue medium cloud"><i></i>Download<span>Mac OS X<?= get_mac_client_size(" · ") ?></span></a>
			<?php } else { ?>
			<a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=windows')?>" class="button blue medium cloud"><i></i>Download<span>Windows<?= get_windows_client_size(" · ") ?></span></a>
			<?php } ?>
			<a class="help">
			<i class="help"><div>
				The software finds and visits people who 
are of interest for you. Since you are the one 
visiting, we want the visits to come from 
your computer. 
				</div></i>Why do I need to install this?</a>
			<span class="all_versions">All versions:</span>
			<div class="version">
				<h2>Mac OS X 10.7.0+</h2>
				<span>Download for <a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=mac')?>">Mac OS X</a><?= get_mac_client_size(" · ") ?></span>
			</div>
			<div class="version">
				<h2>Windows XP/Vista/7/8</h2>
				<span>Download for <a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=windows')?>">Windows</a><?= get_windows_client_size(" · ") ?></span>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="segment bottom">
	<div class="segment_inner">
	</div>
</div>
<?php

require_once('footer.php');

?>
