# README #

A step by step guide to set up ViewGen on a dev server and run it with minimum example data.

### 1 Set up the server ###
Run a linux server, we run Ubuntu 16.04.1. The following packages are required:


Install packages:
```
#!bash
sudo apt-get update
sudo apt-get install nginx mysql-server-5.7 php7.0 php7.0-fpm php7.0-mbstring php7.0-mysql php7.0-curl php7.0-xml
sudo apt-get install geoip-database geoip-database-extra php-geoip
```

##Edit /etc/php/7.0/fpm/php.ini

```
memory_limit = 512M
short_open_tag = On
```

##Production setup
```sh
mkdir /var/www/live
git clone https://username@bitbucket.org/joscul/viewgentools.git /var/www/live
cd /var/www/live
sudo cp install/nginx-setup/viewgentools.com /etc/nginx/sites-enabled/
```

##Development setup:
```sh
git clone https://username@bitbucket.org/joscul/viewgentools.git ~/some/path/to-you-viewgen-src
sudo ln -s ~/some/path/to-you-project-src /phpdevel
cd /phpdevel
sudo cp install/nginx-setup/viewgentools.com_local /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/viewgentools.com_local /etc/nginx/sites-enabled/viewgentools.com_local
```

##Common setup
[Optional] Get geoip paths:
```
   dpkg -L geoip-database
   dpkg -L geoip-database-extra
```

/etc/nginx/nginx.conf
```
http {
    ...
    geoip_country /some-path-from-above/GeoIP.dat;
    geoip_city /some-path-from-above/GeoIPCity.dat;
    ...
```
```
sudo cp install/nginx-setup/fastcgi_params2 /etc/nginx

sudo systemctl restart nginx
```

##Database setup
### MySQL 5.7+ compatibility fix

We have problems with zero dates 0000-00-00 00:00:00

```sh
cat > /etc/mysql/conf.d/disable_strict_mode.cnf
```
```
[mysqld]
sql_mode=IGNORE_SPACE,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

### Setup database from scratch
Set up the mysql database: Start with opening a mysql shell and create the following databases:

```
mysql -u root -p

create database searchquant;
create database viewgentools_avada;
create database viewgentools_blog;
create database viewgentools_help;

```
Import database
```
cd install

gunzip -k viewgen_data.sql.gz viewgentools_avada.sql.gz viewgentools_blog.sql.gz viewgentools_help.sql.gz

mysql -u root -p

use searchquant
source viewgen_schema.sql
source viewgen_data.sql
use viewgentools_avada
source viewgentools_avada.sql
use viewgentools_blog
source viewgentools_blog.sql
use viewgentools_help
source viewgentools_help.sql
```
###Alternative DB setup from a dump

Import main ViewGen and viewgentools_avada DB dumps from mysql:

```sh
mysqldump -u root viewgentools_avada > viewgentools_avada.sql
#jozo database contains staging testing data
mysqldump -u root jozo > viewgen.sql
scp root@dev.viewgentools.com:/var/www/viewgentools_avada.sql viewgentools_avada.sql
scp root@dev.viewgentools.com:/var/www/viewgen.sql viewgen.sql
```

```
mysql -u root -p
```

```
create database viewgen;
use viewgen;
source viewgen.sql

create database viewgentools_avada;
use viewgentools_avada;
source viewgentools_avada.sql
```

#### [Production] Change the database credentials in the following files:
```
db.php
help/wp-config.php
blog/wp-config.php
```
####OR, [Development] create a settings.local.php and set your credentials:
```sh
cp settings.local.php.example settings.local.php
```

####A sample account with sample data can be found with the following login:
```
email: demo@example.com
password: demo@example.com
```

####restart services
```
sudo systemctl restart php7.0-fpm
sudo systemctl restart nginx
```

####install this Chrome extension for debug sesison activation

https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc/related?hl=en

#### PhpStorm
after all steps above, follow this for PhpStorm
https://confluence.jetbrains.com/display/PhpStorm/Zero-configuration+Web+Application+Debugging+with+Xdebug+and+PhpStorm

## logs
tail -f /var/log/nginx/error.log


##Debugging

### [OPTIONAL] install XDebug
```bash
sudo apt-get install php-xdebug
sudo nano /etc/php/7.0/mods-available/xdebug.ini
```

```
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.show_local_vars=0
xdebug.var_display_max_data=10000
xdebug.var_display_max_depth=20
xdebug.show_exception_trace=1
xdebug.show_error_trace=1

```

