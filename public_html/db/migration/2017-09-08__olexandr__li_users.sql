ALTER TABLE `li_users`
  ADD COLUMN `last_invitations_check` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
  AFTER `last_email_check`,
  ADD INDEX `idx_last_invitations_check` (`last_invitations_check`);