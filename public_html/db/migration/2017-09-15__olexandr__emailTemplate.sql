UPDATE `emailTemplate`
SET `emailTemplateSubject` = 'Your ViewGenTools campaign finished',
  `emailTemplateBody` = '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Hi there,&nbsp;</span></p>
 <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">This is an alert to let you know that the campaign "{campaign name}" for ViewGenTools.com user "{linkedin first name} {linkedin last name}" has finished. The campaign visited {num profiles visited} profiles.&nbsp;</span></p>
 <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Here is a link to the report for this campaign:&nbsp;</span><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">{campaign link}</span></p>
 <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">- ViewGenTools</span></p>'
WHERE `emailTemplateName` = 'Campaign Finished'