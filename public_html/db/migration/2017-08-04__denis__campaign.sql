alter table campaign add column base_url VARCHAR(2048) NULL;
ALTER TABLE campaign MODIFY COLUMN original_url VARCHAR(10000) NOT NULL;