ALTER TABLE `li_users`
    DROP COLUMN `anonymousHtml`,
    DROP COLUMN `wizard_finished`,
    DROP COLUMN `wizard_data`,
    DROP INDEX `wizard_finished` ;
