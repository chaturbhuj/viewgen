ALTER TABLE `search_url`
  ADD COLUMN `skipped_count` BIGINT(12) NOT NULL DEFAULT 0,
  ADD COLUMN `connected_skipped_count` BIGINT(12) NOT NULL DEFAULT 0,
  ADD COLUMN `nopublic_skipped_count` BIGINT(12) NOT NULL DEFAULT 0,
  ADD COLUMN `visited_skipped_count` BIGINT(12) NOT NULL DEFAULT 0,
  ADD INDEX `idx_skipped_count` (`skipped_count`),
  ADD INDEX `idx_connected_skipped_count` (`connected_skipped_count`),
  ADD INDEX `idx_nopublic_skipped_count` (`nopublic_skipped_count`),
  ADD INDEX `idx_visited_skipped_count` (`visited_skipped_count`);