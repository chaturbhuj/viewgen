ALTER TABLE `campaign`
  MODIFY `search_last_name` TEXT NULL,
  MODIFY `search_first_name` TEXT NULL,
  MODIFY `years_of_experience` TEXT NULL,
  MODIFY `years_at_current_company` TEXT NULL,
  MODIFY `years_in_current_position` TEXT NULL,
  MODIFY `postal_code` varchar(30) NULL,
  MODIFY `postal_code_country` varchar(30) NULL,
  MODIFY `postal_code_within` int(11) NULL,
  MODIFY `functions` TEXT NULL,
  MODIFY `company_sizes` TEXT NULL,
  MODIFY `seniority_levels` TEXT NULL,
  MODIFY `titles` TEXT NULL,
  MODIFY `title_scope` varchar(100) NULL;