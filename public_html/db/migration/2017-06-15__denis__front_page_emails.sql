ALTER TABLE front_page_emails ADD contact_type varchar(30) NULL default NULL;
ALTER TABLE front_page_emails ADD INDEX idx_contact_type (contact_type);

ALTER TABLE front_page_emails ADD name varchar(200) NULL default NULL;
ALTER TABLE front_page_emails ADD INDEX idx_name (name);

ALTER TABLE front_page_emails ADD phone varchar(30) NULL default NULL;
ALTER TABLE front_page_emails ADD INDEX idx_phone (phone);

ALTER TABLE front_page_emails ADD message text NULL default NULL;

ALTER TABLE front_page_emails ADD created TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE front_page_emails ADD INDEX idx_created (created);

