CREATE TABLE IF NOT EXISTS `api_bad_pages` (
  `id`             BIGINT(12)   NOT NULL AUTO_INCREMENT,
  `created`        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id`     BIGINT(12)   NOT NULL,
  `linkedin_id`    VARCHAR(255) NOT NULL,
  `dump_url`       TEXT         NOT NULL,
  `message`        TEXT                  DEFAULT '',
  `html_dump_id`   INT(12)               DEFAULT NULL,
  `campaign_id`    INT(12)               DEFAULT NULL,
  `search_url_id`  INT(12)               DEFAULT NULL,
  `people_list_id` INT(12)               DEFAULT NULL,
  `html_dump`      LONGTEXT     NOT NULL,
  `version_id`     INT(12)      NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_idx` (`created`),
  KEY `company_id_idx` (`company_id`),
  KEY `linkedin_id_idx` (`linkedin_id`),
  KEY `html_dump_id_idx` (`html_dump_id`),
  KEY `campaign_id_idx` (`campaign_id`),
  KEY `search_url_id_idx` (`search_url_id`),
  KEY `people_list_id_idx` (`people_list_id`),
  KEY `version_id_idx` (`version_id`)
);