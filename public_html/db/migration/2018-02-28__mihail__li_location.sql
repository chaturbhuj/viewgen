ALTER TABLE `li_location`
  ADD COLUMN `is_country` TINYINT(1) DEFAULT 0,
  ADD COLUMN `is_state`   TINYINT(1) DEFAULT 0,
  ADD INDEX idx_is_country (is_country),
  ADD INDEX idx_is_state (is_state)
;

UPDATE li_location SET is_country = 1 WHERE linked_in_id like '%:0';
UPDATE li_location SET is_state = 1 WHERE linked_in_id IN('us:dc', 'us:wy', 'us:wi', 'us:wv', 'us:wa', 'us:va', 'us:vt', 'us:ut', 'us:tx', 'us:tn', 'us:sd', 'us:sc', 'us:ri', 'us:pa', 'us:or', 'us:ok', 'us:oh', 'us:nd', 'us:nc', 'us:ny', 'us:nm', 'us:nj', 'us:nh', 'us:nv', 'us:ne', 'us:mt', 'us:mo', 'us:ms', 'us:mn', 'us:mi', 'us:ma', 'us:md', 'us:me', 'us:la', 'us:ky', 'us:ks', 'us:ia', 'us:in', 'us:il', 'us:id', 'us:hi', 'us:ga', 'us:fl', 'us:de', 'us:ct', 'us:co', 'us:ca', 'us:ar', 'us:az', 'us:ak', 'us:al');



