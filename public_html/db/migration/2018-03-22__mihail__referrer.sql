CREATE TABLE IF NOT EXISTS
  vg_referrer_email
(
  id INT(10) AUTO_INCREMENT NOT NULL PRIMARY KEY,
  created_at TIMESTAMP DEFAULT NOW(),
  message TEXT,
  referrer_id INT(10) NOT NULL
)
;

CREATE TABLE IF NOT EXISTS
  vg_referrer
  (
    id INT(10) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    company_id bigint(12) NOT NULL,
    referred_email varchar(128) UNIQUE NOT NULL,
    first_name VARCHAR(64),
    last_name VARCHAR(64),
    hash VARCHAR(32),
    created_at TIMESTAMP DEFAULT NOW(),
    referred_company_id bigint(12) DEFAULT NULL,
    linked_by ENUM('link', 'email') DEFAULT 'link',
    converted_at TIMESTAMP NULL DEFAULT NULL,
    paid_at TIMESTAMP NULL DEFAULT NULL,
    notes TEXT DEFAULT NULL
  )
;

INSERT INTO `emailTemplate` (
  `emailTemplateStatus`,
  `emailTemplateName`,
  `emailTemplateSubject`,
  `emailTemplateBody`,
  `availableVariables`,
  `emailTemplateOrder`,
  `numberOfSentEmails`)
VALUES (
  'active',
  'Add Referrer',
  'ViewGenTool.com Invite',
  "Hey, {firstName},
   I`m using ViewGwn to generate tons of inbound leads. Click on the link below to get a trial of ViewGen.
   {referLink}


   {companyFirstName} {companyLastName}
   {companyEmail}
  ",
  '["firstName","referLink", "companyName", "companyFirstName", "companyLastName", "companyEmail"]',
  16,
  0
)
;