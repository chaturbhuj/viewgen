CREATE TABLE IF NOT EXISTS
  vg_service_runtime
(
  param_id varchar(50) NOT NULL,
  service_id varchar(50) NOT NULL,
  user_id bigint(12) NOT NULL,
  value varchar(255) NOT NULL,
  PRIMARY KEY (param_id, service_id, user_id),
  INDEX vg_service_runtime_param_id_idx (param_id),
  INDEX vg_service_runtime_service_id_idx (service_id),
  INDEX vg_service_runtime_user_id_idx (user_id)
)
;

CREATE TABLE IF NOT EXISTS
  vg_settings
(
  type varchar(50) NOT NULL,
  object_id bigint(12) NOT NULL,
  name varchar(50) NOT NULL,
  value varchar(255) NOT NULL,
  changed_at TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (type, object_id, name),
  INDEX vg_settings_type_idx (type),
  INDEX vg_settings_name_idx (name),
  INDEX vg_settings_object_id_idx (object_id)
)
;

