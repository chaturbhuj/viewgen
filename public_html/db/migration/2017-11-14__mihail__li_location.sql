ALTER TABLE `li_location`
  ADD COLUMN `sales` TINYINT(1) NOT NULL DEFAULT 0
;

INSERT INTO
  `li_location` (`location_name`, `linked_in_id`, `hits`, `sales`)
VALUES
  ('Alabama', 'us:al', 0, 1),
  ('Alaska', 'us:ak', 0, 1),
  ('Arizona', 'us:az', 0, 1),
  ('Arkansas', 'us:ar', 0, 1),
  ('California', 'us:ca', 0, 1),
  ('Colorado', 'us:co', 0, 1),
  ('Connecticut', 'us:ct', 0, 1),
  ('Delaware', 'us:de', 0, 1),
  ('Florida', 'us:fl', 0, 1),
  ('Georgia US', 'us:ga', 0, 1),
  ('Hawaii', 'us:hi', 0, 1),
  ('Idaho', 'us:id', 0, 1),
  ('Illinois', 'us:il', 0, 1),
  ('Indiana', 'us:in', 0, 1),
  ('Iowa', 'us:ia', 0, 1),
  ('Kansas', 'us:ks', 0, 1),
  ('Kentucky', 'us:ky', 0, 1),
  ('Louisiana', 'us:la', 0, 1),
  ('Maine', 'us:me', 0, 1),
  ('Maryland', 'us:md', 0, 1),
  ('Massachusetts', 'us:ma', 0, 1),
  ('Michigan', 'us:mi', 0, 1),
  ('Minnesota', 'us:mn', 0, 1),
  ('Mississippi', 'us:ms', 0, 1),
  ('Missouri', 'us:mo', 0, 1),
  ('Montana', 'us:mt', 0, 1),
  ('Nebraska', 'us:ne', 0, 1),
  ('Nevada', 'us:nv', 0, 1),
  ('New Hampshire', 'us:nh', 0, 1),
  ('New Jersey', 'us:nj', 0, 1),
  ('New Mexico', 'us:nm', 0, 1),
  ('New York', 'us:ny', 0, 1),
  ('North Carolina', 'us:nc', 0, 1),
  ('North Dakota', 'us:nd', 0, 1),
  ('Ohio', 'us:oh', 0, 1),
  ('Oklahoma', 'us:ok', 0, 1),
  ('Oregon', 'us:or', 0, 1),
  ('Pennsylvania', 'us:pa', 0, 1),
  ('Rhode Island', 'us:ri', 0, 1),
  ('South Carolina', 'us:sc', 0, 1),
  ('South Dakota', 'us:sd', 0, 1),
  ('Tennessee', 'us:tn', 0, 1),
  ('Texas', 'us:tx', 0, 1),
  ('Utah', 'us:ut', 0, 1),
  ('Vermont', 'us:vt', 0, 1),
  ('Virginia', 'us:va', 0, 1),
  ('Washington', 'us:wa', 0, 1),
  ('West Virginia', 'us:wv', 0, 1),
  ('Wisconsin', 'us:wi', 0, 1),
  ('Wyoming', 'us:wy', 0, 1)
;