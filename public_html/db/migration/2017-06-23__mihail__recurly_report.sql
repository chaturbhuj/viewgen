CREATE TABLE `rc_account_report` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(12),
  `account_code` varchar(100) NOT NULL UNIQUE,
  `state` enum('active','closed') NOT NULL,
  `email` varchar(256),
  `first_name` varchar(64),
  `last_name` varchar(64),
  `company_name` varchar(64),
  `created_at` datetime,
  `updated_at` datetime,
  `closed_at` datetime,
  `has_active_subscription` tinyint(1),
  `has_past_due_invoice` tinyint(1),
  PRIMARY KEY (`id`),
  KEY (`account_code`)
) ENGINE=InnoDB CHARSET=utf8;

CREATE TABLE `rc_subscription_report` (
  `uuid` varchar(64) NOT NULL,
  `rc_account_report_id` bigint(12) NOT NULL,
  `state` enum('active','canceled','expired','future','in_trial','live','past_due') NOT NULL,
  `plan_code` varchar(32),
  `currency` varchar(8),
  `unit_amount_in_cents` bigint(12),
  `quantity` bigint(12),
  `activated_at` datetime,
  `canceled_at` datetime,
  `expires_at` datetime,
  `current_period_started_at` datetime,
  `current_period_ends_at` datetime,
  `trial_started_at` datetime,
  `trial_ends_at` datetime,
  `has_past_due_invoice` tinyint(1),
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
