ALTER TABLE client_log ADD INDEX created_idx (created);
ALTER TABLE client_log ADD INDEX when_and_who_idx (created, company_id);
