CREATE TABLE IF NOT EXISTS client_versions (
  id int(10) auto_increment primary key,
  name varchar(100) UNIQUE
);

INSERT INTO client_versions (name)(SELECT distinct client_version FROM li_users where client_version <> "");

ALTER TABLE li_users ADD client_version_id int(10) default null, ADD CONSTRAINT FOREIGN KEY (client_version_id) REFERENCES client_versions(id) ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS company_client_version_history (
  company_id bigint(12),
  client_version_id int(10),
  created_at timestamp,
  INDEX company_id (company_id),
  FOREIGN KEY (client_version_id) REFERENCES client_versions(id) ON UPDATE CASCADE,
  FOREIGN KEY (company_id) REFERENCES company(id) ON UPDATE CASCADE
);

UPDATE li_users u INNER JOIN client_versions cv ON u.client_version = cv.name SET u.client_version_id = cv.id;

CREATE TABLE IF NOT EXISTS htmlDump_to_client_version (
  html_dump_id bigint(12),
  client_version_id int(10),
  FOREIGN KEY (client_version_id) REFERENCES client_versions(id) ON UPDATE CASCADE,
  FOREIGN KEY (html_dump_id) REFERENCES htmlDump(htmlDumpId) ON UPDATE CASCADE ON DELETE CASCADE
);

-- last change, after code push

ALTER TABLE li_users DROP client_version;

