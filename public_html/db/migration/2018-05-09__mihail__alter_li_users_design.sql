ALTER TABLE `li_users`
  ADD COLUMN `search_design_version` TINYINT(3) NOT NULL DEFAULT 0,
  ADD INDEX `idx_search_design_version` (`search_design_version`)
;