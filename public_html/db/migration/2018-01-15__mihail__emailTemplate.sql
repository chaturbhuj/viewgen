INSERT INTO `emailTemplate` (
  `emailTemplateStatus`,
  `emailTemplateName`,
  `emailTemplateSubject`,
  `emailTemplateBody`,
  `availableVariables`,
  `emailTemplateOrder`,
  `numberOfSentEmails`)
VALUES (
  'active',
  'Software offline user',
  'ViewGenTools software offline',
  '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Hello {user}.&nbsp;</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Your ViewGen desktop client is offline more than 24 hours.</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">- ViewGenTools</span></p>',
  '["company name","users"]',
  16,
  0
)