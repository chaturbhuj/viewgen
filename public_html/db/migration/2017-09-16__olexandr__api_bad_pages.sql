ALTER TABLE `api_bad_pages`
  ADD COLUMN `page_type`  VARCHAR(255) AFTER `message`,
  ADD INDEX `page_type_idx` (`page_type`);