CREATE TABLE IF NOT EXISTS
people_flags
(
  people_id bigint(12) NOT NULL,
  flag_id INT(3) NOT NULL,
  value TINYINT(1) NOT NULL,
  PRIMARY KEY (people_id, flag_id),
  INDEX people_flags_people_id_idx (people_id),
  INDEX people_flags_flag_id_idx (flag_id),
  INDEX people_flags_value_idx (value),
  FOREIGN KEY (people_id) REFERENCES people (id) ON DELETE CASCADE ON UPDATE CASCADE
)
;

ALTER TABLE
  li_users
    ADD COLUMN invitesLast24Hours INT(4) NOT NULL DEFAULT 0,
    ADD COLUMN invites24HoursLimit INT(4) NOT NULL DEFAULT 100,
    ADD INDEX li_users_invitesLast24Hours_idx (invitesLast24Hours)
;