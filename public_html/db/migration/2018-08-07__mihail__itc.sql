ALTER TABLE `campaign`
  ADD COLUMN `itc` TINYINT NOT NULL DEFAULT 0,
  ADD INDEX `idx_campaign_itc` (`itc`),
  ADD COLUMN `itc_message` TEXT
;