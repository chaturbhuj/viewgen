CREATE TABLE IF NOT EXISTS `client_log` (
  `id` bigint(12) NOT NULL auto_increment,
  `parent_exception_id` bigint(12) NULL,     
  `company_id` bigint(12) NOT NULL,
  `log_type` varchar(512) NOT NULL,     
  `message` text NOT NULL,     
  `stack_trace` text NULL,     
  `linked_in_id` varchar(256) NULL,     
  `linked_in_name` varchar(512) NULL,     
   created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY  (`id`),
   FOREIGN KEY (company_id)
           REFERENCES company(id)
           ON DELETE CASCADE,
   FOREIGN KEY (parent_exception_id)
           REFERENCES client_log(id),
   KEY `company_id_idx` (company_id),
   KEY `log_type_idx` (log_type),
   KEY `linked_in_id_idx` (linked_in_id),
   KEY `linked_in_name_idx` (linked_in_name)
);