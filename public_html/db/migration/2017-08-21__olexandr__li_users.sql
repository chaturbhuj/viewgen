ALTER TABLE `li_users`
  ADD COLUMN `multiple_sales` TINYINT(1) NOT NULL DEFAULT 0 AFTER `have_sales_navigator`,
  ADD COLUMN `last_update_multiple_sales` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `multiple_sales`,
  ADD COLUMN `last_email_check` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `lastSubscriptionsCheck`,
  ADD INDEX `idx_multiple_sales` (`multiple_sales`),
  ADD INDEX `idx_last_email_check` (`last_email_check`),
  ADD INDEX `idx_last_update_multiple_sales` (`last_update_multiple_sales`);