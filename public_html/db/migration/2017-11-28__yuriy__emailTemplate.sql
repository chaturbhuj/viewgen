INSERT INTO `emailTemplate` (
  `emailTemplateStatus`,
  `emailTemplateName`,
  `emailTemplateSubject`,
  `emailTemplateBody`,
  `availableVariables`,
  `emailTemplateOrder`,
  `numberOfSentEmails`)
VALUES (
  'active',
  'Reset Password',
  'ViewGenTools reset password',
  '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Hello {company name}.&nbsp;</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">To change the password, click the link <a href="{reset password url}">{reset password url}</a> .&nbsp;</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">- ViewGenTools</span></p>',
  '["company name","reset password url"]',
  16,
  0
)