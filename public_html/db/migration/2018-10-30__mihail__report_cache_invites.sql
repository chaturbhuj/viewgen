ALTER TABLE
    reports_cache
    ADD COLUMN number_of_invites bigint(12) NOT NULL DEFAULT 0
;