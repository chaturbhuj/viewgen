CREATE TABLE IF NOT EXISTS `predefined_geo` (
  `id`              INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name`            VARCHAR(255) NOT NULL,
  `weight`          INT(4) DEFAULT 0,
  `created_at`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at`      TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS `predefined_geo_to_location` (
  `predefined_geo_id` INT(4) NOT NULL,
  `li_location_id`    bigint(12) NOT NULL,
  `weight`            INT(4) DEFAULT 0,
  `created_at`        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`predefined_geo_id`, `li_location_id`),
  FOREIGN KEY (predefined_geo_id) REFERENCES predefined_geo(id) ON DELETE CASCADE on UPDATE CASCADE,
  FOREIGN KEY (li_location_id) REFERENCES li_location(id) ON DELETE CASCADE on UPDATE CASCADE
);

INSERT INTO `predefined_geo` (`id`, `name`, `weight`) VALUES
  (1, 'Western region (U.S.)', 11),
  (2, 'Midwest region (U.S.)', 10),
  (3, 'Central region (U.S.)', 9),
  (4, 'Northeast region (U.S.)', 8),
  (5, 'Southeast region (U.S.)', 7),
  (6, 'Top 10 Countries', 6),
  (7, 'UK & Europe', 5),
  (8, 'Latin America', 4),
  (9, 'Asia', 3),
  (10, 'Middle East', 2),
  (11, 'Africa', 1)
;

INSERT INTO li_location (`location_name`, `linked_in_id`, `hits`, `sales`) VALUES ('District Of Columbia', 'us:dc', 123421, 1);

INSERT INTO `predefined_geo_to_location` (`predefined_geo_id`, `li_location_id`, `weight`)
  SELECT 1, id, 13 FROM li_location WHERE linked_in_id = 'us:ca'
  UNION
  SELECT 1, id, 12 FROM li_location WHERE linked_in_id = 'us:wa'
  UNION
  SELECT 1, id, 11 FROM li_location WHERE linked_in_id = 'us:az'
  UNION
  SELECT 1, id, 10 FROM li_location WHERE linked_in_id = 'us:co'
  UNION
  SELECT 1, id, 9 FROM li_location WHERE linked_in_id = 'us:ut'
  UNION
  SELECT 1, id, 8 FROM li_location WHERE linked_in_id = 'us:or'
  UNION
  SELECT 1, id, 7 FROM li_location WHERE linked_in_id = 'us:nv'
  UNION
  SELECT 1, id, 6 FROM li_location WHERE linked_in_id = 'us:id'
  UNION
  SELECT 1, id, 5 FROM li_location WHERE linked_in_id = 'us:mt'
  UNION
  SELECT 1, id, 4 FROM li_location WHERE linked_in_id = 'us:wy'
  UNION
  SELECT 1, id, 3 FROM li_location WHERE linked_in_id = 'us:nm'
  UNION
  SELECT 1, id, 2 FROM li_location WHERE linked_in_id = 'us:hi'
  UNION
  SELECT 1, id, 1 FROM li_location WHERE linked_in_id = 'us:ak'
  UNION
  SELECT 2, id, 12 FROM li_location WHERE linked_in_id = 'us:il'
  UNION
  SELECT 2, id, 11 FROM li_location WHERE linked_in_id = 'us:in'
  UNION
  SELECT 2, id, 10 FROM li_location WHERE linked_in_id = 'us:mi'
  UNION
  SELECT 2, id, 9 FROM li_location WHERE linked_in_id = 'us:oh'
  UNION
  SELECT 2, id, 8 FROM li_location WHERE linked_in_id = 'us:mn'
  UNION
  SELECT 2, id, 7 FROM li_location WHERE linked_in_id = 'us:wi'
  UNION
  SELECT 2, id, 6 FROM li_location WHERE linked_in_id = 'us:mo'
  UNION
  SELECT 2, id, 5 FROM li_location WHERE linked_in_id = 'us:ks'
  UNION
  SELECT 2, id, 4 FROM li_location WHERE linked_in_id = 'us:ia'
  UNION
  SELECT 2, id, 3 FROM li_location WHERE linked_in_id = 'us:nd'
  UNION
  SELECT 2, id, 2 FROM li_location WHERE linked_in_id = 'us:sd'
  UNION
  SELECT 2, id, 1 FROM li_location WHERE linked_in_id = 'us:ne'
  UNION
  SELECT 3, id, 8 FROM li_location WHERE linked_in_id = 'us:tx'
  UNION
  SELECT 3, id, 7 FROM li_location WHERE linked_in_id = 'us:ok'
  UNION
  SELECT 3, id, 6 FROM li_location WHERE linked_in_id = 'us:ar'
  UNION
  SELECT 3, id, 5 FROM li_location WHERE linked_in_id = 'us:la'
  UNION
  SELECT 3, id, 4 FROM li_location WHERE linked_in_id = 'us:ms'
  UNION
  SELECT 3, id, 3 FROM li_location WHERE linked_in_id = 'us:al'
  UNION
  SELECT 3, id, 2 FROM li_location WHERE linked_in_id = 'us:tn'
  UNION
  SELECT 3, id, 1 FROM li_location WHERE linked_in_id = 'us:ky'
  UNION
  SELECT 4, id, 9 FROM li_location WHERE linked_in_id = 'us:ny'
  UNION
  SELECT 4, id, 8 FROM li_location WHERE linked_in_id = 'us:ma'
  UNION
  SELECT 4, id, 7 FROM li_location WHERE linked_in_id = 'us:pa'
  UNION
  SELECT 4, id, 6 FROM li_location WHERE linked_in_id = 'us:nj'
  UNION
  SELECT 4, id, 5 FROM li_location WHERE linked_in_id = 'us:ct'
  UNION
  SELECT 4, id, 4 FROM li_location WHERE linked_in_id = 'us:ri'
  UNION
  SELECT 4, id, 3 FROM li_location WHERE linked_in_id = 'us:vt'
  UNION
  SELECT 4, id, 2 FROM li_location WHERE linked_in_id = 'us:me'
  UNION
  SELECT 4, id, 1 FROM li_location WHERE linked_in_id = 'us:nh'
  UNION
  SELECT 5, id, 9 FROM li_location WHERE linked_in_id = 'us:fl'
  UNION
  SELECT 5, id, 8 FROM li_location WHERE linked_in_id = 'us:ga'
  UNION
  SELECT 5, id, 7 FROM li_location WHERE linked_in_id = 'us:va'
  UNION
  SELECT 5, id, 6 FROM li_location WHERE linked_in_id = 'us:nc'
  UNION
  SELECT 5, id, 5 FROM li_location WHERE linked_in_id = 'us:dc'
  UNION
  SELECT 5, id, 4 FROM li_location WHERE linked_in_id = 'us:md'
  UNION
  SELECT 5, id, 3 FROM li_location WHERE linked_in_id = 'us:de'
  UNION
  SELECT 5, id, 2 FROM li_location WHERE linked_in_id = 'us:sc'
  UNION
  SELECT 5, id, 1 FROM li_location WHERE linked_in_id = 'us:wv'
  UNION
  SELECT 6, id, 11 FROM li_location WHERE linked_in_id = 'us:0'
  UNION
  SELECT 6, id, 10 FROM li_location WHERE linked_in_id = 'nl:0'
  UNION
  SELECT 6, id, 9 FROM li_location WHERE linked_in_id = 'gb:0'
  UNION
  SELECT 6, id, 8 FROM li_location WHERE linked_in_id = 'ca:0'
  UNION
  SELECT 6, id, 7 FROM li_location WHERE linked_in_id = 'fr:0'
  UNION
  SELECT 6, id, 6 FROM li_location WHERE linked_in_id = 'it:0'
  UNION
  SELECT 6, id, 5 FROM li_location WHERE linked_in_id = 'es:0'
  UNION
  SELECT 6, id, 4 FROM li_location WHERE linked_in_id = 'mx:0'
  UNION
  SELECT 6, id, 3 FROM li_location WHERE linked_in_id = 'au:0'
  UNION
  SELECT 6, id, 2 FROM li_location WHERE linked_in_id = 'br:0'
  UNION
  SELECT 6, id, 1 FROM li_location WHERE linked_in_id = 'de:0'
  UNION
  SELECT 7, id, 17 FROM li_location WHERE linked_in_id = 'gb:0'
  UNION
  SELECT 7, id, 16 FROM li_location WHERE linked_in_id = 'fr:0'
  UNION
  SELECT 7, id, 15 FROM li_location WHERE linked_in_id = 'de:0'
  UNION
  SELECT 7, id, 14 FROM li_location WHERE linked_in_id = 'nl:0'
  UNION
  SELECT 7, id, 13 FROM li_location WHERE linked_in_id = 'es:0'
  UNION
  SELECT 7, id, 12 FROM li_location WHERE linked_in_id = 'it:0'
  UNION
  SELECT 7, id, 11 FROM li_location WHERE linked_in_id = 'pt:0'
  UNION
  SELECT 7, id, 10 FROM li_location WHERE linked_in_id = 'tr:0'
  UNION
  SELECT 7, id, 9 FROM li_location WHERE linked_in_id = 'dk:0'
  UNION
  SELECT 7, id, 8 FROM li_location WHERE linked_in_id = 'ch:0'
  UNION
  SELECT 7, id, 7 FROM li_location WHERE linked_in_id = 'fi:0'
  UNION
  SELECT 7, id, 6 FROM li_location WHERE linked_in_id = 'no:0'
  UNION
  SELECT 7, id, 5 FROM li_location WHERE linked_in_id = 'pl:0'
  UNION
  SELECT 7, id, 4 FROM li_location WHERE linked_in_id = 'at:0'
  UNION
  SELECT 7, id, 3 FROM li_location WHERE linked_in_id = 'be:0'
  UNION
  SELECT 7, id, 2 FROM li_location WHERE linked_in_id = 'se:0'
  UNION
  SELECT 7, id, 1 FROM li_location WHERE linked_in_id = 'ie:0'
  UNION
  SELECT 8, id, 17 FROM li_location WHERE linked_in_id = 'bo:0'
  UNION
  SELECT 8, id, 16 FROM li_location WHERE linked_in_id = 'br:0'
  UNION
  SELECT 8, id, 15 FROM li_location WHERE linked_in_id = 'cl:0'
  UNION
  SELECT 8, id, 14 FROM li_location WHERE linked_in_id = 'co:0'
  UNION
  SELECT 8, id, 13 FROM li_location WHERE linked_in_id = 'cr:0'
  UNION
  SELECT 8, id, 12 FROM li_location WHERE linked_in_id = 'do:0'
  UNION
  SELECT 8, id, 11 FROM li_location WHERE linked_in_id = 'ec:0'
  UNION
  SELECT 8, id, 10 FROM li_location WHERE linked_in_id = 'sv:0'
  UNION
  SELECT 8, id, 9 FROM li_location WHERE linked_in_id = 'gt:0'
  UNION
  SELECT 8, id, 8 FROM li_location WHERE linked_in_id = 'jm:0'
  UNION
  SELECT 8, id, 7 FROM li_location WHERE linked_in_id = 'mx:0'
  UNION
  SELECT 8, id, 6 FROM li_location WHERE linked_in_id = 'pa:0'
  UNION
  SELECT 8, id, 5 FROM li_location WHERE linked_in_id = 'pe:0'
  UNION
  SELECT 8, id, 4 FROM li_location WHERE linked_in_id = 'pr:0'
  UNION
  SELECT 8, id, 3 FROM li_location WHERE linked_in_id = 'tt:0'
  UNION
  SELECT 8, id, 2 FROM li_location WHERE linked_in_id = 'uy:0'
  UNION
  SELECT 8, id, 1 FROM li_location WHERE linked_in_id = 've:0'
  UNION
  SELECT 9, id, 13 FROM li_location WHERE linked_in_id = 'au:0'
  UNION
  SELECT 9, id, 12 FROM li_location WHERE linked_in_id = 'bd:0'
  UNION
  SELECT 9, id, 11 FROM li_location WHERE linked_in_id = 'cn:0'
  UNION
  SELECT 9, id, 10 FROM li_location WHERE linked_in_id = 'hk:0'
  UNION
  SELECT 9, id, 9 FROM li_location WHERE linked_in_id = 'in:0'
  UNION
  SELECT 9, id, 8 FROM li_location WHERE linked_in_id = 'id:0'
  UNION
  SELECT 9, id, 7 FROM li_location WHERE linked_in_id = 'jp:0'
  UNION
  SELECT 9, id, 6 FROM li_location WHERE linked_in_id = 'kr:0'
  UNION
  SELECT 9, id, 5 FROM li_location WHERE linked_in_id = 'my:0'
  UNION
  SELECT 9, id, 4 FROM li_location WHERE linked_in_id = 'ph:0'
  UNION
  SELECT 9, id, 3 FROM li_location WHERE linked_in_id = 'sg:0'
  UNION
  SELECT 9, id, 2 FROM li_location WHERE linked_in_id = 'th:0'
  UNION
  SELECT 9, id, 1 FROM li_location WHERE linked_in_id = 'vn:0'
  UNION
  SELECT 10, id, 11 FROM li_location WHERE linked_in_id = 'bh:0'
  UNION
  SELECT 10, id, 10 FROM li_location WHERE linked_in_id = 'eg:0'
  UNION
  SELECT 10, id, 9 FROM li_location WHERE linked_in_id = 'ir:0'
  UNION
  SELECT 10, id, 8 FROM li_location WHERE linked_in_id = 'il:0'
  UNION
  SELECT 10, id, 7 FROM li_location WHERE linked_in_id = 'jo:0'
  UNION
  SELECT 10, id, 6 FROM li_location WHERE linked_in_id = 'kw:0'
  UNION
  SELECT 10, id, 5 FROM li_location WHERE linked_in_id = 'lb:0'
  UNION
  SELECT 10, id, 4 FROM li_location WHERE linked_in_id = 'ma:0'
  UNION
  SELECT 10, id, 3 FROM li_location WHERE linked_in_id = 'qa:0'
  UNION
  SELECT 10, id, 2 FROM li_location WHERE linked_in_id = 'sa:0'
  UNION
  SELECT 10, id, 1 FROM li_location WHERE linked_in_id = 'ae:0'
  UNION
  SELECT 11, id, 11 FROM li_location WHERE linked_in_id = 'tz:0'
  UNION
  SELECT 11, id, 10 FROM li_location WHERE linked_in_id = 'tn:0'
  UNION
  SELECT 11, id, 9 FROM li_location WHERE linked_in_id = 'ug:0'
  UNION
  SELECT 11, id, 8 FROM li_location WHERE linked_in_id = 'ke:0'
  UNION
  SELECT 11, id, 7 FROM li_location WHERE linked_in_id = 'eg:0'
  UNION
  SELECT 11, id, 6 FROM li_location WHERE linked_in_id = 'gh:0'
  UNION
  SELECT 11, id, 5 FROM li_location WHERE linked_in_id = 'mu:0'
  UNION
  SELECT 11, id, 4 FROM li_location WHERE linked_in_id = 'ma:0'
  UNION
  SELECT 11, id, 3 FROM li_location WHERE linked_in_id = 'za:0'
  UNION
  SELECT 11, id, 2 FROM li_location WHERE linked_in_id = 'ng:0'
  UNION
  SELECT 11, id, 1 FROM li_location WHERE linked_in_id = 'om:0'
;
