ALTER TABLE `li_users`
  ADD COLUMN `lastAnonymusSalesCheck` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
  AFTER `lastAnonymusCheck`,
  ADD INDEX `idx_lastAnonymusSalesCheck` (`lastAnonymusSalesCheck`),
  ADD COLUMN `anonymousSales` varchar(100)
  AFTER `anonymous`,
  ADD INDEX `idx_anonymousSales` (`anonymousSales`)
;