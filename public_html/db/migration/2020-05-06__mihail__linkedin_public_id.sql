ALTER TABLE
    inbound_visit
    ADD COLUMN linked_in_public_id varchar(255) NOT NULL DEFAULT '',
    ADD INDEX `idx-linked_in_public_id` (`linked_in_public_id`);
;
