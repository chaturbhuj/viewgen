INSERT INTO `emailTemplate` (
  `emailTemplateStatus`,
  `emailTemplateName`,
  `emailTemplateSubject`,
  `emailTemplateBody`,
  `availableVariables`,
  `emailTemplateOrder`,
  `numberOfSentEmails`)
VALUES (
  'active',
  'Add users',
  'ViewGenTool.com Invite',
  "Your company's <a href=\"http://viewgentools.com/\">viewgentools.com</a> account owner has invited you to access the <a href=\"http://viewgentools.com/\">viewgentools.com</a> account they're using, which means you're minutes away from turning LinkedIn into your own inbound nirvana.
  Click here to download the software: <a href=\"http://viewgentools.com/install_instructions.php?company_id={company-id}\"><b>Software Download Page</b></a>
  <a href=\"http://viewgentools.com/get_going.php\">Operating instructions, best practices & sample profiles</a>
  Customer Support
  ",
  '["company-id","company-name", "company-first-name", "company-last-name", "company-email"]',
  16,
  0
)