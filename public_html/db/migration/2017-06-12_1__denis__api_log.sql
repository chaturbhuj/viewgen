CREATE TABLE IF NOT EXISTS `api_log` (
  `id` bigint(12) NOT NULL auto_increment,
  `company_id` bigint(12) NULL,
  `uid` char(40) NOT NULL,   -- sha1 size is char(40)
  `endpoint` char(64) NOT NULL,     
  `error_type` varchar(32) NOT NULL,       
  `logger_error_type` varchar(32) NULL,       
  `message` text NOT NULL,     
  `logger_message` text NULL,     
  `short_message` varchar(255) NOT NULL, -- 255 is a limitation for index     
  `logger_short_message` varchar(255) NULL, 
  `linked_in_id` varchar(128) NULL,     
  `linked_in_name` varchar(128) NULL,     
  `client_version` varchar(128) NULL,     
  `stack_trace` text NULL,     
  `client_json` text NULL,     
   created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY  (`id`),
   FOREIGN KEY (company_id)
           REFERENCES company(id)
           ON DELETE CASCADE,
   KEY `company_id_idx` (company_id),
   KEY `endpoint_idx` (endpoint),
   KEY `error_type_idx` (error_type),
   KEY `logger_error_type_idx` (logger_error_type),
   KEY `short_message_idx` (short_message),
   KEY `logger_short_message_idx` (logger_short_message),
   KEY `linked_in_id_idx` (linked_in_id),
   KEY `linked_in_name_idx` (linked_in_name),
   KEY `client_version_idx` (client_version),
   KEY `created_idx` (created),
   KEY `when_and_who_idx` (created, company_id)
);