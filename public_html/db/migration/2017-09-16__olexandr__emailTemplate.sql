UPDATE `emailTemplate`
SET `emailTemplateBody` = '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Hello - this is an automated email alert from ViewGenTools.&nbsp;</span></p>
 <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">ViewGenTools user "{linkedin first name} {linkedin last name}" has no active campaigns. No active campaigns means no visiting, so if you want to keep up the volume, go to <a href="https://viewgentools.com/dashboard.php">your ViewGenTools account home page</a> and create a new campaign.&nbsp;</span></p>
 <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">- ViewGenTools</span></p>'
WHERE `emailTemplateName` = 'No Active Campaigns'