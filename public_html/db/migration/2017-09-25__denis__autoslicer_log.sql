CREATE TABLE IF NOT EXISTS `autoslicer_run` (
  `id`                    BIGINT(12)   NOT NULL AUTO_INCREMENT,
  `company_id`            BIGINT(12)   NULL,
  `campaign_id`           BIGINT(12)   NULL,
  `client_version`        VARCHAR(128) NULL,
  `client_hash`          VARCHAR(256) NULL,
  `crawled_by_id`        VARCHAR(64) NULL,
  `crawled_by_name`        VARCHAR(128) NULL,
  
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  
  -- denotes that autoslicer entered splitting phase after passing initial checks
  -- (like do we have unckecked URLs for visits)
  -- may be used to health check, if we have many split-pase runs and still not "final"
  -- it means problems in autoslicer
  split_phase varchar(32) NULL,
  
  -- signals that this run is final for this campaign
  -- is set when no more search url to split.
  -- normally, there only one run should happen, if one Original URL present
  final TIMESTAMP NULL DEFAULT NULL,  
  
  PRIMARY KEY  (`id`),
  FOREIGN KEY (company_id)
          REFERENCES company(id)
          ON DELETE CASCADE,
  FOREIGN KEY (campaign_id)
         REFERENCES campaign(id)
         ON DELETE CASCADE,
  KEY `autoslicer_run__company_id_idx` (company_id),
  KEY `autoslicer_run__campaign_id_idx` (campaign_id),
  KEY `autoslicer_run__client_version_idx` (client_version),
  KEY `autoslicer_run__client_hash_idx` (client_hash),
  KEY `autoslicer_run__crawled_by_id_idx` (crawled_by_id),
  KEY `autoslicer_run__crawled_by_name_idx` (crawled_by_name),
  KEY `autoslicer_run__split_phase_idx` (split_phase),
  KEY `autoslicer_run__final_idx` (final)
);

CREATE TABLE IF NOT EXISTS `autoslicer_log` (
  `id`   BIGINT(12)   NOT NULL AUTO_INCREMENT,
  `run_id`   BIGINT(12)   NOT NULL,

  `log_step` int NOT NULL,                        -- sequential number of the autoslicer steps  

  `splitter` varchar(64) NULL,       
  `action` varchar(128) NOT NULL,       
  `split_on` varchar(4096) NULL,       -- the parameter slitting going on

  `is_subsequent` bool NULL,       -- is this second or futher call to the same splitter on current the URL set
  `splits_needed` float NULL,       -- e.g. part of boolean expression, recursively
  `split_steps_done` int NULL,       -- total number of splits executed in this autoslicer session
  `split_steps_success` int NULL,       -- number of successful splits done in this autoslicer session

  `input_urls_number` int NULL,     
  `output_urls_number` int NULL,     

  `input_url` varchar(10000) NULL,        -- makes sense only inside splitter cycle per each input URL       
  `input_urls_set` text NULL,        -- all urls set coming into splitter       
  `output_urls` text NULL,     

  `notes` text NULL,     
   created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

   PRIMARY KEY  (`id`),
   FOREIGN KEY (run_id)
          REFERENCES autoslicer_run(id)
          ON DELETE CASCADE,
   KEY `autoslicer_log__log_step_idx` (log_step),
   KEY `autoslicer_log__input_url_idx` (input_url),
   KEY `autoslicer_log__action_idx` (action),
   KEY `autoslicer_log__splitter_idx` (splitter),
   KEY `autoslicer_log__split_on_idx` (split_on),
   KEY `autoslicer_log__is_subsequent_idx` (is_subsequent),
   KEY `autoslicer_log__splits_needed_idx` (splits_needed),
   KEY `autoslicer_log__split_steps_done_idx` (split_steps_done),
   KEY `autoslicer_log__split_steps_success_idx` (split_steps_success),
   KEY `autoslicer_log__input_urls_number_idx` (input_urls_number),
   KEY `autoslicer_log__output_urls_number_idx` (output_urls_number),
   KEY `autoslicer_log__created_idx` (created)
);