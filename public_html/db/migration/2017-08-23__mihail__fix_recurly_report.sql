ALTER TABLE `rc_account_report`
CHANGE COLUMN `state` `state` VARCHAR(25) NOT NULL ;

ALTER TABLE `rc_subscription_report`
CHANGE COLUMN `state` `state` VARCHAR(25) NOT NULL ;
