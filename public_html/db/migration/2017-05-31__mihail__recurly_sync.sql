ALTER TABLE company ADD recurly_sync TINYINT(1) default 1;

UPDATE company SET recurly_sync = 0;

