INSERT INTO `emailTemplate` (
  `emailTemplateStatus`,
  `emailTemplateName`,
  `emailTemplateSubject`,
  `emailTemplateBody`,
  `availableVariables`,
  `emailTemplateOrder`,
  `numberOfSentEmails`)
VALUES (
  'active',
  'Software offline',
  'ViewGenTools software offline',
  '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Hello {company name}.&nbsp;</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">Following users are offline more than 24 hours: {users}</span></p>
   <p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px;">- ViewGenTools</span></p>',
  '["company name","users"]',
  16,
  0
)