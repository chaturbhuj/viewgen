ALTER TABLE `li_users`
  ADD COLUMN `last_groups_check` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
  AFTER `last_invitations_check`,
  ADD INDEX `idx_last_groups_check` (`last_groups_check`);

CREATE TABLE IF NOT EXISTS `li_groups` (
  `linked_in_id`    BIGINT(12)   NOT NULL PRIMARY KEY,
  `name`            VARCHAR(255) NOT NULL,
  `image`           VARCHAR(255) NULL,
  `created_at`      TIMESTAMP             DEFAULT CURRENT_TIMESTAMP,
  `updated_at`      TIMESTAMP             DEFAULT CURRENT_TIMESTAMP,
  KEY `name_idx` (`name`),
  KEY `created_idx` (`created_at`)
);

CREATE TABLE IF NOT EXISTS `li_users_groups` (
  `user_li_id`      BIGINT(12)   NOT NULL,
  `group_li_id`     BIGINT(12)   NOT NULL,
  `created_at`         TIMESTAMP             DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_li_id`, `group_li_id`),
  KEY `user_id_idx` (`user_li_id`),
  KEY `group_id_idx` (`group_li_id`),
  KEY `created_idx` (`created_at`)
);

ALTER TABLE `campaign`
  ADD COLUMN `groups` TEXT DEFAULT NULL
  AFTER `search_last_name`;
