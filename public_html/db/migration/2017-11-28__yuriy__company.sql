ALTER TABLE company
 ADD COLUMN reset_password_hash VARCHAR(255) NULL,
 ADD COLUMN reset_password_date DATE NULL,
 ADD INDEX `idx_reset_password_hash` (`reset_password_hash`);
