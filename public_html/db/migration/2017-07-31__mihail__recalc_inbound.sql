CREATE TABLE tmp_inbound_visit LIKE inbound_visit;

CREATE TABLE backup_inbound_visit LIKE inbound_visit;
INSERT backup_inbound_visit SELECT * FROM inbound_visit;

CREATE TABLE backup_visit_connection LIKE visit_connection;
INSERT backup_visit_connection SELECT * FROM visit_connection;

CREATE TABLE `tmp_recalc_inbound_state` (
  `linked_in_id` bigint(12),
  `started_at` datetime,
  `delete_started_at` datetime,
  `copy_started_at` datetime,
  `connection_started_at` datetime,
  `ended_at` datetime,
  PRIMARY KEY (`linked_in_id`)
) ENGINE=InnoDB CHARSET=utf8;
