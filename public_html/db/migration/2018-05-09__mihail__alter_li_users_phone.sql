ALTER TABLE `li_users`
  ADD COLUMN `last_phone_check` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
  AFTER `last_email_check`,
  ADD INDEX `idx_last_phone_check` (`last_phone_check`),
  ADD COLUMN `user_phone` varchar(50)
  AFTER `user_email`
;