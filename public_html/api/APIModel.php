<?php

require_once(__DIR__ . '/services/response_svc.php');

use function api\services\response\{
    successResponse
};

//get DB object
try {
    require_once('../db.php');
} catch (Exception $e) {
    throw new Exception('Database connection error.');
}
require_once('htmlStorage.php');
require_once('helpers/ParserHelper.php');
require_once('ParserModel.php');
require_once('models/GetNextUrlModel.php');
require_once('models/autoslicer/Premium.php');
require_once('models/FirstPageModel.php');
require_once('models/PrivacyModel.php');
require_once('models/PrivacySalesModel.php');
require_once('models/InboundSalesParser.php');
require_once('models/SubsParser.php');
require_once('models/EmailParser.php');
require_once('models/PhoneParser.php');
require_once('models/InboundFreeModel.php');
require_once('models/SearchSalesParser.php');
require_once('models/ProfileSalesParser.php');
require_once('models/SearchFreeParser.php');
require_once('FreeParserModel.php');
require_once(__DIR__ . '/models/BadPageHandler.php');
require_once(__DIR__ . '/models/InvitParser.php');
require_once(__DIR__ . '/models/GroupsParser.php');
require_once(__DIR__ . '/models/GNU.php');
require_once(__DIR__ . '/../intercom/helper.php');
require_once(__DIR__ . '/models/services/BaseService.php');
require_once(__DIR__ . '/models/services/UninviterService.php');
require_once(__DIR__ . '/models/services/Manager.php');
require_once(__DIR__ . '/../classes/Settings.php');
require_once(__DIR__.'/../classes/linkedin/search/Url.php');


use api\models\SearchFreeParser;
use api\models\InvitParser;
use api\models\GNU;
use api\models\services\Manager;
use linkedinSearch\Url;

/**
 * class APIModel
 */
class APIModel
{
    //client's versions
    const WINDOWS = 'Windows 1.0';
    const MAC = 'Mac 1.0';
    //Status
    const STATUS_LOGGEDOFF = "Logged Off";
    const STATUS_WAITING = "Waiting";
    const STATUS_RUNNING = "Running";
    //page type
    const FIRSTPAGE = 'firstpage';
    const PERSON_SN = 'personsn';
    const SEARCH_SN = 'searchsn';
    const PERSON_SIMPLE = 'personsimple';
    const SEARCH_SIMPLE = 'searchsimple';
    const PRIVACY_PAGE = 'privacy';
    const PRIVACY_SALES_PAGE = 'privacy-sn';
    const SN_INBOUND_PAGE = 'inbound-sn';
    const FREE_INBOUND_PAGE = 'inbound-free';
    const SUBS_PAGE = 'subscriptions';
    const EMAIL_PAGE = 'email-page';
    const PHONE_PAGE = 'phone-page';
    const INVIT_PAGE = 'invit-page';
    const GROUPS_PAGE = 'groups-page';
    //li-user type
    const FREE = 'free';
    const SALESNAV = 'salesnavigator';
    const RECRUITER = 'recruiter';
    const UNDEF_UT = 'undefined-user-type';
    //
    const UNDEFINED_USER_NAME = 'undefined';
    const DEFAULT_WAIT = 180;
    const SFW_STATUS_WAITING = 'waiting';
    //URLs
    const LOGIN_URL = 'https://www.linkedin.com/uas/login';
    const FREE_SEARCH_URL = 'https://www.linkedin.com/search/results/people/';
    const FREE_PROFILE_URL = 'https://www.linkedin.com/in/';
    const SN_SEARCH_URL = 'https://www.linkedin.com/sales/search';
    const SN_PROFILE_URL = 'https://www.linkedin.com/sales/profile/';
    const PRIVACY_URL = 'https://www.linkedin.com/psettings/profile-visibility';
    const PRIVACY_SALES_URL = 'https://www.linkedin.com/sales/settings?trk=nav_user_menu_manage_sales_nav';
    const SUBS_URL = 'https://www.linkedin.com/premium/manage';
    const EMAIL_URL = 'https://www.linkedin.com/psettings/email';
    const PHONE_URL = 'https://www.linkedin.com/psettings/phone';
    const INVIT_URL = 'https://www.linkedin.com/mynetwork/invitation-manager/';
    const VIEWGEN_MAIL = 'support@viewgentools.com';
    const BASE_VG_URL = 'https://viewgentools.com';
    const GROUPS_URL = 'https://www.linkedin.com/groups/my-groups';


    public static function response(string $status, array $payload = []): void
    {
        header('Content-Type: application/json');
        echo json_encode([
            'status' => $status,
            'payload' => $payload
        ]);
        exit(0);
    }

    /**
     * @return array
     */
    public static function loggedOffReturn()
    {
        return [
            'url' => APIModel::LOGIN_URL,
            'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
            'username' => 'Logged Out',
            'status_message' => [
                'status' => 1,
                'message' => 'You need to sign in'
            ]
        ];
    }

//    public static function repeatRequest($json)
//    {
//        return [
//            'url' => APIModel::LOGIN_URL,
//            'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
//            'username' => 'Logged Out',
//            'status_message' => [
//                'status' => 1,
//                'message' => 'You need to sign in'
//            ]
//        ];
//    }

    /**
     * @param $json
     * @return string version of client
     */
    public static function getVersion($json): string
    {
        if (isset($json->version)) {
            $version = $json->version;
        } elseif (isset($_SERVER['HTTP_USER_AGENT']) and preg_match("#Darwin/#is", $_SERVER['HTTP_USER_AGENT'])) {
            $version = self::MAC;
        } else {
            $version = self::WINDOWS;
        }
        return $version;
    }

    /**
     * @param $productKey
     * @return array|bool company from DB or false
     * @throws Exception
     */
    public static function getCompanyByProductKey($productKey)
    {
        try {
            $query = \DB::prep("SELECT company.* FROM company JOIN software_license ON (software_license.company_id = company.id) WHERE product_key = :product_key");
            $query->execute([
                'product_key' => $productKey
            ]);
            $company = $query->fetch();
            return $company;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * get company users
     * @param int $companyId
     * @return array
     */
    function getCompanyUsers($companyId)
    {
        $query = \DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0");
        $query->execute([
            'company_id' => $companyId
        ]);
        return $query->fetchAll();
    }

    /**
     * @param $companyId
     * @param $searchUrlId
     * @return bool|array
     * @throws Exception
     */
    public static function getPeopleFromUrl($companyId, $searchUrlId)
    {
        try {
            $query = \DB::prep("SELECT 
                                      people.*, 
                                      search_url.*, 
                                      people.id AS people_id, 
                                      people_list.url AS referer 
                                      FROM people 
                                      JOIN search_url ON people.search_url_id = search_url.id 
                                      JOIN people_list ON people.people_list_id = people_list.id 
                                      WHERE search_url.id = :search_url_id AND 
                                            people.name = :people_name AND 
                                            sent_to_crawler = :sent_to_crawler AND 
                                            search_url.finished = 0 AND 
                                            people.company_id = :company_id 
                                      LIMIT 1");
            $query->execute([
                'company_id' => $companyId,
                'search_url_id' => $searchUrlId,
                'people_name' => '',
                'sent_to_crawler' => '0000-00-00 00:00:00'

            ]);
            $people = $query->fetch();
            return $people;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $campaignId
     * @return array|bool
     * @throws Exception
     */
    public static function getCampaignById($campaignId)
    {
        try {
            $query = \DB::prep("SELECT * FROM campaign WHERE id = :id");
            $query->execute([
                'id' => $campaignId
            ]);
            $campaign = $query->fetch();
            return $campaign;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $searchUrlId
     * @return array|bool
     * @throws Exception
     */
    public static function getPeopleListBySearchUrlId($searchUrlId)
    {
        try {
            $query = \DB::prep("SELECT * FROM people_list WHERE search_url_id = :search_url_id AND retry = 1");
            $query->execute([
                'search_url_id' => $searchUrlId
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            $peopleList = $query->fetch();
            return $peopleList;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $searchUrlId
     * @return array|bool
     * @throws Exception
     */
    public static function getMorePeople2Visit($searchUrlId)
    {
        try {
            $query = \DB::prep("SELECT * FROM people_list WHERE search_url_id = :search_url_id ORDER BY page_nr DESC LIMIT 1");
            $query->execute([
                'search_url_id' => $searchUrlId
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            $peopleList = $query->fetch();
            return $peopleList;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $companyId
     * @param $visitById
     * @param $campaignId
     * @param string $orderBy
     * @return array|bool
     * @throws Exception
     */
    public static function getUrl2Visit($companyId, $visitById, $campaignId, $orderBy = 'id DESC')
    {
        try {
            $query = \DB::prep("SELECT * FROM search_url WHERE company_id = :company_id AND campaign_id = :campaign_id AND finished = 0 AND paused = 0 AND (visit_after = 0000-00-00 OR visit_after <= NOW() ) AND visit_by_id = :visit_by_id ORDER BY " . $orderBy);
            $query->execute([
                'company_id' => $companyId,
                'visit_by_id' => $visitById,
                'campaign_id' => $campaignId
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            $url = $query->fetch();
            return $url;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $companyId
     * @param $visitById
     * @return array|bool
     * @throws Exception
     */
    public static function getPublishedCampaign($companyId, $visitById)
    {

        try {
            $query = \DB::prep("
                                  SELECT 
                                    *
                                    FROM 
                                      campaign
                                    WHERE
                                      publish_status = 'published'
                                      AND company_id = :company_id
                                      AND status = 'Active'
                                      AND (start_date = 0000-00-00 OR start_date <= date(NOW()) )
                                      AND visit_by_id = :visit_by_id
                                      ".(self::checkInvitesLimit($companyId, $visitById) ? "" : "AND itc = 0")."
                                    ORDER BY
                                      status ASC,
                                      priority DESC,
                                      id DESC
                                ");
            $query->execute([
                'company_id' => $companyId,
                'visit_by_id' => $visitById
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            $campaign = $query->fetch();
            return $campaign;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $companyId
     * @param $linkedInId
     * @return array|bool
     * @throws Exception
     */
    public static function getLiUser(int $companyId, int $linkedInId)
    {
        try {
            $query = \DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND company_id = :company_id");
            $query->execute([
                'company_id' => (int)$companyId,
                'linked_in_id' => (int)$linkedInId
            ]);
            $liUsers = $query->fetch();
            return $liUsers;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int $companyId
     * @param int $linkedInId
     * @return bool false when itc enabled and limit reached, otherwise true
     */
    public static function checkInvitesLimit(int $companyId, int $linkedInId){
        $user = self::getLiUser($companyId, $linkedInId);
        $userSettingsModel = new \classes\Settings($companyId, $linkedInId);

        if($userSettingsModel->getForUser('itc-enabled') && $user['invitesLast24Hours'] >= $user['invites24HoursLimit']){
            return false;
        }
        return true;
    }

    /**
     * Called from APIModel::getNextUrl()
     * @param $companyId
     * @param $linkedInId
     * @return array|bool
     * @throws Exception
     */
    public static function getLiUserWithOneHourAgo($companyId, $linkedInId)
    {
        try {
            $query = \DB::prep("SELECT * , DATE_SUB(UTC_TIMESTAMP(), INTERVAL 1 HOUR) AS oneHourAgo , DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 MINUTE) AS halfHourAgo
                                      FROM li_users 
                                      WHERE linked_in_id = :linked_in_id 
                                        AND company_id = :company_id");
            $query->execute([
                'company_id' => $companyId,
                'linked_in_id' => $linkedInId
            ]);
            $liUsers = $query->fetch();
            return $liUsers;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $linkedInId
     * @return bool|int
     * @throws Exception
     */
    public static function getNumberOfVisitedProfiles($linkedInId)
    {
        try {
            $query = \DB::prep("SELECT visitsLast24Hours AS people_count FROM li_users WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'linked_in_id' => $linkedInId
            ]);
            $resultArray = $query->fetch();
            if ($resultArray === false OR !isset($resultArray['people_count'])) {
                throw new Exception('APIModel::getNumberOfVisitedProfiles() - query failed.');
            }
            $peopleCount = $resultArray['people_count'];
            return $peopleCount;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Check if we have a search url where we did not check for visits.
     * @param $campaignId
     * @return array|bool
     * @throws Exception
     */
    public static function getSearchUrlUnchecked4Visits($campaignId)
    {
        try {
            $query = \DB::prep("SELECT search_url.* FROM search_url LEFT JOIN people_list ON people_list.search_url_id = search_url.id 
                  WHERE people_list.id IS NULL AND campaign_id = :campaign_id AND paused = 0 AND finished = 0 AND total_matches < 0");
            //"WHERE people_list.id IS NULL" - it means that there is no people_list related to search_url - we need to create it
            $query->execute([
                'campaign_id' => $campaignId
            ]);
            $searchUrl = $query->fetch();
            return $searchUrl;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $data
     * @return string
     */
    public static function getPageType($data): string
    {
        if (empty($data->url)) {
            return self::FIRSTPAGE;
        }
        if (strpos($data->url, "/sales/profile/") || strpos($data->url, "/sales/people/")) {
            return self::PERSON_SN;
        } elseif (strpos($data->url, "/search/results/people/")) {
            return self::SEARCH_SIMPLE;
        } elseif (strpos($data->url, "/sales/search")) {
            return self::SEARCH_SN;
        } elseif (strpos($data->url, "/in/")) {
            return self::PERSON_SIMPLE;
        } elseif (strpos($data->url, "/psettings/profile-visibility")) {
            return self::PRIVACY_PAGE;
        } elseif (strpos($data->url, "nav_user_menu_manage_sales_nav")) {
            return self::PRIVACY_SALES_PAGE;
        } elseif (strpos($data->url, "/premium/manage")) {
            return self::SUBS_PAGE;
        } elseif (strpos($data->url, "/psettings/email")) {
            return self::EMAIL_PAGE;
        } elseif (strpos($data->url, "/psettings/phone")) {
            return self::PHONE_PAGE;
        } elseif (strpos($data->url, "/me/profile-views")) {
            return self::FREE_INBOUND_PAGE;
        } elseif (strpos($data->url, "/sales/wvmpUpdate")) {
            return self::SN_INBOUND_PAGE;
        } elseif (strpos($data->url, "/mynetwork/invitation-manager")) {
            return self::INVIT_PAGE;
        } elseif (strpos($data->url, "/groups/my-groups")) {
            return self::GROUPS_PAGE;
        } else {
            return self::FIRSTPAGE;
        }
    }

    /**
     * Get user account type within received page. There is check only for SALESNAV and FREE yet.
     * @param string $html
     * @param string $pageType type of page (personal or search result)
     * @return string type of user account (self::SALESNAV by default)
     * @throws Exception
     */
    public static function getAccountType(string $html, string $pageType): string
    {
        //We are support only FREE and SALES NAVIGATOR types of account for now!!!
        //Suggestion for recruiter: RECRUITER_LITE Recruiter Lite
        switch ($pageType) {
            case self::SEARCH_SN:
                return self::SALESNAV;
            case self::PERSON_SN:
                return self::SALESNAV;
            case self::SEARCH_SIMPLE:
                return self::FREE;
            case self::PERSON_SIMPLE:
                return self::FREE;
            case self::SN_INBOUND_PAGE:
                return self::SALESNAV;
            case self::FREE_INBOUND_PAGE:
                return self::FREE;
        }
        //If we have got first page
        if ($pageType === self::FIRSTPAGE AND !empty($html)) {
            //'SALES_NAVIGATOR' on the user page means that he has sales navigator
            return self::SALESNAV; //disable salesnav check, temp fix
            if (ParserHelper::isSalesNavButton($html)) {
                return self::SALESNAV;
            }
        }

        return self::FREE;
    }

    /**
     * Update actual linkedIn account type in our DB
     * @param array $liUser li_users object
     * @param string $accountType self::SALESNAV|self::FREE|self::RECRUITER
     * @return bool false
     * @throws Exception if $accountType has unacceptable value or query failed
     */
    public static function updateUserType(array $liUser, string $accountType)
    {
        try {
            if (!in_array($accountType, [self::SALESNAV, self::FREE, self::RECRUITER])) {
                throw new Exception('APIModel::updateUserType() - Unacceptable value');
            }

            if ($liUser['li_account_type'] === $accountType) {
                return false;
            }

            $query = \DB::prep('UPDATE  li_users SET li_account_type = :li_account_type, have_sales_navigator = :have_sales_navigator WHERE linked_in_id = :id');
            $query->execute([
                'id' => $liUser['id'],
                'li_account_type' => $accountType,
                'have_sales_navigator' => $accountType === APIModel::SALESNAV ? 1 : 0,
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param string $accountType
     * @param float $value
     * @return int
     * @throws Exception
     */
    public static function calculateDelay(string $accountType, float $value): int
    {
        if (!in_array($accountType, [self::SALESNAV, self::FREE, self::RECRUITER])) {
            throw new Exception('APIModel::calculateDelay() - Unacceptable value');
        }
        switch ($accountType) {
            case self::RECRUITER:
                return round($value * 1.5);
            case self::FREE:
                return round($value * 2);
            case self::SALESNAV:
                return round($value);
        }
        return false;
    }

    /**
     * @param $companyId
     * @param $visitById
     * @return bool
     * @throws Exception
     */
    public static function isActiveCampaign($companyId, $visitById)
    {
        if(self::getPublishedCampaign($companyId, $visitById)){
            return true;
        }
        return false;
    }

    /**
     * Called from APIModel::getNextUrl()
     * @param $companyId
     * @param $linkedInId
     * @param $sfwStatus
     * @return bool false
     * @throws Exception
     */
    public static function updateSfwStatus($companyId, $linkedInId, $sfwStatus)
    {
        try {
            $query = \DB::prep("UPDATE li_users
                                      SET software_status = :software_status,
                                          last_connection = UTC_TIMESTAMP()
                                      WHERE company_id = :company_id 
                                        AND linked_in_id = :linked_in_id");
            $query->execute([
                'company_id' => $companyId,
                'linked_in_id' => $linkedInId,
                'software_status' => $sfwStatus
            ]);
            global $CONFIG;
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->pushUserStatus($companyId, $linkedInId);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $companyId
     * @param $linkedInId
     * @return bool false
     * @throws Exception
     */
    public static function updateLastConnection($companyId, $linkedInId)
    {
        try {
            $query = \DB::prep("UPDATE li_users SET last_connection = UTC_TIMESTAMP() WHERE company_id = :company_id AND linked_in_id = :linked_in_id");
            $query->execute([
                'company_id' => $companyId,
                'linked_in_id' => $linkedInId
            ]);
            global $CONFIG;
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->pushUserStatus($companyId, $linkedInId);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $companyId
     * @param $linkedInId
     * @param $limitReachedOf
     * @return bool false
     * @throws Exception
     */
    public static function updateLimitReached($companyId, $linkedInId, $limitReachedOf)
    {
        try {
            $query = \DB::prep("UPDATE li_users SET limit_reached_at = UTC_TIMESTAMP(), limit_reached_of = :limit_reached_of WHERE company_id = :company_id AND linked_in_id = :linked_in_id");
            $query->execute([
                'company_id' => $companyId,
                'linked_in_id' => $linkedInId,
                'limit_reached_of' => $limitReachedOf
            ]);
            global $CONFIG;
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->pushUserStatus($companyId, $linkedInId);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $companyId
     * @param $linkedInId
     * @param $sfwStatus
     * @param $campaignId
     * @return bool false
     * @throws Exception
     */
    public static function updateLiUserCurrentCampaignAndSfwStatus($companyId, $linkedInId, $sfwStatus, $campaignId)
    {
        try {
            $query = \DB::prep("UPDATE li_users SET software_status = :software_status, last_connection = UTC_TIMESTAMP(), campaign_id = :campaign_id WHERE company_id = :company_id AND linked_in_id = :linked_in_id");
            $query->execute([
                'company_id' => $companyId,
                'linked_in_id' => $linkedInId,
                'software_status' => $sfwStatus,
                'campaign_id' => $campaignId
            ]);
            global $CONFIG;
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->pushUserStatus($companyId, $linkedInId);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $companyId
     * @return bool false
     * @throws Exception
     */
    public static function updateCompanyLastVisit($companyId)
    {
        try {
            $query = \DB::prep("UPDATE company SET last_visit = NOW() WHERE id = :company_id");
            $query->execute([
                'company_id' => $companyId
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $campaignId
     * @return bool false
     * @throws Exception
     */
    public static function updateCampaignVisit($campaignId)
    {
        try {
            $query = \DB::prep("UPDATE campaign SET campaign_visit_count = campaign_visit_count + 1, campaign_last_visit = NOW() WHERE id = :campaign_id");
            $query->execute([
                'campaign_id' => $campaignId
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $searchUrlId
     * @return bool false
     * @throws Exception
     */
    public static function updateSearchUrlVisit($searchUrlId)
    {
        try {
            $query = \DB::prep("UPDATE search_url SET search_url_visit_count = search_url_visit_count + 1, search_url_last_visit = NOW() WHERE id = :search_url_id");
            $query->execute([
                'search_url_id' => $searchUrlId
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param $peopleId
     * @return bool false
     * @throws Exception
     */
    public static function updatePeopleCrawler($peopleId)
    {
        try {
            $query = \DB::prep("UPDATE people SET sent_to_crawler = NOW(), sent_to_crawler_date = NOW(), sent_to_crawler_date_utc = UTC_TIMESTAMP() WHERE id =  :people_id");
            $query->execute([
                'people_id' => $peopleId
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    public static function setPeopleInvite($peopleId){
        try {
            $query = \DB::prep("
                                INSERT INTO
                                    people_flags
                                SET 
                                    people_id = :people_id,
                                    flag_id = 1,
                                    `value` = 1
                                ON DUPLICATE KEY UPDATE 
                                  `value` = 1
                               ");
            $query->execute([
                'people_id' => $peopleId
            ]);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $peopleListId
     * @return bool false
     */
    public static function updatePeopleListRetry($peopleListId)
    {
        $query = \DB::prep("UPDATE people_list SET retry = 0 WHERE id = :people_list_id");
        $query->execute([
            'people_list_id' => $peopleListId
        ]);
        return false;
    }

    /**
     * @param $companyId
     * @param $linkedinId
     * @param $versionId
     * @return bool
     * @throws Exception
     */
    public static function updateClientVersionId($companyId, $linkedinId, $versionId)
    {
        try {
            $query = DB::prep("UPDATE li_users SET client_version_id = :version_id WHERE linked_in_id = :linked_in_id AND company_id = :company_id");
            $query->execute([
                'version_id' => $versionId ?? null,
                'company_id' => $companyId,
                'linked_in_id' => $linkedinId,
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return \FALSE;
    }

    /**
     * Called from APIModel::getNextUrl() / autoSlicer()
     * @param $searchUrlId
     * @param $pageNr
     * @param $url
     * @return bool|string
     * @throws Exception
     */
    public static function insertPeopleList($searchUrlId, $pageNr, $url)
    {
        try {
            $query = \DB::prep("INSERT INTO people_list (search_url_id, page_nr, url) 
                                      VALUES (:search_url_id, :page_nr, :url)");
            $query->execute([
                'search_url_id' => $searchUrlId,
                'page_nr' => $pageNr,
                'url' => $url
            ]);
            if ($query->rowCount() != 1) {
                return false;
            }
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $campaignId
     * @param $companyId
     * @param $url
     * @param $visitBy
     * @param $visitById
     * @param $parentSearchUrlId
     * @return bool|string
     * @throws Exception
     */
    public static function insertSearchUrl($campaignId, $companyId, $url, $visitBy, $visitById, $parentSearchUrlId)
    {
        try {
            $query = \DB::prep("INSERT INTO search_url (campaign_id, company_id, url, visit_by, visit_by_id,
					created, parent_search_url_id) VALUES(:campaign_id, :company_id, :url, :visit_by, :visit_by_id,
					NOW(), :parent_search_url_id)");
            $query->execute([
                'campaign_id' => $campaignId,
                'company_id' => $companyId,
                'url' => $url,
                'visit_by' => $visitBy,
                'visit_by_id' => $visitById,
                'parent_search_url_id' => $parentSearchUrlId
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int $companyId
     * @param string $version
     * @return bool if DB was updated - true, else - false
     */
    public static function logClientVersion(int $companyId, string $version): bool
    {
        $versionId = self::getClientVersionIdByName($version);
        if ($versionId === 0) {
            $versionId = self::insertClientVersion($version);
        }

        if (self::getLastCompanyClientVersionId($companyId) !== $versionId) {
            self::insertClientVersionHistory($companyId, $versionId);
            return true;
        }
        return false;
    }

    /**
     * @param int $companyId
     * @param int $clientVersionId
     * @return int
     * @throws Exception
     */
    public static function insertClientVersionHistory(int $companyId, int $clientVersionId): int
    {
        $query = \DB::prep("INSERT INTO company_client_version_history (company_id, client_version_id, created_at) VALUES (:company_id, :client_version_id, now())");
        $query->execute([
            'company_id' => $companyId,
            'client_version_id' => $clientVersionId ? $clientVersionId : NULL
        ]);
        if ($query->rowCount() == 0) {
            throw new Exception('APIModel::insertClientVersionHistory() - insert failed.');
        }
        return \DB::get_instance()->lastInsertId();
    }

    /**
     * @param int $companyId
     * @return int 0 if nothing was find
     */
    public static function getLastCompanyClientVersionId(int $companyId): int
    {
        $query = \DB::prep("SELECT client_version_id FROM company_client_version_history WHERE company_id = :company_id ORDER BY created_at DESC LIMIT 1");
        $query->execute([
            'company_id' => $companyId
        ]);
        return (int)$query->fetchColumn();
    }

    /**
     * @param string $versionName
     * @return int 0 if nothing was find
     * @throws Exception
     */
    public static function getClientVersionIdByName(string $versionName): int
    {
        $query = \DB::prep("SELECT id FROM client_versions WHERE name = :name");
        $query->execute([
            'name' => $versionName
        ]);
        return (int)$query->fetchColumn();
    }

    /**
     * @param string $versionName
     * @return int
     * @throws Exception
     */
    public static function insertClientVersion(string $versionName): int
    {
        $query = \DB::prep("INSERT INTO client_versions (name) VALUE (:name)");
        $query->execute([
            'name' => $versionName
        ]);
        if ($query->rowCount() == 0) {
            throw new Exception('APIModel::insertClientVersion() - insertion failed.');
        }
        return \DB::get_instance()->lastInsertId();
    }

    /**
     * @param $json
     * @return string
     */
    public static function getLiUserTypeFromDB($json)
    {
        $liUserType = false;
        //Search by CrawledBy
        if (!empty($json->CrawledBy)) {
            $crawledBy = self::explodeCrawledBy($json->CrawledBy);
            if (!empty($crawledBy[0])) {
                //Search by CrawledById
                $crawledById = $crawledBy[0];
                $query = \DB::prep('SELECT li_account_type FROM li_users WHERE linked_in_id = :linked_in_id');
                $query->execute([
                    'linked_in_id' => $crawledById
                ]);
                $liUserType = $query->fetchColumn();
            } elseif (!empty($crawledBy[1])) {
                //Search by CrawledByName
                $crawledByName = $crawledBy[1];
                $query = \DB::prep('SELECT li_account_type FROM li_users WHERE name = :name');
                $query->execute([
                    'name' => $crawledByName
                ]);
                $liUserType = $query->fetchColumn();
            }
        }
        return $liUserType;
    }

    /**
     * Called from probe.php
     * @param $json
     * @param $version
     * @return array|bool|null
     * @throws Exception
     */
    public static function generatePayload4Probe($json, $version)
    {
        if (empty($json->client_hash)) {
            throw new Exception('No Client Hash');
        }

//        throw new Exception('api disabled');

        if (empty($json->linked_in_id)) {
            //Go to First page
            return [
                'url' => 'https://www.linkedin.com/',
                'referer' => ''
            ];
//            throw new Exception('No ID');
//            return APIModel::loggedOffReturn();
        }
        $crawledById = $json->linked_in_id;
        $crawledByName = $json->username ?? '';
        $payload = [];
        $company = self::getCompanyByProductKey($json->client_hash);
        $companyId = $company['id'];
        APIModel::updateLastConnection($companyId, $crawledById);
        APIModel::logClientVersion($companyId, $version);
        $versionId = APIModel::getClientVersionIdByName($version);
        APIModel::updateClientVersionId($companyId, $crawledById, $versionId);
        $liUser = FirstPageModel::getLiUsersByLinkedinId($companyId, $crawledById);
        if (empty($crawledByName) AND !empty($liUser['name'])) {
            $crawledByName = $liUser['name'];
        }
        //Temporary hard code to not change GetNextUrl methods
        if (empty($json->CrawledBy)) {
            $json->CrawledBy = $crawledById . '|' . $crawledByName;
        }
        $liUserType = $liUser['li_account_type'];//self::getLiUserTypeFromDB($json);
        try {
            $payload = false;
            if ($liUserType === self::FREE) {
                if ($company && $company['company_runs_query_slicer']) {
                    
                    $autoslicer = new PremiumAutoslicer([
                        'planner' => true,
                        'multipass' => true,
                        'splits_per_strategy' => null
                    ], $liUser);
                    $payload = $autoslicer->autoslice($json->client_hash, $crawledById, $crawledByName, $version);
                    
                }
                if ($payload === false) {
                    $payload = GetNextUrlModel::run($json->client_hash, !empty($json->CrawledBy) ? $json->CrawledBy : '', $json, $version, true, $liUserType);
                }

            } elseif ($liUserType === self::SALESNAV) {
                $GNU = api\models\GNU::getInstance($json);
                if ($company && $company['company_runs_query_slicer']) {

                    if(!empty($liUser['search_design_version'])){
                        require_once('models/autoslicer/Sales_v1.php');
                    }else{
                        require_once('models/autoslicer/Sales.php');
                    }

                    $autoslicer = new SalesAutoslicer([
                        'planner' => true,
                        'multipass' => true,
                        'splits_per_strategy' => null
                    ], $liUser);
                    $payload = $autoslicer->autoslice($json->client_hash, $crawledById, $crawledByName, $version);
                }
                if ($payload === false) {
                    $payload = $GNU->getNextUrl();
                }
            }
        } catch (Exception $e) {
            if ($e->getMessage() == "no_campaign") {
                successResponse(['WaitBetweenConnections' => 30, "status_message" => [
                    'status' => 2,
                    'message' => sprintf("Waiting for active campaign - Sign in to www.viewgentools.com\nto manage campaigns (%d visits last 24h)", isset($json->VisitedProfiles24) ? $json->VisitedProfiles24 : 0)
                ]]);
            }
            throw $e;
        }

        if (empty($payload['VisitedProfiles24'])) {
            $payload['VisitedProfiles24'] = (int)$liUser["visitsLast24Hours"];
        }

        if (!isset($payload['url'])) {
            $payload['status_message'] = [
                'status' => 2,
                'message' => sprintf("Waiting for active campaign - Sign in to www.viewgentools.com\nto manage campaigns (%d visits last 24h)", isset($json->VisitedProfiles24) ? $json->VisitedProfiles24 : 0)
            ];
            $payload['url'] = '';
            $payload['WaitBetweenConnections'] = 30;
        } elseif (!isset($payload['status_message'])) {
            $payload['status_message'] = [
                'status' => 3,
                'message' => 'Running - Waiting for instructions from server'
            ];
            if (isset($payload['campaign_id'])) {
                $campaign = self::getCampaignById($payload['campaign_id']);
                if ($campaign) {
                    $payload['campaign_name'] = $campaign['name'];
                    $payload['status_message'] = [
                        'status' => 3,
                        'message' => sprintf('Running campaign %s (%d visits last 24h)', $campaign['name'], isset($json->VisitedProfiles24) ? $json->VisitedProfiles24 : 0)
                    ];
                }
            }
        }
        return $payload;
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function urlNormalize($url)
    {
        $url = explode('?', $url);
        $url[1] = str_replace('\\', '', $url[1]);
        $url[1] = str_replace('[', '%5B', $url[1]);
        $url[1] = str_replace(']', '%5D', $url[1]);
        $url[1] = str_replace('"', '%22', $url[1]);
        $url[1] = str_replace(':', '%3A', $url[1]);
        $url = implode('?', $url);
        return $url;
    }

    /**
     * NOTE: $json->DOM always TRUE
     * @param string $json result of json_decode()
     * @param string $version
     * @return mixed payload
     * @throws Exception
     */
    public static function generatePayload4Save($json, string $version = '')
    {
        if (empty($json->client_hash)) {
            throw new Exception('No Client Hash');
        }
        if (!isset($json->html)) {
            throw new Exception('No HTML-parameter');
        }
        //client may return url with '\'
        if (!empty($json->url)) {
            if (strpos($json->url, "?")) {
                $json->url = self::urlNormalize($json->url);
            }
        }
        $payload = [];
        $company = self::getCompanyByProductKey($json->client_hash);
        $companyId = $company['id'];
        self::logClientVersion($companyId, $version);
        if (!empty($json->linked_in_id) and !empty($version)) {
            APIModel::updateLastConnection($companyId, $json->linked_in_id);
            $versionId = APIModel::getClientVersionIdByName($version);
            APIModel::updateClientVersionId($companyId, $json->linked_in_id, $versionId);
        }
        $pageType = self::getPageType($json);
        $accountType = self::getAccountType($json->html, $pageType);
        //if we haven't linked_in_id - go to the first page and get it!

        if (!empty($json->linked_in_id) && isset($json->url)) {
            $payload = (new Manager($companyId, $json->linked_in_id))->processResponse($json->url, $json->html, $version, $json->client_hash);
            if ($payload) {
                return $payload;
            }
        }

        if ($pageType === self::FIRSTPAGE) {
            $payload = FirstPageModel::firstPage($json->client_hash, $json, $version, $accountType);
        } elseif ($pageType === self::PRIVACY_PAGE) {
            $payload = PrivacyModel::checkPrivacy($company, $json, $version);
        } elseif ($pageType === self::PRIVACY_SALES_PAGE) {
            $payload = PrivacySalesModel::checkPrivacy($company, $json, $version);
        } elseif ($pageType === self::SUBS_PAGE) {
            $payload = SubsParser::checkSubs($company, $json, $version);
        } elseif ($pageType === self::EMAIL_PAGE) {
            $payload = EmailParser::checkEmail($company, $json, $version);
        } elseif ($pageType === self::PHONE_PAGE) {
            $payload = PhoneParser::checkPhone($company, $json, $version);
        } elseif ($pageType === self::INVIT_PAGE) {
            $parser = api\models\InvitParser::getInstance($json, $version);
            $payload = $parser->checkInvitations();
        } elseif ($pageType === self::GROUPS_PAGE) {
            $parser = api\models\GroupsParser::getInstance($json, $version);
            $payload = $parser->check();
        } elseif ($pageType === self::SN_INBOUND_PAGE and $accountType === APIModel::SALESNAV and isset($json->inbound_visits_url) and $json->inbound_visits_url > 0) {
            $payload = InboundSalesParser::checkInboundViews($json, $version);
        } elseif ($pageType === self::FREE_INBOUND_PAGE) {
            $payload = InboundFreeModel::checkInboundViews($json, $version);
        } elseif ($pageType === self::PERSON_SN and $accountType === self::SALESNAV) {
            $payload = ProfileSalesParser::parseProfilePage($json, $version);
        } elseif ($pageType === self::SEARCH_SN and $accountType === self::SALESNAV) {
            $payload = SearchSalesParser::parseSearchPage($json, $version);
        } elseif ($pageType === self::SEARCH_SIMPLE and $accountType === self::FREE) {
            //Parse for simple search page (available for FREE li-users)
            $parser = api\models\SearchFreeParser::getInstance($json, $version);
            $payload = $parser->searchPage();
        } elseif ($pageType === self::PERSON_SIMPLE and $accountType === self::FREE) {
            //Parse for simple search page (available for FREE li-users)
            $payload = FreeParserModel::profilePage($json->client_hash, $json);
        } else {
            $payload = FirstPageModel::firstPage($json->client_hash, $json, $version, $accountType);
        }
        $payload['linked_in_id'] = $payload['linked_in_id'] ?? $json->linked_in_id ?? 0;
        $payload['username'] = $payload['username'] ?? $json->username ?? '';
        $payload['VisitedProfiles24'] = $payload['VisitedProfiles24'] ?? $json->VisitedProfiles24 ?? 0;
        $payload['company_name'] = $company['company_name'];
        return $payload;
    }

    /**
     * @param $crawledBy
     * @return array
     */
    public static function explodeCrawledBy($crawledBy)
    {
        $p = explode("|", $crawledBy);
        $crawledById = (int)$p[0];
        $crawledByName = $p[1] ?? '';

        return [$crawledById, $crawledByName];
    }

    /**
     * Called from getNewUserMessage.php
     * @param $json
     * @return array|bool|null
     */
    public static function generatePayload4getNewUserMessage($json)
    {
        $version = self::getVersion($json);
        $company = self::getCompanyByProductKey($json->client_hash);
        self::logClientVersion($company['id'], $version);

        $payload = [
            "show_message" => true,
            "message" => ""
        ];

        if ($company) {
            $payload['message'] = $company['recurly_sync'] ? "per user pricing" : "contact support"; //@TODO change messages to actual
        } else {
            $payload['show_message'] = false;
        }

        return $payload;
    }

    /**
     * Called from getNewUserMessage.php
     * @param $json
     * @return array|bool|null
     */
    public static function generatePayload4getCompanyUsers($json)
    {
        $version = self::getVersion($json);
        $company = self::getCompanyByProductKey($json->client_hash);
        self::logClientVersion($company['id'], $version);

        $payload = [];
        $liUsers = self::getCompanyUsers($company['id']);

        foreach ($liUsers as $user) {
            $payload[] = [
                "linked_in_id" => intval($user['linked_in_id']),
                "linked_in_username" => $user['name']
            ];
        }

        return $payload;
    }
}
