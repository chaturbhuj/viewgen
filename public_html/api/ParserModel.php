<?php
require_once('htmlStorage.php');

class ParserModel extends APIModel
{
    public static function randomizeWait($wait)
    {
        return rand() % ($wait + 1) + ((int)$wait / 2);
    }

    /**
     * @param int $peopleListId
     * @return array|bool
     * @throws Exception
     */
    public static function getPeopleList(int $peopleListId)
    {
        $query = \DB::prep("SELECT
                        people_list.*,
                        total_matches 
                    FROM people_list 
                    JOIN search_url ON people_list.search_url_id = search_url.id
                    WHERE people_list.id = :people_list_id");//'" . mysql_real_escape_string($people_list_id) . "'";
        $query->execute([
            'people_list_id' => $peopleListId
        ]);
        $result = $query->fetch();
        return $result;
    }

    /**
     * @param int $companyId
     * @param string $name
     * @return bool|array
     * @throws Exception
     */
    public static function getLiUsersByCompanyIdAndName(int $companyId, string $name)
    {
        $query = \DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND name LIKE :name");
        $query->execute([
            'company_id' => $companyId,
            'name' => $name
        ]);
        if ($query->rowCount() == 0) {
            return false;
        }
        $result = $query->fetch();
        return $result;
    }

    /**
     * @param $companyId
     * @param $linkedinId
     * @param $dumpUrl
     * @param $htmlDump
     * @param $versionId
     * @param null $htmlDumpId
     * @param null $campaignId
     * @param null $searchUrlId
     * @param null $peopleListId
     * @param string $message
     * @param string $pageType
     * @return bool|string
     * @throws Exception
     */
    public static function insertApiBadPages($companyId, $linkedinId, $dumpUrl, $htmlDump, $versionId, $htmlDumpId = \NULL, $campaignId = \NULL, $searchUrlId = \NULL, $peopleListId = \NULL, $message = '', $pageType = '')
    {
        try {
            $query = \DB::prep("INSERT INTO api_bad_pages (company_id, linkedin_id, dump_url, version_id, html_dump_id, campaign_id, search_url_id, people_list_id, message, page_type) VALUES (:company_id, :linkedin_id, :dump_url, :version_id,  :html_dump_id, :campaign_id, :search_url_id, :people_list_id, :message, :page_type)");
            $query->execute([
                'company_id' => $companyId,
                'linkedin_id' => $linkedinId,
                'dump_url' => $dumpUrl,
                'version_id' => $versionId,
                'html_dump_id' => $htmlDumpId,
                'campaign_id' => $campaignId,
                'search_url_id' => $searchUrlId,
                'people_list_id' => $peopleListId,
                'message' => $message,
                'page_type' => $pageType,
            ]);
            if ($query->rowCount() == 0) {
                return \FALSE;
            }
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Notes: 1) we don't use client_version_id for htmlDump table 2) htmlDump.dump ($pageURL) takes page URL
     * @param $pageURL
     * @param $companyId
     * @param $crawledBy
     * @param $crawledById
     * @param $ip
     * @param $clientHash
     * @param null $htmlPeopleId
     * @param $versionId
     * @param $htmlDump
     * @param string $scope
     * @return bool
     */
    public static function saveHtmlDump($pageURL, $companyId, $crawledBy, $crawledById, $ip, $clientHash, $htmlPeopleId = NULL, $versionId, $htmlDump, $scope = 'htmlDump')
    {
        $htmlDumpId = ParserModel::insertHtmlDump($companyId, $crawledBy, $crawledById, $clientHash, $ip, $pageURL, $htmlPeopleId);
        if (!empty($htmlDumpId && $versionId)) {
            ParserModel::insertHtmlDumpClient($htmlDumpId, $versionId);
        }
        if (!empty($htmlDumpId)) {
            HtmlStorage::store($htmlDump, $scope, $crawledById, $htmlDumpId);
        }
        return !empty($htmlDumpId) ? $htmlDumpId : false;
    }

    /**
     * @param int $companyId
     * @param string $crawledBy
     * @param string $crawledById crawled_by_id    varchar(250)
     * @param string $clientHash
     * @param string $ip
     * @param string $dump - here we save only page URL
     * @param $htmlPeopleId int | NULL by default in DB
     * @return int
     * @throws Exception
     */
    public static function insertHtmlDump(int $companyId, string $crawledBy, string $crawledById, string $clientHash, string $ip, string $dump, $htmlPeopleId = NULL)
    {
        $query = \DB::prep("INSERT INTO 
                                        htmlDump (companyId,crawled_by,crawled_by_id,client_hash,ip,dump,html_people_id) 
                                        VALUES (
                                            :companyId,
                                            :crawled_by,
                                            :crawled_by_id,
                                            :client_hash,
                                            :ip,
                                            :dump,
                                            :html_people_id)
			");
        $query->execute([
            'companyId' => $companyId,
            'crawled_by' => $crawledBy,
            'crawled_by_id' => $crawledById,
            'client_hash' => $clientHash,
            'ip' => $ip,
            'dump' => $dump,
            'html_people_id' => $htmlPeopleId
        ]);
        if ($query->rowCount() == 0) {
            return false;
        }
        return (int)DB::get_instance()->lastInsertId();
    }

    /**
     * @param int $htmlDumpId
     * @param int $clientVersionId
     * @return int
     * @throws Exception
     */
    public static function insertHtmlDumpClient(int $htmlDumpId, int $clientVersionId): int
    {
        $query = \DB::prep("INSERT INTO htmlDump_to_client_version (html_dump_id, client_version_id) 
                                          VALUES (:htmlDumpId, :clientVersionId)");
        $query->execute([
            "htmlDumpId" => $htmlDumpId,
            "clientVersionId" => $clientVersionId
        ]);
        if ($query->rowCount() == 0) {
            return false;
        }
        return (int)DB::get_instance()->lastInsertId();
    }

    /**
     * @param int $companyId
     * @param int $campaignId
     * @param int $peopleListId
     * @param string $linkedinId
     * @param int $searchUrlId
     * @param int $memberId
     * @param int $premium 0 by default
     * @return int
     * @throws Exception
     */
    public static function insertPeople(int $companyId, int $campaignId, int $peopleListId, string $linkedinId, int $searchUrlId, int $memberId, int $premium = 0): int
    {
        try {
            $query = \DB::prep("INSERT INTO people (company_id,campaign_id,people_list_id,linked_in_id,search_url_id,member_id, premium) VALUES (:company_id, :campaign_id, :people_list_id, :linked_in_id, :search_url_id, :member_id, :premium)");
            $query->execute([
                "company_id" => $companyId,
                "campaign_id" => $campaignId,
                "people_list_id" => $peopleListId,
                "linked_in_id" => $linkedinId,
                "search_url_id" => $searchUrlId,
                "member_id" => $memberId,
                "premium" => $premium,

            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            return (int)DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int $companyId
     * @param string $fullName
     * @return bool false
     * @throws Exception
     */
    public static function updateLiUsersOnboarding(int $companyId, string $fullName): bool
    {
        $query = \DB::prep("UPDATE li_users SET is_onboarding = 1 WHERE company_id = :company_id AND name LIKE :name");
        $query->execute([
            'company_id' => $companyId,
            'name' => $fullName
        ]);
        return false;
    }

    /**
     * @param int $peopleListId
     * @return bool
     */
    public static function updatePeopleListRetry1(int $peopleListId): bool
    {
        $query = \DB::prep("UPDATE people_list SET retry = 1, retry_count = retry_count + 1 WHERE id = :people_list_id");
        $query->execute([
            'people_list_id' => $peopleListId
        ]);
        return false;
    }

    /**
     * @param string $name
     * @param string $title
     * @param string $crawledBy
     * @param int $crawledById
     * @param int $peopleId
     * @return bool
     * @throws Exception
     */
    public static function updatePeopleError(string $name, string $title, string $crawledBy, int $crawledById, int $peopleId): bool
    {
        $query = \DB::prep("UPDATE people 
                                    SET 
                                      name = :name, 
                                      title = :title, 
                                      crawled_by = :crawled_by, 
                                      crawled_by_id = :crawled_by_id 
                                      WHERE id = :id");
        $query->execute([
            'name' => $name,
            'title' => $title,
            'crawled_by' => $crawledBy,
            'crawled_by_id' => $crawledById,
            'id' => $peopleId
        ]);
        return false;
    }

    /**
     * @param string $crawledBy
     * @param int $crawledById
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public static function updatePeopleList(string $crawledBy, int $crawledById, int $id): bool
    {
        try {
            $query = \DB::prep("UPDATE people_list SET crawled_by = :crawled_by, crawled_by_id = :crawled_by_id, people_list_last_visit = NOW() WHERE id = :id");
            $query->execute([
                'crawled_by' => $crawledBy,
                'crawled_by_id' => $crawledById,
                'id' => $id
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param string $crawledBy
     * @param int $crawledById
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public static function updatePeopleListCompleted(string $crawledBy, int $crawledById, int $id): bool
    {
        $query = \DB::prep("UPDATE people_list SET crawled_by = :crawled_by, crawled_by_id = :crawled_by_id, people_list_completed = NOW() WHERE id = :id");
        $query->execute([
            'crawled_by' => $crawledBy,
            'crawled_by_id' => $crawledById,
            'id' => $id
        ]);
        return false;
    }

    /**
     * @param int $linkedinId
     * @return bool
     * @throws Exception
     */
    public static function updateLiUsersSalesNav(int $linkedinId): bool
    {
        $query = \DB::prep("UPDATE li_users SET have_sales_navigator = 1, is_onboarding = 0 WHERE linked_in_id = :linked_in_id");
        $query->execute([
            'linked_in_id' => $linkedinId
        ]);
        return false;
    }

    /**
     * @param int $totalMatches
     * @param int $searchUrlId
     * @return bool
     * @throws Exception
     */
    public static function updateSearchUrlTotalMatches(int $totalMatches, int $searchUrlId): bool
    {
        try {
            //total_matches < 2 AND is for the case when first we found matches but then LinkedIn start saying that there are none
            $query = \DB::prep("UPDATE search_url SET total_matches = :total_matches WHERE id = :id");
            $query->execute([
                'total_matches' => $totalMatches,
                'id' => $searchUrlId
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return false;
    }

    /**
     * @param int $searchUrlId
     * @param int $count
     * @return bool
     */
    public static function updateSearchUrlDuplicate(int $searchUrlId, int $count = 1): bool
    {
        $query = \DB::prep("UPDATE search_url SET duplicateSkipCount = duplicateSkipCount + :count WHERE id = :id");
        $query->execute([
            'id' => $searchUrlId,
            'count' => $count
        ]);
        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function updatePeople(array $data): bool
    {
        $query = \DB::prep("UPDATE people 
                                    SET
                                        name = ?,
                                        title = ?,
                                        location = ?,
                                        industry = ?,
                                        employer = ?,
                                        employer_url = ?,
                                        crawled_by = ?,
                                        crawled_by_id = ?,
                                        email = ?,
                                        is_connected = ?
                                    WHERE id = ?");
        $query->execute($data);
        return false;
    }


    /**
     * @param int $companyId
     * @param int $campaignId
     * @param string $uri
     * @return bool
     */
    public static function isPeopleByCompanyCampaignLinkedin(int $companyId, int $campaignId, string $uri): bool
    {
        $query = \DB::prep("SELECT id FROM people WHERE company_id = :company_id AND campaign_id = :campaign_id AND linked_in_id = :linked_in_id LIMIT 1");
        $query->execute([
            'company_id' => $companyId,
            'campaign_id' => $campaignId,
            'linked_in_id' => $uri
        ]);
        $result = $query->fetchColumn();
        if ($result !== false) {
            return true;
        }
        return false;
    }
}