<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 10.06.2017
 * Time: 16:07
 * class ParserHelper
 * Help to handle html dump of page
 */
class ParserHelper
{

    /**
     * Search all data in <code> (as array) where key found
     * @param $html
     * @param $key
     * @return array|mixed|null|object
     */
    public static function findCodeOnPageByKey($html, $key)
    {
        $result = NULL;
        $dom = new DOMDocument();
        if (!@$dom->loadHTML($html)) return NULL;
        $codes = $dom->getElementsByTagName('code');
        foreach ($codes as $code) {
            $data = json_decode($code->nodeValue, true);
            if (!is_array($data)) continue;
            if (array_key_exists($key, $data)) {
                $result = $data;
                return $result;
            } else {
                foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        $result = self::getArrayValueByKey($v, $key);
                        if ($result !== NULL) return $data;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Search all data in <code> (as array) where key->value found
     * @param $html
     * @param $key
     * @param $value
     * @return array|mixed|null|object
     */
    public static function findCodeOnPageByKeyValue($html, $key, $value)
    {
        $result = NULL;
        $dom = new DOMDocument();
        if (!@$dom->loadHTML($html)) return NULL;
        $codes = $dom->getElementsByTagName('code');
        foreach ($codes as $code) {
            $data = json_decode($code->nodeValue, true);
            if (!is_array($data)) continue;
            if (array_key_exists($key, $data) && $data[$key] === $value) {
                $result = $data;
                return $result;
            } else {
                foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        $result = self::getArrayValueByKey($v, $key);
                        if ($result === $value) return $data;
                    }
                }
            }
        }
        return $result;
    }


    /**
     * @param $html
     * @param $value
     * @return bool|null
     */
    public static function findJsonOnPageByValue($html, $value)
    {
        $dom = new DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            return \NULL;
        }
        $codes = $dom->getElementsByTagName('code');
        foreach ($codes as $code) {
            $data = json_decode($code->nodeValue, true);
            if (!is_array($data)) continue;
            $result = self::getArrayByValue($data, $value);
            if ($result !== \FALSE) return $result;
        }
        return FALSE;
    }

    /**
     * @param $array
     * @param $value
     * @return bool
     */
    public static function getArrayByValue($array, $value)
    {
        if (!is_array($array)) return \FALSE;
        foreach ($array as $v) {
            if ($v === $value) {
                return $array;
            } elseif (is_array($v)) {
                $result = self::getArrayByValue($v, $value);
                if ($result !== \FALSE) return $result;
            }
        }
        return \FALSE;
    }

    /**
     * Search of json code on the page (in <code> tags) and then return value by specified key.
     * Related to ParserHelper::getArrayValueByKey
     * @param $html
     * @param $key
     * @return mixed NULL if nothing found
     */
    public static function findJsonValueOnPageByKey($html, $key)
    {
        $result = NULL;
        $dom = new DOMDocument();
        if (!@$dom->loadHTML($html)) return NULL;
        $codes = $dom->getElementsByTagName('code');
        foreach ($codes as $code) {
            $data = json_decode($code->nodeValue, true);
            if (!is_array($data)) continue;
            if (array_key_exists($key, $data)) {
                $result = $data[$key];
                return $result;
            } else {
                foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        $result = self::getArrayValueByKey($v, $key);
                        if ($result !== NULL) return $result;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Search of json code on the page (in <code> tags) and then check if value exists in there
     * Related to ParserHelper::isValueInArray
     * @param $html
     * @param $value
     * @return bool
     */
    public static function isJsonValueOnPage($html, $value)
    {
        $dom = new DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            return NULL;
        }
        $codes = $dom->getElementsByTagName('code');
        foreach ($codes as $code) {
            $data = json_decode($code->nodeValue, true);
            if (!is_array($data)) continue;
            $result = self::isValueInArray($data, $value);
            if ($result === TRUE) return $result;
        }
        return FALSE;
    }

    public static function isSalesNavButton($html){
        if (ParserHelper::isJsonValueOnPage($html, 'SALES_NAVIGATOR')) {
            return true;
        }

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            return false;
        }
        $finder = new \DomXPath($dom);
        $node = $finder->query("//a[@data-control-name='nav.sales_navigator']")->item(0);

        return $node !== \NULL ? true : false;
    }

    /**
     * Recursive search of the value in the multidimensional array by key.
     * Related to ParserHelper::findJsonValueOnPageByKey
     * @param $array
     * @param $key
     * @return mixed NULL if nothing found
     */
    public static function getArrayValueByKey($array, $key)
    {
        $find = NULL;
        if (!is_array($array)) return NULL;
        if (array_key_exists($key, $array)) {
            $find = $array[$key];
        } else {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $find = self::getArrayValueByKey($v, $key);
                    if ($find) return $find;
                }
            }
        }
        return $find;
    }

    /**
     * Recursive search array in received multidimensional parental array by key and value.
     * @param $array
     * @param $key
     * @param $value
     * @return array|mixed|null
     */
    public static function getArrayByKeyAndValue($array, $key, $value)
    {
        $find = NULL;
        if (!is_array($array)) return NULL;
        if (isset($array[$key]) and $array[$key] === $value) {
            $find = $array;
        } else {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $find = self::getArrayByKeyAndValue($v, $key, $value);
                    if ($find) return $find;
                }
            }
        }
        return $find;
    }

    /**
     * Recursive search of the value in the multidimensional array.
     * Related to ParserHelper::isJsonValueOnPage
     * @param $array
     * @param $value
     * @return bool
     */
    public static function isValueInArray($array, $value)
    {
        if (!is_array($array)) return FALSE;
        //in_array() and array_search() are not worked!!!
        foreach ($array as $k => $v) {
            if ($v === $value) {
                return TRUE;
            } elseif (is_array($v)) {
                $result = self::isValueInArray($v, $value);
                if ($result === TRUE) return $result;
            }
        }
        return FALSE;
    }

}