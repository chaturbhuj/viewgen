<?
require __DIR__ . '/../config.php';
global $CONFIG;

require __DIR__ . '/../services/version_svc.php';
use function \services\version\{get_client_binary_version, get_version_number_as_integer};

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;

function errorResponse($message){
    $output = [];
    $output["update"] = false;
    $output["error"] = $message;
    die(json_encode($output));
}

$contents = file_get_contents("php://input");
$jsonParameters = [];
if ($contents != "") {
    $jsonParameters = json_decode($contents,true);

    if ($jsonParameters !== null and $jsonParameters != "") {
        $_GET = $jsonParameters;
    }
}

try{
    if (!isset($_GET["current_version"])) {
        errorResponse("current_version is not set");
    }
    $system = "Unknown";
    $currentVersion = 0;
    if (preg_match('#([a-z]+?) ([0-9.]+)#is', $_GET['current_version'], $match)) {
        $system = $match[1];
        $currentVersion = $match[2];
    } else {
        errorResponse("malformed parameter current_version");
    }
    
    $currentVersionNumber = get_version_number_as_integer($currentVersion);
    
    $output = [];
    
    if ($system == "Windows") {
        //LEGACY_CODE $latestWindowsInstaller = get_latest_windows_installer_file();
        //LEGACY_CODE $latestWindowsVersion = get_product_version("../client/windows_installers/" . $latestWindowsInstaller);
        
        $latestWindowsVersion = get_client_binary_version($CONFIG['path']['windows_client_binary_url']);
        $latestWindowsVersion = get_version_number_as_integer($latestWindowsVersion);
    
        if ($currentVersionNumber < $latestWindowsVersion) {
            $output["update"] = true;
            $output["download_url"] = "http://viewgentools.com/client/download.php?type=windows";
        } else {
            $output["update"] = false;
        }
    
    } else {
        $output["update"] = false;
    }
    die(json_encode($output));
    
} catch (Exception $e) {
    errorResponse(log_api_error('should_update', $jsonParameters, $e));
}
?>
