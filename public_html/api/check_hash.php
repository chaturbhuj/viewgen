<?php

$GLOBALS['NOSESSION'] = true;

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;

require_once(__DIR__ . '/services/response_svc.php');
use function api\services\response\{successResponse, errorResponse};

$json = NULL;

try{
    include("functions.php");
    
    require_once(__DIR__ . '/../db.php');
    
    $json = json_decode(file_get_contents("php://input"));
    
    if ($json === null) {
        response("error");
    }
    
    $query = DB::prep("SELECT company.* FROM company JOIN software_license ON (software_license.company_id = company.id)
        WHERE product_key = ?");
    $query->execute(array($json->client_hash));
    
    if ($query->rowCount() == 0) {
        response("error");
    }
    
    $company = $query->fetchObject();
    
    response("success", array(
        'company_name' => $company->company_name
    ));
} catch (Exception $e) {
    errorResponse(log_api_error('check_hash', $json, $e));
}

?>
