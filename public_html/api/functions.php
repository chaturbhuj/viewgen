<?php
require_once (__DIR__.'/../lib/recurly.php');

function response($status, $payload = array()) {
    header('Content-Type: application/json');
    echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	exit(0);
}

function _getClientVersionIdByName($name){
	$query = DB::prep("SELECT id FROM client_versions WHERE name = :name");
	$query->execute([
		'name' => $name
	]);
	return intval($query->fetchColumn());
}


/**
 * calculate subscription price regarding of users count
 * result in cents
 *
 * @param int $usersCount
 *
 * @return mixed int when success or null in case users count < 1
 */
function calcSubscriptionPrice($usersCount){
    $result = null;
    if($usersCount > 0){
        if($usersCount > 10) {
            $usersCount = 10;
        }
        $result = (200 - ($usersCount - 1) * 10) * 100;
    }

    return $result;
}


/**
 * update subscription price and count
 *
 * @param Recurly_Subscription $subscription
 * @param int $count (number of users)
 * @param int $price (in cents)
 *
 * @return bool true if success or false if not
 */
function updateRecurlySubscription($subscription, $count, $price){
   try{
       $subscription->quantity = $count;
       $subscription->unit_amount_in_cents = $price;

       $subscription->updateImmediately();

   } catch (Recurly_Error $e){
       error_log("Recurly_Error: ".$e->getMessage());
       return false;
   }
    return true;
}

/**
 * update company subscription on users count change
 *
 * @param int $companyId
 *
 * @return bool true if success or false if not
 */
function usersCountUpdate($companyId){
    $company = getCompanyById($companyId);
    if(!$company){
        return false;
    }
    $usersCount = countCompanyUsers($companyId);
    $subscription = getSubscription($company['recurlyAccountCode']);

    if(!$subscription){
        return false;
    }

    if($company['recurly_sync']){
        return updateRecurlySubscription($subscription, $usersCount, calcSubscriptionPrice($usersCount));
    } else {
        return true;
    }
}

/**
 * count company users
 *
 * @param int $companyId
 *
 * @return int number of users
 */
function countCompanyUsers($companyId){
    $query = DB::prep("SELECT count(*) FROM li_users WHERE company_id = :company_id AND hidden = 0");
    $query->execute([
        'company_id' => $companyId
    ]);
    return intval($query->fetchColumn());
}

/**
 * get company by id
 *
 * @param int $id
 *
 * @return array company
 */
function getCompanyById(int $id){
    $query = DB::prep("SELECT * FROM company WHERE id = :company_id");
    $query->execute([
        'company_id' => $id
    ]);
    return $query->fetch();
}

/**
 * get Recurly subscription
 *
 * @param string $accountCode
 *
 * @return Recurly_Subscription object
 *
 * @throws Exception when company has multiple active subscriptions
 */
function getSubscription($accountCode){
    try {
        $subscriptions = Recurly_SubscriptionList::getForAccount($accountCode);
    } catch (Recurly_Error $e){
        error_log("Recurly_Error: ".$e->getMessage());
        return false;
    }

    $activeSubscriptions = array();
    $lastActivatedSubscription = null;
    foreach ($subscriptions as $tmpSubscription) {
        if ($tmpSubscription->state == "active") {
            $activeSubscriptions[] = $tmpSubscription;
        }

        if ($lastActivatedSubscription == null) {
            $lastActivatedSubscription = $tmpSubscription;
        } else if ($lastActivatedSubscription->activated_at < $tmpSubscription->activated_at) {
            $lastActivatedSubscription = $tmpSubscription;
        }
    }

    $subscription = null;
    if (count($activeSubscriptions) == 1) {
        $subscription = $activeSubscriptions[0];
    } else if (count($activeSubscriptions) > 1) {
        throw new Exception("This account has multiple active subscriptions, account code is: ".$accountCode);
    } else {
        $subscription = $lastActivatedSubscription;
    }

    return $subscription;
}

?>
