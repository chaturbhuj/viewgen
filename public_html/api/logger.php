<?php

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;

require_once(__DIR__ . '/services/response_svc.php');
use function api\services\response\{successResponse, errorResponse};


$GLOBALS['NOSESSION'] = true;

$data = file_get_contents("php://input");
$post = json_decode($data, true);

if ($post === null) {
    errorResponse(['message' => 'Wrong post JSON body']);
}

if (!isset($post['client_hash'])) {
    errorResponse(['message' => 'Client Hash absent!']);
}


try {
    require_once(__DIR__ . '/../db.php');
    
    $query = DB::prep("SELECT id FROM software_license JOIN company " .
        "ON (company.id = software_license.company_id) AND product_key = :product_key");
    $query->execute([
        'product_key' => $post['client_hash']
    ]);
    $company = $query->fetch();

    $check = function ($what) use ($post) {
        if (isset($post[$what])) return $post[$what];
        errorResponse(['message' => 'Empty parameter: ' . (string)$what]);
        return '';
    };

    if ($company) {

        $crawled_by = isset($post['crawled_by']) ? explode('|', $post['crawled_by']) : [null, null];

        $insertException = function ($json, $parent_id = null) use (&$insertException, $company, $check, $crawled_by) {

            $query = DB::prep("INSERT INTO client_log
                 (company_id, log_type, message, stack_trace, parent_exception_id, linked_in_id, linked_in_name, client_version) " .
                "VALUES(:company_id, :log_type, :message, :stack_trace, :parent_exception_id, :linked_in_id, :linked_in_name, :client_version)");
            $query->execute([
                'company_id' => $company['id'],
                'log_type' => $check('log_type'),
                'message' => $check('message'),
                'stack_trace' => $json['stack_trace'] ?? null,
                'parent_exception_id' => $parent_id,
                'linked_in_id' => $crawled_by[0],
                'linked_in_name' => $crawled_by[1] ?? '',
                'client_version' => $json['client_version'] ?? null
            ]);

            $newClientLogId = DB::get_instance()->lastInsertId();

            if (isset($json['inner_exception']) && $json['inner_exception']) {
                return $insertException($json['inner_exception'], $newClientLogId);
            } else {
                return $newClientLogId;
            }
        };


        successResponse(['id' => $insertException($post)]);

    } else {
        errorResponse(['message' => 'Wrong client hash!']);
    }

} catch (Exception $e) {
    errorResponse(log_api_error('logger', $post, $e));
}

?>
