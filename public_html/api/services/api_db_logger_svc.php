<?php

namespace api\services\logger;

//this should be the very first line
require_once __DIR__ . '/../../config/force_exceptions.php';

require_once(__DIR__ . '/../../db.php');

use \Exception;
use \DB;

function _divider($counter){
    return '\n\n========================= Previous Exception (#'. (string)$counter . ') ==============================\n';
} 

function _tracer(Exception $exception, int $counter=0){
    $message = $exception->getMessage();
    $message .= '\n\ncode:' . (string)$exception->getCode();
    
    $stack_trace = $exception->getTraceAsString();
    
    $postfix = '';
    
    try {
        $next = $exception->getPrevious();
        if ($next != NULL) {
            $counter++;
            $div = _divider($counter);
            $next_results = _tracer($next, $counter);
            return [
                'message' => $message . $div . $next_results['message'],
                'stack_trace' => $stack_trace . $div . $next_results['stack_trace'],
            ];
        }
    }catch (Exception $e){
        $postfix = ' \n\n=======\n\n EXCEPTION IN THE TRACE PARSER: ' . $e->getMessage();
    }

    return ['message' => $message, 'stack_trace' => ($stack_trace . $postfix)];
    
}

function _fetch_company($json){
    $company = null;
    $error = false;
    
    if (isset($json['company_id'])) return ['company_id' => $json['company_id']];

    if (!isset($json['client_hash'])) {
        return [
            'company_id' => NULL,
            'logger_error_type' => "data:bad_client_hash",
            'logger_message' => 'Empty client hash'
        ];
    }

    try {

        $query = DB::prep("SELECT id FROM software_license JOIN company " .
            "ON (company.id = software_license.company_id) AND product_key = :product_key");
        $query->execute([
            'product_key' => $json['client_hash']
        ]);

        $company = $query->fetch();

        if ($company) {
            return ['company_id' => $company['id']];
    
        }
        
        return [   
            'company_id' => NULL,
            'logger_error_type' => "db:company_fetch",
            'logger_message' => 'No company, wrong client hash? client_hash = ' . $json['client_hash']
        ];
        
    } catch (Exception $e) {
        $error = $e;
    }
    
    return [ 
        'company_id' => NULL,
        'logger_error_type' => "db:company_fetch",
        'logger_message' => 'Exception during Company fetch\n' .
            '===================== logger stack trace ====================\n' . 
            json_encode($error ? $error->getTraceAsString() : 'FAILED TO RECEIVE ERROR')
    ];
}

function log_api_error($endpoint, $json, $exception){
    return _log_error($endpoint, $json, $exception, 'E');
}

function log_error_message($endpoint, $json, $message){
    return _log_error($endpoint, $json, $message, 'M');
}

function _log_error($endpoint, $json, $exceptionOrMessage, $method){
    if ($method === 'E') {
        $exceptionOrMessage = $exceptionOrMessage ?? (new Exception('UNKNOWN_ERROR_MARKER'));
    }
    
    $data = [
        //unique error id, that is available even in case of the error
        // inside logger and inability to write into the DB
        'uid' => sha1(uniqid() . '|' . (string)$exceptionOrMessage)
    ];
    $data['company_id'] = NULL;
    $data['endpoint'] = $endpoint;
    $data['error_type'] = "api:exception";
    $data['logger_error_type'] = NULL;
    $data['logger_message'] = '* LOGGER IS OK *';
    $data['client_version'] = NULL;
    $data['client_json'] = NULL;
    $data['linked_in_id'] = NULL;
    $data['linked_in_name'] = NULL;
    $data['message'] = $method === 'M' ? $exceptionOrMessage : '';
    $data['stack_trace'] = NULL;
    $data['short_message'] = "";
    $data['logger_short_message'] = NULL;
    
    $error = false;
    
    try {
        if ($method === 'E'){
            $data = array_merge($data, _tracer($exceptionOrMessage));
        } 

        $data['short_message'] = substr($data['message'], 0, 254);

        if ($json) {
            if (is_object($json)){
                //we handle array representation, not object
                $json = json_decode(json_encode($json), true);
            }
            
            $data['client_version'] = $json['client_version'] ?? $json['version'] ?? null;
            $data['client_json'] = json_encode($json);

            $data = array_merge($data, _fetch_company($json));

            $crawled_by_string = $json['crawled_by'] ?? 
                $json['latest_li_user_id_inbound_crawled_by'] ?? 
                false;
            
            if ($crawled_by_string) {
                $crawled_by = explode('|', $crawled_by_string);
                $data['linked_in_id'] = $crawled_by[0] ?? '';
                $data['linked_in_name'] = $crawled_by[1] ?? '';
            }else{
                $data['linked_in_id'] = $json['linked_in_id'] ?? NULL;
            }
        } else {
            $data['logger_error_type'] = "request:bad_json";
            $data['logger_message'] = 'Wrong JSON';
        }
        

        $data['logger_short_message'] =
            $data['logger_message'] ? substr($data['logger_message'], 0, 254) : NULL;
        
    } catch (Exception $e) {
        $error = $e;
    }

    if ($error){
        @error_log("api_db_logger_failed_on_processing_error !!! UID= " . $data['uid'] . "\n\nLogger_Exception= " . (string)$error . "\n\nAPI_ExceptionOrMessage=" . (string)$exceptionOrMessage . "\n\nJSON= " . (string)$json);
        $error = false;
    }    

    try{    
        $query = DB::prep("INSERT INTO api_log
             (company_id, uid, endpoint, error_type, logger_error_type, short_message, logger_short_message, message, logger_message, stack_trace, linked_in_id, linked_in_name, client_version, client_json) " .
            "VALUES(:company_id, :uid, :endpoint, :error_type, :logger_error_type, :short_message, :logger_short_message, :message, :logger_message, :stack_trace, :linked_in_id, :linked_in_name, :client_version, :client_json)");
        $query->execute($data);
    
    } catch (Exception $e) {
        $error = $e;
    }

    if ($error) {
        @error_log("api_db_logger_failed_on_db_write !!! UID= " . $data['uid'] . "\n\nLogger_Exception=" . (string)$error . "\n\nAPI_Exception=" . (string)$exceptionOrMessage . "\n\nJSON= " . (string)$json);
    }

    return[
        'logged_error_id' => $data['uid'],
        'error_summary' => $data['short_message'],
        'status_message' => 'Logged API Error, endpoint=' . $endpoint .', UID= ' . $data['uid'],
        'error_details' => $data
    ];
}

?>
