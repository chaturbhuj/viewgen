<?php

namespace api\services\response;

function response(string $status, array $payload = []): void
{
    header('Content-Type: application/json');
    echo json_encode([
        'status' => $status,
        'payload' => $payload
    ]);
    exit(0);
}

function successResponse(array $payload = []): void
{
    response('success', $payload);
}

function errorResponse(array $payload = []): void
{
    response('error', $payload);
}


?>