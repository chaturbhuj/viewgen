<?php

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;

$GLOBALS['NOSESSION'] = true;

$json = NULL;

require_once(__DIR__ . '/services/response_svc.php');
use function api\services\response\{successResponse, errorResponse};

try {
    require_once("APIModel.php");

    $json = json_decode(file_get_contents("php://input"));
    if ($json === null) {
        APIModel::response('error', ['status_message' => 'No data were received.']);
    }

    $version = APIModel::getVersion($json);

    $payload = APIModel::generatePayload4Save($json, $version);

    successResponse($payload);

} catch (Exception $e) {
    errorResponse(log_api_error('save', $json, $e));
}