<?php
require_once('ParserModel.php');
require_once('functions.php');

require_once(__DIR__ . '/../intercom/helper.php');


class FreeParserModel extends ParserModel
{
    const NEWUI = 'New UI';
    const PROFILES_PER_PAGE = 10;

    /**
     * @param $clientHash
     * @param $payload
     * @param string $version
     * @return array|string
     * @throws Exception
     */
    public static function profilePage($clientHash, $payload, $version = '')
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        //---Initialize basic values
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $versionId = parent::getClientVersionIdByName($version);
        if (!empty($payload->people_id)) {
            $peopleId = $payload->people_id;
        } else {
            throw new Exception("Server did not get profile ID for parse.");
        }
        $companyData = parent::getCompanyByProductKey($clientHash);
        $waitBetweenConnections = $companyData['wait_between_connections'];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($payload->html)) throw new Exception("Invalid html");
        $finder = new \DomXPath($dom);
        $crawledByNameFromPageNode = $finder->query("//img[contains(@class,'nav-item__profile-member-photo')]/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';
        $liUser = APIModel::getLiUser($companyData['id'], $crawledById);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByNameFromPage);
        //---END: Initialize basic values

        if (!empty($companyData["id"] && $crawledById && $clientHash && $crawledByName)) {
            ParserModel::saveHtmlDump($payload->url, $companyData["id"], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, $peopleId, $versionId, $payload->html);
        }

        ProfileSalesParser::handleProblemProfilePage($payload->html, $crawledByName, $crawledById, $peopleId);

        //---Parsing profile page
        //How it works (Non-documented feature!): http://php.net/manual/en/domxpath.query.php#99760
        $context = $finder->query("//div[@role='main']/section[contains(@class,'pv-top-card-section')]")->item(0);
        //people.is_connected - it's 1st degree
        $isConnected = 0;
        $degreeNode = $finder->query(".//div[contains(@class,'pv-top-card-section__distance-badge')]//span[@class='dist-value']", $context)->item(0);
        $degree = $degreeNode !== \NULL ? (int)substr(trim($degreeNode->nodeValue), 0, 1) : 0;
        if ($degree === 1) {
            $isConnected = 1;
        }
        $profileNameNode = $finder->query(".//h1[contains(@class, 'pv-top-card-section__name')]", $context)->item(0);
        $profileName = $profileNameNode !== \NULL ? trim($profileNameNode->nodeValue) : '';
        $headlineNode = $finder->query(".//h2[contains(@class, 'pv-top-card-section__headline')]", $context)->item(0);
        $headline = $headlineNode !== \NULL ? trim($headlineNode->nodeValue) : '';
        $locationNode = $finder->query(".//h3[contains(@class, 'pv-top-card-section__location')]", $context)->item(0);
        $location = $locationNode !== \NULL ? trim($locationNode->nodeValue) : '';
        $industry = ParserHelper::findJsonValueOnPageByKey($payload->html, 'industryName');
        $employerNode = $finder->query(".//h3[contains(@class, 'pv-top-card-section__company')]", $context)->item(0);
        $employer = $employerNode !== \NULL ? trim($employerNode->nodeValue) : '';
        $email = '';


        $employerId = '';
        try{
            $jsonResult = \ParserHelper::findCodeOnPageByKeyValue($payload->html, '$type', 'com.linkedin.voyager.identity.profile.ProfileView');
            $positionViewURN = $jsonResult['data']['positionView'];
            $companyViewURN = null;
            foreach($jsonResult['included'] as $inc){
                if($inc['$type'] === "com.linkedin.voyager.identity.profile.PositionView" && $inc['entityUrn'] === $positionViewURN){
                    $companyViewURN = $inc['elements'][0];
                    break;
                }
            }
            foreach($jsonResult['included'] as $inc){
                if($inc['$type'] === "com.linkedin.voyager.identity.profile.Position" && $inc['entityUrn'] === $companyViewURN){
                    $companyURN = !empty($inc['companyUrn']) ? $inc['companyUrn'] : '';
                    break;
                }
            }
            if (!empty($companyURN) && preg_match('#urn:li:fs_miniCompany:(.*)#i', $companyURN, $match)) {
                if (!empty($match[1])) {
                    $employerId = (int)$match[1];
                }
            }
        } catch(Exception $e){

        }


        //---END: Parsing profile page

        //save collected data into DB
        if (!empty($crawledById)) {
            parent::updatePeople([$profileName, $headline, $location, $industry, $employer, $employerId, $crawledByName, $crawledById, $email, $isConnected, $peopleId]);
        }

        return [
            'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
            'CrawledBy' => $crawledById . "|" . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => $crawledById,
            'WaitBetweenConnections' => parent::randomizeWait($waitBetweenConnections)
        ];
    }

    /**
     * @param $companyId
     * @param $crawledBy
     * @param $crawledById
     * @param $sfwStatus
     * @param $accountType
     * @return array li_users object
     * @internal param $lastConnection
     */
    public static function insertLiUser($companyId, $crawledBy, $crawledById, $sfwStatus, $accountType)
    {
        try {
            $query = DB::prep("INSERT INTO li_users (company_id, name, linked_in_id, software_status, last_connection, li_account_type, have_sales_navigator) VALUES (:company_id, :name, :linked_in_id, :software_status, UTC_TIMESTAMP(), :li_account_type, :have_sales_navigator)");
            $query->execute([
                'company_id' => $companyId,
                'name' => !empty($crawledBy) ? $crawledBy : APIModel::UNDEFINED_USER_NAME,
                'linked_in_id' => $crawledById,
                'software_status' => $sfwStatus,
                'li_account_type' => $accountType,
                'have_sales_navigator' => $accountType === APIModel::SALESNAV ? 1 : 0,
            ]);
            $userId = DB::get_instance()->lastInsertId();

            $query = DB::prep("SELECT * FROM li_users WHERE id = :id");
            $query->execute([
                'id' => $userId,
            ]);
            if ($query->rowCount() == 0) {
                throw new Exception('User was not created');
            }
            $result = $query->fetch();


            global $CONFIG;
            $intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
            $intercomHelper->addUser($companyId, $crawledById);

            return $result;
        } catch (Exception $e) {
            self::response("error", ["message" => $e->getMessage()]);
        }
    }
}