<?
require __DIR__ . '/../config.php';
global $CONFIG;

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;

function errorResponse($message){
    $output = [];
    $output["error"] = $message;
    die(json_encode($output));
}

$contents = file_get_contents("php://input");
$jsonParameters = [];
if ($contents != "") {
    $jsonParameters = json_decode($contents,true);

    if ($jsonParameters !== null and $jsonParameters != "") {
        $_GET = $jsonParameters;
    }
}

try{
    if (!isset($_GET["version"])) {
        errorResponse("version is not set");
    }


    /**
     * TODO implement logging
    */
//    $query = DB::prep("
//        INSERT INTO
//          startup_log
//        ");
//    $query->execute([
//        'linked_in_id' => $user['linked_in_id'],
//        'company_id' => $_GET['company_id'],
//        'date_start' => $date_start,
//        'date_end' => $date_end
//    ]);


    $output = [];
    
    $output["success"] = true;
    die(json_encode($output));
    
} catch (Exception $e) {
    errorResponse(log_api_error('startup_log', $jsonParameters, $e));
}

