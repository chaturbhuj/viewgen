<?php

class HtmlStorage {

	private static $curl = null;
    private static $FILE_DB_IP = '10.128.0.11';

	public static function store($html, $scope, $crawled_by_id, $id = "") {

		try {

			if (self::$curl === null) self::$curl = curl_init();

			curl_setopt(self::$curl, CURLOPT_URL, "http://" . self::$FILE_DB_IP . "/store.php?" . http_build_query([
				'scope' => $scope,
				'crawled_by_id' => $crawled_by_id,
				'id' => $id
			]));
		//	curl_setopt(self::$curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		//	curl_setopt(self::$curl, CURLOPT_USERPWD, "searchquant:sUnL22v552duJd3mkDskwKBN");
			curl_setopt(self::$curl, CURLOPT_POST, 1);
			curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $html);
			curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, 1);
			curl_exec(self::$curl);

		} catch (Exception $error) {
			return "";
		}
	}

	public static function retrieve($scope, $crawled_by_id, $id = "") {

		try {

			if (self::$curl === null) self::$curl = curl_init();

			curl_setopt(self::$curl, CURLOPT_URL, "http://" . self::$FILE_DB_IP . "/get.php?" . http_build_query([
				'scope' => $scope,
				'crawled_by_id' => $crawled_by_id,
				'id' => $id
			]));
		//	curl_setopt(self::$curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		//	curl_setopt(self::$curl, CURLOPT_USERPWD, "searchquant:sUnL22v552duJd3mkDskwKBN");
			curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec(self::$curl);

		} catch (Exception $error) {
			return "";
		}

		return $data;
	}

}

?>
