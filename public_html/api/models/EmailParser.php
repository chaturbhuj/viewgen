<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 21.08.2017
 * Time: 14:52
 */
require_once(__DIR__ . '/BadPageHandler.php');

use api\models\BadPageHandler;

class EmailParser
{

    /**
     * @param $company
     * @param $payload
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function checkEmail($company, $payload, $version)
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        $pageData = EmailParser::getEmailFromPage($payload->html);
//        var_dump($pageData['name']);die();
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $email = $pageData['email'] ?? '';
        $crawledByName = !empty($crawledByName) ? $crawledByName : $pageData['name'];

        $liUser = APIModel::getLiUser($company['id'], $crawledById);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByName);

        $wait = $company['wait_between_connections'];

        $versionId = APIModel::getClientVersionIdByName($version);
        $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $company['id'], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);

        if (empty($email)) {
            //==== CHECK FOR BAD PAGE
            $bpHandler = new api\models\BadPageHandler($payload->html, $crawledById);
            $bphResult = $bpHandler->handleBadPage();
            if ($bphResult) {
                ParserModel::insertApiBadPages($company['id'], $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, \NULL, \NULL, \NULL, $bphResult['bp-message'], \APIModel::EMAIL_PAGE);
                return array_merge([
                    'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                    'CrawledBy' => $crawledById . "|" . $crawledByName,
                    'username' => $crawledByName,
                    'linked_in_id' => (int)$crawledById,
                    'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
                ], $bphResult['response']);
            } else {
                return [
                    'bp-message' => 'html: p.email-address',
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'No needed data on the page. Try again...'
                        ]
                    ]
                ];
            }
        }

        if ($liUser['user_email'] !== $email) {
            EmailParser::updateEmail($crawledById, $email);
        } else {
            EmailParser::updateEmail($crawledById);
        }

        APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_RUNNING);
        return [
            'WaitBetweenConnections' => ParserModel::randomizeWait($wait),
            'CrawledBy' => $crawledById . '|' . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => (int)$crawledById,
            'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
            'checkingEmail' => 0
        ];
    }

    /**
     * @param $html
     * @return array|null
     */
    public static function getEmailFromPage($html)
    {
        $result = [];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);
        $node = $finder->query("//ul[@class='existing-email-addresses']//span[@class='is-primary']//ancestor::li//p[@class='email-address']")->item(0);
        $result['email'] = $node !== \NULL ? trim($node->nodeValue) : '';
        $node = $finder->query("//div[@class='user-title']/h2")->item(0);
        $result['name'] = $node !== \NULL ? trim($node->nodeValue) : '';
        return $result;
    }

    /**
     * @param $crawledById
     * @param bool $email
     * @return bool
     */
    public static function updateEmail($crawledById, $email = \FALSE)
    {
        if ($email === \FALSE) {
            $query = \DB::prep('UPDATE li_users SET last_email_check = NOW() WHERE linked_in_id = :linked_in_id');
            $query->execute([
                'linked_in_id' => $crawledById
            ]);
        } else {
            $query = \DB::prep('UPDATE li_users SET last_email_check = NOW(), user_email = :user_email WHERE linked_in_id = :linked_in_id');
            $query->execute([
                'linked_in_id' => $crawledById,
                'user_email' => $email
            ]);
        }
        if ($query->rowCount() == 0) {
            return \FALSE;
        }
        return \TRUE;
    }

}