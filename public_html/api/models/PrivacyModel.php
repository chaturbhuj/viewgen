<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 16.06.2017
 * Time: 10:24
 */
class PrivacyModel
{
    const FULL = 'Full profile';
    const ANON = 'Characteristics';
    const HIDE = 'Private mode';
    const UNKNOWN = 'Could Not Be Determined';


    /**
     * @param $html
     * @return array|null
     */
    public static function parsePrivacyPage($html)
    {
        $result = [];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);
        $node = $finder->query("//div[contains(@class, 'user-title')]/h2")->item(0);
        $result['name'] = $node !== \NULL ? trim($node->nodeValue) : '';
        $inputFull = $dom->getElementById('option-full') ?? '';
        $inputAnonymous = $dom->getElementById('option-anonymous') ?? '';
        $inputHide = $dom->getElementById('option-hide') ?? '';
        if (!empty($inputFull) AND $inputFull->hasAttribute('checked')) {
            $result['privacy'] = self::FULL;
        } elseif (!empty($inputAnonymous) AND $inputAnonymous->hasAttribute('checked')) {
            $result['privacy'] = self::ANON;
        } elseif (!empty($inputHide) AND $inputHide->hasAttribute('checked')) {
            $result['privacy'] = self::HIDE;
        } else {
            $result['privacy'] = self::UNKNOWN;
        }
        return $result;
    }


    /**
     * @param $name
     * @param bool $anonymous
     * @param $anonymousHtml
     * @return bool
     */
    public static function updateAnonymous($name, $anonymous = \FALSE)
    {
        if ($anonymous === \FALSE) {
            $query = \DB::prep('UPDATE li_users SET lastAnonymusCheck = NOW() WHERE name = :name');
            $query->execute([
                'name' => $name
            ]);
        } else {
            $query = \DB::prep('UPDATE li_users SET lastAnonymusCheck = NOW(), anonymous = :anonymous WHERE name = :name');
            $query->execute([
                'name' => $name,
                'anonymous' => $anonymous,
            ]);
        }
        if ($query->rowCount() == 0) {
            return \FALSE;
        }
        return \TRUE;
    }

    /**
     * @param $emailTo
     * @param $userName
     * @return bool
     */
    public static function sendNotification($emailTo, $userName)
    {
        //TODO make notification
//        include(__DIR__ . '/../../classes/Email.php');
//        $em = new Email();
//        $em->setHtmlBody(' ');
//        $em->setTextBody(' ');
//        $em->setSubject(' ');
//        $em->setTemplateId('6789d090-8b81-45a0-ae89-9b1c7a80939a');
//        $em->addTo($emailTo);
//        $em->send();
        return false;
    }

    /**
     * @param $company
     * @param $payload
     * @return array
     * @throws Exception
     */
    public static function checkPrivacy($company, $payload, $version)
    {
        //Client send ready DOM-html!
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        $pageData = self::parsePrivacyPage($payload->html);
        $crawledById = $payload->linked_in_id;
        $anonymous = $pageData['privacy'] ?? '';
        $crawledByName = $pageData['name'] ?? '';

        $versionId = APIModel::getClientVersionIdByName($version);
        $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $company['id'], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);

        $liUser = APIModel::getLiUser($company['id'], $crawledById);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByName);

        $wait = $company['wait_between_connections'];

        //==== CHECK FOR BAD PAGE
        $bpHandler = new api\models\BadPageHandler($payload->html, $crawledById);
        $badPage = $bpHandler->handleBadPage();
        if ($badPage) {
            ParserModel::insertApiBadPages($company['id'], $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $badPage['bp-message'], \APIModel::PRIVACY_PAGE);
            return array_merge([
                'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
            ], $badPage['response']);
        }
        //=======

        if(empty($anonymous)){
            throw new Exception('Can\'t find data on the page.');
        }
        if ($liUser['anonymous'] !== $anonymous) {
            self::updateAnonymous($crawledByName, $anonymous);
        } else {
            self::updateAnonymous($crawledByName);
        }
        if ($anonymous !== self::FULL) {
            APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_WAITING);
            if(!empty($company['email'])){
                //TODO only one for one day
//                self::sendNotification($company['email'], $liUser['name']);
            }
            return [
                'url' => '',
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'CrawledBy' => $crawledById . '|' . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
                'status_message' => [
                    'status' => 2,
                    'message' => 'ATTENTION! Please change your privacy mode to public.'
                ]
            ];
        }
        APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_RUNNING);
        return [
            'WaitBetweenConnections' => 10,
            'CrawledBy' => $liUser['linked_in_id'] . '|' . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => (int)$liUser['linked_in_id'],
            'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
            'checkingPrivacySettings' => 0
        ];
    }
}