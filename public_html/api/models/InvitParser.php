<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 02.08.2017
 * Time: 14:35
 */

namespace api\models;
class InvitParser
{
    const NO_DATA_MESSAGE = 'No needed data on the page. Try again...';
    const RES_PER_PAGE = 10;
    private static $_instance;
    private $finder = \NULL;
    private $html = \NULL;
    private $crawledById = \NULL;
    private $versionId = \NULL;
    private $clientHash = \NULL;
    private $companyData = \NULL;
    private $liUser = \NULL;
    private $companyId = \NULL;
    private $crawledByName = \NULL;
    private $url = \NULL;
    private $wait = \APIModel::DEFAULT_WAIT;
    private $htmlDumpId = \NULL;
    public $payload = [];
    private $cards = [];
    public $clientParameters = [
        'linked_in_id',
        'html',
        'client_hash'
    ];

    /**
     * @param $payload
     * @param string $version
     * @return InvitParser
     */
    public static function getInstance($payload, $version = "")
    {
        if (self::$_instance == null) {
            self::$_instance = new self($payload, $version);
        }
        return self::$_instance;
    }

    /**
     * InvitParser constructor.
     * @param $payload
     * @param string $version
     * @throws \Exception
     */
    public function __construct($payload, $version = "")
    {
        try {
            $this->payload = $payload;
            $this->checkParameters();
            $this->html = $payload->html;
            $this->url = $payload->url;
            $this->versionId = \ParserModel::getClientVersionIdByName($version);
            $this->companyData = \ParserModel::getCompanyByProductKey($payload->client_hash);
            $this->companyId = $this->companyData['id'];
            $this->crawledById = $payload->linked_in_id;
            $this->clientHash = $payload->client_hash;
            $this->liUser = \APIModel::getLiUser($this->companyId, $this->crawledById);
            $this->crawledByName = $payload->username ?? '';
            $this->wait = $this->companyData['wait_between_connections'];
            $this->finder = $this->getFinder();
        } catch (\Exception $e) {
            throw new \Exception("Initialization of InvitParser failed: " . $e->getMessage());
        }
    }

    /**
     * Check for needed parameters from client
     * @return bool
     */
    public function checkParameters()
    {
        foreach ($this->clientParameters as $par) {
            if (empty($this->payload->$par)) {
                throw new Exception("No $par parameter!");
            }
        }
        return \TRUE;
    }

    /**
     * @return \DomXPath
     */
    public function getFinder()
    {
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($this->html)) {
            throw new Exception("HTML did not loaded!");
        };
        return new \DomXPath($dom);
    }

    /**
     * Parse name of Li-User and update it in DB if needed.
     */
    public function check4Name()
    {
        $crawledByNameFromPageNode = $this->finder->query("//img[contains(@class, 'nav-item__profile-member-photo')]/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';

        $this->crawledByName = \FirstPageModel::updateLiUserName($this->liUser, $crawledByNameFromPage);

        return \FALSE;
    }

    /**
     * @return array
     */
    public function defaultReturn()
    {
        return [
            'VisitedProfiles24' => (int)$this->liUser["visitsLast24Hours"],
            'CrawledBy' => $this->crawledById . "|" . $this->crawledByName,
            'username' => $this->crawledByName,
            'linked_in_id' => $this->crawledById,
            'WaitBetweenConnections' => \ParserModel::randomizeWait($this->wait)
        ];
    }

    /**
     * Main method
     * @return array
     */
    public function checkInvitations()
    {
        $this->check4Name();

        $this->htmlDumpId = \ParserModel::saveHtmlDump($this->url, $this->companyId, $this->crawledByName, $this->crawledById, $_SERVER["REMOTE_ADDR"], $this->clientHash, 0, $this->versionId, $this->html);

        //==== CHECK FOR BAD PAGE
        $bpHandler = new BadPageHandler($this->html, $this->crawledById);
        $badPage = $bpHandler->handleBadPage();
        if ($badPage) {
            \ParserModel::insertApiBadPages($this->companyId, $this->crawledById, $this->url ?? '', $this->html, $this->versionId, $this->htmlDumpId ?? \NULL, \NULL, \NULL, \NULL, $badPage['bp-message'], \APIModel::INVIT_PAGE);
            return array_merge($this->defaultReturn(), $badPage['response']);
        }
        //===

        $this->cards = $this->parseInvitPage();
//        var_dump($this->cards);
//        die();

        $this->saveInvitations();
        $this->updateInvitCheck();
        \APIModel::updateSfwStatus($this->companyId, $this->crawledById, \APIModel::STATUS_RUNNING);
        return array_merge($this->defaultReturn(), ['invitCheck' => 0]);
    }

    /**
     * @return bool
     */
    public function updateInvitCheck()
    {
        $query = \DB::prep("UPDATE li_users SET last_invitations_check = NOW() WHERE linked_in_id = :linked_in_id");
        $query->execute([
            'linked_in_id' => $this->crawledById
        ]);
        if ($query->rowCount() !== 1) {
            return \FALSE;
        }
        return \TRUE;
    }

    /**
     * @return mixed
     */
    public function saveInvitations()
    {
        foreach ($this->cards as $data) {
            $this->insertInvitations($data);
        }
        return \FALSE;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function insertInvitations($data)
    {
        try {
            $query = \DB::prep("INSERT INTO received_invitations (crawled_by_id, crawled_by_name, linkedin_id, member_id, name, occupation, sent_time) VALUES (:crawled_by_id, :crawled_by_name, :linkedin_id, :member_id, :name, :occupation, :sent_time) ");
            $query->execute([
                'crawled_by_id' => $this->crawledById,
                'crawled_by_name' => $this->crawledByName,
                'linkedin_id' => $data['linkedinId'],
                'member_id' => $data['memberId'],
                'name' => $data['name'],
                'occupation' => $data['occupation'],
                'sent_time' => $data['sentTime']
            ]);
        } catch (Exception $e) {
            throw $e;
        }
        return \FALSE;
    }

    /**
     * @return array
     */
    public function parseInvitPage()
    {
        $jsonResult = \ParserHelper::findCodeOnPageByKey($this->html, 'metadata');
        $jsonResult = \ParserHelper::getArrayValueByKey($jsonResult, 'included');

        $result = [];
        $cardNodes = $this->finder->query("//div[contains(@class,'js-invitation-card__invite-details-container')]");

        foreach ($cardNodes as $card) {
            $linkNode = $this->finder->query(".//a[contains(@class, 'invitation-card__link')]/@href", $card)->item(0);
            $link = $linkNode !== \NULL ? trim($linkNode->nodeValue) : '';
            if (!empty($link)) {
                preg_match('#\/in\/([^\/]*)\/#', $link, $match);
                if (!empty($match AND $match[1])) {
                    $linkedinId = $match[1];
                }
            } else {
                continue;
            }

            $duplicate = $this->checkDuplicate($linkedinId);
            if ($duplicate) {
                continue;
            }

            $nameNode = $this->finder->query(".//span[contains(@class, 'invitation-card__name')]", $card)->item(0);
            $name = $nameNode !== \NULL ? trim($nameNode->nodeValue) : '';
            if (empty($name)) {
                continue;
            }

            $occupationNode = $this->finder->query(".//span[contains(@class, 'invitation-card__occupation')]", $card)->item(0);
            $occupation = $occupationNode !== \NULL ? trim($occupationNode->nodeValue) : '';

            $person = \ParserHelper::getArrayByValue($jsonResult, $linkedinId);
//            var_dump($linkedinId, $person);
            $objectUrn = $person['objectUrn'];
            if (preg_match('#urn:li:member:(.*)#i', $objectUrn, $match)) {
                if (!empty($match[1])) {
                    $memberId = (int)$match[1];
                }
            }
            if (empty($memberId)) {
                continue;
            }

            if (isset($person['entityUrn'])) {
                $entityUrn = $person['entityUrn'];
                $arrTime = \ParserHelper::getArrayByKeyAndValue($jsonResult, "fromMember", $entityUrn);
                if (!empty($arrTime) AND isset($arrTime['sentTime'])) {
                    $sentTime = (int)substr($arrTime['sentTime'], 0, 10);
                }
            }

            $result[] = [
                'name' => $name ?? '',
                'occupation' => $occupation ?? '',
                'linkedinId' => $linkedinId ?? '',
                'memberId' => $memberId ?? '',
                'sentTime' => isset($sentTime) ? date('Y-m-d h:i:s', $sentTime) : \NULL
            ];
        }

        return $result;
    }

    /**
     * @param $linkedinId
     * @return bool
     * @throws \Exception
     */
    public function checkDuplicate($linkedinId)
    {
        try {
            $query = \DB::prep("SELECT COUNT(id) FROM received_invitations WHERE crawled_by_id = :crawled_by_id AND linkedin_id = :linkedin_id");
            $query->execute([
                'crawled_by_id' => $this->crawledById,
                'linkedin_id' => $linkedinId
            ]);
            $count = $query->fetchColumn();
            return $count;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}