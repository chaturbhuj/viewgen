<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 20.06.2017
 * Time: 18:44
 */
class InboundFreeModel
{
    /**
     * @param $name
     * @param $guessedVisitDate
     * @param $crawledById
     * @return bool
     */
    public static function check4Duplicate($name, $guessedVisitDate, $crawledById)
    {
//        $select = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE name = :name AND (guessed_visit_date = :guessed_visit_date OR guessed_visit_date = DATE_SUB(:guessed_visit_date, INTERVAL 1 DAY) OR guessed_visit_date = DATE_ADD(:guessed_visit_date, INTERVAL 1 DAY)) AND crawled_by_id = :crawled_by_id");
        $select = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE name = :name AND guessed_visit_date = :guessed_visit_date AND crawled_by_id = :crawled_by_id");
        $select->execute([
            'name' => $name,
            'guessed_visit_date' => $guessedVisitDate,
            'crawled_by_id' => $crawledById
        ]);
        if ($select->fetchColumn() > 0) return \TRUE;
        return \FALSE;
    }

    /**
     * @param $crawledById
     * @param $linkedinId
     * @param $name
     * @param $headline
     * @param $guessedVisitDate
     * @return string
     */
    public static function insertInbound($crawledById, $linkedinId, $name, $headline, $guessedVisitDate)
    {
        $insert = \DB::prep("INSERT INTO inbound_visit (crawled_by_id, linked_in_public_id, name, headline, guessed_visit_date) VALUES(:crawled_by_id, :linked_in_id, :name, :headline, :guessed_visit_date)");
        $insert->execute([
            'crawled_by_id' => $crawledById,
            'linked_in_id' => $linkedinId,
            'name' => $name,
            'headline' => $headline,
            'guessed_visit_date' => $guessedVisitDate
        ]);
        if ($insert->rowCount() !== 1) {
            return \FALSE;
        }
        return \DB::get_instance()->lastInsertId();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function updateLiUserLastInboundCheck($id)
    {
        $query = DB::prep("UPDATE li_users SET last_inbound_check = utc_timestamp() WHERE linked_in_id = :linked_in_id");
        $query->execute([
            "linked_in_id" => $id
        ]);
        if ($query->rowCount() !== 1) {
            return \FALSE;
        } else{
            return \TRUE;
        }
    }

    /**
     * @param $payload
     * @param string $version
     * @return array|null - the same $payload with few changed params
     * @throws Exception
     */
    public static function checkInboundViews($payload, $version = '')
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        //It's for non English strings
        mb_internal_encoding("UTF-8");
        $versionId = APIModel::getClientVersionIdByName($version);
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        if(empty($crawledByName)){
            $crawledByName = APIModel::explodeCrawledBy($payload->latest_li_user_id_inbound_crawled_by)[1];
        }
        $payload->{'WaitBetweenConnections'} = 10;
        $company = APIModel::getCompanyByProductKey($payload->client_hash);
        $liUser = APIModel::getLiUser($company['id'], $crawledById);
        $inboundNumber = InboundSalesParser::getNumberInbound($crawledById);
        //save into storage.
        if(!empty($company['id'] && $crawledById && $payload->client_hash && $crawledByName)) {
            HtmlStorage::store($payload->html, 'viewbacks', $crawledById, $inboundNumber);
            $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $company['id'], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);
        }

        $wait = $company['wait_between_connections'];

        //==== CHECK FOR BAD PAGE
        $bpHandler = new api\models\BadPageHandler($payload->html, $crawledById);
        $badPage = $bpHandler->handleBadPage();
        if ($badPage) {
            ParserModel::insertApiBadPages($company['id'], $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $badPage['bp-message'], \APIModel::FREE_INBOUND_PAGE);
            return array_merge([
                'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
            ], $badPage['response']);
        }
        //=======

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($payload->html)) return NULL;
        $finder = new \DomXPath($dom);

        $crawledByNameFromPageNode = $finder->query("//img[contains(@class,'nav-item__profile-member-photo')]/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByNameFromPage);

        $profiles = $finder->query("//div[contains(@class,'me-wvmp-viewer-card')]");
        foreach ($profiles as $profile){
            $link = $finder->query(".//a[contains(@class,'card-link-inherit')]/@href", $profile)->item(0);
            $link = $link !== \NULL ? trim($link->nodeValue) : '';
            $public_id = str_replace('/in/', '', $link);
            $public_id = trim($public_id, '/');


            $name = $finder->query(".//span[contains(@class,'me-wvmp-viewer-card__name-text')]", $profile)->item(0);
            $name = $name !== \NULL ? trim($name->nodeValue) : '';

            $headline = $finder->query(".//div[contains(@class,'me-wvmp-viewer-card__viewer-headline')]", $profile)->item(0);
            $headline = $headline !== \NULL ? trim($headline->nodeValue) : '';

            $timeAgo = $finder->query(".//p[contains(@class,'me-wvmp-viewer-card__time-ago')]", $profile)->item(0);
            $timeAgo = $timeAgo !== \NULL ? trim($timeAgo->nodeValue) : '';

            preg_match('/(?P<count>\d+)(?P<interval>(d|w|mo|y)+)/', $timeAgo, $matches);

            $current_date = new DateTime();
            if(!empty($matches)){
                switch ($matches['interval']){
                    case 'd':
                        $current_date = $current_date->sub(new DateInterval('P'.$matches['count'].'D'));
                        break;
                    case 'w':
                        $current_date = $current_date->sub(new DateInterval('P'.$matches['count'].'W'));
                        break;
                    case 'mo':
                        $current_date = $current_date->sub(new DateInterval('P'.$matches['count'].'M'));
                        break;
                    case 'y':
                        $current_date = $current_date->sub(new DateInterval('P'.$matches['count'].'Y'));
                        break;
                }
            }
            $guessedVisitDate = $current_date->format('Y-m-d');

            if(empty($name)){
                continue;
            }

            if (self::check4Duplicate($name, $guessedVisitDate, $crawledById) === \TRUE) {
                continue;
            }

            self::insertInbound($crawledById, $public_id, $name, $headline, $guessedVisitDate);

        }

        self::updateLiUserLastInboundCheck($crawledById);
        $payload->inbound_visits_url = 0;
        $payload->CrawledBy = $payload->latest_li_user_id_inbound_crawled_by;
        $payload->username = $crawledByName;
        $payload->linked_in_id = $crawledById;
        $payload->html = '';
        return (array)$payload;
    }
}