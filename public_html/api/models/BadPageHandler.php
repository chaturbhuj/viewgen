<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 18.08.2017
 * Time: 10:46
 */

namespace api\models;
//require_once(__DIR__ . '/../APIModel.php');

class BadPageHandler
{
    private $finder = \NULL;
    private $html = \NULL;
    private $result = \FALSE;
    private $linkedinId = \NULL;

    /**
     * BadPageHandler constructor.
     * @param $html
     * @param null $linkedinId
     */
    public function __construct($html, $linkedinId = \NULL)
    {
        //Load HTML to parse
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            $this->result = [
                'bp-message' => 'html: loadHTML failed',
                'response' => [
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Bad page received. Try again...'
                    ]
                ]
            ];
        }
        $this->html = $html;
        $this->finder = new \DomXPath($dom);
        if (!empty($linkedinId)) {
            $this->linkedinId = $linkedinId;
        }
    }

    /**
     * @return \DomXPath|null
     */
    public function getFinder()
    {
        return $this->finder;
    }

    /**
     * @return null
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @return array|bool
     */
    public function handleBadPage()
    {
        if ($this->result) {
            return $this->result;
        }
        //'li','html','vg'
        //Frozen VG-account
        if (strpos(trim($this->html), "This account is frozen") === 0) {
            $this->result = [
                'bp-message' => 'vg: frozen',
                'response' => [
                    'url' => \APIModel::BASE_VG_URL . '/frozen_account.php',
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'This account is frozen. Contact ' . \APIModel::VIEWGEN_MAIL
                    ]
                ]
            ];
            return $this->result;
        }

        //Linked In lockout (sales nav limit exceeded)
        $locked = $this->finder->query("//body[@id='pagekey-sales-limit-exceeded']")->length > 0 ? \TRUE : \FALSE;
        if ($locked) {
            $this->result = [
                'bp-message' => 'li: sales nav limit',
                'response' => [
                    'WaitBetweenConnections' => 86400, //wait 24h
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Linkedin: Sales nav limit exceeded'
                    ]
                ]
            ];
            return $this->result;
        }

        //Linkedin Errors
        $appError = $this->finder->query("//div[@class='app-error']")->length > 0 ? \TRUE : \FALSE;
        if ($appError) {
            //Bad URL ("We were unable to retrieve results for your search request at this time.")
            $sorryMessage = $this->finder->query("//div[@class='app-error']//p[@class='sorry-message']")->item(0);
            if ($sorryMessage !== \NULL) {
                $this->result = [
                    'bp-message' => 'li: ' . trim($sorryMessage->nodeValue),
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'Linkedin: ' . trim($sorryMessage->nodeValue)
                        ]
                    ]
                ];
                return $this->result;
            }
        }
        //LI: Security Verification ("Just a quick security check")
        $challenge = $this->finder->query("//div[@id='challengeContent']")->item(0);
        if ($challenge !== \NULL) {
//            $challengeMessageNode = $this->finder->query("//div[@id='challengeContent']//h1")->item(0);
//            $challengeMessage = $challengeMessageNode !== \NULL ? trim($challengeMessageNode->nodeValue) : '';
            $this->result = [
                'bp-message' => 'li: Security Verification',
                'response' => [
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Linkedin: Security Verification'
                    ]
                ]
            ];
            return $this->result;
        }
        //===Linkedin Errors

        //LI: Wiper ("We're getting things cleaned up. We'll be back soon.")
        $wiper = $this->finder->query("//div[@id='wiper']")->item(0);
        if($wiper !== \NULL){
            $liMessageNode = $this->finder->query("//div[@id='en-us']/h2/span[@class='message']")->item(0);
            $liMessage = $liMessageNode !== \NULL ? $liMessageNode->nodeValue : 'We\'re getting things cleaned up. We\'ll be back soon.';
            $this->result = [
                'bp-message' => 'li: wiper',
                'response' => [
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Linkedin: ' . $liMessage
                    ]
                ]
            ];
            return $this->result;
        }

        //<h1 class="search-no-results__message search-no-results__message--no-type Sans-21px-black-85%">No results found.</h1>
        $noResultMessageNode = $this->finder->query("//h1[contains(@class,'search-no-results__message')]")->item(0);
        if (!empty($noResultMessageNode)) {
        $noResultMessage = $noResultMessageNode !== \NULL ? $noResultMessageNode->nodeValue : 'No results found.';
            $this->result = [
                'bp-message' => 'li: search-no-results',
                'response' => [
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Linkedin: ' . $noResultMessage
                    ]
                ]
            ];
            return $this->result;
        }

        //LI: Double Sales
        $contractManagement = $this->finder->query("//div[contains(@id, 'contract-management')]/p[contains(@class,'contracts-header')]")->length;
        $contractChooser = $this->finder->query("//article[@class='contract-chooser']")->length;
        if ($contractManagement > 0 OR $contractChooser > 0) {
            $this->result = [
                'bp-message' => 'li: Multiple Sales',
                'response' => [
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'You’re on multiple Sales Navigator teams. Please pick one to continue.'
                    ]
                ]
            ];
            if (!empty($this->linkedinId)) {
                self::updateMultipleSales($this->linkedinId, 1);
            }
            return $this->result;
        }

        //LI: page still on loading
        $loading = $this->finder->query("//div[@class='initial-loading-state' or @class='initial-load-animation']")->item(0);
        $loadingComplete = $this->finder->query("//body[contains(@class, 'boot-complete')]")->item(0); //new profile page
        if ($loading !== \NULL && $loadingComplete === \NULL) {
            $this->result = [
                'bp-message' => 'html: loading',
                'response' => [
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Page did not load in time. Try again...'
                    ]
                ]
            ];
            return $this->result;
        }

        //LI: Login page
        $login = $this->finder->query("//meta[@name='pageKey' and (contains(@content, 'guest-home') or contains(@content, 'consumerLogin') or contains(@content, 'uno-reg-join'))] | //div[@id='main' and @class='signin'] | //iframe[contains(@class, 'authentication-iframe')]")->item(0);

        if ($login !== \NULL) {
            $this->result = [
                'bp-message' => 'li: login',
                'response' => [
                    'url' => \APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 1,
                        'message' => 'You need to sign in.'
                    ]
                ]
            ];
            return $this->result;
        }

        return \FALSE;
    }

    /**
     * @param $linkedinId
     * @param $val
     * @return bool|int
     * @throws \Exception
     */
    public static function updateMultipleSales($linkedinId, $val)
    {
        if (empty($linkedinId)) {
            throw new \Exception('Empty Linkedin ID.');
        }
        if (!isset($val) OR ($val != 0 AND $val != 1)) {
            throw new \Exception('Wrong parameter for Multiple Sales.');
        }
        try {
            $query = \DB::prep("SELECT multiple_sales FROM li_users WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'linked_in_id' => $linkedinId
            ]);
            $multipleSales = $query->fetchColumn();
            if ($multipleSales == $val) {
                return \FALSE;
            }
            $query = \DB::prep("UPDATE li_users SET last_update_multiple_sales = NOW(), multiple_sales = :multiple_sales WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'multiple_sales' => $val,
                'linked_in_id' => $linkedinId
            ]);
            if ($query->rowCount() > 0) {
                return $query->rowCount();
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return \FALSE;
    }
}