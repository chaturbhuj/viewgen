<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 06.09.2017
 * Time: 12:35
 */

namespace api\models;


class SearchFreeParser
{
    const NO_DATA_MESSAGE = 'No needed data on the page. Try again...';
    const RES_PER_PAGE = 10;
    private static $_instance = \NULL;
    private $finder = \NULL;
    private $html = \NULL;
    private $jsonPageData = \NULL;
    private $crawledById = \NULL;
    private $versionId = \NULL;
    private $clientHash = \NULL;
    private $companyData = \NULL;
    private $campaignData = \NULL;
    private $liUser = \NULL;
    private $companyId = \NULL;
    private $crawledByName = \NULL;
    private $url = \NULL;
    private $wait = \APIModel::DEFAULT_WAIT;
    private $peopleListId = \NULL;
    private $peopleList = \NULL;
    private $campaignId = \NULL;
    private $searchUrlId = \NULL;
    private $totalMatches = \NULL;
    private $peopleOnThisPage = 0;
    private $htmlDumpId = \NULL;
    public $payload = [];
    public $clientParameters = [
        'linked_in_id',
        'html',
        'client_hash'
    ];

    /**
     * @param $payload
     * @param string $version
     * @return SearchFreeParser
     */
    public static function getInstance($payload, $version = "")
    {
        if (self::$_instance == null) {
            self::$_instance = new self($payload, $version);
        }
        return self::$_instance;
    }

    /**
     * SearchFreeParser constructor.
     * @param $payload
     * @param string $version
     * @throws \Exception
     */
    public function __construct($payload, $version = "")
    {
        try {
            $this->payload = $payload;
            $this->checkParameters();
            $this->html = $payload->html;
            $this->url = $payload->url;
            $this->versionId = \ParserModel::getClientVersionIdByName($version);
            $this->companyData = \ParserModel::getCompanyByProductKey($payload->client_hash);
            $this->companyId = $this->companyData['id'];
            $this->crawledById = $payload->linked_in_id;
            $this->clientHash = $payload->client_hash;
            $this->liUser = \APIModel::getLiUser($this->companyId, $this->crawledById);
            $this->crawledByName = $payload->username ?? '';
            $this->wait = $this->companyData['wait_between_connections'];
            $this->peopleListId = $payload->people_list_id ?? \NULL;
            $this->peopleList = \ParserModel::getPeopleList($this->peopleListId);
            $this->searchUrlId = !empty($this->peopleList) ? (int)$this->peopleList['search_url_id'] : \NULL;
            $this->campaignId = !empty($payload->campaign_id) ? (int)$payload->campaign_id : \NULL;
            $this->campaignData = !empty($this->campaignId) ? \APIModel::getCampaignById($this->campaignId) : \NULL;
            $this->finder = $this->getFinder();
        } catch (\Exception $e) {
            throw new \Exception("Initialization of SearchFreeParser failed: " . $e->getMessage());
        }
    }

    /**
     * Check for needed parameters from client
     * @return bool
     * @throws \Exception
     */
    public function checkParameters()
    {
        foreach ($this->clientParameters as $par) {
            if (empty($this->payload->$par)) {
                throw new \Exception("No $par parameter!");
            }
        }
        return \TRUE;
    }

    /**
     * @return \DomXPath
     * @throws \Exception
     */
    public function getFinder()
    {
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($this->html)) {
            throw new \Exception("HTML did not loaded!");
        };
        return new \DomXPath($dom);
    }

    /**
     * @return array
     */
    public function defaultReturn()
    {
        return [
            'VisitedProfiles24' => (int)$this->liUser["visitsLast24Hours"],
            'CrawledBy' => $this->crawledById . "|" . $this->crawledByName,
            'username' => $this->crawledByName,
            'linked_in_id' => $this->crawledById,
            'WaitBetweenConnections' => \ParserModel::randomizeWait($this->wait)
        ];
    }

    /**
     * @param $bpMessage
     * @param string $resMessage
     * @return array
     */
    public function itIsBadPage($bpMessage, $resMessage = SearchFreeParser::NO_DATA_MESSAGE)
    {
        \HtmlStorage::store(file_get_contents("php://input"), "people_list", $this->crawledById, $this->peopleListId);
        if ($this->peopleList['retry_count'] >= \SearchSalesParser::RETRY_COUNT) {
            \SearchSalesParser::updatePeopleListRetry($this->peopleList['id'], 0);
        } else {
            // Retry
            \SearchSalesParser::updatePeopleListRetry($this->peopleList['id'], 1);
        }
        \ParserModel::insertApiBadPages($this->companyId, $this->crawledById, $this->payload->url ?? '', $this->html, $this->versionId, $this->htmlDumpId ?? \NULL, $this->campaignId ?? \NULL, $this->searchUrlId ?? \NULL, $this->peopleListId ?? \NULL, $bpMessage, \APIModel::SEARCH_SIMPLE);
        $response = [
            'bp-message' => $bpMessage,
            'response' => [
                'status_message' => [
                    'status' => 2,
                    'message' => $resMessage
                ]
            ]];
        return array_merge($this->defaultReturn(), $response);
    }

    /**
     * Main method for search page parser for no-salesnav accounts.
     * @return array
     */
    public function searchPage()
    {
        $this->check4Name();
        $this->htmlDumpId = \ParserModel::saveHtmlDump($this->url, $this->companyId, $this->crawledByName, $this->crawledById, $_SERVER["REMOTE_ADDR"], $this->clientHash, 0, $this->versionId, $this->html);

        //==== CHECK FOR BAD PAGE
        $badPage = $this->handleBadPage($this->html, $this->crawledById);
        if ($badPage) {
            if ($this->isItLastPage() AND ($badPage['bp-message'] == 'li: search-no-results' OR $this->peopleList['retry_count'] >= \SearchSalesParser::RETRY_COUNT)) {
                $this->finishPeopleListAndCampaign();
                return $this->defaultReturn();
            }
            return $this->itIsBadPage($badPage['bp-message'], $badPage['response']['status_message']['message']);
        }
        //===

        $this->parseTotalMatches();
        $this->parseJsonData();

        \ParserModel::updatePeopleList($this->crawledByName, $this->crawledById, $this->peopleListId);

        if (!empty($peopleListId) AND $this->peopleOnThisPage == 0) {
            \HtmlStorage::store(file_get_contents("php://input"), "people_list", $this->liUser["linked_in_id"], $peopleListId);
            // Check if we should retry.
            $retry = \SearchSalesParser::check4Retry($this->peopleList);
            if ($retry) {
                return $this->defaultReturn();
            }
            $this->finishPeopleListAndCampaign();
        }

        return $this->defaultReturn();
    }

    /**
     * @return bool
     */
    public function finishPeopleListAndCampaign()
    {
        \SearchSalesParser::updatePeopleListCompleted($this->crawledByName, $this->crawledById, $this->peopleListId, $this->searchUrlId);
        if(\SearchSalesParser::checkCampaign4FinishAndRevisit($this->campaignId, $this->peopleListId, $this->peopleOnThisPage)){
        \SearchSalesParser::sendNotification([
            'company_id' => $this->companyId,
            'li_user_name' => $this->crawledByName,
            'company_name' => $this->companyData['company_name'],
            'campaign_name' => $this->campaignData['name'],
            'campaign_visit_count' => $this->campaignData['campaign_visit_count'],
            'total_visitors' => $this->campaignData['total_visitors'],
            'campaign_id' => $this->campaignId,
            'company_email' => $this->companyData['email'],
        ]);
        }
        return \FALSE;
    }

    /**
     * @return bool
     */
    public function isItLastPage()
    {
        if ($this->peopleList['page_nr'] < ceil($this->peopleList['total_matches'] / self::RES_PER_PAGE) AND $this->peopleList['page_nr'] < ceil(1000 / self::RES_PER_PAGE)) {
            return \FALSE;
        }
        return \TRUE;
    }

    /**
     * @return bool
     */
    public function check4Retry()
    {
        if ($this->peopleList['page_nr'] < ceil($this->peopleList['total_matches'] / self::RES_PER_PAGE)) {
            if ($this->peopleList['retry_count'] >= \SearchSalesParser::RETRY_COUNT) {
                \SearchSalesParser::updatePeopleListRetry($this->peopleList['id'], 0);
                return \FALSE;
            } else {
                // Retry
                \SearchSalesParser::updatePeopleListRetry($this->peopleList['id'], 1);
                return \TRUE;
            }
        }
        return \FALSE;
    }

    /**
     * Parse name of Li-User and update it in DB if needed.
     */
    public function check4Name()
    {
        $crawledByNameFromPageNode = $this->finder->query("//img[contains(@class,'nav-item__profile-member-photo')]/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';

        $this->crawledByName = \FirstPageModel::updateLiUserName($this->liUser, $crawledByNameFromPage);

        return \FALSE;
    }

    /**
     * @param $html
     * @param $crawledById
     * @return array|bool
     */
    public static function handleBadPage($html, $crawledById)
    {
        $bpHandler = new BadPageHandler($html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if (!$bphResult) {
            $finder = $bpHandler->getFinder();
            //HTML: No data for search sales page
            $resultsList = $finder->query("//div[@class='search-results-container']")->item(0);
            if ($resultsList === \NULL) {
                $result = [
                    'bp-message' => 'html: div@search-results-container',
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'No needed data on the page. Try again...'
                        ]
                    ]
                ];
                return $result;
            }

            $finder = $bpHandler->getFinder();
            //HTML: No data for search sales page
            $noResults = $finder->query("//div[@class='search-no-results__container']")->item(0);
            if ($noResults !== \NULL) {
                $result = [
                    'bp-message' => 'li: search-no-results',
                    'response' => [
                        'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                        'status_message' => [
                            'status' => 2,
                            'message' => 'Linkedin: No results found'
                        ]
                    ]
                ];
                return $result;
            }

        } else {
            return $bphResult;
        }
        return \FALSE;
    }

    /**
     * Parse total matches on page and update it in DB if needed.
     * @return bool
     */
    public function parseTotalMatches()
    {
        //<h3 class="search-results__total Sans-15px-black-55% pl5 pt4 clear-both">Showing 2,724,882 results</h3>
        $totalMatchesNode = $this->finder->query("//h3[contains(@class,'search-results__total')]")->item(0);
        $totalMatches = $totalMatchesNode !== \NULL ? trim($totalMatchesNode->nodeValue) : \FALSE;
        if ($totalMatches !== \FALSE) {
            preg_match('#Showing ([^ ]*?) results#', $totalMatches, $match);
            if (!empty($match) AND !empty($match[1])) {
                $totalMatches = (int)str_replace(',', '', $match[1]);
                if (!empty($this->searchUrlId) && isset($totalMatches)) {
                    \ParserModel::updateSearchUrlTotalMatches($totalMatches, $this->searchUrlId);
                    $this->totalMatches = $totalMatches;
                    return \TRUE;
                }
            }
        }
        return \FALSE;
    }

    /**
     * Gathering profiles excluding shared connections
     * @return array
     */
    public function collectProfiles($elements = [])
    {
        $profiles = [];
        $profiles4Visiting = [];
        $ids = [];
        $accordance = [];

        //-------new design---------------------
        //@TODO refactor this later
        $idsV2 = [];
        foreach($elements as $element){
            if(isset($element['type']) && $element['type'] == 'SEARCH_HITS'){
                $idsV2 = $element['elements'] ?? [];
                break;
            }
        }

        foreach ($idsV2 as $id){
            if (preg_match('#urn:li:fs_miniProfile:(.*)#i', $id['targetUrn'], $match)) {
                if (!empty($match[1])) {
                    $ids[] = $match[1];
                }
            }
        }
        //------------------end new design---------

        foreach ($this->jsonPageData as $result) {
            if (isset($result['publicIdentifier'])) {
                $profiles[] = $result;
            } elseif (isset($result['distance'])) {
                if(isset($result['id'])){
                    $ids[] = $result['id'];
                }
            }
        }
        foreach ($profiles as $p) {
            $entityUrn = '';
            if (preg_match('#urn:li:fs_miniProfile:(.*)#i', $p['entityUrn'], $match)) {
                if (!empty($match[1])) {
                    $entityUrn = $match[1];
                }
            }
            foreach ($ids as $id) {
                if($entityUrn == $id){
                    $profiles4Visiting[] = $p;
                    break;
                }
            }
        }
        return $profiles4Visiting;
    }

    /**
     * Parse and save data from JSON block on the page. Count and save skipped profiles.
     */
    public function parseJsonData()
    {
        //How it works (Non-documented feature!): http://php.net/manual/en/domxpath.query.php#99760
        $context = $this->finder->query("//div[contains(@class,'search-result__info')]");
        $nameNodes = $this->finder->query("//span[contains(@class,'actor-name')]");
        //We need count all profiles here, including private. There are only dom-visible  profiles just to check if we have at least one on the page.
        $this->peopleOnThisPage = $nameNodes->length;
        $profile = [];
        $jsonResult = \ParserHelper::findCodeOnPageByKey($this->html, 'metadata');
        $elements = [];
        $elements = \ParserHelper::getArrayValueByKey($jsonResult, 'elements');
        $jsonResult = \ParserHelper::getArrayValueByKey($jsonResult, 'included');

        if (empty($jsonResult)) {
            return $this->itIsBadPage('json');
        }
        $this->jsonPageData = $jsonResult;
        $profiles = $this->collectProfiles($elements);
        $skippedCount = 0;
        $duplicateCount = 0;
        $connectedSkippedCount = 0;
        $noPublicSkippedCount = 0;
        $visitedSkippedCount = 0;
        //===
        foreach ($profiles as $profile) {
            if (!isset($profile['publicIdentifier']) OR empty($profile['publicIdentifier'])) {
                continue;
            }
            $profile['publicIdentifier'] = $profile['publicIdentifier'] ?? '';
            $uri = $profile['publicIdentifier'];
            if (!empty($campaignId)) {
                $duplicated = \ParserModel::isPeopleByCompanyCampaignLinkedin($this->companyId, $this->campaignId, $uri);
                if ($duplicated) {
                    $duplicateCount++;
                    continue;
                }
            }

            if ($profile['publicIdentifier'] == 'UNKNOWN') {
                $noPublicSkippedCount++;
                continue;
            }

            $memberId = 0;
            if (preg_match('#urn:li:member:(.*)#i', $profile['objectUrn'], $match)) {
                if (!empty($match[1])) {
                    $memberId = (int)$match[1];
                }
            }

            $userSettingsModel = new \classes\Settings($this->companyId, $this->crawledById);
            if (!$userSettingsModel->getForUser('visiting-disable_visited_skip') && !empty($memberId) AND !empty($this->crawledById)) {
                $visited = \SearchSalesParser::checkProfileVisited($this->crawledById, $memberId);
                if ($visited) {
                    $visitedSkippedCount++;
                    continue;
                }
            }
            if (!empty($this->campaignId && $this->peopleListId && $uri && $this->searchUrlId && $memberId)) {
                // people.premium - There is no info about premium of found profiles in search page or we don't now where it is.
                \ParserModel::insertPeople($this->companyId, $this->campaignId, $this->peopleListId, $uri, $this->searchUrlId, $memberId);
            }
        }//---END
        $skippedCount = $duplicateCount + $connectedSkippedCount + $noPublicSkippedCount + $visitedSkippedCount;
        if ($skippedCount > 0) {
            \SearchSalesParser::updateSearchUrlSkipped($this->searchUrlId, $skippedCount, $duplicateCount, $connectedSkippedCount, $noPublicSkippedCount, $visitedSkippedCount);
        }
        return \FALSE;
    }

    /**
     * Alternative parsing for DOM (but we have only 5 static results, other 5 - only after scroll). Not used yet.
     */
    public function parseDomData()
    {
        //Alternative parsing for DOM (but we have only 5 static results, other 5 - only after scroll)
//        for ($i = 0; $i < $this->peopleOnThisPage; $i++) {
//            $profile["name"] = $nameNodes->item($i) !== \NULL ? trim($nameNodes->item($i)->nodeValue) : '';
//            if ($profile["name"] === 'LinkedIn Member') {
//                //Skip no-public profiles
//                continue;
//            }
//
//            $profile["id"] = $finder->query("//div[contains(@class,'search-result__info')]//a[@data-control-name='search_srp_result']/@href")->item($i);
//            $profile["id"] = $profile["id"] !== \NULL ? trim($profile["id"]->nodeValue) : '';
//            if (preg_match('#\/in\/([^\/]+)\/#', $profile["id"], $match)) {
//                $profile["id"] = $match[1];
//            }
//
//            $profileJson = ParserHelper::getArrayByValue($jsonResult, $profile["id"]);
//            if (preg_match('#urn:li:member:(.*)#i', $profileJson['objectUrn'], $match)) {
//                $profile["member_id"] = (int)$match[1];
//            } else {
//                $profile["member_id"] = 0;
//            }
//
//            $profile["premium"] = $finder->query(".//span[contains(@class,'premium-icon')]", $context->item($i))->item(0) === \NULL ? 0 : 1;
//            if (!empty($campaignId)) {
//                $duplicated = ParserModel::isPeopleByCompanyCampaignLinkedin($companyData['id'], $campaignId, $profile["id"]);
//                if ($duplicated) {
//                    ParserModel::updateSearchUrlDuplicate($searchUrlId);
//                    continue;
//                }
//            }
//            if (isset($companyData["id"]) AND isset($campaignId) AND isset($peopleListId) AND isset($profile["id"]) AND isset($searchUrlId) AND isset($profile["member_id"]) AND isset($profile["premium"])) {
//                $lastInsertId = ParserModel::insertPeople($companyData["id"], $campaignId, $peopleListId, $profile["id"], $searchUrlId, $profile["member_id"], $profile["premium"]);
//            }
//        }
        return \FALSE;
    }
}