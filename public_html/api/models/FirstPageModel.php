<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 16.06.2017
 * Time: 15:13
 */
class FirstPageModel
{
    /**
     * @param $clientHash
     * @param $payload
     * @param $version
     * @param $accountType
     * @return array
     * @throws Exception
     */
    public static function firstPage($clientHash, $payload, $version, $accountType)
    {
        $versionId = APIModel::getClientVersionIdByName($version);

        $company = APIModel::getCompanyByProductKey($clientHash);
        $companyId = (int)$company['id'];

        mb_internal_encoding("UTF-8");
        if (empty($payload->html)) {
//            return APIModel::loggedOffReturn();
            throw new Exception("No html");
//            return [];
        }
        self::handleFrozenAccount($payload->html);
        //Looking for id and name on page
        $crawledByName = self::getCrawledBy($payload->html)['fullName'];
        if(empty($crawledByName)){
            $crawledByName = $payload->username ?? '';
        }
        //NULL or int
        $crawledById = (int)self::getCrawledById($payload->html);
        if(empty($crawledById)){
            $crawledById = $payload->linked_in_id ?? 0;
        }

        if (!empty($payload->url) && !empty($companyId) && !empty($crawledById) && !empty($clientHash) && !empty($crawledByName)) {
            $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $companyId, $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);
        }

        $bpHandler = new api\models\BadPageHandler($payload->html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if($bphResult){
            \ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $bphResult['bp-message'], \APIModel::FIRSTPAGE);
            return array_merge([
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
            ], $bphResult['response']);
        }
        //we have login page or other page with no needed data
        if (!self::getCrawledById($payload->html)) {
            \ParserModel::insertApiBadPages($companyId, 0, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, "No ID on the first page", \APIModel::FIRSTPAGE);
            return [
                'url' => APIModel::LOGIN_URL,
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'status_message' => [
                    'status' => 2,
                    'message' => 'No needed data on the page. Try again...'
                ]
            ];
        }

        $liUser = self::getLiUsersByLinkedinId($companyId, $crawledById);

        //If it's new user - add him (if both values are not empty and we didn't find li-user)
        if (!$liUser) {
            $liUser = FreeParserModel::insertLiUser($companyId, $crawledByName, $crawledById, APIModel::SFW_STATUS_WAITING, $accountType);
            //update company subscription on users count change (from functions.php)
            usersCountUpdate($companyId);
        }

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByName);
        //Update check and update user type
        APIModel::updateUserType($liUser, $accountType);

        return [
            'url' => '',
            'WaitBetweenConnections' => 5,
            'CrawledBy' => $crawledById . "|" . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => (int)$crawledById,
            'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
            'status_message' => [
                'status' => 3,
                'message' => 'Verification is complete'
            ]
        ];
    }

    /**
     * @param $html
     * @return array|bool
     */
    public static function getCrawledBy($html)
    {
        $crawledBy = [];

        $userData = ParserHelper::findCodeOnPageByKey($html, 'plainId');
        if(!empty($userData['included'][0])){
            $crawledBy['firstName'] = $userData['included'][0]['firstName'] ?? '';
            $crawledBy['lastName'] = $userData['included'][0]['lastName'] ?? '';
            $crawledBy['fullName'] = $crawledBy['firstName'] . ' ' . $crawledBy['lastName'];
        }

        if(empty($crawledBy)){
            $dom = new \DOMDocument();
            $dom->validateOnParse = true;
            if (!@$dom->loadHTML($html)) {
                return \FALSE;
//            throw new Exception('Bad HTML');
            }
            $finder = new \DomXPath($dom);
            $nameNode = $finder->query("//a[@data-control-name='identity_welcome_message']")->item(0);
            $name = $nameNode !== \NULL ? trim($nameNode->nodeValue) : '';
            $crawledBy = [];
            $crawledBy['fullName'] = $name;
            $nameArray = explode(" ", $name);
            $crawledBy['firstName'] = $nameArray[0];
            $crawledBy['lastName'] = implode(' ', array_slice($nameArray, 1));
        }

        return $crawledBy;
    }

    /**
     * @param $html
     * @return int|null
     */
    public static function getCrawledById($html)
    {
        //$crawledById - for li_users linked_in_id bigint(12)
        $crawledById = NULL;
        //takes value of 'plainId' as integer
        $crawledById = ParserHelper::findJsonValueOnPageByKey($html, 'plainId');

        if(!$crawledById){
            $dom = new \DOMDocument();
            $dom->validateOnParse = true;
            if (!@$dom->loadHTML($html)) {
                return $crawledById;
            }
            $finder = new \DomXPath($dom);
            $node = $finder->query("//meta[@name='__init']/@content")->item(0);
            $json = $node !== \NULL ? trim($node->nodeValue) : '';

            $data = json_decode($json);

            if(empty($data->lix)){
                return $crawledById;
            }

            foreach ($data->lix as $val){
                if(!empty($val->trackingInfo) && !empty($val->trackingInfo->urn)){
                    $crawledById = str_replace('urn:li:member:', '', $val->trackingInfo->urn);
                }
            }
        }
        return $crawledById;
    }

    /**
     * @param $companyId
     * @param $crawledById
     * @return bool|mixed
     */
    public static function getLiUsersByLinkedinId($companyId, $crawledById)
    {
        $query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND linked_in_id = :linked_in_id");
        $query->execute([
            'company_id' => $companyId,
            'linked_in_id' => $crawledById
        ]);
        if ($query->rowCount() == 0) {
            return false;
        }
        $result = $query->fetch();
        return $result;
    }

    /**
     * @param $html
     * @return array
     */
    public static function handleFrozenAccount($html)
    {
        if (isset($html) and trim($html) == "This account is frozen, contact info@searchquant.net to unfreeze it") {
            return [
                'url' => 'http://searchquant.net/frozen_account.php',
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'CrawledBy' => "Frozen Account|Frozen Account",
                'username' => 'Frozen Account',
                'status_message' => [
                    'status' => 2,
                    'message' => 'This account is frozen. Contact info@searchquant.net'
                ]
            ];
        }
    }

    /**
     * @param array $liUser
     * @param string $crawledBy
     *
     * @return string
     */
    public static function updateLiUserName(array $liUser, string $crawledBy)
    {
        if (empty($crawledBy)) {
            $crawledBy = APIModel::UNDEFINED_USER_NAME;
        }

        if (($liUser['name'] !== $crawledBy && $crawledBy !== APIModel::UNDEFINED_USER_NAME) || ($liUser['name'] === '')) {
            try {
                $query = DB::prep("UPDATE li_users SET name = :name WHERE linked_in_id = :linked_in_id");
                $query->execute([
                    "linked_in_id" => $liUser['linked_in_id'],
                    "name" => $crawledBy
                ]);

            } catch (Exception $e) {
                APIModel::response('error', ['message' => 'Database query failed.']);
            }

        }else{
            $crawledBy = $liUser['name'];
        }

        return $crawledBy;
    }

}