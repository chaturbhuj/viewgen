<?php
/**
 * Created by PhpStorm.
 * User: Mihael
 * Date: 23.01.2018
 * Time: 14:35
 */

namespace api\models;
class GroupsParser
{
    const NO_DATA_MESSAGE = 'No needed data on the page. Try again...';
    const RES_PER_PAGE = 10;
    private static $_instance;
    private $finder = \NULL;
    private $html = \NULL;
    private $crawledById = \NULL;
    private $versionId = \NULL;
    private $clientHash = \NULL;
    private $companyData = \NULL;
    private $liUser = \NULL;
    private $companyId = \NULL;
    private $crawledByName = \NULL;
    private $url = \NULL;
    private $wait = \APIModel::DEFAULT_WAIT;
    private $htmlDumpId = \NULL;
    public $payload = [];
    private $cards = [];
    public $clientParameters = [
        'linked_in_id',
        'html',
        'client_hash'
    ];

    /**
     * @param $payload
     * @param string $version
     * @return GroupsParser
     */
    public static function getInstance($payload, $version = "")
    {
        if (self::$_instance == null) {
            self::$_instance = new self($payload, $version);
        }
        return self::$_instance;
    }

    /**
     * GroupsParser constructor.
     * @param $payload
     * @param string $version
     * @throws \Exception
     */
    public function __construct($payload, $version = "")
    {
        try {
            $this->payload = $payload;
            $this->checkParameters();
            $this->html = $payload->html;
            $this->url = $payload->url;
            $this->versionId = \ParserModel::getClientVersionIdByName($version);
            $this->companyData = \ParserModel::getCompanyByProductKey($payload->client_hash);
            $this->companyId = $this->companyData['id'];
            $this->crawledById = $payload->linked_in_id;
            $this->clientHash = $payload->client_hash;
            $this->liUser = \APIModel::getLiUser($this->companyId, $this->crawledById);
            $this->crawledByName = $payload->username ?? '';
            $this->wait = $this->companyData['wait_between_connections'];
            $this->finder = $this->getFinder();
        } catch (\Exception $e) {
            throw new \Exception("Initialization of GroupsParser failed: " . $e->getMessage());
        }
    }

    /**
     * Check for needed parameters from client
     * @return bool
     */
    public function checkParameters()
    {
        foreach ($this->clientParameters as $par) {
            if (empty($this->payload->$par)) {
                throw new Exception("No $par parameter!");
            }
        }
        return \TRUE;
    }

    /**
     * @return \DomXPath
     */
    public function getFinder()
    {
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($this->html)) {
            throw new Exception("HTML did not loaded!");
        };
        return new \DomXPath($dom);
    }

    /**
     * Parse name of Li-User and update it in DB if needed.
     */
    public function check4Name()
    {
        $crawledByNameFromPageNode = $this->finder->query("//img[contains(@class, 'nav-item__profile-member-photo')]/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';

        $this->crawledByName = \FirstPageModel::updateLiUserName($this->liUser, $crawledByNameFromPage);

        return \FALSE;
    }

    /**
     * @return array
     */
    public function defaultReturn()
    {
        return [
            'VisitedProfiles24' => (int)$this->liUser["visitsLast24Hours"],
            'CrawledBy' => $this->crawledById . "|" . $this->crawledByName,
            'username' => $this->crawledByName,
            'linked_in_id' => $this->crawledById,
            'WaitBetweenConnections' => \ParserModel::randomizeWait($this->wait)
        ];
    }

    /**
     * Main method
     * @return array
     */
    public function check()
    {
        //$this->check4Name();

        $this->htmlDumpId = \ParserModel::saveHtmlDump($this->url, $this->companyId, $this->crawledByName, $this->crawledById, $_SERVER["REMOTE_ADDR"], $this->clientHash, 0, $this->versionId, $this->html);

        //==== CHECK FOR BAD PAGE
        $bpHandler = new BadPageHandler($this->html, $this->crawledById);
        $badPage = $bpHandler->handleBadPage();
        if ($badPage) {
            \ParserModel::insertApiBadPages($this->companyId, $this->crawledById, $this->url ?? '', $this->html, $this->versionId, $this->htmlDumpId ?? \NULL, \NULL, \NULL, \NULL, $badPage['bp-message'], \APIModel::GROUPS_PAGE);
            return array_merge($this->defaultReturn(), $badPage['response']);
        }
        //===

        $this->cards = $this->parsePage();

        $this->saveGroups();
        $this->updateCheckDate();
        \APIModel::updateSfwStatus($this->companyId, $this->crawledById, \APIModel::STATUS_RUNNING);
        return array_merge($this->defaultReturn(), ['groupsCheck' => 0]);
    }

    /**
     * @return bool
     */
    public function updateCheckDate()
    {
        $query = \DB::prep("UPDATE li_users SET last_groups_check = NOW() WHERE linked_in_id = :linked_in_id");
        $query->execute([
            'linked_in_id' => $this->crawledById
        ]);
        if ($query->rowCount() !== 1) {
            return \FALSE;
        }
        return \TRUE;
    }

    /**
     * @return mixed
     */
    public function saveGroups()
    {
        foreach ($this->cards as $data) {
            $this->saveGroup($data);
        }
        $this->saveUserGroups();

        return \FALSE;
    }


    /**
     * @param void
     * @return bool
     * @throws \Exception
     */
    public function saveUserGroups()
    {
        try {
            \DB::get_instance()->beginTransaction();

            $query = \DB::prep("
                DELETE
                FROM
                  li_users_groups
                WHERE
                  user_li_id = ?
            ");
            $query->execute([
                $this->crawledById
            ]);

            $groupsIds = array_keys($this->cards);

            foreach ($groupsIds as $key => $groupsId){
                $groupsIds[$key] = "('".$this->crawledById."', '".$groupsId."')";
            }

            if(count($this->cards) > 0){
                $query = \DB::prep("
                    INSERT INTO
                      li_users_groups (user_li_id, group_li_id)
                    VALUES
                      ".implode(', ', $groupsIds)." 
                ");
                $query->execute();
            }

            \DB::get_instance()->commit();
        } catch (Exception $e) {
            \DB::get_instance()->rollBack();
            throw $e;
        }
        return \FALSE;
    }


    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function saveGroup($data)
    {
        try {
            $query = \DB::prep("
                SELECT
                  *
                FROM
                  li_groups
                WHERE
                  linked_in_id = ?
            ");
            $query->execute([
                $data['id']
            ]);

            if($group = $query->fetch()){
                if($data['name'] !== $group['name'] || $data['image'] !== $group['image']){ //update db only when new data received
                    $query = \DB::prep("
                        UPDATE
                          li_groups
                        SET
                          name = :name,
                          image = :image,
                          updated_at = NOW()
                        WHERE
                          linked_in_id = :id
                    ");
                    $query->execute([
                        'id' => $data['id'],
                        'name' => $data['name'],
                        'image' => $data['image']
                    ]);
                }
            } else {
                $query = \DB::prep("
                    INSERT INTO
                      li_groups (linked_in_id, name, image)
                    VALUES
                      (:id, :name, :image) 
                ");
                $query->execute([
                    'id' => $data['id'],
                    'name' => $data['name'],
                    'image' => $data['image']
                ]);
            }
        } catch (Exception $e) {
            throw $e;
        }
        return \FALSE;
    }

    /**
     * @return array
     */
    public function parsePage()
    {
        $result = [];

        $jsonResult = \ParserHelper::findCodeOnPageByKeyValue($this->html, '$type', 'com.linkedin.voyager.groups.Group');

        if(!is_array($jsonResult) || empty($jsonResult['data']['*elements']) || empty($jsonResult['included'])){
            return $result;
        }

        $groupIds = $jsonResult['data']['*elements'];

        foreach ($jsonResult['included'] as $item){
            if(!isset($item['$type']) || $item['$type'] !== 'com.linkedin.voyager.groups.Group' || !in_array($item['entityUrn'], $groupIds)){
                continue;
            }

            $groupId = str_replace('urn:li:fs_group:', '', $item['entityUrn']);

            if( empty($groupId) ) {
                continue;
            }

            $result[$groupId] = [
                'name' => isset($item['name']['text']) ? trim($item['name']['text']) : '',
                'image' => '', //it makes no sense to parse because images are not accessible for unauthorized in linkedin users
                'id' => trim($groupId)
            ];
        }

        return $result;
    }

    /**
     * @param $linkedinId
     * @return bool
     * @throws \Exception
     */
    public function checkDuplicate($linkedinId)
    {
        try {
            $query = \DB::prep("SELECT COUNT(id) FROM received_invitations WHERE crawled_by_id = :crawled_by_id AND linkedin_id = :linkedin_id");
            $query->execute([
                'crawled_by_id' => $this->crawledById,
                'linkedin_id' => $linkedinId
            ]);
            $count = $query->fetchColumn();
            return $count;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}