<?php
namespace api\models\services;
use classes\Settings;

/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 28.06.18
 * Time: 18:22
 */
class BaseService
{
    const SERVICE_ID = '';
    const SERVICE_RUNTIME_TABLE = 'vg_service_runtime';
    public $user = null;
    public $company = null;
    public $settings = null;
    public $state = [];

    public function __construct($company, $user)
    {
        $this->user = $user;
        $this->company = $company;
        $this->settings = new Settings($company['id'], $user['id']);
        $this->loadState();
    }

    public function __destruct()
    {
        $this->saveState();
    }

    public function loadState(){
        $this->state = array_merge($this->state, $this->getStateFromDB());
    }

    public function saveState(){
        If(!count($this->state)){
            return;
        }

        $existingParams = $this->getStateFromDB();

        foreach($this->state as $key => $value){
            if(!isset($existingParams[$key])){
                $this->insertStateParam($key, $value);
            }elseif($existingParams[$key] != $value){
                $this->updateStateParam($key, $value);
            }
        }
    }

    public function getStateFromDB(){
        $query = \DB::prep("SELECT * FROM ".self::SERVICE_RUNTIME_TABLE." WHERE service_id = :id AND user_id = :user_id");
        $query->execute([
            'id' => static::SERVICE_ID,
            'user_id' => $this->user['id']
        ]);
        $params = $query->fetchAll();

        $result = [];
        foreach($params as $param){
            $result[$param['param_id']] = $param['value'];
        }

        return $result;
    }

    private function updateStateParam($id, $value){
        $query = \DB::prep("
                            UPDATE 
                              ".self::SERVICE_RUNTIME_TABLE."
                            SET
                              `value` = :value
                            WHERE
                              service_id = :id
                              AND user_id = :user_id
                              AND param_id = :param_id
                           ");
        $query->execute([
            'id' => static::SERVICE_ID,
            'user_id' => $this->user['id'],
            'param_id' => $id,
            'value' => $value
        ]);
    }

    private function insertStateParam($id, $value){
        $query = \DB::prep("
                            INSERT INTO 
                              ".self::SERVICE_RUNTIME_TABLE."
                            (param_id, service_id, user_id, `value`)
                            VALUES (:param_id, :service_id, :user_id, :value)
                           ");
        $query->execute([
            'service_id' => static::SERVICE_ID,
            'user_id' => $this->user['id'],
            'param_id' => $id,
            'value' => $value
        ]);
    }

}