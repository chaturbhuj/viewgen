<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 17.06.18
 * Time: 19:31
 */

namespace api\models\services;
use \APIModel;
use \ParserModel;
ini_set('error_reporting', E_ALL);


class Manager extends BaseService
{
    const SERVICE_ID = 'manager';
    const SETTINGS_TABLE = '';

    public function __construct($companyId, $crawledById)
    {
        $query = \DB::prep("SELECT * FROM company WHERE id = :id");
        $query->execute([
            'id' => $companyId
        ]);
        $this->company = $query->fetch();

        $query = \DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND linked_in_id = :crawled_by_id");
        $query->execute([
            'company_id' => $this->company['id'],
            'crawled_by_id' => $crawledById
        ]);
        $this->user = $query->fetch();

        if(!$this->company || !$this->user){
            throw new \Exception('User or Company not found');
        }
        parent::__construct($this->company, $this->user);
    }

    public function getRequest(){
        $service = new Uninviter($this->company, $this->user);

        $result = $service->getRequest();

        if(!empty($result)){
            $result['username'] = $this->user['name'];
            $result['linked_in_id'] = $this->user['linked_in_id'];
            $result['VisitedProfiles24'] = $this->user["visitsLast24Hours"];
            $result['CrawledBy'] = $this->user['linked_in_id'] . '|' . $this->user['name'];
            $result['referer'] = \APIModel::LOGIN_URL;
        }

        return $result;
    }

    public function processResponse($url, $html, $version, $clientHash){
        if(self::getPageType($url) === 'invitation_sent'){
            $service = new Uninviter($this->company, $this->user);

            //TODO move html dump saving to more common place
            $versionId = APIModel::getClientVersionIdByName($version);
            $htmlDumpId = ParserModel::saveHtmlDump($url, $this->company['id'], $this->user['name'], $this->user['linked_in_id'], $_SERVER["REMOTE_ADDR"], $clientHash, 0, $versionId, $html);

            $result = $service->processResponse($html);

            $result['WaitBetweenConnections'] = 10;

            $result['CrawledBy'] = $this->user['linked_in_id'] . '|' . $this->user['name'];
            $result['username'] = $this->user['name'];
            $result['linked_in_id'] = (int)$this->user['linked_in_id'];
            $result['VisitedProfiles24'] = (int)$this->user['visitsLast24Hours'];
            $result['company_name'] = $this->company['company_name'];

            APIModel::updateSfwStatus($this->company['id'], $this->user['linked_in_id'], APIModel::STATUS_RUNNING);

            return $result;
        }

        return false;
    }


    public static function getPageType($url): string
    {
        $path = parse_url($url, PHP_URL_PATH);

        switch ($path){
            case "/mynetwork/invitation-manager/sent/":
                return 'invitation_sent';
            default:
                return '';
        }
    }

}