<?php
namespace api\models\services;
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 28.06.18
 * Time: 17:43
 */
class Uninviter extends BaseService
{
    const SERVICE_ID = 'uninviter';

    public $state = [
        'is_running' => false,
        'current_page' => 1,
    ];

    public $js;


    public function getRequest(){
        $this->js = file_get_contents(__DIR__.'/js/uninviter.js');
        $this->js = str_replace('\'{{uninviter-time_period}}\'', $this->settings->getForUser('uninviter-time_period'), $this->js);

        if($this->state['is_running']){
            return [
                'url' => 'https://www.linkedin.com/mynetwork/invitation-manager/sent/'. ($this->state['current_page'] > 1 ? ("?page=".$this->state['current_page']) : ''),
                'WaitBetweenConnections' => 30,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Uninviter running'
                ],
                'javaScript' => $this->js
            ];
        }
        return false;
    }

    public function processResponse($html){
        $pageData = $this->getPageData($html);
        file_put_contents('../debug.log', var_export($pageData, true), FILE_APPEND|FILE_USE_INCLUDE_PATH);
        $toUninviteCount = 0;
        foreach($pageData['invitations'] as $invitation){
            if($invitation['time'] > $this->settings->getForUser('uninviter-time_period')){
                $toUninviteCount++;
            }
        }


        if($toUninviteCount > 0){
            return;
        }

        if($pageData['pages'] > $this->state['current_page']){
            $this->state['current_page']++;
            return;
        }

        $this->state['is_running'] = false;
        return;
    }


    public function getPageData($html){
        $result = [
            'invitations' => [],
            'pages' => 1,
            'total' => 0
        ];

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);

        $node = $finder->query("//label[@for='contact-select-checkbox']")->item(0);
        $nodeValue = $node !== \NULL ? trim($node->nodeValue) : '';

        $matches = [];
        if(preg_match('/of\s([0-9]+)/', $nodeValue, $matches)){
            $result['total'] = (int) $matches[1];
        }

        $node = $finder->query("//ol[@class='mn-invitation-pagination']/li/a");
        $pageLinksCount = count($node);
        if($pageLinksCount && ($pageLinksCount - 2) > 0){
            $result['pages'] = $pageLinksCount - 2;
        }

        $nodes = $finder->query("//ul[@class='mn-invitation-list']//div[@class='invitation-card__details']");
        foreach ($nodes as $node){
            $link = '';
            $time = null;

            $nodeLink = $finder->query(".//a[contains(@class, 'invitation-card__link')]/@href", $node)->item(0);
            $link = $nodeLink !== \NULL ? trim($nodeLink->nodeValue) : '';

            $nodeTime = $finder->query(".//time[contains(@class, 'time-ago')]", $node)->item(0);
            $time = $nodeTime !== \NULL ? trim($nodeTime->nodeValue) : '';

            if(preg_match('/([0-9]+)\s(s|mi|h|d|w|mo|y)/', $time, $matches)){
                $time = (int) $matches[1];
                $period = $matches[2];

                switch($period){
                    case 's':
                        break;
                    case 'mi':
                        $time *= 60;
                        break;
                    case 'h':
                        $time *= 60*60;
                        break;
                    case 'd':
                        $time *= 60*60*24;
                        break;
                    case 'w':
                        $time *= 60*60*24*7;
                        break;
                    case 'mo':
                        $time *= 60*60*24*7*30;
                        break;
                    case 'y':
                        $time *= 60*60*24*7*30*365;
                        break;
                }
            }

            if(!empty($link)){
                $result['invitations'][] = [
                    'link' => $link,
                    'time' => $time
                ];
            }
        }

        return $result;
    }
}