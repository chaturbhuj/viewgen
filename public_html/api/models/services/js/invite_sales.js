var userFullName = $('.profile-topcard-person-entity__name').text().trim();
var userJSON = '';

var firstSpace = userFullName.indexOf(' ');

var userData = {
    'firstName': (firstSpace > -1) ? userFullName.substring(0, firstSpace) : userFullName,
    'lastName': (firstSpace > -1) ? userFullName.substring(firstSpace + 1) : ''
};

if(userFullName){
    userJSON = $('code:contains("'+userFullName+'")').text();
    if(userJSON){
        userData = JSON.parse(userJSON);
    }
}

var params = {
    "first_name": userData.firstName,
    "last_name": userData.lastName
};

var message = '{{itc_message}}';

for(var key in params){
    message = message.replace(new RegExp('{{'+key+'}}', 'gi'), params[key]);
}

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (!mutation.addedNodes) return;

        for (var i = 0; i < mutation.addedNodes.length; i++) {
            // do things to your newly added nodes here
            var node = mutation.addedNodes[i];
            var jNode = $(node);
            if(jNode.is('textarea#connect-cta-form__invitation')){
                jNode.val(message).trigger('keyup');
                $('div.connect-cta-form__footer-container button.connect-cta-form__send').trigger('click');
                observer.disconnect();
            }
        }
    })
});

observer.observe(document.body, {
    childList: true
    , subtree: true
    , attributes: false
    , characterData: false
});
$('artdeco-dropdown-item[data-control-name="connect"]').trigger('click');
