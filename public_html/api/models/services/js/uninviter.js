$('li.invitation-card--selectable ').each(function () {
    var time, period, checkBox;
    time = $(this).find('time.time-ago').text();
    checkBox = $(this).find('input:checkbox');

    time = time.match(/([0-9]+)\s(s|mi|h|d|w|mo|y)/);

    period = time[2];
    time = time[1];

    switch(period){
        case 's':
            break;
        case 'mi':
            time *= 60;
            break;
        case 'h':
            time *= 60*60;
            break;
        case 'd':
            time *= 60*60*24;
            break;
        case 'w':
            time *= 60*60*24*7;
            break;
        case 'mo':
            time *= 60*60*24*7*30;
            break;
        case 'y':
            time *= 60*60*24*7*30*365;
            break;
    }

    if(time > '{{uninviter-time_period}}'){
        checkBox.prop('checked', true).trigger('change');
    }

});

$('button[data-control-name="withdraw_all"]').trigger('click');