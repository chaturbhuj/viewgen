var userFullName = $('img.pv-top-card-section__photo').attr('alt').trim();

var params = {
    "full_name": userFullName
};

var message = '{{itc_message}}';

for(var key in params){
    message = message.replace(new RegExp('{{'+key+'}}', 'gi'), params[key]);
}

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (!mutation.addedNodes) return;

        for (var i = 0; i < mutation.addedNodes.length; i++) {
            // do things to your newly added nodes here
            var node = mutation.addedNodes[i];
            var jNode = $(node);
            if(jNode.is('button.pv-s-profile-actions--connect')){
                jNode.trigger('click');
            }else if(jNode.is('button:contains("Add a note")')){
                jNode.trigger('click');
            }else if(jNode.is('textarea#custom-message')){
                jNode.val('{{itc_message}}').trigger('keyup');
                $('button:contains("Send invitation")').trigger('click');
                observer.disconnect();
            }
        }
    })
});

observer.observe(document.body, {
    childList: true
    , subtree: true
    , attributes: false
    , characterData: false
});

var startObject = $('button.pv-s-profile-actions--connect');
if(!startObject.length){
    startObject = $('button.pv-s-profile-actions__overflow-toggle');
}
startObject.trigger('click');