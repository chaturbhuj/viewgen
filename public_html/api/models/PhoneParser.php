<?php
/**
 * Created by PhpStorm.
 * User: Mihael
 * Date: 09.05.2018
 * Time: 17:16
 */
require_once(__DIR__ . '/BadPageHandler.php');

use api\models\BadPageHandler;

class PhoneParser
{

    /**
     * @param $company
     * @param $payload
     * @param $version
     * @return array
     * @throws Exception
     */
    public static function checkPhone($company, $payload, $version)
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        $pageData = self::getPhoneFromPage($payload->html);
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $phone = $pageData['phone'] ?? '';
        $crawledByName = !empty($crawledByName) ? $crawledByName : $pageData['name'];

        $liUser = APIModel::getLiUser($company['id'], $crawledById);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByName);

        $wait = $company['wait_between_connections'];

        $versionId = APIModel::getClientVersionIdByName($version);
        $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $company['id'], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);

        if (empty($phone)) {
            //==== CHECK FOR BAD PAGE
            $bpHandler = new api\models\BadPageHandler($payload->html, $crawledById);
            $bphResult = $bpHandler->handleBadPage();
            if ($bphResult) {
                ParserModel::insertApiBadPages($company['id'], $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, \NULL, \NULL, \NULL, $bphResult['bp-message'], \APIModel::EMAIL_PAGE);
                return array_merge([
                    'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                    'CrawledBy' => $crawledById . "|" . $crawledByName,
                    'username' => $crawledByName,
                    'linked_in_id' => (int)$crawledById,
                    'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
                ], $bphResult['response']);
            }
        }

        if ($liUser['user_phone'] !== $phone) {
            self::updatePhone($crawledById, $phone);
        } else {
            self::updatePhone($crawledById);
        }

        APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_RUNNING);
        return [
            'WaitBetweenConnections' => ParserModel::randomizeWait($wait),
            'CrawledBy' => $crawledById . '|' . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => (int)$crawledById,
            'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
            'checkingPhone' => 0
        ];
    }

    /**
     * @param $html
     * @return array|null
     */
    public static function getPhoneFromPage($html)
    {
        $result = [];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);
        $node = $finder->query("//ul[@class='existing-phone-numbers']//span[@class='is-primary']//ancestor::li//p[@class='phone-number']")->item(0);
        $result['phone'] = $node !== \NULL ? trim($node->nodeValue) : '';
        if(empty($result['phone'])){
            $node = $finder->query("//ul[@class='existing-phone-numbers']//p[@class='phone-number']")->item(0);
            $result['phone'] = $node !== \NULL ? trim($node->nodeValue) : '';
        }
        $node = $finder->query("//div[@class='user-title']/h2")->item(0);
        $result['name'] = $node !== \NULL ? trim($node->nodeValue) : '';
        return $result;
    }

    /**
     * @param $crawledById
     * @param bool $phone
     * @return bool
     */
    public static function updatePhone($crawledById, $phone = \FALSE)
    {
        if ($phone === \FALSE) {
            $query = \DB::prep('UPDATE li_users SET last_phone_check = NOW() WHERE linked_in_id = :linked_in_id');
            $query->execute([
                'linked_in_id' => $crawledById
            ]);
        } else {
            $query = \DB::prep('UPDATE li_users SET last_phone_check = NOW(), user_phone = :user_phone WHERE linked_in_id = :linked_in_id');
            $query->execute([
                'linked_in_id' => $crawledById,
                'user_phone' => $phone
            ]);
        }
        if ($query->rowCount() == 0) {
            return \FALSE;
        }
        return \TRUE;
    }

}