<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 20.09.2017
 * Time: 14:20
 */

namespace api\models;
use api\models\services\Manager;
use linkedinSearch\Url;

class GNU
{
    private static $_instance = \NULL;
    private $payload = \NULL;
    private $crawledById = \NULL;
//    private $clientHash = \NULL;
    private $companyData = \NULL;
    private $campaignData = \NULL;
    private $liUser = \NULL;
    private $companyId = \NULL;
    private $crawledByName = \NULL;
    private $searchUrlOrder = \NULL;
    private $url2Visit = \NULL;
    private $campaignId = \NULL;
    private $userSettingsModel = \NULL;
    public $clientParameters = [
        'CrawledBy',
        'client_hash'
    ];

    public $baseResult;

    /**
     * @param $payload
     * @return GNU|null
     */
    public static function getInstance($payload)
    {
        if (self::$_instance == null) {
            self::$_instance = new self($payload);
        }
        return self::$_instance;
    }

    /**
     * GNU constructor.
     * @param $payload
     * @throws \Exception
     */
    public function __construct($payload)
    {
        try {
            $this->payload = $payload;
            $this->checkParameters();
            $this->companyData = \APIModel::getCompanyByProductKey($this->payload->client_hash);
            $this->companyId = $this->companyData['id'];
            $this->wait = $this->companyData['wait_between_connections'];
            list($this->crawledById, $this->crawledByName) = \APIModel::explodeCrawledBy($this->payload->CrawledBy);
            if (empty($this->crawledById)) {
                throw new \Exception('No ID');
            }
            $this->liUser = \APIModel::getLiUserWithOneHourAgo($this->companyId, $this->crawledById);
            if (empty($crawledByName)) {
                $this->crawledByName = $this->liUser['name'];
            }
            $this->searchUrlOrder = $this->companyData['company_runs_query_slicer'] == 1 ? \TRUE : \FALSE;

            $this->userSettingsModel = new \classes\Settings($this->companyId, $this->crawledById);


            $this->baseResult = [
                'referer' => \APIModel::LOGIN_URL,
                'WaitBetweenConnections' => \ParserModel::randomizeWait($this->wait),
                'username' => $this->crawledByName,
                'linked_in_id' => $this->crawledById,
                'VisitedProfiles24' => $this->liUser["visitsLast24Hours"],
                'CrawledBy' => $this->crawledById . "|" . $this->crawledByName,
            ];
        } catch (\Exception $e) {
            throw new \Exception("Initialization of GNU failed: " . $e->getMessage());
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function checkParameters()
    {
        foreach ($this->clientParameters as $par) {
            if (empty($this->payload->$par)) {
                throw new \Exception("No $par parameter!");
            }
        }
        return \TRUE;
    }

    /**
     * @return array|bool
     */
    public function getNextUrl()
    {
        //VG account is frozen
        if ($this->companyData["frozen"]) {
            \APIModel::updateSfwStatus($this->companyId, $this->crawledById, 'waiting');
            return $this->responseVGFrozen();
        }

        //is_onboarding means that someone has created a sales navigator account but not filled answered all LinkedIn's question for them.
        if ($this->liUser["is_onboarding"]) {
            return $this->responseOnBoarding();
        }

        //Privacy check
        $privacyCheck = $this->privacyCheck();
        if ($privacyCheck) {
            return $privacyCheck;
        }

        //Privacy sales check
        $privacySalesCheck = $this->privacySalesCheck();
        if ($privacySalesCheck) {
            return $privacySalesCheck;
        }

        //Subscriptions check
        $subsCheck = $this->subsCheck();
        if ($subsCheck) {
            return $subsCheck;
        }

        //If we haven't been getting multiple sales page for last 3 days
        if ($this->liUser['multiple_sales'] == 1 AND $this->liUser['last_update_multiple_sales'] < date('Y-m-d', strtotime('-3 days', strtotime($this->liUser['last_connection']))) . " 00:00:00") {
            \api\models\BadPageHandler::updateMultipleSales($this->crawledById, 0);
        }

        //Email check
        $emailCheck = $this->emailCheck();
        if ($emailCheck) {
            return $emailCheck;
        }

        //Email check
        $phoneCheck = $this->phoneCheck();
        if ($phoneCheck) {
            return $phoneCheck;
        }

        //Invitations check
        $invitCheck = $this->invitCheck();
        if ($invitCheck) {
            return $invitCheck;
        }

        //Groups check
        $groupsCheck = $this->groupsCheck();
        if ($groupsCheck) {
            return $groupsCheck;
        }

        $inboundViewsCheck = $this->inboundViewsCheck();
        if ($inboundViewsCheck) {
            return $inboundViewsCheck;
        }

        $visitsLast24Hours = $this->visitsLast24Hours();
        if ($visitsLast24Hours) {
            return $visitsLast24Hours;
        }

        $services = (new Manager($this->companyId, $this->crawledById))->getRequest();
        if($services){
            return $services;
        }

        //Getting and handling next URL
        $this->url2Visit = $this->getUrl2Visit();
        if ($this->url2Visit) {
            //Is URL correct?
            $checkUrl = $this->checkSearchUrl();
            if ($checkUrl) {
                return $checkUrl;
            }
            //Check for profiles we need to visit
            $profileVisiting = $this->profileVisiting();
            if ($profileVisiting) {
                return $profileVisiting;
            }
            //Check for search URLs for retry visit
            $retryVisit = $this->retryVisitingSearchUrl();
            if ($retryVisit) {
                return $retryVisit;
            }
            //Get new people list (page) of search url
            $nextPage = $this->getNextPageUrl();
            if ($nextPage) {
                return $nextPage;
            }
        }
        \APIModel::updateSfwStatus($this->companyId, $this->crawledById, 'waiting');
        return [];
    }

    /**
     * @return array
     */
    public function responseVGFrozen()
    {
        return array_merge($this->baseResult, [
            'url' => 'http://viewgentools.com/frozen_account.php',
            'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
            'status_message' => [
                'status' => 2,
                'message' => 'Account is frozen, please contact support'
            ]
        ]);
    }

    /**
     * @return array
     */
    public function responseOnBoarding()
    {
        return array_merge($this->baseResult, [
            'url' => '',
            'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
            'status_message' => [
                'status' => 2,
                'message' => 'ATTENTION! Please complete your Sales Navigator setup'
            ]
        ]);
    }

    /**
     * @return array|bool
     */
    public function privacyCheck()
    {
        if ($this->liUser["lastAnonymusCheck"] < date('Y-m-d', strtotime('-1 days', time())) . " 00:00:00" OR $this->liUser['anonymous'] !== \PrivacyModel::FULL) {
            return array_merge($this->baseResult, [
                'url' => 'https://www.linkedin.com/psettings/profile-visibility',
                'checkingPrivacySettings' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Privacy Settings'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function privacySalesCheck()
    {
        if ($this->liUser["lastAnonymusSalesCheck"] < date('Y-m-d', strtotime('-1 days', time())) . " 00:00:00" OR ($this->liUser['anonymousSales'] !== \PrivacyModel::FULL AND $this->liUser['anonymousSales'] !== \PrivacyModel::UNKNOWN)) {
            return array_merge($this->baseResult, [
                'url' => 'https://www.linkedin.com/sales/settings?trk=nav_user_menu_manage_sales_nav',
                'checkingPrivacySettings' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Sales Navigator Privacy Settings'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function subsCheck()
    {
        return \FALSE; //disabled, temp fix
        if (\SubsParser::isUserSubsSupported($this->liUser) === false OR $this->liUser["lastSubscriptionsCheck"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
            return array_merge($this->baseResult, [
                'url' => 'https://www.linkedin.com/premium/manage',
                'checkingSubscriptions' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking LI Subscription'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function emailCheck()
    {
        if ($this->liUser["last_email_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
            return array_merge($this->baseResult, [
                'url' => \APIModel::EMAIL_URL,
                'checkingEmail' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Email'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function phoneCheck()
    {
        if ($this->liUser["last_phone_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
            return array_merge($this->baseResult, [
                'url' => \APIModel::PHONE_URL,
                'checkingPhone' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Phone'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function invitCheck()
    {
        if ($this->liUser["last_invitations_check"] < date('Y-m-d', strtotime('-1 days', time())) . " 00:00:00") {
            $result = array_merge($this->baseResult, [
                'url' => \APIModel::INVIT_URL,
                'invitCheck' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Invitations'
                ]
            ]);

            if($this->userSettingsModel->getForUser('invite-auto_accept')){
                $result['javaScript'] = 'if (document.querySelector("#contact-select-checkbox"))
    document.querySelector("#contact-select-checkbox").click();
if (document.querySelector("[data-control-name=\'accept_all\']"))
    document.querySelector("[data-control-name=\'accept_all\']").click();';
            }

            return $result;
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function groupsCheck()
    {
        if ($this->liUser["last_groups_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
            return array_merge($this->baseResult, [
                'url' => \APIModel::GROUPS_URL,
                'groupsCheck' => 1,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Groups'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function inboundViewsCheck()
    {
        if ($this->liUser["last_inbound_check"] === null || $this->liUser["last_inbound_check"] < $this->liUser["halfHourAgo"]) {
            \APIModel::updateSfwStatus($this->companyId, $this->crawledById, 'running');

            return [
                'url' => 'https://www.linkedin.com/me/profile-views/',
                'referer' => 'https://www.linkedin.com/',
                'WaitBetweenConnections' => 30,
                'inbound_visits_url' => 1,
                'latest_li_user_id_inbound_crawled_by' => $this->crawledById . "|" . $this->crawledByName,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Checking Inbound View Backs'
                ]
            ];
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function visitsLast24Hours()
    {
        //Here we check if the rolling 24 hour limit has been reached and if it has we stop
        if ($this->liUser["limit_reached_of"] >= $this->liUser["max_visits_per_day"] and $this->liUser["limit_reached_at"] >= $this->liUser["oneHourAgo"]) {
            \APIModel::updateSfwStatus($this->companyId, $this->crawledById, 'waiting');
            return array_merge($this->baseResult, [
                'url' => '',
                'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                'status_message' => [
                    'status' => 2,
                    'message' => sprintf('24 hour limit of %d visits reached', $this->liUser["limit_reached_of"])
                ]
            ]);
        }
        //here we select the number of profiles that was visited the last 24 hours and determine if should stop here and set the user to have reached his limit
        $peopleCount = \APIModel::getNumberOfVisitedProfiles($this->crawledById);
        $should_stop = false;
        if ($this->liUser === false) {
            $max = $this->companyData['after_n_visits'];
            if ($peopleCount > $this->companyData['after_n_visits']) {
                $should_stop = true;
            }
        } else {
            $max = $this->liUser['max_visits_per_day'];
            if ($peopleCount > $this->liUser['max_visits_per_day']) {
                $should_stop = true;
            }
        }
        if ($max <= $peopleCount + 1) {
            \APIModel::updateLimitReached($this->companyId, $this->crawledById, $max);
        }
        if ($should_stop) {
            return array_merge($this->baseResult, [
                'url' => '',
                'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                'status_message' => [
                    'status' => 2,
                    'message' => sprintf('24 hour limit of %d visits reached', $max)
                ]
            ]);
        }
        return \FALSE;
    }

    public function prepareNewSearchResult(){
        $result = array_merge($this->baseResult, [
            'campaign_id' => $this->campaignId,
            'status_message' => [
                'status' => 3,
                'message' => 'Running Campaign "' . $this->campaignData['name'] . '": Building Profile List'
            ]
        ]);
        if(!empty($this->liUser['search_design_version']) && $this->liUser['search_design_version'] == 2){
            $result['javaScript'] = 'window.scrollTo(0,document.body.scrollHeight);
            function sleep(milliseconds) {
              var start = new Date().getTime();
              for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                  break;
                }
              }
            }
            sleep(10000);
            console.log(new Date());
            ';
        }

        return $result;
    }

    /**
     * @return array|bool
     */
    public function getNextPageUrl()
    {
        //Get existing people lists of search url
        $subData = \APIModel::getMorePeople2Visit($this->url2Visit['id']);

        $result = $this->prepareNewSearchResult();

        if ($subData) {
            //we found a previous page so we go to the next page
            $newPageNumber = $subData["page_nr"] + 1;
            $peopleListUrl = $this->url2Visit['url'];

            if(!empty($this->liUser['search_design_version'])){
                if (!strpos($peopleListUrl, "page=")) {
                    $peopleListUrl = str_replace("?", "?page=1&", $peopleListUrl);
                }
                $newUrl = preg_replace("#page=[^&]*#", 'page=' . $subData["page_nr"], $peopleListUrl);
            }else{
                if (!strpos($peopleListUrl, "start=")) {
                    $peopleListUrl = str_replace("?", "?count=25&start=0&", $peopleListUrl);
                }
                $newUrl = preg_replace("#&start=[^&]*#", '&start=' . $subData["page_nr"] * 25, $peopleListUrl);
            }

            $insertedPeopleListId = \APIModel::insertPeopleList($this->url2Visit['id'], $newPageNumber, mysql_real_escape_string($newUrl));

            $result['people_list_id'] = $insertedPeopleListId;
            $result['referer'] = str_replace(" ", "%20", $subData["url"]);
            $result['url'] = str_replace(" ", "%20", $newUrl);
        } else {
            //If we didn't find any people_list of this search_url
            if(!empty($this->liUser['search_design_version'])) {
                $url = $this->url2Visit['url'];
            }else{
                $url = preg_replace("#rsid=([0-9]+)[0-9]{13}#", 'rsid=${1}' . round(microtime(true) * 1000), $this->url2Visit['url']);
            }
            $insertedPeopleListId = \APIModel::insertPeopleList($this->url2Visit['id'], 1, mysql_real_escape_string($url));

            $result['people_list_id'] = $insertedPeopleListId;
            $result['url'] = str_replace(" ", "%20", trim(str_replace("\n", "", str_replace("\r", "", $this->url2Visit['url']))));
        }
        return $result;
    }

    /**
     * @return array
     */
    public function retryVisitingSearchUrl()
    {
        //here we see if there is a search result page (people_list) that we should retry to visit (perhaps there was something temporarily wrong with LinkedIn)
        $retryList = \APIModel::getPeopleListBySearchUrlId($this->url2Visit['id']);
        if ($retryList) {
            \APIModel::updatePeopleListRetry($retryList['id']);

            $result = $this->prepareNewSearchResult();

            $result['people_list_id'] = $retryList['id'];
            $result['url'] = str_replace(" ", "%20", trim(str_replace("\n", "", str_replace("\r", "", Url::convertUrl($retryList['url'], $this->liUser)))));

            return $result;
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function profileVisiting()
    {
        $peopleData = \APIModel::getPeopleFromUrl($this->companyId, $this->url2Visit['id']);
        if ($peopleData) {
            \APIModel::updateCompanyLastVisit($this->companyId);
            \APIModel::updateCampaignVisit($peopleData["campaign_id"]);
            \APIModel::updateSearchUrlVisit($peopleData["search_url_id"]);
            \APIModel::updatePeopleCrawler($peopleData["people_id"]);
            //WHY??
//            $p = explode("'", $peopleData["linked_in_id"]);
//            $profileLinkedInId = $p[0];
            $result = array_merge($this->baseResult, [
                'people_id' => $peopleData["people_id"],
                'campaign_id' => $this->campaignId,
                'referer' => $peopleData["referer"],
                'url' => 'https://www.linkedin.com/sales/'.(!empty($this->liUser['search_design_version']) ? 'profile' : 'profile').'/' . $peopleData["linked_in_id"],
                'status_message' => [
                    'status' => 3,
                    'message' => 'Running Campaign "' . $this->campaignData['name'] . '": Visiting Profiles'
                ]
            ]);

            if($this->campaignData['itc'] && $this->userSettingsModel->getForUser('itc-enabled')){
                \APIModel::setPeopleInvite($peopleData["people_id"]);

                $js = file_get_contents(__DIR__.'/services/js/invite_sales.js');
                $js = str_replace("'{{itc_message}}'", json_encode($this->campaignData['itc_message']), $js);
                $result['javaScript'] = $js;
            }else{
                $result['javaScript'] = 'window.scrollTo(0,document.body.scrollHeight);
                    function sleep(milliseconds) {
                      var start = new Date().getTime();
                      for (var i = 0; i < 1e7; i++) {
                        if ((new Date().getTime() - start) > milliseconds){
                          break;
                        }
                      }
                    }
                    sleep(10000);
                    console.log(new Date());
                    ';
            }

            return $result;
        }
        return \FALSE;
    }

    /**
     * @return array|bool
     */
    public function checkSearchUrl()
    {
        if (strpos($this->url2Visit["url"], \APIModel::SN_SEARCH_URL) !== 0 AND strpos($this->url2Visit["url"], \APIModel::FREE_SEARCH_URL) !== 0) {
            return array_merge($this->baseResult, [
                'url' => '',
                'WaitBetweenConnections' => \APIModel::DEFAULT_WAIT,
                'status_message' => [
                    'status' => 2,
                    'message' => 'The active campaign has unsupported URL. Please rebuild your campaign.'
                ]
            ]);
        }
        return \FALSE;
    }

    /**
     * @return array|bool|null
     * @throws \Exception
     */
    public function getUrl2Visit()
    {
        $url2Visit = \NULL;
        $this->campaignData = \APIModel::getPublishedCampaign($this->companyId, $this->crawledById);
        if ($this->campaignData === \FALSE) {
            \APIModel::updateSfwStatus($this->companyId, $this->crawledById, 'waiting');
            throw new \Exception('no_campaign');
        } else {
            $this->campaignId = $this->campaignData['id'];
            \APIModel::updateLiUserCurrentCampaignAndSfwStatus($this->companyId, $this->crawledById, 'running', $this->campaignId);
        }
        //Here we find an active URL that belongs to the campaign where we should do the visiting
        if ($this->searchUrlOrder AND $this->campaignId) {
            $url2Visit = \APIModel::getUrl2Visit($this->companyId, $this->crawledById, $this->campaignId, 'total_matches ASC');
        } elseif ($this->campaignId) {
            $url2Visit = \APIModel::getUrl2Visit($this->companyId, $this->crawledById, $this->campaignId, 'id DESC');
        }

        if($url2Visit){
            $url2Visit['url'] = Url::convertUrl($url2Visit['url'], $this->liUser);
        }

        return $url2Visit;
    }
}