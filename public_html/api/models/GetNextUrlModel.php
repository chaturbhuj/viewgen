<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 09.06.2017
 * Time: 10:51
 */
require_once(dirname(__FILE__) . "/../APIModel.php");
use api\models\services\Manager;

//Only for FREE users
class GetNextUrlModel extends APIModel
{

    /**
     * Called from APIModel::generatePayload4Probe()
     * @param $clientHash
     * @param $crawledBy
     * @param $payload
     * @param string $version
     * @param bool $pickUrlWithFewestHitsFirst
     * @param $liUserType
     * @return array
     * @throws Exception
     */
    public static function run($clientHash, $crawledBy, $payload, $version = "", $pickUrlWithFewestHitsFirst = false, $liUserType)
    {
        $baseSearchUrl = $liUserType === APIModel::SALESNAV ? APIModel::SN_SEARCH_URL : APIModel::FREE_SEARCH_URL;
        $baseProfileUrl = $liUserType === APIModel::SALESNAV ? APIModel::SN_PROFILE_URL : APIModel::FREE_PROFILE_URL;
        //First we use the client has to make sure we have the right account/company
        $companyData = parent::getCompanyByProductKey($clientHash);
        $companyId = $companyData['id'];
        $wait = $companyData['wait_between_connections'];

        //here we make sure that the client know who we are signed in as, if the client did not send that information, then we instruct the client to go to LinkedIn's main page
        list($crawledById, $crawledByName) = parent::explodeCrawledBy($crawledBy);
        if (empty($crawledByName) or empty($crawledById)) {
            return [
                'url' => 'https://www.linkedin.com/',
                'referer' => ''
            ];
        }
        $userSettingsModel = new \classes\Settings($companyId, $crawledById);

        //if an account has been frozen (for not paying) we make sure that they are redirected to a page where they are instructed to contact support
        if ($companyData["frozen"]) {
            parent::updateSfwStatus($companyId, $crawledById, 'waiting');
            return [
                'url' => 'http://viewgentools.com/frozen_account.php',
                'username' => $crawledByName ?? '',
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'status_message' => [
                    'status' => 2,
                    'message' => 'Account is frozen, please contact support'
                ]
            ];
        }

        $liUser = parent::getLiUserWithOneHourAgo($companyId, $crawledById);

        if ($liUser) {
            //is_onboarding means that someone has created a sales navigator account but not filled answered all LinkedIn's question for them, they have to sign in to LinkedIn and answer those questions.
            if ($liUser["is_onboarding"]) {
                return [
                    'url' => '',
                    'WaitBetweenConnections' => 60,
                    'CrawledBy' => $crawledById . "|" . $crawledByName,
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'ATTENTION! Please complete your Sales Navigator setup'
                    ]
                ];
            }
            //Privacy check
            if (($liUser["lastAnonymusCheck"] < date('Y-m-d', strtotime('-1 days', time())) . " 00:00:00") OR $liUser['anonymous'] !== PrivacyModel::FULL) {
                return [
                    'url' => 'https://www.linkedin.com/psettings/profile-visibility',
                    'referer' => APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => 10,
                    'checkingPrivacySettings' => 1,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Privacy Settings'
                    ]
                ];
            }

            if ($liUser["last_email_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
                return [
                    'url' => \APIModel::EMAIL_URL,
                    'referer' => \APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => 10,
                    'username' => $crawledByName,
                    'linked_in_id' => $crawledById,
                    'VisitedProfiles24' => $liUser["visitsLast24Hours"],
                    'checkingEmail' => 1,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Email'
                    ]
                ];
            }

            if ($liUser["last_phone_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
                return [
                    'url' => \APIModel::PHONE_URL,
                    'referer' => \APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => 10,
                    'username' => $crawledByName,
                    'linked_in_id' => $crawledById,
                    'VisitedProfiles24' => $liUser["visitsLast24Hours"],
                    'checkingPhone' => 1,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Phone'
                    ]
                ];
            }


            //Subscriptions check
            //disabled, temp fix
//            if (SubsParser::isUserSubsSupported($liUser) === false OR $liUser["lastSubscriptionsCheck"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
//                return [
//                    'url' => 'https://www.linkedin.com/premium/manage',
//                    'referer' => APIModel::LOGIN_URL,
//                    'WaitBetweenConnections' => 10,
//                    'checkingSubscriptions' => 1,
//                    'status_message' => [
//                        'status' => 3,
//                        'message' => 'Checking LI Subscription'
//                    ]
//                ];
//            }

            //Invitations check
            if ($liUser["last_invitations_check"] < date('Y-m-d', strtotime('-1 days', time())) . " 00:00:00") {
                $result = [
                    'url' => APIModel::INVIT_URL,
                    'referer' => APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => ParserModel::randomizeWait($wait),
                    'username' => $crawledByName,
                    'linked_in_id' => $crawledById,
                    'VisitedProfiles24' => $liUser["visitsLast24Hours"],
                    'invitCheck' => 1,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Invitations'
                    ]
                ];

                if($userSettingsModel->getForUser('invite-auto_accept')){
                    $result['javaScript'] = 'if (document.querySelector("#contact-select-checkbox"))
    document.querySelector("#contact-select-checkbox").click();
if (document.querySelector("[data-control-name=\'accept_all\']"))
    document.querySelector("[data-control-name=\'accept_all\']").click();';
                }
                return $result;
            }

            $services = (new Manager($companyId, $crawledById))->getRequest();
            if($services){
                return $services;
            }

            if ($liUser["last_groups_check"] < date('Y-m-d', strtotime('-3 days', time())) . " 00:00:00") {
                return [
                    'url' => \APIModel::GROUPS_URL,
                    'referer' => \APIModel::LOGIN_URL,
                    'WaitBetweenConnections' => \ParserModel::randomizeWait($wait),
                    'username' => $crawledByName,
                    'linked_in_id' => $crawledById,
                    'VisitedProfiles24' => $liUser["visitsLast24Hours"],
                    'groupsCheck' => 1,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Groups'
                    ]
                ];
            }

            //Inbound views check for SalesNAv
            //disabled due to not working wvmpUpdate page
//            if ($liUser['have_sales_navigator'] == 1) {
//                // Here we tell the client to directly to a json page where we can see who has viewed this persons profile.
//                if ($liUser["last_inbound_check"] === null || $liUser["last_inbound_check"] < $liUser["oneHourAgo"]) {
//                    self::updateSfwStatus($companyId, $crawledById, 'running');
//                    //if on a previous request, we had already started the visiting we get the new start position here to continue finding more people to visit
//                    $pageStart = 0;
//                    if (isset($payload->inbound_visit_page) and $payload->inbound_visit_page >= 0) {
//                        $pageStart = $payload->inbound_visit_page + InboundSalesParser::INBOUND_VIEWS_PER_PAGE;
//                    }
//                    return [
//                        'url' => sprintf('https://www.linkedin.com/sales/wvmpUpdate?start=%d&_=%d', $pageStart, rand()),
//                        'referer' => 'https://www.linkedin.com/',
//                        'WaitBetweenConnections' => 30,
//                        'inbound_visits_url' => 1,
//                        'inbound_visit_page' => $pageStart,
//                        'username' => $crawledByName ?? '',
//                        'linked_in_id' => (int)$crawledById,
//                        'latest_li_user_id_inbound_crawled_by' => $crawledById . "|" . $crawledByName,
//                        'status_message' => [
//                            'status' => 3,
//                            'message' => 'Checking Inbound View Backs'
//                        ]
//                    ];
//                }
//            }
            //inbound views for FREE user
            if ($liUser["last_inbound_check"] === null || $liUser["last_inbound_check"] < $liUser["halfHourAgo"]) {
                APIModel::updateSfwStatus($companyId, $crawledById, 'running');
                return [
                    'url' => 'https://www.linkedin.com/me/profile-views/',
                    'referer' => 'https://www.linkedin.com/',
                    'WaitBetweenConnections' => 30,
                    'inbound_visits_url' => 1,
                    'latest_li_user_id_inbound_crawled_by' => $crawledById . "|" . $crawledByName,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Checking Inbound View Backs'
                    ]
                ];
            }
            //Here we check if the rolling 24 hour limit has been reached and if it has we stop
            if ($liUser["limit_reached_of"] >= $liUser["max_visits_per_day"] and $liUser["limit_reached_at"] >= $liUser["oneHourAgo"]) {
                parent::updateSfwStatus($companyId, $crawledById, 'waiting');
                return [
                    'url' => '',
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                    'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => sprintf('24 hour limit of %d visits reached', $liUser["limit_reached_of"])
                    ]
                ];
            }
        }

        //here we check for an active campaign and if we don't find one we exit here
        $isActiveCampaign = APIModel::isActiveCampaign($companyId, $crawledById);
        if ($isActiveCampaign === false) {
            APIModel::updateSfwStatus($companyId, $crawledById, 'waiting');
            throw new Exception('no_campaign');
        }

        //here we update li_user's flag of last connection. So that we can see when they were last active and visiting
        APIModel::updateLastConnection($companyId, $crawledById);

        if ($liUser['have_sales_navigator'] != 1) {
            // Fetch the latest campaign.
            $campaign = APIModel::getPublishedCampaign($companyId, $crawledById);
            // Check if we have a search url where we did not check for visits. (total_matches < 0)
            $searchUrl = APIModel::getSearchUrlUnchecked4Visits($campaign['id']);
            if ($searchUrl) {
                $url = $searchUrl['url'];
                if (strpos($url, "page=") === false) {
                    $url = str_replace("?", "?page=1&", $url);
                }
                // Insert a people list.
                $peopleListId = APIModel::insertPeopleList($searchUrl['id'], 1, $url);

                return [
                    'url' => $url,
                    'people_list_id' => $peopleListId,
                    'campaign_id' => $campaign['id'],
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'WaitBetweenConnections' => 10,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Running Campaign "'.$campaign['name'].'": Building Profile List'
                    ]
                ];
            }
        }

        //here we select the number of profiles that was visited the last 24 hours and determine if should stop here and set the user to have reached his limit
        $peopleCount = APIModel::getNumberOfVisitedProfiles($crawledById);

        $liUser = APIModel::getLiUser($companyId, $crawledById);

        $should_stop = false;

        if ($liUser === false) {
            $max = $companyData['after_n_visits'];
            if ($peopleCount > $companyData['after_n_visits']) {
                $should_stop = true;
            }
        } else {
            $max = $liUser['max_visits_per_day'];
            if ($peopleCount > $liUser['max_visits_per_day']) {
                $should_stop = true;
            }
        }

        if ($max <= $peopleCount + 1) {
            APIModel::updateLimitReached($companyId, $crawledById, $max);
        }

        if ($should_stop) {
            return [
                'url' => '',
                'username' => $crawledByName ?? '',
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                'status_message' => [
                    'status' => 2,
                    'message' => sprintf('24 hour limit of %d visits reached', $max)
                ]
            ];
        }

        //Here we find the campaign that we should do the visiting in (the previous similar request was just a quick check to exit quickly)
        $publishedCampaign = APIModel::getPublishedCampaign($companyId, $crawledById);
        if ($publishedCampaign === false) {
            APIModel::updateSfwStatus($companyId, $crawledById, 'waiting');
            throw new Exception('no_campaign');
        } else {
            $campaignId = $publishedCampaign['id'];
            APIModel::updateLiUserCurrentCampaignAndSfwStatus($companyId, $crawledById, 'running', $campaignId);
        }

        //Here we find an active URL that belongs to the campaign where we should do the visiting
        if ($pickUrlWithFewestHitsFirst) {
            $url2Visit = APIModel::getUrl2Visit($companyId, $crawledById, $campaignId, 'total_matches ASC');
        } else {
            $url2Visit = APIModel::getUrl2Visit($companyId, $crawledById, $campaignId, 'id DESC');
        }

        if ($url2Visit) {
            //this is a check to see if we have found a URL with the old URL style, if we have we send out a message that they have to rebuild their campaigns
            //add check for SN too when we will create universal GNU method
            if (strpos($url2Visit["url"], APIModel::FREE_SEARCH_URL) !== 0) {
                return [
                    'url' => '',
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'WaitBetweenConnections' => parent::DEFAULT_WAIT,
                    'status_message' => [
                        'status' => 2,
                        'message' => 'The active campaign has unsupported URL. Please rebuild your campaign.'
                    ]
                ];
            }

            //here we select a profile (people) that comes from the URL and campaign we selected above
            $peopleData = parent::getPeopleFromUrl($companyId, $url2Visit['id']);
            if ($peopleData) {
                //here we update timestamps and counters on company, campaign, search_url and people to know when we last did any visiting and how many people have been visited for a campaign or url
                parent::updateCompanyLastVisit($companyId);

                parent::updateCampaignVisit($peopleData["campaign_id"]);

                parent::updateSearchUrlVisit($peopleData["search_url_id"]);

                parent::updatePeopleCrawler($peopleData["people_id"]);


                $p = explode("'", $peopleData["linked_in_id"]);
                $profileLinkedInId = $p[0];

                $result = [
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'people_id' => $peopleData["people_id"],
                    'campaign_id' => $campaignId,
                    'referer' => $peopleData["referer"],
                    'url' => $baseProfileUrl . $profileLinkedInId,
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Running Campaign "'.$publishedCampaign['name'].'": Visiting Profiles'
                    ]
                ];

                if($publishedCampaign['itc'] && $userSettingsModel->getForUser('itc-enabled')){
                    parent::setPeopleInvite($peopleData["people_id"]);

                    $js = file_get_contents(__DIR__.'/services/js/invite.js');
                    $js = str_replace("'{{itc_message}}'", json_encode($publishedCampaign['itc_message']), $js);
                    $result['javaScript'] = $js;
                }

                return $result;
            }

            //here we see if there is a search result page (people_list) that we should retry to visit (perhaps there was something temporarily wrong with LinkedIn)
            $retryList = parent::getPeopleListBySearchUrlId($url2Visit['id']);
            if ($retryList) {
                parent::updatePeopleListRetry($retryList['id']);
                return [
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'people_list_id' => $retryList['id'],
                    'campaign_id' => $campaignId,
                    'url' => str_replace(" ", "%20", trim(str_replace("\n", "", str_replace("\r", "", $retryList["url"])))),
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Running Campaign "'.$publishedCampaign['name'].'": Building Profile List'
                    ]
                ];
            }

            //here we go to the next page of the search results to find more profiles (people) to visit
            $subData = parent::getMorePeople2Visit($url2Visit['id']);
            if ($subData) {
                //we found a previous page so we go to the next page
                $newPageNumber = $subData["page_nr"] + 1;
                //SN: count=25&start=100 FREE: page=5
                if (!strpos($url2Visit['url'], "page=")) {
                    $url2Visit['url'] = str_replace("?", "?page=1&", $url2Visit['url']);
                }
                $newUrl = preg_replace("#page=[^&]*#", 'page=' . $newPageNumber, $url2Visit['url']);

                $insertedPeopleListId = parent::insertPeopleList($url2Visit['id'], $newPageNumber, mysql_real_escape_string($newUrl));

                return [
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'people_list_id' => $insertedPeopleListId,
                    'campaign_id' => $campaignId,
                    'referer' => str_replace(" ", "%20", $subData["url"]),
                    'url' => str_replace(" ", "%20", $newUrl),
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Running Campaign "'.$publishedCampaign['name'].'": Building Profile List'
                    ]
                ];

            } else {
                //???
                //we didn't find any previous page we visited so we start from the beginning
                $url = preg_replace("#rsid=([0-9]+)[0-9]{13}#", 'rsid=${1}' . round(microtime(true) * 1000), $url2Visit['url']);

                $insertedPeopleListId = parent::insertPeopleList($url2Visit['id'], 1, mysql_real_escape_string($url));

                return [
                    'username' => $crawledByName ?? '',
                    'linked_in_id' => (int)$crawledById,
                    'people_list_id' => $insertedPeopleListId,
                    'campaign_id' => $campaignId,
                    'url' => str_replace(" ", "%20", trim(str_replace("\n", "", str_replace("\r", "", $url2Visit['url'])))),
                    'status_message' => [
                        'status' => 3,
                        'message' => 'Running Campaign "'.$publishedCampaign['name'].'": Building Profile List'
                    ]
                ];
            }
        }

        parent::updateSfwStatus($companyId, $crawledById, 'waiting');

        return [];
    }
}