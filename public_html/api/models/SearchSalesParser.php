<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 20.06.2017
 * Time: 11:16
 */
require_once(__DIR__ . '/BadPageHandler.php');
require_once(__DIR__ . '/pageModels/v0/SalesSearchPage.php');
require_once(__DIR__ . '/pageModels/v1/SalesSearchPage.php');
require_once(__DIR__ . '/pageModels/v2/SalesSearchPage.php');

use api\models\BadPageHandler;

class SearchSalesParser
{
    const RES_PER_PAGE = 25;
    const RETRY_COUNT = 6;

    /**
     * @param $html
     * @return bool
     */
    public static function isDoubleSalesNav($html)
    {
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);
        $contractManagement = $finder->query("//div[contains(@id, 'contract-management')]/p[contains(@class,'contracts-header')]")->length;
        if ($contractManagement > 0) {
            return \TRUE;
        }
        $contractChooser = $finder->query("//article[@class='contract-chooser']")->length;
        if ($contractChooser > 0) {
            return \TRUE;
        }
        return \FAlSE;
    }

    /**
     * @param $html
     * @return array|bool
     */
    public static function return4FrozenAccount($html)
    {
        if (isset($html) and trim($html) == "This account is frozen, contact info@searchquant.net to unfreeze it") {
            return [
                'url' => APIModel::BASE_VG_URL . '/frozen_account.php',
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'CrawledBy' => "Frozen Account|Frozen Account",
                'username' => 'Frozen Account',
                'linked_in_id' => 0,
                'status_message' => [
                    'status' => 2,
                    'message' => 'This account is frozen. Contact info@searchquant.net'
                ]
            ];
        }
        return \FALSE;
    }

    /**
     * @param $html
     * @param $crawledById
     * @return array|bool
     */
    public static function handleBadPage($html, $crawledById)
    {
        $bpHandler = new api\models\BadPageHandler($html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if (!$bphResult) {
            $finder = $bpHandler->getFinder();

            if(self::getPageVersion($html) > 0){
                return false;
            }
            //HTML: No data for search sales page
            $resultsList = $finder->query("//ul[@id='results-list']")->item(0);
            if ($resultsList === \NULL) {
                $result = [
                    'bp-message' => 'html: ul#results-list',
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'No needed data on the page. Try again...'
                        ]
                    ]
                ];
                return $result;
            }
        } else {
            return $bphResult;
        }
        return \FALSE;
    }

    /**
     * Former parse_dom_response only for search page
     * @param $payload
     * @param string $version
     * @return array
     * @throws Exception
     */
    public static function parseSearchPage($payload, $version = "")
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No linked_in_id parameter");
        }
        if (empty($payload->html)) {
            throw new Exception("No html parameter");
        }
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $versionId = APIModel::getClientVersionIdByName($version);
        if (isset($payload->campaign_id)) {
            $campaignId = $payload->campaign_id;
            $campaignData = \APIModel::getCampaignById($campaignId);
        }
        if (isset($payload->people_list_id)) {
            $peopleListId = $payload->people_list_id;
            $peopleList = ParserModel::getPeopleList($peopleListId);
            if (!empty($peopleList)) {
                $searchUrlId = (int)$peopleList['search_url_id'];
            }
        }
        $companyData = APIModel::getCompanyByProductKey($payload->client_hash);
        $companyId = $companyData["id"];
        $liUser = APIModel::getLiUser($companyId, $crawledById);

        $userSettingsModel = new \classes\Settings($companyId, $crawledById);

        $wait = $companyData['wait_between_connections'];

        $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $companyId, $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);

        $baseResult = [
            'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
            'CrawledBy' => $crawledById . "|" . $crawledByName,
            'linked_in_id' => $crawledById,
            'username' => $crawledByName,
            'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
        ];
        /**
         * @var api\models\pageModels\v0\SalesSearchPage $pageData
         */
        $pageData = self::extractData($payload->html, $payload->url);
        if($pageData){
            if(static::updateLiUserSearchDesignVersion($liUser, $pageData->designVersion)){
                //search version changed, need to retry
                SearchSalesParser::updatePeopleListRetry($peopleList['id'], 1);

                ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, 'li: search interface changed', \APIModel::SEARCH_SN);
                return array_merge($baseResult,[
                    'WaitBetweenConnections' => 30,//ParserModel::randomizeWait($wait),
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Bad page from LinkedIn. Try again...'
                    ]
                ]);
            }


            //$baseResult['designVersion'] = $pageData->designVersion;
        }
        //file_put_contents('/var/www/viewgentools/public_html/api/models/debug.log', var_export($pageData, true), FILE_APPEND);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $pageData->crawledByName);

        if ($pageData->totalMatches !== null) {
            if (!empty($searchUrlId) AND !empty($peopleList) AND $pageData->totalMatches != $peopleList['total_matches']) {
                ParserModel::updateSearchUrlTotalMatches($pageData->totalMatches, $searchUrlId);
                $peopleList['total_matches'] = $pageData->totalMatches;
            }
        }


        if ($pageData->isNoResultFound) {
            HtmlStorage::store(file_get_contents("php://input"), "people_list", $liUser["linked_in_id"], $peopleListId);
            //If it is not a last page we need to retry (flag in DB).
            $retry = self::check4Retry($peopleList);
            if (!$retry) {
                //this is a people list without people and so it's time to exit
                if (!empty($crawledByName && $crawledById && $peopleListId)) {
                    self::updatePeopleListCompleted($crawledByName, $crawledById, $peopleListId, $searchUrlId);
                }
                if(self::checkCampaign4FinishAndRevisit($campaignId, $peopleListId, 0)){
                    self::sendNotification([
                        'company_id' => $companyId,
                        'li_user_name' => $crawledByName,
                        'company_name' => $companyData['company_name'],
                        'campaign_name' => $campaignData['name'],
                        'campaign_visit_count' => $campaignData['campaign_visit_count'],
                        'total_visitors' => $campaignData['total_visitors'],
                        'campaign_id' => $campaignId,
                        'company_email' => $companyData['email'],
                    ]);
                }
                return $baseResult;
            } else {
                ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, 'li: no results', \APIModel::SEARCH_SN);
                return array_merge($baseResult,[
                    'WaitBetweenConnections' => 60 * 3,//ParserModel::randomizeWait($wait),
                    'status_message' => [
                        'status' => 2,
                        'message' => 'Bad page from LinkedIn. Try again...'
                    ]
                ]);
            }
        }

        //==== CHECK FOR BAD PAGE
        $badPage = self::handleBadPage($payload->html, $crawledById);
        if ($badPage) {
            HtmlStorage::store(file_get_contents("php://input"), "people_list", $liUser["linked_in_id"], $peopleListId);
            if ($peopleList['retry_count'] >= self::RETRY_COUNT) {
                self::updatePeopleListRetry($peopleList['id'], 0);
            } else {
                // Retry
                self::updatePeopleListRetry($peopleList['id'], 1);
            }
            ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $badPage['bp-message'], \APIModel::SEARCH_SN);
            return array_merge($baseResult, $badPage['response']);
        }
        //=======

        $skip1st = false;
        if (!empty($payload->url) and strpos($payload->url, 'facet.N=A') and !strpos($payload->url, 'facet.N=F')) {
            $skip1st = true;
        }

        $duplicateCount = 0;
        $connectedSkippedCount = 0;
        $noPublicSkippedCount = 0;
        $visitedSkippedCount = 0;
        //=====
        foreach ($pageData->profiles as $profile) {
            if (!empty($campaignId)) {
                $duplicated = ParserModel::isPeopleByCompanyCampaignLinkedin($companyData['id'], $campaignId, $profile["id"]);
                if ($duplicated) {
                    $duplicateCount++;
                    continue;
                }
            }
            if ($skip1st) {
                if ($profile['degree'] === 'YOU' OR (int)$profile['degree'] === 1) {
                    $connectedSkippedCount++;
                    continue;
                }
            }
            if ($profile["is_private"]) {
                //Skip no-public profiles
                $noPublicSkippedCount++;
                continue;
            }

            if (!$userSettingsModel->getForUser('visiting-disable_visited_skip') && self::checkProfileVisited($crawledById, $profile["member_id"], $profile["id"])) {
                //Skip already visited profiles
                $visitedSkippedCount++;
                continue;
            }

            if (isset($companyData["id"]) AND isset($campaignId) AND isset($peopleListId) AND !empty($profile["id"]) AND isset($searchUrlId)) {
                $lastInsertId = ParserModel::insertPeople($companyData["id"], $campaignId, $peopleListId, $profile["id"], $searchUrlId, $profile["member_id"], $profile["premium"]);
            }
        }
        //=========

        $skippedCount = $duplicateCount + $connectedSkippedCount + $noPublicSkippedCount + $visitedSkippedCount;

        if ($skippedCount > 0) {
            self::updateSearchUrlSkipped($searchUrlId, $skippedCount, $duplicateCount, $connectedSkippedCount, $noPublicSkippedCount, $visitedSkippedCount);
//            SearchSalesParser::updateSearchUrlSkippedCount($searchUrlId, $skippedCount);
        }

//        if($duplicateCount > 0) {
//            ParserModel::updateSearchUrlDuplicate($searchUrlId, $duplicateCount);
//        }

        if (!empty($crawledByName && $crawledById && $peopleListId)) {
            ParserModel::updatePeopleList($crawledByName, $crawledById, $peopleListId);
        }

        if (!empty($peopleListId) AND count($pageData->profiles) == 0) {
            //this is a people list without people and so it's time to exit
            HtmlStorage::store(file_get_contents("php://input"), "people_list", $liUser["linked_in_id"], $peopleListId);

            // Check if we should retry.
            $retry = self::check4Retry($peopleList);
            if ($retry) {
                return $baseResult;
            }

//            if ($peopleList['page_nr'] < ceil($peopleList['total_matches'] / SearchSalesParser::RES_PER_PAGE)) {
//                if ($peopleList['retry_count'] >= 3) {
//                    SearchSalesParser::updatePeopleListRetry($peopleList['id'], 0);
//                } else {
//                    // Retry
//                    SearchSalesParser::updatePeopleListRetry($peopleList['id'], 1);
//                    return [
//                        'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
//                        'CrawledBy' => $crawledById . "|" . $crawledByName,
//                        'username' => $crawledByName,
//                        'linked_in_id' => $crawledById,
//                        'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
//                    ];
//                }
//            }
            if (!empty($crawledById && $peopleListId)) {
                self::updatePeopleListCompleted($crawledByName, $crawledById, $peopleListId, $searchUrlId);
            }
            if(self::checkCampaign4FinishAndRevisit($campaignId, $peopleListId, count($pageData->profiles))){
                self::sendNotification([
                    'company_id' => $companyId,
                    'li_user_name' => $crawledByName,
                    'company_name' => $companyData['company_name'],
                    'campaign_name' => $campaignData['name'],
                    'campaign_visit_count' => $campaignData['campaign_visit_count'],
                    'total_visitors' => $campaignData['total_visitors'],
                    'campaign_id' => $campaignId,
                    'company_email' => $companyData['email'],
                ]);
            }
        }

        return $baseResult;
    }


    public static function extractData($html, $url){

        $pageVersion = self::getPageVersion($html);

        $parserClassName = 'api\models\pageModels\v'.$pageVersion.'\SalesSearchPage';
        return new $parserClassName($html, $url);
    }

    static public function getPageVersion($html){
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) throw new Exception('invalid html');
        $finder = new \DomXPath($dom);

        if($finder->query("//body[contains(@class, 'boot-complete')]")->item(0) === \NULL){
            return 0;
        }

        if(\ParserHelper::findCodeOnPageByKey($html, 'decoratedSpotlights') !== null){
            return 1;
        }

        return 2;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function sendNotification($data)
    {
        //return if finished email for this campaign already sent
        $check = DB::prep("SELECT * FROM campaign WHERE has_sent_campaign_finished_email = 1 AND id = :campaign_id");
        $check->execute([
            'campaign_id' => $data['campaign_id']
        ]);
        if($check->rowCount() > 0){
            return \FALSE;
        }


        require_once(__DIR__ . '/../../classes/EmailTemplate.php');
        $update = DB::prep("UPDATE campaign SET has_sent_campaign_finished_email = 1 WHERE id = :campaign_id");
        $emailTemplate = new EmailTemplate($data['company_id']);
        $vbr = $data['campaign_visit_count'] == 0 ? 0 : ($data['total_visitors'] / $data['campaign_visit_count']);
        $name_parts = explode(" ", trim($data['li_user_name']));
        $variables = [
            'linkedin first name' => $name_parts[0],
            'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
            'company name' => $data['company_name'],
            'campaign name' => $data['campaign_name'],
            'num profiles visited' => $data['campaign_visit_count'],
            'num inbound views' => $data['total_visitors'],
            'view back rate' => round(100 * $vbr, 1) . '%',
            'campaign link' => sprintf('<a href="%1$s">%1$s</a>', \APIModel::BASE_VG_URL . '/visitedProfiles.php?campaign_id=' . $data['campaign_id'] . '&company_id=' . $data['company_id'])
        ];
        $emailTemplate->sendEmail($data['company_email'], "Campaign Finished", $variables);
        $update->execute([
            'campaign_id' => $data['campaign_id']
        ]);

        $select = \DB::prep("SELECT * FROM campaign WHERE publish_status = 'published' AND company_id = :company_id AND
	status = 'Active' AND (start_date = 0000-00-00 OR start_date <= date(NOW()) ) AND visit_by_id = :crawled_by_id
	LIMIT 1");
        if ($select->rowCount() == 0){
            $variables = [
                'linkedin first name' => $name_parts[0],
                'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
                'company name' => $data['company_name']
            ];
            $emailTemplate->sendEmail($data['company_email'], "No Active Campaigns", $variables);
        }
        return \FALSE;
    }

    /**
     * @param $peopleList
     * @return bool
     */
    public static function check4Retry($peopleList)
    {
        if ($peopleList['page_nr'] < ceil($peopleList['total_matches'] / SearchSalesParser::RES_PER_PAGE) AND $peopleList['page_nr'] < ceil(1000 / SearchSalesParser::RES_PER_PAGE)) {
            if ($peopleList['retry_count'] >= SearchSalesParser::RETRY_COUNT) {
                SearchSalesParser::updatePeopleListRetry($peopleList['id'], 0);
                return \FALSE;
            } else {
                // Retry
                SearchSalesParser::updatePeopleListRetry($peopleList['id'], 1);
                return \TRUE;
            }
        }
        return \FALSE;
    }

    /**
     * @param $searchUrlId
     * @param $skippedCount
     * @param int $duplicateCount
     * @param int $connectedSkippedCount
     * @param int $noPublicSkippedCount
     * @param int $visitedSkippedCount
     * @return bool
     * @throws Exception
     */
    public static function updateSearchUrlSkipped($searchUrlId, $skippedCount, $duplicateCount = 0, $connectedSkippedCount = 0, $noPublicSkippedCount = 0, $visitedSkippedCount = 0)
    {
        if ($skippedCount == 0) {
            return \FALSE;
        }
        try {
            $query = \DB::prep("UPDATE search_url SET skipped_count = skipped_count + :add_count WHERE id = :id");
            $query->execute([
                'id' => $searchUrlId,
                'add_count' => $skippedCount
            ]);
            if ($duplicateCount > 0) {
                ParserModel::updateSearchUrlDuplicate($searchUrlId, $duplicateCount);
            }
            if ($connectedSkippedCount > 0) {
                $query = \DB::prep("UPDATE search_url SET connected_skipped_count = connected_skipped_count + :add_count WHERE id = :id");
                $query->execute([
                    'id' => $searchUrlId,
                    'add_count' => $connectedSkippedCount
                ]);
            }
            if ($noPublicSkippedCount > 0) {
                $query = \DB::prep("UPDATE search_url SET nopublic_skipped_count = nopublic_skipped_count + :add_count WHERE id = :id");
                $query->execute([
                    'id' => $searchUrlId,
                    'add_count' => $noPublicSkippedCount
                ]);
            }
            if ($visitedSkippedCount > 0) {
                $query = \DB::prep("UPDATE search_url SET visited_skipped_count = visited_skipped_count + :add_count WHERE id = :id");
                $query->execute([
                    'id' => $searchUrlId,
                    'add_count' => $visitedSkippedCount
                ]);
            }
        } catch (Exception $e) {
            throw $e;
        }
        return \FALSE;
    }

    /**
     * @param $crawledById
     * @param $memberId
     * @param $linked_in_id
     * @return bool
     * @throws Exception
     */
    public static function checkProfileVisited($crawledById, $memberId, $linked_in_id = null)
    {
        try {
            if(!empty($memberId)){
                $query = \DB::prep("SELECT count(*) FROM people WHERE crawled_by_id = :crawled_by_id AND member_id = :member_id AND sent_to_crawler > DATE_SUB(NOW(), INTERVAL 3 DAY)");
                $query->execute([
                    'crawled_by_id' => $crawledById,
                    'member_id' => $memberId
                ]);
            }else{
                $query = \DB::prep("SELECT count(*) FROM people WHERE crawled_by_id = :crawled_by_id AND linked_in_id = :linked_in_id AND sent_to_crawler > DATE_SUB(NOW(), INTERVAL 3 DAY)");
                $query->execute([
                    'crawled_by_id' => $crawledById,
                    'linked_in_id' => $linked_in_id
                ]);
            }

            $result = $query->fetchColumn();
            if ($result > 0) {
                return \TRUE;
            }
        } catch (Exception $e) {
            throw $e;
        }
        return \FALSE;
    }

    /**
     * @param $campaignId
     * @param $peopleListId
     * @param $peopleOnThisPage
     * @return bool (true when campaign is finished)
     */
    public static function checkCampaign4FinishAndRevisit($campaignId, $peopleListId, $peopleOnThisPage)
    {
        $campaignIsFinished = \FALSE;
        $query = DB::prep("SELECT count(*) AS cnt FROM search_url WHERE campaign_id = :campaign_id AND finished = 0");
        $query->execute(['campaign_id' => $campaignId]);
        $data = $query->fetch();
        if ($data["cnt"] == 0) {
            $query = DB::prep("UPDATE campaign SET status = 'Finished',campaign_completed = NOW() WHERE id = :id AND priority>=0");
            $query->execute(['id' => $campaignId]);
            $campaignIsFinished = \TRUE;

            // Check if this campaign should revisit.
            $query = DB::prep("SELECT * FROM campaign WHERE id=:id AND priority>=0");
            $query->execute(['id' => $campaignId]);
            $campaign = $query->fetch();
            if ($campaign) {
                if ($campaign['revisit_after'] > 0 && ($campaign['end_date'] == '0000-00-00' || strtotime($campaign['end_date']) > time())) {
                    // Copy campaign.
                    $newCampaignId = copy_campaign($campaignId, "", true);

                    // Schedule new campaign in future.
                    $query = DB::prep("UPDATE campaign SET start_date=:start_date, publish_status='published' WHERE id=:id");
                    $query->execute([
                        'start_date' => date('Y-m-d', time() + $campaign['revisit_after'] * 3600 * 24),
                        'id' => $newCampaignId
                    ]);
                }
            }
        } elseif ($peopleListId != "" and $peopleOnThisPage > 0) {
            // We got matches. Remove retry.
            $query = DB::prep("UPDATE people_list SET retry = 0 WHERE id = :people_list_id");
            $query->execute([
                'people_list_id' => $data['id']
            ]);
        }
        return $campaignIsFinished;
    }

    /**
     * @param string $crawledBy
     * @param int $crawledById
     * @param int $peopleListId
     * @param $searchUrlId
     * @return bool
     */
    public static function updatePeopleListCompleted(string $crawledBy, int $crawledById, int $peopleListId, $searchUrlId): bool
    {
        $query = DB::prep("UPDATE people_list SET crawled_by = :crawled_by, crawled_by_id = :crawled_by_id, people_list_completed = NOW() WHERE id = :id");
        $query->execute([
            'crawled_by' => $crawledBy,
            'crawled_by_id' => $crawledById,
            'id' => $peopleListId
        ]);
        $query = DB::prep("UPDATE people_list SET last_list = 1 WHERE id = :id");
        $query->execute(['id' => $peopleListId]);

        $query = DB::prep("UPDATE search_url SET finished = 1 WHERE id = :id");
        $query->execute(['id' => $searchUrlId]);
        return false;
    }

    /**
     * @param $peopleListId
     * @param $retry
     * @return bool
     */
    public static function updatePeopleListRetry($peopleListId, $retry)
    {
        if ($retry === 0) {
            $query = DB::prep("UPDATE people_list SET retry = 0 WHERE id = :people_list_id");
        } elseif ($retry === 1) {
            $query = DB::prep("UPDATE people_list SET retry = 1, retry_count = retry_count + 1 WHERE id = :people_list_id");
        }
        $query->execute([
            'people_list_id' => $peopleListId
        ]);
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function updateLiUserSearchDesignVersion($liUser, $version)
    {

        if($liUser['search_design_version'] != $version){
            $query = DB::prep("UPDATE li_users SET search_design_version = :version WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'linked_in_id' => $liUser['linked_in_id'],
                'version' => $version
            ]);

            return true;
        }
        return false;
    }
}