<?php
namespace api\models\pageModels\v0;

require_once(__DIR__ . '/../PageBase.php');
use api\models\pageModels\PageBase;
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 19.06.18
 * Time: 15:44
 */
class SalesSearchPage extends PageBase
{
    public $totalMatches = null;
    public $isNoResultFound = true;
    public $profiles = [];

    public function __construct($html)
    {
        parent::__construct($html);

        $this->designVersion = 0;
        $this->loadData();
    }

    public function loadData(){
        $this->crawledByName = $this->extractCrawledByName();
        $this->totalMatches = (int)$this->extractTotalMatches();
        $this->isNoResultFound = $this->extractNoResultsMessage() ? true : false;

        if(!$this->isNoResultFound){
            $this->profiles = $this->extractProfiles();
        }
    }

    public function extractProfiles(){
        $results = [];
        //How it works (Non-documented feature!): http://php.net/manual/en/domxpath.query.php#99760
        $context = $this->finder->query("//div[@class='entity-info']");
        $names = $this->finder->query("//a[@class='name-link profile-link']");

        //We need count all profiles here, including private.
        $peopleOnThisPage = $names->length;

        for ($i = 0; $i < $peopleOnThisPage; $i++) {
            $profile = [
                'id' => '',
                'member_id' => '',
                'name' => '',
                'degree' => '',
                'premium' => false,
                'is_private' => false
            ];

            $profile["id"] = $this->finder->query("//a[@class='name-link profile-link']/@href")->item($i);
            $profile["id"] = $profile["id"] === \NULL ? '' : trim($profile["id"]->nodeValue);
            if (preg_match('#\/sales\/profile\/([^\?]+)\?#', $profile["id"], $match)) {
                $profile["id"] = $match[1];
            }

            $degreeNode = $this->finder->query("//abbr[contains(@class,'degree-icon')]/text()")->item($i);
            $profile['degree'] = $degreeNode !== \NULL ? trim($degreeNode->nodeValue) : 0;

            $profile["name"] = $names->item($i) === \NULL ? '' : trim($names->item($i)->nodeValue);

            if ($profile["name"] === 'LinkedIn Member') {
                //Skip no-public profiles
                $profile["is_private"] = true;
            }

            if (!empty($profile["id"]) AND preg_match('#([^,]+)\,#', $profile["id"], $match)) {
                if (!empty($match[1])) {
                    $profile["member_id"] = (int)$match[1];
                }
            }

            $profile["premium"] = $this->finder->query(".//li-icon[@type='linkedin-premium-icon']", $context->item($i))->item(0) === \NULL ? false : true;

            if($profile['id']){
                $results[] = $profile;
            }
        }
        return $results;
    }

    public function extractNoResultsMessage(){
        //<h3 class=\"empty-result-header\">Sorry, no results containing your search terms were found.</h3>
        $noResultMessageNode = $this->finder->query("//h3[@class='empty-result-header']")->item(0);
        return $noResultMessageNode !== \NULL ? trim($noResultMessageNode->nodeValue) : '';
    }

    public function extractCrawledByName(){
        $crawledByNameFromPageNode = $this->finder->query("//img[@class='profile-photo']/@alt")->item(0);
        return $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';
    }

    public function extractTotalMatches(){
        $totalMatchesNode = $this->finder->query("//button[contains(@class,'select-spotlight TR')]/span[contains(@class,'spotlight-result-count')]")->item(0);
        $totalMatches = !empty($totalMatchesNode) ? trim($totalMatchesNode->nodeValue) : \FALSE;
        if ($totalMatches !== \FALSE) {
            preg_match('/([\d\.]*)([KM]?)/', $totalMatches, $match);
            //$match[1] - number, $match[2] - K or M or ''
            $k = 1;
            switch ($match[2]) {
                case 'K':
                    $k = 1000;
                    break;
                case 'M':
                    $k = 1000000;
                    break;
            }
            if (!empty($k)) {
                return (float)$match[1] * $k;
            }
            return $totalMatches;
        }
        return null;
    }
}