<?php
namespace api\models\pageModels\v2;

require_once(__DIR__ . '/../PageBase.php');
use api\models\pageModels\PageBase;
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 19.06.18
 * Time: 15:44
 */
class SalesSearchPage extends PageBase
{
    public $totalMatches = null;
    public $isNoResultFound = true;
    public $profiles = [];

    public function __construct($html)
    {
        parent::__construct($html);

        $this->designVersion = 2;
        $this->loadData();
    }

    public function loadData(){
        $this->crawledByName = $this->extractCrawledByName();
        $this->totalMatches = (int)$this->extractTotalMatches();
        $this->isNoResultFound = $this->extractNoResultsMessage() ? true : false;

        if(!$this->isNoResultFound){
            $this->profiles = $this->extractProfiles();
        }
    }

    public function extractProfiles(){
        $results = [];

        $peoples = $this->finder->query("//div[contains(@class,'search-results__result-container')]");

        foreach ($peoples as $people){
            $profile = [
                'id' => '',
                'member_id' => '',
                'name' => '',
                'degree' => '',
                'premium' => false,
                'is_private' => false
            ];

            $profile_link_node = $this->finder->query(".//dt[@class='result-lockup__name']/a", $people)->item(0);

            $profile["id"] = $this->finder->query("./@href", $profile_link_node)->item(0);
            $profile["id"] = $profile["id"] === \NULL ? '' : trim($profile["id"]->nodeValue);
            if (preg_match('#\/sales\/people\/([^\?]+)\?#', $profile["id"], $match)) {
                $profile["id"] = $match[1];
            }

            $profile["name"] = $profile_link_node === \NULL ? '' : trim($profile_link_node->nodeValue);

            if ($profile["name"] === 'LinkedIn Member') {
                //Skip no-public profiles
                $profile["is_private"] = true;
            }

            if (!empty($profile["id"]) AND preg_match('#([^,]+)\,#', $profile["id"], $match)) {
                if (!empty($match[1])) {
                    $profile["member_id"] = (int)$match[1];
                }
            }

            $profile["premium"] = $this->finder->query(".//li-icon[@color='premium']", $people)->item(0) === \NULL ? false : true;


            $degreeNode = $this->finder->query(".//ul/li/span[contains(@class,'label-16dp')]", $people)->item(0);
            $profile['degree'] = $degreeNode !== \NULL ? intval(trim($degreeNode->nodeValue)) : 0;


            if($profile['id']){
                $results[] = $profile;
            }

        }

        return $results;
    }

    public function extractNoResultsMessage(){
        $noResultMessageNode = $this->finder->query("//p[@class='search-results__no-results-message']")->item(0);
        return $noResultMessageNode !== \NULL ? trim($noResultMessageNode->nodeValue) : '';
    }

    public function extractCrawledByName(){
        $crawledByNameFromPageNode = $this->finder->query("//li[@class='nav-item nav-item-user']/h2/span")->item(0);
        return $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';
    }

    public function extractTotalMatches(){
        $totalMatchesNode = $this->finder->query("//artdeco-spotlight-tab[contains(@class,'search-spotlights__tab active')]/span[contains(@class,'artdeco-tab-primary-text')]")->item(0);
        $totalMatches = !empty($totalMatchesNode) ? trim($totalMatchesNode->nodeValue) : \FALSE;
        if ($totalMatches !== \FALSE) {
            $totalMatches = str_replace(',', '', $totalMatches);
            return intval($totalMatches);
        }
        return null;
    }
}