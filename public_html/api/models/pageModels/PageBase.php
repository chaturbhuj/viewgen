<?php
namespace api\models\pageModels;

/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 19.06.18
 * Time: 15:47
 */
class PageBase
{
    public $finder = null;
    public $html = '';
    public $designVersion;
    public $crawledByName;

    public function __construct($html)
    {
        $this->html = $html;
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) throw new Exception('invalid html');
        $finder = new \DomXPath($dom);

        $this->finder = $finder;
    }

}