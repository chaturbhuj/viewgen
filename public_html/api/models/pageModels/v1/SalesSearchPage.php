<?php
namespace api\models\pageModels\v1;

require_once(__DIR__ . '/../PageBase.php');
use api\models\pageModels\PageBase;
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 19.06.18
 * Time: 15:44
 */
class SalesSearchPage extends PageBase
{
    public $totalMatches = null;
    public $isNoResultFound = true;
    public $profiles = [];

    public function __construct($html, $url)
    {
        parent::__construct($html);

        $this->designVersion = 1;
        $this->loadData();
    }

    public function loadData(){
        $this->crawledByName = $this->extractCrawledByName();
        $this->isNoResultFound = $this->extractNoResultsMessage() ? true : false;

        if(!$this->isNoResultFound){
            $this->profiles = $this->extractProfiles();
        }
    }

    public function extractProfiles(){
        $results = [];

        $pageData = \ParserHelper::findCodeOnPageByKey($this->html, 'decoratedSpotlights');

        if(!is_array($pageData)){
            //throw new \Exception('no data found');
            return $results;
        }

        $this->totalMatches = $pageData['paging']['total'];

        foreach ($pageData['elements'] as $element) {
            $profile = [
                'id' => '',
                'member_id' => '',
                'name' => '',
                'degree' => '',
                'premium' => false,
                'is_private' => false,
                'geoRegion' => '',
                'firstName' => '',
                'lastName' => '',
                'viewed' => false
            ];


            if (preg_match('#urn:li:fs_salesProfile:\((.+)\)#', $element["entityUrn"], $match)) {
                $profile["id"] = $match[1];
            }
            if (preg_match('#urn:li:member:(.+)#', $element["objectUrn"], $match)) {
                $profile["member_id"] = (int)$match[1];
            }

            $profile['degree'] = isset($element["degree"]) ? $element["degree"] : '';
            $profile["name"] = isset($element["fullName"]) ? $element["fullName"] : '';
            $profile["premium"] = isset($element["premium"]) ? $element["premium"] : false;
            $profile["geoRegion"] = isset($element["geoRegion"]) ? $element["geoRegion"] : '';
            $profile["firstName"] = isset($element["firstName"]) ? $element["firstName"] : '';
            $profile["lastName"] = isset($element["lastName"]) ? $element["lastName"] : '';
            $profile["viewed"] = isset($element["viewed"]) ? $element["viewed"] : false;

            if ($profile["name"] === 'LinkedIn Member') {
                //Skip no-public profiles
                $profile["is_private"] = true;
            }

            if($profile['id']){
                $results[] = $profile;
            }
        }
        return $results;
    }

    public function extractNoResultsMessage(){
        $noResultMessageNode = $this->finder->query("//div[@class='search-results__no-results']")->item(0);
        return $noResultMessageNode !== \NULL ? trim($noResultMessageNode->nodeValue) : '';
    }

    public function extractCrawledByName(){
        $crawledByNameFromPageNode = $this->finder->query("//li[@class='nav-item nav-item-user']/h2/span")->item(0);
        return $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';
    }
}