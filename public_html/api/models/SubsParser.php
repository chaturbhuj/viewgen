<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 28.06.2017
 * Time: 11:23
 * class SubsParser
 * Parse page https://www.linkedin.com/premium/manage to get account type and subscriptions
 */
class SubsParser
{
    const ALLOWED_SUBS = ['Premium Business', 'Executive', 'Sales Navigator', 'Premium Job Seeker', 'Business'];
    const ACCOUNT_TYPES = ['Premium Business', 'Executive', 'Premium Career', 'Basic', 'Premium Job Seeker', 'Business'];
    const SUBSCRIPTIONS = ['Sales Navigator'];
    const UNDEFINED_TYPE = 'Undefined';
    const WAIT = 10;

    /**
     * @param $linkedinId
     * @param bool $pageSubs
     * @return bool
     */
    public static function updateSubscriptions($linkedinId, $pageSubs = \FALSE)
    {
        /**
         * @TODO update account type when subscriptions changed
         */
        if ($pageSubs === \FALSE) {
            $query = \DB::prep("UPDATE li_users SET lastSubscriptionsCheck = NOW() WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'linked_in_id' => $linkedinId
            ]);
        } else {
            $query = \DB::prep("UPDATE li_users SET lastSubscriptionsCheck = NOW(), subscriptions = :subscriptions WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'subscriptions' => $pageSubs,
                'linked_in_id' => $linkedinId
            ]);
        }
        if ($query->rowCount() !== 1) {
            return \FALSE;
        }
        return \TRUE;
    }

    /**
     * @param $user
     * @return bool
     */
    public static function isUserSubsSupported($user)
    {
        return true; //disable subscription check, temp fix
        $result = false;
        $subs = explode(',', $user['subscriptions']);
        foreach ($subs as $val) {
            if (in_array($val, self::ALLOWED_SUBS)) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @param $emailTo
     * @param $userName
     * @return bool
     * @internal param $email
     */
    public static function sendNotification($emailTo, $userName)
    {
        //TODO make notification
//        include(__DIR__ . '/../../classes/Email.php');
//        $em = new Email();
//        $em->setHtmlBody(' ');
//        $em->setTextBody(' ');
//        $em->setSubject(' ');
//        $em->setTemplateId('20abcfab-261b-435d-b18c-af22c42ab0d3');
//        $em->addTo($emailTo);
//        $em->send();
        return false;
    }

    /**
     * @param $html
     * @param $crawledById
     * @return array|bool
     */
    public static function handleBadPage($html, $crawledById)
    {
        $result = [
            'badPage' => null,
            'pageVersion' => 0
        ];

        $bpHandler = new api\models\BadPageHandler($html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if (!$bphResult) {
            $finder = $bpHandler->getFinder();
            $resultsList = $finder->query("//div[@id='premium_account_section']")->item(0);
            $resultsList_v1 = $finder->query("//div[@id='setting-premium-subscription-content']")->item(0);

            if($resultsList_v1 !== \NULL){
                $result['pageVersion'] = 1;
            }

            if ($resultsList === \NULL && $resultsList_v1 === \NULL) {
                $result['badPage'] = [
                    'bp-message' => 'html: div#premium_account_section',
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'No needed data on the page. Try again...'
                        ]
                    ]
                ];
            }
        } else {
            $result['badPage'] =  $bphResult;
        }
        return $result;
    }

    /**
     * @param $company
     * @param $payload
     * @return array
     * @throws Exception
     */
    public static function checkSubs($company, $payload, $version)
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $versionId = APIModel::getClientVersionIdByName($version);
        $liUser = APIModel::getLiUser($company['id'], $crawledById);
        if (empty($crawledByName)) {
            $crawledByName = $liUser['name'];
        }
        $wait = $company['wait_between_connections'];

        $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $company['id'], $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);
        //==== CHECK FOR BAD PAGE
        $badPage = self::handleBadPage($payload->html, $crawledById);
        if ($badPage['badPage']) {
            ParserModel::insertApiBadPages($company['id'], $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $badPage['badPage']['bp-message'], \APIModel::SUBS_PAGE);
            return array_merge([
                'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
            ], $badPage['badPage']['response']);
        }
        //=======

        if($badPage['pageVersion']){
            $data = self::parseSubs_v1($payload->html);
        }else{
            $data = self::parseSubs($payload->html);
        }
        $crawledByName = $data['name'] ?? '';
        $pageSubs = $data['subscriptions'] ?? '';

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByName);

        if (!empty($pageSubs) AND $pageSubs !== $liUser['subscriptions']) {
            self::updateSubscriptions($crawledById, $pageSubs);
        } else {
            self::updateSubscriptions($crawledById);
        }
        if ($data['supported'] === false) {
            APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_WAITING);
            if (!empty($company['email'])) {
                //TODO only one for one day
//                self::sendNotification($company['email'], $liUser['name']);
            }
            return [
                'CrawledBy' => $crawledById . '|' . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'checkingSubscriptions' => 0,
                'status_message' => [
                    'status' => 2,
                    'message' => 'Your type of Linkedin account is not supported now.'
                ]
            ];
        }
        APIModel::updateSfwStatus($company['id'], $crawledById, APIModel::STATUS_RUNNING);
        return [
            'WaitBetweenConnections' => ParserModel::randomizeWait($wait),
            'CrawledBy' => $crawledById . '|' . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => (int)$crawledById,
            'VisitedProfiles24' => (int)$liUser['visitsLast24Hours'],
            'checkingSubscriptions' => 0
        ];
    }

    /**
     * @param $html
     * @return array with li-user's name and string with subscriptions he has
     * @throws Exception
     */
    public static function parseSubs($html)
    {
        $result = ['name' => '', 'supported' => \FALSE, 'subscriptions' => ''];
        $subscriptions = [];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            throw new Exception('Bad HTML');
        }
        $finder = new \DomXPath($dom);

        $nameNode = $finder->query("//img[@class='profile-photo']/@alt")->item(0);
        $result['name'] = $nameNode !== \NULL ? trim($nameNode->nodeValue) : '';

        //Check for Sales Navigator
        $salesNavNode = $finder->query("//div[contains(@id,'cap_sales_section')]//span[contains(@class, 'cap-account-name')]")->item(0);
        $salesNav = $salesNavNode !== \NULL ? trim($salesNavNode->nodeValue) : '';
        if (!empty($salesNav) AND (strpos($salesNav, "Sales Navigator") !== false OR strpos($salesNav, "Wells Fargo - Middle Market Banking") !== false)) { //hotfix for custom sales plan
            $subscriptions[] = "Sales Navigator";
        }

        //Check for Recruiter
        $recruiterNode = $finder->query("//div[contains(@id,'cap_recruiter_section')]//p[contains(@class, 'status-active')]")->item(0);
        $recruiter = $recruiterNode !== \NULL ? trim($recruiterNode->nodeValue) : '';
        if (!empty($recruiter)) {
            $subscriptions[] = "Premium Business"; //TODO temporary fix, experimental recruiter, seems like recruiter has the same interface as premium
            $subscriptions[] = "Recruiter"; //TODO temporary fix, experimental recruiter, seems like recruiter has the same interface as premium
        }


        //Check for Account Type
        $accountTypeNode = $finder->query("//div[contains(@id,'premium_account_section')]//span[contains(@id,'account-type')]")->item(0);
        $accountType = $accountTypeNode !== \NULL ? trim($accountTypeNode->nodeValue) : '';
        foreach (SubsParser::ACCOUNT_TYPES as $sub) {
            if (strpos($accountType, $sub) !== false) {
                $subscriptions[] = $sub;
            }
        }

        $result['supported'] = false;
        foreach ($subscriptions as $val) {
            if (in_array($val, self::ALLOWED_SUBS)) {
                $result['supported'] = true;
            }
        }

        $result['subscriptions'] = '';
        if (count($subscriptions) > 0) {
            $result['subscriptions'] = implode(",", $subscriptions);
        }

        return $result;
    }

    /**
     * @param $html
     * @return array with li-user's name and string with subscriptions he has
     * @throws Exception
     */
    public static function parseSubs_v1($html)
    {
        $result = ['name' => '', 'supported' => \FALSE, 'subscriptions' => ''];
        $subscriptions = [];
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) {
            throw new Exception('Bad HTML');
        }
        $finder = new \DomXPath($dom);

        $nameNode = $finder->query("//img[@class='profile-photo']/@alt")->item(0);
        $result['name'] = $nameNode !== \NULL ? trim($nameNode->nodeValue) : '';


        $planDetailNodes = $finder->query("//div[@id='setting-premium-subscription-content']/div[contains(@class, 'subs-detail')]");

        foreach ($planDetailNodes as $planDetailNode){
            $detailText = $planDetailNode !== \NULL ? trim($planDetailNode->nodeValue) : '';

            if(strpos($detailText, 'You’re currently subscribed to ') === false) continue; //skip if node does not contain plan name

            if(strpos($detailText, "Wells Fargo - Middle Market Banking") !== false) $subscriptions[] = 'Sales Navigator'; //hotfix for custom sales plan
            if(strpos($detailText, "Recruiter") !== false) $subscriptions[] = 'Premium Business'; //TODO temporary fix, experimental recruiter, seems like recruiter has the same interface as premium

            foreach (SubsParser::ACCOUNT_TYPES as $sub) {
                if (strpos($detailText, $sub) !== false) {
                    $subscriptions[] = $sub;
                    break;
                }
            }

        }

        $result['supported'] = false;
        foreach ($subscriptions as $val) {
            if (in_array($val, self::ALLOWED_SUBS)) {
                $result['supported'] = true;
            }
        }

        $result['subscriptions'] = '';
        if (count($subscriptions) > 0) {
            $result['subscriptions'] = implode(",", $subscriptions);
        }

        return $result;
    }
}