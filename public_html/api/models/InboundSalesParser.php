<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 28.06.2017
 * Time: 16:21
 */
require_once(__DIR__ . '/BadPageHandler.php');

use api\models\BadPageHandler;
class InboundSalesParser
{
    const INBOUND_VIEWS_PER_PAGE = 20;
    const INBOUND_VIEWS_MAX_DUBS = 15;
    const DAY = 24 * 3600;
    const WEEK = 24 * 3600 * 7;
    const MONTH = 24 * 3600 * 30;
    const NAME4PRIVATE = 'PRIVATE VIEWBACK';
    const UNAVAILABLE = 'UNAVAILABLE';
//    const SOMEONE = 'SOMEONE';

    /**
     * @param $crawledById
     * @return mixed
     * @throws Exception
     */
    public static function getNumberInbound($crawledById)
    {
        try {
            $query = DB::prep("SELECT COUNT(*) numberOf FROM inbound_visit WHERE crawled_by_id = :crawled_by_id");
            $query->execute(['crawled_by_id' => $crawledById]);
            $data = $query->fetch();
        } catch (Exception $e) {
            throw $e;
        }
        return $data['numberOf'];
    }

    /**
     * @param $crawledById
     * @param $linkedinId
     * @param $linkedinHashedId
     * @param $name
     * @param $industryId
     * @param $industryName
     * @param $headline
     * @param $guessedVisitDate
     * @return bool|string
     * @throws Exception
     */
    public static function insertInbound($crawledById, $linkedinId, $linkedinHashedId, $name, $industryId, $industryName, $headline, $guessedVisitDate)
    {
        try {
            $insert = \DB::prep("INSERT INTO inbound_visit (crawled_by_id, linked_in_id, linked_in_hashed_id, name, industry_id, industry_name, headline, guessed_visit_date) VALUES(:crawled_by_id, :linked_in_id, :linked_in_hashed_id, :name, :industry_id, :industry_name, :headline, :guessed_visit_date)");
            $insert->execute([
                'crawled_by_id' => $crawledById,
                'linked_in_id' => $linkedinId,
                'linked_in_hashed_id' => $linkedinHashedId,
                'name' => $name,
                'industry_id' => $industryId,
                'industry_name' => $industryName,
                'headline' => $headline,
                'guessed_visit_date' => $guessedVisitDate
            ]);
            if ($insert->rowCount() !== 1) {
                return \FALSE;
            }
            //it must be string
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $html
     * @return object JSON
     * @throws Exception
     */
    public static function getJsonFromPage($html)
    {
//        /\{(?:[^{}]|(?R))*\}/x  -  doesn't work
        $html = preg_replace('#[\n\r]#', ' ', $html);
        $correct = preg_match('#\{.*\}#', $html, $match);
        if (!$correct OR !isset($match[0])) {
            return \NULL;
        }
        //Sometimes after preg_match we get string with EOL symbols
        $json = preg_replace('#[\n\r]#', ' ', $match[0]);
        $json = json_decode($json);
        return $json;
    }

    /**
     * @param $crawledById
     * @return mixed time like this "2017-07-13 13:49:50" OR NULL (no time) OR FALSE (query error)
     */
    public static function getLastInboundCheck($crawledById)
    {
        try {
            $query = \DB::prep("SELECT last_inbound_check FROM li_users WHERE linked_in_id = :linked_in_id");
            $query->execute([
                'linked_in_id' => $crawledById
            ]);
            return $query->fetchColumn();
        } catch (Exception $e) {
            errorResponse($e->getMessage());
        }
    }

    /**
     * Check if we need to parse all viewbacks from Linkedin
     * @param $crawledById
     * @return bool
     */
    public static function checkAllData($crawledById)
    {
        $lic = InboundSalesParser::getLastInboundCheck($crawledById);
        if (empty($lic)) {
            return \TRUE;
        }
        $lastCheck = date('Y-m-d', strtotime($lic));
        $today = date('Y-m-d', time());
        $diff = InboundSalesParser::daysDiff($today, $lastCheck);
        if ($diff > 6) {
            return \TRUE;
        }
        return \FALSE;
    }

    public static function object2Array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = self::object2Array($value);
            }
            return $result;
        }
        return $data;
    }

    /**
     * @param $html
     * @param $crawledById
     * @return array|bool
     */
    public static function handleBadPage($html, $crawledById)
    {
        $bpHandler = new api\models\BadPageHandler($html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if (!$bphResult) {
            $result = [
                'bp-message' => 'html: json',
                'response' => [
                    'status_message' => [
                        'status' => 2,
                        'message' => 'No needed data on the page. Try again...'
                    ]
                ]
            ];
            return $result;
        } else {
            return $bphResult;
        }
        return \FALSE;
    }

    /**
     * @param $payload
     * @param string $version
     * @return array - the same $payload with few changed params
     * @throws Exception
     */
    public static function checkInboundViews($payload, $version = '')
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        $response = [];
        mb_internal_encoding("UTF-8");
        $versionId = APIModel::getClientVersionIdByName($version);
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        if (empty($crawledByName) and isset($payload->latest_li_user_id_inbound_crawled_by)) {
            $crawledByName = APIModel::explodeCrawledBy($payload->latest_li_user_id_inbound_crawled_by)[1] ?? '';
        }
        $companyData = APIModel::getCompanyByProductKey($payload->client_hash);
        $companyId = $companyData['id'];
        $wait = $companyData['wait_between_connections'];
        $liUsers = APIModel::getLiUser($companyId, $crawledById);
        if (empty($crawledByName)) {
            $crawledByName = $liUsers['name'] ?? '';
        }
        $inboundNumber = self::getNumberInbound($crawledById);

        if (!empty($companyId && $crawledById && $payload->client_hash && $crawledByName)) {
            HtmlStorage::store($payload->html, 'viewbacks', $crawledById, $inboundNumber);
            $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $companyId, $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, 0, $versionId, $payload->html);
        }

        //---Preparing for parsing
        $json = self::getJsonFromPage($payload->html);

        //CHECK FOR BAD PAGE
        $badPage = InboundSalesParser::handleBadPage($payload->html, $crawledById);
        if (!isset($json->viewers) AND !isset($json->pagination)) {
            ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, \NULL, \NULL, \NULL, $badPage['bp-message'], \APIModel::SN_INBOUND_PAGE);
            return array_merge([
                'BadPage' => \TRUE,
                'VisitedProfiles24' => (int)$liUsers["visitsLast24Hours"],
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'linked_in_id' => $crawledById,
                'username' => $crawledByName,
                'WaitBetweenConnections' => ParserModel::randomizeWait($wait)
            ], $badPage['response']);
        }

        $numberOfPublicInbound = 0;
        $totalInbound = 0;
        $unavailableInbound = 0;
        $hiddenInbound = 0;
        $badInbound = 0;
        $gotDuplicate = 0;
        $weeklyDup = 0;
        $moDup = 0;
        $shouldStop = false;
        $checkAll = InboundSalesParser::checkAllData($crawledById);
        $curPage = $payload->inbound_visit_page ?? false;
        $viewCount = intval($payload->inbound_visit_count ?? false);
//        $swichedParseArea = $payload->swiched_parse_area ?? 'to parse';
//        $emptyPage = $payload->empty_page ?? false;
        //['inbound', 'dayStart', 'dayEnd', 'count']
        $arrPrivateVisits = $payload->arr_private_visits ?? [];
        $arrPrivateVisits = self::object2Array($arrPrivateVisits);
        $iAPV = count($arrPrivateVisits);

        $arrDayPrivate = $payload->arr_day_private_visits ?? [];
        $arrDayPrivate = self::object2Array($arrDayPrivate);

        if (!empty($json->pagination)) {
            if (isset($json->pagination->firstPage) and $json->pagination->firstPage === true) {
                if (isset($json->viewCount)) {
                    $viewCount = (int)str_replace(',', '', $json->viewCount);
                }
            }
        }
        if (isset($json->viewers)) {

            try {

                foreach ($json->viewers as $inbound) {
                    if (empty($inbound->privacy)) {
                        $badInbound++;
                        continue;
                    }
                    if ($inbound->privacy == 'HIDDEN' OR $inbound->privacy == 'OBFUSCATED') {
                        //Several hidden viewers (aggregated)
                        if (!empty($inbound->aggregatedViewer)) {
                            $hiddenCount = 1;
                            if (!empty($inbound->aggregatedViewer->aggregatedAnonViewerCount)) {
                                $hiddenCount = $inbound->aggregatedViewer->aggregatedAnonViewerCount;
                                $hiddenInbound += $hiddenCount;
                                if (!empty($inbound->aggregatedViewer->aggregationDateStart) AND !empty($inbound->aggregatedViewer->aggregationDateEnd)) {
                                    $dayStart = date("Y-m-d", strtotime($inbound->aggregatedViewer->aggregationDateStart));
                                    $dayEnd = date("Y-m-d", strtotime($inbound->aggregatedViewer->aggregationDateEnd));
                                    $arrPrivateVisits[$iAPV] = [
                                        'inbound' => $inbound,
                                        'count' => $hiddenCount,
                                        'dayEnd' => $dayEnd,
                                        'dayStart' => $dayStart
                                    ];
                                    $iAPV++;
//                                InboundSalesParser::savePrivateVisitors($inbound, $hiddenCount, $crawledById, $dayEnd, $dayStart);
                                    continue;
                                } else {
                                    $hiddenInbound -= $hiddenCount;
                                    $badInbound += $hiddenCount;
                                    continue;
                                }
                            } else {
                                $badInbound++;
                                continue;
                            }
                        } elseif (!empty($inbound->timeAgo)) {
                            //One hidden viewer
                            $hiddenInbound++;
                            $dayDate = '';
                            if (isset($inbound->timeAgo->days)) {
                                $dayDate = date('Y-m-d', time() - $inbound->timeAgo->days * (self::DAY));
                                $arrDayPrivate[$dayDate][] = $inbound;
//                            if ($checkAll) {
//                                $isThisDup = InboundSalesParser::check4DayPrivate($inbound, $crawledById);
//                                if ($isThisDup) {
//                                    continue;
//                                }
//                            }
                            } elseif (isset($inbound->timeAgo->weeks) AND $checkAll) {
//                                $isThisDup = InboundSalesParser::check4WeekPrivate($inbound, $crawledById);
//                                if ($isThisDup) {
//                                    continue;
//                                }
                                $dayDate = InboundSalesParser::getRandomWeekDate($inbound->timeAgo->weeks);
                            } elseif (isset($inbound->timeAgo->months) AND $checkAll) {
//                                $isThisDup = InboundSalesParser::check4MoPrivate($inbound, $crawledById);
//                                if ($isThisDup) {
//                                    continue;
//                                }
                                $dayDate = InboundSalesParser::getRandomMoDate($inbound->timeAgo->months);
                            } elseif (!$checkAll AND (isset($inbound->timeAgo->months) OR isset($inbound->timeAgo->weeks))) {
                                $shouldStop = true;
//                                var_dump('HERE!290');
                                break;
                            }
                            if (!empty($dayDate) AND !isset($inbound->timeAgo->days)) {
                                InboundSalesParser::savePrivateVisitors($inbound, 1, $crawledById, $dayDate);
                                continue;
                            } else {
                                $hiddenInbound--;
                                $badInbound++;
                                continue;
                            }
                        }
                    }

                    if ($inbound->privacy !== "PUBLIC" OR empty($inbound->memberId)) {
                        //This is incorrect record. We just skip it.
                        $badInbound++;
                        continue;
                    }

                    // For public viewers
                    if (empty($inbound->fullName)) {
                        $unavailableInbound++;
                        $inbound->fullName = self::UNAVAILABLE;
                    } else {
                        $numberOfPublicInbound++;
                    }
                    if (isset($inbound->timeAgo->days)) {
                        $isThisDup = InboundSalesParser::check4DayDuplicate($inbound, $crawledById);
                        if ($isThisDup === \TRUE) {
                            $gotDuplicate++;
                            continue;
                        }
                        InboundSalesParser::saveDayVisitor($inbound, $crawledById);
                    } elseif (isset($inbound->timeAgo->weeks) AND !$checkAll) {
                        $shouldStop = true;
//                        var_dump('HERE!331');
                        break;
                    } elseif (isset($inbound->timeAgo->weeks) AND $checkAll) {
                        $isThisDup = InboundSalesParser::check4WeeklyDuplicate($inbound, $crawledById);
                        if ($isThisDup) {
                            $weeklyDup++;
                            continue;
                        }
                        InboundSalesParser::saveWeekVisitor($inbound, $crawledById);
                    } elseif (isset($inbound->timeAgo->months) AND !$checkAll) {
                        $shouldStop = true;
//                        var_dump('HERE!347');
                        break;
                    } elseif (isset($inbound->timeAgo->months) AND $checkAll) {
                        $isThisDup = InboundSalesParser::check4MoDuplicate($inbound, $crawledById);
                        if ($isThisDup) {
                            $moDup++;
                            continue;
                        }
                        InboundSalesParser::saveMoVisitor($inbound, $crawledById);
                    } else {
                        if (empty($inbound->fullName) OR $inbound->fullName === self::UNAVAILABLE) {
                            $unavailableInbound--;
                        } else {
                            $numberOfPublicInbound--;
                        }
                        $badInbound++;
                        continue;
                    }
                }
            } catch (Exception $e) {
                errorResponse($e->getMessage());
            }
        }
        $totalInbound = $numberOfPublicInbound + $hiddenInbound + $badInbound + $unavailableInbound;
        $originalViewers = $numberOfPublicInbound + $unavailableInbound;
        $isDup = ($gotDuplicate + $weeklyDup + $moDup) > 0 ? \TRUE : \FALSE;
        if ($curPage > $viewCount || count($json->viewers) == 0) {
            $shouldStop = true;
//            var_dump('HERE!386');
        }
//        if (isset($json->pagination) AND empty($json->viewers)) {
//            $shouldStop = true;
//            var_dump('HERE!389');
//        }


        //May be it's bug on LinkedIn, but there is possible not last page with less then max records per page.
//        if ($totalInbound < InboundSalesParser::INBOUND_VIEWS_PER_PAGE OR $isDup) {
//            $shouldStop = true;
//        }
        $response['WaitBetweenConnections'] = ParserModel::randomizeWait($wait);
        $response['latest_li_user_id_inbound_crawled_by'] = $payload->latest_li_user_id_inbound_crawled_by;
        $response['username'] = $crawledByName;
        $response['linked_in_id'] = $crawledById;
        $response['VisitedProfiles24'] = (int)$liUsers["visitsLast24Hours"];
        if (isset($payload->BadPage)) {
            $response['BadPage'] = \FALSE;
        }
//        $payload->html = '';
//        $payload->swiched_parse_area = $swichedParseArea;

        if ($shouldStop) {
            try {
                $response['inbound_visits_url'] = 0;
                $response['inbound_visit_page'] = -1;
                InboundSalesParser::privatesPostProcessing($crawledById, $arrDayPrivate, $arrPrivateVisits);
                InboundFreeModel::updateLiUserLastInboundCheck($crawledById);
            } catch (Exception $e) {
                errorResponse($e->getMessage());
            }
        } else{
            $response['inbound_visits_url'] = $payload->inbound_visits_url;
            $response['inbound_visit_page'] = $payload->inbound_visit_page;
            $response['check_all'] = $checkAll;
            $response['inbound_visit_count'] = $viewCount;
            $response['arr_private_visits'] = $arrPrivateVisits;
            $response['arr_day_private_visits'] = $arrDayPrivate;
        }
        //Only for testing
//        $payload->totalInbound = $totalInbound;
//        $payload->numberOfPublicInbound = $numberOfPublicInbound;
//        $payload->hiddenInbound = $hiddenInbound;
//        $payload->badInbound = $badInbound;
//        $payload->isDup = $isDup;
//        $payload->gotDuplicate = $gotDuplicate;
//        $payload->weeklyDup = $weeklyDup;
//        $payload->moDup = $moDup;
//        $payload->unavailableInbound = $unavailableInbound;
//        $payload->stop = $shouldStop;
//        return (array)$payload;
        return $response;
    }

    /**
     * @param $crawledById
     * @param $arrDayPrivate
     * @param $arrPrivateVisits
     * @return bool
     */
    public static function privatesPostProcessing($crawledById, $arrDayPrivate, $arrPrivateVisits)
    {
        //For days private records
        foreach ($arrDayPrivate as $key => $val) {
            $dayDate = $key;
            $vnDb = InboundSalesParser::getPrivateNumberOfDay($crawledById, $dayDate);
            $vnLinkedin = count($val);
            if ($vnDb < $vnLinkedin) {
                $diff = $vnLinkedin - $vnDb;
                for ($i = 0; $i < $diff; $i++) {
                    InboundSalesParser::savePrivateVisitors($val[$i], 1, $crawledById, $dayDate);
                }
            }
        }

        //Looking for records of private visits with the same time period and summarize them in new array
        $tempArr = [];
        foreach ($arrPrivateVisits as $key => $val) {
            $count = $arrPrivateVisits[$key]['count'];
            for ($i = $key + 1; $i < count($arrPrivateVisits); $i++) {
                if ($arrPrivateVisits[$key]['dayStart'] == $arrPrivateVisits[$i]['dayStart'] AND $arrPrivateVisits[$key]['dayEnd'] == $arrPrivateVisits[$i]['dayEnd']) {
                    $count += $arrPrivateVisits[$i]['count'];
                }
            }
            //constraint future parts of periods. set today as newer date
            if (strtotime($arrPrivateVisits[$key]['dayEnd']) > time()) {
                $arrPrivateVisits[$key]['dayEnd'] = date('Y-m-d', time());
            }
            $tempArr[] = [
                'count' => $count,
                'dayEnd' => $arrPrivateVisits[$key]['dayEnd'],
                'dayStart' => $arrPrivateVisits[$key]['dayStart']
            ];
        }
        //Compare with DB data and correct records about private visits (remove or add records into DB for related period)
        foreach ($tempArr as $key => $val) {
            //Get visits number in DB for period. dayStart - older
            $vnDb = InboundSalesParser::getPrivateNumberBetween($crawledById, $tempArr[$key]['dayStart'], $tempArr[$key]['dayEnd']);
            if ($vnDb < $tempArr[$key]['count']) {
                $diff = $tempArr[$key]['count'] - $vnDb;
                for ($i = 0; $i < $diff; $i++) {
                    InboundSalesParser::insertPrivateInboundVisits($crawledById, $tempArr[$key]['dayStart'], $tempArr[$key]['dayEnd']);
                }
            }
//            elseif ($vnDb > $tempArr[$key]['count']) {
//                $diff = $vnDb - $tempArr[$key]['count'];
//                for ($i = 0; $i < $diff; $i++) {
//                    InboundSalesParser::deletePrivateInboundVisits($crawledById, $tempArr[$key]['dayStart'], $tempArr[$key]['dayEnd']);
//                }
//            }
        }
        return \FALSE;
    }

    /**
     * @param $lastDayAgo - older one (ex: '2017-06-01')
     * @param $firstDayAgo - newer one (ex: '2017-06-30')
     * @return string
     */
    public static function getRandomPeriodDay($lastDayAgo, $firstDayAgo)
    {
        $diff = InboundSalesParser::daysDiff($lastDayAgo, $firstDayAgo);
        $randDay = rand(0, $diff);
        $resDate = date('Y-m-d', strtotime($firstDayAgo) - self::DAY * $randDay);
        return $resDate;
    }

    /**
     * @param $crawledById
     * @param $lastDayAgo
     * @param $firstDayAgo
     * @return bool
     */
    public static function deletePrivateInboundVisits($crawledById, $lastDayAgo, $firstDayAgo)
    {
        try {
            $qSelect = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE crawled_by_id = :crawled_by_id AND linked_in_id = 0 AND name = :name AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
            $qSelect->execute([
                'crawled_by_id' => $crawledById,
                'name' => self::NAME4PRIVATE,
                'lastDayAgo' => $lastDayAgo,
                'firstDayAgo' => $firstDayAgo
            ]);
            if ($qSelect->fetchColumn() > 0) {
                $qDelete = \DB::prep("DELETE FROM inbound_visit WHERE crawled_by_id = :crawled_by_id AND linked_in_id = 0 AND name = :name AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
                $qDelete->execute([
                    'crawled_by_id' => $crawledById,
                    'name' => self::NAME4PRIVATE,
                    'lastDayAgo' => $lastDayAgo,
                    'firstDayAgo' => $firstDayAgo
                ]);
            }
        } catch (Exception $e) {
            errorResponse($e->getMessage());
        }
        return \FALSE;
    }

    /**
     * @param $crawledById
     * @param $lastDayAgo - older
     * @param $firstDayAgo
     * @return bool|string
     * @throws Exception
     */
    public static function insertPrivateInboundVisits($crawledById, $lastDayAgo, $firstDayAgo)
    {
        try {
            $guessedVisitDate = InboundSalesParser::getRandomPeriodDay($lastDayAgo, $firstDayAgo);
            $insert = \DB::prep("INSERT INTO inbound_visit (crawled_by_id, linked_in_id, linked_in_hashed_id, name, industry_id, industry_name, headline, guessed_visit_date) VALUES(:crawled_by_id, :linked_in_id, :linked_in_hashed_id, :name, :industry_id, :industry_name, :headline, :guessed_visit_date)");
            $insert->execute([
                'crawled_by_id' => $crawledById,
                'linked_in_id' => 0,
                'linked_in_hashed_id' => '',
                'name' => self::NAME4PRIVATE,
                'industry_id' => 0,
                'industry_name' => '',
                'headline' => '',
                'guessed_visit_date' => $guessedVisitDate
            ]);
            if ($insert->rowCount() !== 1) {
                return \FALSE;
            }
            //it must be string
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $crawledById
     * @param $dayDate
     * @return mixed
     */
    public static function getPrivateNumberOfDay($crawledById, $dayDate)
    {
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = 0 AND crawled_by_id = :crawled_by_id AND guessed_visit_date=:guessed_visit_date");
        $query->execute([
            'crawled_by_id' => $crawledById,
            'guessed_visit_date' => $dayDate
        ]);
        return $query->fetchColumn();
    }

    /**
     * @param $crawledById
     * @param $lastDayAgo - dayStart - older
     * @param $firstDayAgo - dayEnd - newer
     * @return mixed
     */
    public static function getPrivateNumberBetween($crawledById, $lastDayAgo, $firstDayAgo)
    {
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = 0 AND crawled_by_id = :crawled_by_id AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
        $query->execute([
            'crawled_by_id' => $crawledById,
            'lastDayAgo' => $lastDayAgo,
            'firstDayAgo' => $firstDayAgo
        ]);
        return $query->fetchColumn();
    }

//    public static function addRandomPrivates2DB($nPrivates, $crawledById){
//        $rand = rand(1,89);
//        $gvd =
//        $query = \DB::prep("INSERT INTO inbound_visit (crawled_by_id, linked_in_id, name, guessed_visit_date) VALUES (:crawled_by_id, :linked_in_id, :name, :guessed_visit_date)");
//        $query->execute([
//            'crawled_by_id' => $crawledById,
//            'linked_in_id' => 0,
//            'name' => self::NAME4PRIVATE,
//            'guessed_visit_date' => $gvd
//        ]);
//    }

    /**
     * @param $date1 - more modern
     * @param $date2 - older
     * @return int - number of days
     */
    public static function daysDiff($date1, $date2)
    {
        $nDays = floor((strtotime($date1) - strtotime($date2)) / (self::DAY));
        return abs($nDays);
    }

    /**
     * @param $inbound
     * @param $hiddenCount
     * @param $crawledById
     * @param $dayStart
     * @param $dayEnd
     * @return bool
     */
    public static function savePrivateVisitors($inbound, $hiddenCount, $crawledById, $dayEnd, $dayStart = '')
    {
        for ($i = 0; $i < $hiddenCount; $i++) {
            if (!empty($dayStart AND $dayEnd)) {
                $daysDiff = InboundSalesParser::daysDiff($dayEnd, $dayStart);
                $randDay = $daysDiff > 0 ? rand(0, $daysDiff) : 0;
                $daysAgoEnd = floor(time() - strtotime($dayEnd));
                $guessedVisitDate = date('Y-m-d', time() - ($daysAgoEnd + self::DAY * $randDay));
            } elseif (empty($dayStart) and !empty($dayEnd)) {
                $guessedVisitDate = date('Y-m-d', strtotime($dayEnd));
            }
            $headline = $inbound->obfuscationString ?? '';
            $lastInsertId = InboundSalesParser::insertInbound($crawledById, 0, '', self::NAME4PRIVATE, 0, '', $headline, $guessedVisitDate);
            HtmlStorage::store(json_encode($inbound), "inserted_viewbacks", $crawledById, $lastInsertId);
        }
        return \FALSE;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function saveDayVisitor($inbound, $crawledById)
    {
        $guessedVisitDate = date('Y-m-d', time() - $inbound->timeAgo->days * (self::DAY));
        $headline = $inbound->headline ?? '';
        $lastInsertId = InboundSalesParser::insertInbound($crawledById, $inbound->memberId, '', $inbound->fullName, 0, '', $headline, $guessedVisitDate);
        HtmlStorage::store(json_encode($inbound), "inserted_viewbacks", $crawledById, $lastInsertId);
        return \FALSE;
    }

    /**
     * @param $data
     * @param $crawledById
     * @param $guessedVisitDate
     * @return bool
     */
    public static function saveVisitor($data, $crawledById, $guessedVisitDate)
    {
        $lastInsertId = InboundSalesParser::insertInbound($crawledById, $data['linked_in_id'] ?? 0, '', $data['name'] ?? '', 0, '', $data['headline'] ?? '', $guessedVisitDate);
        HtmlStorage::store(json_encode($data), "inserted_viewbacks", $crawledById, $lastInsertId);
        return \FALSE;
    }

    /**
     * @param $weekAgo int
     * @return false|string
     */
    public static function getRandomWeekDate($weekAgo)
    {
        $randDay = rand(0, 6);
        $rwDate = date('Y-m-d', time() - ($weekAgo * (self::WEEK) + self::DAY * $randDay));
        return $rwDate;
    }

    /**
     * @param $moAgo
     * @return false|string
     */
    public static function getRandomMoDate($moAgo)
    {
        $randDay = rand(0, 29);
        $rmDate = date('Y-m-d', time() - ($moAgo * (self::MONTH) + self::DAY * $randDay));
        return $rmDate;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function saveWeekVisitor($inbound, $crawledById)
    {
        $guessedVisitDate = InboundSalesParser::getRandomWeekDate($inbound->timeAgo->weeks);
        $headline = $inbound->headline ?? '';
        $lastInsertId = InboundSalesParser::insertInbound($crawledById, $inbound->memberId, '', $inbound->fullName, 0, '', $headline, $guessedVisitDate);
        HtmlStorage::store(json_encode($inbound), "inserted_viewbacks", $crawledById, $lastInsertId);
        return \FALSE;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function saveMoVisitor($inbound, $crawledById)
    {
        $guessedVisitDate = InboundSalesParser::getRandomMoDate($inbound->timeAgo->months);
        $headline = $inbound->headline ?? '';
        $lastInsertId = InboundSalesParser::insertInbound($crawledById, $inbound->memberId, '', $inbound->fullName, 0, '', $headline, $guessedVisitDate);
        HtmlStorage::store(json_encode($inbound), "inserted_viewbacks", $crawledById, $lastInsertId);
        return \FALSE;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     * @internal param $name
     */
    public static function check4DayDuplicate($inbound, $crawledById)
    {
        $guessedVisitDate = date('Y-m-d', time() - $inbound->timeAgo->days * (self::DAY));
        $select = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = :linked_in_id AND guessed_visit_date = :guessed_visit_date AND crawled_by_id = :crawled_by_id");
        $select->execute([
            'linked_in_id' => $inbound->memberId,
            'guessed_visit_date' => $guessedVisitDate,
            'crawled_by_id' => $crawledById
        ]);
        if ($select->fetchColumn() > 0) return \TRUE;
        return \FALSE;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function check4DayPrivate($inbound, $crawledById)
    {
        $guessedVisitDate = date('Y-m-d', time() - $inbound->timeAgo->days * self::DAY);
        $select = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = 0 AND guessed_visit_date = :guessed_visit_date AND crawled_by_id = :crawled_by_id AND name = :name");
        $select->execute([
            'guessed_visit_date' => $guessedVisitDate,
            'crawled_by_id' => $crawledById,
            'name' => self::NAME4PRIVATE
        ]);
        if ($select->fetchColumn() > 0) return \TRUE;
        return \FALSE;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function check4WeekPrivate($inbound, $crawledById)
    {
        $firstDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->weeks * (self::DAY * 7)));
        $lastDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->weeks * (self::DAY * 7) + self::DAY * 6));
        //Older date first
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = 0 AND crawled_by_id = :crawled_by_id AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
        $query->execute([
            'crawled_by_id' => $crawledById,
            'lastDayAgo' => $lastDayAgo,
            'firstDayAgo' => $firstDayAgo
        ]);
        $weeklyDup = $query->fetchColumn() > 0 ? \TRUE : \FALSE;
        return $weeklyDup;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function check4MoPrivate($inbound, $crawledById)
    {
        $firstDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->months * (self::MONTH)));
        $lastDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->months * (self::MONTH) + self::DAY * 29));
        //Older date first
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = :linked_in_id AND crawled_by_id = :crawled_by_id AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
        $query->execute([
            'linked_in_id' => 0,
            'crawled_by_id' => $crawledById,
            'lastDayAgo' => $lastDayAgo,
            'firstDayAgo' => $firstDayAgo
        ]);
        $weeklyDup = $query->fetchColumn() > 0 ? \TRUE : \FALSE;
        return $weeklyDup;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function check4WeeklyDuplicate($inbound, $crawledById)
    {
        $firstDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->weeks * (self::DAY * 7)));
        $lastDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->weeks * (self::DAY * 7) + self::DAY * 6));
        //Older date first
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = :linked_in_id AND crawled_by_id = :crawled_by_id AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
        $query->execute([
            'linked_in_id' => $inbound->memberId,
            'crawled_by_id' => $crawledById,
            'lastDayAgo' => $lastDayAgo,
            'firstDayAgo' => $firstDayAgo
        ]);
        $weeklyDup = $query->fetchColumn() > 0 ? \TRUE : \FALSE;
        return $weeklyDup;
    }

    /**
     * @param $inbound
     * @param $crawledById
     * @return bool
     */
    public static function check4MoDuplicate($inbound, $crawledById)
    {
        $firstDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->months * (self::MONTH)));
        $lastDayAgo = date('Y-m-d', time() - ($inbound->timeAgo->months * (self::MONTH) + self::DAY * 29));
        //Older date first
        $query = \DB::prep("SELECT COUNT(inbound_visit_id) FROM inbound_visit WHERE linked_in_id = :linked_in_id AND crawled_by_id = :crawled_by_id AND (guessed_visit_date BETWEEN :lastDayAgo AND :firstDayAgo)");
        $query->execute([
            'linked_in_id' => $inbound->memberId,
            'crawled_by_id' => $crawledById,
            'lastDayAgo' => $lastDayAgo,
            'firstDayAgo' => $firstDayAgo
        ]);
        $weeklyDup = $query->fetchColumn() > 0 ? \TRUE : \FALSE;
        return $weeklyDup;
    }
}