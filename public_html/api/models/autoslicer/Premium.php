<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 05.07.2017
 * Time: 14:37
 */
require_once(__DIR__ . '/Engine.php');

class PremiumAutoslicer extends AutoslicerEngine {
    
    protected function company_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetCurrentCompany', $splitsNeeded, $isSubsequentCall);
    }

    protected function geography_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetGeoRegion', $splitsNeeded, $isSubsequentCall);
    }

    protected function industry_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetIndustry', $splitsNeeded, $isSubsequentCall);
    }

    protected function relationship_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetNetwork', $splitsNeeded, $isSubsequentCall);
    }

    protected function nonprofit_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetNonprofitInterest', $splitsNeeded, $isSubsequentCall);
    }

    protected function past_company_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetPastCompany', $splitsNeeded, $isSubsequentCall);
    }

    protected function school_P_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_json_array_splitter($url, 'facetSchool', $splitsNeeded, $isSubsequentCall);
    }

    protected function setupSplitters($config, $results, $originalUrlRecord){
        //Strategies are arranged in order of results number
        
        $strategies = [
            'company_P', 
            'past_company_P', 
        ];
        
        foreach ($strategies as $facet){
            $results = $this->runStrategy($facet, $results);
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
        }
        
        //Keywords: OR splitting
        for($i=0; $i < $config['boolean_max_recursive_depth']; $i++){
            //in case boolean_max_recursive_depth > 1
            //this will be recursive walk into boolean expression
            $results = $this->runStrategy('keywords', $results);
            //IMPORTANT! this check for 'pass' is needed for all recursive strategies
            if ($results['pass']) break;
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
            
        }
        
        //non-recursive strategies
        $strategies = [
            'industry_P', 
            'geography_P',
            'school_P',
            'nonprofit_P',
            //intentionally put the last, very asymmetric split
            'relationship_P',   
        ];
        
        foreach ($strategies as $facet){
            $results = $this->runStrategy($facet, $results);
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
        }
        
        //control shot
        $this->saveUrlsOrPass($results, $originalUrlRecord, true);
        return false;
    }
}