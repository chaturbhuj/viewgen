<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 05.07.2017
 * Time: 14:37
 */
require_once(__DIR__ . '/../../../services/db_svc.php');
use function \services\db\{execute_prep, prep_insert};
use linkedinSearch\Url;


class AutoslicerEngine
{
    private $campaign = null;
    private $user = null;
    private $log_step = 1;
    private $runId = null;
    private $booleanParser = null;
    private $booleanRenderer = null;
    private $config = null;
    private $crawledById = null;
    private $crawledByName = null;
    private $clientVersion = null;
    private $steps_done = 0;
    private $steps_success = 0;
    
    public function __construct($config, $user)
    {
        $this->config = array_merge([
            //turn automatic planner for autoslicer
            'planner' => true,
            //Do only One Run
            //normally, only one run needed, except several origin URLs
            //but in this case Autoslicer will see all its generated split URLs in the next turn,
            //and other unpredictable conditions can lead with low probability to infinite slicing  
            'multipass' => true,
            //number of results of Search URL to treat it as splittable
            'split_threshold' => 1100,
            //configurePlanner() changes this to true ALWAYS
            //how many successful split steps to take in one autoslicer run
            //( how many times to run splitter(s) with successful split, non-splits are not counting)
            'split_steps_success_limit' => 1, 
            'split_steps_limit' => 50, 
            //configurePlanner() changes this
            'urls_needed_number' => 20.0,   //without auto planner split_steps/success_limit act as a constraint
            //maximum number of URL split to happen per one Autoslicer pass
            //important because many split URKs tend to have very non-uniform result numbers
            //more intermediate checks of numbers needed to point Slicer the right direction
            //Useful only inside the Planner
            'urls_needed_limit' => 20.0, 
            //limits number of splits per strategy
            //experimental feature, potentially low numbers might increase autoslicer efficiency,
            //because splits will be distributed over more strategies,
            //and more autoslicer passes will be taken so result numbers will be more accurate
            // because of results number calculation in between 
            //--> affects Planner behaviour
            'splits_per_strategy' => null,
            //configurePlanner() changes this
            'boolean_max_recursive_depth' => 5,
            //Threshold (multiplier * number of URLs on the total visits number done on the all campaign's URLs split at the moment,
            //after wich
            //where to stop return "false" back to continue with GetNextUrl,
            //and start autoslicing again
            //and then, after another autoslicer session completes,
            //new splitted URLs come, and in product with this threshold multiplier they give bigger threshold,
            //and visiting process take lead over autoslicing again
            'avtoslicer_vs_visiting_balance_multiplier' => 10
        ], $config);

        $this->user = $user;
        
        $this->booleanParser = new Icecave\Dialekt\Parser\ExpressionParser(null, false);
        $this->booleanRenderer = new Icecave\Dialekt\Renderer\ExpressionRenderer;
        
    }

    private function initialize($clientHash, $crawledById, $crawledByName, $clientVersion){
        $this->log_step = 1;
        
        $this->steps_done = 0;
        $this->steps_success = 0;
        
        $this->crawledById = $crawledById;
        $this->crawledByName = $crawledByName;

        $companyData = APIModel::getCompanyByProductKey($clientHash);
        $companyId = $companyData['id'];
        // Fetch the latest campaign.
        $this->campaign = APIModel::getPublishedCampaign($companyId, $crawledById);
        $this->clientVersion = $clientVersion;

    }

    private function initialChecksAndRun($clientHash, $crawledById, $crawledByName, $clientVersion){
        if (!$this->campaign){
            return false;
        }
        //TODO MAYBE MOVE actually this check is not logical part of the autoslicer itself
        $firstUncheckedUrlResponse = $this->searchForUncheckedUrls();
        if ($firstUncheckedUrlResponse !== false) {
            //create a Run record and note finishing before the split_phase
            $this->runId = $this->getLastAutoslicerRunOrCreate(
                $this->campaign, $clientHash, $crawledById, $crawledByName, $clientVersion,
                'no__unchecked'); 

            $this->logStep(
                'unchecked_visits__EXIT',
                null, 
                null,
                null,
                $firstUncheckedUrlResponse['url'] ?? '', 
                [], 
                [],
                json_encode($firstUncheckedUrlResponse)
            );
            
            //pass check signal
            //it prevents autoslicing before all checks done
            return $firstUncheckedUrlResponse;
        }
        
        //now, if autoslicing is finished for this campaign
        //just proceed with normal getNextUrl procedure
        if ($this->isSlicingFinalized($this->campaign['id'])){
            //do not log, do not create a new run, it is too common, it can be found via autoslicer_run records
            return false;
        }
        
        // In case of multiple Autoslicer runs,
        // Check for non splitters before entering
        // to allow parallel visits checking and slicing,
        // because slicing can be long or even buggy-endless in this case
        // This is taken from legacy parallel logic, and is NOT needed in case of one run
        if ($this->config['multipass'] && 
            $this->hasNonSplitUrlsForParallelVisiting($this->campaign['id'])
        ) {
            //create a Run record and note finishing before the split_phase
            //TODO too much logs
//            $this->runId = $this->getLastAutoslicerRunOrCreate(
//                $this->campaign, $clientHash, $crawledById, $crawledByName, $clientVersion,
//                'no__visiting'); 
//            $this->logStep(
//                'autoslice__visit_non_split_first'
//            );
            return false;
        }

        //create a Run record and note that split_pase is entered
        $this->runId = $this->createAutoslicerRun(
            $this->campaign, $clientHash, $crawledById, $crawledByName, $clientVersion,
            'yes'); //enter split_pase

        return true;

    }
    
    /**
     * Main entry point
     * 
     * @param $clientHash
     * @param $crawledById
     * @param $crawledByName
     * @param $clientVersion
     * @return array|bool
     */
    public function autoslice($clientHash, $crawledById, $crawledByName, $clientVersion)
    {
        $this->initialize($clientHash, $crawledById, $crawledByName, $clientVersion);
        
        // ******************************* initial checks and Run record  ************************************************************
        $checkResponse = $this->initialChecksAndRun($clientHash, $crawledById, $crawledByName, $clientVersion);
        if ($checkResponse !== true) return $checkResponse;
        //****************************** main algorithm starts *************************************************************************

        // Check for splitters.
        $originalUrlRecord = $this->getFirstSplittableUrl($this->campaign['id']);
        if ($originalUrlRecord) {
            
            $originalUrl = $originalUrlRecord['url'];
            
            //****************************** planner *****************************
            if ($this->config['planner']) $this->configurePlanner($originalUrlRecord);
            //*********************************************************************
            
            $this->logStep(
                'autoslice__split_ENTER',
                null, 
                null,
                null,
                $originalUrl,
                [],
                [],
                "CONFIG: " . var_export($this->config, true)
            );
            
            // Split algorithm.
            //********************** results initialization *******************
            $results = [
                'urls' => [ $originalUrl ], //initial split set is one original URL
                'split' => false, //have not split anything yet
                'pass' => false, //pass to the next splitter, in case of repetitive calls without results
                'splitter' => null,  //current splitter is not set yet
            ];

            //********************* core logic: splitters ************************************
            
            return $this->setupSplitters($this->config, $results, $originalUrlRecord);

            //*************************** end of splitters ***********************
            
        }else{
            //no more URLs to split - finalize
            $this->finalizeAutoslicerRun();
            $this->logStep(
                'autoslice__FINALIZED',
                null, 
                null,
                null,
                null,
                [],
                [],
                "original CONFIG: " . var_export($this->config, true)
            );
    
        }
        return false;
    }


    protected function setupSplitters($config, $results, $originalUrlRecord){
        throw new Exception('setupSplitters(): Implement me!!!');
        return false;
    }
    
    
    protected function saveUrlsOrPass($results, $originalUrlRecord, $end = false){
        $newUrls = $results['urls'];
        $size = count($newUrls);
        
        if ($end || 
            $this->steps_done >= $this->config['split_steps_limit'] || 
            $this->steps_success >= $this->config['split_steps_success_limit'] || 
            $size >= $this->config['urls_needed_number']) {
            
            if (count($newUrls) > 1) {
                foreach ($newUrls as $newUrl) {
                    APIModel::insertSearchUrl($originalUrlRecord['campaign_id'], 
                        $originalUrlRecord['company_id'], $newUrl, $originalUrlRecord['visit_by'], 
                        $originalUrlRecord['visit_by_id'], $originalUrlRecord['id']);
                }
                $this->deactivateOriginalSearchURL($originalUrlRecord['id']);
                $this->logStep(
                    'save__DONE_finish_url',
                    null, 
                    null,
                    null,
                    $originalUrlRecord['url'],
                    [],
                    $newUrls,
                    'RESULTS: ' . json_encode($results)
                );
                
            } else {
                $this->updateSearchUrlToNonSplit($originalUrlRecord['id']);
                $this->logStep(
                    'save__FAILED_set_NON_split',
                    null, 
                    null,
                    null,
                    $originalUrlRecord['url'],
                    [],
                    [],
                    'RESULTS: ' . json_encode($results)
                );
            }
            
            if (! $this->config['multipass']){
                $this->finalizeAutoslicerRun();
                $this->logStep(
                    'save__FINALIZED_NON_multipass'
                );
            }
            
            return true;    
        }
        
        return false; //pass
    }

    /**
     * Deactivates original search ULR if it was splitted
     * rewrite of APIModel::updateSearchUrlPauseAndFinish(), latter was removed as unused anywhere else
     * @param $searchUrlId
     * @return bool false
     * @throws Exception
     */
    private function deactivateOriginalSearchURL($searchUrlId)
    {
        try {
            $query = \DB::prep("UPDATE search_url SET paused = 1, finished = 1, search_url_can_be_splitted = 0 
                WHERE id = :search_url_id LIMIT 1");
            $query->execute([
                'search_url_id' => $searchUrlId
            ]);
        } catch (Exception $e) {
            try {
                $this->logStep(
                    'ERROR', 'deactivateOriginalSearchURL()', '', null, '', [], [],
                    var_export($e, true),
                    null
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
        return false;
    }

    /**************************************************************************************************************
     * This function makes planning of the strategy needed
     * **************************************************************************************************************
     * @param $originalUrlRecord
     */
    private function configurePlanner($originalUrlRecord){
        $threshold = $this->config['split_threshold'];
        $total = $originalUrlRecord['total_matches'];
        
        if ($total > $threshold){ //this should be always true on this stage though
            //it is float, and will be converted to int at the end numbers of urls per splitter
            $urlsNeeded = (float)$total / (float)$threshold;
            
            //reasonable limit to take not too much steps per pass
            //this value will not be reashed in normal circumstances, it is just fault-harness
            $this->config['split_steps_limit'] = 100;
            
            //more then 20 splits give too ununiform splits
            //each strategy run gives one split step (successful or not)
            //some strategies (like keywords) may be run recursively
            $this->config['split_steps_success_limit'] = 20;
            
            $this->config['urls_needed_number'] = min($urlsNeeded, $this->config['urls_needed_limit']);
            
            //set big recursive limit but not too big in case of super weird URLs
            $this->config['boolean_max_recursive_depth'] = 20;
        }
    }
    
    private function searchForUncheckedUrls()
    {
        // Check if we have a search url where we did not check for visits.
        $searchUrl = APIModel::getSearchUrlUnchecked4Visits($this->campaign['id']);
        if ($searchUrl) {
            $url = $searchUrl['url'];

            $url = Url::convertUrl($url, $this->user);

//            if (strpos($url, "start=") === false) {
//                $url = str_replace("?", "?count=25&start=0&", $url);
//            }
            // Insert a people list.
            $peopleListId = APIModel::insertPeopleList($searchUrl['id'], 1, $url);

            $data = [
                'url' => $url,
                'people_list_id' => $peopleListId,
                'campaign_id' => $this->campaign['id'],
                'username' => $this->crawledByName,
                'linked_in_id' => $this->crawledById,
                'WaitBetweenConnections' => 10,
                'status_message' => [
                    'status' => 3,
                    'message' => 'Running Campaign "'.$this->campaign['name'].'": Building Profile List'
                ]
            ];

            if(!empty($this->user['search_design_version']) && $this->user['search_design_version'] == 2){
                $data['javaScript'] = 'window.scrollTo(0,document.body.scrollHeight);
                function sleep(milliseconds) {
                  var start = new Date().getTime();
                  for (var i = 0; i < 1e7; i++) {
                    if ((new Date().getTime() - start) > milliseconds){
                      break;
                    }
                  }
                }
                sleep(10000);
                console.log(new Date());
                ';
            }

            return $data;
        }
        return false;
    }

    /**
     * Check for non split URLs can be visited in pseudo-parallel mode
     * with further (recursive) Autoslicer passes
     * avtoslicer_vs_visiting_balance_multiplier - is the threshold when control passed back to autoslicer
     * (after a URL got some visits already)
     *
     * @param $campaignId
     * @return bool
     * @throws Exception
     */
    private function hasNonSplitUrlsForParallelVisiting($campaignId)
    {
        try {
            //search_url_can_be_splitted = 0 - means this URL should be considered as NON-split disregarding of total_matches
            $query = \DB::prep("
                SELECT 
                  id 
                FROM 
                  search_url 
                WHERE 
                  campaign_id = :campaign_id
                  AND paused = 0
                  AND finished = 0
                  AND (
                        (total_matches <= :split_threshold AND total_matches > 0)
                        OR search_url_can_be_splitted = 0
                      ) 
                  AND (
                        select SUM(search_url_visit_count) from search_url WHERE campaign_id = :campaign_id AND paused = 0
                      ) < :multiplier * (
                                          select count(*) from search_url WHERE campaign_id = :campaign_id AND paused = 0 AND
                                          ((total_matches <= :split_threshold AND total_matches > 0) OR search_url_can_be_splitted = 0)
                                        )   
                ORDER BY 
                  total_matches DESC 
                LIMIT 1
		    ");
            $query->execute([
                'campaign_id' => $campaignId,
                'split_threshold' => (int)$this->config['split_threshold'],
                'multiplier' => (int)$this->config['avtoslicer_vs_visiting_balance_multiplier']
            ]);
            if ($query->rowCount() > 0) {
                return true;
            }
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'hasNonSplitUrlsForParallelVisiting()', '', null, '', [], [],
                    var_export($e, true), 
                    null 
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
        return false;
    }


    /**
     * Check for splitters. Get search_url
     * @param $campaignId
     * @return array|bool
     * @throws Exception
     */
    private function getFirstSplittableUrl($campaignId)
    {
        try {
            $query = \DB::prep(
                "SELECT * FROM search_url WHERE campaign_id = :campaign_id AND paused = 0 AND finished = 0 AND
		              total_matches > :split_threshold AND search_url_can_be_splitted = 1 
		              ORDER BY total_matches DESC LIMIT 1");
            execute_prep($query, [
                'campaign_id' => $campaignId,
                'split_threshold' => (int)$this->config['split_threshold'],
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            $result = $query->fetch();

            $result['url'] = Url::convertUrl($result['url'], $this->user);

            return $result;
            
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'getFirstSplittableUrl()', '', null, '', [], [],
                    var_export($e, true), 
                    null 
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
    }


    /**
     * Get last autoslicer_run record
     * @param $campaign
     * @param $clientHash
     * @param $crawledById
     * @param $crawledByName
     * @param $clientVersion
     * @param $splitPhase
     * @return array|bool
     * @internal param $campaignId
     */
    private function getLastAutoslicerRunOrCreate(
        $campaign, $clientHash, $crawledById, $crawledByName, $clientVersion, $splitPhase)
    {
        $id = false;
        try {
            $query = \DB::prep(
                "SELECT * FROM autoslicer_run WHERE campaign_id = :campaign_id 
                  ORDER BY created DESC LIMIT 1");
            execute_prep($query, [
                'campaign_id' => $campaign['id']
            ]);
            if ($query->rowCount() > 0) {
                $id = $query->fetch()['id'];
            }
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'getLastAutoslicerRunOrCreate()', '', null, '', [], [],
                    var_export($e, true),
                    null
                );
            } catch(Exception $e2){}
            
            //do not throw, create new record instead
        }
        if ($id === false){
            $id = $this->createAutoslicerRun($campaign, $clientHash, $crawledById, $crawledByName, $clientVersion, $splitPhase);
        };
        return $id;
    }


    /**
     * Check if auto-slicing is finalized
     * @param $campaignId
     * @return array|bool
     * @throws Exception
     */
    private function isSlicingFinalized($campaignId)
    {
        try {
            $query = \DB::prep(
                "SELECT id, split_phase FROM autoslicer_run WHERE campaign_id = :campaign_id AND final >= created");
            execute_prep($query, [
                'campaign_id' => $campaignId
            ]);
            if ($query->rowCount() > 0) {
                return true;
            }
        } catch (Exception $e) {
            try {
                $this->logStep(
                    'ERROR', 'isSlicingFinalized()', '', null, '', [], [],
                    var_export($e, true),
                    null
                );
            }catch(Exception $e2){}
            
            throw $e;
        }
        return false;
    }


    /**
     * @param $url
     * @return string
     */
    protected static function getKeywords($url)
    {
        $params = self::getParameters($url);
        return rawurldecode(!empty($params['keywords']) ? $params['keywords'][0] : '');
    }

    
    protected static function setKeywords($url, $keywords)
    {
        $keywords = rawurlencode($keywords);
        
        if (strpos($url, 'keywords=') === false) {
            $url .= '&keywords=' . $keywords;
        }else{
            $url = preg_replace('/([&\?]keywords\s*=)[^&]*/', '${1}'.$keywords, $url);
        }
        return $url;
    }

    
    protected static function getParameters($url)
    {
        $parts = explode("?", $url, 2);
        $query_string = $parts[1];
        $parts = explode("&", $query_string);
        $params = [];
        foreach ($parts as $part){
            $part = explode('=', $part);
            $name = trim($part[0]);
            $value = trim(!empty($part[1]) ? $part[1] : '');
            if (! isset($params[$name])) $params[$name] = [];
            $params[$name][] = $value;
        }
        return $params;
    }


    /**
     * @param $url
     * @param $filter
     * @return array
     */
    protected static function getFacets($url, $filter)
    {
        $params = self::getParameters($url);
        
        $facets = [];
        foreach ($params as $name => $values) {
            foreach($values as $pureName){
                if ($name == 'facet' && in_array($pureName, $filter)) {
                    $facets[$pureName] = [];
                }
            }
        }

        foreach ($facets as $pureName => $_) {
            $facetValueName = 'facet.' . $pureName;
            if (isset($params[$facetValueName])) {
                $facets[$pureName] = $params[$facetValueName];
            }
        }

        return $facets;
    }


    /**
     * @param $url
     * @param $facets
     * @return string
     */
    protected static function addFacets($url, $facets)
    {
        foreach ($facets as $facet => $values) {
            if (count($values) > 0) {
                //check for duplications
                if (strpos($url, 'facet=' . $facet) === false) {
                    $url .= '&facet=' . $facet;
                }
                foreach ($values as $value) {
                    $value = (string)$value;
                    //check for duplications
                    if (strpos($url, 'facet.' . $facet . '=' . $value) === false) {
                        $url .= '&facet.' . $facet . '=' . $value;
                    }
                }
            }
        }
        return $url;
    }

    protected static function addParams($url, $params)
    {
        foreach ($params as $name => $values) {
            if (count($values) > 0) {
                foreach ($values as $value) {
                    //check for duplications
                    if (strpos($url, $name . '=' . $value) === false) {
                        $url .= '&' . $name . '=' . $value;
                    }
                }
            }
        }
        return $url;
    }

    protected static function deleteFacets($url, $facets)
    {
        foreach ($facets as $type) {
            $url = preg_replace('/(&?)(\??)facet\s*=\s*'.$type.'/', '${2}', $url);
            $url = preg_replace('/(&?)(\??)facet\.'.$type.'\s*=[^&]*/', '${2}', $url);
            $url = str_replace('?&', '?', $url);
        }
        return $url;
    }
    
    
    protected static function deleteParam($url, $param)
    {
        $param = str_replace('.', '\.', $param);
        $url = preg_replace('/(&?)(\??)'.$param.'\s*=[^&]*/', '${2}', $url);
        $url = str_replace('?&', '?', $url);
        return $url;
    }
    
    
    protected function runStrategy($splitter, $results){
        $newUrlSet = [];
        $split = false;
        $isSubsequentCall = $results['splitter'] === $splitter;
            
        //check for force pass
        if ($results['pass'] && $isSubsequentCall) return $results;

        $urlsNeededNumber = (float)$this->config['urls_needed_number'];
        
        //we are calculating how much times we need to split each url from the 
        //incoming $results['urls'] in the next coming splitter
        //to get ideal distribution for all urls < 'split_threshold'
        //btu we can encounter NON-split cases, this should happen for all URLs
        $calculatedSplitsNeeded = (float)$urlsNeededNumber / (float)count(($results['urls']));
        
        //now we try to take fixed number of splits per splitter from config first,
        //otherwise using calculated estimated value
        if (isset($this->config['splits_per_strategy']) && $this->config['splits_per_strategy'] > 0){
            $splitsNeeded = min($this->config['splits_per_strategy'], $calculatedSplitsNeeded);
        } else {
            $splitsNeeded = $calculatedSplitsNeeded;
        }
        
        //normalize splits number to not split too many small-sized urls at the last step
        $normalizedSplitsNeeded = $this->normalizeSplitsNeeded($splitsNeeded);
        
        //check if we rerun the same splitter (so doing recursive splits)
        //and previous results are non-splits
        //in this case we can simply skip
        if (! $results['split'] && $isSubsequentCall) {
            $this->logStep(
                'strategy__PASS_after_NON_split',
                $splitter, 
                null,
                $splitsNeeded,
                null,
                $results['urls'],
                [],
                '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                    ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                $isSubsequentCall
            );
            
            $results['pass'] = true;
            
            return $results;
        
        } else {
            $this->logStep(
                'strategy__ENTER',
                $splitter, 
                null,
                $splitsNeeded,
                null,
                $results['urls'],
                [],
                '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                    ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                $isSubsequentCall
            );
        }

        if ($normalizedSplitsNeeded > 1.0) { 

            foreach ($results['urls'] as $url) {
                //********************** Concrete Splitter algorithm execution ************************
                $splitterResults = $this->{$splitter . '_splitter'}(
                    $url, 
                    $normalizedSplitsNeeded, 
                    $isSubsequentCall
                );
                //**************************************************************************************

                $urls = $splitterResults[0];
                $splitOn = $splitterResults[1];

                if (count($urls) > 0) {
                    //add new urls, and retire original one were split
                    $newUrlSet = array_merge($newUrlSet, $urls);
                    $split = true; //we have at least some splits

                    $this->logStep(
                        'strategy__SPLIT',
                        $splitter,
                        $splitOn,
                        $splitsNeeded,
                        $url,
                        [],
                        $urls,
                        '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                            ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                        $isSubsequentCall
                    );
                } else {
                    //Theoretically, if at least one url from the $results['urls']
                    //is NON-split, this implies all $result['urls'] are NON-split
                    //that should follow from the same other parameters on each split URL
                    
                    //But in case of auto-planner, we should hardly ensure, we don't split anything from the set, 
                    //if at least one is NON-split, because otherwise keeping original urls
                    //will break splitting homogeneity and prediction url numbers will not work for splitting planning
                    if ($this->config['planner']){
                        $newUrlSet =  $results['urls'];
                        $split = false;
                        
                        $this->logStep(
                            'strategy__NON_split_STOP_planner',
                            $splitter,
                            $splitOn,
                            $splitsNeeded,
                            $url,
                            [],
                            [],
                            '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                                ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                            $isSubsequentCall
                        );
                        break;
                    }
                    
                    //else we can just preserve original url that has appeared NON-split
                    //and try to split other siblings from this URL set
                    $newUrlSet[] = $url;

                    $this->logStep(
                        'strategy__NON_split_pass',
                        $splitter,
                        $splitOn,
                        $splitsNeeded,
                        $url,
                        [],
                        [],
                        '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                            ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                        $isSubsequentCall
                    );
                }
            }
            
            $this->logStep(
                'strategy__full_RESULTS',
                $splitter, 
                null,
                $splitsNeeded,
                null, 
                $results['urls'], 
                $newUrlSet,
                '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                    ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                $isSubsequentCall
            );

        }else{
            $newUrlSet = $results['urls'];
            
            //needed number of urls already split
            //saveUrlsOrPass() will catch this up further
            $this->logStep(
                'strategy__splits_GATHERED',
                $splitter, 
                null,
                $splitsNeeded,
                null, 
                $newUrlSet, 
                [],
                '$calculatedSplitsNeeded = ' . (string)$calculatedSplitsNeeded . 
                    ', $normalizedSplitsNeeded = ' . (string)$normalizedSplitsNeeded,
                $isSubsequentCall
            );
        }        
        
        $this->steps_done = $this->steps_done + 1;
        $this->steps_success = $this->steps_success + ($split === true ? 1 : 0);

        return [
            'splitter' => $splitter,
            'urls' => $newUrlSet,
            'split' => $split,
            'pass' => false
        ];
    }


    private function normalizeSplitsNeeded($splitsNeeded){
        //take 1 for all cases < 1.5, this decreases the number of too small split URLs
        //at the last step of splitting in a run
        return $splitsNeeded >= 1.5 ? $splitsNeeded : 1.0;
    }
    
    private function keywords_splitter($url, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];

        $keywords = self::getKeywords($url);
        
        $renderer = $this->booleanRenderer;
        
        if (strlen($keywords) > 0){
            $expression = $this->booleanParser->parse($keywords);
        
            if ($expression instanceof Icecave\Dialekt\AST\LogicalOr){
                //this is top-OR expression
                $children = $expression->children();
                //we take $this->normalizeSplitsNeeded() because in case of e.g. 1.15 splits needed we should take 1
                //to not get more splits then expected.
                //better to have less splits in this case, and split again in the next Autoslicer pass
                $clausesPerSplit = ceil((float)count($children) / $splitsNeeded);
                
                $compiledClauses = array_map(function ($c) use($renderer){
                    //render back sub-expressions as strings
                    //to save it to subsequent URLs
                    return $renderer->render($c);
                }, $children);
                
                for ($i = 0; $i < count($compiledClauses); $i += $clausesPerSplit){
                    $newKeywords = array_slice($compiledClauses, $i, $clausesPerSplit);
                    //implode clauses thru "OR" - so we'll have semi-or-full-split set of ORed expressions
                    $newUrlSet[] = AutoslicerEngine::setKeywords($url, implode(' OR ', $newKeywords));
                }

                $this->logStep(
                    'splitter__keywords__OR',
                    'keywords', 
                    $keywords,
                    $splitsNeeded, 
                    $url, 
                    [], 
                    $newUrlSet,
                    '',
                    $isSubsequentCall
                );
        
            } else {
                $result = $this->_keywords_trySkipLogicSplit($expression, $splitsNeeded);
                if ($result !== false){
                    $parts = $result[0];
                    foreach ($parts as $part){
                        $newUrlSet[] = AutoslicerEngine::setKeywords($url, $part);
                    }
                    $this->logStep(
                        'splitter__keyword_SKIPS',
                        'keywords', 
                        $keywords,
                        $splitsNeeded,
                        $url, 
                        [], 
                        $newUrlSet,
                        'INCLUDES: '. $result[1] . ' SKIPS: ' . $result[2],
                        $isSubsequentCall
                    );
                }
            }
            
        }
        return [$newUrlSet, $keywords];
    }

    private function _keywords_trySkipLogicSplit($expression, $splitsNeeded){
        $renderer = $this->booleanRenderer;
        //this one is needed for SKIP logic splitting in case first part is OR expression
        if ($expression instanceof Icecave\Dialekt\AST\LogicalAnd){
            $children = $expression->children();
            if (count($children) === 2 && $children[1] instanceof Icecave\Dialekt\AST\LogicalNot){
                $skips = $children[1]->child();  //has only one child and only this method
                $includes = $children[0];
                
                if ($includes instanceof Icecave\Dialekt\AST\LogicalOr){
                    $children = $includes->children();
                    $clausesPerSplit = ceil((float)count($children) / $splitsNeeded);

                    $compiledClauses = array_map(function ($c) use($renderer){
                        //render back sub-expressions as strings
                        //to save it to subsequent URLs
                        return $renderer->render($c);
                    }, $children);
                    
                    $orClauses = [];
                    for ($i = 0; $i < count($compiledClauses); $i += $clausesPerSplit){
                        $clause = array_slice($compiledClauses, $i, $clausesPerSplit);
                        //implode clauses thru "OR" - so we'll have semi-or-full-split set of ORed expressions
                        $orClauses[] = '(' . implode(' OR ', $clause) . ')';
                    }
                    
                    $or_array = [];
                    $skipsStr = $renderer->render($skips);
                    
                    foreach ($orClauses as $oc) {
                        $or_array[] = $oc . ' AND NOT ' . $skipsStr;
                    }

                    return [$or_array, $renderer->render($includes), $skipsStr];
                }
            }
                
        }
        return false;
    }
    
    
    protected function _experience_years_splitter($url, $type, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];
        
        $facets = self::getFacets($url, explode(',', $type));
        
        $splitParameter = json_encode($facets);  //facets to split on

        //we do EXACTLY 5 split for each possible year range facet value,
        // NOT min(5, $splitsNeeded), because this split
        //is synthetic, and its distribution should cover all years of experience ranges
        //to not loose the results
        $facetsNumber = 5;
        
        if (!isset($facets[$type]) || count($facets[$type]) == 0) {
            for ($value = 1; $value <= $facetsNumber; $value++) {
                $facets[$type] = [$value];
                $newUrlSet[] = AutoslicerEngine::addFacets($url, $facets);
            }
            $this->logStep(
                'splitter__split',
                'exp_years_' . $type, 
                $splitParameter,
                $splitsNeeded,
                $url, 
                [], 
                $newUrlSet,
                'fixed 5 splits DONE', 
                $isSubsequentCall
            );
            
        }//else do not touch to not break
        
        return [$newUrlSet, $splitParameter];
    }

    protected function _experience_years_array_splitter($url, $type, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];

        $params = self::getParameters($url);
        $entitiesString = urldecode(($params[$type] ?? [''])[0]);

        $splitOnParameter = $entitiesString;  //facets to split on

        $entities = $entitiesString ? explode(',', $entitiesString) : [];

        //we do EXACTLY 5 split for each possible year range facet value,
        // NOT min(5, $splitsNeeded), because this split
        //is synthetic, and its distribution should cover all years of experience ranges
        //to not loose the results
        $facetsNumber = 5;
        if (empty($entities)) {
            for ($value = 1; $value <= $facetsNumber; $value++) {
                $newUrlSet[] = AutoslicerEngine::addParams($url, ["$type" => [urlencode($value)]]);
            }
            $this->logStep(
                'splitter__split',
                'exp_years_array_' . $type,
                $splitOnParameter,
                $splitsNeeded,
                $url,
                [],
                $newUrlSet,
                'fixed 5 splits DONE',
                $isSubsequentCall
            );

        }//else do not touch to not break

        return [$newUrlSet, $splitOnParameter];
    }

    /**
     * Entity splitter like
     *  &companyEntities=Atari_4574 ...
     *  &jobTitleEntities=director ...
     * @param $url
     * @param $entityName
     * @param $splitsNeeded
     * @param $isSubsequentCall
     * @return array
     */
    protected function _entity_splitter($url, $entityName, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];
        $params = self::getParameters($url);
        $entities = $params[$entityName] ?? [];

        $splitOnParameter = json_encode($entities);  //facets to split on
        $maxSplits = count($entities);

        if ($maxSplits > 0){
            $newUrl = self::deleteParam($url, $entityName);

            //if more then max splits needd it will be 1
            $entitiesPerSplit = ceil((float)$maxSplits / $splitsNeeded);
            
            for ($i = 0; $i < $maxSplits; $i += $entitiesPerSplit){
                $values = array_slice($entities, $i, $entitiesPerSplit);
                $newUrlSet[] = AutoslicerEngine::addParams($newUrl, ["$entityName" => $values]);
            }
            
            $this->logStep(
                'splitter__split',
                'entity_' . $entityName, 
                $splitOnParameter,
                $splitsNeeded,
                $url, 
                [], 
                $newUrlSet,
                'Original url with stripped entities: ' . $newUrl . 
                    "|________| Entities per split " . (string)$entitiesPerSplit, 
                $isSubsequentCall 
            );
        }
        return [$newUrlSet, $splitOnParameter];
    }
    
    /**
     * @param $url
     * @param $entityName
     * @param $splitsNeeded
     * @param $isSubsequentCall
     * @return array
     */
    protected function _entity_json_array_splitter($url, $entityName, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];
        $params = self::getParameters($url);
        $entitiesString = trim(urldecode(($params[$entityName] ?? [''])[0]));

        $splitOnParameter = $entitiesString;  //facets to split on
        
        $entities = json_decode($entitiesString, true);
        
        $maxSplits = !empty($entities) ? count($entities) : 0;

        if ($maxSplits > 0){
            $newUrl = self::deleteParam($url, $entityName);

            //if more then max splits needd it will be 1
            $entitiesPerSplit = ceil((float)$maxSplits / $splitsNeeded);
            
            for ($i = 0; $i < $maxSplits; $i += $entitiesPerSplit){
                $valuesString = json_encode(array_slice($entities, $i, $entitiesPerSplit));
                $newUrlSet[] = AutoslicerEngine::addParams($newUrl, ["$entityName" => [urlencode($valuesString)]]);
            }
            
            $this->logStep(
                'splitter__split',
                'entity_json_array_' . $entityName,
                $splitOnParameter,
                $splitsNeeded,
                $url, 
                [], 
                $newUrlSet,
                'Original url with stripped entities: ' . $newUrl . 
                    "|________| Entities per split " . (string)$entitiesPerSplit, 
                $isSubsequentCall 
            );
        }
        return [$newUrlSet, $splitOnParameter];
    }

    /**
     * @param $url
     * @param $entityName
     * @param $splitsNeeded
     * @param $isSubsequentCall
     * @return array
     */
    protected function _entity_array_splitter($url, $entityName, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];
        $params = self::getParameters($url);
        $entitiesString = trim(urldecode(($params[$entityName] ?? [''])[0]));

        $splitOnParameter = $entitiesString;  //facets to split on

        $entities = !empty($entitiesString) ? explode(',', $entitiesString) : [];

        $maxSplits = count($entities);

        if ($maxSplits > 0){
            $newUrl = self::deleteParam($url, $entityName);

            //if more then max splits needd it will be 1
            $entitiesPerSplit = ceil((float)$maxSplits / $splitsNeeded);

            for ($i = 0; $i < $maxSplits; $i += $entitiesPerSplit){
                $valuesString = implode(',', array_slice($entities, $i, $entitiesPerSplit));
                $newUrlSet[] = AutoslicerEngine::addParams($newUrl, ["$entityName" => [urlencode($valuesString)]]);
            }

            $this->logStep(
                'splitter__split',
                'entity_array_' . $entityName,
                $splitOnParameter,
                $splitsNeeded,
                $url,
                [],
                $newUrlSet,
                'Original url with stripped entities: ' . $newUrl .
                "|________| Entities per split " . (string)$entitiesPerSplit,
                $isSubsequentCall
            );
        }
        return [$newUrlSet, $splitOnParameter];
    }

    protected function _facets_splitter($url, $type, $splitsNeeded, $isSubsequentCall){
        $newUrlSet = [];
        
        $facets = self::getFacets($url, explode(',', $type));
        
        $splitOnParameter = json_encode($facets);  //facets to split on
        
        if (isset($facets[$type]) && count($facets[$type]) > 0) {
            $newUrl = self::deleteFacets($url, [$type]);
            
            $maxSplits = count($facets[$type]);

            //if more then max splits need it will be 1
            $facetsPerSplit = ceil((float)$maxSplits / $splitsNeeded);
            
            for ($i = 0; $i < $maxSplits; $i += $facetsPerSplit){
                $values = array_slice($facets[$type], $i, $facetsPerSplit);
                $newUrlSet[] = AutoslicerEngine::addFacets($newUrl, ["$type" => $values]);
            }
            
            $this->logStep(
                'splitter__split',
                'facet__' . $type, 
                $splitOnParameter,
                $splitsNeeded,
                $url, 
                [], 
                $newUrlSet,
                'Original url with stripped facets: ' . $newUrl . 
                    "|________| facets per split " . (string)$facetsPerSplit, 
                $isSubsequentCall 
            );
            
        }
        
        return [$newUrlSet, $splitOnParameter];
    }


    protected function logStep( $action, $splitter = null, $splitOn = null, $splits_needed = null,
                             $input_url = null, $input_urls_set = [], $output_urls = [], $notes = '', 
                             $isSubsequentCall = false)
    {
        try {
            $query = prep_insert("autoslicer_log", 
                "run_id, log_step, splitter, split_on, splits_needed, is_subsequent, split_steps_success, split_steps_done,". 
                "action, input_url, input_urls_set, output_urls, input_urls_number, output_urls_number, notes");
            execute_prep($query, [
                'run_id' => (int)$this->runId,
                'log_step' => (int)$this->log_step++, //this is logger step

                'action' => $action,
                'splitter' => $splitter,
                'split_on' => $splitOn,
                'splits_needed' => $splits_needed,
                'input_url' => $input_url,
                'input_urls_set' => implode('|________|', $input_urls_set),
                'output_urls' => implode('|________|', $output_urls),
                'input_urls_number' => (int)count($input_urls_set),
                'output_urls_number' => (int)count($output_urls),
                'is_subsequent' => (bool)$isSubsequentCall,
                'split_steps_success' => (int)$this->steps_success,
                'split_steps_done' => (int)$this->steps_done,
                
                'notes' => $notes
            ]);
            if ($query->rowCount() == 0) {
                //TODO may be report logging error
            }
        } catch (Exception $e) {
            //do not throw, just be silent
        }
    }

    
    private function createAutoslicerRun($campaign, $client_hash, $crawled_by_id, $crawled_by_name, $clientVersion, $splitPhase)
    {
        try {
            $query = prep_insert("autoslicer_run",  
                "company_id, campaign_id, client_version, client_hash, crawled_by_id, crawled_by_name, split_phase");
            execute_prep($query, [
                'campaign_id' => $campaign['id'],
                'company_id' => $campaign['company_id'],
                'client_version' => $clientVersion,
                'client_hash' => $client_hash,
                'crawled_by_id' => $crawled_by_id,
                'crawled_by_name' => $crawled_by_name,
                'split_phase' => $splitPhase
            ]);
            if ($query->rowCount() == 0) {
                return false;
            }
            return \DB::get_instance()->lastInsertId();
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'createAutoslicerRun()', '', null, '', [], [],
                    var_export($e, true), 
                    null 
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
    }

    /**
     * @param $searchUrlId
     * @return bool false
     * @throws Exception
     */
    private function updateSearchUrlToNonSplit($searchUrlId)
    {
        try {
            $query = \DB::prep("UPDATE search_url SET search_url_can_be_splitted = 0 WHERE id = :search_url_id LIMIT 1");
            execute_prep($query, [
                'search_url_id' => (int)$searchUrlId
            ]);
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'updateSearchUrlToNonSplit()', '', null, '', [], [],
                    var_export($e, true), 
                    null 
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
        return false;
    }

    /**
     * @param $searchUrlId
     * @return bool false
     * @throws Exception
     */
    private function finalizeAutoslicerRun()
    {
        try {
            $query = \DB::prep("UPDATE autoslicer_run SET final = NOW() WHERE id = :run_id LIMIT 1");
            execute_prep($query, [
                'run_id' => (int)$this->runId
            ]);
        } catch (Exception $e) {
            try{
                $this->logStep(
                    'ERROR', 'finalizeAutoslicerRun()', '', null, '', [], [],
                    var_export($e, true), 
                    null 
                );
            } catch(Exception $e2){}
            
            throw $e;
        }
        return false;
    }

}

