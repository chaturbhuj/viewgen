<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 05.07.2017
 * Time: 14:37
 */
require_once(__DIR__ . '/Engine.php');

class SalesAutoslicer extends AutoslicerEngine {
    
    protected function seniority_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'seniority', $splitsNeeded, $isSubsequentCall);
    }

    protected function relationship_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'relationship', $splitsNeeded, $isSubsequentCall);
    }

    protected function geography_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'geo', $splitsNeeded, $isSubsequentCall);
    }

    protected function company_size_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'companySize', $splitsNeeded, $isSubsequentCall);
    }

    protected function function_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'function', $splitsNeeded, $isSubsequentCall);
    }

    protected function company_type_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'companyType', $splitsNeeded, $isSubsequentCall);
    }

    protected function groups_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'group', $splitsNeeded, $isSubsequentCall);
    }

    protected function school_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'school', $splitsNeeded, $isSubsequentCall);
    }

    protected function industry_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'industry', $splitsNeeded, $isSubsequentCall);
    }

    //Format type 1: $facet=CC&facet.CC=12345 ...
    protected function company_cc_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'currCompany', $splitsNeeded, $isSubsequentCall);
    }
    
    //Format type 2: &companyEntities=Atari_4574 ... 
    protected function company_entity_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'company', $splitsNeeded, $isSubsequentCall);
    }

    //&jobTitleEntities=director ... 
    protected function title_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'title', $splitsNeeded, $isSubsequentCall);
    }

    protected function past_company_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_entity_array_splitter($url, 'pastCompany', $splitsNeeded, $isSubsequentCall);
    }
    
    protected function exp_years_company_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_experience_years_array_splitter($url, 'tenureAtCurrentCompany', $splitsNeeded, $isSubsequentCall);
    }
    
    protected function exp_years_position_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_experience_years_array_splitter($url, 'tenureAtCurrentPosition', $splitsNeeded, $isSubsequentCall);
    }
    
    protected function exp_years_total_splitter($url, $splitsNeeded, $isSubsequentCall){
        return $this->_experience_years_array_splitter($url, 'yearsOfExperience', $splitsNeeded, $isSubsequentCall);
    }
    

    protected function setupSplitters($config, $results, $originalUrlRecord){
        //Strategies are arranged in order of results number
        
        $strategies = [
            'company_cc', 
            'company_entity',
            'past_company'
        ];
        
        foreach ($strategies as $facet){
            $results = $this->runStrategy($facet, $results);
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
        }
        
        //Keywords: OR splitting
        for($i=0; $i < $config['boolean_max_recursive_depth']; $i++){
            //in case boolean_max_recursive_depth > 1
            //this will be recursive walk into boolean expression
            $results = $this->runStrategy('keywords', $results);
            //IMPORTANT! this check for 'pass' is needed for all recursive strategies
            if ($results['pass']) break;
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
            
        }
        
        //non-recursive strategies
        $strategies = [
            'title', 
            'industry', 
            'seniority', 
            'geography',
            'company_size',
            'function',
            'company_type',
            'groups',
            'school',
            // these 3 ones put at the end because of potentially number decreasing splits
            // these facets are introduced synthetically, see their splitter core
            'exp_years_company',    
            'exp_years_position',
            'exp_years_total',    
            //intentionally put the last, very asymmetric split
            'relationship',   
        ];
        
        foreach ($strategies as $facet){
            $results = $this->runStrategy($facet, $results);
            if ($this->saveUrlsOrPass($results, $originalUrlRecord)) return false;
        }
        
        //control shot
        $this->saveUrlsOrPass($results, $originalUrlRecord, true);
        return false;
    }
}