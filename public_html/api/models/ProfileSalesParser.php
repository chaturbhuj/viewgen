<?php

/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 27.06.2017
 * Time: 15:37
 *
 * Request example:
 *
 * Posted JSON: {"DOM":true,"WaitBetweenConnections":7,"linked_in_id":372841886,"VisitedProfiles24":"35","people_id":"58499339","campaign_id":"56777","referer":"https://www.linkedin.com/sales/search?jobTitleEntities=ceo&facet=N&facet=G&facet.N=S&facet.G=us%3A0&titleScope=CURRENT&count=25&start=0&updateHistory=true&rsid=3728418861502354102485&orig=MDYS&facet=TE&facet.TE=2&facet=YP&facet.YP=5","url":"https://www.linkedin.com/sales/profile/426154120,sYgz,NAME_SEARCH","status_message":{"status":3,"message":"Running Campaign \"ceo\": Visiting Profiles"},"html":"","client_hash":"ivanh8a374c2e63c2493a9ff66f708","company_name":"ivanh","version":"Windows 5.4.1.0"}
 */
class ProfileSalesParser
{
    /**
     * Handling bad html page
     * @param $html
     * @param $crawledByName
     * @param $crawledById
     * @param $peopleId
     * @return array
     * @throws Exception
     */
    public static function handleProblemProfilePage($html, $crawledByName, $crawledById, $peopleId)
    {
        //There is no any html
        if (!isset($html)) {
            throw new Exception("No html");
        }
        //If account of our LI-user is frozen
        if (isset($html) and trim($html) == "This account is frozen, contact info@searchquant.net to unfreeze it") {
            return [
                'url' => 'http://searchquant.net/frozen_account.php',
                'WaitBetweenConnections' => APIModel::DEFAULT_WAIT,
                'CrawledBy' => "Frozen Account|Frozen Account",
                'username' => 'Frozen Account',
                'status_message' => [
                    'status' => 2,
                    'message' => 'This account is frozen. Contact info@searchquant.net'
                ]
            ];
        }

        //If something wrong with profile page
        if (strpos($html, "The page you requested is no longer available, or cannot be found")) {
            $nameError = '404 not found';
            $titleError = 'The page you requested is no longer available, or cannot be found';
            $waitBetweenConnections = 100;
            ParserModel::updatePeopleError($nameError, $titleError, $crawledByName, $crawledById, $peopleId);
        } elseif (strpos($html, "Sorry, there was a problem processing your request. Please try again")) {
            $nameError = 'Problem Processing Request';
            $titleError = 'Sorry, there was a problem processing your request. Please try again';
            $waitBetweenConnections = 100;
            ParserModel::updatePeopleError($nameError, $titleError, $crawledByName, $crawledById, $peopleId);
        } elseif (strpos($html, "There was an unexpected problem that prevented us from completing your request")) {
            $nameError = 'Unexpected Problem';
            $titleError = 'There was an unexpected problem that prevented us from completing your request';
            ParserModel::updatePeopleError($nameError, $titleError, $crawledByName, $crawledById, $peopleId);
            $waitBetweenConnections = 50;
        }
        if (!empty($nameError)) {
            return [
                'WaitBetweenConnections' => empty($waitBetweenConnections) ? APIModel::DEFAULT_WAIT : $waitBetweenConnections,
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'username' => $crawledByName
            ];
        }
        //---END: Handling bad html page
    }

    /**
     * @param $html
     * @param $crawledById
     * @return array|bool
     */
    public static function handleBadPage($html, $crawledById)
    {
        $bpHandler = new api\models\BadPageHandler($html, $crawledById);
        $bphResult = $bpHandler->handleBadPage();
        if (!$bphResult) {
            if (self::getPageVersion($html) === null) {
                $result = [
                    'bp-message' => 'html: section#topcard',
                    'response' => [
                        'status_message' => [
                            'status' => 2,
                            'message' => 'No needed data on the page. Try again...'
                        ]
                    ]
                ];
                return $result;
            }
        } else {
            return $bphResult;
        }
        return \FALSE;
    }

    /**
     * @param $payload
     * @param string $version
     * @return array
     * @throws Exception
     */
    public static function parseProfilePage($payload, $version = '')
    {
        if (empty($payload->linked_in_id)) {
            throw new Exception("No ID");
        }
        if (empty($payload->html)) {
            throw new Exception("No html");
        }
        //---Initialize basic values
        $versionId = APIModel::getClientVersionIdByName($version);
        if (!empty($payload->people_id)) {
            $peopleId = $payload->people_id;
        }
        if (!empty($payload->campaign_id)) {
            $campaignId = $payload->campaign_id;
        }
//        $peopleData = ProfileSalesParser::getPeopleById($peopleId);
//        if(empty($campaignId)){
//            $campaignId = $peopleData['campaign_id'];
//        }
//        $searchUrlId = $peopleData['search_url_id'];
        $crawledById = $payload->linked_in_id;
        $crawledByName = $payload->username ?? '';
        $companyData = APIModel::getCompanyByProductKey($payload->client_hash);
        $companyId = $companyData["id"];
        $waitBetweenConnections = $companyData['wait_between_connections'];

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($payload->html)) return NULL;
        $finder = new \DomXPath($dom);
        $crawledByNameFromPageNode = $finder->query("//img[@class='profile-photo']/@alt")->item(0);
        $crawledByNameFromPage = $crawledByNameFromPageNode !== \NULL ? trim($crawledByNameFromPageNode->nodeValue) : '';
        $liUser = APIModel::getLiUser($companyId, $crawledById);

        $crawledByName = FirstPageModel::updateLiUserName($liUser, $crawledByNameFromPage);

        //---END: Initialize basic values

        if (!empty($companyId && $crawledById && $payload->client_hash && $crawledByName)) {
            $htmlDumpId = ParserModel::saveHtmlDump($payload->url, $companyId, $crawledByName, $crawledById, $_SERVER["REMOTE_ADDR"], $payload->client_hash, $peopleId, $versionId, $payload->html);
        }

        //Bad Page
        $badPage = ProfileSalesParser::handleBadPage($payload->html, $crawledById);
        if ($badPage) {
            ParserModel::insertApiBadPages($companyId, $crawledById, $payload->url ?? '', $payload->html, $versionId, $htmlDumpId ?? \NULL, $campaignId ?? \NULL, $searchUrlId ?? \NULL, $peopleListId ?? \NULL, $badPage['bp-message'], \APIModel::PERSON_SN);
            return array_merge([
                'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
                'CrawledBy' => $crawledById . "|" . $crawledByName,
                'username' => $crawledByName,
                'linked_in_id' => (int)$crawledById,
                'WaitBetweenConnections' => ParserModel::randomizeWait($waitBetweenConnections)
            ], $badPage['response']);
        }
        //=======
        $parseMethod = 'parseProfileV'.self::getPageVersion($payload->html);
        $profile = self::$parseMethod($payload->html);
        //---END: Parsing profile page

        //save collected data into DB
        ParserModel::updatePeople([$profile['profileName'], $profile['headline'], $profile['location'], $profile['industry'], $profile['employer'], $profile['employerId'], $crawledByName, $crawledById, $profile['email'], $profile['isConnected'], $peopleId]);

        return [
            'VisitedProfiles24' => (int)$liUser["visitsLast24Hours"],
            'CrawledBy' => $crawledById . "|" . $crawledByName,
            'username' => $crawledByName,
            'linked_in_id' => $crawledById,
            'WaitBetweenConnections' => ParserModel::randomizeWait($waitBetweenConnections),
            'linkedin_current_username' => $profile['profileName'],
            'linkedin_current_job_position' => $profile['headline'],
            'linkedin_current_job_place' => $profile['employer']
        ];
    }

    /**
     *
     * @param $html
     *
     * @return int|null page design version or null in case of unknown page
     */
    public static function getPageVersion($html){
        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);


        if($finder->query("//section[@id='topcard']")->item(0) !== null){
            return 0;
        }

        if(\ParserHelper::findCodeOnPageByKey($html, 'memberBadges') !== null){
            return 1;
        }

        if($finder->query("//div[contains(@class, 'profile-topcard')]")->item(0) !== null){
            return 2;
        }


        return null;
    }

    public static function parseProfileV1($html){
        $data = \ParserHelper::findCodeOnPageByKey($html, 'memberBadges');

        $profile = [
            'profileName' => !empty($data['fullName']) ? $data['fullName'] : '',
            'headline' => !empty($data['headline']) ? $data['headline'] : '',
            'location' => !empty($data['location']) ? $data['location'] : '',
            'industry' => !empty($data['industry']) ? $data['industry'] : '',
            'employer' => '',
            'employerId' => '',
            'email' => '',
            'isConnected' => (!empty($data['degree']) && $data['degree'] == 1)? 1 : 0,
            'peopleId' => ''
        ];


        if(!empty($data['contactInfo']) && !empty($data['contactInfo']['primaryEmail'])){
            $profile['email'] = $data['contactInfo']['primaryEmail'];
        }

        if(!empty($data['defaultPosition'])){
            $profile['employer'] = isset($data['defaultPosition']['companyName']) ? $data['defaultPosition']['companyName'] : '';

            if (isset($data['defaultPosition']['companyUrn']) && preg_match('#urn:li:fs_salesCompany:(.*)#i', $data['defaultPosition']['companyUrn'], $match)) {
                if (!empty($match[1])) {
                    $profile['employerId'] = (int)$match[1];
                }
            }
        }

        if (preg_match('#urn:li:member:(.*)#i', $data['objectUrn'], $match)) {
            if (!empty($match[1])) {
                $profile['peopleId'] = (int)$match[1];
            }
        }
        return $profile;
    }

    public static function parseProfileV0($html){
        $profile = [
            'profileName' => '',
            'headline' => '',
            'location' => '',
            'industry' => '',
            'employer' => '',
            'employerId' => '',
            'email' => '',
            'isConnected' => 0,
            'peopleId' => ''
        ];

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);


        $degreeNode = $finder->query("//abbr[contains(@class,'degree-icon')]/text()")->item(0);
        $degree = $degreeNode !== \NULL ? (int)trim($degreeNode->nodeValue) : 0;
        if ($degree === 1) {
            $profile['isConnected'] = 1;
        }
        $profileNameNode = $finder->query("//h1[@class='member-name']")->item(0);
        $profile['profileName'] = $profileNameNode !== \NULL ? trim($profileNameNode->nodeValue) : '';
        $headlineNode = $finder->query("//li[@class='title']")->item(0);
        $profile['headline'] = $headlineNode !== \NULL ? trim($headlineNode->nodeValue) : '';
        $locationNode = $finder->query("//li[@class='location-industry']/span[@class='location']")->item(0);
        $profile['location'] = $locationNode !== \NULL ? trim($locationNode->nodeValue) : '';
        $industryNode = $finder->query("//li[@class='location-industry']/span[@class='industry']")->item(0);
        $profile['industry'] = $industryNode !== \NULL ? trim($industryNode->nodeValue) : '';
        $employerNode = $finder->query("//ul[@class='positions']/li")->item(0);
        $employer = $employerNode !== \NULL ? trim($employerNode->nodeValue) : '';

        $p = explode(" at ", $employer);
        if (count($p) > 1) {
            $profile['employer'] = $p[1];
        } else {
            $profile['employer'] = $p[0];
        }

        if($profile['employer']){
            $companyLink = $finder->query("//h3[@class='company-name' and a = '".htmlentities($employer, ENT_QUOTES | ENT_HTML401)."']/a/@href")->item(0);
            if($companyLink && $companyLink->nodeValue){
                $profile['employerId'] = substr(strrchr($companyLink->nodeValue, '='), 1);
            }
        }

        $emailNode = $finder->query("//div[@class='module-footer']//a[contains(@href,'mailto')]")->item(0);
        $profile['email'] = $emailNode !== \NULL ? trim($emailNode->nodeValue) : '';

        return $profile;
    }

    public static function parseProfileV2($html){
        $profile = [
            'profileName' => '',
            'headline' => '',
            'location' => '',
            'industry' => '',
            'employer' => '',
            'employerId' => '',
            'email' => '',
            'isConnected' => 0,
            'peopleId' => ''
        ];

        $dom = new \DOMDocument();
        $dom->validateOnParse = true;
        if (!@$dom->loadHTML($html)) return NULL;
        $finder = new \DomXPath($dom);

        $profileCardNode = $finder->query("//div[contains(@class,'profile-topcard')]/div[@class='container']/div")->item(0);
        $mainInfoNode = $finder->query("//div", $profileCardNode)->item(0);
        $contactInfoNode = $finder->query("//div[contains(@class, 'profile-topcard__contact-info')]", $profileCardNode)->item(0);


        $degreeNode = $finder->query("//div[contains(@class,'profile-topcard-person-entity__content')]/dl/dt/ul/li[1]/span", $mainInfoNode)->item(0);
        $degree = $degreeNode !== \NULL ? (int)trim($degreeNode->nodeValue) : 0;
        if ($degree === 1) {
            $profile['isConnected'] = 1;
        }
        $profileNameNode = $finder->query("//div[contains(@class,'profile-topcard-person-entity__content')]/dl/dt/span[contains(@class, 'profile-topcard-person-entity__name')]", $mainInfoNode)->item(0);
        $profile['profileName'] = $profileNameNode !== \NULL ? trim($profileNameNode->nodeValue) : '';
        $headlineNode = $finder->query("//div[contains(@class,'profile-topcard-person-entity__content')]/dl/dd[1]", $mainInfoNode)->item(0);
        $profile['headline'] = $headlineNode !== \NULL ? trim($headlineNode->nodeValue) : '';
        $locationNode = $finder->query("//div[contains(@class,'profile-topcard-person-entity__content')]/dl/dd/div[contains(@class, 'profile-topcard__location-data')]", $mainInfoNode)->item(0);
        $profile['location'] = $locationNode !== \NULL ? trim($locationNode->nodeValue) : '';

        $industryNode = $finder->query("//li[@class='location-industry']/span[@class='industry']")->item(0); //@TODO find industry on new design
        $profile['industry'] = $industryNode !== \NULL ? trim($industryNode->nodeValue) : '';


        $employerIDNode = $finder->query("//dd[contains(@class,'profile-topcard__current-positions')]/div/div/a/@href", $mainInfoNode)->item(0);
        $profile['employerId'] = $employerIDNode !== \NULL ? trim($employerIDNode->nodeValue) : '';

        if($profile['employerId']){
            $profile['employerId'] = preg_replace('/\D/', '', $profile['employerId']);

            $employerNode = $finder->query("//dd[contains(@class,'profile-topcard__current-positions')]/div/div/span/a", $mainInfoNode)->item(0);
        }else{
            $employerNode = $finder->query("//dd[contains(@class,'profile-topcard__current-positions')]/div/div/span/span[2]", $mainInfoNode)->item(0);
        }
        $profile['employer'] = $employerNode !== \NULL ? trim($employerNode->nodeValue) : '';


        $emailNode = $finder->query("//a[contains(@href,'mailto')]", $contactInfoNode)->item(0);
        $profile['email'] = $emailNode !== \NULL ? trim($emailNode->nodeValue) : '';

        return $profile;
    }

}