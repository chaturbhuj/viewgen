<?php

require(__DIR__ . '/services/api_db_logger_svc.php');
use function api\services\logger\log_api_error;


$GLOBALS['NOSESSION'] = true;

require_once(__DIR__ . '/services/response_svc.php');
use function api\services\response\{successResponse, errorResponse};

$post = NULL;

try{
    require_once("functions.php");
    
    require_once(__DIR__ . '/../db.php');
    
    $data = file_get_contents("php://input");
    $post = json_decode($data, true);
    
    if ($post === null) {
        echo json_encode(['status' => 'error', 'message' => 'Invalid JSON in the POST body']);
        die;
    }
    
    $invalid_creds_message = "Invalid username or password";
    
    if (isset($post['username']) && isset($post['password'])) {
    
        $query = DB::prep("SELECT * FROM company WHERE company_name = :username OR email = :username");
        $query->execute([
            'username' => $post['username']
        ]);
    
        if ($query->rowCount() == 0) {
            echo json_encode(['status' => 'error', 'message' => $invalid_creds_message]);
            die;
        }
    
        $company = $query->fetch();
        if ($company['password_hash'] == "") {
            if ($company['company_password'] != $post['password']) {
                echo json_encode(['status' => 'error', 'message' => $invalid_creds_message]);
                die;
            }
        } elseif (hash("sha256", $post["password"].$company['password_salt']) != $company['password_hash']) {
            echo json_encode(['status' => 'error', 'message' => $invalid_creds_message]);
            die;
        }
    
        $query = DB::prep("SELECT * FROM software_license WHERE company_id = :company_id");
        $query->execute([
            'company_id' => $company['id']
        ]);
    
        $software_license = $query->fetch();
        echo json_encode([
            'status' => 'success', 
            'client_hash' => $software_license['product_key'],
            'company_name' => $company['company_name'],
            'company_id' => $company['id'],
        ]);
        die;
    }

} catch (Exception $e) {
    errorResponse(log_api_error('login', $post, $e));
}
?>
