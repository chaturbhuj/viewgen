<?php

$HTML_TITLE = "Settings";
require_once("header.php");
require_once("shared_functions.php");

$error_message = "";
if(isset($_POST["save_settings"])){
	
	if($_POST["password"] != ""){
		if(strlen($_POST["password"]) < 6){
			$error_message .="Password is too short - must be at least 6 characters<br>";
		}elseif($_POST["password"] != $_POST["repeat_password"]){
			$error_message .="Password and repeat password did not match<br>";
		}
		if($error_message == ""){
			$hash_password = make_hash_password($_POST["password"],$_COMPANY_DATA["password_salt"]);
			//echo "hash_password $hash_password _POST[password] $_POST[password] _COMPANY_DATA[password_salt] $_COMPANY_DATA[password_salt]";
			
			$query = DB::prep("UPDATE company SET password_hash = ? WHERE id = ?");
			$query->execute(array($hash_password,$_GET['company_id']));
		}
	}

	if (isset($_POST['automatic_alerts_email'])) {
		$query = DB::prep("UPDATE company SET send_automatic_alerts = :send_automatic_alerts,
			automatic_alerts_email = :automatic_alerts_email WHERE id = :company_id");
		$query->execute([
			'send_automatic_alerts' => isset($_POST['send_automatic_alerts']) ? 1 : 0,
			'automatic_alerts_email' => $_POST['automatic_alerts_email'],
			'company_id' => $_GET['company_id']
		]);
	}

	if($error_message == ""){

		foreach ($_POST['visits_per_day'] as $li_user_id => $visits) {
			$query = DB::prep("update li_users set max_visits_per_day=? where id=? and company_id=?");
			$query->execute(array($visits, $li_user_id, $_GET['company_id']));
		}

		$query = DB::prep("UPDATE company SET wait_between_connections = ? WHERE id = ?");
		$query->execute(array($_POST['wait_between_connections'],$_GET['company_id']));
		redirect("settings.php?company_id=$_GET[company_id]");
	}

}

if(isset($_GET["hide"]) AND isset($_USER_DATA["admin"])){
	$query = DB::prep("UPDATE li_users SET hidden = 1 WHERE linked_in_id = ? AND company_id = ?");
	$query->execute(array($_GET['hide'],$_GET['company_id']));
	redirect("settings.php?company_id=$_GET[company_id]");
}

$secondsOptions[] = 1;
$secondsOptions[] = 2;
$secondsOptions[] = 3;
$secondsOptions[] = 5;
$secondsOptions[] = 10;
$secondsOptions[] = 15;
$secondsOptions[] = 20;
$secondsOptions[] = 30;
$secondsOptions[] = 45;
$secondsOptions[] = 60;
$secondsOptions[] = 120;

// Fetch li users
$query = DB::prep("select * from li_users where company_id=? and hidden = 0");
$query->execute(array($_GET['company_id']));

$li_users = $query->fetchAll();

?>
<form action="" method="post">

<div class="container settings">
	<div class="row">
		<div class="col-xs-12">
			<h1><font color=red><?=$error_message?></font></h1>
			<h1>Settings</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3 settings_box">
			<h2>Visits</h2>
			<label>Average wait between visits:</label>
			<input type=hidden name=save_settings value=true>
			<select name=wait_between_connections>
	<?
	foreach($secondsOptions as $option){
		if($option == $data["wait_between_connections"]){
			echo "<option selected=selected value=$option>$option</option>";
		}else{
			echo "<option value=$option>$option</option>";
		}
		
	}
	?>
			</select>
	<? if ($_COMPANY_DATA['account_type'] == "free" AND $_COMPANY_DATA['subscription_plan_code'] != "noLimit") {?>
			<label><font color=red>Upgrade your account to increase visits per day</font></label>
		<? } ?>
	<?php foreach ($li_users as $li_user) { ?>
			<label>Visits per day for <?=$li_user['name']; ?>
			<? if($_USER_DATA["admin"]){ ?>
				<a href=settings.php?company_id=<?=$_GET["company_id"]?>&hide=<?=$li_user['linked_in_id']?>><u>Hide</u></a>  
			<? } ?>
			</label>
			<select name="visits_per_day[<?=$li_user['id']?>]">
	<?
	if ($_COMPANY_DATA["account_type"] == "free" AND $_COMPANY_DATA['subscription_plan_code'] != "noLimit") {
		echo "<option value=".$li_user['max_visits_per_day'].">".$li_user['max_visits_per_day']."</option>";
	} else {
		if ($li_user['name'] =="Ivan Hallberg Ryman") {
			$visitsOptions[] = 1100;
			$visitsOptions[] = 1200;
			$visitsOptions[] = 1300;
			$visitsOptions[] = 1400;
			$visitsOptions[] = 1500;
		}
		if ($li_user['name'] =="Chris Zaharias") {
			$visitsOptions[] = 1100;
			$visitsOptions[] = 1200;
			$visitsOptions[] = 1300;
			$visitsOptions[] = 1500;
			$visitsOptions[] = 2000;
			$visitsOptions[] = 2500;
			$visitsOptions[] = 3000;
			$visitsOptions[] = 5000;
		}
		foreach($visitsOptions as $option){
			if($option == $li_user['max_visits_per_day']){
				echo "<option selected=selected value=$option>$option</option>";
			}else{
				echo "<option value=$option>$option</option>";
			}
		}
	}

	?>
		</select>
		
	<?php } ?>
		</div>
		<div class="col-xs-3 settings_box">
			<h2>Change Password</h2>
			<label>New Password:</label>
			<input name=password type="password">
			<label>Repeat Password:</label>
			<input name=repeat_password type="password">
			<br/>
			<br/>
			<h2>Automatic Alerts</h2>
			<label>
				<input type="checkbox" style="width: auto;margin-right: 5px;" name="send_automatic_alerts" <?=$_COMPANY_DATA['send_automatic_alerts'] ? ' checked="checked"' : ''?>/>
				We want to receive alerts
			</label>
			<label>Emails to receive automated alerts and reports (one per line)</label><br/>
			<textarea name="automatic_alerts_email" class="settings-alerts-email"><?=$_COMPANY_DATA['automatic_alerts_email'] == "" ? $_COMPANY_DATA['email'] : $_COMPANY_DATA['automatic_alerts_email'] ?></textarea>
		</div>

		<div class="col-xs-3 settings_box">
			<h2>Account Key</h2>
			<label>Account key for client software:</label>
			<input type="text" readonly value="<?=$_COMPANY_DATA['client_hash'];?>">

			<div class="submit_box">
				<br><a href="<?=base_url('settings.php')?>" id="create_campaign_submit" class="button green large next">Save<i></i></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="settings_separator"></div>
			<h1>Download & Install the Client Software</h1>
			<div class="install_instructions">
				<ol>
					<li>Download the software by clicking download</li>
					<li>Install the software on your computer</li>
					<li>Sign in to LinkedIn inside the software</li>
					<li>Click on "Campaigns" in the menu at the top of this page and set up a campaign for your LinkedIn user</li>
				</ol>
			</div>
			<div class="install_links">
			
				<?php if (get_os() == "mac") { ?>
				<a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=mac')?>" class="button blue medium cloud"><i></i>Download<span>Mac OS X<?= get_mac_client_size(" · ") ?></span></a>
				<?php } else { ?>
				<a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=windows')?>" class="button blue medium cloud"><i></i>Download<span>Windows<?= get_windows_client_size(" · ") ?></span></a>
				<?php } ?>
				
				<span class="all_versions">All versions:</span>
				<div class="version">
					<h2>Mac OS X 10.7.0+</h2>
					<span>Download for <a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=mac')?>">Mac OS X</a>· ca<?= get_mac_client_size(" · ") ?></span>
				</div>
				<div class="version">
					<h2>Windows XP/Vista/7/8</h2>
					<span>Download for <a href="<?=base_url('client/download.php?company_id='.$_COMPANY_DATA['id'].'&type=windows')?>">Windows</a><?= get_windows_client_size(" · ") ?></span>
				</div>
			</div>
		</div>
	</div>
</div>

</form>

<?php

require_once("footer.php");

?>
