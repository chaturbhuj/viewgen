<?php

if ($_SERVER['SERVER_NAME'] == "viewgentools.com" && strpos($_SERVER['REQUEST_URI'], '/api/') === false) {
	if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
	    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	    header("HTTP/1.1 301 Moved Permanently");
	    header("Location: $redirect");
		die;
	}
}

?>
