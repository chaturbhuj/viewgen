<?php 
include_once(__DIR__ . '/../../config/versions.php');
use const config\versions\ASSETS_VERSION;
require_once(__DIR__ . '/../../shared_functions.php');
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="en"/>
    <title>ViewGen</title>
    
    <?php include(__DIR__ . '/../../ui/include/favicon.php'); ?>
    
    <link rel="stylesheet" href="<?= '/assets/vendor/bootstrap3/css/bootstrap.min.css?' . ASSETS_VERSION ?>">
    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Roboto%3A300%2C100%2C400%2C600&#038;subset=latin'
          type='text/css' media='all'/>
<!--    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">-->
    <link rel="stylesheet" href="<?= '/client_ui/assets/css/client.css?' . ASSETS_VERSION ?>">

    <link rel="stylesheet" href="/assets/vendor/fontawesome/css/font-awesome.min.css" />

    <script type="application/javascript" src="<?= '/assets/js/jquery.min.js?' . ASSETS_VERSION ?>"></script>
<!--    <script type="application/javascript" src="--><?//= 'assets/js/datepickr.min.js?' . ASSETS_VERSION ?><!--"></script>-->

<!--    <script type="application/javascript"-->
<!--            src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>-->
    
    <script type="application/javascript" src="<?= '/assets/js/utilities.js?' . ASSETS_VERSION ?>"></script>
