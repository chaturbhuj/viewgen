<?php 
    include_once(__DIR__ . '/welcome_video_native.php');
    die;
?>

<!--<style>-->
<!--body {-->
<!--		background: url('/client_ui/assets/images/splash.png') no-repeat 0 0;-->
<!--			background-size: cover;-->
<!--}-->
<!--</style>-->
<?php

if (isset($_GET['client_hash'])) {

	// look for the company
	$query = DB::prep("SELECT * FROM software_license WHERE product_key = :client_hash");
	$query->execute([
		'client_hash' => $_GET['client_hash']
	]);

	if ($query->rowCount() > 0) {
		$company = $query->fetch();

		$_SESSION['auth'] = array(
			'user_id' => 0,
			'company_id' => $company['company_id']
		);

		$user = null;
		if (isset($_GET['linked_in_id'])) {
			// Look for the user.
			$parts = explode('|', $_GET['linked_in_id']);
			$query = DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND company_id = :company_id");
			$query->execute([
				'linked_in_id' => $parts[0],
				'company_id' => $company['company_id'],
			]);

			if ($query->rowCount() > 0) {
				$user = $query->fetch();
				header("Location: /new-ui/campaign_builder.php?company_id=" . $company['company_id'] . "&user_id=" .
					$user['id']);
				die;
			}
		}

		header("Location: /dashboard.php?company_id=" . $company['company_id']);
		die;
	}
}
?>
