<?php

include(__DIR__ . '/parts/header.php');
include_once(__DIR__ . '/../config/versions.php');
use const config\versions\ASSETS_VERSION;

?>

<link rel="stylesheet" href="<?='/client_ui/assets/css/video-native.css?' . ASSETS_VERSION ?>">

<?php include(__DIR__ . '/parts/header-end.php'); ?>

<div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group videoplayer-list">
                  <a href="#" class="list-group-item active"
                     data-mp4="<?= '/assets/video/ViewGenOverview.mp4?' . ASSETS_VERSION ?>"
                     data-webm="<?= '/assets/video/ViewGenOverview.webm?' . ASSETS_VERSION ?>"
                     data-poster="<?= '/assets/video/ViewGenOverview.png?' . ASSETS_VERSION ?>"
                  >
                      <h4 class="list-group-item-heading">ViewGen Introduction</h4>
                      <p class="list-group-item-text">Brief explanation of the ViewGen - Automated Lead Generation tool for LinkedIn</p>
                  </a>
                </div>
            </div>
            <div class="col-md-9">
                <figure class="videoplayer" id="welcome_video" data-fullscreen="false">
                    <video controls
                           preload="metadata"
                           poster=""
                    >
                        <source class="v_webm" src="" type="video/webm; codecs=vp8, vorbis">
                        <source class="v_mp4" src="" type="video/mp4">
                        <!-- Offer download -->
                    </video>
<!--                    <figcaption>-->
<!--                        &copy; ViewGen | <a href="http://viewgentools.com">ViewGen</a>-->
<!--                    </figcaption>-->
                </figure>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        var dims = screenDims();

        $('.videoplayer').height(dims.height);
        $('.videoplayer-list').height(dims.height);
        
        var video = $('.videoplayer video');
        var source_mp4 = video.find('source.v_mp4');
        var source_webm = video.find('source.v_webm');
        
        var videoHeight = video.height();
        //video.css('margin-top',  Math.floor(dims.height / 2) - Math.floor(videoHeight / 2) );
        
        var listLinks = $('.videoplayer-list a');
        
        listLinks.on('click', function (event) {
            listLinks.removeClass('active');
            var $t = $(this);
            $t.addClass('active');
            var data = $t.data(); 
            video.attr('poster', data.poster);
            source_mp4.attr('src', data.mp4);
            source_webm.attr('src', data.webm);
        });


        listLinks.first().click();
        
        //var supportsVideo = !!document.createElement('video').canPlayType;



    });
</script>

<?php include(__DIR__ . '/parts/footer.php'); ?>
