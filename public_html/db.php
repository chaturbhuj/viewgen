<?php

require_once(__DIR__ . '/config.php');

// Base url of the site, without ending slash
$HOST = $CONFIG['host'];

$BASE_URI = "";

$BASE_URL = "//".$HOST.$BASE_URI;

if (isset($GLOBALS['NOSESSION']) and $GLOBALS['NOSESSION'] === true) {
} else {
	require_once('include/session.php');
}
require_once('shared_functions.php');


/* DB connection */

mb_internal_encoding("UTF-8");

function qq($query) {
	return mysql_real_escape_string($query);
}

function mysql_connect($host, $user, $pass) {
	$GLOBALS['mysqli'] = mysqli_connect($host, $user, $pass);
}

function mysql_select_db($dbName) {
	return mysqli_select_db($GLOBALS['mysqli'], $dbName);
}

function mysql_query($query) {
	return mysqli_query($GLOBALS['mysqli'], $query);
}

function mysql_fetch_array($result) {
	return mysqli_fetch_array($result);
}

function mysql_fetch_assoc($result) {
	return mysqli_fetch_assoc($result);
}

function mysql_num_rows($result) {
	return mysqli_num_rows($result);
}

function mysql_insert_id() {
	return mysqli_insert_id($GLOBALS['mysqli']);
}

function mysql_real_escape_string($string) {
	return mysqli_real_escape_string($GLOBALS['mysqli'], $string);
}

class DB extends PDO {

	private static $instance;
	private $db_host = "";
	private $db_user = "";
	private $db_pass = "";
	private $db_name = "";

	public static function get_instance() {
		if (self::$instance)
			return self::$instance;

		self::$instance = new self();
		return self::$instance;
	}

	public static function get_new($dsn = '') {
		return new DB($dsn);
	}

	public function __construct($dsn = '') {
        global $CONFIG;
        
        $this->db_host = $CONFIG['db_host'];
        $this->db_user = $CONFIG['db_user'];
        $this->db_pass = $CONFIG['db_pass'];
        $this->db_name = $CONFIG['db_name'];

		if ($dsn == '')
			$dsn = "mysql:dbname=".$this->db_name.";host=".$this->db_host;

		try {
			parent::__construct($dsn, $this->db_user, $this->db_pass, 
                array(
                    PDO::ATTR_PERSISTENT => false, 
                    PDO::ATTR_ERRMODE =>  ($CONFIG['dev'] ? PDO::ERRMODE_EXCEPTION : PDO::ERRMODE_WARNING), 
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
                ));
		} catch (PDOException $e) {
			throw $e; //this will be catched in API logger. Or should fail anyway
		}
		parent::setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_BOTH);
		$q = $this->prepare("set names utf8");
		$q->execute();

		@mysql_connect($this->db_host, $this->db_user, $this->db_pass);
		mysql_select_db($this->db_name);
		mysql_query("set names utf8");
	}

	public static function prep($query) {
		return DB::get_instance()->prepare($query);
	}

}

DB::get_instance();

?>
