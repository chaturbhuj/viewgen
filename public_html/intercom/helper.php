<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once (__DIR__ . '/../lib/recurly.php');

use Intercom\IntercomClient;

class IntercomHelper
{
    public $intercomClient = null;

    public function __construct($api_key){
        try{
            $this->intercomClient = new IntercomClient($api_key, null);
        }catch(Exception $e){
        }
    }

    public function pushCompanyData($company, $checkRecurlyByEmail = false){
        if(empty($company)){
            return null;
        }
        $account = null;
        try {
            if(empty($company['recurlyAccountCode'])) throw new Exception('Empty account code');
            $account = Recurly_Account::get($company['recurlyAccountCode']);
        } catch (Exception $error) {
            try {
                if(empty($company['rc_account_code'])) throw new Exception('Empty account code');
                $account = Recurly_Account::get($company['rc_account_code']);
            } catch (Exception $error) {
                if($checkRecurlyByEmail){
                    try {
                        $account = Recurly_Account::get($company['email']);
                    } catch (Exception $error) {

                    }
                }
            }
        }

        $hasActiveSubscription = false;
        $lastSubscription = null;
        $canceledInTrial = false;
        if ($account !== null) {
            // This company has a recurly account. Look at the subscriptions.
            $subscriptions = Recurly_SubscriptionList::getForAccount($account->account_code);
            foreach ($subscriptions as $subscription) {
                $canceledInTrialTMP = false;
                if ($subscription->state == "active" || $subscription->state == "future") {
                    $hasActiveSubscription = true;
                    $lastSubscription = $subscription;
                    $canceledInTrial = false;
                    break;
                }
                if ($subscription->state == "canceled" || $subscription->state == "expired") {
                    if (isset($subscription->trial_started_at) && isset($subscription->trial_ends_at)) {
                        $endDateTmp = !empty($subscription->canceled_at) ? $subscription->canceled_at : $subscription->expires_at;

                        if ($subscription->trial_started_at->format('Y-m-d H:i:s') != $subscription->trial_ends_at->format('Y-m-d H:i:s') and
                            (strtotime($subscription->trial_ends_at->format('Y-m-d H:i:s')) >=
                                strtotime($endDateTmp->format('Y-m-d H:i:s')))) {
                            $canceledInTrialTMP = true;
                        }
                    }
                    if(empty($lastSubscription)){
                        $canceledInTrial = $canceledInTrialTMP;
                        $lastSubscription = $subscription;
                    }else if($lastSubscription->expires_at < $subscription->expires_at){
                        $canceledInTrial = $canceledInTrialTMP;
                        $lastSubscription = $subscription;
                    }
                }
            }
        }

        try{
            $res = $this->intercomClient->users->update([
                "user_id" => "company_" . $company['id'],
                "email" => $company['email'],
                "phone" => $company['phone'],
                "signed_up_at" => $company['company_created'],
                "name" => $company['first_name'] . " " . $company['last_name'],
                "last_seen_ip" => "",
                "custom_attributes" => [
                    "role" => "owner"
                ],
                "last_seen_user_agent" => "",
                "companies" => [
                    [
                        "remote_created_at" => $company["company_created"],
                        "company_id" => $company["id"],
                        "name" => $company["company_name"],
                        "monthly_spend" => 0,
                        "plan" => $company["subscription_plan_code"],
                        "size" => 0,
                        "website" => "",
                        "industry" => "",
                        "custom_attributes" => [
                            "state" => $company["state"],
                            "account_type" => $company["account_type"],
                            "deleted" => $company["deleted"],
                            "last_visit" => $company["last_visit"],
                            "people_count" => $company["people_count"],
                            "first_name" => $company["first_name"],
                            "last_name" => $company["last_name"],
                            "company" => $company["company"],
                            "frozen" => $company["frozen"],
                            "last_page_view_viewgen" => $company["last_page_view_viewgen"],
                            "last_page_view_searchquant" => $company["last_page_view_searchquant"],
                            "company_runs_query_slicer" => $company["company_runs_query_slicer"],
                            "recurlyAccountCode" => $company["recurlyAccountCode"],
                            "recurlyCoupon" => $company["recurlyCoupon"],
                            "is_test" => $company["is_test"],
                            "recurly_sync" => $company["recurly_sync"],
                            "has_active_subscription" => $hasActiveSubscription,
                            "canceled_in_trial" => $canceledInTrial,
                            "rc_account_code" => !empty($account) ? $account->account_code : "",
                            "rc_account_state" => !empty($account) ? $account->state : "",
                            "rc_account_has_past_due_invoice" => (!empty($account) && !empty($account->has_past_due_invoice)) ? true : false,
                            "rc_subscription_uuid" => !empty($lastSubscription) ? $lastSubscription->uuid : "",
                            "rc_subscription_state" => !empty($lastSubscription) ? $lastSubscription->state : "",
                            "rc_subscription_plan" => !empty($lastSubscription) ? $lastSubscription->plan->plan_code : "",
                            "rc_subscription_price" => !empty($lastSubscription) ? $lastSubscription->unit_amount_in_cents : 0,
                            "rc_subscription_quantity" => !empty($lastSubscription) ? $lastSubscription->quantity : 0,
                            "rc_subscription_activated_at" => (!empty($lastSubscription) && !empty($lastSubscription->activated_at)) ? $lastSubscription->activated_at->getTimestamp() : null,
                            "rc_subscription_canceled_at" => (!empty($lastSubscription) && !empty($lastSubscription->canceled_at)) ? $lastSubscription->canceled_at->getTimestamp() : null,
                            "rc_subscription_expires_at" => (!empty($lastSubscription) && !empty($lastSubscription->expires_at)) ? $lastSubscription->expires_at->getTimestamp() : null,
                            "rc_subscription_current_period_started_at" => (!empty($lastSubscription) && !empty($lastSubscription->current_period_started_at)) ? $lastSubscription->current_period_started_at->getTimestamp() : null,
                            "rc_subscription_current_period_ends_at" => (!empty($lastSubscription) && !empty($lastSubscription->current_period_ends_at)) ? $lastSubscription->current_period_ends_at->getTimestamp() : null,
                            "rc_subscription_trial_started_at" => (!empty($lastSubscription) && !empty($lastSubscription->trial_started_at)) ? $lastSubscription->trial_started_at->getTimestamp() : null,
                            "rc_subscription_trial_ends_at" => (!empty($lastSubscription) && !empty($lastSubscription->trial_ends_at)) ? $lastSubscription->trial_ends_at->getTimestamp() : null,
                        ]
                    ]
                ],
                "last_request_at" => null,
                "unsubscribed_from_emails" => $company['company_do_not_email'],
                "update_last_request_at" => false,
                "new_session" => false
            ]);
        }catch(Exception $e){
            $res = false;
        }
        return $res;
    }

    public function pushUserData($user, $unsubscribed = 0){
        if(empty($user)){
            return null;
        }
        try{
            $res = $this->intercomClient->users->update([
                "user_id" => $user['id'],
                "email" => $user['user_email'],
                "phone" => $user['user_phone'],
                "signed_up_at" => $user['user_created'],
                "name" => $user['name'],
                "last_seen_ip" => "",
                "custom_attributes" => [
                    "role" => "user",
                    "linked_in_id" => $user["linked_in_id"],
                    "li_account_type" => $user["li_account_type"],
                    "max_visits_per_day" => $user["max_visits_per_day"],
                    "software_status" => $user["software_status"],
                    "last_connection" => $user["last_connection"],
                    "hidden" => $user["hidden"],
                    "limit_reached_at" => $user["limit_reached_at"],
                    "visitsLast24Hours" => $user["visitsLast24Hours"],
                    "anonymous" => $user["anonymous"],
                    "anonymous_sales" => $user["anonymousSales"],
                    "subscriptions" => $user["subscriptions"],
                    "active_campaign_count" => $user["active_campaign_count"],
                    "finished_campaign_count" => $user["finished_campaign_count"],
                    "archived_campaign_count" => $user["archived_campaign_count"],
                    "paused_campaign_count" => $user["paused_campaign_count"],
                    "total_visited" => $user["total_visited"],
                    "total_days_visiting" => $user["total_days_visiting"],
                    "first_visit" => $user["first_visit"],
                    "last_visit" => $user["last_visit"],
                    "client_version" => $user["version_name"] ? $user["version_name"] : $user["client_version"]
                ],
                "last_seen_user_agent" => "",
                "companies" => [
                    [
                        "company_id" => $user["company_id"]
                    ]
                ],
                "last_request_at" => $user["last_connection"],
                "unsubscribed_from_emails" => $unsubscribed,
                "update_last_request_at" => false,
                "new_session" => false
            ]);
        }catch(Exception $e){
            $res = false;
        }
    }

    public function pushUserMetrics($user){
        if(empty($user)){
            return null;
        }
        try{
            $res = $this->intercomClient->users->update([
                "user_id" => $user['id'],
                "custom_attributes" => [
                    "li_account_type" => $user["li_account_type"],
                    "software_status" => $user["software_status"],
                    "last_connection" => $user["last_connection"],
                    "limit_reached_at" => $user["limit_reached_at"],
                    "visitsLast24Hours" => $user["visitsLast24Hours"],
                    "anonymous" => $user["anonymous"],
                    "anonymous_sales" => $user["anonymousSales"],
                    "subscriptions" => $user["subscriptions"],
                    "active_campaign_count" => $user["active_campaign_count"],
                    "finished_campaign_count" => $user["finished_campaign_count"],
                    "archived_campaign_count" => $user["archived_campaign_count"],
                    "paused_campaign_count" => $user["paused_campaign_count"],
                    "total_visited" => $user["total_visited"],
                    "total_days_visiting" => $user["total_days_visiting"],
                    "last_visit" => $user["last_visit"],
                ],
                "last_request_at" => $user["last_connection"]
            ]);
        }catch(Exception $e){
            $res = false;
        }
    }


    public function pushSignUpStarted($company){
        try{
            $this->intercomClient->events->create([
                "event_name" => "signup-started",
                "created_at" => time(),
                "user_id" => "company_" . $company['id']
            ]);
        }catch(Exception $e){
        }
    }
    public function pushSignUpFinished($company){
        try{
            $this->intercomClient->events->create([
                "event_name" => "signup-finished",
                "created_at" => time(),
                "user_id" => "company_" . $company['id']
            ]);
        }catch(Exception $e){
        }
    }
    public function pushEvent($company, $event_name){
        try{
            if(!empty($event_name) && !empty($company)) {
                $this->intercomClient->events->create([
                    "event_name" => $event_name,
                    "created_at" => time(),
                    "user_id" => "company_" . $company['id']
                ]);
            }
        }catch(Exception $e){
        }
    }

    public function pushUserStatus($company_id, $linked_in_id){
        //switch off due to intercom limit exceeded
//        $user = $this->getUserByCompany($company_id, $linked_in_id);
//        if(empty($user)) return false;
//        $this->pushUserMetrics($user);
    }

    public function addUser($company_id, $linked_in_id){
        //switch off due to intercom limit exceeded

//        $user = $this->getUserByCompany($company_id, $linked_in_id);
//        if(empty($user)) return false;
//        $this->pushUserData($user);
    }

    private function getUserByCompany($company_id, $linked_in_id){
        try{
            $query = \DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND linked_in_id = :linked_in_id");
            $query->execute([
                'company_id' => $company_id,
                'linked_in_id' => $linked_in_id
            ]);
            return $query->fetch();
        }catch(Exception $e){
        }
    }
}



