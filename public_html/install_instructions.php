<?php

require_once('include/session.php');
require_once('shared_functions.php');

$IGNORE_AUTH = true;
$STOPMENU = true;

if (!isset($_GET['company_id'])) {
	die;
}

//$FIRST_PAGE = true;
require_once("header.php");
require_once("header_index.php");

?>
<div class="">
<div class="unsubscribe instruction">
	<h4>Instructions</h4>
	<span class="instruction">
		1) Download the desktop software: <a href="/client/download.php?company_id=<?=$_GET['company_id']?>&type=mac">Macintosh version</a> / <a href="/client/download.php?company_id=<?=$_GET['company_id']?>&type=windows">Windows version</a>;
	</span>
	<span class="instruction">
	2) Install the Win or Mac desktop software, and log in to your LinkedIn account from within it; 
	</span>
	<span class="instruction">
	3) Log in to <a href="https://viewgentools.com/">ViewGenTool.com</a> using the Account Name and Password your company's account owner has (they have it; we do not);
	</span>
	<span class="instruction">
	4) Create your first campaign;
	</span>
</div>

