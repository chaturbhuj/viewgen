<?php
//INCLUDE THIS FILE ONLY IF YOU PIECE OF CODE IS READY TO CATCH AND HANDLE EXCEPTIONS!!!

//SEE the tool: public_html/api/services/api_db_logger_svc.php
//use like this: require_once __DIR__ . '/../../config/force_exceptions.php';

//Important error setup
//set global error_handler to convert all errors into exceptions

error_reporting(E_ALL);

ini_set("display_errors", 0);

set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

?>