<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

function ajax_response($status, $payload = array()) {
	echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

$ACTIVE_HEADER_PAGE = "dashboard";

require_once('authenticate.php');

require_once("shared_functions.php");
if (!isset($_GET['company_id'])) {
	ajax_response("error");
}

if ($_POST['action'] == "get_client_status") {
	$query = DB::prep("select *,(SELECT name FROM campaign WHERE campaign.id=li_users.campaign_id) as campaignName,visitsLast24Hours AS visitCount, (limit_reached_at > '0000-00-00 00:00:00' AND limit_reached_at > date_sub(NOW(), interval 1 hour)) limitReached from li_users where company_id=?  and hidden = 0");
	$query->execute(array($_GET['company_id']));

	$software_statuses = $query->fetchAll();

	$statuses = [];
	foreach ($software_statuses as $status) {
		$status['name'] = mb_strlen($status['name']) > 25 ? mb_substr($status['name'],0,22)."..." : $status['name'];
		if ($status['have_new_interface'] == 1) {
			$status['newRowAlert'] = "<font color=red>&uarr; Note! This user must upgrade to LinkedIn Sales Navigator</font>";
		}
		$status['message'] = ($status["visitCount"] > 0 ? "($status[visitCount] visits last 24h) &nbsp; &nbsp; &nbsp; &nbsp;": "").ucfirst($status['software_status'] == 'waiting' ? (($status['visitCount'] >= $status['max_visits_per_day'] or $status['limitReached']) ? '24h limit reached' : 'Waiting for Active Campaign') : $status['software_status']) . (trim($status['software_status'])=='running' ? ' "'.make_short($status['campaignName'],20).'"': "");
		$statuses[] = $status;
	}

	$filter = " and campaign.publish_status = 'published' and campaign.status IN ('Active')";
	$query = DB::prep("select *,DATE(NOW()) today
		from campaign
		where company_id = ? 
		$filter
		order by if(priority<0, 1, 0) asc, status ASC, if(status = 'Finished','zzz',priority) DESC,id DESC");
	$query->execute(array($_GET['company_id']));

	$campaigns = $query->fetchAll();

	$ret_campaigns = [];
	foreach ($campaigns as $campaign) {
		$ret_campaigns[] = [
			'campaign_id' => $campaign['id'],
			'visits' => $campaign['campaign_visit_count']
		];
	}

	ajax_response("success", [
		'campaigns' => $ret_campaigns,
		'statuses' => $statuses
	]);
}

if ($_POST['action'] == 'get_campaign_visits') {
	
}

?>
