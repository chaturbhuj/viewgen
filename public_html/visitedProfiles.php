<?php
//die("Visited profiles is temporarily unavailable, please check back later");

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

$export = false;
$json = false;
if (isset($_GET["json"]) and $_GET["json"] != "") {
    $json = true;
    $STOPHEAD = true;
    $STOPMENU = true;
    $export = true;
}

if (isset($_GET["export"]) and $_GET["export"] != "") {
    $STOPHEAD = true;
    $STOPMENU = true;
    $export = true;
    ini_set('memory_limit', '3048M');
}
if (!isset($_GET["order_by"])) {
    $_GET["order_by"] = "";
}
if (!isset($_GET["direction"])) {
    $_GET["direction"] = "";
}
if (!isset($_GET["start_date"])) {
    $_GET["start_date"] = "";
}
if (!isset($_GET["end_date"])) {
    $_GET["end_date"] = "";
}
if (!isset($_GET['search_url_id'])) {
    $_GET['search_url_id'] = "";
}


if (!$export and !$json){
    $WIDEOUTERPAGE = true;
}
$HTML_TITLE = "Visited Profiles";
include('header.php');

if (!isset($_GET['company_id'])) {
    die;
}



$company_id = $_GET['company_id'];
function myJsonDecode($jsonText){
    $jsonObject = json_decode('["'.$jsonText.'"]',true);
    return $jsonObject[0];
}


/*$query = DB::prep("select * from search_url where id=?");
$query->execute(array($_GET['search_url_id']));
if ($query->rowCount() == 0) die;

$searchUrl = $query->fetch();*/

if (!isset($_GET['search_url_id']) or $_GET['search_url_id'] == '') {
    $search_url_id = 0;
} else {
    $search_url_id = (int)$_GET['search_url_id'];
}
if (!isset($_GET['user']) or $_GET['user'] == '') {
    $user = 0;
} else {
    $user = (int)$_GET['user'];
}
if (!isset($_GET['campaign_id']) or $_GET['campaign_id'] == '') {
    $campaign_id = 0;
} else {
    $campaign_id = (int)$_GET['campaign_id'];
}

if ($campaign_id > 0) {
    // Fetch the li user id.
    $query = DB::prep("SELECT * FROM campaign WHERE id = :campaign_id");
    $query->execute([
        'campaign_id' => $campaign_id
    ]);

    if ($query->rowCount() > 0) {
        $campaignRow = $query->fetch();
        $user = $campaignRow['visit_by_id'];
    }
}

if ($search_url_id > 0) {
    // Fetch the li user id.
    $query = DB::prep("SELECT * FROM search_url WHERE id = :search_url_id");
    $query->execute([
        'search_url_id' => $search_url_id
    ]);

    if ($query->rowCount() > 0) {
        $searchUrlRow = $query->fetch();
        $user = $searchUrlRow['visit_by_id'];
    }
}

// Fetch urls
if (!isset($_GET["page"])) {
    $currentPage = 1;
} else {
    $currentPage = $_GET["page"];
}

$direction = "asc";
if (isset($_GET["direction"]) and $_GET["direction"] == 'desc') {
    $direction = "desc";
}
$orderBy = "people.id";
if (isset($_GET["order_by"]) and $_GET["order_by"] != "") {
    if ($_GET["order_by"] == "name") {
        $_GET["order_by"] = "people.name";
    }
    if ($_GET["order_by"] == "campaign") {
        $_GET["order_by"] = "campaign.name";
    }
    $orderBy = str_replace("timestamp","people.id",mysql_real_escape_string($_GET["order_by"]));
} else {
    $orderBy = "people.sent_to_crawler";
    $direction = "desc";
}
$limit = "LIMIT 2000";
if ($currentPage > 1) {
    $limit = "LIMIT 2000 OFFSET ".(($currentPage-1)*2000) ." ";
}
if ($json) {
    $limit = "LIMIT 100000";
}else if ($export) {
    $limit = "LIMIT 1000000";
}
$nameFilter = "";
if (isset($_GET["filter"]) and trim($_GET["filter"]) != "") {
    $nameFilter = " AND (
	people.name LIKE '%".mysql_real_escape_string($_GET["filter"])."%' OR
	people.employer LIKE '%".mysql_real_escape_string($_GET["filter"])."%' OR
	people.location LIKE '%".mysql_real_escape_string($_GET["filter"])."%' OR
	people.title LIKE '%".mysql_real_escape_string($_GET["filter"])."%'
	)";
}
$start_date = 0;
$end_date = 0;


if (!isset($_GET["days"])) {
    if ($campaign_id > 0 OR $search_url_id > 0) {
        $x_days = 0;
    } else {
        $x_days = 7;
    }

} else {
    if ($_GET["days"] == -1) {
        $start_date = format_date_to_db($_GET["start_date"]);
        $end_date = format_date_to_db($_GET["end_date"]);
        $x_days = -1;
    } else {
        $x_days = $_GET["days"];
    }

}
$visit_connection_joing = "LEFT JOIN";
$group_on_visit_connection = "";
if (isset($_GET["only_view_backs"])) {
    $visit_connection_joing = "JOIN";
    $group_on_visit_connection = "GROUP BY visit_connection.people_id";
}

//jag tar tog bort den här AND substr(inbound_visit.headline, 1, 40) sounds LIKE substr(people.title, 1, 40) i båda sub querisarna
//tog bort andra subqueryn också ,(SELECT inbound_visit.inbound_visit_id FROM inbound_visit WHERE inbound_visit.crawled_by_id = :user_id AND inbound_visit.name = people.name  ORDER BY guessed_visit_date DESC LIMIT 1) AS inbound_visit_id
//den här försökte jag ha med: AND inbound_visit.guessed_visit_date >= people.sent_to_crawler_date AND inbound_visit.guessed_visit_date <= DATE_ADD(people.sent_to_crawler_date,INTERVAL 7 DAY)
//tog bort , guessed_visit_date asc
$query = DB::prep("SELECT SQL_CALC_FOUND_ROWS
	people.*,
	campaign.name campaignName,
	people_list.page_nr,
	people_list.url,
	visit_connection.guessed_visit_date AS guessed_visit_date,
	visit_connection.inbound_visit_id AS inbound_visit_id
FROM people 
JOIN people_list ON people_list.id = people.people_list_id
JOIN campaign ON campaign.id = people.campaign_id
$visit_connection_joing visit_connection ON visit_connection.people_id = people.id
WHERE people.name != '' AND
:company_id = people.company_id AND
(:search_url_id = people.search_url_id OR :search_url_id = 0) AND
(:user_id = people.crawled_by_id OR :user_id = 0) AND
(DATE_SUB(NOW(), INTERVAL :x_days DAY) < sent_to_crawler OR :x_days = 0 OR :x_days = -1) AND
(:start_date = 0 OR sent_to_crawler_date >= :start_date) AND
(:end_date = 0 OR sent_to_crawler_date <= :end_date) AND
(:campaign_id = people.campaign_id OR :campaign_id = 0)
".$nameFilter."
".$group_on_visit_connection."
ORDER BY campaign.name ASC, IF(".$orderBy." = '','zzz',".$orderBy.") ".$direction.", inbound_visit_id asc
".$limit);

$query->execute(array(
    "company_id"=>$company_id,
    "search_url_id"=>$search_url_id,
    "user_id"=>$user,
    "x_days"=>$x_days,
    "start_date"=>$start_date,
    "end_date"=>$end_date,
    "campaign_id"=>$campaign_id
));

/*
SELECT
        COUNT(*) AS number_of_visits,
        MAX(sent_to_crawler_date) AS max_sent_to_crawler_date
    FROM people
    WHERE
        crawled_by_id = :crawled_by_id AND
        name = :name
*/

$queryWithRows = $query;

$query = DB::prep("SELECT FOUND_ROWS() totalRows");
$query->execute(array());
$totalRows = $query->fetch();
$profileCount = $totalRows[0];

if ($json) {
    header('Content-Type: application/json');
    header("Pragma: no-cache");
    header("Expires: 0");
    $arrayOfRows = array();
    while ($query = $queryWithRows->fetch()) {
        $workAt = "";
        $atSplit = explode(" at ",myJsonDecode($query["title"]));
        if(count($atSplit) == 2) {
            $workAt = $atSplit[1];
        }
        $currentCompany = "";
        if ($query["employer"] != "") {
            $currentCompany = $query["employer"];
        } else {
            $currentCompany = $workAt;
        }
        $linkedInId = html_entity_decode(myJsonDecode($query["linked_in_id"]));
        /*if (true) { // short version
            $p = explode("&",$linkedInId);
            $linkedInId = $p[0];
        }*/

        $names = explode(" ",$query['name']);
        $lastName = $names[count($names)-1];
        $firstName = trim(str_replace(' '.$lastName,'',$query['name']));
        //		utf8_decode(html_entity_decode(myJsonDecode($query["employer"]))),
        $row = array();
        $row["FirstName"] = html_entity_decode($firstName);
        $row["LastName"] = html_entity_decode($lastName);
        $row["Title"] = html_entity_decode($query["title"]);
        $row["CurrentCompany"] = html_entity_decode($currentCompany);
        $row["CurrentCompanyId"] = $query['employer_url'];
        $row["Industry"] = html_entity_decode($query["industry"]);
        $row["Location"] = html_entity_decode(myJsonDecode($query["location"]));
        $row["User"] = $query["crawled_by"];
        $row["Campaign"] = $query["campaignName"];
        $row["Date"] = $query["sent_to_crawler"];
        $row["HaveSubscription"] = $query["premium"]==1?true:false;
        //$row["DateOfViewBack"] = $query["guessed_visit_date"];
        $row["ProfileUrl"] = "https://www.linkedin.com/sales/profile/".$linkedInId;

        //print_r($row);
        $arrayOfRows[] = $row;

    }
    echo json_encode($arrayOfRows);
    die();

} else if ($export) {

    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="Visited Profiles.csv"');
    header("Pragma: no-cache");
    header("Expires: 0");
    $row = ["First Name","Last Name","Title","Current Company", "Current Company Id","Industry","Location","User","Campaign","Email","You Visited Them","Premium","Visited You On","LinkedIn Profile"];
    echo "sep=,\r\n";
    echo implode(",",$row)."\r\n";
    while ($query = $queryWithRows->fetch()) {
        $workAt = "";
        $atSplit = explode(" at ",myJsonDecode($query["title"]));
        if(count($atSplit) == 2) {
            $workAt = $atSplit[1];
        }
        $currentCompany = "";
        if ($query["employer"] != "") {
            $currentCompany = $query["employer"];
        } else {
            $currentCompany = $workAt;
        }
        $linkedInId = utf8_decode(html_entity_decode(myJsonDecode($query["linked_in_id"])));
        /*if (true) { // short version
            $p = explode("&",$linkedInId);
            $linkedInId = $p[0];
        }*/

        $names = explode(" ",$query['name']);
        $lastName = $names[count($names)-1];
        $firstName = trim(str_replace(' '.$lastName,'',$query['name']));
        //		utf8_decode(html_entity_decode(myJsonDecode($query["employer"]))),
        $title = $query["title"];
//			$title = explode(" at ", $query["title"]);
//			$title = $title[0];

        $row = array(
            utf8_decode(html_entity_decode($firstName)),
            utf8_decode(html_entity_decode($lastName)),
            utf8_decode(html_entity_decode($title)),
            utf8_decode(html_entity_decode($currentCompany)),
            $query['employer_url'],
            utf8_decode(html_entity_decode($query["industry"])),
            utf8_decode(html_entity_decode(myJsonDecode($query["location"]))),
            utf8_decode(html_entity_decode($query["crawled_by"])),
            $query["campaignName"],
            $query["email"],
            date("Y-m-d H:i", strtotime($query["sent_to_crawler"])),
            $query["premium"],
            $query["guessed_visit_date"],
            "https://www.linkedin.com/sales/profile/".$linkedInId
        );
        for ($i=0; $i<count($row); $i++) {
            if (strpos($row[$i],"\n") !== false) {
                $row[$i] = str_replace("\n",'',$row[$i]);
            }
            if (strpos($row[$i],"\r") !== false) {
                $row[$i] = str_replace("\r",'',$row[$i]);
            }
            if (strpos($row[$i],'"') !== false) {
                $row[$i] = str_replace('"','',$row[$i]);
            }
            if (strpos($row[$i],",") !== false) {
                $row[$i] = '"'.$row[$i].'"';
            }
        }
        echo implode(",",$row)."\r\n";
    }

    die();
}


?>

<? if ($_GET["company_id"] == 605) {?>
    <div id="page">
        <h1>Visited Profiles</h1>
        Visited profiles are disabled for this account.
    </div>

    <?
    exit;
} ?>

<div class="container full-width">
    <div class="row">
        <div class="col-xs-12">

            <h1>Visited Profiles</h1><br>
            <?if ($user > 0) {

                $query = DB::prep("SELECT * FROM li_users WHERE linked_in_id = ?");
                $query->execute(array($user));
                if ($li_user = $query->fetch()) { ?>
                    User: <b><?=$li_user["name"]?></b><br>
                    <?
                }
            }?>
            <?if ($campaign_id > 0) {

                $query = DB::prep("SELECT * FROM campaign WHERE id = ?");
                $query->execute(array($campaign_id));
                if ($campaignData = $query->fetch()) { ?>
                    Campaign: <b><?=$campaignData["name"]?></b><br>
                    <?
                }
            }?>
            <?if ($search_url_id > 0) {

                $query = DB::prep("SELECT * FROM search_url WHERE id = ?");
                $query->execute(array($search_url_id));
                if ($searchUrlData = $query->fetch()) { ?>
                    Search URL: <b><?=$searchUrlData["url"]?></b><br>
                    <?
                }
            }?>
            <form>
                <input class="filterInput" type=text name=filter value="<?=isset($_GET["filter"])?$_GET["filter"]:""?>" placeholder="Search Visited Profiles"> <input class="button blue small display-option-button" type=submit value="Search"style="height: 50px; width: 150px; font-size: 20px;"> <font color=red>Search by name, title, company or city/country</font>

                <br>
                <div>
                    <select name=days class="display-option-dropdown">
                        <?
                        $options[] = 7;
                        $options[] = 14;
                        $options[] = 30;
                        $options[] = 90;
                        $options[] = 365;
                        foreach($options as $option) {
                            $selected = "";
                            if ($option == $x_days) {
                                $selected = " selected";
                            }

                            echo "<option value=$option$selected>last $option days</option>";
                        }
                        ?>
                        <option value=-1<?=($x_days == -1?" selected":"")?>>Custom date range</option>
                        <option value=0<?=($x_days == 0?" selected":"")?>>All time</option>
                    </select>
                    <div id="hiddenDiv" class="visited-profiles-date-picker" <?=($x_days == -1?"":'style="opacity:0;"')?>>

                        <label style="margin-top: 5px; margin-right: 3px;">Start Date</label>
                        <input type="text" name="start_date" id="start_date" class="date" value="<?=isset($_GET["start_date"])?$_GET["start_date"]:date('M j, Y', time())?>">
                        <label style="margin-top: 5px; margin-right: 3px;">End Date</label>
                        <input type="text" name="end_date" id="end_date" class="date" value="<?=isset($_GET["end_date"])?$_GET["end_date"]:date('M j, Y', time())?>">
                        <div class="clear"></div>
                    </div>
                </div>
                <br>

                <? if (isset($_GET["company_id"])) {?>
                    <input type=hidden name=company_id value="<?=$_GET["company_id"]?>">
                <? } ?>

                <? if (isset($_GET["user"])) {?>
                    <input type=hidden name=user value="<?=$_GET["user"]?>">
                <? } ?>

                <? if (isset($_GET["search_url_id"])) {?>
                    <input type=hidden name=search_url_id value="<?=$_GET["search_url_id"]?>">
                <? } ?>

                <? if (isset($_GET["campaign_id"])) {?>
                    <input type=hidden name=campaign_id value="<?=$_GET["campaign_id"]?>">
                <? } ?>

                <? if (isset($_GET["direction"])) {?>
                    <input type=hidden name=direction value="<?=$_GET["direction"]?>">
                <? } ?>

                <? if (isset($_GET["order_by"])) {?>
                    <input type=hidden name=order_by value="<?=$_GET["order_by"]?>">
                <? } ?>
                <div class="checkbox">
                    <label style="font-size: 16px;">
                        <input type=checkbox name="only_view_backs" <?=isset($_GET["only_view_backs"])?"checked":""?>>
                        Only show people who viewed me back
                    </label>
                </div>
            </form>
            <div>
                <br>
                <?
                $baseUrl = "/visitedProfiles.php?company_id=".$company_id;
                if (isset($_GET["search_url_id"])) {
                    $baseUrl .= "&search_url_id=".$_GET["search_url_id"];
                }
                if (isset($_GET["campaign_id"])) {
                    $baseUrl .= "&campaign_id=".$_GET["campaign_id"];
                }
                if (isset($_GET["user"])) {
                    $baseUrl .= "&user=".$_GET["user"];
                }

                if (isset($_GET["days"])) {
                    $baseUrl .= "&days=".$_GET["days"];
                }
                if (isset($_GET["start_date"])) {
                    $baseUrl .= "&start_date=".urlencode($_GET["start_date"]);
                }
                if (isset($_GET["end_date"])) {
                    $baseUrl .= "&end_date=".urlencode($_GET["end_date"]);
                }

                //"&search_url_id=".$search_url_id."&user=".$user."&campaign_id=".$_GET['campaign_id']
                ?>
                <?/*<a href=<?=$baseUrl?>><u>View All Profiles</u></a> */?>

                <?/*<a href=/xlsx.php?company_id=<?=$_GET["company_id"]?>&order_by=<?=$_GET["order_by"]?>&direction=<?=$_GET["direction"]?>&search_url_id=<?=$_GET['search_url_id']?>&user=<?=$_GET['user']?>><u>Export as Excel</u></a> &nbsp; &nbsp; &nbsp; */?>

                <a href=<?=$baseUrl?>&order_by=<?=$_GET["order_by"]?>&direction=<?=$_GET["direction"]?>&days=<?=$x_days?>&start_date=<?=urlencode($_GET["start_date"])?>&end_date=<?=urlencode($_GET["end_date"])?><?=isset($_GET["only_view_backs"])?"&only_view_backs=on":""?>&export=true><u>Export to Excel (.csv)</u></a>

                <br>
                <?=$profileCount?> profiles. <br>
                Click on any underlined column to sort by that value
                <? if ($profileCount > 2000) {

                    $pages = ceil($profileCount/2000);
                    $showingTo = $currentPage*2000;
                    if ($showingTo > $profileCount) {
                        $showingTo = $profileCount;
                    }

                    ?><br>Showing <?=(($currentPage-1)*2000+1)?> to <?=$showingTo?><br>Go to page <?

                    for ($i=1;$i<=$pages;$i++) {?>
                        <? if ($currentPage == $i) { ?>
                            <b><?=$currentPage?></b>
                        <? } else { ?>

                            <a href=<?=$baseUrl?>&order_by=<?=$_GET["order_by"]?>&direction=<?=$_GET["direction"]?>&days=<?=$x_days?>&page=<?=$i?>><u><?=$i?></u></a>
                        <? } ?>
                    <?}
                } ?>
                <?

                //if (isset($_GET["search_url_id"]) or $_GET["search_url_id"] !="" and $_GET["search_url_id"] !="") {
                //	$baseUrl .= ;
                /*}
                if (isset($_GET["user"]) and $_GET["user"] !="") {
                    $baseUrl .= "&user=".$_GET['user'];
                }*/
                $isOrderedByNameAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "people.name" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedByNameAsc = true;
                }
                $isOrderedByTitleAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "title" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedByTitleAsc = true;
                }

                $isOrderedByEmployerAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "employer" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedByEmployerAsc = true;
                }

                $isOrderedByLocationAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "location" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedByLocationAsc = true;
                }

                $isOrderedTimestampAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "timestamp" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedTimestampAsc = true;
                }
                $isOrderedEmailAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "email" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedEmailAsc = true;
                }

                $isOrderedByPremiumAsc = false;
                if (isset($_GET["order_by"]) and $_GET["order_by"] == "premium" and isset($_GET["direction"]) and $_GET["direction"] == "asc") {
                    $isOrderedByPremiumAsc = true;
                }

                $isOrderedByInboundVisitAsc = false;
                if (isset($_GET['order_by']) and $_GET['order_by'] == "guessed_visit_date" and isset($_GET['direction']) and $_GET['direction'] == 'asc') {
                    $isOrderedByInboundVisitAsc = true;
                }
                $isOrderedByCampaignAsc = false;
                if (isset($_GET['order_by']) and $_GET['order_by'] == "campaign.name" and isset($_GET['direction']) and $_GET['direction'] == 'asc') {
                    $isOrderedByCampaignAsc = true;
                }
                ?>
                <table class="query">
                    <tr>
                        <th>#</th>
                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=name&direction=<?=$isOrderedByNameAsc?"desc":"asc"?>>First&nbsp;Name</a>
                            <?=($_GET["order_by"] == "people.name" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "people.name" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>
                        <th  style="font-size: 18px;">Last&nbsp;Name
                        </th>
                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=title&direction=<?=$isOrderedByTitleAsc?"desc":"asc"?>>Title</a>
                            <?=($_GET["order_by"] == "title" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "title" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>
                        <?/*<th>Company (based on title)</th>
				<th><a href=<?=$baseUrl?>&order_by=employer&direction=<?=$isOrderedByEmployerAsc?"desc":"asc"?>>Company (first listed)</a>
				<?=($_GET["order_by"] == "employer" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
				<?=($_GET["order_by"] == "employer" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
				</th>*/?>
                        <th style="font-size: 18px;">Company</th>
                        <th style="font-size: 18px;">Company Id</th>
                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=industry&direction=<?=$isOrderedByTitleAsc?"desc":"asc"?>>Industry</a>
                            <?=($_GET["order_by"] == "industry" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "industry" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>
                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=location&direction=<?=$isOrderedByLocationAsc?"desc":"asc"?>>Location</a>
                            <?=($_GET["order_by"] == "location" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "location" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>
                        <th style="min-width: 120px;">
                            <a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=guessed_visit_date&direction=<?=$isOrderedByInboundVisitAsc?"asc":"desc"?>>Visited&nbsp;You&nbsp;On</a>
                            <?=($_GET["order_by"] == "guessed_visit_date" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "guessed_visit_date" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>
                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=timestamp&direction=<?=$isOrderedTimestampAsc?"desc":"asc"?>>You Visited Them</a>&nbsp;<?=($_GET["order_by"] == "timestamp" and $_GET["direction"] == "asc")?"&#x25B2;":""?><?=($_GET["order_by"] == "timestamp" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>

                        <th style="font-size: 18px;">User</th>

                        <th style="min-width: 120px;">
                            <a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=campaign&direction=<?=$isOrderedByCampaignAsc?"desc":"asc"?>>Campaign</a>
                            <?=($_GET["order_by"] == "campaign.name" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
                            <?=($_GET["order_by"] == "campaign.name" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>

                        <th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=email&direction=<?=$isOrderedEmailAsc?"desc":"asc"?>>Email</a>&nbsp;<?=($_GET["order_by"] == "email" and $_GET["direction"] == "asc")?"&#x25B2;":""?><?=($_GET["order_by"] == "email" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
                        </th>


                        <?/*
				<th><a style="font-size: 18px;" href=<?=$baseUrl?>&order_by=premium&direction=<?=$isOrderedByPremiumAsc?"desc":"asc"?>>Premium</a>
				<?=($_GET["order_by"] == "premium" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
				<?=($_GET["order_by"] == "premium" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
				</th>
				*/?>


<!--                        <th style="font-size: 18px;">Profile</th>-->

                    </tr>

                    <?php
                    $rowNr = 0;
                    while ($query = $queryWithRows->fetch()) {
                        $rowNr++;
                        ?>
                        <tr class="<?=$query['guessed_visit_date'] !== null ? 'got-viewback' : ''?>">
                            <?
                            $names = explode(" ",$query['name']);
                            $lastName = $names[count($names)-1];
                            $firstName = trim(str_replace(' '.$lastName,'',$query['name']));
                            $companyBasedOnTitle ="";
                            $atSplit = explode(" at ",myJsonDecode($query["title"]));
                            if(count($atSplit) == 2) {
                                $companyBasedOnTitle = $atSplit[1];
                            }
                            ?>
                            <td class='row-number-column'><?=$rowNr?></td>
                            <td class='first-name-column'><?=$firstName?></td>
                            <td class='last-name-column'><?=$lastName?></td>
                            <td class='title-column'><?=myJsonDecode($query["title"])?></td>
                            <?/*<td class='companyBasedOnTitle-column'><?=$companyBasedOnTitle?></td>

				<td class='employer-column'><?=myJsonDecode($query["employer"])?></td>*/?>
                            <td class='current-company-column'><?=$query["employer"]==""?$companyBasedOnTitle:$query["employer"]?></td>
                            <td class='current-company-id-column'><?=$query["employer_url"]==""?"NA":$query['employer_url']?></td>
                            <td class='industry-column'><?=$query["industry"]?></td>
                            <td class='location-column'><?=myJsonDecode($query["location"])?></td>
                            <td class="inbound-visit-date"><?=$query['guessed_visit_date'] == ''?"":date("M d Y", strtotime($query['guessed_visit_date']))?></td>
                            <td class='timestamp-column'><?=date("M d Y h:i A", strtotime($query["sent_to_crawler"]))?></td>
                            <td class='user-column'><?=$query["crawled_by"]?></td>
                            <td class='campaign-column'><?=$query["campaignName"]?></td>
                            <td class='email-column'><?=$query["email"]?></td>

                            <?/*<td class='premium-column'><?=$query["premium"]?></td>*/?>
                            <?/*<td class='timestamp-column'><a href=<?=$query["url"]?>><?=$query["page_nr"]?><a></td>*/?>


                            <?/*<td class='query-column'><a href=<?=myJsonDecode($query["url"])?> target="_blank">Query</a></td>*/?>
<!--                            <td class='profile-column'><a href=https://www.linkedin.com/sales/profile/--><?//=myJsonDecode($query["linked_in_id"])?><!-- target="_blank">Go To</a></td>-->


                            <?/*		echo "
	<tr><td class='name'>".utf8_encode($data["name"])."</td>
	<td class='title'>".myJsonDecode($data["title"])."</td>
	<td class='companyBasedOnTitle'>";
	$atSplit = explode(" at ",myJsonDecode($data["title"]));
	//print_r($atSplit);
	if(count($atSplit) == 2) {
		echo $atSplit[1];
	}
	//echo "testars";
	echo"
	</td>
	<td class='employer'>".myJsonDecode($data["employer"])."</td>
	<td class='location'>".myJsonDecode($data["location"])."</td>
	<td class='visited_by'>".myJsonDecode($data["crawled_by"])."</td>
	<td><a href=https://www.linkedin.com/profile/view?id=".myJsonDecode($data["linked_in_id"]).">Go To</a></td><td class='timestamp'>$data[sent_to_crawler]</td></tr>";*/?>
                        </tr>
                    <?php } ?>
                </table>
                <?
                //fetch finalized people_list
                $query = DB::prep("SELECT * FROM people_list WHERE last_list = 1 AND search_url_id = ?");
                $query->execute(array($_GET['search_url_id']));

                while ($lastPage = $query->fetch()) {
                    echo "We have reached the end, <u><a href=lastPage.php?people_list_id=".$lastPage["id"]."&company_id=".$_GET["company_id"]."> See last page (".$lastPage["page_nr"].")</a></u><br>";
                }

                if(!$_USER_DATA["admin"]){
                    //echo "<a href=/visitedProfiles.php?company_id=281&search_url_id=77859&revive=true>Revive</a>"
                }

                ?>
            </div>
        </div>
    </div>
</div>
<?php


include('footer.php');

?>
