<?php

$HTML_TITLE = "Support";
$ACTIVE_HEADER_PAGE = "support";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

include('header.php');

?>

<div class="main-wrapper" id="campaign_builder" data-company-id="<?=$_GET['company_id']?>"
	data-user-id="<?=$_GET['user_id']?>" data-function="dynamic-dashboard">
	<iframe class="support-iframe" src="//viewgentools.com/help/"></iframe>
</div>

<? include('footer.php'); ?>
