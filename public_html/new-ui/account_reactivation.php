<?php
function createRecurlySubscription($recurlyAccount)
{
    $subscription = new Recurly_Subscription();
    $subscription->plan_code = 'vg-basic';
    $subscription->currency = 'USD';
    $subscription->quantity = 1;
    $subscription->trial_ends_at = new DateTime();
    $subscription->account = $recurlyAccount;
    $subscription->create();
    return $subscription;
}

function createRecurlyAccount($company){
    $accountR = new Recurly_Account();
    $accountR->account_code = uniqid();
    $accountR->first_name = $company['first_name'];
    $accountR->last_name = $company['last_name'];
    $accountR->email = $company['email'];
    $accountR->company_name = $company['company_name'];
    $accountR->create();

    $query = DB::prep("
                UPDATE
                    company
                SET
                    recurlyAccountCode = :account_code
                WHERE
                    id = :id
            ");
    $query->execute(array(
        'id' => $company["id"],
        'account_code' => $accountR->account_code
    ));
    return $accountR;
}

function createBillingInfo($recurlyAccountCode, $recurlyToken){
    $billing_info = new Recurly_BillingInfo();
    $billing_info->token_id = $recurlyToken;
    $billing_info->account_code = $recurlyAccountCode;
    $billing_info->create();
    return $billing_info;
}

function unfrezeCompany($company){
    $query = DB::prep("
            UPDATE
                company
            SET
                frozen = 0
            WHERE
                id = :id
        ");
    $query->execute(array(
        'id' => $company["id"]
    ));
}

function getCompany($id){
    $query = DB::prep("
        SELECT 
            * 
        FROM 
            company 
        WHERE 
            id = ?
    ");
    $query->execute(array($id));
    return $query->fetch();
}

$HTML_TITLE = "Billing";
$ACTIVE_HEADER_PAGE = "billing";

require_once('../authenticate.php');

global $_COMPANY_DATA;
global $_USER_DATA;
global $CONFIG;

$company_id = (string)($_GET['company_id'] ?? $_COMPANY_DATA["id"] ?? '');
//default user is fine to be  0 here, as long as company exists -
// this usr_id is NOT user id that on the campaign_builder
//campaign_builder user_id is a li_users id
$user_id = (string)($_GET['user_id'] ?? $_USER_DATA["id"] ?? '0');

if ($company_id == '') {
	if($_USER_DATA["admin"] ?? false){
		redirect("/adminDashboard.php");
	}
	die;
}

include('header.php');
require_once ('../lib/recurly.php');
$company = getCompany($_COMPANY_DATA["id"]);

if (empty($company)) {
    redirect("/");
}

$accountR =null;

if(!empty($company['recurlyAccountCode'])) {
    try {
        $accountR = Recurly_Account::get($company['recurlyAccountCode']);
    } catch (Exception $e) {
    }
}

$lastCanceledSubscription = null;
$hasActiveSubscriptions = false;
$billingInfo = null;
if(!empty($accountR)){

    $subscriptions = Recurly_SubscriptionList::getForAccount($accountR->account_code);

    foreach ($subscriptions as $tmpSubscription) {
        if ($tmpSubscription->state === "active" || $tmpSubscription->state === "feature") {
            $hasActiveSubscriptions = true;
            break;
        }

        if ($tmpSubscription->state === "canceled") {
            if ($lastCanceledSubscription == null) {
                $lastCanceledSubscription = $tmpSubscription;
            } else if ($lastCanceledSubscription->canceled_at < $tmpSubscription->canceled_at) {
                $lastCanceledSubscription = $tmpSubscription;
            }
        }
    }

    if($accountR->state ==="active"){
        try {
            $billingInfo = Recurly_BillingInfo::get($accountR->account_code);
        } catch (Exception $e) {
        }
    }

    if($accountR->state ==="active" && !empty($billingInfo)){
        if($lastCanceledSubscription && !$hasActiveSubscriptions){
            $lastCanceledSubscription->reactivate();
        } else if(!$hasActiveSubscriptions){
            createRecurlySubscription($accountR);
        }

        unfrezeCompany($company);
        redirect("/new-ui/billing.php?company_id=".$company_id);
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        if(empty($accountR)){
            $accountR = createRecurlyAccount($company);
        } else if($accountR->state !== "active"){
            $accountR->reopen();
        }

        if(empty($billingInfo)){
            createBillingInfo($accountR->account_code, $_POST['recurly-token']);
        }


        if($lastCanceledSubscription){
            $lastCanceledSubscription->reactivate();
        } else {
            createRecurlySubscription($accountR);
        }


        unfrezeCompany($company);
        redirect("/new-ui/billing.php?company_id=".$company_id);
    }catch(Exception $error){
        $errors['form'] = $error->getMessage();
    }
}


?>

<style>
    strong.blue-billing-info {
        font-size: 16px;
        color: #2188bf;
        font-weight: 400;
        font-family: Arial;
    }

    .signup-table table h2 {
        font-size: 18px;
        margin-bottom: 0;
    }

    .big-blue-button {
        background: #2188bf;
        width: 100%;
        height: 40px;
        border: none;
        color: #fff;
        margin-top: 20px;
        margin-bottom: 40px;
        font-size: 18px;
        font-family: 'Roboto';
        font-weight: 100;
        line-height: 1.5;
        letter-spacing: 0px;
        cursor: pointer;
    }

    select {
        background-color: #d6d6d6;
        color: #a3a3a3;
        border: 1px solid #d6d6d6;
        font-size: 13px;
        height: 35px;
        text-indent: 5px;
        width: 100%;
    }

    input.s, input[type="email"], input[type="text"], select {
        height: 44px;
        padding-top: 0;
        padding-bottom: 0;
    }

    .form_section input[type="password"], .form_section input[type="tel"] {
        border: 1px solid #d6d6d6;
        height: 44px;
        padding-top: 0;
        padding-bottom: 0;
        border: 1px solid #d2d2d2;
        font-size: 13px;
        color: #747474;
        padding: 8px 15px;
        margin-right: 1%;
        width: 100%;
        box-sizing: border-box;
    }

    input[type="file"], input[type="password"], input[type="submit"], input[type="text"], textarea {
        -webkit-appearance: none;
        -webkit-border-radius: 0;
    }
    input, select, textarea {
        font: 100% Arial,Helvetica,sans-serif;
        font-size: 100%;
        vertical-align: middle;
        color: #000;
    }
    h1.error_box {
        color: red !important;
        font-weight: bold !important;
        font-size: 15px !important;
    }

    .billing-form {
        width: 470px;
    /*    height: 150px;*/
        background: #fff;
        border: 1px solid #ccc;
        padding: 20px;
        margin: 50px auto;
        text-align: center;
    }
    .cc-number {
        width: 70%;
        float: left;
    }
    .cvv-number {
        width: 25%;
        float: right;
    }
    .signup-table table h2 {
         font-size: 18px;
         margin-bottom: 0;
        font-family: 'Roboto', serif;
        font-weight: 300;
        line-height: 1.5;
        letter-spacing: 0;
    }
    .signup-table table {
        text-align: left;
    }
    .form_section h2 {
        margin-bottom: 5px;
        margin-top: 20px;
    }
    input {
        border: 1px solid #d2d2d2;
        border-top-color: rgb(210, 210, 210);
        border-right-color: rgb(210, 210, 210);
        border-bottom-color: rgb(210, 210, 210);
        border-left-color: rgb(210, 210, 210);
        font-size: 13px;
        color: #747474;
        padding: 8px 15px;
        padding-top: 8px;
        padding-bottom: 8px;
        margin-right: 1%;
        width: 100%;
        box-sizing: border-box;
    }
</style>

<div class="new_container">
<div class="container" id="campaign_builder" data-company-id="<?=$company_id?>"
	data-user-id="<?=$user_id?>">
	<div class="row">
		<div class="col-md-12">

            <form action="" method="post" id="recurly_form">
                <input type="hidden" name="action" value="update_billing">
                <input type="hidden" name="recurly-token" data-recurly="token">

                <div class="billing-table-container">
                    <div class="signup-table billing-form">
                        <strong class="blue-billing-info">BILLING INFORMATION</strong>
                        <table>
                            <tr>
                                <td valign="top">
                                    <div class="form_section">
                                        <h2>First Name:</h2>
                                        <input type="text" name="billing_first_name" value="<?=$company['first_name']?>" data-recurly="first_name">
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                    &nbsp;
                                </td>
                                <td valign="top">
                                    <div class="form_section">
                                        <h2>Last Name:</h2>
                                        <input type="text" name="billing_last_name" value="<?=$company['last_name']?>" data-recurly="last_name">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="3">
                                    <div class="form_section cc-number">
                                        <h2>Credit Card Number:</h2>
                                        <input type="text" data-recurly="number">
                                    </div>
                                    <div class="form_section cvv-number">
                                        <h2>CVV:</h2>
                                        <input type="password" data-recurly="cvv">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <div class="form_section">
                                        <h2>Expires (Month):</h2>
                                        <select data-recurly="month" style="background: #fff;">
                                            <?php for ($month = 1; $month <= 12; $month++):?>
                                                <option value="<?=$month?>">
                                                    <?=$month < 10 ? "0".$month : $month?>
                                                </option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                    &nbsp;
                                </td>
                                <td valign="top">
                                    <div class="form_section">
                                        <h2>Expires (Year):</h2>
                                        <select data-recurly="year" style="background: #fff;">
                                            <?php for ($year = 2017; $year <= 2037; $year++):?>
                                                <option value="<?=$year?>"><?=$year?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr><td><br /></td></tr>
                        </table>
                        <h1 class="error_box inactive" id="error_box__form" ></h1>
                        <br>
                        <hr>
                        <button class="big-blue-button">Save</button>
                    </div>
                </div>
                <div class="clear"></div>
            </form>

		</div>
	</div>
</div>
    <script src="/assets/vendor/jquery/jquery-3.min.js"></script>

    <script src="//js.recurly.com/v3/recurly.js"></script>

    <script>
        recurly.configure('<?=$CONFIG['recurly_js_public_key']?>');

        $('form#recurly_form').on('submit', function (event) {
            var form = this;
            event.preventDefault();
            recurly.token(form, function (err, token) {
                if (err) {
                    // handle error using err.code and err.fields
                    console.log(err);
                    $('.error_box').remove();
                    for (var i in err.fields) {
                        var $field = $('[data-recurly="' + err.fields[i] + '"]');
                        var $error = $("<h1 class=\"error_box recurly\"></h1>").html('Invalid field');
                        $field.after($error);
                    }
                } else {
                    // recurly.js has filled in the 'token' field, so now we can submit the
                    // form to your server; alternatively, you can access token.id and do
                    // any processing you wish
                    form.submit();
                }
            });
        });
    </script>


    <script>
        $(function () {
            <?php if(!empty($errors)): ?>
            <?php foreach($errors as $name => $message): ?>
            $('#error_box__<?=$name?>').html('<?=$message?>').show();
            <?php endforeach;?>
            <?php endif;?>
        });
    </script>


<? include('footer.php'); ?>
