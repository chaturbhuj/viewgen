<?php
	require_once('../db.php');
//require_once('../authenticate.php');
require_once(__DIR__ . '/../services/li_account_type_svc.php');
use function services\li_account_type\checkLIAccount;

function ajax_response($status, $payload = array()) {
    header('Content-Type: application/json');
    echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

if (!isset($_POST['action'])) ajax_response("error");

$action = $_POST['action'];

if ($action == "get_location") {
	if (!isset($_POST['str'])) ajax_response("error");

	$str = $_POST['str'];

	if ($str == "") ajax_response("success", array());

	$query = DB::prep("SELECT * FROM li_users WHERE id = ?");
	$query->execute(array(intval($_POST['user_id'])));

	if ($query->rowCount() > 0 && checkLIAccount($query->fetch()) === 'simple') {
        $is_sales = 0;
	}else{
        $is_sales = 1;
    }

	$query = DB::prep("select * from li_location where location_name like :search AND sales < :sales ORDER BY is_country desc, is_state desc, hits desc limit 25");
	$query->execute(array('search' => '%'.$str."%", 'sales' => $is_sales+1));

	$res = array();
	while ($row = $query->fetch()) {
 
		$res[] = array(
			'n' => $row['location_name'],
			'c' => '',
			't' => $row['hits'],
			'id' => $row['linked_in_id']
		);
	}
	ajax_response("success", $res);
} else {
	ajax_response("error");
}

?>
