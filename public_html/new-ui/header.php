<?php
	global $CONFIG;
	global $_USER_DATA;
	global $_COMPANY_DATA;
	if(!$_USER_DATA["admin"]){
		//Vi ska bara registrera om man inte är admin
		$query = "UPDATE company SET last_page_view_viewgen = NOW() WHERE id = '".mysql_real_escape_string($_COMPANY_DATA["id"])."'";
		mysql_query($query);
	}
?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Language" content="en" />
	<title>ViewGen</title>
    
    <?php include(__DIR__ . '/../ui/include/favicon.php'); ?>
    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
		integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/vendor/fontawesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/assets/css/helpers.css">
	<link rel="stylesheet" href="/assets/css/new-ui.css">
	<link rel='stylesheet' id='avada_google_fonts-css'
		href='https://fonts.googleapis.com/css?family=Roboto%3A300%2C100%2C400%2C600&#038;subset=latin' type='text/css'
		media='all' />
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<!--    <link rel="stylesheet" href="/assets/css/bootstrap-treeview.min.css">-->
    <link rel="stylesheet" href="/assets/css/jqtree.css">
    <!--  keep this script before any dependants, especially not present in footer -->
    <script src="/assets/js/utilities.js" type="application/javascript"></script>
</head>
<body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93459439-1', 'auto');
  ga('send', 'pageview');

</script>


<div class="main-wrapper">
	<div class="left-side">
		<div class="row">
			<div class="col-md-12 header">
				<a class="logo" href="/new-ui/campaign_builder.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>"></a>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div class="row">
			<div class="col-md-12 header">
				<? if (!isset($STOP_MENU) || !$STOP_MENU) { ?>
					<div class="menu">
						<a class="menu-item<?=($ACTIVE_HEADER_PAGE ?? '') == "campaign_builder" ? ' active' : ''?>"
							href="/new-ui/campaign_builder.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">CAMPAIGNS</a>
						<a class="menu-item<?=($ACTIVE_HEADER_PAGE ?? '') == "dashboard" ? ' active' : ''?>"
							href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">REPORTS</a>
						<a target="_blank" class="menu-item<?=($ACTIVE_HEADER_PAGE ?? '') == "support" ? ' active' : ''?>"
							href="https://viewgen.zendesk.com">SUPPORT</a>
						<a class="menu-item<?=($ACTIVE_HEADER_PAGE ?? '') == "add_users" ? ' active' : ''?>"
						   href="/new-ui/add_users.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">ADD USERS</a>

                        <span id="float-right-menu" style="cursor: pointer">Account
                        	<ul>
								<?php if($_USER_DATA["admin"]): ?>
                                	<li><a target="_blank" href="/newAdmin/html_dumps.php?companyId=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">html dumps</a></li>
									<li><a target="_blank" href="/newAdmin/bad_pages.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">bad pages</a></li>
								<?php endif; ?>
                                <li><a href="/new-ui/change_password.php?company_id=<?=$_GET['company_id']?>">change password</a></li>
                                <li><a href="/new-ui/notifications_settings.php?company_id=<?=$_GET['company_id']?>">notifications</a></li>
                                <li><a href="/new-ui/download.php?company_id=<?=$_GET['company_id']?>">download client</a></li>
                                <li><a href="/new-ui/billing.php?company_id=<?=$_GET['company_id']?>">billing</a></li>
                                <li><a href="/new-ui/user_settings.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>">utils</a></li>
                                <li><a href="/logout.php">log out</a></li>
                           	</ul>
                        </span>
						<?/*<a class="menu-item" href="#">PROFILE OPTIMIZER</a>
						<a class="menu-item" href="#">SUPPORT</a>
						*/?>
					</div>
				<? } ?>
			</div>
		</div>
	</div>
</div>

<? if ($_COMPANY_DATA['frozen']) { ?>
	<div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
	<span style="color: red;">This account has been frozen. To reactivate click <a href="/new-ui/billing.php?company_id=<?=$_GET['company_id']?>">here</a></span>
	</div>
<? } ?>

<!--<div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">-->
<!--	<span style="color: red;">Today we experienced some unplanned downtime, we are working on resolution and ViewGen will be operational soon. No actions from users required ViewGen will resume activity automatically as soon as we will complete the fix.-->
<!--Apologies for inconvenience-->
<!--ViewGen support team-->
<!--Please reach us if you have any questions on :-->
<!--<a href="mailto:Info@viewgentools.com">Info@viewgentools.com</a>-->
<!--	</span>-->
<!--</div>-->

