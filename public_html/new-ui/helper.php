<?PHP

/*

This is just a helper class to make certain operations quicker to accomplish.
Normally these would be functions all by themselves, but I don't want to worry
about duplicating names of function already in code base, so it's a class 
in name only.

*/

error_reporting(1);
define( 'ABSOLUTE_PATH', dirname(__FILE__) . '/' );

class Helper{
	
	public function evaluate( $file_location, $parameters = array() ){
	
		//Function will evaluate any file and fill in php variables within the file.
		//Pass file location and key value array of variable names/values.
		ob_start();
		include( $file_location );
		$str = ob_get_clean();
		if( ! empty( $parameters ) ){
			foreach( $parameters as $k => $v ){
				if( strpos( $str, '$'.$k ) !== false ){
					$str = str_replace( '$'.$k, $v, $str );	
				}
			}
		}
		return $str;
	
	}
	
	public function authenticated( $reference, $referenceType ){
		
		//Need to check if a user is authenticated in an ajax call based on the reference/type passed to the function
		//Right now, all I know of to worry about is campaign_id as this can be changed via javascript inspector
		//I'll need to verify in php that we're not able to plug someone elses stuff in.
		if( $_SESSION['auth']['user_id'] > 0 ){
			//We're logged in as a valid user, so check if they are an admin, if so, then authenticated
			$sql = 'SELECT admin FROM user WHERE id = ?';
			$q = DB::prep( $sql );
			$q->execute( array( $_SESSION['auth']['user_id'] ) );
			if( $q->fetch() > 0 )
				return true;
		}
		switch( $referenceType ){
			case 'campaign_id':
				if( $reference > 0 && $_SESSION['auth']['company_id'] > 0 ){
					//Need to actually have an id to authenticate
					$sql = 'SELECT id FROM campaign WHERE company_id = ? AND id = ?';
					$q = DB::prep( $sql );
					$q->execute( array( $_SESSION['auth']['company_id'], $reference ) );
					if( $q->fetch() > 0 )
						return true;
				}
				return false;	
			break;	
		}
		
	}
	
	public function action_menu( $label = 'Actions', $links = array() ){
		
		if( empty( $links ) )
			return NULL;
		global $DEFERRED_ASSETS;
		$DEFERRED_ASSETS[] = 'html_templates/action_menu.js';
		foreach( $links as $link => $display ){
			$out[] = $this->evaluate( ABSOLUTE_PATH.'html_templates/action_menu_item.html', get_defined_vars() );
		}
		$action_menu_list_items = implode( "\n", $out );
		return evaluate( ABSOLUTE_PATH.'html_templates/action_menu.html', get_defined_vars() );
		
	}

	function formatQueryString($getVars, $newValues = []){
		$queryStringParts = [];
		foreach($getVars as $k => $v){
			if(isset($newValues[$k])){
				continue;
			}
			$queryStringParts[] = $k.'='.$v;
		}
		foreach($newValues as $k => $v){
			if(empty($v)){
				continue;
			}
			$queryStringParts[] = $k.'='.$v;
		}
		return implode('&', $queryStringParts);
	}

}

?>
