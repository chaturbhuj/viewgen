<?php
require_once('modal_controller.php');
class SimpleSearchModalController extends ModalController
{
    public function __construct()
    {
        $this->menuItems = $this->buildMenuItems();
        $this->idToPredefinedRegions = $this->getPredefinedGeo();

    }

    public function outputHTML()
    {
        unset($this->idToRelationship['A']);//it's for remove filter "Group Member" that unavailable for simple search
        $relationship = $this->checkbox($this->idToRelationship, 'relation');
        $countries = $this->option($this->idToCountry);
        $industries = $this->checkboxGrouped($this->idToIndustry, 'industry_location');
        $predefinedRegions = $this->checkboxGrouped( $this->idToPredefinedRegions, 'predefinedRegions' );
        //This is where we evaluate all of our html files that jquery will generate the side menu with.
        if (empty($this->menuItems))
            return 'what';
        $helper = new Helper;
        foreach ($this->menuItems as $m) {
            if (file_exists(ABSOLUTE_PATH . 'new_campaign_modal/html/campaign_' . $m . '.html'))
                $innerHTML[] = $helper->evaluate(ABSOLUTE_PATH . 'new_campaign_modal/html/campaign_' . $m . '.html', get_defined_vars());
        }
        $innerHTML[] = '<script> window.CampaignBuilderDialogImpl =  "SimpleCampaignBuilderDialog"; </script>';
        $innerHTML = implode("\n", $innerHTML);
        $outerHTML = $helper->evaluate(ABSOLUTE_PATH . 'new_campaign_modal/html/wrapper.html', get_defined_vars());
        return $outerHTML;

    }

    public function getPredefinedGeo(){
        $result = [];
        $query = DB::prep("
          SELECT
            predefined_geo.name, li_location.linked_in_id, li_location.location_name 
          FROM 
            predefined_geo 
              JOIN predefined_geo_to_location ON predefined_geo.id = predefined_geo_to_location.predefined_geo_id
              JOIN li_location ON predefined_geo_to_location.li_location_id = li_location.id
          WHERE
            li_location.sales <> 1
          ORDER BY 
            predefined_geo.weight DESC , predefined_geo_to_location.weight DESC 
         ");
        $query->execute();

        while ($row = $query->fetch()){
            $result[$row['name']][0] = '';
            $result[$row['name']][1][$row['linked_in_id']] = ['', $row['location_name']];
        }
        return $result;
    }

    protected function buildMenuItems()
    {
        //Each entry of this array corresponds to an html file in the html folder. These also correspond to a the modal window.
        //For each item in this array, there will be a new entry in the left pane list that will work the exact same way.
        return array(
            'title',
            'keywords',
            'geography',
            'relationship',
            'industry',
            'company',
            'school',
            'first_name',
            'last_name'
        );
    }
}