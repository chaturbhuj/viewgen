<?PHP

include_once( '../helper.php' );

class ModalController{
	
	protected $menuItems;
	protected $idToLanguage = array(
		'ab' => 'Abkhaz',
		'aa' => 'Afar',
		'af' => 'Afrikaans',
		'ak' => 'Akan',
		'sq' => 'Albanian',
		'am' => 'Amharic',
		'ar' => 'Arabic',
		'an' => 'Aragonese',
		'hy' => 'Armenian',
		'as' => 'Assamese',
		'av' => 'Avaric',
		'ae' => 'Avestan',
		'ay' => 'Aymara',
		'az' => 'Azerbaijani',
		'bm' => 'Bambara',
		'ba' => 'Bashkir',
		'eu' => 'Basque',
		'be' => 'Belarusian',
		'bn' => 'Bengali, Bangla',
		'bh' => 'Bihari',
		'bi' => 'Bislama',
		'bs' => 'Bosnian',
		'br' => 'Breton',
		'bg' => 'Bulgarian',
		'my' => 'Burmese',
		'ca' => 'Catalan',
		'ch' => 'Chamorro',
		'ce' => 'Chechen',
		'ny' => 'Chichewa, Chewa, Nyanja',
		'zh' => 'Chinese',
		'cv' => 'Chuvash',
		'kw' => 'Cornish',
		'co' => 'Corsican',
		'cr' => 'Cree',
		'hr' => 'Croatian',
		'cs' => 'Czech',
		'da' => 'Danish',
		'dv' => 'Divehi, Dhivehi, Maldivian',
		'nl' => 'Dutch',
		'dz' => 'Dzongkha',
		'en' => 'English',
		'eo' => 'Esperanto',
		'et' => 'Estonian',
		'ee' => 'Ewe',
		'fo' => 'Faroese',
		'fj' => 'Fijian',
		'fi' => 'Finnish',
		'fr' => 'French',
		'ff' => 'Fula, Fulah, Pulaar, Pular',
		'gl' => 'Galician',
		'ka' => 'Georgian',
		'de' => 'German',
		'el' => 'Greek (modern)',
		'gn' => 'Guaraní',
		'gu' => 'Gujarati',
		'ht' => 'Haitian, Haitian Creole',
		'ha' => 'Hausa',
		'he' => 'Hebrew (modern)',
		'hz' => 'Herero',
		'hi' => 'Hindi',
		'ho' => 'Hiri Motu',
		'hu' => 'Hungarian',
		'ia' => 'Interlingua',
		'id' => 'Indonesian',
		'ie' => 'Interlingue',
		'ga' => 'Irish',
		'ig' => 'Igbo',
		'ik' => 'Inupiaq',
		'io' => 'Ido',
		'is' => 'Icelandic',
		'it' => 'Italian',
		'iu' => 'Inuktitut',
		'ja' => 'Japanese',
		'jv' => 'Javanese',
		'kl' => 'Kalaallisut, Greenlandic',
		'kn' => 'Kannada',
		'kr' => 'Kanuri',
		'ks' => 'Kashmiri',
		'kk' => 'Kazakh',
		'km' => 'Khmer',
		'ki' => 'Kikuyu, Gikuyu',
		'rw' => 'Kinyarwanda',
		'ky' => 'Kyrgyz',
		'kv' => 'Komi',
		'kg' => 'Kongo',
		'ko' => 'Korean',
		'ku' => 'Kurdish',
		'kj' => 'Kwanyama, Kuanyama',
		'la' => 'Latin',
		'lb' => 'Luxembourgish, Letzeburgesch',
		'lg' => 'Ganda',
		'li' => 'Limburgish, Limburgan, Limburger',
		'ln' => 'Lingala',
		'lo' => 'Lao',
		'lt' => 'Lithuanian',
		'lu' => 'Luba-Katanga',
		'lv' => 'Latvian',
		'gv' => 'Manx',
		'mk' => 'Macedonian',
		'mg' => 'Malagasy',
		'ms' => 'Malay',
		'ml' => 'Malayalam',
		'mt' => 'Maltese',
		'mi' => 'Māori',
		'mr' => 'Marathi (Marāṭhī)',
		'mh' => 'Marshallese',
		'mn' => 'Mongolian',
		'na' => 'Nauruan',
		'nv' => 'Navajo, Navaho',
		'nd' => 'Northern Ndebele',
		'ne' => 'Nepali',
		'ng' => 'Ndonga',
		'nb' => 'Norwegian Bokmål',
		'nn' => 'Norwegian Nynorsk',
		'no' => 'Norwegian',
		'ii' => 'Nuosu',
		'nr' => 'Southern Ndebele',
		'oc' => 'Occitan',
		'oj' => 'Ojibwe, Ojibwa',
		'cu' => 'Old Church Slavonic, Church Slavonic, Old Bulgarian',
		'om' => 'Oromo',
		'or' => 'Oriya',
		'os' => 'Ossetian, Ossetic',
		'pa' => 'Eastern Punjabi, Eastern Panjabi',
		'pi' => 'Pāli',
		'fa' => 'Persian (Farsi)',
		'pl' => 'Polish',
		'ps' => 'Pashto, Pushto',
		'pt' => 'Portuguese',
		'qu' => 'Quechua',
		'rm' => 'Romansh',
		'rn' => 'Kirundi',
		'ro' => 'Romanian',
		'ru' => 'Russian',
		'sa' => 'Sanskrit (Saṁskṛta)',
		'sc' => 'Sardinian',
		'sd' => 'Sindhi',
		'se' => 'Northern Sami',
		'sm' => 'Samoan',
		'sg' => 'Sango',
		'sr' => 'Serbian',
		'gd' => 'Scottish Gaelic, Gaelic',
		'sn' => 'Shona',
		'si' => 'Sinhalese, Sinhala',
		'sk' => 'Slovak',
		'sl' => 'Slovene',
		'so' => 'Somali',
		'st' => 'Southern Sotho',
		'es' => 'Spanish',
		'su' => 'Sundanese',
		'sw' => 'Swahili',
		'ss' => 'Swati',
		'sv' => 'Swedish',
		'ta' => 'Tamil',
		'te' => 'Telugu',
		'tg' => 'Tajik',
		'th' => 'Thai',
		'ti' => 'Tigrinya',
		'bo' => 'Tibetan Standard, Tibetan, Central',
		'tk' => 'Turkmen',
		'tl' => 'Tagalog',
		'tn' => 'Tswana',
		'to' => 'Tonga (Tonga Islands)',
		'tr' => 'Turkish',
		'ts' => 'Tsonga',
		'tt' => 'Tatar',
		'tw' => 'Twi',
		'ty' => 'Tahitian',
		'ug' => 'Uyghur',
		'uk' => 'Ukrainian',
		'ur' => 'Urdu',
		'uz' => 'Uzbek',
		've' => 'Venda',
		'vi' => 'Vietnamese',
		'vo' => 'Volapük',
		'wa' => 'Walloon',
		'cy' => 'Welsh',
		'wo' => 'Wolof',
		'fy' => 'Western Frisian',
		'xh' => 'Xhosa',
		'yi' => 'Yiddish',
		'yo' => 'Yoruba',
		'za' => 'Zhuang, Chuang',
		'zu' => 'Zulu',
		);
	protected $idToCountry = array(
		'af' => 'Afghanistan',
		'al' => 'Albania',
		'dz' => 'Algeria',
		'as' => 'American Samoa',
		'ad' => 'Andorra',
		'ao' => 'Angola',
		'ai' => 'Anguilla',
		'aq' => 'Antarctica',
		'ag' => 'Antigua and Barbuda',
		'ar' => 'Argentina',
		'am' => 'Armenia',
		'aw' => 'Aruba',
		'au' => 'Australia',
		'at' => 'Austria',
		'az' => 'Azerbaijan',
		'bs' => 'Bahamas',
		'bh' => 'Bahrain',
		'bd' => 'Bangladesh',
		'bb' => 'Barbados',
		'by' => 'Belarus',
		'be' => 'Belgium',
		'bz' => 'Belize',
		'bj' => 'Benin',
		'bm' => 'Bermuda',
		'bt' => 'Bhutan',
		'bo' => 'Bolivia',
		'bq' => 'Bonaire',
		'ba' => 'Bosnia and Herzegovina',
		'bw' => 'Botswana',
		'bv' => 'Bouvet Island',
		'br' => 'Brazil',
		'io' => 'British Indian Ocean Territory',
		'bn' => 'Brunei Darussalam',
		'bg' => 'Bulgaria',
		'bf' => 'Burkina Faso',
		'bi' => 'Burundi',
		'kh' => 'Cambodia',
		'cm' => 'Cameroon',
		'ca' => 'Canada',
		'cv' => 'Cape Verde',
		'ky' => 'Cayman Islands',
		'cf' => 'Central African Republic',
		'td' => 'Chad',
		'cl' => 'Chile',
		'cn' => 'China',
		'cx' => 'Christmas Island',
		'cc' => 'Cocos (Keeling) Islands',
		'co' => 'Colombia',
		'km' => 'Comoros',
		'cg' => 'Congo',
		'cd' => 'Democratic Republic of the Congo',
		'ck' => 'Cook Islands',
		'cr' => 'Costa Rica',
		'hr' => 'Croatia',
		'cu' => 'Cuba',
		'cw' => 'CuraÃ§ao',
		'cy' => 'Cyprus',
		'cz' => 'Czech Republic',
		'ci' => 'CÃ´te d\'Ivoire',
		'dk' => 'Denmark',
		'dj' => 'Djibouti',
		'dm' => 'Dominica',
		'do' => 'Dominican Republic',
		'ec' => 'Ecuador',
		'eg' => 'Egypt',
		'sv' => 'El Salvador',
		'gq' => 'Equatorial Guinea',
		'er' => 'Eritrea',
		'ee' => 'Estonia',
		'et' => 'Ethiopia',
		'fk' => 'Falkland Islands (Malvinas)',
		'fo' => 'Faroe Islands',
		'fj' => 'Fiji',
		'fi' => 'Finland',
		'fr' => 'France',
		'gf' => 'French Guiana',
		'pf' => 'French Polynesia',
		'tf' => 'French Southern Territories',
		'ga' => 'Gabon',
		'gm' => 'Gambia',
		'ge' => 'Georgia',
		'de' => 'Germany',
		'gh' => 'Ghana',
		'gi' => 'Gibraltar',
		'gr' => 'Greece',
		'gl' => 'Greenland',
		'gd' => 'Grenada',
		'gp' => 'Guadeloupe',
		'gu' => 'Guam',
		'gt' => 'Guatemala',
		'gg' => 'Guernsey',
		'gn' => 'Guinea',
		'gw' => 'Guinea-Bissau',
		'gy' => 'Guyana',
		'ht' => 'Haiti',
		'hm' => 'Heard Island and McDonald Mcdonald Islands',
		'va' => 'Holy See (Vatican City State)',
		'hn' => 'Honduras',
		'hk' => 'Hong Kong',
		'hu' => 'Hungary',
		'is' => 'Iceland',
		'in' => 'India',
		'id' => 'Indonesia',
		'ir' => 'Iran, Islamic Republic of',
		'iq' => 'Iraq',
		'ie' => 'Ireland',
		'im' => 'Isle of Man',
		'il' => 'Israel',
		'it' => 'Italy',
		'jm' => 'Jamaica',
		'jp' => 'Japan',
		'je' => 'Jersey',
		'jo' => 'Jordan',
		'kz' => 'Kazakhstan',
		'ke' => 'Kenya',
		'ki' => 'Kiribati',
		'kp' => 'Korea, Democratic People\'s Republic of',
		'kr' => 'Korea, Republic of',
		'kw' => 'Kuwait',
		'kg' => 'Kyrgyzstan',
		'la' => 'Lao People\'s Democratic Republic',
		'lv' => 'Latvia',
		'lb' => 'Lebanon',
		'ls' => 'Lesotho',
		'lr' => 'Liberia',
		'ly' => 'Libya',
		'li' => 'Liechtenstein',
		'lt' => 'Lithuania',
		'lu' => 'Luxembourg',
		'mo' => 'Macao',
		'mk' => 'Macedonia, the Former Yugoslav Republic of',
		'mg' => 'Madagascar',
		'mw' => 'Malawi',
		'my' => 'Malaysia',
		'mv' => 'Maldives',
		'ml' => 'Mali',
		'mt' => 'Malta',
		'mh' => 'Marshall Islands',
		'mq' => 'Martinique',
		'mr' => 'Mauritania',
		'mu' => 'Mauritius',
		'yt' => 'Mayotte',
		'mx' => 'Mexico',
		'fm' => 'Micronesia, Federated States of',
		'md' => 'Moldova, Republic of',
		'mc' => 'Monaco',
		'mn' => 'Mongolia',
		'me' => 'Montenegro',
		'ms' => 'Montserrat',
		'ma' => 'Morocco',
		'mz' => 'Mozambique',
		'mm' => 'Myanmar',
		'na' => 'Namibia',
		'nr' => 'Nauru',
		'np' => 'Nepal',
		'nl' => 'Netherlands',
		'nc' => 'New Caledonia',
		'nz' => 'New Zealand',
		'ni' => 'Nicaragua',
		'ne' => 'Niger',
		'ng' => 'Nigeria',
		'nu' => 'Niue',
		'nf' => 'Norfolk Island',
		'mp' => 'Northern Mariana Islands',
		'no' => 'Norway',
		'om' => 'Oman',
		'pk' => 'Pakistan',
		'pw' => 'Palau',
		'ps' => 'Palestine, State of',
		'pa' => 'Panama',
		'pg' => 'Papua New Guinea',
		'py' => 'Paraguay',
		'pe' => 'Peru',
		'ph' => 'Philippines',
		'pn' => 'Pitcairn',
		'pl' => 'Poland',
		'pt' => 'Portugal',
		'pr' => 'Puerto Rico',
		'qa' => 'Qatar',
		'ro' => 'Romania',
		'ru' => 'Russian Federation',
		'rw' => 'Rwanda',
		're' => 'Reunion',
		'bl' => 'Saint Barthelemy',
		'sh' => 'Saint Helena',
		'kn' => 'Saint Kitts and Nevis',
		'lc' => 'Saint Lucia',
		'mf' => 'Saint Martin (French part)',
		'pm' => 'Saint Pierre and Miquelon',
		'vc' => 'Saint Vincent and the Grenadines',
		'ws' => 'Samoa',
		'sm' => 'San Marino',
		'st' => 'Sao Tome and Principe',
		'sa' => 'Saudi Arabia',
		'sn' => 'Senegal',
		'rs' => 'Serbia',
		'sc' => 'Seychelles',
		'sl' => 'Sierra Leone',
		'sg' => 'Singapore',
		'sx' => 'Sint Maarten (Dutch part)',
		'sk' => 'Slovakia',
		'si' => 'Slovenia',
		'sb' => 'Solomon Islands',
		'so' => 'Somalia',
		'za' => 'South Africa',
		'gs' => 'South Georgia and the South Sandwich Islands',
		'ss' => 'South Sudan',
		'es' => 'Spain',
		'lk' => 'Sri Lanka',
		'sd' => 'Sudan',
		'sr' => 'Suriname',
		'sj' => 'Svalbard and Jan Mayen',
		'sz' => 'Swaziland',
		'se' => 'Sweden',
		'ch' => 'Switzerland',
		'sy' => 'Syrian Arab Republic',
		'tw' => 'Taiwan, Province of China',
		'tj' => 'Tajikistan',
		'tz' => 'United Republic of Tanzania',
		'th' => 'Thailand',
		'tl' => 'Timor-Leste',
		'tg' => 'Togo',
		'tk' => 'Tokelau',
		'to' => 'Tonga',
		'tt' => 'Trinidad and Tobago',
		'tn' => 'Tunisia',
		'tr' => 'Turkey',
		'tm' => 'Turkmenistan',
		'tc' => 'Turks and Caicos Islands',
		'tv' => 'Tuvalu',
		'ug' => 'Uganda',
		'ua' => 'Ukraine',
		'ae' => 'United Arab Emirates',
		'gb' => 'United Kingdom',
		'us' => 'United States',
		'um' => 'United States Minor Outlying Islands',
		'uy' => 'Uruguay',
		'uz' => 'Uzbekistan',
		'vu' => 'Vanuatu',
		've' => 'Venezuela',
		'vn' => 'Viet Nam',
		'vg' => 'British Virgin Islands',
		'vi' => 'US Virgin Islands',
		'wf' => 'Wallis and Futuna',
		'eh' => 'Western Sahara',
		'ye' => 'Yemen',
		'zm' => 'Zambia',
		'zw' => 'Zimbabwe',
		'ax' => 'Aland Islands'
		);
	protected $idToIndustry = array(
        'High Tech' => ['10.1%', [
            '96' => ['2.4%', 'Information Technology and Services'],
            '4' => ['2.3%', 'Computer Software'],
            '8' => ['1.8%', 'Telecommunications'],
            '6' => ['1.1%', 'Internet'],
            '5' => ['0.5%', 'Computer Networking'],
            '3' => ['0.4%', 'Computer Hardware'],
            '24' => ['0.4%', 'Consumer Electronics'],
            '118' => ['0.3%', 'Computer & Network Security'],
            '84' => ['0.3%', 'Information Services'],
            '109' => ['0.2%', 'Computer Games'],
            '7' => ['0.2%', 'Semiconductors'],
            '119' => ['0.1%', 'Wireless'],
            '114' => ['0.1%', 'Nanotechnology'],
        ]],
        'Retail ' => ['7.7%', [
            '27' => ['2.5%', 'Retail'],
            '34' => ['1.5%', 'Food & Beverages'],
            '19' => ['0.8%', 'Apparel & Fashion'],
            '25' => ['0.8%', 'Consumer Goods'],
            '26' => ['0.3%', 'Furniture'],
            '22' => ['0.2%', 'Supermarkets'],
            '142' => ['0.2%', 'Wine and Spirits'],
            '21' => ['0.1%', 'Tobacco'],
            '111' => ['0.8%', 'Arts and Crafts'],
            '18' => ['0.5%', 'Cosmetics'],
            '143' => ['', 'Luxury Goods & Jewelry'],
            '20' => ['', 'Sporting Goods'],
        ]],
        'Finance' => ['6.6%', [
            '43' => ['2.5%', 'Financial Services'],
            '41' => ['2%', 'Banking'],
            '42' => ['1.3%', 'Insurance'],
            '46' => ['0.3%', 'Investment Management'],
            '45' => ['0.2%', 'Investment Banking'],
            '129' => ['0.2%', 'Capital Markets'],
            '106' => ['0.1%', 'Venture Capital & Private Equity'],
        ]],
        'Professional Services' => ['10.6%', [
            '47' => ['2.4%', 'Accounting'],
            '80' => ['2.2%', 'Marketing and Advertising'],
            '137' => ['1.2%', 'Human Resources'],
            '99' => ['1.1%', 'Design'],
            '136' => ['1%', 'Photography'],
            '50' => ['0.9%', 'Architecture & Planning'],
            '98' => ['0.5%', 'Public Relations and Communications'],
            '105' => ['0.5%', 'Professional Training & Coaching'],
            '104' => ['0.4%', 'Staffing and Recruiting'],
            '140' => ['0.5%', 'Graphic Design'],
            '97' => ['0.2%', 'Market Research'],
            '108' => ['0.2%', 'Translation and Localization'],
        ]],
        'Service Industries' => ['12.1%', [
            '48' => ['3.4%', 'Construction'],
            '44' => ['1.8%', 'Real Estate'],
            '128' => ['0.6%', 'Commercial Real Estate'],
            '91' => ['1.1%', 'Consumer Services'],
            '116' => ['0.9%', 'Logistics and Supply Chain'],
            '32' => ['0.8%', 'Restaurants'],
            '59' => ['0.7%', 'Utilities'],
            '121' => ['0.5%', 'Security and Investigations'],
            '88' => ['0.5%', 'Individual & Family Services'],
            '120' => ['0.1%', 'Alternative Dispute Resolution'],
            '110' => ['0.1%', 'Events Services'],
            '87' => ['0.1%', 'Package/Freight Delivery'],
            '122' => ['', 'Facilities Services'],
            '11' => ['', 'Management Consulting'],
            '123' => ['', 'Outsourcing/Offshoring'],
            '144' => ['', 'Renewables & Environment'],
        ]],
        'Manufacturing & Industrial' => ['12.6%', [
            '53' => ['2%', 'Automotive'],
            '57' => ['1.7%', 'Oil & Energy'],
            '135' => ['1.5%', 'Mechanical or Industrial Engineering'],
            '51' => ['1%', 'Civil Engineering'],
            '55' => ['0.7%', 'Machinery'],
            '52' => ['0.5%', 'Aviation & Aerospace'],
            '147' => ['0.4%', 'Industrial Automation'],
            '1' => ['0.3%', 'Defense & Space'],
            '112' => ['1.4%', 'Electrical/Electronic Manufacturing'],
            '60' => ['0.4%', 'Textiles'],
            '117' => ['0.2%', 'Plastics'],
            '49' => ['0.6%', 'Building Materials'],
            '138' => ['0.4%', 'Business Supplies and Equipment'],
            '133' => ['0.7%', 'Wholesale'],
            '56' => ['0.6%', 'Mining & Metals'],
            '61' => ['0.2%', 'Paper & Forest Products'],
            '54' => ['', 'Chemicals'],
            '145' => ['', 'Glass'],
            '134' => ['', 'Import and Export'],
            '141' => ['', 'International Trade and Development'],
            '95' => ['', 'Maritime'],
            '146' => ['', 'Packaging and Containers'],
            '62' => ['', 'Railroad Manufacture'],
            '58' => ['', 'Shipbuilding'],
            '93' => ['', 'Warehousing'],
        ]],
		'Healthcare' => ['8.6%', [
            '14' => ['3.5%', 'Hospital & Health Care'],
            '13' => ['1.8%', 'Medical Practice'],
            '124' => ['1.3%', 'Health, Wellness & Fitness'],
            '139' => ['0.5%', 'Mental Health Care'],
            '125' => ['0.2%', 'Alternative Medicine'],
            '16' => ['0.2%', 'Veterinary'],
        ]],
        'Biotech & Pharma' => ['2.0%', [
            '15' => ['1.1%', 'Pharmaceuticals'],
    		'17' => ['0.5%', 'Medical Devices'],
    		'12' => ['0.4%', 'Biotechnology'],
        ]],
        'Travel' => ['3.5%', [
            '31' => ['1.3%', 'Hospitality'],
            '92' => ['1%', 'Transportation/Trucking/Railroad'],
            '30' => ['0.6%', 'Leisure'],
            '94' => ['0.5%', 'Airlines/Aviation'],
            '40' => ['0.1%', 'Recreational Facilities and Services'],
            '29' => ['', 'Gambling & Casinos'],
        ]],
        'Media' => ['2.6%', [
            '28' => ['0.8%', 'Entertainment'],
            '36' => ['0.5%', 'Broadcast Media'],
            '126' => ['0.5%', 'Media Production'],
            '113' => ['0.2%', 'Online Media'],
            '127' => ['0.2%', 'Animation'],
            '35' => ['0.2%', 'Motion Pictures and Film'],
            '81' => ['0.2%', 'Newspapers'],
            '83' => ['', 'Printing'],
            '82' => ['', 'Publishing'],
            '103' => ['', 'Writing and Editing'],
        ]],
        'Legal' => ['1.7%', [
            '9' => ['1%', 'Law Practice'],
            '10' => ['0.7%', 'Legal Services'],
        ]],
        'Food Production' => ['1.7%', [
            '23' => ['0.8%', 'Food Production'],
            '63' => ['0.6%', 'Farming'],
            '64' => ['0.1%', 'Ranching'],
            '65' => ['0.1%', 'Dairy'],
            '66' => ['0.1%', 'Fishery'],
        ]],
        'Government' => ['5.2%', [
            '75' => ['2.1%', 'Government Administration'],
            '70' => ['1.1%', 'Research'],
            '71' => ['0.5%', 'Military'],
            '76' => ['0.3%', 'Executive Office'],
            '77' => ['0.3%', 'Law Enforcement'],
            '73' => ['0.2%', 'Judiciary'],
            '74' => ['0.2%', 'International Affairs'],
            '78' => ['0.2%', 'Public Safety'],
            '79' => ['0.2%', 'Public Policy'],
            '72' => ['0.1%', 'Legislative Office'],
            '148' => ['', 'Government Relations'],
        ]],
        'Organizations & Non-Profits' => ['3.2%', [
            '100' => ['0.8%', 'Nonprofit Organization Management'],
            '86' => ['0.6%', 'Environmental Services'],
            '88' => ['0.5%', 'Individual and Family Services'],
            '90' => ['0.4%', 'Civic & Social Organization'],
            '89' => ['0.3%', 'Religious Institutions'],
            '102' => ['0.2%', 'Program Development'],
            '101' => ['0.1%', 'Fund-Raising'],
            '107' => ['0.1%', 'Political Organization'],
            '131' => ['0.1%', 'Philanthropy'],
            '130' => ['0.1%', 'Think Tanks'],
        ]],
        'Education' => ['6.2%', [
            '69' => ['2.8%', 'Education Management'],
    		'68' => ['1.8%', 'Higher Education'],
    		'67' => ['1.3%', 'Primary/Secondary Education'],
    		'132' => ['0.3%', 'E-Learning'],
            '85' => ['', 'Libraries'],
        ]],
        'Art' => ['1.9%', [
            '115' => ['0.7%', 'Music'],
    		'33' => ['0.6%', 'Sports'],
    		'38' => ['0.4%', 'Fine Art'],
    		'39' => ['0.2%', 'Performing Arts'],
            '37' => ['', 'Museums and Institutions'],
        ]],
//        'Others' => ['', [
//        ]],
		);
	protected $newIdToSeniorityLevel = array(
        '8' => 'CXO',
        '9' => 'Partner',
        '10' => 'Owner',
        '7' => 'VP',
        '6' => 'Director',
        '5' => 'Manager',
        '4' => 'Senior',
        '3' => 'Entry',
        '2' => 'Training',
		'1' => 'Unpaid',
		);
	protected $newIdToFunction = array(
		'1' => 'Accounting',
		'2' => 'Administrative',
		'3' => 'Arts and Design',
		'4' => 'Business Development',
		'5' => 'Community and Social Services',
		'6' => 'Consulting',
		'7' => 'Education',
		'8' => 'Engineering',
		'9' => 'Entrepreneurship',
		'10' => 'Finance',
		'11' => 'Healthcare Services',
		'12' => 'Human Resources',
		'13' => 'Information Technology',
		'14' => 'Legal',
		'15' => 'Marketing',
		'16' => 'Media and Communication',
		'17' => 'Military and Protective Services',
		'18' => 'Operations',
		'19' => 'Product Management',
		'20' => 'Program and Project Management',
		'21' => 'Purchasing',
		'22' => 'Quality Assurance',
		'23' => 'Real Estate',
		'24' => 'Research',
		'25' => 'Sales',
		'26' => 'Support',
		);
    protected $idToCompanySize = array(
        'B' => '1-10',
        'C' => '11-50',
        'D' => '51-200',
        'E' => '201-500',
        'F' => '501-1000',
        'G' => '1001-5000',
        'H' => '5001-10000',
        'I' => '10000+',
        'A' => 'Self-employed'
    );
    protected $idToTitleScope = array(
        'CURRENT' => 'Current',
        'CURRENT_OR_PAST' => 'Current or Past',
        'PAST' => 'Past',
        'PAST_NOT_CURRENT' => 'Past Not Current',
    );
    protected $idToCompanyScope = array(
        'CURRENT' => 'Current',
        'CURRENT_OR_PAST' => 'Current or Past',
        'PAST' => 'Past',
        'PAST_NOT_CURRENT' => 'Past Not Current',
    );
	protected $idToRelationship = array(
		'F' => '1st Connections',
		'S' => '2nd Connections',
		'A' => 'Group Members',
		'O' => '3rd + Everyone Else',
		);
	protected $visitsOptions = array(
		'0' => '50',
		'1' => '100',
		'2' => '200',
		'3' => '300',
		'4' => '400',
		'5' => '500',
		'6' => '600',
		'7' => '700',
		'8' => '800',
		'9' => '900',
		'10' => '1000',
		);
	protected $idToYearsInCurrentPosition = array(
		'1' => 'Less than 1 year',
		'2' => '1 to 2 years',
		'3' => '3 to 5 years',
		'4' => '6 to 10 years',
		'5' => 'More than 10 years',
		);
	protected $idToYearsInCurrentCompany = array(
		'1' => 'Less than 1 year',
		'2' => '1 to 2 years',
		'3' => '3 to 5 years',
		'4' => '6 to 10 years',
		'5' => 'More than 10 years',
		);
	protected $idToYearsOfExperience = array(
		'1' => 'Less than 1 year',
		'2' => '1 to 2 years',
		'3' => '3 to 5 years',
		'4' => '6 to 10 years',
		'5' => 'More than 10 years',
		);
    protected $idToGroups = [];

    protected $idToPredefinedRegions = array(
        'Western United States' =>['', [
            'us:ca' => ['', 'California'],
            'us:wa' => ['', 'Washington'],
            'us:az' => ['', 'Arizona'],
            'us:co' => ['', 'Colorado'],
            'us:ut' => ['', 'Utah'],
            'us:or' => ['', 'Oregon'],
            'us:nv' => ['', 'Nevada'],
            'us:id' => ['', 'Idaho'],
            'us:mt' => ['', 'Montana'],
            'us:wy' => ['', 'Wyoming'],
            'us:nm' => ['', 'New Mexico'],
            'us:hi' => ['', 'Hawaii'],
            'us:ak' => ['', 'Alaska'],
        ]],
        'Midwest United States' =>['', [
            'us:il' => ['', 'Illinois'],
            'us:in' => ['', 'Indiana'],
            'us:mi' => ['', 'Michigan'],
            'us:oh' => ['', 'Ohio'],
            'us:mn' => ['', 'Minnesota'],
            'us:wi' => ['', 'Wisconsin'],
            'us:mo' => ['', 'Missouri'],
            'us:ks' => ['', 'Kansas'],
            'us:ia' => ['', 'Iowa'],
            'us:nd' => ['', 'North Dakota'],
            'us:sd' => ['', 'South Dakota'],
            'us:ne' => ['', 'Nebraska'],
        ]],
        'Central U.S.' =>['', [
            'us:tx' => ['', 'Texas'],
            'us:ok' => ['', 'Oklahoma'],
            'us:ar' => ['', 'Arkansas'],
            'us:la' => ['', 'Louisiana'],
            'us:ms' => ['', 'Mississippi'],
            'us:al' => ['', 'Alabama'],
            'us:tn' => ['', 'Tennessee'],
            'us:ky' => ['', 'Kentucky'],
        ]],
        'Northeast U.S.' =>['', [
            'us:ny' => ['', 'New York'],
            'us:ma' => ['', 'Massachusetts'],
            'us:pa' => ['', 'Pennsylvania'],
            'us:nj' => ['', 'New Jersey'],
            'us:ct' => ['', 'Connecticut'],
            'us:ri' => ['', 'Rhode Island'],
            'us:vt' => ['', 'Vermont'],
            'us:me' => ['', 'Maine'],
            'us:nh' => ['', 'New Hampshire'],
        ]],
        'Southeast U.S.' =>['', [
            'us:fl' => ['', 'Florida'],
            'us:ga' => ['', 'Georgia'],
            'us:va' => ['', 'Virginia'],
            'us:nc' => ['', 'North Carolina'],
            'us:dc' => ['', ''],
            'us:md' => ['', 'Maryland'],
            'us:de' => ['', 'Delaware'],
            'us:sc' => ['', 'South Carolina'],
            'us:wv' => ['', 'West Virginia'],
        ]],
    );

	public function __construct($user){
        $this->idToGroups = $this->getUserGroups($user);
        $this->menuItems = $this->buildMenuItems();
        $this->idToPredefinedRegions = $this->getPredefinedGeo();
	}
	
	public function outputHTML(){
	
		$campaignFunctions = $this->checkbox( $this->newIdToFunction, 'function' );
		$yearsAtCurrentCompany = $this->checkbox($this->idToYearsInCurrentCompany, 'yearsAtCurrentCompany');
		$relationship = $this->checkbox($this->idToRelationship, 'relation');
		$companySize = $this->checkbox($this->idToCompanySize, 'companySize');
		$yearsInCurrentPosition = $this->checkbox($this->idToYearsInCurrentPosition, 'yearsInCurrentPosition');
		$seniority = $this->checkbox($this->newIdToSeniorityLevel, 'seniorityLevel');
		$yearsOfExperience = $this->checkbox($this->idToYearsOfExperience, 'yearsOfExperience');
		$countries = $this->option($this->idToCountry);
		$industries = $this->checkboxGrouped($this->idToIndustry, 'industry_location');
        $campaignGroups = $this->checkbox($this->idToGroups, 'groups');
        $predefinedRegions = $this->checkboxGrouped( $this->idToPredefinedRegions, 'predefinedRegions' );
		//This is where we evaluate all of our html files that jquery will generate the side menu with.
		if( empty( $this->menuItems ) )
			return 'what';
		$helper = new Helper;
		foreach( $this->menuItems as $m ){
			if( file_exists( ABSOLUTE_PATH.'new_campaign_modal/html/campaign_'.$m.'.html' ) )
				$innerHTML[] = $helper->evaluate( ABSOLUTE_PATH.'new_campaign_modal/html/campaign_'.$m.'.html', get_defined_vars() );
		}
		$innerHTML[] = '<script> window.CampaignBuilderDialogImpl =  "FullCampaignBuilderDialog"; </script>';
		$innerHTML = implode( "\n", $innerHTML );
		$outerHTML = $helper->evaluate( ABSOLUTE_PATH.'new_campaign_modal/html/wrapper.html', get_defined_vars() );
		return $outerHTML;
		
	}

	public function getUserGroups($user){
        $groups = [];
        $query = DB::prep("SELECT * FROM li_groups JOIN li_users_groups ON li_groups.linked_in_id = li_users_groups.group_li_id WHERE li_users_groups.user_li_id = :user_id");
        $query->execute([
            'user_id' => $user['linked_in_id']
        ]);
        while ($group = $query->fetch()){
            $groups[$group['linked_in_id']] = $group['name'];
        }
        return $groups;
    }

    public function getPredefinedGeo(){
        $result = [];
        $query = DB::prep("
          SELECT
            predefined_geo.name, li_location.linked_in_id, li_location.location_name 
          FROM 
            predefined_geo 
              JOIN predefined_geo_to_location ON predefined_geo.id = predefined_geo_to_location.predefined_geo_id
              JOIN li_location ON predefined_geo_to_location.li_location_id = li_location.id
          ORDER BY 
            predefined_geo.weight DESC , predefined_geo_to_location.weight DESC 
         ");
        $query->execute();

        while ($row = $query->fetch()){
            $result[$row['name']][0] = '';
            $result[$row['name']][1][$row['linked_in_id']] = ['', $row['location_name']];
        }
        return $result;
    }

    protected function checkbox( $arr, $fieldName ){
		
		foreach( $arr as $k => $v ){
				$out[] = '<label><input type="checkbox" name="'.$fieldName.'['.$k.']"/>'.$v.'</label>';
		}
		return implode( "\n", $out );
			
	}
	
    protected function checkboxGrouped( $arr, $fieldName){
   		$out = [];
       foreach( $arr as $k => $v ){
                $out[] = '<br /><label class="checkbox-grouped"><input class="cb-header" type="checkbox" data-group="' . $k . '"/><span class="group-header"><span class="label-name">'. $k .'</span><span class="percents">' . $v[0] . '</span></span></label>';
           foreach( $v[1] as $k2 => $v2 ){
                    $out[] = '<label class="leaf checkbox-grouped"><input class="cb-leaf" type="checkbox" data-group="' . $k . '" name="'.$fieldName.'['.$k2.']"/><span class="label-name">'.$v2[1] .'</span><span class="percents">' . $v2[0] . '</span></label>';
            }
        }
   		    
   		return implode( "\n", $out );
   			
   	}
	protected function checkboxTwo( $arr, $fieldName ){
	
		foreach( $arr as $k => $v ){
			$out[] = '<label><input type="checkbox" name="'.$fieldName.'['.$v.']"/>'.$v.'</label>';
	}
		return implode( "\n", $out );
			
	}


	protected function option( $arr ){

		foreach( $arr as $k => $v ){
			$out[] = '<option value="'.$k.'">'.$v.'</option>';
		}
		return implode( "\n", $out );

	}
	
	protected function buildMenuItems(){
		
		//Each entry of this array corresponds to an html file in the html folder. These also correspond to a the modal window.
		//For each item in this array, there will be a new entry in the left pane list that will work the exact same way.
		return array( 
			'title', 
			'keywords', 
			'geography', 
			'relationship', 
			'industry', 
			'company', 
			'company_size', 
			'function', 
			'seniority_level',
//			groups hasn't been added yet
			'groups',
			'years_in_current_position', 
			'years_at_current_company',
			'years_of_experience',
//			for a future version
//			'past_company',
//			company type isn't supported yet, maybe in a future version
//			'company_type',
			'postal_code',
			'school',
//			for a future version
//			'profile_language',
//			for a future version
//			'member_since',
//			for a future version
//			'interested_in',
//		for a future version
//			'posted_content_keywords',
			'first_name',
			'last_name'
			);
		
	}
	
}

?>