<?php

$HTML_TITLE = "Billing";
$ACTIVE_HEADER_PAGE = "billing";

require_once('../authenticate.php');

global $_COMPANY_DATA;
global $_USER_DATA;
global $CONFIG;

$company_id = (string)($_GET['company_id'] ?? $_COMPANY_DATA["id"] ?? '');
//default user is fine to be  0 here, as long as company exists -
// this usr_id is NOT user id that on the campaign_builder
//campaign_builder user_id is a li_users id
$user_id = (string)($_GET['user_id'] ?? $_USER_DATA["id"] ?? '0');


if ($company_id == '') {
	if($_USER_DATA["admin"] ?? false){
		redirect("/adminDashboard.php");
	}
	die;
}

include('header.php');
require_once ('../lib/recurly.php');

if(!empty($_COMPANY_DATA['recurlyAccountCode'])) {
	try {
		$accountR = Recurly_Account::get($_COMPANY_DATA['recurlyAccountCode']);
		$subscriptions = Recurly_SubscriptionList::getForAccount($_COMPANY_DATA['recurlyAccountCode']);
	} catch (Exception $e) {
	}
}

if(!empty($accountR)){
	$invoices = Recurly_InvoiceList::getForAccount($_COMPANY_DATA['recurlyAccountCode']);

	$activeSubscriptions = array();
	$lastActivatedSubscription = null;
	foreach ($subscriptions as $tmpSubscription) {
		if ($tmpSubscription->state == "active") {
			$activeSubscriptions[] = $tmpSubscription;
		}

		if ($lastActivatedSubscription == null) {
			$lastActivatedSubscription = $tmpSubscription;
		} else if ($lastActivatedSubscription->activated_at < $tmpSubscription->activated_at) {
			$lastActivatedSubscription = $tmpSubscription;
		}
	}

	if (count($activeSubscriptions) == 1) {
		$subscription = $activeSubscriptions[0];
	} else if (count($activeSubscriptions) > 1) {
		die("This account has multiple active subscriptions, please contact info@searchquant.net to handle your subscriptions.");
	} else {
		$subscription = $lastActivatedSubscription;
	}

	if (isset($_GET['cancel']) and $_GET['cancel'] == "true") {
		try {
			$subscription->cancel();
			$query = DB::prep("update rc_subscription set state='canceled' where company_id=?");
			$query->execute(array($company_id));
//			$query = DB::prep("update company set frozen=1 where id=?");
//			$query->execute(array($company_id));
		} catch (Exception $e) {
		}

		redirect('/new-ui/billing.php?company_id='.$company_id);
	}
}

?>

<style>
	.billing-price {
		display: block;
		float: left;
		background: white;
        padding: 20px 20px 0 20px;
        margin: 50px auto;
        border: 1px solid #ccc;
        width: 350px;
	}


    /*.price-row:first-child {*/
        /*border: 2px solid #008abb;*/
    /*}*/
    .price-row {
        width: 100%;
    }

    .price-left, .price-right {
        line-height: 25px;
        font-size: 20px;
        font-weight: inherit;
    }
    .price-row, .price-left, .price-right {
        height: 30px;
    }
    .price-left {
        float: left;
        width: 20%;
        padding-right: 10px;
        text-align: right;
    }

    .price-right {
        float: left;
        width: 30%;
        padding-left: 10px;
        text-align: left;
    }

    .price-border {
        height: 14px;
        float: left;
        width: 50%;
        border-bottom: 1px solid #008abb;
    }

    .billing-price table {
        width: 100%;
    }
    table {
        max-width: 100%;
        background-color: transparent;
        border-collapse: collapse;
        border-spacing: 0;
    }
</style>

<div class="new_container">
<div class="container" id="campaign_builder" data-company-id="<?=$company_id?>"
	data-user-id="<?=$user_id?>">
	<div class="row">
		<div class="col-md-12">

			<div class="billing-price">
				<table>
					<tr>
						<td valign="top" colspan="2">
							<div class="form_section" style="text-align: center;">
								<h2>Pricing</h2>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<span class="blue-billing-info"># of Users</span>
						</td>
						<td style="text-align: right">
							<span class="blue-billing-info">$ per User per Month</span>
						</td>
					</tr>
				</table>
				<div>
					<div class="price-row">
						<div class="price-left">*1</div>
						<div class="price-border"></div>
						<div class="price-right">$200</div>
					</div>
					<div class="price-row">
						<div class="price-left">2</div>
						<div class="price-border"></div>
						<div class="price-right">$190</div>
					</div>
					<div class="price-row">
						<div class="price-left">3</div>
						<div class="price-border"></div>
						<div class="price-right">$180</div>
					</div>
					<div class="price-row">
						<div class="price-left">4</div>
						<div class="price-border"></div>
						<div class="price-right">$170</div>
					</div>
					<div class="price-row">
						<div class="price-left">5</div>
						<div class="price-border"></div>
						<div class="price-right">$160</div>
					</div>
					<div class="price-row">
						<div class="price-left">6</div>
						<div class="price-border"></div>
						<div class="price-right">$150</div>
					</div>
					<div class="price-row">
						<div class="price-left">7</div>
						<div class="price-border"></div>
						<div class="price-right">$140</div>
					</div>
					<div class="price-row">
						<div class="price-left">8</div>
						<div class="price-border"></div>
						<div class="price-right">$130</div>
					</div>
					<div class="price-row">
						<div class="price-left">9</div>
						<div class="price-border"></div>
						<div class="price-right">$120</div>
					</div>
					<div class="price-row">
						<div class="price-left">10+</div>
						<div class="price-border"></div>
						<div class="price-right">$110</div>
					</div>
				</div>
				<p>* All trials start as a 1 user plan.</p>
				<p>Additional users are added from within the service.</p>
                <p> The trial is free for 7 days. After 7 days, it will convert into a monthly subscription.</p>
                <p> If you cancel before 7 days, you will never be billed. If, after 7 days, you still are uncertain, we are happy discuss extending the trial and help answer any lingering questions.</p>
			</div>


			<?php if(!empty($accountR)):?>
				<div class="download-box" style="height: 200px;">
					<h2>Subscription</h2>
					<?php if ($subscription->state == 'active' or $subscription->state == 'future') { ?>
					<p>Your subscription "<?=$subscription->plan->name?>" is active and will renew on <?=$subscription->current_period_ends_at->format("M d, Y")?></p>
<!--					<p><a href=billing.php?company_id=--><?//=$company_id?><!--&cancel=true>Cancel subscription</a></p>-->
					<p>Wish to cancel? Please contact us by clicking the <button onclick="zE.activate();">Help</button> at the lower right.</p>

					<?php } else { ?>
						<p>Your subscription is <?=$subscription->state?></p>
						<p><a href=account_reactivation.php?company_id=<?=$company_id?>>Activate account</a></p>
					<?php } ?>

				</div>
				<?php if($accountR->state === "active"):?>
					<div class="download-box" style="height: auto;">
						<h2>Payments</h2>
						<p><a target="_blank" href="https://<?=$CONFIG['recurly_subdomain']?>.recurly.com/account/billing_info/edit?ht=<?=$accountR->hosted_login_token?>">Update credit card info</a></p>
						<?php foreach ($invoices as $invoice) { ?>
							<a class="download-box-link" target="_blank" href="https://<?=$CONFIG['recurly_subdomain']?>.recurly.com/account/invoices/<?=$invoice->invoice_number?>?ht=<?=$accountR->hosted_login_token?>">
								<?=$invoice->created_at->format("M j, Y")?> - <span style="<?=($invoice->state === 'failed' || $invoice->state === 'past_due') ? 'color: red' : '' ?>"><?=$invoice->state === 'collected' ? 'paid' : $invoice->state ?></span> - <?=abs($invoice->total_in_cents/100)?> <?=$invoice->currency?> <?=$invoice->total_in_cents < 0 ? '<span style="color: green">refund</span>' : ''?>
							</a>
							<br>
						<?php } ?>
					</div>
				<?php endif;?>
			<?php else:?>
				<div class="download-box" style="height: 200px;">
					<p><a href=account_reactivation.php?company_id=<?=$company_id?>>Activate account</a></p>
				</div>
			<?php endif;?>
		</div>
	</div>
</div>

<? include('footer.php'); ?>
