<?php
require_once(__DIR__."/../classes/Settings.php");
require_once(__DIR__."/../classes/linkedin/search/Url.php");

$HTML_TITLE = "Campaign Builder";
$ACTIVE_HEADER_PAGE = "campaign_builder";
$PRIORITY_MAP = [
    1 => 'Low',
    2 => 'Medium',
    3 => 'High'
];

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
    if (isset($_USER_DATA["admin"])) {
        redirect("/adminDashboard.php");
    }
    die;
}

require_once('../api/APIModel.php');

if (!isset($_GET['user_id']) || $_GET['user_id'] == '') {
    // Redirect to the first user.
    $query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0 LIMIT 1");
    $query->execute([
        'company_id' => $_GET['company_id']
    ]);

    if ($query->rowCount() == 0) {
        redirect('/new-ui/no_user.php?company_id=' . $_GET['company_id']);
        die;
    }

    $user = $query->fetch();

    redirect("/new-ui/campaign_builder.php?"
        . "company_id=" . $_GET['company_id']
        . '&user_id=' . $user['id']
        . '&linked_in_id=' . $user['linked_in_id']
    );
    die;
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND id = :user_id");
$query->execute([
    'company_id' => $_GET['company_id'],
    'user_id' => $_GET['user_id']
]);

if ($query->rowCount() == 0) {
    echo "Invalid user";
    die;
}

$user = $query->fetch();

$isSimpleSearch = false;
if($user['li_account_type'] === APIModel::FREE){
    $isSimpleSearch = true;
} elseif(empty($user['li_account_type'])){
    $isSimpleSearch = (int)$user['have_sales_navigator'] === 1 ? false : true;
}

$urlModel = \linkedinSearch\Url::getInstance($user);

$basicSearchUrl = $urlModel::BASE_URL;
$basicProfileUrl = $isSimpleSearch ? APIModel::FREE_PROFILE_URL : APIModel::SN_PROFILE_URL;

if (isset($_GET["unhide"])) {
	$query = DB::prep("UPDATE li_users SET hidden = 0 WHERE company_id = :company_id AND id = :user_id");
	$query->execute([
		'company_id' => $_GET['company_id'],
		'user_id' => $_GET['user_id']
	]);
    redirect("/new-ui/campaign_builder.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
}

if (isset($_GET["confirm_completed_onboarding"])) {
    $query = DB::prep("UPDATE li_users SET is_onboarding = 0 WHERE company_id = :company_id AND id = :user_id");
    $query->execute([
        'company_id' => $_GET['company_id'],
        'user_id' => $_GET['user_id']
    ]);
    redirect("/new-ui/campaign_builder.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
}

// Fetch the campaigns.
$query = DB::prep("SELECT *, DATE(NOW()) today, campaign_visit_count AS visits, start_date as sort_date
	FROM campaign
	WHERE
		company_id = :company_id AND
		visit_by_id = :visit_by_id
	ORDER BY IF(priority < 0, 1, 0) ASC, status ASC, if(status = 'Finished','zzz',priority) DESC, secondary_priority DESC, id DESC");

$query->execute(array(
    'company_id' => $_GET['company_id'],
    'visit_by_id' => $user['linked_in_id']
));

if ($query->rowCount() == 0) {
    $url = $urlModel->get1stDegreeUrl();

    // create a default campaign with 1st deg connections.
    $query = DB::prep("insert into campaign (company_id, publish_status, is_query_campaign, priority, start_date,
		end_date, name, visit_by, visit_by_id, status, revisit_after, locations, original_url)
		values(:company_id, :publish_status, :is_query_campaign, :priority, :start_date, :end_date, :name, :visit_by, :visit_by_id, :status, :revisit_after, :locations, :original_url)");
    $query->execute([
        'company_id' => $_GET['company_id'],
        'publish_status' => 'published',
        'is_query_campaign' => 1,
        'priority' => 3,
        'start_date' => 'NOW()',
        'end_date' => '000-00-00',
        'name' => "1st degree connections",
        'visit_by' => $user['name'],
        'visit_by_id' => $user['linked_in_id'],
        'status' => 'Active',
        'revisit_after' => 0,
        'locations' => '',
        'original_url' => $url
    ]);
    $newCampaignId = DB::get_instance()->lastInsertId();
    $query = DB::prep("insert into search_url (total_matches,url, campaign_id, company_id, visit_by, visit_by_id) values(-1,?, ?, ?, ?, ?)");

    $query->execute(array($url, $newCampaignId, $_GET['company_id'], $user['name'], $user['linked_in_id']));


    // Submit data for campaign.
    $query = DB::prep("update campaign set publish_status='published' where company_id=? and id=?");
    $query->execute(array(
        $_GET['company_id'],
        $newCampaignId
    ));

    redirect("/new-ui/campaign_builder.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
}

$campaigns = $query->fetchAll();
$first_active_campaign = array();
$active_campaigns = [];
$paused_campaigns = [];
$finished_campaigns = [];
$archived_campaigns = [];

$is_first_active = true;
$running_campaign = null;
$active_campaign = null;
foreach ($campaigns as $key => $cm) {

    //if (!isset($first_active_campaign[$cm['visit_by_id']]));
    //	$first_active_campaign[$cm['visit_by_id']] = true;

    // Format dates.
    $unformated_start_date = $cm['start_date'];
    $unformated_end_date = $cm['end_date'];
    if ($cm['start_date'] == "0000-00-00")
        $cm['start_date'] = date('M j, Y', strtotime($cm['created']));
    else
        $cm['start_date'] = date('M j, Y', strtotime($cm['start_date']));

    if ($cm['end_date'] == "0000-00-00")
        $cm['end_date'] = "-";
    else
        $cm['end_date'] = date('M j, Y', strtotime($cm['end_date']));
    if ($cm['campaign_completed'])
        $cm['campaign_completed'] = date('M j, Y', strtotime($cm['campaign_completed']));
    if ($cm['created'])
        $cm['created'] = date('M j, Y', strtotime($cm['created']));

    // Format values
    $cm['keywords'] = format_multi_value($cm['keywords']);
    $cm['companies'] = format_multi_value($cm['companies']);
    $cm['titles'] = format_multi_value($cm['titles']);
    $cm['seniority_levels'] = format_multi_value($cm['seniority_levels'], 2, 30, $newIdToSeniorityLevel);
    $cm['company_sizes'] = format_multi_value($cm['company_sizes'], 2, 30, $idToCompanySize);
    $cm['relationships'] = format_multi_value($cm['relationships'], 2, 25, $idToRelationship, true);
    $cm['functions'] = format_multi_value($cm['functions'], 2, 30, $newIdToFunction);
    $cm['industries'] = format_multi_value($cm['industries'], 2, 30, $idToIndustry);


    $loc = explode("\n", $cm['locations']);
    $query = DB::prep("select * from zipCode where id=?");
    $loc_str = array();
    foreach ($loc as $loc_id) {
        $query->execute(array($loc_id));
        if ($query->rowCount() > 0) {
            $row = $query->fetch();
            $loc_str[] = $row['name'];
        }
    }
    $cm['locations'] = format_multi_value(implode("\n", $loc_str));

    if ($cm['revisit_after'] > 0) {
        $cm['revisit_after'] = sprintf('after %d days', $cm['revisit_after']);
    } else {
        $cm['revisit_after'] = 'Never';
    }

    $cm['ofLabel'] = "";
    /*if($cm['total_visits'] > 0 AND $cm['priority'] > 0){
        $cm['ofLabel'] = " of ".number_format($cm['total_visits']);
        if($cm["missingEstimate"]>0){
            $cm['ofLabel'] .= "+ (still counting)";
        }
    }*/
    $cm['visits'] = number_format($cm['visits']);
    $cm['status_message'] = $cm['status'];

    if (trim($cm['name']) == "") {
        $cm['name'] = "Untitled";
    }

    if ($cm['status'] == 'Active') {
        if ($unformated_start_date > $cm['today']) {
            $cm['status_message'] = "Scheduled";
        }
        if ($unformated_end_date > "0000-00-00" AND $unformated_end_date < $cm['today']) {
            $cm['status_message'] = "Finished";
            $cm['status'] = "Finished";
            $query = DB::prep("UPDATE campaign SET status = 'Finished' WHERE end_date > '0000-00-00' AND end_date <= DATE(NOW())");
            $query->execute(array());
        }
    }

    if ($cm['status'] == "Active" && $cm['publish_status'] == "published") {
        if ($is_first_active) {
            $cm['is_first_active'] = true;
            $is_first_active = false;
        }
        if ($running_campaign === null) {
            $running_campaign = $cm;
        }
        $active_campaigns[$key] = $cm;
    }
    if ($cm['status'] == "Paused" && $cm['publish_status'] != "archived") {
        $paused_campaigns[$key] = $cm;
    }
    if ($cm['status'] == "Finished" && $cm['publish_status'] != "archived") {
        $finished_campaigns[$key] = $cm;
    }
    if ($cm['publish_status'] == "archived") {
        $archived_campaigns[$key] = $cm;
    }

    if (isset($_GET['campaign_id']) && $cm['id'] == $_GET['campaign_id']) {
        $active_campaign = $cm;
    }

}
usort($paused_campaigns,function($a,$b){
     return strcmp($b['sort_date'], $a['sort_date']);
 });
usort($finished_campaigns,function($a,$b){
     return strcmp($b['sort_date'], $a['sort_date']);
 });
usort($archived_campaigns,function($a,$b){
     return strcmp($b['sort_date'], $a['sort_date']);
 });

if ($active_campaign === null && count($active_campaigns) > 0) {
    // Pick the first one.
    $active_campaign = $active_campaigns[array_keys($active_campaigns)[0]];
}

$query = DB::prep("SELECT *	FROM campaign WHERE id = :id");
$query->execute(array(
    'id' => $active_campaign['id'],
));
$raw_active_campaign = $query->fetch();

$settingsModel = new \classes\Settings($_GET['company_id'], $user['id']);


include('header.php');

?>

<? if (!$user['have_sales_navigator'] and !$isSimpleSearch) { ?>
    <div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
        <span style="color: red;"><b>You have to have Sales Navigator in LinkedIn before any visiting can occur.</b> Click <a
                href="https://www.linkedin.com/premium/manage" target="_blank"><u>here</u></a> to manage your LinkedIn subscriptions</span>
    </div>
<? } ?>

<? if (!empty($user['anonymous']) AND ($user['anonymous'] === PrivacyModel::HIDE OR $user['anonymous'] === PrivacyModel::ANON)) { ?>
    <div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
        <span style="color: red;"><b>You have to keep your LinkedIn account in the public mode.</b> Click <a
                href="https://www.linkedin.com/psettings/profile-visibility" target="_blank"><u>here</u></a> to manage your LinkedIn profile viewing options.</span>
    </div>
<? } ?>
<? if (!empty($user['anonymousSales']) AND $user['anonymousSales'] !== PrivacySalesModel::FULL AND $user['anonymousSales'] !== PrivacySalesModel::UNKNOWN) { ?>
    <div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
        <span style="color: red;"><b>You have to keep your LinkedIn Sales Navigator account in the public mode.</b> Click <a
                href="https://www.linkedin.com/sales/settings?trk=nav_user_menu_manage_sales_nav" target="_blank"><u>here</u></a> to manage your LinkedIn Sales Navigator profile viewing options.</span>
    </div>
<? } ?>

<? if (!empty($user['subscriptions']) AND SubsParser::isUserSubsSupported($user) === false) { ?>
    <div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
        <span style="color: red;"><b>Your type of Linkedin account is not supported now.</b> Click <a
                href="https://www.linkedin.com/premium/manage" target="_blank"><u>here</u></a> to manage your LinkedIn subscriptions or run ViewGen again to update information about subscriptions.</span>
    </div>
<? } ?>
<? /*if ($user['is_onboarding']) { ?>
	<div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
	<span style="color: red;"><b>You have to sign in to Sales Navigator and complete their onboarding process.</b> Click <a href="https://www.linkedin.com/sales?trk=d_flagship3_nav" target="_blank"><u>here</u></a> to go to Sales Navigator<br>
	When you have done that, please <a href=/new-ui/campaign_builder.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$user['id']?>&confirm_completed_onboarding=true>click here to verify that you completed the onboarding</a> and to start using this service.</span>
	</div>
<? } */ ?>

<? if ($user['is_onboarding'] and !$isSimpleSearch) { ?>
    <div class="alert alert-danger" style="border-radius: 0; margin-bottom: 0;">
	<span style="color: red;"><b>Sales Navigator setup is incomplete</b>
	1. Click <a href="https://www.linkedin.com/sales?trk=d_flagship3_nav" target="_blank"><u>here</u></a> to go complete Sales Navigator setup.
	2. Once completed click <a
            href=/new-ui/campaign_builder.php?company_id=<?= $_GET['company_id'] ?>&user_id=<?= $user['id'] ?>&confirm_completed_onboarding=true><u>here</u></a> to confirm.</span>
    </div>
<? } ?>
<div class="main-wrapper" id="campaign_builder" data-company-id="<?= $_GET['company_id'] ?>"
     data-user-id="<?= $_GET['user_id'] ?>" data-function="dynamic-dashboard">
    <div class="left-side left-sidebar">
        <div class="row">
            <div class="col-md-12">
                <? include('user_info_box.php'); ?>

                <div class="row campaign-list">
                    <div class="col-md-12 campaign-action-header new-campaign" data-toggle="modal" data-target="#buildCampaignModal" onclick="useDefaultNewCompaignbuilderData = true; $('#mainItcField').prop('checked', false);">
                    Add New Campaign
                    </div>
                    <div class="col-md-12 campaign-list-header">
                        Active / Pending (<?= count($active_campaigns) ?>)
                    </div>
                    <div class="connectedSortable" id="sortable_active_campaigns">
                        <? foreach ($active_campaigns as $campaign) { ?>
                            <div
                                class="col-md-12 campaign-item<?= ($active_campaign && $active_campaign['id'] == $campaign['id']) ? ' active' : '' ?> ui-state-default"
                                data-id="<?= $campaign['id'] ?>">
								<div class="clickable-body">
                                <span class="campaign-name ellipsis"><?= make_short($campaign['name'], 25) ?>
                                    <span data-dynamic="software_status"><i
                                            class="status-icon <?= $user['software_status'] ?>"></i></span>
								</span>
                                <span class="campaign-progress">
									(<span
                                        data-dynamic="campaign_visits_<?= $campaign['id'] ?>"><?= $campaign['campaign_visit_count'] ?></span> visits /
                                        VBR: <?= $campaign['campaign_visit_count'] ? number_format(($campaign['total_visitors']/$campaign['campaign_visit_count'])*100,1) . "%" : "-"?>)
                                </span>
                                <span class="campaign-progress">
                                    Start date: <?=$campaign['start_date']?>
                                    <?php if($campaign['itc']):?>
                                        <i class="fa fa-user-plus" title="ITC is ON, Message: <?=$campaign['itc_message']?>"></i>
                                    <?php endif;?>
                                </span>
                                </div>
                                <span class="move-arrows">
                                    <i class="fa fa-arrow-up ctl__position_up" data-campaign-id="<?= $campaign['id'] ?>"></i>
                                    <i class="fa fa-arrow-down ctl__position_down second" data-campaign-id="<?= $campaign['id'] ?>"></i>
                                </span>
                            </div>
                        <? } ?>
                    </div>
                    <?PHP
                    $aria_expanded = 'false';
                    $collapseClass = 'collapsed';
                    $show_or_hide = '';
                    if (!empty($paused_campaigns) && isset($_GET['campaign_id'])) {
                        foreach ($paused_campaigns as $c) {
                            if ($c['id'] == $_GET['campaign_id']) {
                                $aria_expanded = 'true';
                                $collapseClass = '';
                                $show_or_hide = 'collapse show';
                            }
                        }
                    }
                    ?>
                    <div class="col-md-12 campaign-list-header <?= $collapseClass ?>" data-toggle="collapse"
                         href="#sortableCampaigns5"
                         aria-expanded="<?= $aria_expanded ?>" aria-controls="sortableCampaigns2">
                        Paused (<?= count($paused_campaigns) ?>)
                    </div>
                    <div class="connectedSortable initial-collapsed <?= $show_or_hide ?>" id="sortableCampaigns5">
                        <? foreach ($paused_campaigns as $campaign) { ?>
                            <div
                                class="col-md-12 campaign-item<?= ($active_campaign && $active_campaign['id'] == $campaign['id']) ? ' active' : '' ?> ui-state-default"
                                data-id="<?= $campaign['id'] ?>">
                                <span class="campaign-name ellipsis"><?= $campaign['name'] ?></span>
                                <span class="campaign-progress">(<?= $campaign['campaign_visit_count'] ?> visits /
                                    VBR: <?= $campaign['campaign_visit_count'] ? number_format(($campaign['total_visitors']/$campaign['campaign_visit_count'])*100,1) . "%" : "-"?>)</span>
                                <span class="campaign-progress">
                                    Start date: <?=$campaign['start_date']?>
                                    <?php if($campaign['itc']):?>
                                        <i class="fa fa-user-plus" title="ITC is ON, Message: <?=$campaign['itc_message']?>"></i>
                                    <?php endif;?>
                                </span>
                            </div>
                        <? } ?>
                    </div>
                    <?PHP
                    $aria_expanded = 'false';
                    $collapseClass = 'collapsed';
                    $show_or_hide = '';
                    if (!empty($finished_campaigns) && isset($_GET['campaign_id'])) {
                        foreach ($finished_campaigns as $c) {
                            if ($c['id'] == $_GET['campaign_id']) {
                                $aria_expanded = 'true';
                                $collapseClass = '';
                                $show_or_hide = 'collapse show';
                            }
                        }
                    }
                    ?>
                    <div class="col-md-12 campaign-list-header <?= $collapseClass ?>" data-toggle="collapse"
                         href="#sortableCampaigns2"
                         aria-expanded="<?= $aria_expanded ?>" aria-controls="sortableCampaigns2">
                        Finished (<?= count($finished_campaigns) ?>)
                    </div>
                    <div class="connectedSortable initial-collapsed <?= $show_or_hide ?>" id="sortableCampaigns2">
                        <? foreach ($finished_campaigns as $campaign) { ?>
                            <div
                                class="col-md-12 campaign-item<?= ($active_campaign && $active_campaign['id'] == $campaign['id']) ? ' active' : '' ?> ui-state-default"
                                data-id="<?= $campaign['id'] ?>">
                                <span class="campaign-name ellipsis"><?= $campaign['name'] ?></span>
                                <span class="campaign-progress">(<?= $campaign['campaign_visit_count'] ?> visits /
                                    VBR: <?= $campaign['campaign_visit_count'] ? number_format(($campaign['total_visitors']/$campaign['campaign_visit_count'])*100,1) . "%" : "-"?>)</span>
                                 <span class="campaign-progress">
                                    Start date: <?=$campaign['start_date']?>
                                     <?php if($campaign['itc']):?>
                                         <i class="fa fa-user-plus" title="ITC is ON, Message: <?=$campaign['itc_message']?>"></i>
                                     <?php endif;?>
                                </span>
                                <span class="campaign-rerun"><a href="/new-ui/copy_campaign.php?campaign_id=<?=$campaign['id']?>&company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>">Re-run</a></span>
                            </div>
                        <? } ?>
                    </div>
                    <?PHP
                    $aria_expanded = 'false';
                    $collapseClass = 'collapsed';
                    $show_or_hide = '';
                    if (!empty($archived_campaigns) && isset($_GET['campaign_id'])) {
                        foreach ($archived_campaigns as $c) {
                            if ($c['id'] == $_GET['campaign_id']) {
                                $aria_expanded = 'true';
                                $collapseClass = '';
                                $show_or_hide = 'collapse show';
                            }
                        }
                    }
                    ?>
                    <div class="col-md-12 campaign-list-header <?= $collapseClass ?>" data-toggle="collapse"
                         href="#sortableCampaigns3"
                         aria-expanded="<?= $aria_expanded ?>" aria-controls="sortableCampaigns3">
                        Archived (<?= count($archived_campaigns) ?>)
                    </div>
                    <div class="connectedSortable initial-collapsed <?= $show_or_hide ?>" id="sortableCampaigns3">
                        <? foreach ($archived_campaigns as $campaign) { ?>
                            <div
                                class="col-md-12 campaign-item<?= ($active_campaign && $active_campaign['id'] == $campaign['id']) ? ' active' : '' ?> ui-state-default"
                                data-id="<?= $campaign['id'] ?>">
                                <span class="campaign-name ellipsis"><?= $campaign['name'] ?></span>
                                <span class="campaign-progress">(<?= $campaign['campaign_visit_count'] ?> visits /
                                    VBR: <?= $campaign['campaign_visit_count'] ? number_format(($campaign['total_visitors']/$campaign['campaign_visit_count'])*100,1) . "%" : "-"?>)</span>
                                <span class="campaign-progress">
                                    Start date: <?=$campaign['start_date']?>
                                    <?php if($campaign['itc']):?>
                                        <i class="fa fa-user-plus" title="ITC is ON, Message: <?=$campaign['itc_message']?>"></i>
                                    <?php endif;?>
                                </span>
                                <span class="campaign-rerun"><a href="/new-ui/copy_campaign.php?campaign_id=<?=$campaign['id']?>&company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>">Re-run</a></span>
                            </div>
                        <? } ?>
                    </div>
                    <?php if($settingsModel->getForUser('itc-enabled')):?>
                        <div class="col-md-12 campaign-action-header new-campaign" data-toggle="modal" data-target="#buildCampaignModal" onclick="useDefaultNewCompaignbuilderData = true; $('#mainItcField').prop('checked', true);">
                            Add New Invite Campaign
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>

    <div class="right-side">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 chart-headers">
                        <? /*
						<div class="time-pickers">
							<div class="time-pickers-specify">SPECIFY TIME RANGE:</div>
							<a href="#">24 HOURS</a>
							<a href="#">7 DAYS</a>
							<a href="#" class="active">30 DAYS</a>
							<a href="#">90 DAYS</a>
							<a href="#">1 YEAR</a>
							<a href="#">ALL TIME</a>
						</div>
						<div class="time-chart">
							<div id="time_chart"></div>
						</div>
						*/ ?>
                        <div class="campaign-info-header">
                            <a href="/new-ui/campaign_builder.php?company_id=<?= $_GET['company_id'] ?>&user_id=<?= $_GET['user_id'] ?>&campaign_id=<?= $active_campaign['id'] ?>"
                                <?= ($_GET['page'] ?? 'info') == 'info' ? 'class="active"' : '' ?>>Campaign Details</a>
                            <a href="/new-ui/campaign_builder.php?company_id=<?= $_GET['company_id'] ?>&user_id=<?= $_GET['user_id'] ?>&campaign_id=<?= $active_campaign['id'] ?>&page=outbound"
                                <?= ($_GET['page'] ?? 'info') == 'outbound' ? 'class="active"' : '' ?>>Visited
                                Profiles</a>
                            <a href="/new-ui/campaign_builder.php?company_id=<?= $_GET['company_id'] ?>&user_id=<?= $_GET['user_id'] ?>&campaign_id=<?= $active_campaign['id'] ?>&page=inbound"
                                <?= ($_GET['page'] ?? 'info') == 'inbound' ? 'class="active"' : '' ?>>Return Profile
                                Views</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 campaign-info">
                        <?php
						if ($user["hidden"]) {?>
							This user is hidden for the customer. Click <a href=campaign_builder.php?company_id=<?=$_GET["company_id"]?>&user_id=<?=$_GET["user_id"]?>&unhide=true>here</a> to make him/her visible.
                        <? } else if (isset($active_campaign)) {
                            if (($_GET['page'] ?? '') == 'outbound') {
                                include('campaign_outbound.php');
                            } elseif (($_GET['page'] ?? '') == 'inbound') {
                                include('campaign_inbound.php');
                            } else {
                                include('campaign_info.php');
                            }
                        } else if (count($finished_campaigns) + count($archived_campaigns) + count($paused_campaigns) > 0) { ?>
                            <h1 style="font-size: 22px; margin-top: 8px;"> <- Create a new campaign</h1>
                            <? if (count($finished_campaigns) > 0) { ?>
                                <br><br><br>
                                <h1 style="font-size: 22px; margin-top: 8px;"> <- Or take a look at your Finished
                                    campaigns</h1>
                            <? } ?>
                        <? } else { ?>
                            <h1 style="font-size: 22px; margin-top: 8px;"> <- Create your first campaign</h1>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? include('modal_campaign.php'); //should be before newDynamicDashboard.js inclusion ?>

<? include('footer.php'); ?>
<script>
    var useDefaultNewCompaignbuilderData = false;
    var defaultCompaignbuilderData = {
        geography: [
            <?php
                $query = DB::prep("SELECT * FROM li_location WHERE linked_in_id in ('".implode("', '", property_list_to_array($raw_active_campaign['locations']))."')");
                $query->execute();
                $locations = $query->fetchAll();

                foreach ($locations as $location){
                    echo "{n: '".$location['location_name']."', id : '".$location['linked_in_id']."'}, ";
                }
            ?>
        ],
        relation: <?=json_encode(property_list_to_array($raw_active_campaign['relationships']))?>,
        titles: <?=json_encode(property_list_to_array($raw_active_campaign['titles']))?>,
        skipTitles: <?=json_encode(property_list_to_array($raw_active_campaign['skip_titles']))?>,
        keywords: <?=json_encode(property_list_to_array($raw_active_campaign['keywords']))?>,
        skipKeywords: <?=json_encode(property_list_to_array($raw_active_campaign['skip_keywords']))?>,
        industry_location: <?=json_encode(property_list_to_array($raw_active_campaign['industries']))?>,
        companies: <?=json_encode(property_list_to_array($raw_active_campaign['companies']))?>,
        companySize: <?=json_encode(property_list_to_array($raw_active_campaign['company_sizes']))?>,
        'function': <?=json_encode(property_list_to_array($raw_active_campaign['functions']))?>,
        seniorityLevel: <?=json_encode(property_list_to_array($raw_active_campaign['seniority_levels']))?>,
        yearsInCurrentPosition: <?=json_encode(property_list_to_array($raw_active_campaign['years_in_current_position']))?>,
        yearsAtCurrentCompany: <?=json_encode(property_list_to_array($raw_active_campaign['years_at_current_company']))?>,
        yearsOfExperience: <?=json_encode(property_list_to_array($raw_active_campaign['years_of_experience']))?>,
        groups: <?=json_encode(property_list_to_array($raw_active_campaign['groups']))?>,
        firstName: <?=json_encode(property_list_to_array($raw_active_campaign['search_first_name']))?>,
        lastName: <?=json_encode(property_list_to_array($raw_active_campaign['search_last_name']))?>,
        postalCodeWithin: '<?=$raw_active_campaign['postal_code_within']?>',
        postalCodeCountry: '<?=$raw_active_campaign['postal_code_country']?>',
        postalCode: '<?=$raw_active_campaign['postal_code']?>',
        itc: '<?=$raw_active_campaign['itc']?>',
        itc_message: <?=json_encode($raw_active_campaign['itc_message'])?>
    }
</script>

<script src="/assets/js/newDynamicDashboard.js" type="application/javascript"></script>

