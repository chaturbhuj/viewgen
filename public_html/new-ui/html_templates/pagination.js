var currently_paginating = false;

$( document ).ready( function(){
	$( window ).scroll( function(){
		check_for_pagination();
	});
	if( location.hash.replace( '#', '' ) > 0 ){
		perform_paginate( location.hash.replace( '#', '' ), 'true' );	
	}
});

var check_for_pagination = function(){
	
	if( ! currently_paginating ){
		var pagination_top = $( 'tr#ajax_pagination_row' ).offset().top;
		var scroll_top = $( window ).scrollTop();
		var body_height = $( 'body' ).height();
		var paginate_position = pagination_top - 600 - $( window ).height();
		if( scroll_top >= paginate_position ){
			currently_paginating = true;
			var curLimit = location.hash;
			curLimit = curLimit.replace( '#', '' );
			if( ! ( curLimit > 0 ) )
				curLimit = 100;
			if( $( 'tr#ajax_pagination_row' ).attr( 'max-results' ) >= parseFloat( curLimit ) )
				perform_paginate( curLimit, 'false' );
		}
	}
	
}

var perform_paginate = function( curLimit, loading ){

	var ajax_params = { 'cur_limit':curLimit, 'campaign_id':$( 'tr#ajax_pagination_row' ).attr( 'campaign-id' ), 'loading':loading };
	var client = new XMLHttpRequest();
	client.onreadystatechange = function(){
		if( client.readyState == 4 && client.status == 200 ){
			var response = JSON.parse( client.response );
			$( 'tr#ajax_pagination_row' ).before( response.output );
			location.hash = response.new_limit;
			currently_paginating = false;
		}
	};
	client.open( 'POST', 'http://viewgentools.com/new-ui/api/campaign_outbound_pagination.php' );
	client.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
	client.send( 'params='+JSON.stringify( ajax_params ) );
	
}