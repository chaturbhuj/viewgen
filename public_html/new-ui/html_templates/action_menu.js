$( document ).ready( function(){
	
	$( 'body' ).on( 'click', 'div.action-menu', function(){
		
		var menu = $( this );
		var list = $( this ).children( 'ul' );
		if( menu.children( 'ul:visible' ).length > 0 && menu.children( 'ul:hover' ).length > 0 )
			list.slideToggle();
		else if( ! ( menu.children( 'ul:visible' ) > 0 ) )
			list.slideToggle();
		
	});
	
	$( 'body' ).on( 'click', function(){
		
		if( ! ( $( 'div.action-menu:hover' ) ) )
			$( 'div.action-menu ul' ).slideToggle();
			
	});
	
});