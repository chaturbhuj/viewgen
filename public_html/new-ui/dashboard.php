<?php
use classes\Settings;

$HTML_TITLE = "Dashboard";
$ACTIVE_HEADER_PAGE = "dashboard";

require_once('../authenticate.php');
require_once(__DIR__.'/../classes/Settings.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

if (!isset($_GET['user_id']) || $_GET['user_id'] == '') {
	// Redirect to the first user.
	$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0 LIMIT 1");
	$query->execute([
		'company_id' => $_GET['company_id']
	]);

	if ($query->rowCount() == 0) {
		redirect('/new-ui/no_user.php?company_id=' . $_GET['company_id']);
		die;
	}

	$user = $query->fetch();

	redirect("/new-ui/dashboard.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
	die;
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND id = :user_id");
$query->execute([
	'company_id' => $_GET['company_id'],
	'user_id' => $_GET['user_id']
]);

if ($query->rowCount() == 0) {
	echo "Invalid user";
	die;
}

$user = $query->fetch();

$q = DB::prep("SELECT sum(campaign_visit_count) as campaign_visit_count, sum(total_visitors) as total_visitors FROM campaign WHERE visit_by_id = :linked_in_id and company_id = :company_id");
$q->execute([
	'linked_in_id' => $user['linked_in_id'],
    'company_id' => $user['company_id']
]);
$userVisits = $q->fetch();

$q = DB::prep("SELECT sum(number_of_invites) as invites_count FROM reports_cache WHERE li_users_id = :linked_in_id and company_id = :company_id");
$q->execute([
    'linked_in_id' => $user['linked_in_id'],
    'company_id' => $user['company_id']
]);
$userInvites = $q->fetch();

$settingsModel = new Settings($user['company_id'], $user['id']);


include('header.php');
?>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<div class="main-wrapper" id="campaign_builder" data-company-id="<?=$_GET['company_id']?>" data-file="dashboard.php">
	<div class="left-side left-sidebar">
		<div class="row">
			<div class="col-md-12">

				<? include('user_info_box.php'); ?>

				<div class="row user-stats">
					<div class="col-md-9 offset-md-1 user-stats-title text-center">
						OUTBOUND ACTIVITY
					</div>

					<div class="col-md-12 left-stats text-center">
						Total Profile Visits
					</div>
					<div class="col-md-12 user-stats-row">
						<div class="left-stats"><?=number_format($user['total_visited'])?></div>
					</div>
					<div class="col-md-9 offset-md-1 user-stats-title text-center">
						INBOUND ACTIVITY
					</div>

					<div class="col-md-12 left-stats text-center">
						Total Return Profile Views
					</div>
					<div class="col-md-12 user-stats-row">
						<div class="left-stats"><?=number_format($userVisits['total_visitors'])?></div>
					</div>
					<div class="col-md-9 offset-md-1 user-stats-title text-center">
						VBR
					</div>

					<div class="col-md-12 left-stats text-center">
						Total View Back Rate
					</div>
					<div class="col-md-12 user-stats-row">
						<div class="left-stats"><?=number_format($userVisits['total_visitors'] / $user['total_visited'] * 100, 1)?>%</div>
					</div>

                    <?php if($settingsModel->getForUser('itc-enabled')):?>
                        <div class="col-md-9 offset-md-1 user-stats-title text-center">
                            INVITES
                        </div>

                        <div class="col-md-12 left-stats text-center">
                            Total sent invites by ViewGen
                        </div>
                        <div class="col-md-12 user-stats-row">
                            <div class="left-stats"><?=number_format($userInvites['invites_count'])?></div>
                        </div>
                    <?php endif;?>
				</div>
				<div class="row user-stats">
				</div>
			</div>
		</div>
	</div>
	<div class="right-side">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 chart-headers">
						<div class="time-pickers">
							<div class="time-pickers-specify">SPECIFY TIME RANGE:</div>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24?>"<?=($_GET['time_range'] ?? 0) == 3600*24 ? ' class="active"' : ''?>>24 HOURS</a>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24*7?>"<?=($_GET['time_range'] ?? 0) == 3600*24*7 ? ' class="active"' : ''?>>7 DAYS</a>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24*30?>"<?=($_GET['time_range'] ?? 3600*24*30) == 3600*24*30 ? ' class="active"' : ''?>>30 DAYS</a>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24*90?>"<?=($_GET['time_range'] ?? 0) == 3600*24*90 ? ' class="active"' : ''?>>90 DAYS</a>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24*365?>"<?=($_GET['time_range'] ?? 0) == 3600*24*365 ? ' class="active"' : ''?>>1 YEAR</a>
							<a href="/new-ui/dashboard.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id']?>&time_range=<?=3600*24*500?>"<?=($_GET['time_range'] ?? 0) == 3600*24*500 ? ' class="active"' : ''?>>ALL TIME</a>
						</div>
					</div>
				</div>

				<div class="charts">

					<? include('graphs/outbound_profile_views.php')?>
					<? include('graphs/inbound_profile_views.php')?>
                    <? include('graphs/inbound_connections.php')?>

                    <?php if($settingsModel->getForUser('itc-enabled')):?>
                        <? include('graphs/outbound_invites.php')?>
                    <?php endif;?>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- HTML -->

<?php
include('footer.php');
?>

<script type="application/javascript">
    //TODO this is the copy of some functionality from newDynamicDasboard.js
    function handleChangeUser (event) {
        var $select = $(event.currentTarget);
        var file = "campaign_builder.php";
        if (this.data('file')) {
            file = this.data('file');
        }
        document.location.href = "/new-ui/" + file + "?company_id=" + this.data("company-id") + "&user_id=" + $select.val();
    }

    $('#change_user').bind('change', handleChangeUser.bind($('#campaign_builder')));
</script>