<?php
require_once(__DIR__ . '/../classes/EmailTemplate.php');
require_once(__DIR__ . '/../classes/Referrer.php');

$HTML_TITLE = "Add users";
$ACTIVE_HEADER_PAGE = "add_users";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
$query->execute([
    'company_id' => $_GET['company_id']
]);
$company = $query->fetch();

$Referrer = new Referrer();

$inviteMessage = '';
$query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateName = :emailTemplateName");
$query->execute([
    'emailTemplateName' => 'Add users'
]);
$emailTemplate = $query->fetch();
$inviteMessage = $emailTemplate['emailTemplateBody'];



if (isset($_POST["sendInvites"])) {

    foreach ($_POST['sendInviteEmail'] as $email) {
        $email = trim($email);
        if ($email == "") continue;

        $insert = DB::prep("INSERT IGNORE INTO sent_invite (company_id, email, sent_at)
			VALUES(:company_id, :email, NOW()) ON DUPLICATE KEY UPDATE sent_at = NOW()");
        $insert->execute([
            'company_id' => $company['id'],
            'email' => $email
        ]);

        $emailTemplate = new EmailTemplate($company['id']);
        $variables = [
            'company-id' => $company['id'],
            'company-name' => $company['company_name'],
            'company-first-name' => $company['first_name'],
            'company-last-name' => $company['last_name'],
            'company-email' => $company['email']
        ];
        $emailTemplate->sendEmail($email, "Add users", $variables);

    }

    redirect('/new-ui/add_users.php?company_id='.$_GET['company_id']);
    die;
}

if (isset($_POST["sendReferral"])) {

    $email = trim($_POST['email']);
    $firstName = trim($_POST['first_name']);
    $lastName = trim($_POST['last_name']);
    $message = $Referrer->getMessageTemplate();//trim($_POST['message']);

    $res = $Referrer->create($company['id'], $email, $firstName, $lastName, $message);

    if($res === true){
        redirect('/new-ui/add_users.php?company_id='.$_GET['company_id']);
        die;
    }

    $errors = $res;
}

$query = DB::prep("SELECT * FROM sent_invite WHERE company_id = :company_id");
$query->execute([
    'company_id' => $_GET['company_id']
]);
$sentInvites = $query->fetchAll();




include('header.php');
?>
<style>
    .new_container {
        padding-top: 50px;
    }
    .container {
        max-width: 1170px;
        margin: 0 auto;
        padding: 0 15px;
    }
    .forms-block {
        padding: 50px;
        display: flex;
        background-color: #fff;
    }
    .separator {
        padding: 200px 40px 0;
        font-weight: bold;
    }
    .invite-form__wr, .refer-form__wr {
        border: 1px solid #ddd;
        flex: 1 1;
    }
    .header-form {
        position: relative;
        padding: 7px 5px 7px 55px;
        font-size: 26px;
        font-weight: 700;
        border-bottom: 1px solid #ddd;
    }
    .refer-form__wr .header-form:before {
        background-image: url("data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9JzIwMCcgd2lkdGg9JzIwMCcgIGZpbGw9IiMwMDAwMDAiIHhtbG5zOng9Imh0dHA6Ly9ucy5hZG9iZS5jb20vRXh0ZW5zaWJpbGl0eS8xLjAvIiB4bWxuczppPSJodHRwOi8vbnMuYWRvYmUuY29tL0Fkb2JlSWxsdXN0cmF0b3IvMTAuMC8iIHhtbG5zOmdyYXBoPSJodHRwOi8vbnMuYWRvYmUuY29tL0dyYXBocy8xLjAvIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSItMjA1IDIwNyAxMDAgMTAwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IC0yMDUgMjA3IDEwMCAxMDA7IiB4bWw6c3BhY2U9InByZXNlcnZlIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHJlcXVpcmVkRXh0ZW5zaW9ucz0iaHR0cDovL25zLmFkb2JlLmNvbS9BZG9iZUlsbHVzdHJhdG9yLzEwLjAvIiB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIj48L2ZvcmVpZ25PYmplY3Q+PGcgaTpleHRyYW5lb3VzPSJzZWxmIj48cGF0aCBkPSJNLTE1NSwyNTdDLTE1NSwyNTctMTU1LDI1Ny0xNTUsMjU3Qy0xNTUsMjU3LTE1NSwyNTctMTU1LDI1N3oiPjwvcGF0aD48cGF0aCBkPSJNLTE1NSwyNTdDLTE1NSwyNTctMTU1LDI1Ny0xNTUsMjU3Qy0xNTUsMjU3LTE1NSwyNTctMTU1LDI1N3oiPjwvcGF0aD48cGF0aCBkPSJNLTE2Mi4zLDIyMGMwLjMtNy40LDYuNS03LjMsNy4xLTcuM2MwLjYsMCw2LjgtMC4xLDcuMSw3LjNjMCwwLDEuMiwxMS4xLTcuMSwxMS4xdjBjMCwwLDAsMCwwLDBjMCwwLDAsMCwwLDB2MCAgICBDLTE2My41LDIzMS0xNjIuMywyMjAtMTYyLjMsMjIweiBNLTE3MC4zLDI0NC4yYzAtMC41LDAtMSwwLTEuNXYtMC4yYzAsMCwwLDAsMCwwYzAuMS0yLjQsMC41LTQsMy01LjZjMy4yLTIsNi44LTMuOCw2LjgtMy44ICAgIGwyLjksOS4xbDEuNy00LjhjLTIuOS00LjEsMC4yLTQuMywwLjgtNC4zdjBjMCwwLDAsMCwwLDBjMCwwLDAsMCwwLDB2MGMwLjYsMCwzLjcsMC4yLDAuOCw0LjNsMS43LDQuOGwyLjktOS4xICAgIGMwLDAsMy42LDEuOCw2LjgsMy44YzIuNCwxLjUsMi45LDMuMiwzLDUuNmMwLDAsMCwwLDAsMHYwLjJjMCwwLjUsMCwwLjksMCwxLjVjMCwwLDAtMC4xLDAtMC4ydjYuNmgtMTUuMmgtMTUuMVYyNDQgICAgQy0xNzAuMywyNDQuMS0xNzAuMywyNDQuMi0xNzAuMywyNDQuMnogTS0xNDAsMjQyLjVDLTE0MCwyNDIuNS0xNDAsMjQyLjUtMTQwLDI0Mi41Qy0xNDAsMjQyLjUtMTQwLDI0Mi41LTE0MCwyNDIuNXogICAgIE0tMTcwLjMsMjQyLjVDLTE3MC4zLDI0Mi41LTE3MC4zLDI0Mi41LTE3MC4zLDI0Mi41Qy0xNzAuMywyNDIuNS0xNzAuMywyNDIuNS0xNzAuMywyNDIuNXogTS0xMjQuNSwyODEuOUwtMTI0LjUsMjgxLjkgICAgQy0xMjQuNSwyODEuOS0xMjQuNCwyODEuOS0xMjQuNSwyODEuOUMtMTI0LjQsMjgxLjktMTI0LjQsMjgxLjktMTI0LjUsMjgxLjlMLTEyNC41LDI4MS45YzguNC0wLjEsNy4yLTExLjEsNy4yLTExLjEgICAgYy0wLjMtNy40LTYuNS03LjMtNy4xLTcuM2MtMC42LDAtNi44LTAuMS03LjEsNy4zQy0xMzEuNiwyNzAuNy0xMzIuOCwyODEuOC0xMjQuNSwyODEuOXogTS0xMzkuNiwyOTQuOWMwLTAuNSwwLTEsMC0xLjV2LTAuMiAgICBjMCwwLDAsMCwwLDBjMC4xLTIuNCwwLjUtNCwzLTUuNmMzLjItMiw2LjgtMy44LDYuOC0zLjhsMi45LDkuMWwxLjctNC44Yy0yLjktNC4xLDAuMi00LjMsMC44LTQuM3YwYzAsMCwwLDAsMCwwYzAsMCwwLDAsMCwwdjAgICAgYzAuNiwwLDMuNywwLjIsMC44LDQuM2wxLjcsNC44bDIuOS05LjFjMCwwLDMuNiwxLjgsNi44LDMuOGMyLjQsMS41LDIuOSwzLjIsMyw1LjZjMCwwLDAsMCwwLDB2MC4yYzAsMC41LDAsMC45LDAsMS41ICAgIGMwLDAsMC0wLjEsMC0wLjJ2Ni42aC0xNS4yaC0xNS4xdi02LjZDLTEzOS42LDI5NC45LTEzOS42LDI5NC45LTEzOS42LDI5NC45eiBNLTEzOS42LDI5My4yICAgIEMtMTM5LjYsMjkzLjItMTM5LjYsMjkzLjItMTM5LjYsMjkzLjJDLTEzOS42LDI5My4yLTEzOS42LDI5My4yLTEzOS42LDI5My4yeiBNLTE4NS42LDI4MS45TC0xODUuNiwyODEuOSAgICBDLTE4NS42LDI4MS45LTE4NS42LDI4MS45LTE4NS42LDI4MS45Qy0xODUuNSwyODEuOS0xODUuNSwyODEuOS0xODUuNiwyODEuOUwtMTg1LjYsMjgxLjljOC40LTAuMSw3LjItMTEuMSw3LjItMTEuMSAgICBjLTAuMy03LjQtNi41LTcuMy03LjEtNy4zYy0wLjYsMC02LjgtMC4xLTcuMSw3LjNDLTE5Mi43LDI3MC43LTE5My45LDI4MS44LTE4NS42LDI4MS45eiBNLTIwMC43LDI5NC45YzAtMC41LDAtMSwwLTEuNXYtMC4yICAgIGMwLDAsMCwwLDAsMGMwLjEtMi40LDAuNS00LDMtNS42YzMuMi0yLDYuOC0zLjgsNi44LTMuOGwyLjksOS4xbDEuNy00LjhjLTIuOS00LjEsMC4yLTQuMywwLjgtNC4zdjBjMCwwLDAsMCwwLDBjMCwwLDAsMCwwLDB2MCAgICBjMC42LDAsMy43LDAuMiwwLjgsNC4zbDEuNyw0LjhsMi45LTkuMWMwLDAsMy42LDEuOCw2LjgsMy44YzIuNCwxLjUsMi45LDMuMiwzLDUuNmMwLDAsMCwwLDAsMHYwLjJjMCwwLjUsMCwwLjksMCwxLjUgICAgYzAsMCwwLTAuMSwwLTAuMnY2LjZoLTE1LjJoLTE1LjF2LTYuNkMtMjAwLjcsMjk0LjktMjAwLjcsMjk0LjktMjAwLjcsMjk0Ljl6IE0tMTcwLjQsMjkzLjIgICAgQy0xNzAuNCwyOTMuMi0xNzAuNCwyOTMuMi0xNzAuNCwyOTMuMkMtMTcwLjQsMjkzLjItMTcwLjQsMjkzLjItMTcwLjQsMjkzLjJ6IE0tMTU1LjEsMjU3LjJjLTguNSwwLTE1LjQsNi45LTE1LjQsMTUuNCAgICBzNi45LDE1LjQsMTUuNCwxNS40YzguNSwwLDE1LjQtNi45LDE1LjQtMTUuNFMtMTQ2LjYsMjU3LjItMTU1LjEsMjU3LjJ6IE0tMTUwLjcsMjc4LjdjLTAuNywwLjgtMS42LDEuNC0yLjYsMS43ICAgIGMtMC40LDAuMS0wLjYsMC40LTAuNiwwLjhjMCwwLjUsMCwwLjksMCwxLjRjMCwwLjQtMC4yLDAuNi0wLjYsMC42Yy0wLjUsMC0xLDAtMS41LDBjLTAuNCwwLTAuNi0wLjMtMC42LTAuN2MwLTAuMywwLTAuNywwLTEgICAgYzAtMC43LDAtMC44LTAuNy0wLjljLTAuOS0wLjEtMS44LTAuMy0yLjYtMC44Yy0wLjctMC4zLTAuNy0wLjUtMC41LTEuMmMwLjEtMC41LDAuMy0xLDAuNC0xLjVjMC4yLTAuNiwwLjMtMC43LDAuOS0wLjQgICAgYzAuOSwwLjUsMS45LDAuNywyLjksMC45YzAuNywwLjEsMS4zLDAsMS45LTAuM2MxLjEtMC41LDEuMy0xLjgsMC40LTIuNmMtMC4zLTAuMy0wLjctMC41LTEuMS0wLjZjLTEtMC40LTItMC44LTMtMS4zICAgIGMtMS41LTAuOS0yLjUtMi4yLTIuNC00YzAuMS0yLjEsMS4zLTMuNCwzLjItNC4xYzAuOC0wLjMsMC44LTAuMywwLjgtMS4xYzAtMC4zLDAtMC42LDAtMC44YzAtMC42LDAuMS0wLjcsMC43LTAuOCAgICBjMC4yLDAsMC40LDAsMC42LDBjMS4zLDAsMS4zLDAsMS4zLDEuM2MwLDAuOSwwLDAuOSwwLjksMS4xYzAuNywwLjEsMS40LDAuMywyLjEsMC42YzAuNCwwLjIsMC41LDAuNCwwLjQsMC44ICAgIGMtMC4yLDAuNi0wLjMsMS4xLTAuNSwxLjdjLTAuMiwwLjUtMC4zLDAuNi0wLjksMC40Yy0xLTAuNS0yLjEtMC43LTMuMy0wLjZjLTAuMywwLTAuNiwwLjEtMC45LDAuMmMtMSwwLjQtMS4xLDEuNS0wLjMsMi4yICAgIGMwLjQsMC4zLDAuOSwwLjYsMS40LDAuOGMwLjksMC40LDEuOCwwLjcsMi42LDEuMkMtMTQ5LjUsMjczLjEtMTQ4LjgsMjc2LjUtMTUwLjcsMjc4Ljd6Ij48L3BhdGg+PC9nPjwvc3dpdGNoPjwvc3ZnPg==");
    }
    .invite-form__wr .header-form:before {
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIoSURBVGiB7dk9axRRFIfxX6JBoqISSBcIBAUhoIVNKgUtUqTUJl0a8xW20zZfQdJZWKSKWMTGXnwBIZWgoGAhiAgqaJBkLOZMnA27477MZkK4Dwzc3Tnn3ufP7rB3ZkkkEolEA4wha1qiDsabFqiLk6XxWGMWw5FxjD6RcpDLjVkMTptzFscvrDRhMyArcufCf39QHA8w0ZBcL0zIHQ967w/uYzfGW5hqRLOaKTyVO+7ing5B4AY+x+tPWDhs0woW5E6Z3PF6vN8xCMzgebz3G3cPy7SCVblLJnebKZ3rGgROYb107iEmRyzbiclYu/BYD7cylUEKVrET519hdgSy3ZjF61h7J1w60VMQuIaPUfMFN+syreBWrJXhQzh0o+cgMI1nUfcHrSFFq2jFGlmsOf2f+r6CkO/L1kr1j3B6QNlOnIk5i/nXtO8Fu9F3kIJl/IyeN5jro7cbczFXFnMv99E7cBC4ivfR9xWLffaXWYw5MrzDlT77hwpC/iu7Fb178q9CP7vp8ejZizk2cW4Aj6GDkN/DtPzb2jzB+R76LkRtsdVoGfx+qJYgBUv4FvO8xXxF7XzUZNGzNOTatQaBS9iOub7jdoeaO/gRNdvRMyy1B4Gz2NB+3ZyIo3w9bERtHYwkCPlFXL4l2MRj7VvvOp8PjCxIQfmW4ODWu05GHgQu4iVexHgUZMjKD+jS46CjQApy1EhBjhopyFHj2ARJ/1glEolEIoG/iYDLHtgfQKwAAAAASUVORK5CYII=')
    }
    .header-form::before {
        content: '';
        position: absolute;
        left: 13px;
        top: 13px;
        width: 35px;
        height: 28px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: contain;
    }
    .info-content {
        display: none;
    }
    .forms-block input, .forms-block textarea {
        display: block;
        width: 100%;
        padding: 7px 10px;
        margin-bottom: 15px;
        border: 1px solid #ddd;
    }
    row {
        margin-left: -15px;
        margin-right: -15px;
    }
    .col2, .col1 {
        padding: 0 15px;
    }
    .col1 {
        width:100%;
    }
    .col2 {
        float: left;
        width: 50%;
        box-sizing: border-box;
    }
    .note-wr {
        position: relative;
        padding-right: 150px;
        padding-bottom: 40px;
        font-size: 12px;
    }
    .btn-submit {
        position: absolute;
        top: 5px;
        right: 0;
        padding: 10px 20px;
        background-color: #1f88c0;
        color: #fff;
        border: none;
        cursor: pointer;
    }
    .forms-block label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 14px;
    }
    .forms-block form {
        margin: 0;
        padding: 20px 15px;
    }
    .forms-block textarea {
        min-height: 100px;
    }

    .info-ic {
        position: relative;
    }
    .info-ic:before {
        content: '?';
        display: inline-block;
        margin-left: 10px;
        position: relative;
        top: -2px;
        width: 16px;
        height: 16px;
        border-radius: 50%;
        background-color: #1f88c0;
        font-size: 11px;
        text-align: center;
        line-height: 16px;
        cursor: pointer;
        color: #fff;
        font-weight: bold;
    }
    .info-ic:hover .info-content  {
        position: absolute;
        display: block;
    }
    .info-content {
        position: absolute;
        width: 200px;
        padding: 10px;
        left: -92px;
        top: 42px;
        font-size: 14px;
        font-weight: 500;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    .info-content:before, .info-content:after  {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #ccc;
        top: -10px;
        left: 50%;
        margin-left: -10px;
    }
    .info-content:after  {
        border-bottom: 10px solid #fff;
        top: -9px
    }

    p.error {
        color: red;
        text-align: center;
        font-weight: bold;
        font-size: 15px;
    }
</style>

<div class="new_container">
    <div class="container">
        <div class="forms-block">
            <div class="invite-form__wr">
                <header class="header-form">
                    Invite a new team member
                    <span class="info-ic">
                        <span class="info-content">
                            Send an internal invitation to other people at your company
                        </span>
                    </span>
                </header>
                <form action="" method="post">
                    <div class="row">
                        <div class="col2">
                            <label for="firstName">First Name</label>
                            <input type="text" id="firstName">
                        </div>
                        <div class="col2">
                            <label for="lastName">Last Name</label>
                            <input type="text" id="lastName">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col1">
                            <label for="email">Email Address</label>
                            <input type="email" id="email" name="sendInviteEmail[0]">
                        </div>
                    </div>
<!--                    <div class="row">-->
<!--                        <div class="col1">-->
<!--                            <label for="invite">Invite Message</label>-->
<!--                            <textarea type="text" id="invite" disabled>--><?//=$inviteMessage?><!--</textarea>-->
<!--                        </div>-->
<!--                    </div>-->
                    <p class="error"></p>
                    <div class="row">
                        <div class="col1">
                            <div class="note-wr">
                                <p>NOTE: This invitation is for organizational purposes. We recommend sending this invitation to people within your company or organization.</p>
                                <button type="submit" class="btn-submit" name="sendInvites">Send Invite</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="separator">
                OR
            </div>
            <div class="refer-form__wr">
                <header class="header-form">
                    Refer a friend or colleague
                    <span class="info-ic">
                        <span class="info-content">
                            Get $75 for every friend or colleague that joins ViewGen
                        </span>
                    </span>
                </header>
                <form action="" method="post">
                    <div class="row">
                        <div class="col2">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name">
                        </div>
                        <div class="col2">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col1">
                            <label for="email">Email Address</label>
                            <input type="email" name="email" id="email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col1">
                            <label for="message">Invite Message</label>
                            <textarea type="text" name="message" disabled="disabled" id="message">Hey, I`m using ViewGen to generate tons of inbound leads. Click on the link below to get a trial of ViewGen.</textarea>
                        </div>
                    </div>
                    <p class="error"><?=!empty($errors) ? $errors : ''?></p>

                    <div class="row">
                        <div class="col1">
                            <div class="note-wr">
                                <button type="submit" class="btn-submit" name="sendReferral">Send Referral</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- HTML -->

<?php
include('footer.php');
?>
