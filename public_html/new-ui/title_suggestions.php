<?php

$HTML_TITLE = "Title suggestions";
$ACTIVE_HEADER_PAGE = "campaign_builder";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

if (!isset($_GET['user_id']) || $_GET['user_id'] == '') {
	// Redirect to the first user.
	$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0 LIMIT 1");
	$query->execute([
		'company_id' => $_GET['company_id']
	]);

	if ($query->rowCount() == 0) {
		redirect('/new-ui/no_user.php?company_id=' . $_GET['company_id']);
		die;
	}

	$user = $query->fetch();

	redirect("/new-ui/title_suggestions.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
	die;
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND id = :user_id");
$query->execute([
	'company_id' => $_GET['company_id'],
	'user_id' => $_GET['user_id']
]);

if ($query->rowCount() == 0) {
	echo "Invalid user";
	die;
}

$user = $query->fetch();

require_once(__DIR__ . '/../api/APIModel.php');

include('header.php');

if (!($_POST["baseUrls"] ?? false)) {
//	$_POST["baseUrls"] = "https://www.linkedin.com/vsearch/p?openAdvancedForm=true&locationType=Y&rsid=161487691455819675684&orig=ADVS";
	$_POST["baseUrls"] = "";
//	$_POST["baseUrls"] = "https://www.linkedin.com/sales/search?keywords=&facet=N&facet=G&facet.N=S&facet.N=A&facet.G=us%3A0&count=25&start=0&updateHistory=true&rsid=161487691512472265649&orig=MDYS";
}
if (!($_POST["minWords"] ?? false)) {
	$_POST["minWords"] = 2;
}

$minWordOptions = array(1,2,3,4);
?>
<script type="application/javascript" src="<?=base_url('assets/js/jquery.min.js')?>"></script>

<script type="text/javascript">
$(function () {
	$(".select-all-titles").change(function () {
		var $checkboxes = $(this).parents("form").find('input[name^="selectedTitle"]');
		$checkboxes.prop('checked', $(this).prop('checked'));
	});
});
</script>

<div class="main-wrapper">
	<div class="campaign-info-box">
	<div class="campaign-info-box">
		<div class="col-md-12">
			<h2>Create Queries Based on Common Titles</h2>
		</div>
		<div class="ccol-md-12">
			On this page you find all the most common titles of the people you have visited. You can combine these titles with URL from LinkedIn to create highly targeted URL.<br>
			To use this, you first select "Base URL" which includes all the parameters you want excpt for title. Paste this URL into the text area and select all the relevant titles (click the top box to select all).
		</div>

	</div>
	</div>
</div>
<div class="main-wrapper">

<div class="left-side left-sidebar">
	<div class="row">
		<form id="title_form" method=post action=title_suggestions.php?company_id=<?=$_GET["company_id"] . '&user_id=' . $user['id']?>>
		<div class="col-xs-4">
			<b>1. Select titles</b><br>
			<table class="bordered-table">
				<tr>
					<th>
						<button type="submit" class="button gray small display-option-button is_hidden">Show</button>
						<select style="width: 200px;" class="display-option-dropdown" name=minWords>
<?php
foreach ($minWordOptions as $option) {
	echo "<option ".($option==$_POST["minWords"]?" selected":"")." value=$option>$option words or more</option>";
}
?>
						</select>
					</th>
					<th><input type="checkbox" name="selectAll" class="select-all-titles"></th>
				</tr>
<?php

$query = DB::prep("SELECT *
	FROM people
	WHERE
		company_id = :company_id AND
		(:userFilter = 0 OR :userFilter = crawled_by_id)
	LIMIT 100000");
$query->execute(array(
	"company_id"=>$_GET["company_id"],
	"userFilter"=>$user['linked_in_id']
));

$titleCount = array();
$titleOnce = array();
while ($people = $query->fetch()) {
	$p = explode(" at",$people["title"]);
	$p = explode(" @",$p[0]);
	$p = explode(" -",$p[0]);
	$cleanTitle = trim(mb_strtolower($p[0]));
	if ($titleOnce[$cleanTitle] ?? false) {
		if ($titleCount[$cleanTitle] ?? false) {
			$titleCount[$cleanTitle] ++;
		} else {
			$titleCount[$cleanTitle] = 2;
		}

	} else {
		$titleOnce[$cleanTitle] = 1;
	}
}
$baseUrls = trim($_POST["baseUrls"]);
arsort($titleCount);
$count = 0;
foreach ($titleCount as $key => $value) {
	if ($count < 200) {
		$words = explode(" ",$key);
		if (mb_strlen($key) > 2 AND count($words) >= $_POST["minWords"] AND mb_strlen($key) < 32) {
			echo "<tr>
				<td><label>\"$key\" ($value)";
			echo "</td>
			<td><input type='checkbox' ";
			if ($_POST["selectedTitle"][urlencode($key)] ?? false) {
				echo "checked ";
			}
			echo "name='selectedTitle[".urlencode($key)."]'></label></td>";
			$count++;

		}
	}
}

?>
			</table>
		</div>
    </div>
</div>
    <div class="right-side">
		<div class="col-xs-8">
            <?php
            $campaignName = '';
            $campaignNameEmpty = false;
            if (isset($_POST["campaign_name"])) {
                $campaignName = trim($_POST["campaign_name"]);
                if (!$campaignName) {
                    $campaignNameEmpty = true;
                }
            }
            ?>
			<b>2. Enter the Name of campaign</b><br>
            <input name="campaign_name" class="form-control" placeholder="Name the campaign..." type="text" value="<?=$campaignName?>">
			<br>
			<br>
			<b>3. Add "base URL" from LinkedIn to use together with your titles</b>
			<br>
			<textarea name=baseUrls rows=5 cols=120 class="form-control"><?=$_POST["baseUrls"]?></textarea><br>

			<b>4. Generate URL</b><br>
			<button type="submit" onclick="javascript:$('#title_form').submit()" class="btn btn-success btn-lg">Generate URL</button>
		</form>
			<br><br>
			<?php
			if (count($_POST["selectedTitle"] ?? []) == "" or ($_POST["selectedTitle"] ?? 0) == 0) {
				echo "<h1>Select at least one title among the boxes to the left</h1>";
			} else if (trim($_POST["baseUrls"]) == "") {
				echo "<h1>Add at least one URL to from LinkedIn to base the new URL on</h1>";
			} else if (strpos($_POST["baseUrls"],"title=")) {
				echo "<h1>Your base URL should not contain a title tag</h1>";
			} else if ($campaignNameEmpty) {
				echo "<h1>Enter the Name of campaign</h1>";
			} else {
				echo '
				<form method=post action="create_campaign.php?company_id='.$_GET["company_id"].'&user_id=' . $user['id'] . '">
				<b>5. Create a campaign based on the result</b>
				<div class="form-group">
                <input name="action" value="submit_campaign_form" type="hidden">
                <input name="campaign_name" value="' . $campaignName . '" type="hidden">
				<textarea name="query_urls" rows=12 cols=120 class="form-control">';
				$titleKeys = array_keys($_POST["selectedTitle"]);

                foreach ($titleKeys as $selectedTitle) {
                    $baseUrls .=  "&jobTitleEntities=".trim($selectedTitle);
                }
                $baseUrls .=  "&titleScope=CURRENT";
                $baseUrls = APIModel::urlNormalize(addOrFixRsid(trim($baseUrls), $user['linked_in_id']));
				echo $baseUrls . '</textarea></div>
				<button type="submit" class="btn btn-success btn-lg">Create Campaign Based on these URL</button>
				</form>
				';
			} ?>
		</div>
	</div>
</div>
