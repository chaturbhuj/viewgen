<?php
	require_once('../db.php');
//require_once('../authenticate.php');

function ajax_response($status, $payload = array()) {
    header('Content-Type: application/json');
    echo json_encode(array(
		'status' => $status,
		'payload' => $payload
	));
	die;
}

if (!isset($_POST['action'])) ajax_response("error");

$action = $_POST['action'];

if ($action == "get_location") {
	if (!isset($_POST['str'])) ajax_response("error");

	$str = $_POST['str'];

	//if ($str == "") ajax_response("success", array());

	$res = array();
	foreach ($idToIndustry as $id => $industry) {

		if ($str === "" || stripos($industry, $str) === 0) {
			$res[] = array(
				'n' => $industry,
				'c' => '',
				't' => 0,
				'id' => $id
			);
		}
	}
	ajax_response("success", $res);
} else {
	ajax_response("error");
}

?>
