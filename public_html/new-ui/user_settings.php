<?php
use classes\Settings;
use classes\ServiceStatus;

$HTML_TITLE = "User Settings";
$ACTIVE_HEADER_PAGE = "user_settings";

require_once('../authenticate.php');

require_once(__DIR__.'/../classes/Settings.php');
require_once(__DIR__.'/../classes/ServiceStatus.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

if (!isset($_GET['user_id']) || $_GET['user_id'] == '') {
	// Redirect to the first user.
	$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0 LIMIT 1");
	$query->execute([
		'company_id' => $_GET['company_id']
	]);

	if ($query->rowCount() == 0) {
		redirect('/new-ui/no_user.php?company_id=' . $_GET['company_id']);
		die;
	}

	$user = $query->fetch();

	redirect("/new-ui/user_settings.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
	die;
}

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND id = :user_id");
$query->execute([
	'company_id' => $_GET['company_id'],
	'user_id' => $_GET['user_id']
]);

if ($query->rowCount() == 0) {
	echo "Invalid user";
	die;
}

$user = $query->fetch();

$settingsModel = new Settings($user['company_id'], $user['id']);
$serviceStatusModel = new ServiceStatus($user['company_id'], $user['id']);
$settings = $settingsModel->getAllForUser();

$uninviterStatus = $serviceStatusModel->getStatus('uninviter');

if(!empty($_POST)){
    $settingNames = array_keys($settings);

    if(isset($_POST['action']) && $_POST['action'] == 'settings'){
        foreach($_POST as $key => $value){
            if(!in_array($key, $settingNames)){
                continue;
            }
            if($key == 'uninviter-time_period'){
                $value = $value * 60*60*24;
            }
            $settingsModel->setForUser($key, $value);
        }
    }

    if(isset($_POST['action']) && $_POST['action'] == 'start' && !empty($_POST['service'])){
        if(isset($_POST['uninviter-time_period'])){
            $settingsModel->setForUser('uninviter-time_period', intval($_POST['uninviter-time_period']) * 60*60*24);
        }

        $serviceStatusModel->start($_POST['service']);
    }


    redirect("/new-ui/user_settings.php?company_id=" . $_GET['company_id'] . '&user_id=' . $user['id']);
}






include('header.php');
?>

<div class="main-wrapper" id="campaign_builder" data-company-id="<?=$_GET['company_id']?>" data-file="user_settings.php">
	<div class="left-side left-sidebar">
		<div class="row">
			<div class="col-md-12">

				<? include('user_info_box.php'); ?>

				<div class="row user-stats">
					<div class="col-md-9 offset-md-1 user-stats-title text-center">
						VBR
					</div>

					<div class="col-md-12 left-stats text-center">
						Total View Back Rate
					</div>
					<div class="col-md-12 user-stats-row">
						<div class="left-stats">0</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="right-side uninviter" style="padding-top: 20px;">

        <?php if($settings['uninviter-enabled']): ?>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="uninviter-title">Uninviter</h2>
                    <h6 class="uninviter-subtitle">Here you can withdraw an invitation to connect if the recipient has not yet taken any action on the request. <br>The ability to forward or archive sent invitations is not available.</h6>
                </div>
            </div>
            <div class="flx-bl">
                <div class="flx-item interval-field">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="uninviter-time_period">Set age for non answered invites to be removed ( it will remove all invites older than particular age including ones sent manually from LinkedIn )</label>
                            <input class="form-control" type="text" value="<?=$settings['uninviter-time_period']/(60*60*24)?>" name="uninviter-time_period" id="uninviter-time_period">
                        </div>
                        <input type="hidden" name="service" value="uninviter">
                        <input type="hidden" name="action" value="settings">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
                <div class="flx-bl counting-start">
                    <form action="" method="post">
                        <label>Status: <span style="color: <?=$uninviterStatus == ServiceStatus::STATUS_RUNNING ? 'green' : 'red'?>"><?=ucfirst($uninviterStatus)?></span></label>
                        <br>
                        <input type="hidden" name="service" value="uninviter">
                        <input type="hidden" name="action" value="start">
                        <button
                            <?php if($uninviterStatus == ServiceStatus::STATUS_RUNNING): ?>disabled<?php endif;?>
                            type="submit"
                            class="btn btn-primary"
                        >
                            Start
                        </button>
                    </form>
                </div>
            </div>
            <p class="margin-top20">Or you can access sent invitations using this link and withdraw them manually <a href="https://www.linkedin.com/mynetwork/invitation-manager/sent">https://www.linkedin.com/mynetwork/invitation-manager/sent</a></p>
        <?php endif; ?>

        <?php if($settings['email_extractor-enabled']): ?>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="email_extractor-title">Email extractor</h2>
                </div>
            </div>
            <div class="flx-bl">
                Available requests: <?=$settingsModel->getForCompany('email_extractor-limit')?>
                Valid untill: <?=$settingsModel->getForCompany('email_extractor-expiration_date')?>
            </div>
        <?php endif; ?>
	</div>
</div>

<!-- HTML -->

<?php
include('footer.php');
?>

<script type="application/javascript">
    //TODO this is the copy of some functionality from newDynamicDasboard.js
    function handleChangeUser (event) {
        var $select = $(event.currentTarget);
        var file = "campaign_builder.php";
        if (this.data('file')) {
            file = this.data('file');
        }
        document.location.href = "/new-ui/" + file + "?company_id=" + this.data("company-id") + "&user_id=" + $select.val();
    }

    $('#change_user').bind('change', handleChangeUser.bind($('#campaign_builder')));
</script>