SELECT 
  apie.created AS 'apie__created_UTC',
	apie.company_id AS 'apie__company_id', 
  apie.linked_in_id AS 'linked_in_id', 
  apie.linked_in_name AS 'apie__linked_in_name', 
  apie.client_version AS 'apie__client_version', 
  apie.endpoint AS 'apie__endpoint', 
  apie.message AS 'apie__message', 
  apie.stack_trace AS 'apie__stack_trace', 
  apie.client_json AS 'apie__client_json', 
  apie.error_type AS 'apie__error_type', 
  apie.short_message AS 'apie__short_message', 
  apie.logger_short_message AS 'apie__logger_short_message', 
  apie.logger_message AS 'apie__logger_message', 
  apie.logger_error_type AS 'apie__logger_error_type', 
  apie.uid AS 'apie__uid', 
  apie.id AS 'id' 
FROM api_log apie
$where
ORDER BY apie.created
DESC