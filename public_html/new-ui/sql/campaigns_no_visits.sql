SELECT 
	a.id AS account_id, 
	a.company_name AS 'account',
	c.id AS 'Campaign Id', 
	c.name AS 'Campaign Name', 
	DATEDIFF(now(), c.created) AS 'Since Start',
	DATEDIFF(now(), c.campaign_completed) AS 'Since Finish',
	c.status as 'Status',
	COUNT(s.id) AS '# of URLs'
FROM campaign c
LEFT JOIN 
	search_url s ON c.id = s.campaign_id 
LEFT JOIN 
	company a ON c.company_id = a.id
WHERE 
c.campaign_visit_count = 0
AND
a.frozen = 0
AND
c.status = 'Finished'
GROUP BY
c.id
ORDER BY 
a.id 
DESC 
