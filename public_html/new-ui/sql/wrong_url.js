$(document).ready(function(){
	$("td.campaign_id").each(function(index){
		$(this).html( '<a href="?report=campaign_detail&filter=detail&campaign_id=' + $(this).text()  + '">' + $(this).text() + '</a>' );
	});		
	$("td.url").each(function(index){
		$(this).html( '<a href="' + $(this).text()  + '">' + $(this).text() + '</a>' );
	});
	$("td.visit_by").each(function(index){
		$(this).html( '<a href="?report=campaign_list&filter=user_and_account&account_id=' + $(this).siblings("td.account_id").text() + '&user_id=' + $(this).siblings("td.visit_by_id").text() + '">' + $(this).text() + '</a>' );
		$(".visit_by_id").hide();
	});	
	var hideStyle = $('<style class="hideStyle">#report_table td:nth-child(6){display: none;}</style>');
	$('html > head').append(hideStyle);		
});