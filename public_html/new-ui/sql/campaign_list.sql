SELECT 
	a.id AS account_id, 
	a.company_name AS 'account',
	c.id AS 'Campaign Id', 
	c.name AS 'Campaign Name',
	c.visit_by_id as user_id,
	c.visit_by AS 'Created By',
	s.total_matches AS '# of Matches', 
	c.campaign_visit_count AS '# of Visits',
	CONCAT(ROUND(SUM(c.campaign_visit_count)/SUM(s.total_matches) * 100,1) , '%')  AS '% Difference',
	DATEDIFF(now(), c.created) AS 'Since Start',
	DATEDIFF(now(), c.campaign_completed) AS 'Since Finish',
	c.status as 'Status',
	COUNT(s.id) AS '# of URLs'
FROM campaign c 
LEFT JOIN 
	search_url s ON c.id = s.campaign_id 
JOIN 
company a ON c.company_id = a.id
$where
GROUP BY 
	c.id  
ORDER BY c.id
DESC