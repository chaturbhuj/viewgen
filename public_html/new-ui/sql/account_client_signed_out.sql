SELECT 
a.id AS account_id,
a.company_name AS account_name,
u.id AS user_id,
u.linked_in_id,
u.name,
DATEDIFF(now(), u.user_created) AS '# of Days Active',
DATEDIFF(now(), u.last_connection) AS 'Days Since Last Connection'

FROM li_users u

LEFT JOIN company a
ON u.company_id = a.id

WHERE
u.software_status = 'offline'

AND
u.last_connection 
BETWEEN DATE_SUB(now(), INTERVAL 30 day)
AND DATE_SUB(now(), INTERVAL 0 day)

ORDER BY
u.last_connection
DESC