SELECT
	a.id as account_id,
	a.company_name as account_name,
	COUNT(DISTINCT u.linked_in_id) AS '# of Users',
	a.subscription_plan_code AS 'Subscription',
	a.account_type
FROM company a

LEFT JOIN li_users u
ON a.id = u.company_id

$where

GROUP BY 
a.id
ORDER BY
a.id
DESC