SELECT
id,
campaign_id,
company_id as account_id,
url,
visit_by,
visit_by_id,
DATEDIFF(now(), created) AS 'Since Start'
FROM search_url WHERE url NOT LIKE 'https://www.linkedin.com/sales%'

GROUP BY
campaign_id
ORDER BY
id DESC
LIMIT 100