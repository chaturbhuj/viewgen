$(document).ready(function () {
    $("td.cl__message, td.cl__stack_trace").each(function (index) {
        var el = $(this);
        var prevHtml = el.html();
        el.html('<button>...</button> ' + prevHtml);
        el.find('button').on('click', function (event) {
            modalLikePopup(null, 'Traces', prevHtml);
        })
    });
    
    var indexOfDateFilterInput = $('th.cl__created').index();
    
    $('td[data-column=' + indexOfDateFilterInput + '] input').attr('type', 'date');
});