SELECT
	a.id,
	a.company_name AS account_name,
	CONCAT_WS("<br>", CONCAT(a.first_name, ' ' , a.last_name),
		a.email ,
		a.phone ) AS 'Contact',
		DATEDIFF(now(), a.company_created) AS '# of Days Active',
		a.subscription_plan_code AS 'Subscription'
FROM company a

LEFT JOIN li_users u
ON a.id = u.company_id

LEFT JOIN search_url s
ON a.id = s.company_id

WHERE u.company_id IS NULL

GROUP BY 
a.id
ORDER BY
a.id
DESC

LIMIT 200