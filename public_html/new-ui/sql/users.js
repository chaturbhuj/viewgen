function postAction(controller, action, data, e) {
	$(e.currentTarget).parent("td").html("wait...");
	$.ajax({
		type: "POST",
		url: "/new-ui/controllers/index.php?controller="+controller+"&action="+action,
		data: JSON.stringify(data),
		success: function () {
		},
		contentType: "application/json",
		dataType: "json"
	}).always(function(){
		location.reload(true);
	});
	return true;
}

$(document).ready(function(){
	$("td.account_id").each(function(index){
		$(this).html( '<a href="//viewgentools.com/new-ui/campaign_builder.php?company_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});	
	$("td.account").each(function(index){
		$(this).html( '<a href="?report=accounts&filter=account&account_id=' + $(this).siblings("td.account_id").text() + '">' + $(this).text() + '</a>' );
		//$(this).text( '--' + $(this).text() );
	});
	$("td.total_campaigns").each(function(index){
		$(this).html( '<a href="?report=campaign_list&filter=user_and_account&account_id=' + $(this).siblings("td.account_id").text() + '&user_id=' + $(this).siblings("td.linked_in_id").text() + '">' + $(this).text() + '</a>' );
		//$(this).text( '--' + $(this).text() );
	});
		$("td.active_campaigns").each(function(index){
		$(this).html( '<a href="?report=campaign_list&filter=active&account_id=' + $(this).siblings("td.account_id").text() + '&user_id=' + $(this).siblings("td.linked_in_id").text() + '">' + $(this).text() + '</a>' );
		//$(this).text( '--' + $(this).text() );
	});

	$("td.primary_contact_email").each(function(index){
		$(this).html( '<a href="mailto:' + $(this).text() + '">' + $(this).text() + '</a>' );
		//$(this).text( '--' + $(this).text() );
	});
	$("td.private").each(function(index){
		if($(this).text() == 'Private mode'){
			$(this).parent().addClass('flag');
		}
	});
	$("td.of.Visits").each(function(index){
		$(this).html( '<a href="?report=visited_profiles&filter=account&account_id=' + $(this).siblings("td.account_id").text() + '">' + $(this).text() + '</a>' );
	});
	$("td.Actions").each(function(index){
		var data = JSON.parse($(this).text());
		var that = $(this);
		$(this).html('');
		data.actions.forEach(function(item){
			that.append($('<button>'+item.action+'</button>').click(function(e){
				postAction(item.controller,item.action,item.data,e);
			}));
		});
	});
	$(".linked_in_id").hide();
	var hideStyle = $('<style class="hideStyle">#report_table td:nth-child(9){display: none;}</style>');
	$('html > head').append(hideStyle);
});