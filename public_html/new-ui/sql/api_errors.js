$(document).ready(function () {
    $("td.apie__message, td.apie__stack_trace, td.apie__client_json, td.apie__logger_message").each(function (index) {
        var el = $(this);
        var prevHtml = el.html();
        el.html('<button>...</button><div style="max-height: 150px; overflow: scroll"></div>');
        el.find('div').html(prevHtml);
        el.find('button').on('click', function (event) {
            modalLikePopup(null, 'Traces', prevHtml);
        })
    });
    
    var indexOfDateFilterInput = $('th.apie__created_UTC').index();
    
    $('td[data-column=' + indexOfDateFilterInput + '] input').attr('type', 'date');
});