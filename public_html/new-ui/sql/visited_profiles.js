$(document).ready(function(){
	$("td.account_id").each(function(index){
		$(this).html( '<a href="?report=accounts&filter=account&account_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});
	$("td.crawled_by_id").each(function(index){
		$(this).html( '<a href="?report=html_dump&filter=account_user&account_id=' + $(this).siblings("td.account_id").text() + '&user_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});	
	$("td.crawled_by").each(function(index){
		$(this).html( '<a href="?report=users&filter=detail&user_id=' + $(this).siblings("td.crawled_by_id").text() + '">' + $(this).text() + '</a>' );
	});
	$("td.campaign_id").each(function(index){
		$(this).html( '<a href="?report=campaign_detail&filter=detail&campaign_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});
	$("td.account_id").each(function(index){
		$(this).html( '<a href="?report=accounts&filter=account&account_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});
	$("td.linked_in_id").each(function(index){
		$(this).html( '<a href="https://www.linkedin.com/sales/profile/' + $(this).text() + '">' + $(this).text() + '</a>' );
	});
});