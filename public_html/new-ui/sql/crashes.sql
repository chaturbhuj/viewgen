SELECT 
	cl.id AS 'id', 
	sl.product_key AS 'sl__product_key', 
	cl.company_id AS 'cl__company_id', 
	cl.parent_exception_id AS 'cl__parent_exception_id',
	cl.log_type AS 'cl__log_type', 
	cl.message AS 'cl__message', 
	cl.stack_trace AS 'cl__stack_trace', 
	cl.linked_in_id AS 'cl__linked_in_id', 
	cl.linked_in_name AS 'cl__linked_in_name',
	cl.created AS 'cl__created',
	c.is_test as 'c__is_test',
	cl.client_version AS 'cl__client_version'
FROM client_log cl INNER JOIN software_license sl 
      ON cl.company_id = sl.company_id
   INNER JOIN company c 
    ON cl.company_id = c.id
$where
ORDER BY cl.created
DESC