SELECT 
	a.id AS account_id, 
	a.company_name AS 'account',
	c.id AS 'Campaign Id', 
	c.name AS 'Campaign Name',
	s.total_matches AS '# of Matches', 
	c.campaign_visit_count AS '# of Visits',
	100 - ROUND(SUM(c.campaign_visit_count)/SUM(s.total_matches) * 100)  AS '% Difference',
	DATEDIFF(now(), c.created) AS 'Since Start',
	DATEDIFF(now(), c.campaign_completed) AS 'Since Finish',
	TIMEDIFF(u.last_connection, now()) AS 'Since Last Connection',
	c.status as 'Status',
	u.software_status,
	COUNT(s.id) AS '# of URLs'
FROM campaign c 
LEFT JOIN 
	search_url s ON c.id = s.campaign_id 
JOIN 
company a ON c.company_id = a.id
LEFT JOIN
li_users u ON c.visit_by_id = u.linked_in_id
WHERE
    u.software_status = 'running'
AND
	u.last_connection > now() - INTERVAL 3 hour
AND 
	c.status = 'Active'
GROUP BY 
	c.id  
ORDER BY c.id
DESC
