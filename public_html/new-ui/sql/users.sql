/*
this is the user table
*/
SELECT 
	c.id as account_id,
	c.company_name as `account`,
	u.name as `user_name`,
	
	sum(visited) as `Visits`,
	sum(visitors) as `RPV`,
	concat(round(sum(visitors)/sum(visited) * 100,1) , '%') as `VBR`,
	u.avg_views_before_sq as 'Avg Views Before',
	u.avg_views_with_sq as 'Avg Views With',
	
	u.linked_in_id,
	datediff(now(), u.user_created) as `days_active`,
	
	IF( u.have_sales_navigator = 0, 
		'<i class="fa fa-times" aria-hidden="true"></i>',
		IF( u.have_sales_navigator = 1,
			'<i class="fa fa-check" aria-hidden="true"></i>',
			''
		)
	) AS 'S.Nav',
	
	IF( u.anonymous = 'Full profile', 
		'<i class="fa fa-star" aria-hidden="true" style="color:#00b200;"></i>',
		IF( u.anonymous = 'Characteristics',
			'<i class="fa fa-times-circle" aria-hidden="true" style="color:#ffe400;"></i>',
			IF( u.anonymous = 'Private mode',
			'<i class="fa fa-times-circle" aria-hidden="true" style="color:#b20000;"></i>',
				IF( u.anonymous = 'Could Not Be Determined' OR '',
					'<i class="fa fa-question-circle" aria-hidden="true" style="color:#1a1aff;"></i>',
						IF( u.anonymous = '',
						'<i class="fa fa-question-circle" aria-hidden="true" style="color:#1a1aff;"></i>',
					''
		) ) ) )
	) AS 'Prv.',
	
	
	
	DATEDIFF(now(), u.lastAnonymusCheck) as 'Prv.Chk.',
	IF( u.software_status = 'offline', 
		'<i class="fa fa-circle" aria-hidden="true" style="color:#ccc;"></i>',
		IF( u.software_status = 'waiting',
			'<i class="fa fa-circle" aria-hidden="true" style="color:#ffe400;"></i>',
			IF( u.software_status = 'running',
				'<i class="fa fa-circle" aria-hidden="true" style="color:#8dc63f;"></i>',
				''
		) )
	) AS 'Sts.',
	
	u.active_campaign_count + u.paused_campaign_count + u.finished_campaign_count + u.archived_campaign_count as 'Camp.',
	u.active_campaign_count as 'A.Camp.',
	concat('{"actions":[{"controller":"users","action":"',  IF( u.hidden = 0, 'delete', 'restore'), '", "data": {"id": "',  u.id,  '", "companyId": "',  c.id,  '"}}]}') as 'Actions'
	

FROM li_users u 

left join company c 
on u.company_id = c.id

left join campaign_statistics m
on u.linked_in_id = m.li_user_id
and
c.id = m.company_id

$where


group by u.linked_in_id, c.id

order by u.hidden asc, u.user_created desc

;

