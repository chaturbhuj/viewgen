function postAction(controller, action, data, e) {
	$(e.currentTarget).parent("td").html("wait...");
	$.ajax({
		type: "POST",
		url: "/new-ui/controllers/index.php?controller="+controller+"&action="+action,
		data: JSON.stringify(data),
		success: function () {
		},
		contentType: "application/json",
		dataType: "json"
	}).always(function(){
		location.reload(true);
	});
	return true;
}

$(document).ready(function(){
	$("td.account_id").each(function(index){
		$(this).html( '<a href="//viewgentools.com/new-ui/campaign_builder.php?company_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});	
	$("td.of.Users").each(function(index){
		$(this).html( '<a href="?report=users&filter=company&company_id=' + $(this).siblings("td.account_id").text() + '">' + $(this).text() + '</a>' );
	});
	$("td.of.Campaigns").each(function(index){
		$(this).html( '<a href="?report=campaign_list&filter=account&account_id=' + $(this).siblings("td.account_id").text() + '">' + $(this).text() + '</a>' );
	});
	$("td.Actions").each(function(index){
		var data = JSON.parse($(this).text());
		var that = $(this);
		$(this).html('');
		data.actions.forEach(function(item){
			that.append($('<button>'+item.action+'</button>').click(function(e){
				postAction(item.controller,item.action,item.data,e);
			}));
		});
	});
});