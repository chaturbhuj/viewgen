SELECT
	a.id,
	a.company_name,
	CONCAT_WS("<br>", CONCAT(a.first_name, ' ' , a.last_name),
		a.email ,
		a.phone ) AS 'Contact',
	DATEDIFF(now(), a.company_created) AS '# of Days Active',
	a.subscription_plan_code AS 'Subscription',
	DATEDIFF(now(), MAX(u.last_connection)) as days_inactive
FROM company a

JOIN li_users u
ON a.id = u.company_id

WHERE u.last_connection > now() - INTERVAL 4 week

GROUP BY
a.id
HAVING SUM(u.active_campaign_count) = 0
ORDER BY
a.id
DESC