SELECT
company_id as account_id,
campaign_id,
crawled_by_id,
crawled_by,
name,
title,
employer,
location,
DATE_FORMAT(sent_to_crawler, '%c/%e/%y %I:%i %p') AS 'Crawled Date',
linked_in_id
FROM
people

$where

AND
crawled_by_id != 0

ORDER BY
id