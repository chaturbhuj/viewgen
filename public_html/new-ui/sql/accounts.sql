SELECT
	a.id as account_id,
	a.company_name as a__company_name,
	CONCAT_WS("<br>", CONCAT(a.first_name, ' ' , a.last_name),
		a.email ,
		a.phone ) AS 'Contact',
		DATEDIFF(now(), a.company_created) AS '# of Days Active',
		COUNT(DISTINCT u.linked_in_id) AS '# of Users',
		SUM(u.active_campaign_count + u.paused_campaign_count + u.finished_campaign_count + u.archived_campaign_count) AS '# of Campaigns',
		a.subscription_plan_code AS 'Subscription',
		a.recurlyAccountCode AS 'Recurly account',
		IF( a.recurly_sync = 0, 'off', 'on') AS 'Recurly auto sync',
	  concat('{"actions":[{"controller":"companies","action":"', 'syncRecurly', '", "data": {"id": "',  a.id,  '"}},{"controller":"companies","action":"', IF( a.recurly_sync = 0, 'recurlySyncOn', 'recurlySyncOff'), '", "data": {"id": "',  a.id,  '"}}]}') as 'Actions'
FROM company a

LEFT JOIN li_users u
ON a.id = u.company_id AND u.hidden = 0

$where

GROUP BY 
a.id
ORDER BY
a.id
DESC