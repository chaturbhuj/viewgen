SELECT 
	a.id AS account_id, 
	a.company_name AS 'account',
	c.id AS 'Campaign Id', 
	c.name AS 'Campaign Name', 
	c.campaign_visit_count AS '# of Visits',
	DATEDIFF(now(), c.created) AS 'Since Start',
	c.status as 'Status'
FROM campaign c 
LEFT JOIN 
	search_url s ON c.id = s.campaign_id 
LEFT JOIN 
	company a ON c.company_id = a.id 
WHERE
c.created > now() - INTERVAL 2 month
GROUP BY 
	c.id 
HAVING COUNT(s.id) = 0 
ORDER BY c.id 
DESC