SELECT
	companyId AS account_id,
	crawled_by,
	crawled_by_id,
	dump,
	htmlDumpId,
	htmlDumpCreated,
	ip,
	client_hash,
	html_people_id
FROM
htmlDump

$where

ORDER BY
htmlDumpId
DESC

LIMIT 500