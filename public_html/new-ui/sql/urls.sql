SELECT
u.id,
u.company_id AS account_id,
a.company_name AS account_name,
u.visit_by,
u.visit_by_id,
u.campaign_id,
u.url,
u.finished,
u.paused,
u.total_matches,
u.name,
u.revisit_after,
u.visit_after,
u.created,
u.zip_code_id,
u.parent_search_url_id,
u.duplicateSkipCount,
u.auto_generated,
u.search_url_vbr,
u.search_url_vbr_date,
u.search_url_visits,
u.search_url_viewbacks,
u.search_url_visit_count,
u.search_url_last_visit,
u.search_url_can_be_splitted,
max(p.id) as people_list_id 
FROM

search_url u

LEFT JOIN
people_list p
ON u.id = p.search_url_id
LEFT JOIN
company a ON
u.company_id = a.id

$where

GROUP BY u.id

ORDER BY u.id 
DESC

limit 1000
;