$(document).ready(function(){
	$("td.account_id").each(function(index){
		$(this).html( '<a href="//viewgentools.com/new-ui/campaign_builder.php?company_id=' + $(this).text() + '">' + $(this).text() + '</a>' );
	});	
	$("td.account").each(function(index){
		$(this).html( '<a href="?report=accounts&filter=account&account_id=' + $(this).siblings("td.account_id").text() + '">' + $(this).text() + '</a>' );
	});
	$("td.Campaign.Name").each(function(index){
		$(this).html( '<a href="?report=campaign_detail&filter=detail&campaign_id=' + $(this).siblings("td.Campaign.Id").text() + '">' + $(this).text() + '</a>' );
		$(".Campaign.Id").hide();
	});
	$("td.of.URLs").each(function(index){
		$(this).html( '<a href="?report=urls&filter=campaign&campaign_id=' + $(this).siblings("td.Campaign.Id").text() + '">' + $(this).text() + '</a>' );
	});	
	$("td.of.Visits").each(function(index){
		$(this).html( '<a href="?report=visited_profiles&filter=campaign&campaign_id=' + $(this).siblings("td.Campaign.Id").text() + '">' + $(this).text() + '</a>' );
	});			
	var hideStyle = $('<style class="hideStyle">#report_table td:nth-child(3){display: none;}</style>');
	$('html > head').append(hideStyle);
});