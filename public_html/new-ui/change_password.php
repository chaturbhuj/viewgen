<?php

$HTML_TITLE = "Download";
$ACTIVE_HEADER_PAGE = "download";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

$errorMessage = "";
$successMessage = "";
if (isset($_POST['password1'])) {
	if ($_COMPANY_DATA['password_hash'] == make_hash_password($_POST["password"], $_COMPANY_DATA['password_salt']) || !empty($_USER_DATA["admin"])) {
		if ($_POST['password1'] == $_POST['password2']) {
			if (strlen($_POST['password1']) >= 6) {
				$query = DB::prep("UPDATE company SET password_hash = :hash WHERE id = :company_id LIMIT 1");
				$query->execute([
					'hash' => make_hash_password($_POST["password1"], $_COMPANY_DATA['password_salt']),
					'company_id' => $_GET['company_id']
				]);
				$successMessage = "Password Updated";
			} else {
				$errorMessage = "Password is too short";
			}
		} else {
			$errorMessage = "Passwords did not match";
		}
	} else {
		$errorMessage = "The old password you provided did not match";
	}
}

include('header.php');

?>
<div class="new_container">
    <div class="container" id="campaign_builder" data-company-id="<?=$_GET['company_id']?>"
        data-user-id="<?=$_GET['user_id']?>">
        <div class="row">
            <div class="col-md-12">
                <div class="download-box" style="height: auto;">
                    <form method="post" action="/new-ui/change_password.php?company_id=<?=$_GET['company_id']?>">
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" name="password1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Repeat Password</label>
                            <input type="password" name="password2" class="form-control">
                        </div>
                        <?php if(empty($_USER_DATA["admin"])): ?>
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" name="password" class="form-control">
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <? if ($errorMessage) { ?>
                                <div class="alert alert-danger"><?=$errorMessage?></div>
                            <? } ?>
                            <? if ($successMessage) { ?>
                                <div class="alert alert-success"><?=$successMessage?></div>
                            <? } ?>
                            <input type="submit" class="btn btn-info" value="Update Password">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<? include('footer.php'); ?>
