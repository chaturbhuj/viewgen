<?php

namespace ui\api\recurly;

include_once(__DIR__.'/../../services/ajax_svc.php');
include_once(__DIR__.'/../../services/db_svc.php');
include_once(__DIR__.'/../../services/recurly_svc.php');

use function \services\ajax\json_fail;
use function \services\ajax\json_ok;
use function \services\ajax\get_request_data;
use \services\db\DBException;

use function \services\recurly\get_subscriptions;
use function \services\recurly\get_active_subscription;
use function \services\recurly\cancel_subscription;

require_once(__DIR__.'/../../authenticate.php');
authenticate();

$handlers = [
    'cancel-subscription' => function ($params) {
        //Example with JSON and fields validation:
        //get_request_data([
        //  'JSON' => [['company_id'], ['froze', true]]
        //])
        global $_COMPANY_DATA;
        if (! isset($_COMPANY_DATA['recurlyAccountCode'])) return false;
        
        return cancel_subscription(
            get_active_subscription(
                get_subscriptions($_COMPANY_DATA['recurlyAccountCode'])
            )
        );
    },
];
    
    
function run(){
    global $handlers;
    $data = get_request_data([
        'JSON' => [['action'], ['data', [] ]]
    ]);
    
    try {
        $results = $handlers[$data['action']]($data['data']);
        json_ok($results);
    }catch (DBException $e){
        json_fail($e->toArray());
    } catch (\Exception $e) {
        json_fail($e->getMessage());
    }
}

run();

?>
