<?php

namespace ui\api\numeria;

include_once(__DIR__.'/../../services/ajax_svc.php');
include_once(__DIR__.'/../../services/db_svc.php');
include_once(__DIR__.'/../../services/emailCapture/EmailCapture.php');
require_once(__DIR__.'/../../authenticate.php');

use function \services\ajax\get_param;
use function \services\ajax\json_fail;
use function \services\ajax\json_ok;
use function \services\ajax\get_request_data;

use \services\db\DBException;
use \services\emailCapture\EmailCaptureException;
use \services\emailCapture\EmailCapture;

authenticate();

$handlers = [
	'get-email' => function ($params) {
		global $_COMPANY_DATA;
		if (! isset($_COMPANY_DATA['id'])) throw new \Exception('Unauthenticated');
		$company_id = $_COMPANY_DATA['id'];
		$people_id = intval(get_param($params, 'people_id', 0));

		$service = new EmailCapture($company_id);

		try{
			$result = $service->getEmail($people_id);
			if($result){
				return $result;
			}
		}catch(EmailCaptureException $e){
			return $e->getMessage();
		}

		return 'Could not determine';
	},
	'get-all' => function ($params) {
		global $_COMPANY_DATA;
		if (! isset($_COMPANY_DATA['id'])) throw new \Exception('Unauthenticated');
		$company_id = $_COMPANY_DATA['id'];
		$campaign_id = intval(get_param($params, 'campaign_id', 0));
		$ignore_limit = intval(get_param($params, 'ignore_limit', 0));
		$visited = intval(get_param($params, 'visited', 0));

		$service = new EmailCapture($company_id);

		try{
			return $service->getAll($campaign_id, $ignore_limit, $visited);
		}catch(EmailCaptureException $e){
			return ['status' => 'error', 'message' => $e->getMessage()];
		}
	},
];


function run(){
	global $handlers;
	$data = get_request_data([
		'JSON' => [['action'], ['data', [] ]]
	]);

	try {
		$results = $handlers[$data['action']]($data['data']);
		json_ok($results);
	}catch (DBException $e){
		json_fail($e->toArray());
	} catch (\Exception $e) {
		json_fail($e->getMessage());
	}
}

run();

?>
