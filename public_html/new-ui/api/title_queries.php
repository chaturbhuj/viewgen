<?php

namespace ui\ajax\title_queries;

include_once(__DIR__.'/../../services/ajax_svc.php');
include_once(__DIR__.'/../../services/db_svc.php');

use function \services\ajax\get_param;
use function \services\ajax\json_fail;
use function \services\ajax\json_ok;
use function \services\db\select;
use \services\db\DBException;

$handlers = [
    'titles' => function ($search_string) {
        $limit = get_param($_GET, 'limit');
        $offset = get_param($_GET, 'offset', 0);
        return select(
            "SELECT title_global_title FROM title_global WHERE title_global_title != '' "
            . " AND title_global_title LIKE :search "
            . " ORDER BY title_global_count DESC "
            ." LIMIT :limit OFFSET :offset",
            [
                'search' => '%' . ($search_string ?? '') . "%",
                'limit' => (int)$limit,
                'offset' => (int)$offset,
            ],
            'title_global_title'
        );
    },
    'titles-company' => function ($search_string) {
        $company_id = get_param($_GET, 'company_id');
        $limit = get_param($_GET, 'limit');
        $offset = get_param($_GET, 'offset', 0);
        return select(
            "SELECT title_company_title FROM title_company WHERE company_id = :company_id AND title_company_title != '' "
            . " AND title_company_title LIKE :search "
            . " ORDER BY title_company_count DESC "
            ." LIMIT :limit OFFSET :offset",
            
            [
                'search' => '%' . ($search_string ?? '') . "%",
                'company_id' => $company_id,
                'limit' => (int)$limit,
                'offset' => (int)$offset,
            ],
            'title_company_title'
        );
    },
    'titles-linkedin' => function ($search_string) {
        $linked_in_id = get_param($_GET, 'linked_in_id');
        $limit = get_param($_GET, 'limit');
        $offset = get_param($_GET, 'offset', 0);
        return select(
            "SELECT title_company_title FROM title_company WHERE linked_in_id = :linked_in_id AND title_company_title != '' "
            . " AND title_company_title LIKE :search "
            . " ORDER BY title_company_count DESC "
            ." LIMIT :limit OFFSET :offset",
        
            [
                'search' => '%' . ($search_string ?? '') . "%",
                'linked_in_id' => $linked_in_id,
                'limit' => (int)$limit,
                'offset' => (int)$offset,
            ],
            'title_company_title'
        );
    },
    'campaigns' => function ($search_string) {
        $company_id = get_param($_GET, 'company_id');
        return select(
            "SELECT name,id FROM campaign WHERE company_id = :company_id "
            . " ORDER BY name DESC",
            [
                'company_id' => (int)$company_id
            ], [
                'name' => 'name',
                'id' => 'id'
            ]
        );
    },
    'titles-campaign' => function ($search_string) {
        $campaign_id = get_param($_GET, 'campaign_id');
        $limit = get_param($_GET, 'limit');
        $offset = get_param($_GET, 'offset', 0);
        
        $titles = select(
            "SELECT titles FROM campaign WHERE id = :campaign_id "
            . " AND titles LIKE :search "
            ." LIMIT :limit OFFSET :offset",
            [
                'search' => '%' . ($search_string ?? '') . "%",
                'campaign_id' => (int)$campaign_id,
                'limit' => (int)$limit,
                'offset' => (int)$offset,
            ],
            'titles'
        );
        $allTitles = array_reduce($titles, function ($acc, $titleBundle) {
            $ts = preg_split('/\s*(\n+|<\s*br\s*\/?\s*>)\s*/', $titleBundle);
            return array_merge($acc, $ts);
        }, []);

        if ($search_string) {
            return array_filter($allTitles, function ($title) use ($search_string) {
                return strpos($title, $search_string) !== false;
            });
        } else {
            return $allTitles;
        }

//        $titles = join('\n', $titles);
//        return preg_split("/\s*\n\s*/", $titles);
    },
];


function run(){
    global $handlers;
    $action = get_param($_GET, 'action');
    $search_string = isset($_GET['search']) ? $_GET['search'] : null;
    try {
        $results = $handlers[$action]($search_string);
        json_ok($results);
    }catch (DBException $e){
        json_fail($e->toArray());
    }
}


run();
