<?PHP
	
	require_once( __DIR__.'/../helper.php' );
	$helper = new Helper;
	header('Content-Type: application/json');
	error_reporting( 0 );
	$params = json_decode( $_POST['params'], true );
	$authenticated = $helper->authenticated( $params['campaign_id'], 'campaign_id' );
	if( ! $authenticated )
		die( json_encode( array( 'status' => 'error' ) ) );
	if( $params['loading'] == 'false' ){
		$sql = 'SELECT * FROM people WHERE campaign_id = ? AND sent_to_crawler <> ? LIMIT ?, 100';
	}else{
		$sql = 'SELECT * FROM people WHERE campaign_id = ? AND sent_to_crawler <> ? LIMIT 100, ?';
	}
	$badTime = '00-00-0000 00:00:00';
	//Need to cast limit to int for PDO to recognize this as such.
	$limiter = (int)$params['cur_limit'];
	$query = DB::prep( $sql );
	$query->bindParam( 1, $params['campaign_id'], PDO::PARAM_STR );
	$query->bindParam( 2, $badTime, PDO::PARAM_STR );
	$query->bindParam( 3, $limiter, PDO::PARAM_INT );
	$query->execute();
	while( $people = $query->fetch() ){
		if( trim( $people['name'] ) == "" ) 
			continue;
		$ppl_name = $people['name'];
		$ppl_title = $people['title'];
		$ppl_email = $people['email'];
		$ppl_employer = $people['employer'];
		$ppl_industry = $people['industry'];
		$ppl_location = $people['location'];
		$ppl_date = date( 'n/j/y', strtotime( $people['sent_to_crawler'] ) );
		$ppl_time = date( 'g:ia', strtotime( $people['sent_to_crawler'] ) );
		$output[] = $helper->evaluate( ABSOLUTE_PATH.'html_templates/campaign_outbound_list_item.html', get_defined_vars() );
	}
	$output = implode( "\n", $output );
	if( $params['loading'] == 'false' )
		$newLimit = $params['cur_limit'] + 100;
	else
		$newLimit = $params['cur_limit'];
	die( json_encode( array( 'status' => 'success', 'output' => $output, 'new_limit' => $newLimit, 'authenticated' => json_encode( $authenticated ) ) ) );

?>
