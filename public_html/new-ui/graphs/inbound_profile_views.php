<?php

if (1) {

$time_range = 3600*24*30;
if (isset($_GET['time_range'])) $time_range = (int)$_GET['time_range'];

$date_start = date('Y-m-d', time() - $time_range);
$date_end = date('Y-m-d', time());

$query = DB::prep("SELECT guessed_visit_date FROM inbound_visit WHERE crawled_by_id = :linked_in_id AND
	guessed_visit_date >= :date_start AND guessed_visit_date <= :date_end AND name != 'PRIVATE VIEWBACK'");
$query->execute([
	'linked_in_id' => $user['linked_in_id'],
	'date_start' => $date_start,
	'date_end' => $date_end
]);

$dates = [];
$end = strtotime($date_end);
for ($i = strtotime($date_start); $i < $end; $i += 3600*24) {
	$dates[date('Y-m-d', $i)] = 0;
}

while ($row = $query->fetch()) {
	if (!isset($dates[$row['guessed_visit_date']])) {
		$dates[$row['guessed_visit_date']] = 0;
	} else {
		$dates[$row['guessed_visit_date']]++;
	}
}

ksort($dates);

$data = [];
foreach ($dates as $date => $value) {
	$data[] = [
		'date' => $date,
		'value' => $value
	];
}

?>

<span class="chart-title">INBOUND PROFILE VIEWS</span>
<div id="chart_inbound" class="chart"></div>


<script>
var chart = AmCharts.makeChart("chart_inbound", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 60,
    "autoMarginOffset": 20,
    "mouseWheelZoomEnabled":true,
    "dataDateFormat": "YYYY-MM-DD",
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "balloon":{
          "drop":true,
          "adjustBorderColor":false,
          "color":"#ffffff"
        },
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "title": "red line",
        "useLineColorForBulletBorder": true,
        "valueField": "value",
        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
    }],
    "chartCursor": {
        "pan": true,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha":1,
        "cursorColor":"#258cbb",
        "limitToGraph":"g1",
        "valueLineAlpha":0.2,
        "valueZoomable":true
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "dataProvider": <?=json_encode($data);?>
});
</script>
<? } ?>
