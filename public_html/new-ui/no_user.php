<?php

$HTML_TITLE = "No User Yet";
$ACTIVE_HEADER_PAGE = "no_user";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

redirect('/new-ui/download.php?company_id=' . $_GET['company_id']);
?>
