<?php

require_once(__DIR__ . '/../services/ajax_svc.php');
use function services\ajax\json_ok;
use function services\ajax\json_fail;

require_once(__DIR__ . '/../services/li_account_type_svc.php');
use function services\li_account_type\prepareLIUrls;
use function services\li_account_type\checkLIAccount;

require_once(__DIR__ . '/../services/utilities_svc.php');
use function services\utilities_svc\encodeSearchUrl;

require_once(__DIR__ . '/../db.php');
require_once(__DIR__ . '/../api/APIModel.php');
require_once(__DIR__.'/../classes/linkedin/search/Url.php');
use linkedinSearch\Url;

function cleanRawTextParams($postParamName){
    $result = [];
    $dirtyList = explode("\n", trim($_POST[$postParamName]));
    foreach($dirtyList as $dirtyValue) {
        $kw = trim($dirtyValue);
        if ($kw != "") {
            $result[] = $kw;
        }
    }
    return $result;
}

$query = DB::prep("SELECT * FROM li_users WHERE id = ? AND company_id = ?");
$query->execute(array($_GET['user_id'], $_GET['company_id']));

if ($query->rowCount() == 0) {
    echo "Invalid user\n";
    die;
}
$li_user = $query->fetch();

$isSimpleSearch = checkLIAccount($li_user) === 'simple';


//***********************************************************************************************************************
//************************************************ Parameter Builder handling ******************************************
//***********************************************************************************************************************

if (isset($_GET['type']) && $_GET['type'] == "auto" && isset($_POST['action']) && $_POST['action'] == 'submit_campaign_form') {
    $urlModel = Url::getInstance($li_user);

    $itc = !empty($_POST['itc']) ? 1 : 0;
    $itc_message = !empty($_POST['itc_message']) ? $_POST['itc_message'] : '';
    //In case of parameter builder, we choose maximally feature rich URL type first, and then build using it
    //so here we handle the same way
    $urlBase = $urlModel::BASE_URL;

    $campaignParams = [];

    if (isset($_POST["keywords"]) and trim($_POST["keywords"]) != "") {
        $campaignParams['keywords'] = cleanRawTextParams('keywords');
        $campaignParams['skipKeywords'] = cleanRawTextParams('skipKeywords');
    }

    if (isset($_POST["firstName"]) and $_POST["firstName"] != "") {
        $campaignParams['firstName'] = cleanRawTextParams('firstName');
    }

    if (isset($_POST["lastName"]) and $_POST["lastName"] != "") {
        $campaignParams['lastName'] = cleanRawTextParams('lastName');
    }

    if (isset($_POST["titles"]) and trim($_POST["titles"]) != "") {
        $campaignParams['titles'] = cleanRawTextParams('titles');
        $campaignParams['skipTitles'] = cleanRawTextParams('skipTitles');
    }

    if (isset($_POST["companies"]) and trim($_POST["companies"]) != "") {
        $campaignParams['companies'] = cleanRawTextParams('companies');
    }

    if (isset($_POST["companySize"])) {
        $campaignParams['companySize'] = array_keys($_POST["companySize"]);
    }

    if (isset($_POST["relation"])) {
        $campaignParams['relation'] = array_keys($_POST["relation"]);
    }

    if (isset($_POST["groups"])) {
        $campaignParams['groups'] = array_keys($_POST["groups"]);
    }

    if (isset($_POST["seniorityLevel"])) {
        $campaignParams['seniorityLevel'] = array_keys($_POST["seniorityLevel"]);
    }

    if (isset($_POST["function"])) {
        $campaignParams['function'] = array_keys($_POST["function"]);
    }

    if (isset($_POST["yearsInCurrentPosition"])) {
        $campaignParams['yearsInCurrentPosition'] = array_keys($_POST["yearsInCurrentPosition"]);
    }

    if (isset($_POST["yearsAtCurrentCompany"])) {
        $campaignParams['yearsAtCurrentCompany'] = array_keys($_POST["yearsAtCurrentCompany"]);
    }

    if (isset($_POST["yearsOfExperience"])) {
        $campaignParams['yearsOfExperience'] = array_keys($_POST["yearsOfExperience"]);
    }

    $locations = [];
    if (isset($_POST["geography__hidden_location_facets"])) {
        $locations = explode('--', $_POST["geography__hidden_location_facets"]);
        foreach ($locations as $key => $value){
            if(empty($value)){
                unset($locations[$key]);
            }
        }
        if(!empty($_POST["predefinedRegions"])){
            $locations = array_merge($locations, array_keys($_POST["predefinedRegions"]));
        }
        $campaignParams['locations'] = $locations;
    }

    if (isset($_POST["industry_location"])) {
        $campaignParams['industry'] = array_keys($_POST["industry_location"]);
    }

    if (isset($_POST["postalCode"]) and trim($_POST["postalCode"]) != "") {
        $campaignParams['postalCode']["radiusMiles"] = $_POST["postalCodeWithin"];
        $campaignParams['postalCode']["countryCode"] = $_POST["postalCodeCountry"];
        $campaignParams['postalCode']["postalCode"] = $_POST["postalCode"];
    }

    $finalUrls = [];

    $companiesList = $campaignParams['companies'] ?? [];

    $companiesPerUrl = 200;
    $companySliceCount = ceil(count($companiesList)/$companiesPerUrl);
    if(!$companySliceCount){
        $companySliceCount = 1;
    }

    for($i = 0; $i < $companySliceCount; $i++){
        $campaignParams['companies'] = array_slice($companiesList, $i*$companiesPerUrl, $companiesPerUrl);
        $finalUrls[] = $urlModel->assemble($campaignParams);
    }

    //if this is just a URL creation call
    if (isset($_GET['get_url'])) {
        json_ok(["url" =>  $finalUrls[count($finalUrls) - 1]]);
    }


    if (!isset($_POST['location_location'])) $_POST['location_location'] = array();
    if (!isset($_POST['relation'])) $_POST['relation'] = array();
    if (!isset($_POST['companies'])) $_POST['companies'] = "";
    if (!isset($_POST['industry_location'])) $_POST['industry_location'] = array();
    if (!isset($_POST['companySize'])) $_POST['companySize'] = array();
    if (!isset($_POST['function'])) $_POST['function'] = array();
    if (!isset($_POST['titles'])) $_POST['titles'] = array();
    if (!isset($_POST['seniorityLevel'])) $_POST['seniorityLevel'] = array();
    if (!isset($_POST['yearsInCurrentPosition'])) $_POST['yearsInCurrentPosition'] = array();
    if (!isset($_POST['yearsAtCurrentCompany'])) $_POST['yearsAtCurrentCompany'] = array();
    if (!isset($_POST['yearsOfExperience'])) $_POST['yearsOfExperience'] = array();
    if (!isset($_POST['groups'])) $_POST['groups'] = array();
    if (!isset($_POST['postalCodeWithin'])) $_POST['postalCodeWithin'] = '';
    if (!isset($_POST['postalCodeCountry'])) $_POST['postalCodeCountry'] = '';
    if (!isset($_POST['postalCode'])) $_POST['postalCode'] = '';


    $query = DB::prep("INSERT INTO campaign
		(company_id, publish_status, is_query_campaign, priority, secondary_priority, start_date,
		end_date, name, visit_by, visit_by_id, status, revisit_after, locations, original_url, base_url, campaign_comment,
		industries, keywords, skip_keywords, skip_titles, seniority_levels, company_sizes, functions, titles, companies, relationships,
		postal_code_within, postal_code_country, postal_code, years_in_current_position, years_at_current_company,
		years_of_experience, search_first_name, search_last_name, groups, itc, itc_message) VALUES(:company_id, 'published', :is_query_campaign,
		:priority, :secondary_priority, NOW(), '000-00-00', :name, :visit_by, :visit_by_id, :status, :revisit_after, :locations,
		:original_url, :base_url, 'Need URLs', :industries, :keywords, :skip_keywords, :skip_titles, :seniority_levels, :company_sizes, :functions, :titles,
		:companies, :relationships, :postal_code_within, :postal_code_country, :postal_code,
		:years_in_current_position, :years_at_current_company, :years_of_experience, :search_first_name,
		:search_last_name, :groups, :itc, :itc_message)");
    $query->execute(array(
        'company_id' => $_GET['company_id'],
        'is_query_campaign' => 0,
        'priority' => 3,
        'secondary_priority' => 1000,
        'name' => $_POST['campaign_name'],
        'visit_by' => $li_user['name'],
        'visit_by_id' => $li_user['linked_in_id'],
        'status' => 'Active',
        'revisit_after' => 0,
        'locations' => implode("\n", $locations),
        'original_url' => '',
        'base_url' => $urlBase,
        'industries' => implode("\n", array_keys($_POST['industry_location'])),
        'keywords' => $_POST['keywords'],
        'skip_keywords' => $_POST['skipKeywords'],
        'skip_titles' => $_POST['skipTitles'],
        'seniority_levels' => implode("\n", array_keys($_POST["seniorityLevel"])),
        'company_sizes' => implode("\n", array_keys($_POST["companySize"])),
        'functions' => implode("\n", array_keys($_POST["function"])),
        'titles' => $_POST["titles"],
        'companies' => $_POST["companies"],
        'relationships' => implode("\n", array_keys($_POST["relation"])),
        'postal_code_within' => $_POST['postalCodeWithin'],
        'postal_code_country' => $_POST['postalCodeCountry'],
        'postal_code' => $_POST['postalCode'],
        'years_in_current_position' => implode("\n", array_keys($_POST["yearsInCurrentPosition"])),
        'years_at_current_company' => implode("\n", array_keys($_POST["yearsAtCurrentCompany"])),
        'years_of_experience' => implode("\n", array_keys($_POST["yearsOfExperience"])),
        'search_first_name' => $_POST['firstName'],
        'search_last_name' => $_POST['lastName'],
        'groups' => implode("\n", array_keys($_POST["groups"])),
        'itc' => $itc,
        'itc_message' => $itc_message
    ));
    $newCampaignId = DB::get_instance()->lastInsertId();

    $query = DB::prep("INSERT INTO search_url (total_matches,url, campaign_id, company_id, visit_by, visit_by_id) VALUES(-1,?, ?, ?, ?, ?)");

    foreach ($finalUrls as $finalUrl) {
        $query->execute(array($finalUrl, $newCampaignId, $_GET['company_id'], $li_user['name'], $li_user['linked_in_id']));
    }
    redirect('/new-ui/campaign_builder.php?campaign_id=' . $newCampaignId . '&company_id=' . $_GET['company_id'] . '&user_id=' . $_GET['user_id']);

    
//************************************************************************************
//******************************** Manual URLs ***************************************
//************************************************************************************

} elseif (isset($_POST['action']) && $_POST['action'] == 'submit_campaign_form') {
    $itc = !empty($_POST['itc']) ? 1 : 0;
    $itc_message = !empty($_POST['itc_message']) ? $_POST['itc_message'] : '';

    //Manually enter URLs
    $query = DB::prep("SELECT * FROM li_users WHERE id = ? AND company_id = ?");
    $query->execute(array($_GET['user_id'], $_GET['company_id']));

    if ($query->rowCount() == 0) {
        echo "Invalid user\n";
        die;
    }
    $li_user = $query->fetch();

    $query_urls = explode("\n", trim(str_replace("\r", "", $_POST['query_urls'])));

    $query_urls = array_reverse($query_urls);

    $result = prepareLIUrls($query_urls, $isSimpleSearch);
    $error = $result['error'];

    //error_log('enc_urls ' . var_export($encoded_urls, true));

    if (! $error) {
        $encoded_urls = $result['encoded_urls'];
        
        $query = DB::prep("INSERT INTO campaign (company_id, publish_status, is_query_campaign, priority, start_date,
			end_date, name, visit_by, visit_by_id, status, revisit_after, locations, original_url, itc, itc_message)
			VALUES(?, 'published', ?, ?,NOW(), '000-00-00', ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $query->execute(array(
            $_GET['company_id'],
            1,
            3,
            $_POST['campaign_name'],
            $li_user['name'],
            $li_user['linked_in_id'],
            'Active',
            0,
            '',
            implode("|", $encoded_urls),
            $itc,
            $itc_message
        ));
        $newCampaignId = DB::get_instance()->lastInsertId();

        $multipliedUrls = array();

        foreach ($encoded_urls as $url) {
            $query = DB::prep("INSERT INTO search_url (total_matches,url, campaign_id, company_id, 
                              visit_by, visit_by_id) VALUES(-1,?, ?, ?, ?, ?)");
            $query->execute(array($url, $newCampaignId, $_GET['company_id'], $li_user['name'], $li_user['linked_in_id']));
        }

        // Submit data for campaign.
        $query = DB::prep("UPDATE campaign SET publish_status='published' WHERE company_id=? AND id=?");
        $query->execute(array(
            $_GET['company_id'],
            $newCampaignId
        ));

        redirect('/new-ui/campaign_builder.php?campaign_id=' . $newCampaignId . 
            '&company_id=' . $_GET['company_id'] . '&user_id=' . $_GET['user_id']);
    } else {
        $error_url = encodeSearchUrl($result['error_url']);
        redirect("/new-ui/campaign_builder.php?error-type=${error}&error-url=${error_url}&company_id=" . 
            $_GET['company_id'] . '&user_id=' . $_GET['user_id']);
    }

}
?>