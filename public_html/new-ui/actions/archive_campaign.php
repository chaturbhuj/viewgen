<?php

require_once('../../authenticate.php');

if (!isset($_GET['company_id']) || !isset($_GET['user_id'])) {
	die;
}

$query = DB::prep("UPDATE campaign SET publish_status = 'archived' WHERE
	id = :campaign_id AND company_id = :company_id");
$query->execute([
	'campaign_id' => $_GET['campaign_id'],
	'company_id' => $_GET['company_id']
]);

redirect('/new-ui/campaign_builder.php?company_id=' . $_GET['company_id'] . '&user_id=' . $_GET['user_id']);

?>
