<?
require_once(__DIR__."/../classes/Settings.php");

// Find the country.

$query = DB::prep("SELECT * FROM li_location WHERE location_name like :location_name");
$query->execute([
	'location_name' => 'US' // geoip_country_name_by_name($_SERVER['REMOTE_ADDR'])
]);

$defaultLocation = null;
if ($query->rowCount() > 0) {
	$defaultLocation = $query->fetch();
}

global $DEFERRED_ASSETS;
$DEFERRED_ASSETS[] = '/assets/js/tree.jquery.js';
$DEFERRED_ASSETS[] = '/assets/ui/campaign_builder/js/modal_script.js';

$settingsModel = new \classes\Settings($_GET['company_id'], $_GET['user_id']);
?>

<link rel="stylesheet" href="/assets/ui/campaign_builder/css/modal_style.css">

<div class="modal fade" id="buildCampaignModal">
	<div class="modal-dialog modal-wide" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <div class="campaign_controls_header">
               		<input id="mainCampaignNameField" class="item" placeholder="Name the campaign..." type="text" />
                    <button type="button" class="item btn btn-sm btn-primary" id="saveNewCampaign">Save</button>
                    <button type="button" class="item btn btn-sm btn-primary modalCampaignBuilderClear" data-dismiss="modal">Cancel</button>
					<button type="button" class="item btn btn-sm btn-primary modalCampaignBuilderClear" aria-label="Clear">Clear</button>
                    <?php if($settingsModel->getForUser('itc-enabled')):?>
    					<input type="checkbox" id="mainItcField" class="" style="width: 10px"> invite to connect
                    <?php endif;?>

               	</div>
				<h5 class="modal-title">Campaign Builder</h5>
<!--				<a style="color: red; position: relative; top: 2px; right: 20px;" href="http://viewgentools.com/help/index.php/2017/03/03/workaround-for-linkedin-url-problem/" target="_blank"><u>WATCH THIS</u> - "Workaround for LinkedIn URL problem."</a>-->
                <div class="campaign_controls_header">
                <button type="button" class="item btn btn-sm btn-primary campaign-builder-url-toggle builder-nav-urls" href="#"><span class="manual">Manually enter URLs</span><span class="back" style="display: none">Back</span></button>
                <a href="/new-ui/title_suggestions.php?company_id=<?=$_GET['company_id']?>&user_id=<?=$_GET['user_id'] ?? ''?>"><button type="button" class="item btn btn-sm btn-primary builder-nav-urls">Title suggestions</button></a>
               	</div>
                <button type="button" class="close modalCampaignBuilderClear" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
            <?php if($settingsModel->getForUser('itc-enabled')):?>
                <div class="modal-header-extra flx-bl">
                    <div class="message-wr">
                        <textarea id="mainItcMessageField" placeholder="Invite message" maxlength="300">Hello {{first_name}}</textarea>
                        <div class="character-counter" id='counter'></div>
                    </div>
                    <div class="btns-wr">
                        <?php if($isSimpleSearch === false):?>
                            <button type="button" class="btn btn-add-name btn-add-param" data-name="first_name" >first_name</button>
                            <button type="button" class="btn btn-add-name btn-add-param" data-name="last_name" >last_name</button>
                        <?php else:?>
                            <button type="button" class="btn btn-add-name btn-add-param" data-name="full_name" >full_name</button>
                        <?php endif;?>
                        <div>We recommend withdrawing your old unanswered Invitations:  please <a href="/new-ui/user_settings.php">click here</a> </div>
                    </div>
                </div>
            <?php endif;?>
			<div class="modal-body">
                <div class="modal-issues modal-errors" style="display:none"></div>
                <div class="modal-issues modal-warnings" style="display:none"></div>
                <div class="modal-issues modal-info" style="display:none"></div>
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active builder-nav-parameter" href="#">Parameter</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" class="builder-nav-urls" href="#">URLs</a>
					</li>
				</ul>
                
				
				<div class="campaign-builder-parameters">
                    <div class="assembled-url">
                        <div><div id="assembled-url"></div></div>
                        <button id="show-assembled-url" 
                                data-url="" 
                                type="button" 
                                class="item btn btn-sm btn-primary"
                                style="display: none"
                        >Show in LinkedIn</button>
                    </div>
					<form method="post" action="/new-ui/create_campaign.php?type=auto&user_id=<?=$_GET['user_id']?>&company_id=<?=$_GET['company_id']?>" 
                          id="autoCampaignForm">
                    <input type="hidden" name="action" value="submit_campaign_form">
<!--                   this hidden field is populated from #mainCampaignNameField-->
                    <input style="display:none"
                           name="campaign_name"
                           type="text" />
                    <input style="display:none"
                           name="itc"
                           type="hidden" />
                    <input style="display:none"
                           name="itc_message"
                           type="hidden" />
                    <?PHP
                        include_once( 'helper.php' );
                        if($isSimpleSearch === false){
                            include_once( ABSOLUTE_PATH.'new_campaign_modal/modal_controller.php' );
							include_once( ABSOLUTE_PATH.'new_campaign_modal/modal_controller_v1.php' );
							if($user['search_design_version']){
								$c = new ModalControllerV1($user);
							}else{
								$c = new ModalController($user);
							}
                        } elseif($isSimpleSearch === true){
                            include_once( ABSOLUTE_PATH.'new_campaign_modal/SimpleSearchModalController.php' );
                            $c = new SimpleSearchModalController($user);
                        }
                        echo $c->outputHTML();
					?>
					</form>
				</div><!--end campaign parameters -->
				<div class="campaign-builder-urls"><!--Manually enter URLs-->
					<form method="post" action="/new-ui/create_campaign.php?user_id=<?=$_GET['user_id']?>&company_id=<?=$_GET['company_id']?>" id="queryCampaignForm">
						<input type="hidden" name="action" value="submit_campaign_form">
                        <input style="display:none"
                               name="campaign_name"
                               type="text" />
                        <input style="display:none"
                               name="itc"
                               type="hidden" />
                        <input style="display:none"
                               name="itc_message"
                               type="hidden" />
						<div class="row">
							<div class="col-md-12">
<!--								<span class="parameter-title"></span>-->
                                <p class="help-message">
<!--                                    Use the 'Advanced Search' or 'Sales Navigator' search tool to generate the query URL and then paste them below.-->
                                    To generate a manual URL, you have to go to <a href="<?=$basicSearchUrl ?>" target="_blank">Search Page</a>,
                                    <br /> construct your filters, press "Search"
                                    and then copy a URL from the address bar after you have a search result in front of you.
                                </p>
<!--                                <ol class="help-message">-->
<!--                                    <li>Paste search query URL(s) from LinkedIn into the box below. Separate with new lines if more than 1.</li>-->
<!--                                </ol>-->
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<span class="parameter-title">Paste a URL from LinkedIn search. Also you can paste a list with one url on each line.</span>
								<textarea class="query-enter-url form-control"></textarea>
							</div>
						</div>
<!--                        <a class="btn btn-primary" id="add_li_url" href="#">Validate and Add</a>-->
<!--                        <a class="clear-query-enter-url btn btn-secondary" href="#">Clear input</a>-->
                        <div class="row show-on-added-urls" style="display: none">
                            <div class="col-md-12">
<!--                                <ol start="2" class="help-message">-->
<!--                                    <li>After automatic validation, URLs are added to the area below and pending for <strong>Save</strong> of campaign to take effect</li>-->
<!--                                </ol>-->
                                <span class="parameter-title">Added URL (click 'x' to reject)</span>
                                <div class="form-control query-urls-list"></div>
                                <span class="parameter-title">Don't forget to SAVE the campaign!</span>
                            </div>
                        </div>
<!--                        <a class="clear-added-urls btn btn-warning" href="#">Clear Added URLs</a>-->
<!--                        <ol start="3" class="show-on-added-urls help-message" style="display: none">-->
<!--                            <li>To reject any added URL, click <span style="font-size: 20px; font-weight: bold">×</span> in front of it</li>-->
<!--                        </ol>-->
                        <textarea class="query-urls" name="query_urls" style="display: none"></textarea>
					</form>

				</div>
			</div>
			<div class="modal-footer" style="display:none">
			</div>
		</div>
	</div>
</div>

