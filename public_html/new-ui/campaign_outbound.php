<?php

global $DEFERRED_ASSETS;
$DEFERRED_ASSETS[] = '/assets/ui/numeria.js';
require_once( 'helper.php' );
$helper = new Helper;

if( isset( $active_campaign ) ){
	$perPage = 100;
	$offset = !empty( $_GET['offset']) ? intval($_GET['offset']) : 0;

    $search = !empty($_GET['search']) ? trim($_GET['search']) : '';

    $activeSort = !empty($_GET['sort']) ? trim($_GET['sort']) : 'visited';

	$sort = 'sent_to_crawler';
    switch ($activeSort){
        case 'visited':
            $sort = 'sent_to_crawler';
            break;
        case 'profile':
            $sort = 'name';
            break;
        case 'company':
            $sort = 'employer';
            break;
        case 'location':
            $sort = 'location';
            break;
		case 'email':
			$sort = 'email';
			break;
    }
	$dir = (!empty($_GET['dir']) && trim($_GET['dir']) === 'asc') ? 'asc' : 'desc';

    $q = DB::prep('
                    SELECT
                      SQL_CALC_FOUND_ROWS
                      *
                    FROM
                      people
                    WHERE
                      campaign_id = :campaignId
                      AND sent_to_crawler != \'0000-00-00 00:00:00\'
                      AND ( :search = \'\' OR ( name LIKE :search OR location LIKE :search OR industry LIKE :search OR title LIKE :search OR employer LIKE :search ))
                    ORDER BY '.$sort.' '.$dir.'
                    LIMIT '.$perPage.'
                    OFFSET '.$offset * $perPage.'
                  ');
    $q->execute([
        'campaignId' => $active_campaign['id'],
        'search' => '%'.$search.'%'
    ]);

	$peoples = array();
	while ($row = $q->fetch()) {
		if (trim($row['name']) == "") continue;
		$peoples[] = $row;
	}

	$q = DB::prep( 'SELECT FOUND_ROWS()' );
	$q->execute();
	$totalResults = $q->fetch();

    $nextOffset = $offset + 1;
	$previousOffset = 0;

    $isAPreviousOffset = false;
    if( $offset > 0 ){
		$isAPreviousOffset = true;
		$previousOffset = $offset - 1;
	}
	$isANextOffset = ( $totalResults[0] <= $nextOffset * $perPage ) ? false : true;

	if( $totalResults[0] > $perPage ){
		if( $isAPreviousOffset )
			$previousButton = '<a class="pagination_previous" href="?'.$helper->formatQueryString($_GET, ['offset' => $previousOffset]).'"><</a>';
		if( $isANextOffset )
			$nextButton = '<a class="pagination_next" href="?'.$helper->formatQueryString($_GET, ['offset' => $nextOffset]).'">></a>';

		//Need to round up to nearest 100 up to know how many pages
        $totalNumber = ceil( (int)$totalResults[0] / $perPage );

		$links = array();
		for( $i = 1; $i <= $totalNumber; $i++ ){
			$offsetBase = $i - 1;
			$class = '';
			if($offset == $offsetBase ){
				$class = 'current_offset_selected';
			}
			$links[] = '<a class="pagination_number '.$class.'" href="?'.$helper->formatQueryString($_GET, ['offset' => $offsetBase]).'">'.$i.'</a>';
		}
		if( ! empty( $links ) ){
			$inbetweenLinks = implode( "\n", $links );	
		}
	}
?>

<style>
    i.active {
        border: solid #008cc9;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
    }

    i.active.up {
        transform: rotate(-135deg);
        -webkit-transform: rotate(-135deg);
    }

    i.active.dn {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
</style>
<div class="row">

	<div class="col-md-12">
		<a href="/visitedProfiles.php?company_id=<?=$_GET['company_id']?>&campaign_id=<?=$active_campaign['id']
			?>&order_by=&direction=&days=0&start_date=&end_date=&export=true"
			class="float-right export-as-csv">Export Visited Profiles as CSV</a>
	</div>
	<div class="col-md-12">
        <form action="<?=$_SERVER['SCRIPT_NAME']?>" method="get">
            <input type="hidden" value="<?=$_GET['company_id']?>" name="company_id" />
            <input type="hidden" value="<?=$_GET['user_id']?>" name="user_id" />
            <input type="hidden" value="<?=$_GET['campaign_id']?>" name="campaign_id" />
            <input type="hidden" value="<?=$_GET['page']?>" name="page" />
            <input type="search" name="search" value="<?=$search?>" placeholder="Search name, title, industry or company..." />
            <button type="submit">Search</button>
        </form>
        <table class="table viewgen-table">
			<thead>
				<tr>
					<th>
                        <a href="?<?=$helper->formatQueryString($_GET, ['offset' => '', 'sort' => 'profile', 'dir' => ($dir === 'asc' && $activeSort === 'profile') ? 'desc' : 'asc'])?>">
                            Profile ( Total Results: <?=$totalResults[0]?> )
                            <i class="<?=$activeSort === 'profile' ? ($dir === 'asc' ? 'active up' : 'active dn') : ''?>"></i>
                        </a>
                    </th>
					<th>
                        <a href="?<?=$helper->formatQueryString($_GET, ['offset' => '', 'sort' => 'company', 'dir' => ($dir === 'asc' && $activeSort === 'company') ? 'desc' : 'asc'])?>">
                            Company
                            <i class="<?=$activeSort === 'company' ? ($dir === 'asc' ? 'active up' : 'active dn') : ''?>"></i>
                        </a>
                    </th>
					<th>
                        <a href="?<?=$helper->formatQueryString($_GET, ['offset' => '', 'sort' => 'location', 'dir' => ($dir === 'asc' && $activeSort === 'location') ? 'desc' : 'asc'])?>">
                            Location
                            <i class="<?=$activeSort === 'location' ? ($dir === 'asc' ? 'active up' : 'active dn') : ''?>"></i>
                        </a>
                    </th>
					<th>
                        <a href="?<?=$helper->formatQueryString($_GET, ['offset' => '', 'sort' => 'visited', 'dir' => ($dir === 'asc' && $activeSort === 'visited') ? 'desc' : 'asc'])?>">
                            Visited
                            <i class="<?=$activeSort === 'visited' ? ($dir === 'asc' ? 'active up' : 'active dn') : ''?>"></i>
                        </a>
                    </th>

					<?php if($settingsModel->getForCompany('email_extractor-enabled')):?>
						<th>
							<a href="?<?=$helper->formatQueryString($_GET, ['offset' => '', 'sort' => 'email', 'dir' => ($dir === 'asc' && $activeSort === 'email') ? 'desc' : 'asc'])?>">
								Email
								<i class="<?=$activeSort === 'email' ? ($dir === 'asc' ? 'active up' : 'active dn') : ''?>"></i>
							</a>
						</th>
					<?php endif;?>
<!--					<th>Profile</th>-->
				</tr>
			</thead>
			<tbody>
            	<tr id="pagination_row">
                	<td colspan="4"><?PHP echo $previousButton.' '.$inbetweenLinks.' '.$nextButton; ?></td>
					<?php if($settingsModel->getForCompany('email_extractor-enabled') && $settingsModel->getForCompany('email_extractor_visited-enabled')):?>
						<td class="email-capture">
							<button class="email-capture get-all-emails" data-campaign_id="<?=$active_campaign['id']?>" data-visited="1">Get all emails</button>
						</td>
					<?php endif;?>
              	</tr>
				<?PHP
                    $tz = new DateTimeZone('America/Los_Angeles');
                    $i = 0;
                    foreach ( $peoples as $people ){
                        $visited = new DateTime($people['sent_to_crawler']);
                        $visited->setTimezone($tz);

                        $ppl_email = $people['email'];
                        $ppl_linked_in_id = $people['linked_in_id'];
                        $ppl_basic_url = $basicProfileUrl;
				?>

						<tr>
							<td>
								<b><?=$people['name']?></b>
								<span class="table-tagline">
									<?=$people['title']?>
								</span>
							</td>
							<td>
								<b><?=$people['employer']?></b>
								<span class="table-tagline">
									<?=$people['industry']?>
								</span>
							</td>
							<td>
								<b><?=$people['location']?></b>
							</td>
							<td>
								<b><?=$visited->format('n/j/y')?></b><br/>
								<b><?=$visited->format('g:ia')?></b>
							</td>
							<td>
								<?php if($settingsModel->getForCompany('email_extractor-enabled')):?>
									<?php if(!empty($people['email'])):?>
										<b><?=$people['email']?></b>
									<?php else:?>
										<button class="email-capture get-email" data-people_id="<?=$people['id']?>">Get email</button>
									<?php endif;?>
								<?php endif;?>
							</td>
							<!--<td>-->
							<!--<a href="$ppl_basic_url$ppl_linked_in_id" target="_blank">Go To</a>-->
							<!--</td>-->
						</tr>

				<?php
                    }
				?>
                <tr id="pagination_row">
                	<td colspan="5"><?PHP echo $previousButton.' '.$inbetweenLinks.' '.$nextButton; ?></td>
              	</tr>
			</tbody>
		</table>
	</div>
</div>

<? } else { ?>
	<h1 style="font-size: 22px; margin-top: 8px;"> <- Create your first campaign</h1>
<? } ?>
