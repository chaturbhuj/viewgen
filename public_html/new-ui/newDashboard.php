<?PHP

if (!$_GET['report']) {
    header("Location: ?report=accounts&filter=recent");
}

require_once(__DIR__ . '/../db.php');
include_once('helper.php');

class Reports
{

    public function __construct($sql, $page, $limit = 100)
    {
        $limit = intval($limit);
        $this->page = intval($page);
        $this->sql = $sql . ' LIMIT ' . (string)$limit . ' OFFSET ' . (string)(($page - 1) * $limit);
    }

    private function create_csv($result)
    {

        $col_count = $result->columnCount();
        $header = [];
        for ($i = 0; $i < $col_count; $i++) {
            $col = $result->getColumnMeta($i);
            $header[] = $this->cleanLabel($col['name']);
        }
        $lines = [];
        while ($row = $result->fetch()) {
            $cols = array();
            for ($x = 0; $x < $col_count; $x++) {
                $cols[] = str_replace(array('<br>', '<br/>'), "\n", $row[$x]);
            }
            $lines[] = '"' . implode('","', $cols) . '"';
        }
        $head = '"' . implode('","', $header) . '"';
        $content = implode("\n", $lines);
        return $head . "\n" . $content;

    }

    public function output($parameters = array(), $download = false, $report_name = NULL)
    {

        //pass sql and sql params to output
        $result = $this->runSql($this->sql, $parameters);
        if ($download) {
            header('Content-Type: application/octet-stream');
            header("Content-Disposition: attachment; filename=$report_name.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            return $this->create_csv($result);
        }
        $count = $result->rowCount();
        $i = 0;
        $headers = [];
        while ($row = $result->fetch()) {
            //$row is one line from the queries result	
            $out[] = '<tr>';
            foreach ($row as $label => $value) {
                if (!is_numeric($label)) {
                    if (!($i > 0))
                        $headers[] = "<th class=\"$label\" data-field=\"$label\">" . $this->cleanLabel($label) . '</th>';
                    $out[] = "<td class=\"$label\"><pre>" . str_replace('\n', '<br />',htmlspecialchars( $value)) . "</pre></td>";
                }
            }
            $out[] = '</tr>';
            $i++;
        }

        $next = preg_replace(
            "/page=\d+/",
            'page=' . (string)($this->page + 1),
            $_SERVER['REQUEST_URI']
        );
        $prev = preg_replace(
            "/page=\d+/",
            'page=' . (string)($this->page === 1 ? 1 : ($this->page - 1)),
            $_SERVER['REQUEST_URI']
        );

        $paginationHTML = '<p>Page: ' .
            (string)$this->page . ' &nbsp <a href="' . $prev .
            '">Prev</a> <a href="' . $next . '">Next</a></p>';

        $serverFilteringButton =
            '<br /><button id="server_filtering_btn">Server query with filters</button> <button id="clean_filtering_btn">Clean</button>' .
            '<button id="sql_toggle_btn">Toggle SQL</button><br /><pre id="sql_preformatted" style="display:none">' . $this->sql . '</pre>';


        if (!empty($out))
            return '<p>Total Results: ' . $count . '</p>' . $paginationHTML . $serverFilteringButton . '<table id="report_table" class="tablesorter"><thead><tr>'
                . implode("\n", $headers) . '</tr></thead><tbody>' . implode("\n", $out) . '</tbody></table>';

        /* how do I tell if there was an error in SQL and report it? */
        return '<p>No data</p>' . $paginationHTML . $serverFilteringButton;

    }

    private function cleanLabel($str)
    {
        $parts = explode('__', $str);
        $s = count($parts) > 1 ? $parts[1] : $parts[0];
        return ucwords(str_replace('_', ' ', $s));

    }

    private function runSql($sql, $parameters)
    {

        //create db object and run query ( will only support strings in placeholders )
        $q = DB::prep($sql);
        $q->execute($parameters);
        //returning db object to fetch later
        return $q;

    }

}

$report = $_GET["report"];
$report_name = $_GET['report'];
$helper = new Helper;
$where = "WHERE 1=1 "; //just to have 'where' word in all cases 
$middle[] = "<div style=\"display:inline-block;\"><a href=\"/new-ui/newDashboardLanding.html\"><img src=\"http://viewgentools.com/static/images/ViewGenTools.png\"></a><h1 style=\"display:inline-block;\">$report</h1></div>";
$middle[] = "<div class=\"loading\"><p>Loading...</p><div class=\"spinner\"></div></div>";
if ($filter = $_GET['filter']) {
    unset($_GET['filter']);
    unset($_GET['report']);
    //everything left will be a parameter
    $filters = explode("-", $filter);
    //var_dump($filters);
    $filter_html = "<p style=\"margin:0;\">Filters: " . implode(" & ", $filters) . "</p>";

    $wheres = array();
    foreach ($filters as $f) {
        if (file_exists(ABSOLUTE_PATH . "sql/$report/$f.sql")) {
            $wheres[] = $helper->evaluate(ABSOLUTE_PATH . "sql/$report/$f.sql");
        } else if (file_exists(ABSOLUTE_PATH . "sql/common-filters/$f.sql")) {
            $wheres[] = $helper->evaluate(ABSOLUTE_PATH . "sql/common-filters/$f.sql");
        }
    }
    $where .= " AND " . implode(" AND ", $wheres);
    //make all searches work with LIKE
    if (in_array("search", $filters) && $_GET["query"]) {
        $_GET["query"] = '%' . $_GET["query"] . '%';
        $searchValue = trim($_GET["query"], '%');
        $targetFilter = $filter;
    } else {
        $targetFilter = "$filter-search";
    }
}

if (isset($_GET['fields'])) {
    $pairs = explode('|', urldecode($_GET['fields']));
    foreach ($pairs as $pair) {
        $pairArr = explode('==', $pair);
        $field_name = str_replace('__', '.', $pairArr[0]);
        if (isset($pairArr[1])) {
            $val = $pairArr[1];
            //Looking for not string type fields
            if (in_array($field_name, ['cl.id', 'cl.company_id', 'cl.parent_exception_id', 'c.is_test'])) {
                $where .= ' AND CONVERT(' . mysql_real_escape_string($field_name) .
                    ', CHAR) =' . intval(mysql_real_escape_string($val));
            } else {
                $where .= ' AND CONVERT(' . mysql_real_escape_string($field_name) .
                    ', CHAR) LIKE \'%' . mysql_real_escape_string($val) . '%\' ';
            }
        }
    }
}


$download = (isset($_GET['download']) && $_GET['download'] == 'true') ? true : false;
unset($_GET['download']);
$middle[] = $filter_html . '<br/>';
$middle[] = "<!--\n$sql\n-->";
$middle[] = "<form id=\"main_search\">
	<input type=\"hidden\" name=\"report\" value=\"$report\">
	<input type=\"hidden\" id=\"prev_filter\" value=\"$filter\">
	<input type=\"hidden\" name=\"filter\" value=\"search\">
	<input type=\"search\" id=\"query\" name=\"query\" placeholder=\"search...\" value=\"$searchValue\"/>
	<input type=\"hidden\" name=\"download\" value=\"false\">
	</form><button id=\"download\">Download CSV</button>\n\n";
$middle[] = '<br/>';

$sql = $helper->evaluate(ABSOLUTE_PATH . "sql/$report.sql", get_defined_vars());

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
unset($_GET['page']);

$report = new Reports($sql, $page);

if ($download) {
    echo $report->output($_GET, $download, $report_name);
    return;
}
$middle[] = $report->output($_GET, $download, $report_name);
$middle_content = implode('', $middle);

?><!DOCTYPE html>
<html>
<head>
    
    <?php include(__DIR__ . '/../ui/include/favicon.php'); ?>
    
    <style>
        table {
            border: none;
            border-collapse: collapse;
        }

        table tr:nth-child( odd ) {
            background-color: #ddd;
        }

        table tr th {
            font-weight: bold;
            font-size: 14px !important;
        }

        table tr th, table tr td {
            width: auto;
            height: 40px;
            line-height: 40px;
            border: none;
            padding: 0 10px;
            box-sizing: border-box;
            max-width: 250px;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        table#report_table tr.flag td {
            background-color: yellow !important;
        }

        div.loading {
            display: block;
            padding-bottom: 15px;
            font-size: 18px;
            font-style: italic;
        }

        div.loading div.spinner {
            background: url(//viewgentools.com/assets/images/small_spinner.gif) no-repeat;
            height: 15px;
            width: 50px;
            display: inline-block;
        }

        div.loading p {
            display: inline-block;
            padding-right: 10px;
        }

        form#main_search {
            display: inline-block;
            width: auto;
            vertical-align: top;
            height: auto;
        }

        button#download {
            display: inline-block;
            vertical-align: top;
            width: auto;
            height: auto;
            margin-left: 10px;
        }

        td.Contact {
            line-height: 20px;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"
            type="application/javascript"></script>
    <script type="application/javascript" src="/assets/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/assets/js/tableSort/theme.blue.css">
    <script type="application/javascript"
            src="/assets/js/tableSort/jquery.tablesorter.combined.js"></script>
    <script type="application/javascript"
            src="/assets/js/utilities.js"></script>
    <link rel="stylesheet" href="/assets/vendor/fontawesome/css/font-awesome.min.css" />

    <script>

        $(document).ready(function () {
            $('input#main_search').submit(function () {
                alert('Hi');
            });
            $('button#download').click(function () {
                $('form#main_search').children('input[name="download"]').val('true');
                var prev_filter = $('form#main_search').children('input#prev_filter').val();
                $('form#main_search').children('input[name="filter"]').val(prev_filter);
                $('form#main_search').submit();
                $('form#main_search').children('input[name="filter"]').val('search');
                $('form#main_search').children('input[name="download"]').val('false');
            });

            $('#sql_toggle_btn').on('click', function () {
                $('#sql_preformatted').toggle();
            });


            // call the tablesorter plugin
            var $reportTable = $("#report_table");

//                    .on('filterEnd', function (e, c) {
//                        // prevent ajax spamming
//                        var ls = c.$table.data('lastSearch');
//                        if (lastSearch.join('-') !== ls.join('-') ) {
//                            lastSearch = ls;
//                            updateAjax(lastSearch);
//                        }
//                    })
            $reportTable.tablesorter({
                theme: 'blue',

                // hidden filter input/selects will resize the columns, so try to minimize the change
                widthFixed: true,

                // initialize zebra striping and filter widgets
                widgets: ["zebra", "filter", "stickyHeaders"],

                ignoreCase: false,

                widgetOptions: {

                    // filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option

                    // If there are child rows in the table (rows with class name from "cssChildRow" option)
                    // and this option is true and a match is found anywhere in the child row, then it will make that row
                    // visible; default is false
                    filter_childRows: false,

                    // if true, filter child row content by column; filter_childRows must also be true
                    filter_childByColumn: false,

                    // if true, include matching child row siblings
                    filter_childWithSibs: true,

                    // if true, a filter will be added to the top of each table column;
                    // disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
                    // if you set this to false, make sure you perform a search using the second method below
                    filter_columnFilters: true,

                    // if true, allows using "#:{query}" in AnyMatch searches (column:query; added v2.20.0)
                    filter_columnAnyMatch: true,

                    // extra css class name (string or array) added to the filter element (input or select)
                    filter_cellFilter: '',

                    // extra css class name(s) applied to the table row containing the filters & the inputs within that row
                    // this option can either be a string (class applied to all filters) or an array (class applied to indexed filter)
                    filter_cssFilter: '', // or []

                    // add a default column filter type "~{query}" to make fuzzy searches default;
                    // "{q1} AND {q2}" to make all searches use a logical AND.
                    filter_defaultFilter: {},

                    // filters to exclude, per column
                    filter_excludeFilter: {},

                    // jQuery selector (or object) pointing to an input to be used to match the contents of any column
                    // please refer to the filter-any-match demo for limitations - new in v2.15
                    filter_external: '',

                    // class added to filtered rows (rows that are not showing); needed by pager plugin
                    filter_filteredRow: 'filtered',

                    // add custom filter elements to the filter row
                    // see the filter formatter demos for more specifics
                    filter_formatter: null,

                    // add custom filter functions using this option
                    // see the filter widget custom demo for more specifics on how to use this option
                    filter_functions: null,

                    // hide filter row when table is empty
                    filter_hideEmpty: true,

                    // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
                    // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
                    // in v2.26.6, this option will also accept a function
                    filter_hideFilters: false,

                    // Set this option to false to make the searches case sensitive
                    filter_ignoreCase: true,

                    // if true, search column content while the user types (with a delay).
                    // In v2.27.3, this option can contain an
                    // object with column indexes or classnames; "fallback" is used
                    // for undefined columns
                    filter_liveSearch: true,

                    // global query settings ('exact' or 'match'); overridden by "filter-match" or "filter-exact" class
                    filter_matchType: {'input': 'exact', 'select': 'exact'},

                    // a header with a select dropdown & this class name will only show available (visible) options within that drop down.
                    filter_onlyAvail: 'filter-onlyAvail',

                    // default placeholder text (overridden by any header "data-placeholder" setting)
                    filter_placeholder: {search: '', select: ''},

                    // jQuery selector string of an element used to reset the filters
                    filter_reset: 'button.reset',

                    // Reset filter input when the user presses escape - normalized across browsers
                    filter_resetOnEsc: true,

                    // Use the $.tablesorter.storage utility to save the most recent filters (default setting is false)
                    filter_saveFilters: false,

                    // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                    // every character while typing and should make searching large tables faster.
                    filter_searchDelay: 300,

                    // allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
                    filter_searchFiltered: true,

                    // include a function to return an array of values to be added to the column filter select
                    filter_selectSource: null,

                    // if true, server-side filtering should be performed because client-side filtering will be disabled, but
                    // the ui and events will still be used.
                    filter_serversideFiltering: false,

                    // Set this option to true to use the filter to find text from the start of the column
                    // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                    filter_startsWith: false,

                    // Filter using parsed content for ALL columns
                    // be careful on using this on date columns as the date is parsed and stored as time in seconds
                    filter_useParsedData: false,

                    // data attribute in the header cell that contains the default filter value
                    filter_defaultAttrib: 'data-value',

                    // filter_selectSource array text left of the separator is added to the option value, right into the option text
                    filter_selectSourceSeparator: '|'

                }

            });

            var fields = URL_PARAMS['fields'];
            if (fields){
                _.each(decodeURIComponent(fields).split('|'), function (pair) {
                    var pairArr = pair.split('==');
                    var col = $reportTable.find('th[data-field="' + pairArr[0] + '"]').data('column');
                    $reportTable.find('input.tablesorter-filter[data-column="' + col + '"]').val(pairArr[1]);
                });
            }
            $('#server_filtering_btn').on('click', function () {
                var fields = [];
                var failure = false;
                $reportTable.find('input.tablesorter-filter').each(function () {
                    var $t = $(this), val = $t.val().trim();
                    if (val) {
                        var fieldName = $reportTable.find('th[data-column="' + $t.data('column') + '"]').data('field').trim();
                        if (/^\w+__\w+$/.test(fieldName)) {
                            //only valid column names
                            fields.push(fieldName + '==' + val);
                        } else {
                            //mark cell as invalid filter
                            $t.css('background-color', '#ed9583');
                            failure = true;
                        }
                    }
                });

                var s = fields.join('|');

                if (! failure && s){
                    window.location.href = window.location.href.replace(/&fields=.*$/, '')
                        + '&fields=' + encodeURIComponent(s);
                }
            });

            $('#clean_filtering_btn').on('click', function () {
                $reportTable.find('input.tablesorter-filter').val('').css('background-color', 'white');
                window.location.href = window.location.href.replace(/&fields=.*$/, '');
            });

            // Clear stored filters - added v2.25.6
            $('.resetsaved').click(function () {
                $('#table').trigger('filterResetSaved');

                // show quick popup to indicate something happened
                var $message = $('<span class="results"> Reset</span>').insertAfter(this);
                setTimeout(function () {
                    $message.remove();
                }, 500);
                return false;
            });

            // External search
            // buttons set up like this:
            // <button type="button" data-filter-column="4" data-filter-text="2?%">Saved Search</button>
            $('button[data-filter-column]').click(function () {
                /*** first method *** data-filter-column="1" data-filter-text="!son"
                 add search value to Discount column (zero based index) input */
                var filters = [],
                    $t = $(this),
                    col = $t.data('filter-column'), // zero-based index
                    txt = $t.data('filter-text') || $t.text(); // text to add to filter

                filters[col] = txt;
                // using "table.hasFilters" here to make sure we aren't targeting a sticky header
                $.tablesorter.setFilters($('#table'), filters, true); // new v2.9

                /** old method (prior to tablsorter v2.9 ***
                 var filters = $('table.tablesorter').find('input.tablesorter-filter');
                 filters.val(''); // clear all filters
                 filters.eq(col).val(txt).trigger('search', false);
                 ******/

                /*** second method ***
                 this method bypasses the filter inputs, so the "filter_columnFilters"
                 option can be set to false (no column filters showing)
                 ******/
                /*
                 var columns = [];
                 columns[5] = '2?%'; // or define the array this way [ '', '', '', '', '', '2?%' ]
                 $('table').trigger('search', [ columns ]);
                 */

                return false;
            });

            $("div.loading").hide();
        });
    </script>


    <?PHP
    echo "<script src=\"sql/$report_name.js\"></script>\n";
    if (!file_exists("sql/$report_name/search.sql")) {
        echo "<style>#query{display:none;}</style>";
    }
    ?>

    <?PHP
    echo "<title>$report_name</title>\n";
    ?>

</head>
<body>


<a href="?report=accounts&page=1&filter=recent-unfrozen">Accounts</a> |
<a href="?report=users&page=1&filter=recent">Users</a> |
<a href="?report=campaign_list&page=1&filter=recent">Campaigns</a> |
<a href="?report=subscriptions&page=1&filter=default">Subscriptions</a> |
<p style="display: inline;">Red Flag Reports:</p>
<a href="?report=campaign_too_few_matches&page=1" target="_blank">Too Few Matches</a> |
<a href="?report=campaign_stopped_early&page=1" target="_blank">Stopped Early</a> |
<a href="?report=campaigns_no_url&page=1" target="_blank">No URL</a> |
<a href="?report=campaigns_no_visits&page=1" target="_blank">No Visits</a> |
<a href="?report=account_no_active_campaigns&page=1" target="_blank">No Active Campaigns</a> |
<a href="?report=account_no_li_users&page=1" target="_blank">No LinkedIn Users</a> |
<a href="?report=account_client_signed_out&page=1" target="_blank">Client Signed Out</a> |
<a href="?report=users&filter=recent-missing_sales_navigator&page=1" target="_blank">No Sales Nav</a> |
<a href="?report=stalled_campaigns&page=1" target="_blank">Stalled Campaigns</a> |
<a href="?report=wrong_url&page=1" target="_blank">Wrong URL Format</a> |
<a href="?report=users&filter=private&page=1" target="_blank">Private/Anonymous</a> |
<a href="?report=crashes&page=1" target="_blank">Crashes</a> |
<a href="?report=api_errors&page=1" target="_blank">API Errors</a> |

<hr>

<?= $middle_content ?>

</body>
</html>
