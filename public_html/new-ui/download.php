<?php

$HTML_TITLE = "Download";
$ACTIVE_HEADER_PAGE = "download";

require_once('../authenticate.php');

if (!authenticate()) {
	header('Location: /');
	die;
}

//if (!isset($_GET['company_id'])) {
//	if(isset($_USER_DATA["admin"])){
//		redirect("/adminDashboard.php");
//	}
//	die;
//}

$url = "";
if (get_os() == "mac") {
	$url = base_url('/client/download.php?company_id=' . $_COMPANY_DATA['id'] . '&type=mac');
	$msg = "Download Mac OS X Installer " . get_mac_client_size(" · ");
} else {
	$url = base_url('/client/download.php?company_id=' . $_COMPANY_DATA['id'] . '&type=windows');
    $msg = "Download Windows Installer " . get_windows_client_size(" · ");
}


include(__DIR__ . '/header.php');

?>

<style>
#wait_for_li_user{
    text-align: center;
}

.my-spinner {
	width: 60px;
	height: 60px;
	background: url('/assets/images/ring.gif');
	background-size: cover;
	margin: 8px auto;
	display: block;
}
</style>

<div class="new_container">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="download-box" style="cursor: pointer">
    			<a href="<?= $url?>"><?= $msg ?></a>
			</div>
            <h5 id="wait_for_li_user"><div class="my-spinner"></div></h5>
		</div>
	</div>
</div>
</div>

<?php include(__DIR__ . '/footer.php'); ?>

<script>
$(function() {
    function ticker() {
        $.ajax({
            'url': '/new-ui/ajax.php?company_id=<?=$_COMPANY_DATA['id']?>',
            'type': 'post',
            'data': {'action': 'checkForUser'},
            'success': function (response) {
                if (response == 1) {
                    $('#wait_for_li_user').html(
                        "<p>Congratulations! First LinkedIn user has been registered in your Desktop Client!</p>" +
                        "<p>You'll be moved to Campaign Builder in few seconds...</p>");
                    setTimeout(function () {
                        document.location.href = '/new-ui/campaign_builder.php?company_id=<?=$_COMPANY_DATA['id']?>';
                    }, 6000);
                    
                }else{
                    $('#wait_for_li_user').html("<br><p>Download Desktop Client and login under a LinkedIn user in it. Waiting...</p><div class=\"my-spinner\"></div>");
                }
            }
        });
    }
    
    ticker();
    setInterval(ticker, 5000);
});


</script>

