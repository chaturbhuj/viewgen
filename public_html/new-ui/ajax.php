<?php

require_once('../authenticate.php');
require_once(__DIR__ . '/../services/li_account_type_svc.php');
use function services\li_account_type\prepareLIUrls;
use function services\li_account_type\checkLIAccount;

require_once(__DIR__ . '/../services/utilities_svc.php');
use function services\utilities_svc\encodeSearchUrl;

require_once(__DIR__ . '/../db.php');


if (!isset($_GET['company_id'])) {
	die;
}

if (($_POST['action'] ?? '') == 'checkForUser') {
	$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id AND hidden = 0 LIMIT 1");
	$query->execute([
		'company_id' => $_GET['company_id']
	]);

	if ($query->rowCount() == 0) {
		echo 0;
	} else {
		echo 1;
	}
	die;
}

if (($_POST['action'] ?? '') == 'checkUrls') {

    $query = DB::prep("SELECT * FROM li_users WHERE id = ? AND company_id = ?");
    $query->execute(array($_GET['user_id'], $_GET['company_id']));
    
    if ($query->rowCount() == 0) {
        echo "Invalid user\n";
        die;
    }
    $li_user = $query->fetch();
    
    $isSimpleSearch = checkLIAccount($li_user) === 'simple';
    
	$query_urls = explode("\n", trim(str_replace("\r", "", $_POST['query_urls'])));
	$query_urls = array_reverse($query_urls);

    $result = prepareLIUrls($query_urls, $isSimpleSearch);
    $error = $result['error'];
    

	if ($error) {
        $error_url = encodeSearchUrl($result['error_url']);
        if ($error === 'bad_format'){
            echo "You are trying to add an invalid URL $error_url <br> ".
                      'All URLs should begin like this:<br>https://www.linkedin.com/sales/search?... (Sales Navigator) or'.
                      '<br>https://www.linkedin.com/search/people?... (Premium/Free accounts)';
        }elseif ($error === 'contains_page_num'){
            echo "Your search URL $error_url <br/> paginated and first page cannot be found. <br/> ".
                'Please resubmit search and copy its URL here without pagination <br> ';
        }elseif ($error === 'sales_nav_not_capable'){
            echo "You are trying to save this Sales Navigator URL $error_url <br/> while your current LinkedIn account is NOT Sales Navigator <br/>".
                'Either resubmit search under this account and copy URL, or switch to Sales Navigator profile here';
        }else {
            echo "Unknown error during save for this URL: $error_url";
        }
	}
	die;
}

if (($_POST['action'] ?? '') == "updateCampaignOrder") {

	$order = $_POST['order'];
	$priority = [];
	if (count($order) > 0) $priority[$order[0]] = 3;
	if (count($order) > 1) $priority[$order[1]] = 2;
	for ($i = 2; $i < count($order); $i++) {
		$priority[$order[$i]] = 1;
	}

	$i = 0;
	foreach ($priority as $campaign_id => $prio) {
		$query = DB::prep("UPDATE campaign SET priority = :priority, secondary_priority = :secondary WHERE
			id = :campaign_id AND company_id = :company_id");
		$query->execute([
			'priority' => $prio,
			'secondary' => count($priority) - $i,
			'campaign_id' => $campaign_id,
			'company_id' => $_GET['company_id']
		]);

		$i++;
	}
	die;

}

if (($_POST['action'] ?? '') == "archiveCampaign") {

	

	die;

}

?>
