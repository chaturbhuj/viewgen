<?php

$query = DB::prep("SELECT * FROM search_url WHERE campaign_id = :campaign_id AND parent_search_url_id > 0");
$query->execute(['campaign_id' => $active_campaign['id']]);
if ($query->rowCount() > 0) {

	$query = DB::prep("SELECT * FROM search_url WHERE campaign_id = :campaign_id");
	$query->execute(['campaign_id' => $active_campaign['id']]);

	$original_query_size = 0;
	$split_query_size = 0;
	$original_visits = 0;
	while ($search_url = $query->fetch()) {
		if ($search_url['total_matches'] < 0) continue;
		if ($search_url['parent_search_url_id'] == 0) {
			$original_query_size += $search_url['total_matches'];
			$original_visits += ($search_url['total_matches'] > 1100) ? 1100 : $search_url['total_matches'];
		}
		
		if ($search_url['paused'] == 0) {
			$split_query_size += ($search_url['total_matches'] > 1100) ? 1100 : $search_url['total_matches'];
		}
	}

	$autoSlicerIncrease = '0.00';
	if ($original_visits < $split_query_size) {
		$autoSlicerIncrease = number_format($original_visits > 0 ? (100*($split_query_size - $original_visits) / $original_visits) : 0, 0);
	}
?>

	<div class="col-md-12">
		<span class="chart-title">AUTO SLICER</span>

		<div class="campaign-info-box">
			<div class="row">
				<div class="col-md-12">
					<span class="campaign-info-box-main-title">PERFORMANCE ANALYSIS</span>
				</div>
				<div class="col-md-3">
					<span class="campaign-info-box-title">Original Query Size</span>
					<span class="campaign-info-box-value"><?=number_format($original_query_size)?></span>
				</div>
				<div class="col-md-3">
					<span class="campaign-info-box-title">Split Query Size</span>
					<span class="campaign-info-box-value"><?=number_format($split_query_size)?></span>
				</div>
				<div class="col-md-3">
					<span class="campaign-info-box-title">Auto Slicer Coverage</span>
					<span class="campaign-info-box-value"><?=number_format($original_query_size > 0 ? (100*$split_query_size / $original_query_size) : 0, 2)?>%</span>
				</div>
				<div class="col-md-3">
					<span class="campaign-info-box-title">Auto Slicer Increase</span>
					<span class="campaign-info-box-value">+<?=$autoSlicerIncrease?>%</span>
				</div>
			</div>
		</div>
	</div>
<?
}
?>
