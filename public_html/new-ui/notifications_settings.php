<?php
GLOBAL $_COMPANY_DATA;

$HTML_TITLE = "Notifications settings";
$ACTIVE_HEADER_PAGE = "Notifications settings";

require_once('../authenticate.php');

if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("/adminDashboard.php");
	}
	die;
}

$company_id = $_COMPANY_DATA['id'];
$query = DB::prep("SELECT unsubscribeType, 0 AS hasUnsubscribed FROM emailTemplate WHERE unsubscribeType != '' GROUP BY unsubscribeType");
$query->execute();
$types = $query->fetchAll();

if (isset($_POST['action']) && $_POST['action'] == 'save') {
    foreach ($types as $type) {
        if (!isset($_POST[str_replace(" ", "_", $type['unsubscribeType'])])) {
            $query = DB::prep("INSERT IGNORE INTO emailUnsubscribed (company_id, unsubscribeType) VALUES(:company_id,
				:unsubscribeType)");
            $query->execute([
                'company_id' => $company_id,
                'unsubscribeType' => $type['unsubscribeType']
            ]);
        } else {
            $query = DB::prep("DELETE FROM emailUnsubscribed WHERE company_id = :company_id AND
				unsubscribeType = :unsubscribeType");
            $query->execute([
                'company_id' => $company_id,
                'unsubscribeType' => $type['unsubscribeType']
            ]);
        }
    }
}

$query = DB::prep("SELECT * FROM emailUnsubscribed WHERE company_id = :company_id");
$query->execute([
    'company_id' => $company_id
]);
$unsubscribed = $query->fetchAll();

foreach ($types as $i => $type) {
    foreach ($unsubscribed as $u) {
        if ($u['unsubscribeType'] == $type['unsubscribeType']) {
            $types[$i]['hasUnsubscribed'] = true;
        }
    }
}

include('header.php');

?>
<div class="new_container">
    <div class="container" id="campaign_builder" data-company-id="<?=$_GET['company_id']?>"
        data-user-id="<?=$_GET['user_id']?>">
        <div class="row">
            <div class="col-md-12">
                <div class="download-box" style="height: auto;">
                    <form method="post" action="/new-ui/notifications_settings.php?company_id=<?=$_GET['company_id']?>">
                        <? foreach ($types as $type) { ?>
                            <div class="form-group">
                                <label><?=$type['unsubscribeType']?></label>
                                <input class="form-control" type="checkbox" name="<?=$type['unsubscribeType']?>"
                                    <?=!$type['hasUnsubscribed'] ? ' checked="checked"' : ''?>>
                            </div>
                        <? } ?>
                        <input type="submit" class="btn btn-info" value="Save">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<? include('footer.php'); ?>
