<?php
if (isset($active_campaign)) {
	global $DEFERRED_ASSETS;
	$DEFERRED_ASSETS[] = '/assets/js/dropit/dropit.js'
?>

<div class="row">
	<div class="col-md-12">
		<a href="/visitedProfiles.php?company_id=<?=$_GET['company_id']?>&campaign_id=<?=$active_campaign['id']
			?>&order_by=&direction=&days=0&start_date=&end_date=&export=true"
			class="float-right export-as-csv">Export Campaign Details as CSV</a>
	</div>
	<div class="col-md-12">
		<span class="chart-title">
			KEY PERFORMANCE INDICATORS &nbsp;
			<span class="float-right">
				<a class="archive-campaign" href="/new-ui/actions/archive_campaign.php?company_id=<?=$_GET['company_id']?>&campaign_id=<?=$active_campaign['id']?>&user_id=<?=$_GET['user_id']?>">Archive Campaign</a>
				&nbsp; - &nbsp;
				<? if ($active_campaign['status'] == 'Paused') { ?>
					<a class="archive-campaign" href="/new-ui/actions/activate_campaign.php?company_id=<?=$_GET['company_id']?>&campaign_id=<?=$active_campaign['id']?>&user_id=<?=$_GET['user_id']?>">Activate Campaign</a>
				<? } else { ?>
					<a class="archive-campaign" href="/new-ui/actions/pause_campaign.php?company_id=<?=$_GET['company_id']?>&campaign_id=<?=$active_campaign['id']?>&user_id=<?=$_GET['user_id']?>">Pause Campaign</a>
				<? } ?>
			</span>
		</span>
		<div class="campaign-info-box">
			<div class="row">
<!--                <div class="col-md-3">-->
<!--                    <span class="campaign-info-box-main-title">PRIORITY</span>-->
<!---->
<!--                    <span class="campaign-info-box-title">Campaign Priority</span>-->
<!--                    <span class="campaign-info-box-value" data-dynamic="campaign_priority_--><?//=$active_campaign['id']?><!--">-->
<!--                        --><?//=$PRIORITY_MAP[$active_campaign['priority']]?>
<!--                    </span>-->
<!--                </div>-->
				<div class="col-md-3">
					<span class="campaign-info-box-main-title">OUTBOUND ACTIVITY</span>

					<span class="campaign-info-box-title">Total Profile Visits</span>
					<span class="campaign-info-box-value" data-dynamic="campaign_visits_<?=$active_campaign['id']?>">
						<?=number_format($active_campaign['campaign_visit_count'])?>
					</span>
				</div>
                <div class="col-md-3">
                    <span class="campaign-info-box-main-title">VBR</span>

                    <span class="campaign-info-box-title">View Back Rate:</span>
                    <span class="campaign-info-box-value" data-dynamic="campaign_vbr_<?=$active_campaign['id']?>">
                        <?=$active_campaign['campaign_visit_count'] < 50 ? '-' : number_format(($active_campaign['total_visitors']/$active_campaign['campaign_visit_count'])*100,1) . "%"?>
                    </span>
                </div>
				<div class="col-md-3">
					<span class="campaign-info-box-main-title">Original URL</span>

					<span class="campaign-info-box-title"></span>
					<span class="campaign-info-box-value">
						<?php if($active_campaign['original_url']):?>
							<?php
								$original_urls = explode('|', $active_campaign['original_url']);
                                $original_urls_count = count($original_urls);
                                if($original_urls_count > 1):
							?>

							<link href="/assets/js/dropit/dropit.css" rel="stylesheet" type="text/css">
							<style>
								ul.dropit-submenu {
									background-color: #fff;
									border: 1px solid #b2b2b2;
									padding: 6px 0;
									margin: 3px 0 0 1px;
									-webkit-border-radius: 3px;
									-moz-border-radius: 3px;
									border-radius: 3px;
									-webkit-box-shadow: 0px 1px 3px rgba(0,0,0,0.15);
									-moz-box-shadow: 0px 1px 3px rgba(0,0,0,0.15);
									box-shadow: 0px 1px 3px rgba(0,0,0,0.15);

								}
								ul.dropit-submenu a {
									display: block;
									text-align: center;
								}

								ul.dropit-submenu li:hover {
									background-color: #90a0b3;
								}
							</style>
							<ul class="menu dropdown_menu">
								<li>
									<a href="#">View on LinkedIn (<?=$original_urls_count?>)</a>
									<ul style="min-width: 110px">
										<?php foreach ($original_urls as $index => $original_url): ?>
										<li><a target="_blank" href="<?=$original_url?>" title="<?=$original_url?>"><?=$index+1?></a></li>
										<?php endforeach; ?>
									</ul>
								</li>
							</ul>
                            <?php else:?>
                                <a target="_blank" href="<?=$original_urls[0]?>" title="<?=$original_urls[0]?>">View on LinkedIn</a>
                            <?php endif;?>
						<?php else:?>
							<a data-toggle="modal" href="" data-target="#buildCampaignModal" onclick="useDefaultNewCompaignbuilderData = false;">Campaign builder</a>
						<?php endif;?>
                    </span>
				</div>
				<div class="col-md-3">
					<span class="campaign-info-box-title">Created:</span>
					<span class="campaign-info-box-value">
                        <?=$active_campaign['created']?>
                    </span>
					<span class="campaign-info-box-title">Start date:</span>
					<span class="campaign-info-box-value">
                        <?=$active_campaign['start_date']?>
                    </span>
					<?php if($active_campaign['status'] === "Finished"):?>
						<span class="campaign-info-box-title">Completed:</span>
						<span class="campaign-info-box-value">
							<?=$active_campaign['campaign_completed']?>
						</span>
					<?php endif;?>
				</div>
			</div>

			<?/*
			<span class="campaign-info-box-title">Unique Profiles Visited</span>
			<span class="campaign-info-box-value">
				9,293
			</span>

			<span class="campaign-info-box-title">Percent Profiles Revisited</span>
			<span class="campaign-info-box-value">
				5.2%
			</span>

			<span class="campaign-info-box-main-title">INBOUND VIEWS GENERATED</span>

			<span class="campaign-info-box-title">Inbound Profile Views</span>
			<span class="campaign-info-box-value">
				821
			</span>

			<span class="campaign-info-box-title">Inbound Connection Requests</span>
			<span class="campaign-info-box-value">
				216
			</span>

			<span class="campaign-info-box-title">Inbound Messages</span>
			<span class="campaign-info-box-value">
				216
			</span>

			<span class="campaign-info-box-main-title">RANKING</span>

			<span class="campaign-info-box-title">Inbound View Back Percentage</span>
			<span class="campaign-info-box-value">
				56,362
			</span>

			<span class="campaign-info-box-title">Average Daily Views</span>
			<span class="campaign-info-box-value">
				56,362
			</span>
			*/?>
		</div>
	</div>
	<? include('auto_slicer_info.php'); ?>
	<div class="col-md-12">
		<span class="chart-title">QUERY URLs</span>
		<? include('query_list.php'); ?>
	</div>
	<?/*
	<div class="col-md-4">
		<span class="campaign-box-header">QUERY PARAMETERS</span>
		<div class="campaign-info-box">
			<span class="campaign-info-box-title">Geography</span>
			<span class="campaign-info-box-value">
				San Francisco Bay Area<br/>
				Los Angeles Area
			</span>

			<span class="campaign-info-box-title">Relationship</span>
			<span class="campaign-info-box-value">
				2nd Connections<br/>
				Groups
			</span>

			<span class="campaign-info-box-title">Title</span>
			<span class="campaign-info-box-value">
				Engineer<br/>
				Product
			</span>

			<span class="campaign-info-box-title">URLS</span>
			<span class="campaign-info-box-value">
				San Francisco Bay Area<br/>
				Los Angeles Area
			</span>

			<span class="campaign-info-box-title">Seniority Level</span>
			<span class="campaign-info-box-value">
				C-Level<br/>
				VP<br/>
				Director
			</span>

			<span class="campaign-info-box-title">Industry</span>
			<span class="campaign-info-box-value">
				Internet<br/>
				Software / Hardware
			</span>

			<span class="campaign-info-box-title">Company Size</span>
			<span class="campaign-info-box-value">
				1-5<br/>
				6-10<br/>
				11-50<br/>
				51-100
			</span>
		</div>
	</div>
	*/?>
</div>
<? } else { ?>
	<h1 style="font-size: 22px; margin-top: 8px;"> <- Create your first campaign</h1>
<? } ?>
