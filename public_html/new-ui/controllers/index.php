<?php
global $_USER_DATA;
/**
 * User: mihael
 * Date: 07.06.17
 * Time: 13:00
 */
require_once 'HTTPException.php';

$controllersPath = __DIR__;

if(!isset($_USER_DATA['admin']) || $_USER_DATA['admin'] != 1){
    sendError(403);
}

if(!isset($_GET['controller']) || !isset($_GET['action'])){
    sendError(404);
}

$controllerName = ucfirst($_GET['controller']).'Controller';
$controllerPath = $controllersPath.'/'.$controllerName.'.php';
$action = $_GET['action'].'Action';

if(!file_exists($controllerPath)){
    sendError(404);
}

include_once ($controllerPath);

$controller = NEW $controllerName();

if(!method_exists($controller, $action)){
    sendError(404);
}

try{
    $result = $controller->$action();
    sendSuccess($result);
} catch(HttpException $e){
    sendError($e->statusCode,$e->getMessage());
} catch (Exception $e){
    sendError();
}

function sendSuccess($data){
    http_response_code(200);
    header('Content-Type: application/json');
    echo json_encode($data);
    die;
}

function sendError($code = 500, $message = ''){
    http_response_code($code);
    echo $message;
    die;
}