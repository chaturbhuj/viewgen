<?php
require_once 'BaseController.php';

require_once __DIR__.'/../../api/functions.php';
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 07.06.17
 * Time: 15:18
 */
class CompaniesController extends BaseController
{
    public function syncRecurlyAction(){
        if($this->isPostRequest()){
            $companyId = $this->params['id'];
            $query = DB::prep("UPDATE company SET recurly_sync = 1 WHERE id = :company_id");
            $query->execute([
                'company_id' => $companyId
            ]);
            usersCountUpdate($companyId);
            return true;
        } else {
            throw NEW HttpException(404);
        }
    }

    public function recurlySyncOnAction(){
        if($this->isPostRequest()){
            $companyId = $this->params['id'];
            $query = DB::prep("UPDATE company SET recurly_sync = 1 WHERE id = :company_id");
            $query->execute([
                'company_id' => $companyId
            ]);
            return true;
        } else {
            throw NEW HttpException(404);
        }
    }

    public function recurlySyncOffAction(){
        if($this->isPostRequest()){
            $companyId = $this->params['id'];
            $query = DB::prep("UPDATE company SET recurly_sync = 0 WHERE id = :company_id");
            $query->execute([
                'company_id' => $companyId
            ]);
            return true;
        } else {
            throw NEW HttpException(404);
        }
    }

}