<?php
/**
 * User: mihael
 * Date: 06.06.17
 * Time: 17:21
 */

if (!function_exists('getallheaders'))  {
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

class BaseController
{
    public $requestMethod;
    public $params;

    public function __construct(){
        $requestHeaders = array();
        foreach (getallheaders() as $key => $val) {
            $requestHeaders[strtolower($key)] = strtolower($val);
        }

        $getParams = $_GET?:[];
        $postParams = $_POST?:[];
        $jsonParams = [];
        if (isset($requestHeaders['content-type']) && strpos($requestHeaders['content-type'], 'application/json') !== false) {
            $params = json_decode(file_get_contents('php://input'), true);
            if (!is_null($params)) {
                $jsonParams = $params;
            }
        }
        $this->params = array_merge($getParams,$postParams,$jsonParams);

        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }

    /**
     * whether the current request is an AJAX (XMLHttpRequest) request.
     * @return boolean whether the current request is an AJAX request.
     */
    public function isAjaxRequest()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
    }

    public function isPostRequest()
    {
        return $this->requestMethod === 'POST';
    }

}