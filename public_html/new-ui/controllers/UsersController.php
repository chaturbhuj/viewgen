<?php
require_once 'BaseController.php';
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 07.06.17
 * Time: 15:18
 */
class UsersController extends BaseController
{
    public function deleteAction(){
        if($this->isPostRequest()){
            $query = DB::prep("UPDATE li_users SET hidden = 1 WHERE id = :user_id AND company_id = :company_id");
            $query->execute([
                'user_id' => $this->params['id'],
                'company_id' => $this->params['companyId']
            ]);
            return true;
        } else {
            throw NEW HttpException(404);
        }
    }

    public function restoreAction(){
        if($this->isPostRequest()){
            $query = DB::prep("UPDATE li_users SET hidden = 0 WHERE id = :user_id AND company_id = :company_id");
            $query->execute([
                'user_id' => $this->params['id'],
                'company_id' => $this->params['companyId']
            ]);
            return true;
        } else {
            throw NEW HttpException(404);
        }
    }
}