<?

// Fetch urls
$query = DB::prep("SELECT *,search_url_visit_count AS visits, DATE_FORMAT(search_url_last_visit,'%b %d %Y') as last_visit FROM search_url WHERE campaign_id = ? AND paused = 0 ORDER BY search_url.finished ASC, search_url.total_matches ASC");
$query->execute(array($active_campaign['id']));

$queries = $query->fetchAll();

?>
<table class="table viewgen-table query-list">
	<tr>
        <th>Status</th>
		<? if(isset($_GET["show_full_urls"])){ ?>
			<th>URL &nbsp; <a class="show-full-urls" href="<?=base_url("/new-ui/campaign_builder.php?campaign_id=".$active_campaign['id']."&company_id=$_GET[company_id]&user_id=$_GET[user_id]")?>">Truncate URLs</a></th>
		<?}else{?>
			<th>URL &nbsp; <a class="show-full-urls" href="<?=base_url("/new-ui/campaign_builder.php?campaign_id=".$active_campaign['id']."&company_id=$_GET[company_id]&user_id=$_GET[user_id]&show_full_urls=true")?>">Show full URLs</a></th>
		<?}?>
		
		<? /*<th>URL</th>*/?>
		
<!--        <th>Priority</th> --- TODO may be per url it makes sense also... -->
		<th>Visits</th>
		<th>Skipped</th>
		<th>Private</th>
		<th>Connected</th>
		<th>Duplicated</th>
		<th>Visited</th>
<!--        <th>VBR</th>-->
		<th>Last Visit</th>
		<?/*<th class="manage">Manage</th>*/?>
	</tr>
	<?php
	$containActiveUrls = false;
	foreach ($queries as $query) { 
		if(!$query['finished']) {
			$containActiveUrls = true;
		}
		/*$urlParts = explode("?",$query['url']);
		if (count($urlParts) == 1) {
			$urlParts[] = "";
		}
		$parts = explode("&",str_replace("%2C",",",$urlParts[1]));
		$relationships = array("All");
		$keywords = array();
		$locations = array();
		for ($i=0; $i<count($parts); $i++) {
			$p = explode("=",$parts[$i]);
			$values = explode(",",$p[1]);
			if ($p[0] == "f_N") {
				for ($j=0;$j<count($values);$j++) {
					if (strtolower($values[$j]) == "s") {
						$values[$j] = "2nd Connections";
					} elseif (strtolower($values[$j]) == "a") {
						$values[$j] = "Group Members";
					} elseif (strtolower($values[$j]) == "o") {
						$values[$j] = "3rd + Everyone Else";
					}elseif (strtolower($values[$j]) == "f") {
						$values[$j] = "1st Connections";
					}
				}
				if (count($values) == 4) {
					$relationships = array("All");
				} else {
					$relationships = $values;
				}
				
			} else if ($p[0] == "keywords") {
				$keywords[] = urldecode($p[1]);
				
			} else if ($p[0] == "f_G") {
				foreach ($values as $locationCode) {
					$locationQuery = DB::prep("select * from li_location where linked_in_id = ?");
					$locationQuery->execute(array(urldecode($locationCode)));

					if ($location = $locationQuery->fetch()) {
						$locations[] = $location["location_name"];
					} else {
						$locations[] = "Unknown Location (".urldecode($locationCode).")";
					}
					
				}
				//die("SearchFor;".urldecode($p[1]));
			}
		}*/
		$facetToColor["Keyword"] ="#008080";
			$facetToColor["First Name"] ="#ffd700";
			$facetToColor["Last Name"] ="#0000ff";
			$facetToColor["Title"] ="#666666";
			$facetToColor["Company"] ="#f6546a";
			$facetToColor["School"] ="#003366";
			$facetToColor["Postal Code"] ="#daa520";
			$facetToColor["Country"] ="#800080";
			$facetToColor["Relationship"] ="#468499";
			$facetToColor["Location"] ="#088da5";
			$facetToColor["Current Company"] ="#800000";
			$facetToColor["Industry"] ="#660066";
			$facetToColor["Past Company"] ="#008000";
			$facetToColor["Profile Language"] ="#ffa500";
			$facetToColor["Years of Experience"] ="#ff4040";
			$facetToColor["Function"] ="#0e2f44";
			$facetToColor["Seniority Level"] ="#3b5998";
			$facetToColor["Interested In"] ="#31698a";
			$facetToColor["Company Size"] ="#8a2be2";
			$facetToColor["Connections of"] ="#69008C";
	?>
	
	<tr>
        <td><?=$query['finished'] == 1 ? 'Finished' : 'Active'?></td>
		<? if(isset($_GET["show_full_urls"])){ ?>
			<td width="500px" style="word-break: break-all;"><a href="<?=$query['url'];?>" target="_blank"><?=$query['url'];?></a>
		<?}else{?>
			<td class="nobreak"><a href="<?= $query['url']; ?>" target="_blank"><?=substr($query['url'],0,65)."..."; ?></a>
		<?}?>
		<br>
		<?
		//if (!strpos($query['url'],"/sales/")) {
			foreach (readable_parameters($query['url']) as $facet => $value) {
                //TODO implement proper parsing or take from parameters
                if (strtolower($facet) === 'keyword') {
                    $keywords = str_replace("_","",implode("; ",$value));
                    if ($keywords){
                        echo "<span class='url-parameters badge flat-warning' style='white-space: normal;'><i>$facet:</i> <br/>". $keywords ."</span> ";
                    }
			    }
			}
		//}
		?>
		
		</td>
		<td>
		
			<a href="/visitedProfiles.php?company_id=<?=$_GET['company_id']?>&search_url_id=<?=$query["id"]?>">
                <?=$query['visits']?><?=$query['total_matches'] > -1 ? ' of '.number_format($query['total_matches']) : ''?>
            </a>
            <?/*<a href="<?=base_url('visitedProfiles.php?company_id='.$_GET['company_id'].'&search_url_id='.$query["id"])?>"><?=$query['visits']?>&nbsp;<?=$query['total_matches'] == -1 ? '' : 'of&nbsp;'.number_format($query['total_matches']) ?><?=$query['finished'] == 1 ? ' Finished' : '' ?><?=$query['duplicateSkipCount'] > 0 ? ' <br>('.$query['duplicateSkipCount'].'&nbsp;duplicates&nbsp;skipped) ' : '' ?></a>*/?>
		</td>
        <td>
            <?=$query['skipped_count']?>
        </td>
        <td>
            <?=$query['nopublic_skipped_count']?>
        </td>
        <td>
            <?=$query['connected_skipped_count']?>
        </td>
        <td>
            <?=$query['duplicateSkipCount']?>
        </td>
        <td>
            <?=$query['visited_skipped_count']?>
        </td>
        <td><?=$query['last_visit']?></td>
<!--	calculate per-url VBR may be.. 	<td>--><?//=$cm['campaign_visit_count'] < 50 ? '-' : number_format(($cm['total_visitors']/$cm['campaign_visit_count'])*100,1) . "%"?><!--</td>-->
	</tr>
	<?php } ?>
</table>
