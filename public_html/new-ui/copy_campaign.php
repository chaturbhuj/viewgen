<?php

require_once('../authenticate.php');

if(isset($_GET["campaign_id"])){

	$query = DB::prep("select * from campaign where id=?");
	$query->execute(array($_GET['campaign_id']));

	$campaign = $query->fetch();
	if ($campaign['company_id'] != $_GET['company_id']) {
		echo "Not your campaign";
		die;
	}

	$new_campaign_id = copy_campaign($_GET['campaign_id'], '');

	$query = DB::prep("UPDATE campaign SET publish_status = 'published', priority=3, secondary_priority=1100 WHERE id = :campaign_id");
	$query->execute([
		'campaign_id' => $new_campaign_id
	]);
	
	redirect('new-ui/campaign_builder.php?company_id=' . $_GET['company_id'] . '&user_id=' . $_GET['user_id'] . '&campaign_id=' . $new_campaign_id);
}

?>
