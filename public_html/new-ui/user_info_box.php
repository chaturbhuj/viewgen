<?php

$query = DB::prep("SELECT * FROM li_users WHERE company_id = :company_id");
$query->execute([
	'company_id' => $_GET['company_id']
]);
$allUsers = $query->fetchAll();

// Fetch software status.
$query = DB::prep("select 
	*,
	(SELECT name FROM campaign WHERE campaign.id=li_users.campaign_id) as campaignName,
	visitsLast24Hours AS visitCount,
	invitesLast24Hours AS invitesCount,
	(limit_reached_at > '0000-00-00 00:00:00' AND limit_reached_at > date_sub(NOW(), interval 1 hour)) limitReached,
	(invitesLast24Hours >= invites24HoursLimit) invitesLimitReached
	from li_users 
	where company_id=?
	AND li_users.id = ?");
	$query->execute(array($_GET['company_id'], $_GET['user_id']));

$status = $query->fetch();

?>
<div class="row user-box">
	<?/*
	<div class="col-md-4">
		<div class="user-profile-image"></div>
	</div>
	*/?>
	<div class="col-md-12">
		<? if (count($allUsers) > 1) { ?>
			<select id="change_user" class="form-control">
				<? foreach ($allUsers as $_user) { ?>
					<? if ($_user['hidden'] == 0 or (isset($_USER_DATA["admin"]) and $_USER_DATA["admin"] != "")) {?>
						<option value="<?=$_user['id']?>"<?=$_user['id'] == $user['id'] ? ' selected' : ''?>>
							<?=$_user['name']?>
							<?=$_user['hidden']?"HIDDEN FROM USER":""?>
						</option>
					<? } ?>
				<? } ?>
			</select>
			<span data-dynamic="software_status" class="beside-changeuser"><i class="status-icon <?=$user['software_status']?>"></i></span>
		<? } else { ?>
			<span class="user-name"><?=$user['name']?> <span data-dynamic="software_status"><i class="status-icon <?=$user['software_status']?>"></i></span></span>
		<? } ?>
		<small class="software-status-text<?=(count($allUsers) > 1) ? ' with-multi' : ''?>" data-dynamic="software_status_text">
			<?=
				($status["visitCount"] > 0 ? "($status[visitCount] visits last 24h) &nbsp;": "")
				.($status["invitesCount"] > 0 ? "($status[invitesCount] invites last 24h ".($status['invitesLimitReached'] ? 'limit reached':'').") &nbsp;": "")
				.ucfirst(
					$status['software_status'] == 'waiting' ? (
					($status['visitCount'] >= $status['max_visits_per_day'] or $status['limitReached']) ? '24h limit reached' : 'Waiting for Campaign'
					) : $status['software_status']
				)
			?>
					<?/*=trim($status['software_status'])=='running' ? ' "'.make_short($status['campaignName'],10).'"': "" */?>
		</small>

		
		<?/*<a class="change-user" href="#">change user...</a>*/?>
	</div>
</div>
<div class="row user-status">
	<div class="col-md-6">
		<?/*
		<span class="status-title">Status:</span>
		<span class="status-value">Quota Reached</span>
		*/?>
	</div>
	<div class="col-md-6">
		<?/*
		<span class="status-title">Last Visit:</span>
		<span class="status-value"> hours ago</span>
		*/?>
	</div>
</div>
