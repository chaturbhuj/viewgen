<?php
$HTML_TITLE = "Company client versions history";
require_once('header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}
$company_id = intval($_GET['company_id']);
if(!$company_id){
    die("company_id is required");
}
$page = intval($_GET['page']) ? intval($_GET['page']) : 1;

$limit = 200;
$offset = $page > 0 ? ($page - 1) * $limit : 0;

$query = DB::prep("SELECT 
		count(*) AS count
	FROM company_client_version_history h 
	WHERE 
		h.company_id = :company_id
	");
$query->execute([
    "company_id" => $company_id
]);
$pages = $query->fetchColumn() / $limit;

$query = DB::prep("SELECT 
		h.*, v.name
	FROM company_client_version_history h 
	LEFT JOIN client_versions v ON (h.client_version_id = v.id)
	WHERE 
		h.company_id = :company_id
	ORDER BY h.created_at DESC 
	LIMIT :limit 
	OFFSET :offset");
$query->bindValue(':company_id', $company_id, PDO::PARAM_INT);
$query->bindValue(':limit', $limit, PDO::PARAM_INT);
$query->bindValue(':offset', $offset, PDO::PARAM_INT);
$query->execute();
$history = $query->fetchAll();

$query = DB::prep("SELECT 
		DISTINCT v.name
	FROM company_client_version_history h 
	LEFT JOIN client_versions v ON (h.client_version_id = v.id)
	WHERE 
		h.company_id = :company_id
	");
$query->execute([
    "company_id" => $company_id
]);
$versions = $query->fetchAll();


?>
<h3>Version change history</h3>
<table class="query" style="width: auto;">
	<thead>
		<tr>
			<th>Version ID</th>
            <th>Version</th>
			<th>Switch date</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($history as $item): ?>
			<tr>
				<td><?=$item['client_version_id'] ? $item['client_version_id'] : "NA"?></td>
				<td><?=$item['name'] ? $item['name'] : "NA"?></td>
				<td><?=$item['created_at']?></td>
			</tr>
		<? endforeach; ?>
        <tr>
            <td align="left" style="border-right: none">
                <? if($page > 1) : ?>
                    <a href="?company_id=<?=$company_id?>&page=<?=$page - 1?>">
                        << Prev
                    </a>
                <? endif ?>
            </td>
            <td colspan = 2 align="right" style="border-left: none">
                <? if($page < $pages) : ?>
                    <a href="?company_id=<?=$company_id?>&page=<?=$page + 1?>">
                        Next >>
                    </a>
                <? endif ?>
            </td>
        </tr>
	</tbody>
</table>
<p></p>
<h3>Versions used by company</h3>
<table class="query" style="width: auto;">
    <thead>
    <tr>
        <th>Versions</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($versions as $item): ?>
        <tr>
            <td><?=$item['name'] ? $item['name'] : "NA"?></td>
        </tr>
    <? endforeach ?>
    </tbody>
</table>
<?php

require_once('footer.php');

?>
