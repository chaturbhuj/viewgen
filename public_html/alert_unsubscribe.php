<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if (!isset($_GET['token'])) {
	header('Location: /');
	die;
}

$token = $_GET['token'];

require_once('include/session.php');
require_once('db.php');
require_once('shared_functions.php');

$IGNORE_AUTH = true;
$STOPMENU = true;
require_once("header.php");
require_once('header_index.php');

$query = DB::prep("SELECT * FROM company WHERE alert_unsubscribe_token = :token");
$query->execute([
	'token' => $token
]);

if ($query->rowCount() == 0) {
	header('Location: /');
	die;
}

$company = $query->fetch();

$query = DB::prep("SELECT unsubscribeType, 0 AS hasUnsubscribed FROM emailTemplate WHERE unsubscribeType != '' GROUP BY unsubscribeType");
$query->execute();
$types = $query->fetchAll();

if (isset($_POST['action']) && $_POST['action'] == 'save') {
	foreach ($types as $type) {
		if (!isset($_POST[str_replace(" ", "_", $type['unsubscribeType'])])) {
			$query = DB::prep("INSERT IGNORE INTO emailUnsubscribed (company_id, unsubscribeType) VALUES(:company_id,
				:unsubscribeType)");
			$query->execute([
				'company_id' => $company['id'],
				'unsubscribeType' => $type['unsubscribeType']
			]);
		} else {
			$query = DB::prep("DELETE FROM emailUnsubscribed WHERE company_id = :company_id AND
				unsubscribeType = :unsubscribeType");
			$query->execute([
				'company_id' => $company['id'],
				'unsubscribeType' => $type['unsubscribeType']
			]);
		}
	}
	redirect('alert_unsubscribe.php?token=' . $token);
}

$query = DB::prep("SELECT * FROM emailUnsubscribed WHERE company_id = :company_id");
$query->execute([
	'company_id' => $company['id']
]);
$unsubscribed = $query->fetchAll();

foreach ($types as $i => $type) {
	foreach ($unsubscribed as $u) {
		if ($u['unsubscribeType'] == $type['unsubscribeType']) {
			$types[$i]['hasUnsubscribed'] = true;
		}
	}
}

?>

<div class="container">
	<div class="col-xs-4 col-xs-offset-4" style="margin-top: 40px;">
		<div class="bordered-box">
			<form method="post">
				<div class="row">
					<div class="col-xs-12">
						<b>Email Settings</b>
						<? foreach ($types as $type) { ?>
							<div class="checkbox">
								<label><input type="checkbox" name="<?=$type['unsubscribeType']?>"
									<?=!$type['hasUnsubscribed'] ? ' checked="checked"' : ''?>>
									<?=$type['unsubscribeType']?></label>
							</div>
						<? } ?>
					</div>
					<div class="col-xs-12 text-center">
						<div class="form-group">
							<button class="btn btn-info" type="submit" name="action" value="save">Save</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?
include('footer.php');
?>
