<?php

require_once(__DIR__ . '/helpers/local_settings.php');

$CONFIG = [
    'dev' => false, //initial value
    'localhost' => false, //initial value
    'path' => [
        'client_files_base' => 'https://storage.googleapis.com/viewgen-bins/',
        'windows_client_binary_url' => 'https://storage.googleapis.com/viewgen-bins/ViewGenInstaller.exe',

        'mac_client_binary_url' => 'https://storage.googleapis.com/viewgen-bins/ViewGenInstaller.pkg',

        'mac_client_appcast_url' => 'https://storage.googleapis.com/viewgen-bins/appcast.xml',
        'mac_client_update_pack_url' => 'https://storage.googleapis.com/viewgen-bins/ViewGen.pkg.zip',
    ]
];

if (isset($IS_CRON_JOB) && $IS_CRON_JOB === true) {
    $host = 'viewgentools.com';
    
}else if (isset($_SERVER['HTTP_HOST'])) {
    $host = $_SERVER['HTTP_HOST'];

} elseif (isset($LOCAL_SETTINGS['host'])) {
    $host = $LOCAL_SETTINGS['host'];

} else {
    die('Wrong host');
}

$CONFIG['host'] = $host;

//zendesk settings
$CONFIG['zendesk_key'] = "IUXhH4HpDJHGcHGmDefCgeTVOE77KLPpTLKEEa5Qhd01RjOO";
$CONFIG['zendesk_subdomain'] = "viewgen";

//intercom sandbox
$CONFIG['intercom_api_key'] = "dG9rOjIzYjI0MmMyX2E5NmJfNGEwNF85OTFhXzY1YzJiNmZkZTc5MzoxOjA=";
$CONFIG['intercom_app_id'] = "n6l7ej9a";

//Recurly Dev settings, common to all devs, will be overwritten in the prod
$CONFIG['recurly_subdomain'] = "vgdevsandbox";
$CONFIG['recurly_api_key'] = "c326f33dfb0c417b9a0f1a02b0ac4860";
$CONFIG['recurly_private_key'] = "";
$CONFIG['recurly_js_public_key'] = "ewr1-hGo8papbKUKIaLSUMC3sg8";

if (strpos($host, "localhost") === 0) {
    $CONFIG['dev'] = true;
    $CONFIG['localhost'] = $host;
    $CONFIG['db_host'] = $LOCAL_SETTINGS['db_host'];
    $CONFIG['db_user'] = $LOCAL_SETTINGS['db_user'];
    $CONFIG['db_pass'] = $LOCAL_SETTINGS['db_pass'];
    $CONFIG['db_name'] = $LOCAL_SETTINGS['db_name'];

//} elseif (strpos($host, "jozo.dev.viewgentools.com") === 0) {
//    $CONFIG['dev'] = true;
//    $CONFIG['db_host'] = "searchquant.net";
//    $CONFIG['db_user'] = "devserver";
//    $CONFIG['db_pass'] = "98347693486osdfghj";
//    $CONFIG['db_name'] = "searchquant";
//
} elseif (strpos($host, "staging.viewgentools.com") === 0) {
    $CONFIG['dev'] = true;
    $CONFIG['db_host'] = "35.188.33.45";
    $CONFIG['db_user'] = "dev";
    $CONFIG['db_pass'] = "HnkjhhwBkPQP80fAZ";
    $CONFIG['db_name'] = "searchquant";

} elseif (strpos($host, "new-dev.viewgentools.com") === 0) {
    $CONFIG['dev'] = true;
//    $CONFIG['db_host'] = "localhost";
//    $CONFIG['db_user'] = "root";
//    $CONFIG['db_pass'] = "Pass1234";
//    $CONFIG['db_name'] = "dev";
    $CONFIG['db_host'] = "35.188.33.45";
    $CONFIG['db_user'] = "dev";
    $CONFIG['db_pass'] = "HnkjhhwBkPQP80fAZ";
    $CONFIG['db_name'] = "searchquant";

} else {
    // Live configuration.
    $CONFIG['dev'] = false;
    $CONFIG['db_host'] = "104.197.44.130";
    $CONFIG['db_user'] = "dev";
    $CONFIG['db_pass'] = "HnkjhhwBkPQP80fAZ";
    $CONFIG['db_name'] = "searchquant";

    //overwrite Recurly settings to production
	$CONFIG['recurly_subdomain'] = "viewgen";
	$CONFIG['recurly_api_key'] = "69b5474d815c4e40824fc8df92778895";
	$CONFIG['recurly_private_key'] = "46a4ec9695db4e8386e82da4741201ca";
	$CONFIG['recurly_js_public_key'] = "ewr1-bmduG3fY8occnUVwWanPVV";

    //intercom prod
    $CONFIG['intercom_api_key'] = "dG9rOjgzYzljNTNkXzFlNGVfNDgxMV9hM2U3XzYyOWNkZDE4YzgyYToxOjA=";
    $CONFIG['intercom_app_id'] = "hmgyw4dh";
}


?>
