<?php
$HTML_TITLE = "Bad pages";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "api_bad_pages.html_dump_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Dump",
            "html" => function($data){
                return '<a target="_blank" href="/viewHtmlDump.php?company_id='.$data['api_bad_pages.company_id'].'&html_id='.$data['api_bad_pages.html_dump_id'].'">'.$data['api_bad_pages.html_dump_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['html_dump_id']) ? $_GET['html_dump_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'">'.$data['api_bad_pages.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['company_id']) ? $_GET['company_id'] : null
        ],
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "company.company_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'">'.$data['company.company_name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['company_name']) ? $_GET['company_name'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['user_id']) ? $_GET['user_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.linkedin_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['api_bad_pages.linkedin_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['linkedin_id']) ? $_GET['linkedin_id'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "li_users.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['user_name']) ? $_GET['user_name'] : null
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "api_bad_pages.created",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between",
            "searchValue" =>  isset($_GET['created']) ? $_GET['created'] : (date("Y-m-d").' 00:00:00 ~ '.date("Y-m-d").' 23:59:59')
        ],
        [
            "config" => [
                "className" => "",
                "name" => "api_bad_pages.dump_url",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Url",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['dump_url']) ? $_GET['dump_url'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "api_bad_pages.message",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Message",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['message']) ? $_GET['message'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "api_bad_pages.page_type",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Page Type",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['page_type']) ? $_GET['page_type'] : null
        ],
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "api_bad_pages.campaign_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Campaign",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'&campaign_id='.$data['api_bad_pages.campaign_id'].'">'.$data['api_bad_pages.campaign_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['campaign_id']) ? $_GET['campaign_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.search_url_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Search url id",
            "html" => '',
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['search_url_id']) ? $_GET['search_url_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.people_list_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "People list id",
            "html" => '',
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['people_list_id']) ? $_GET['people_list_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Id",
            "html" => '',
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['id']) ? $_GET['id'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_versions.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Client",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['client_version']) ? $_GET['client_version'] : null
        ],
    ],
    "from" => "
		api_bad_pages 
		LEFT JOIN client_versions ON client_versions.id = api_bad_pages.version_id
		LEFT JOIN li_users ON api_bad_pages.linkedin_id = li_users.linked_in_id AND api_bad_pages.company_id = li_users.company_id
		LEFT JOIN company ON api_bad_pages.company_id = company.id
    ",
    "where" => "",
    "group" => ""
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
