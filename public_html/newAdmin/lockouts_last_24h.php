<?php
$HTML_TITLE = "Linked in lockouts last 24h";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'">'.$data['api_bad_pages.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "api_bad_pages.linkedin_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "company.company_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'">'.$data['company.company_name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "li_users.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['api_bad_pages.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "last_lock",
                "orderable" => true,
                "searchable" => false,
            ],
            "title" => "Last lock",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "max(api_bad_pages.created)",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
    ],
    "from" => "
        api_bad_pages 
        left join li_users on li_users.linked_in_id = api_bad_pages.linkedin_id and li_users.company_id = api_bad_pages.company_id
        left join company on company.id = api_bad_pages.company_id
    ",
    "where" => "
        AND api_bad_pages.created between date_sub(now(), interval 1 day) and now()
        AND api_bad_pages.message = 'li: sales nav limit'
    ",
    "group" => "
        api_bad_pages.linkedin_id,
        api_bad_pages.company_id
    "
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
