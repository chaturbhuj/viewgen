<?php
$HTML_TITLE = "Referrers";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
require_once (__DIR__ . '/../classes/Referrer.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

if(isset($_POST) && $_POST['action'] === 'pay'){
    $Referer = new Referrer();
    $Referer->markAsPaid($_POST['id'], $_POST['message']);

    redirect("/newAdmin/referrers.php?".$_SERVER['QUERY_STRING']);
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "vg_referrer.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "vg_referrer.first_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "First name",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "vg_referrer.last_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Last name",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "vg_referrer.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['vg_referrer.company_id'].'">'.$data['vg_referrer.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "company.company_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['vg_referrer.company_id'].'">'.$data['company.company_name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "vg_referrer.created_at",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "vg_referrer.referred_email",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Email",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "vg_referrer.referred_company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Referred company id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['vg_referrer.referred_company_id'].'">'.$data['vg_referrer.referred_company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "ref.company_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Referred company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['vg_referrer.referred_company_id'].'">'.$data['ref.company_name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "vg_referrer.converted_at",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Converted",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "vg_referrer.paid_at",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Paid date",
            "defaultOrder" => ["desc", 0],
            "html" => function($data){
                if($data['vg_referrer.paid_at'] > 0 || !$data['vg_referrer.referred_company_id'] || !$data['vg_referrer.converted_at']){
                    return $data['vg_referrer.paid_at'];
                }

                $res = '
                            <form action="referrers.php?'.$_SERVER['QUERY_STRING'].'" method="post">
                                <input name="message" type="text" placeholder="Comment">
                                <input name="id" type="hidden" value="'.$data['vg_referrer.id'].'">
                                <input name="action" type="hidden" value="pay">
                                <input name="submit" type="submit" value="Pay">
                            </form>
                        ';

                return $res;
            },
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "vg_referrer.notes",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Notes",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
    ],
    "from" => "
        vg_referrer
        JOIN company ON vg_referrer.company_id = company.id
        LEFT JOIN company ref ON vg_referrer.referred_company_id = ref.id
    ",
    "where" => "
        AND company.frozen = 0
        AND company.is_test = 0
        AND company.deleted = 0
    ",
    "group" => "
    	vg_referrer.id
    "
];

if(isset($_GET['referred_company'])){
    $tableConfig['where'] .= ' AND referred_company_id '.($_GET['referred_company'] === 'null' ? 'IS NULL ' : 'IS NOT NULL ');
}
if(isset($_GET['converted'])){
    $tableConfig['where'] .= ' AND converted_at '.($_GET['converted'] === 'null' ? 'IS NULL ' : 'IS NOT NULL ');
}
if(isset($_GET['paid'])){
    $tableConfig['where'] .= ' AND paid_at '.($_GET['paid'] === 'null' ? 'IS NULL ' : 'IS NOT NULL ');
}
//----------------------------------------------------------------------------------------------------------------------
$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
