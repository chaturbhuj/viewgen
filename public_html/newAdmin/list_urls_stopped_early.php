<?php
$HTML_TITLE = "List HTML";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "className" => "",
//                "width" => "50px",
                "name" => "search_url.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "ID",
            "html" => function($data){
                return '<a target="blank" href="/visitedProfiles.php?company_id='.$data['search_url.company_id'].'&search_url_id='.$data['search_url.id'].'">'.$data['search_url.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "visit_diff",
                "orderable" => true,
                "searchable" => false,
            ],
            "title" => "% difference",
            "defaultOrder" => ["desc", 1],
            "sql" => "ROUND(((search_url.total_matches - (search_url.search_url_visit_count + search_url.skipped_count))/search_url.total_matches)*100)",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "search_url.total_matches",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Total matches",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "search_url.search_url_visit_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "search_url.skipped_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Skipped count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "search_url.campaign_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Campaign",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['search_url.company_id'].'&user_id='.$data['li_users.id'].'&campaign_id='.$data['search_url.campaign_id'].'">'.$data['search_url.campaign_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['search_url.company_id'].'">'.$data['search_url.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "search_url.url",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "URL",
            "html" => function($data){
                return '<a target="blank" href="'.$data['search_url.url'].'">'.$data['search_url.url'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.visit_by",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit by",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.visit_by_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit by id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "search_url.created",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "search_url.search_url_last_visit",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Last visit",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.duplicateSkipCount",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Duplicate count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.connected_skipped_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Connected count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.nopublic_skipped_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Private count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "search_url.visited_skipped_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Prev visited count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ]
    ],
    "from" => "
        search_url
        LEFT JOIN li_users ON search_url.visit_by_id = li_users.linked_in_id AND search_url.company_id = li_users.company_id 
    ",
    "where" => "
        AND search_url.finished = 1 
        AND search_url.paused = 0 
        AND search_url.total_matches > (search_url.search_url_visit_count + search_url.skipped_count)
    ",
    "group" => ""
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
