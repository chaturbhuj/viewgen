<?php
$HTML_TITLE = "Li users with no visits";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['li_users.company_id'].'">'.$data['li_users.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['li_users.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.linked_in_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in id",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['li_users.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "company.company_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['li_users.company_id'].'">'.$data['company.company_name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "li_users.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['li_users.company_id'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "visits_count",
                "orderable" => true,
                "searchable" => false,
            ],
            "title" => "Visits",
            "defaultOrder" => ["desc", 1],
            "sql" => "sum(reports_cache.number_of_visits)",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "li_users.last_connection",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Last connection",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
    ],
    "from" => "
        li_users
        LEFT JOIN reports_cache ON li_users.linked_in_id = reports_cache.li_users_id AND li_users.company_id = reports_cache.company_id AND reports_cache.cache_date > date(date_sub(now(), interval 7 day))
        JOIN company ON li_users.company_id = company.id
    ",
    "where" => "
        AND company.frozen = 0
        AND li_users.hidden = 0
        AND company.is_test = 0
        AND company.deleted = 0
    ",
    "group" => "
    	li_users.id
       	having
           	sum(reports_cache.number_of_visits) < 100 OR sum(reports_cache.number_of_visits) IS NULL 
    "
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
