<?php

/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 15.09.17
 * Time: 11:52
 */
class ReportGenerator
{
    public $conf;
    public function __construct($conf){
        $this->conf = $conf;
    }

    public function getData($post){
        $columns = $post['columns'];

        $limit = "";

        if(isset($post['length'])){
            $limit .= " LIMIT ".intval($post['length'])." ";
        }
        if(isset($post['start'])){
            $limit .= " OFFSET ".intval($post['start'])." ";
        }

        $order = "";
        if(isset($post['order'])){
            foreach($post['order'] as $item){
                $dir = $item['dir'] === "asc" ? "ASC" : "DESC";
                if($columns[$item['column']]['orderable'] == "true") $order .= $columns[$item['column']]['name'] . " ".$dir.", ";
            }
            $order = trim($order, ", ");
        }

        $search = "";
        if(isset($post['search']) && !empty($post['search']['value'])){
            $searchValue = $post['search']['value'];

            $searchArray = [];

            foreach ($columns AS $column){
                if($column['searchable'] == "true"){
                    $operation = $this->conf['columns'][$column['data']]['commonSearch'];
                    $columnName = $column['name'];
                    switch($operation){
                        case "=":
                        case ">":
                        case ">=":
                        case "<":
                        case "<=":
                        case "<>":
                            $searchArray[] = $columnName . " ".$operation." '".$searchValue."'";
                            break;
                        case "like":
                            $searchArray[] = $columnName . " LIKE '%".$searchValue."%'";
                            break;
                    }
                }
            }

            if(!empty($searchArray)){
                $search = " AND ( ".implode(" OR ", $searchArray)." ) ";
            }
        }

        if(isset($columns)){
            foreach ($columns as $column){
                $searchValue = $column['search']['value'];
                if(($column['searchable'] == "true") && isset($searchValue) && $searchValue !=""){
                    $operation = $this->conf['columns'][$column['data']]['columnSearch'];
                    $columnName = $column['name'];
                    switch($operation){
                        case "=":
                        case ">":
                        case ">=":
                        case "<":
                        case "<=":
                        case "<>":
                            $search .= "AND " . $columnName . " ".$operation." '".$searchValue."' ";
                            break;
                        case "like":
                            $search .= "AND " . $columnName . " LIKE '%".$searchValue."%' ";
                            break;
                        case "between":
                            $interval = explode("~", $searchValue);
                            $search .= "AND " . $columnName . " BETWEEN '".trim($interval[0])."' AND '".trim($interval[1])."' ";
                            break;
                    }
                }
            }
        }

        $select = "";
        foreach ($this->conf['columns'] as $column){
            $alias = $column['config']['name'];
            $select .= (!empty($column['sql']) ? $column['sql'] : $alias) . " AS `".$alias."`, ";
        }
        $select = trim($select, ", ");

        $query = DB::prep("
          SELECT
              count(*)
          FROM
              (SELECT
                    ".$select."
                FROM
                    ".$this->conf['from']."
                WHERE
                    1
                    ".$this->conf['where']."
                    ".$search."
                ".(!empty($this->conf['group']) ? (" GROUP BY " . $this->conf['group']) : "")."
                LIMIT 1000000) h
        ");
//        echo $query->queryString;die;
        $query->execute();
        $filteredCount = $query->fetchColumn();
        $count = $filteredCount;

        $query = DB::prep("
            SELECT
                ".$select."
            FROM 
                ".$this->conf['from']."
            WHERE 
                1
                ".$this->conf['where']."
                ".$search."
            ".(!empty($this->conf['group']) ? (" GROUP BY " . $this->conf['group']) : "")."
            ORDER BY ".$order."
            ".$limit
        );
        $query->execute();
        $results = $query->fetchAll();

        $data = [];
        foreach ($results as $item) {
            $resultData = [];
            foreach ($this->conf['columns'] as $key => $value){
                $resultData[] = !empty($value['html']) ? $value['html']($item) : $item[$key];
            }
            $data[] = $resultData;
        }

        return [
            "draw" => intval($post['draw']),
            "recordsTotal" => $count,
            "recordsFiltered" => $filteredCount,
            "data" => $data
        ];
    }

    public function processRequest()
    {
        foreach ($this->conf['columns'] as $key => $column){
            $paramName = strtr($column['config']['name'], ["." => "_"]);
            $this->conf['columns'][$key]['searchValue'] = isset($_GET[$paramName]) ? $_GET[$paramName] : (!empty($column['searchValue']) ? $column['searchValue'] : null);
        }

        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            http_response_code(200);
            header('Content-Type: application/json');
            echo json_encode($this->getData($_POST));
        }else{
            echo $this->renderTable();
        }
    }

    public function renderTable(){
        ob_start();
        require __DIR__.'/tableTemplate.php';
        return ob_get_clean();
    }
}