<?php
$HTML_TITLE = "HTML dumps";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "htmlDump.htmlDumpId",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Id",
            "html" => function($data){
                return '<a target="_blank" href="/viewHtmlDump.php?company_id='.$data['htmlDump.companyId'].'&html_id='.$data['htmlDump.htmlDumpId'].'">'.$data['htmlDump.htmlDumpId'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['htmlDumpId']) ? $_GET['htmlDumpId'] : null
        ],
        [
            "config" => [
                "visible" => true,
                "className" => "",
                "name" => "htmlDump.companyId",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['htmlDump.companyId'].'">'.$data['htmlDump.companyId'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['companyId']) ? $_GET['companyId'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['htmlDump.companyId'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['user_id']) ? $_GET['user_id'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "htmlDump.crawled_by_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in id",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['htmlDump.companyId'].'&user_id='.$data['li_users.id'].'">'.$data['htmlDump.crawled_by_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['crawled_by_id']) ? $_GET['crawled_by_id'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "li_users.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User",
            "html" => function($data){
                return '<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$data['htmlDump.companyId'].'&user_id='.$data['li_users.id'].'">'.$data['li_users.name'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['crawled_BY']) ? $_GET['crawled_by'] : null
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "htmlDump.htmlDumpCreated",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "sql" => "",
            "commonSearch" => "",
            "columnSearch" => "between",
            "searchValue" =>  isset($_GET['htmlDumpCreated']) ? $_GET['htmlDumpCreated'] : (date("Y-m-d").' 00:00:00 ~ '.date("Y-m-d").' 23:59:59')
        ],
        [
            "config" => [
                "className" => "",
                "name" => "htmlDump.dump",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Dump",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['dump']) ? $_GET['dump'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "htmlDump.ip",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "IP",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['ip']) ? $_GET['ip'] : null
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "htmlDump.client_hash",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Client hash",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['client_hash']) ? $_GET['client_hash'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "htmlDump.html_people_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "html people id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "=",
            "searchValue" =>  isset($_GET['html_people_id']) ? $_GET['html_people_id'] : null
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_versions.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Client",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like",
            "searchValue" =>  isset($_GET['clientVersion']) ? $_GET['clientVersion'] : null
        ],
    ],
    "from" => "
		htmlDump 
		LEFT JOIN htmlDump_to_client_version ON htmlDump.htmlDumpId = htmlDump_to_client_version.html_dump_id
		LEFT JOIN client_versions ON client_versions.id = htmlDump_to_client_version.client_version_id
		LEFT JOIN li_users ON htmlDump.crawled_by_id = li_users.linked_in_id AND htmlDump.companyId = li_users.company_id
    ",
    "where" => "",
    "group" => ""
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
