<?php
$HTML_TITLE = "List HTML";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "client_log.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "ID",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "client_log.parent_exception_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Parent exception",
            "sql" => "",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_log.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['client_log.company_id'].'">'.$data['client_log.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_log.log_type",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Log type",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "client_log.message",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Message",
            "html" =>  function($data){
                return htmlentities($data['client_log.message']);
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "client_log.stack_trace",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Stack trace",
            "html" => function($data){
                return htmlentities($data['client_log.stack_trace']);
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "client_log.created",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
            "defaultOrder" => ["desc", 0],
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_log.linked_in_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_log.linked_in_name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Linked in name",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "client_log.client_version",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Client version",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ]
    ],
    "from" => "
        client_log
    ",
    "where" => "",
    "group" => ""
];


//----------------------------------------------------------------------------------------------------------------------

$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
