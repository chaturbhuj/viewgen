<?php
$HTML_TITLE = "List HTML";
$STOPHEAD = true;
$STOPMENU = true;
require_once(__DIR__.'/../header.php');
require_once __DIR__.'/ReportGenerator.php';

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$tableConfig = [
    "columns" => [
        [
            "config" => [
                "className" => "",
//                "width" => "50px",
                "name" => "campaign.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "ID",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['campaign.company_id'].'&user_id='.$data['li_users.id'].'&campaign_id='.$data['campaign.id'].'">'.$data['campaign.id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
//                "width" => "50px",
                "className" => "",
                "name" => "bad_urls",
                "orderable" => true,
                "searchable" => false,
            ],
            "title" => "Bad URLs",
            "defaultOrder" => ["desc", 0],
            "sql" => "count(search_url.id)",
            "html" => function($data){
                return '<a target="blank" href="/newAdmin/list_urls_stopped_early.php?search_url.company_id='.$data['campaign.company_id'].'&search_url.campaign_id='.$data['campaign.id'].'">'.$data['bad_urls'].'</a>';
            },
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.company_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Company",
            "html" => function($data){
                return '<a target="blank" href="/new-ui/campaign_builder.php?company_id='.$data['campaign.company_id'].'">'.$data['campaign.company_id'].'</a>';
            },
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.visit_by",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit by",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "campaign.visit_by_id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit by id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "campaign.created",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Created",
//            "defaultOrder" => ["desc", 0],
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between",
            "searchValue" => date("Y-m-d") . " 00:00:00". " ~ " .date("Y-m-d") . " 23:59:59"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "campaign.start_date",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Started at",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "campaign.campaign_completed",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Completed at",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "datetime",
                "name" => "campaign.campaign_last_visit",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Last visit",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "between"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.campaign_visit_count",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visit count",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.name",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Name",
            "html" => "",
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.publish_status",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Publish status",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.status",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Status",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "campaign.is_query_campaign",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Is query",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "campaign.priority",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Priority",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "campaign.secondary_priority",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Secondary Priority",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "className" => "",
                "name" => "campaign.total_visitors",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Visitors",
            "html" => "",
            "commonSearch" => "",
            "columnSearch" => "="
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "campaign.original_url",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "Original url",
            "html" => function($data){
                return '<a target="blank" href="'.$data['campaign.original_url'].'">'.$data['campaign.original_url'].'</a>';
            },
            "commonSearch" => "like",
            "columnSearch" => "like"
        ],
        [
            "config" => [
                "visible" => false,
                "className" => "",
                "name" => "li_users.id",
                "orderable" => true,
                "searchable" => true,
            ],
            "title" => "User id",
            "html" => "",
            "commonSearch" => "=",
            "columnSearch" => "="
        ]
    ],
    "from" => "
        campaign
        LEFT JOIN search_url ON campaign.id = search_url.campaign_id AND search_url.finished = 1 AND search_url.total_matches <> (search_url.search_url_visit_count + search_url.skipped_count)
        LEFT JOIN li_users ON campaign.visit_by_id = li_users.linked_in_id AND campaign.company_id = li_users.company_id
    ",
    "where" => "",
    "group" => "
    	campaign.id
    "
];


//----------------------------------------------------------------------------------------------------------------------
$reportGenerator = new ReportGenerator($tableConfig);

$reportGenerator->processRequest();
