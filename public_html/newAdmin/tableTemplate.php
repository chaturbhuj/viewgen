<script type="application/javascript" src="<?=base_url('assets/js/jquery.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/fc-3.2.2/fh-3.1.2/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/fc-3.2.2/fh-3.1.2/datatables.min.js"></script>

<link rel="stylesheet" href="<?=base_url('assets/js/dateRangePicker/daterangepicker.min.css')?>">
<script type="text/javascript" src="<?=base_url('assets/js/moment.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/js/dateRangePicker/jquery.daterangepicker.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.4.0/css/colReorder.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.4.0/js/dataTables.colReorder.min.js"></script>

<style>
    div.ColVis {
        float: left;
    }
</style>

<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell

        var table = $('#data').DataTable( {
//            "orderCellsTop": true,
//            "colReorder": true,
            "dom":            "lBfrtip",
            "fixedHeader": {
                "header": true
            },
            "order": [
                <?php
                $order = [];
                foreach ($this->conf['columns'] as $key => $column){
                    if(!empty($column['defaultOrder'])){
                        $order[$column['defaultOrder'][1]] = [$key, $column['defaultOrder'][0]];
                    }
                }
                ksort($order);
                ?>
                <?php foreach ($order as $value): ?>
                <?= json_encode($value); ?>,
                <?php endforeach; ?>
            ],
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "",
                type: 'POST'
            },
            "columns" : [
                <?php foreach ($this->conf['columns'] as $column): ?>
                <?= json_encode($column['config'])?>,
                <?php endforeach; ?>
            ],
            "searchCols": [
                <?php foreach ($this->conf['columns'] as $column): ?>
                {"search" : <?= json_encode($column['searchValue'])?>},
                <?php endforeach; ?>
            ],
            "buttons": [
                'colvis'
            ]
        } );

        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
            $( 'input', this.header() ).on('click', function(e) {
                e.stopPropagation();
            });

            $(this.header()).children('input').val(this.search());

            var datetimeFields = $(this.header()).children( 'input.datetime');
            if(datetimeFields.length > 0){
                datetimeFields.dateRangePicker({
                    monthSelect: true,
                    yearSelect: true,
                    startOfWeek: 'monday',
                    separator : ' ~ ',
                    format: 'YYYY-MM-DD HH:mm:ss',
                    autoClose: false,
                    time: {
                        enabled: true
                    },
                    defaultTime: moment().startOf('day').toDate(),
                    defaultEndTime: moment().endOf('day').toDate(),
                    showShortcuts: true,
                    shortcuts :
                    {
                        'prev-days': [3,5,7],
                        'prev': ['week','month','year'],
                        'next-days':null,
                        'next':null
                    },
                    customShortcuts:
                    [
                        //if return an array of two dates, it will select the date range between the two dates
                        {
                            name: 'this week',
                            dates : function()
                            {
                                var start = moment().day(0).startOf('day').toDate();
                                var end = moment().day(6).endOf('day').toDate();
                                return [start,end];
                            }
                        }
                    ],
                    setValue: function(s)
                    {
                        if(!$(this).attr('readonly') && !$(this).is(':disabled') && s != $(this).val())
                        {
                            $(this).val(s);
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        }
                    }
                });
            }
        });

    });
</script>


<table id="data" class="display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <?php foreach ($this->conf['columns'] as $column): ?>
            <th><?= $column['title']?></th>
        <?php endforeach; ?>
    </tr>
    <tr>
        <?php foreach ($this->conf['columns'] as $column): ?>
            <th>
                <?php if($column['config']['searchable']): ?>
                    <input type="text" style="width: 100%" class="<?= $column['config']['className']?>"  />
                <?php endif; ?>
                <span style="display: none"><?= $column['title']?></span>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <?php foreach ($this->conf['columns'] as $column): ?>
            <th><?= $column['title']?></th>
        <?php endforeach; ?>
    </tr>
    </tfoot>
</table>


<?php

require_once(__DIR__.'/../footer.php');
?>
