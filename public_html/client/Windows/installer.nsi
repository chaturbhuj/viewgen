
!include "MUI.nsh"

!define MUI_ICON "logo.ico"

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

Name SearchQuant

RequestExecutionLevel user

# define installer name
OutFile "SearchQuantInstaller.exe"
 
# set desktop as install directory
InstallDir "$LOCALAPPDATA\SearchQuant"

!define UNINSTALLER_NAME "Uninstaller.exe"
!define START_LINK_DIR "$STARTMENU\Programs\SearchQuant"
!define START_LINK_SEARCHQUANT "$STARTMENU\Programs\SearchQuant\SearchQuant.lnk"
!define START_LINK_UNINSTALLER "$STARTMENU\Programs\SearchQuant\Uninstall SearchQuant.lnk"
 
!define REG_UNINSTALL "Software\Microsoft\Windows\CurrentVersion\Uninstall\SearchQuant"
!define WEBSITE_LINK "http://searchquant.net/"

Function .onInit
	ReadRegStr $R0 HKCU "${REG_UNINSTALL}" "DisplayName"
	StrCmp $R0 "SearchQuant" NotInstalled
		Return
	NotInstalled:
		MessageBox MB_OK "SearchQuant is already installed. Uninstall it first."
		Abort

 FunctionEnd
 
# default section start
Section


WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayName" "SearchQuant"
WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayIcon" "$\"$INSTDIR\SearchQuant.exe$\""
WriteRegStr HKCU "${REG_UNINSTALL}" "Publisher" "SearchQuant"
WriteRegStr HKCU "${REG_UNINSTALL}" "DisplayVersion" "1.1.0.0"
WriteRegDWord HKCU "${REG_UNINSTALL}" "EstimatedSize" 52 ;KB
WriteRegStr HKCU "${REG_UNINSTALL}" "HelpLink" "${WEBSITE_LINK}"
WriteRegStr HKCU "${REG_UNINSTALL}" "URLInfoAbout" "${WEBSITE_LINK}"
WriteRegStr HKCU "${REG_UNINSTALL}" "InstallLocation" "$\"$INSTDIR$\""
WriteRegStr HKCU "${REG_UNINSTALL}" "InstallSource" "$\"$EXEDIR$\""
WriteRegDWord HKCU "${REG_UNINSTALL}" "NoModify" 1
WriteRegDWord HKCU "${REG_UNINSTALL}" "NoRepair" 1
WriteRegStr HKCU "${REG_UNINSTALL}" "UninstallString" "$\"$INSTDIR\${UNINSTALLER_NAME}$\""
WriteRegStr HKCU "${REG_UNINSTALL}" "Comments" "Uninstalls SearchQuant."
 
# define output path
SetOutPath $INSTDIR
 
# specify file to go in output path
File Newtonsoft.Json.dll
File SearchQuant.exe
File SearchQuantService.exe

# define uninstaller name
WriteUninstaller $INSTDIR\${UNINSTALLER_NAME}

# register windows startup item
SetRegView 64
WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "SearchQuant" '"$INSTDIR\SearchQuant\SearchQuant.exe"'
WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "SearchQuantService" '"$INSTDIR\SearchQuant\SearchQuantService.exe"'

# register authentication
WriteRegStr HKCU "SOFTWARE\SearchQuant\" "company_name" "Jozo"
WriteRegStr HKCU "SOFTWARE\SearchQuant\" "client_hash" "e8d7b9e2cab3ae28a66d2841d7dc8c68"
WriteRegStr HKCU "SOFTWARE\SearchQuant\" "path" "$INSTDIR\SearchQuant.exe"

SetShellVarContext current
CreateDirectory "${START_LINK_DIR}"
CreateShortCut "${START_LINK_SEARCHQUANT}" "$INSTDIR\SearchQuant.exe"
CreateShortCut "${START_LINK_UNINSTALLER}" "$INSTDIR\${UNINSTALLER_NAME}"

Exec "$INSTDIR\SearchQuant.exe"
Exec "$INSTDIR\SearchQuantService.exe"

#-------
# default section end
SectionEnd
 
# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"

ExecWait  'taskkill /IM "SearchQuantService.exe" /F'
ExecWait  'taskkill /IM "SearchQuant.exe" /F'

Sleep 2000

DeleteRegValue HKCU "${REG_UNINSTALL}" "DisplayName"
DeleteRegValue HKCU "${REG_UNINSTALL}" "DisplayIcon"
DeleteRegValue HKCU "${REG_UNINSTALL}" "Publisher"
DeleteRegValue HKCU "${REG_UNINSTALL}" "DisplayVersion"
DeleteRegValue HKCU "${REG_UNINSTALL}" "EstimatedSize"
DeleteRegValue HKCU "${REG_UNINSTALL}" "HelpLink"
DeleteRegValue HKCU "${REG_UNINSTALL}" "URLInfoAbout"
DeleteRegValue HKCU "${REG_UNINSTALL}" "InstallLocation"
DeleteRegValue HKCU "${REG_UNINSTALL}" "InstallSource"
DeleteRegValue HKCU "${REG_UNINSTALL}" "NoModify"
DeleteRegValue HKCU "${REG_UNINSTALL}" "NoRepair"
DeleteRegValue HKCU "${REG_UNINSTALL}" "UninstallString"
DeleteRegValue HKCU "${REG_UNINSTALL}" "Comments"
DeleteRegValue HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "SearchQuant"
DeleteRegValue HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\" "SearchQuantService"

# Always delete uninstaller first
Delete $INSTDIR\Uninstaller.exe
 
# now delete installed file
Delete $INSTDIR\Newtonsoft.Json.dll
Delete $INSTDIR\SearchQuant.exe
Delete $INSTDIR\SearchQuantService.exe

Delete "${START_LINK_SEARCHQUANT}"
Delete "${START_LINK_UNINSTALLER}"
RMDir "${START_LINK_DIR}"
 
SectionEnd