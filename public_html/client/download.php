<?php

//require_once('../authenticate.php');
require_once('../db.php');
require __DIR__ . '/../services/version_svc.php';
use function \services\version\get_client_binary_version;

if (!isset($_GET['type'])) die;
if (!in_array($_GET['type'], array('mac', 'windows'))) die;

function make_resulting_filename($filePath){
    $version = get_client_binary_version($filePath);
    $pinfo = pathinfo($filePath);
    return $pinfo['filename'] . ($version ? ('-' . $version) : '') . '.' . $pinfo['extension'];
}


if (isset($_GET['company_id'])){
    //company_id passed, so this is an initial download, not update
    //
    //else (as client's binary no longer contain client_hash hardcoded),
    //   we can safely skip this piece and just return the binary
    //   e.g. we don't have valid key in should_update.php
    
    $query = DB::prep("select * from company where id=?");
    $query->execute(array($_GET['company_id']));
    
    if ($query->rowCount() == 0){
        echo '<h1>Wrong company Id ' . $_GET['company_id'] . '</h1>';
        die;
    } 
    
    $company = $query->fetch();
    
    $query = DB::prep("select product_key from software_license where company_id=?");
    $query->execute(array($_GET['company_id']));
    $productKey ="";
    if($software_license = $query->fetch()) {
    	//so we passed valid company_id and found a key,
        //it is useless below but serves as additional check here
        $productKey = $software_license["product_key"];
    } else {
        try{
            //this is the main scenario when company_id is passed
            //
            //we do not have product_key yet
            //so we create it 
            $query = DB::prep("INSERT INTO software_license (product_key, company_id) VALUES (SHA1(CONCAT(UUID(), '|mllK23dY88gByuu46x x33j78ip90l-nbvcCrty55fvioAArfiii76jhhhhuuC54D$3dyFVviklo9p-0p-0pl,76,m6mG')), ?)"); 
            $query->execute([$_GET['company_id']]);
    
            $query = DB::prep("select product_key from software_license where company_id=?");
            $query->execute([$_GET['company_id']]);
    
            $software_license = $query->fetch();
            
            $productKey = $software_license["product_key"];
            
        //	$query = DB::prep("select * from software_license where company_id IS NULL LIMIT 1");
        //	$query->execute(array($_GET['company_id']));
        //	if($software_license = $query->fetch()) {
        //		$productKey = $software_license["product_key"];
        //		$query = DB::prep("UPDATE software_license SET company_id = ? WHERE software_license_id = ?");
        //		$query->execute(array($_GET['company_id'],$software_license["software_license_id"]));
        //	}
            
        }catch(Exception $e){
            
        }
        if ($productKey === "") {
            //if key is invalid, we should stop
        	echo "<h1>Could not create personalized software. Please contact us at info@searchquant.net</h1>";
        	die;
        }
    }
}

if ($_GET['type'] == 'mac') {
    //$targetFilename = "ViewGenInstaller.pkg";
    //LEGACY_CODE $filePath = "/var/www/installers/".$productKey.".pkg";
    
    $filePath = $CONFIG['path']['mac_client_binary_url'];
    $filename = make_resulting_filename($filePath);
    
    //LEGACY_CODE if (!filesize($filePath)) {
    //	die("We could not locate your personalized software. Please contact us at info@viewgentools.com");
    //}
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header(sprintf('Content-Disposition: attachment; filename="%s"', $filename)); //<<< Note the " " surrounding the file name
    header('Content-Transfer-Encoding: binary');
    header('Connection: Keep-Alive');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filePath));
    
    $fp = fopen($filePath, 'rb');
    fpassthru($fp);
	die;
    
	
} else if ($_GET['type'] == 'windows') {
	// Create windows package

	//LEGACY_CODE $filename = get_latest_windows_installer_file();
	$filePath = $CONFIG['path']['windows_client_binary_url'];
    $filename = make_resulting_filename($filePath);

	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
    header(sprintf('Content-Disposition: attachment; filename="%s"', $filename)); //<<< Note the " " surrounding the file name
	header('Content-Transfer-Encoding: binary');
	header('Connection: Keep-Alive');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
    header('Content-Length: ' . filesize($filePath));
    
    $fp = fopen($filePath, 'rb');
    fpassthru($fp);
	die;
	
}else{
    header("HTTP/1.0 404 Not Found");
   	echo "<h1>404 Not Found</h1>";
   	die;
}

?>
