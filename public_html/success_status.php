<?php

$HTML_TITLE = "Success Status";
require_once ('lib/recurly.php');
require_once('header.php');
include('api/htmlStorage.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');


$subscriptions = Recurly_SubscriptionList::getActive(['per_page' => 200]);
$uuIdToSubscription = [];
foreach ($subscriptions as $i => $subscription) {

	$uuIdToSubscription[$subscription->uuid] = $subscription;

}

$query = "SELECT 
company.subscription_plan_code,
company.email,
rc_subscription.uuId,
company_name,
li_users.name username,
company_created,
linked_in_id,
company.id company_id,
people_count
FROM company
LEFT JOIN rc_subscription ON rc_subscription.company_id = company.id 
LEFT JOIN li_users ON li_users.company_id = company.id
WHERE company.state != 'draft' AND company.state != 'draft KILLED' AND company.id >= 1137
ORDER BY company.id DESC
";
// AND li_users.linked_in_id

$non_touchable = [1468, 1448, 1443, 1439, 1424, 1412, 1411, 1383, 1379, 1366, 1363, 1353, 1353, 1352, 1350, 1349, 1341, 1338];

echo "</div><table border=1>
<tr>
	<th style='padding: 0px;margin: 0px;'>Co.ID</th>
	<th style='padding: 0px;margin: 0px;'>Signed Up</th>
	<th style='padding: 0px;margin: 0px;'>Company</th>
	<th style='padding: 0px;margin: 0px;'>Email</th>
	<th style='padding: 0px;margin: 0px;'>User</th>
	<th style='padding: 0px;margin: 0px;'>LI Subscription</th>
	<th style='padding: 0px;margin: 0px;'>Last Snapshot</th>
	<th style='padding: 0px;margin: 0px;'>Visits</th>
	<th style='padding: 0px;margin: 0px;'>Avg. Views before SQ</th>
	<th style='padding: 0px;margin: 0px;'>Avg. Views after SQ</th>
	<th style='padding: 0px;margin: 0px;'>SQ Subscription</th>
	<th style='padding: 0px;margin: 0px;'></th>
	<th style='padding: 0px;margin: 0px;'></th>
	<th style='padding: 0px;margin: 0px;'></th>
	<th style='padding: 0px;margin: 0px;'>Email LI SUB.</th>
	<th style='padding: 0px;margin: 0px;'>Email LI Sales Navigator</th>
	<th style='padding: 0px;margin: 0px;'>Email LI Job Seeker</th>
	<th style='padding: 0px;margin: 0px;'>Email Canceled</th>

</tr>
";

$result = mysql_query($query);
while($data = mysql_fetch_array($result)){
	if ($data["company_created"] == "0000-00-00 00:00:00") {
		$data["company_created"] = "2016-06-02 00:00:00";
	}
	$errors = array();
	$subscription = array();
	$rawSubscription = array();
	$htmlDump = "";
	$beforeViewsAverage = "";
	$afterViewsAverage = "";
	$largestUrl = "";
	$beforeCount = 0;
	$beforeSum = 0;
	$afterCount = 0;
	$afterSum = 0;
	$activeCampaign ="";
	if ($data["linked_in_id"] != "") {
		
		$activeCampaignQuery = "SELECT * 
			FROM campaign 
			WHERE 
				status = 'active' AND
				campaign.visit_by_id = '".qq($data["linked_in_id"])."'
			ORDER BY priority DESC, id DESC
			LIMIT 1";
		$activeCampaignResult = mysql_query($activeCampaignQuery);
		if ($activeCampaignData = mysql_fetch_array($activeCampaignResult)) {
			$activeCampaign = $activeCampaignData["name"];
		} else {
			$activeCampaign = "<b><font color=red>No active Campaign</font></b>";
		}
		
		
		$largestUrlQuery = "SELECT * 
			FROM search_url 
			WHERE 
				search_url.visit_by_id = '".qq($data["linked_in_id"])."'
			ORDER BY total_matches DESC
			LIMIT 1";
		$largestUrlResult = mysql_query($largestUrlQuery);
		if ($largestUrlData = mysql_fetch_array($largestUrlResult)) {
			$largestUrl = "<a href=visitedProfiles.php?company_id=".$data["company_id"]."&search_url_id=".$largestUrlData["id"].">".$largestUrlData["total_matches"]."</a>";
		}
		
		$peopleListQuery = "SELECT * 
			FROM people_list 
			WHERE 
				people_list.crawled_by_id = '".qq($data["linked_in_id"])."' AND
				last_list = 1
			ORDER BY id DESC
			LIMIT 1";
		$peopleListResult = mysql_query($peopleListQuery);
		if ($peopleListData = mysql_fetch_array($peopleListResult)) {
			$peopleListHtml = HtmlStorage::retrieve('people_list', $peopleListData['crawled_by_id'], $peopleListData['id']);
			
			//print_r($peopleListData);
			//echo $peopleListHtml->html;
			//	die;
			if ($peopleListHtml = json_decode($peopleListHtml) and strpos($peopleListHtml->html,"reached the commercial use limit on search")) {
				$errors[] = "<b><font color=red>Reached the commercial use limit on search</font></b>";
			}
		}
		$htmlQuery = "SELECT * 
			FROM htmlDump 
			WHERE 
				htmlDump.crawled_by_id = '".qq($data["linked_in_id"])."'
			ORDER BY htmlDumpId DESC
			LIMIT 1";
		$htmlResult = mysql_query($htmlQuery);
		
		if ($htmlData = mysql_fetch_array($htmlResult)) {
			$html = HtmlStorage::retrieve("htmlDump", $htmlData['crawled_by_id'], $htmlData['htmlDumpId']);
			if (strpos($html,"<title>Sign In | LinkedIn</title>")) {
				$subscription[] = "<s>Is signed out from LinkedIn</s>";
			}
			
			if (strpos($html,": Business Plus")) {
				$subscription[] = "<b>Account: Business Plus</b>";
				$rawSubscription[] = "Business Plus";
			} else if (strpos($html,"Account: Business")) {
				$subscription[] = "<font color=green>Account: Business</font>";
				$rawSubscription[] = "Business";
			}
			if (strpos($html,"Account: Basic") or strpos($html,"Cuenta: Básica")) {
				$subscription[] = "<font color=red>Account: Basic</font>";
				$rawSubscription[] = "Basic";
			}
			if (strpos($html,"Account: Executive")) {
				$subscription[] = "<b><i>Account: Executive</i></b>";
				$rawSubscription[] = "Executive";
			}
			if (strpos($html,"Account: Talent Basic")) {
				$subscription[] = "<font color=purple>Account: Talent Basic</font>";
				$rawSubscription[] = "Talent Basic";
			}
			if (strpos($html,"Sales Navigator")) {
				$subscription[] = "<font color=darkorange>Sales Navigator</font>";
				$rawSubscription[] = "Sales Navigator";
			}
			if (strpos($html,"Account: Job Seeker")) {
				$subscription[] = "<font color=gray>Account: Job Seeker</font>";
				$rawSubscription[] = "Job Seeker";
			}
			$htmlDump = "<a href=/viewHtmlDump.php?company_id=".$data["company_id"]."&html_id=".$htmlData["htmlDumpId"].">".$htmlData["htmlDumpCreated"]."</a>";
		}
		$viewBacks = array();
		$visitsQuery = "SELECT * FROM visitors WHERE linked_in_user_id = '".qq($data["linked_in_id"])."'";

		
		$visitsResult = mysql_query($visitsQuery);
		
		
		while ($visitsData = mysql_fetch_array($visitsResult)) {
			if ($visitsData["date"] > $data["company_created"]) {
				$afterCount++;
				$afterSum += $visitsData["visitor_count"];
			} else {
				$beforeCount++;
				$beforeSum += $visitsData["visitor_count"];
			}
			
		}
	}
	
	if ($beforeCount > 0) {
		$beforeViewsAverage = round($beforeSum/$beforeCount,0);
	} else {
		$beforeViewsAverage = "No Data available";
	}
	if ($afterCount > 0) {
		$afterViewsAverage = round($afterSum/$afterCount,0);
		if ($afterViewsAverage > $beforeViewsAverage*2 ) {
			$afterViewsAverage = "<b><font color=darkgreen>$afterViewsAverage</font></b>";
		} else if ($afterViewsAverage > $beforeViewsAverage) {
			$afterViewsAverage = "<font color=darkgreen>$afterViewsAverage</font>";
		} else {
			$afterViewsAverage = "<font color=red>$afterViewsAverage</font>";
		}
	} else {
		$afterViewsAverage = "No Data available";
	}
	$sqSubscription = "";
	if (isset($uuIdToSubscription[$data["uuId"]])) {
		if (isset($uuIdToSubscription[$data["uuId"]]->plan)) {
			$sqSubscription = $uuIdToSubscription[$data["uuId"]]->plan->name;
			$sqSubscription .= " - $".round($uuIdToSubscription[$data["uuId"]]->unit_amount_in_cents*0.01);
		}
		
	} else if ($data["subscription_plan_code"] == "noLimit"){
		$sqSubscription = "Free Unlimited Usage";
	} else if ($data["subscription_plan_code"] == "free"){
		$sqSubscription = "50 Visits Per Day";
	} else {
		$sqSubscription = "Cancelled";
	}

	$queryEmails = DB::prep("SELECT * FROM email_log WHERE email = ? AND company_id = ?");
	$queryEmails->execute([
		$data['email'],
		$data['company_id']
	]);

	$sentEmails = [];
	while ($email_log = $queryEmails->fetch()) {
		$sentEmails[$email_log['email_type']] = $email_log;
	}

	$emailLiSub = "";
	$hasGoodSubscription = false;
	$badSubscription = "";
	foreach ($rawSubscription as $rawSub) {
		if ($rawSub == "Executive" || $rawSub == "Business Plus") {
			$hasGoodSubscription = true;
		}
		$badSubscription = $rawSub;
	}
	if (!$hasGoodSubscription && $badSubscription != "" && $sqSubscription != "") {
		if (isset($sentEmails['emailLISub'])) {
			$emailLiSub = "<small>SENT " . date('m/d', strtotime($sentEmails['emailLISub']['created'])) . "</small>";
		} else {
			$emailLiSub = "<a class='sent-automated-email' data-type='emailLISub' data-email='$data[email]' data-account-type='$badSubscription' data-company-id='$data[company_id]'><u>Send</u></a>";
		}
	}

	$emailLiSub2 = "";
	$hasGoodSubscription = false;
	$badSubscription = "";
	foreach ($rawSubscription as $rawSub) {
		if ($rawSub == "Executive" || $rawSub == "Business Plus") {
			$hasGoodSubscription = true;
		}
		$badSubscription = $rawSub;
	}
	if (!$hasGoodSubscription && $badSubscription != "" && $sqSubscription != "") {
		if (isset($sentEmails['emailLISub2'])) {
			$emailLiSub2 = "<small>SENT " . date('m/d', strtotime($sentEmails['emailLISub2']['created'])) . "</small>";
		} else {
			$emailLiSub2 = "<a class='sent-automated-email' data-type='emailLISub2' data-email='$data[email]' data-account-type='$badSubscription' data-company-id='$data[company_id]' data-name='$data[username]'><u>Send</u></a>";
		}
	}

	$emailLiSub3 = "";
	$hasGoodSubscription = false;
	$badSubscription = "";
	foreach ($rawSubscription as $rawSub) {
		if ($rawSub == "Executive" || $rawSub == "Business Plus") {
			$hasGoodSubscription = true;
		}
		$badSubscription = $rawSub;
	}
	if (!$hasGoodSubscription && $badSubscription != "" && $sqSubscription != "") {
		if (isset($sentEmails['emailLISub3'])) {
			$emailLiSub3 = "<small>SENT " . date('m/d', strtotime($sentEmails['emailLISub3']['created'])) . "</small>";
		} else {
			$emailLiSub3 = "<a class='sent-automated-email' data-type='emailLISub3' data-email='$data[email]' data-account-type='$badSubscription' data-company-id='$data[company_id]' data-name='$data[username]'><u>Send</u></a>";
		}
	}

	$emailCanceled = "";
	if ($sqSubscription == "Cancelled") {
		if (isset($sentEmails['emailCanceled'])) {
			$emailCanceled = "<small>SENT " . date('m/d', strtotime($sentEmails['emailCanceled']['created'])) . "</small>";
		} else {
			$emailCanceled = "<a class='sent-automated-email' data-type='emailCanceled' data-email='$data[email]' data-account-type='$badSubscription' data-company-id='$data[company_id]' data-name='$data[username]'><u>Send</u></a>";
		}
	}

	if (in_array($data['company_id'], $non_touchable)) {
		echo "<tr style='background: red;'>";
	} else {
		echo "<tr>";
	}
	echo "
		<td>$data[company_id]</td>
		<td>".date("M d Y", strtotime($data["company_created"]))."</td>
		<td><a href=dashboard.php?company_id=$data[company_id]>$data[company_name]</a></td>
		<td>$data[email]</td>
		<td>$data[username]</td>
		<td>".implode(" AND ",$subscription)."</td>
		
		<td>$htmlDump</td>
		<td>$data[people_count]</td>
		<td><a href=reports.php?company_id=$data[company_id]>$beforeViewsAverage</a></td>
		<td><a href=reports.php?company_id=$data[company_id]>$afterViewsAverage</a></td>
		<td>$sqSubscription</td>
		<td>$largestUrl</td>
		<td>$activeCampaign</td>
		<td>".implode("",$errors)."</td>
		<td>
			$emailLiSub
		</td>
		<td>
			$emailLiSub2
		</td>
		<td>
			$emailLiSub3
		</td>
		<td>
			$emailCanceled
		</td>
		</tr>";
}
echo "</table>";
?>
<script type="text/javascript" src="/assets/js/admin.js"></script>
