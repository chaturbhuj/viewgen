<?php
$HTML_TITLE = "Referrals Admin";
require_once('header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

if ($_POST['action'] == "Paid") {

	$query = DB::prep("UPDATE referer SET paid_to_paypal = 1, payment_date = NOW(), paid_to_paypal_account =
		:paid_to_paypal_account WHERE referer_id = :referer_id");
	$query->execute([
		'referer_id' => $_POST['referer_id'],
		'paid_to_paypal_account' => $_POST['paid_to_paypal_account']
	]);

	redirect('adminReferer.php');
}

$query = DB::prep("SELECT referer.*, company.*, refered_by_user.*, refered_company.company_name AS refered_company_name
	FROM referer JOIN company ON (company.id = referer.refered_by_company)
	JOIN company AS refered_company ON (refered_company.id = referer.refered_company_id)
	JOIN li_users AS refered_by_user ON (refered_by_user.id = referer.refered_by_li_user)
	WHERE referer_first_payment IS NOT NULL AND refered_by_user.referer_paypal_account IS NOT NULL");
$query->execute();

?>

<table class="bordered-table" style="width: 100%;">
	<thead>
		<tr>
			<th>Company</th>
			<th>Refered Company</th>
			<th>PayPal</th>
			<th>Amount</th>
			<th>Referred Date</th>
			<th>Paid Date</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<? while ($row = $query->fetch()) { ?>
		<tr<?=$row['paid_to_paypal'] ? ' class="got-viewback"' : ''?>>
			<td><?=$row['company_name']?></td>
			<td><?=$row['refered_company_name']?></td>
			<td><?=$row['paid_to_paypal'] ? $row['paid_to_paypal_account'] : $row['referer_paypal_account']?></td>
			<td>75$</td>
			<td><?=date('Y-m-d', strtotime($row['referer_first_payment']))?></td>
			<td><?=$row['payment_date'] ? date('Y-m-d', strtotime($row['payment_date'])) : ''?></td>
			<td>
				<? if ($row['paid_to_paypal'] == 0) { ?>
					<form method="post">
						<input type="hidden" name="referer_id" value="<?=$row['referer_id']?>">
						<input type="hidden" name="paid_to_paypal_account" value="<?=$row['referer_paypal_account']?>">
						<button type="submit" name="action" value="Paid">Paid</button>
					</form>
				<? } ?>
			</td>
		</tr>
<? } ?>
	</tbody>
