<?php

include_once(__DIR__ . '/Email.php');

class EmailCustomTemplate {

    public $emailTemplate = null;
    public $id = null;
    public $status = '';
    public $name = '';
    public $subject = '';
    public $body = '';
    public $variables = [];
    public $unsubscribeType = '';
    public $order = null;
    public $sentCount = 0;

    public $to = '';


	public function __construct($emailTemplateName) {
        $query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateName = :emailTemplateName");
        $query->execute([
            'emailTemplateName' => $emailTemplateName
        ]);
        if ($query->rowCount() == 0) {
            throw new ErrorException('Unknown template');
        }

        $this->emailTemplate = $query->fetch();
        $this->id = $this->emailTemplate['emailTemplateId'];
        $this->status = $this->emailTemplate['emailTemplateStatus'];
        $this->name = $this->emailTemplate['emailTemplateName'];
        $this->subject = $this->emailTemplate['emailTemplateSubject'];
        $this->body = $this->emailTemplate['emailTemplateBody'];

        $vars = json_decode($this->emailTemplate['availableVariables']);
        $this->variables = !empty($vars) ? array_fill_keys($vars, null) : [];

        $this->unsubscribeType = $this->emailTemplate['unsubscribeType'];
        $this->order = $this->emailTemplate['emailTemplateOrder'];
        $this->sentCount = $this->emailTemplate['numberOfSentEmails'];
	}

	public function sendEmail($isPreview = false) {
		if (!$isPreview && !$this->isActive()) {
			return false;
		}

        $this->updateAvailableVars();

		try {
			$email = new Email();
			$emailSubject = substituteEmailVariables($this->subject, $this->variables);
			$emailBody = substituteEmailVariables($this->body, $this->variables);

			$email->setSubject($emailSubject);
			$email->setHtmlBody($emailBody);
			$email->addTo($this->to);
			$email->send();

			if (!$isPreview) {
                $this->logSentEmail($emailSubject, $emailBody);
				$this->updateSentCounter();
			}
		} catch (Exception $error) {
			//var_dump($error);
			return false;
		}
		return true;
	}

	public function updateAvailableVars(){
        $query = DB::prep("
                            UPDATE 
                              emailTemplate
                            SET
                              availableVariables = :availableVariables
                            WHERE
                              emailTemplateId = :id
                          ");
        $query->execute([
            'availableVariables' => json_encode(array_keys($this->variables)),
            'id' => $this->id
        ]);
    }

    public function logSentEmail($emailSubject, $emailBody){
        $query = DB::prep("
                            INSERT INTO
                              email (
                                emailTemplateId,
                                emailSubject,
                                emailBody,
                                recipient,
                                usedVariables
                              )
                            VALUES
                              (?, ?, ?, ?, ?)
                          ");
        $query->execute([
            $this->id,
            $emailSubject,
            $emailBody,
            $this->to,
            json_encode($this->variables)
        ]);
    }

    public function updateSentCounter(){
        $query = DB::prep("
                            UPDATE
                              emailTemplate
                            SET
                              numberOfSentEmails = numberOfSentEmails + 1
                            WHERE
					          emailTemplateId = :emailTemplateId
					      ");
        $query->execute([
            'emailTemplateId' => $this->id
        ]);
    }

    public function isActive(){
        return $this->status === 'active' ? true : false;
    }

}

