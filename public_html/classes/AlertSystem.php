<?php

include('Email.php');
include('api/htmlStorage.php');

abstract class AlertItem {

	protected $mType = "Unknown";
	public $parameters = [];
	private $unsubscribeToken = "";

	public function __construct($type) {
		$this->mType = $type;
	}

	public function setParameter($name, $value) {
		$this->parameters[$name] = $value;
	}

	public function store($company_id, $email, $emailSubject, $emailBody) {
		$query = DB::prep("INSERT INTO sent_alert (company_id, email, emailSubject, emailBody, sent_at, alert_type,
			parameters) VALUES(:company_id, :email, :emailSubject, :emailBody, NOW(), :alert_type, :parameters)");
		$query->execute([
			'company_id' => $company_id,
			'email' => $email,
			'emailSubject' => $emailSubject,
			'emailBody' => $emailBody,
			'alert_type' => $this->mType,
			'parameters' => json_encode($this->parameters)
		]);
	}

	public function getType() {
		return $this->mType;
	}

	public function generateUnsubscribeToken($company_id) {

		// Generates a token if none is present.
		$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
		$query->execute([
			'company_id' => $company_id
		]);

		$company = $query->fetch();
		if ($company['alert_unsubscribe_token'] == "") {

			$this->unsubscribeToken = md5(rand());
			$query = DB::prep("UPDATE company SET alert_unsubscribe_token = :token WHERE id = :company_id");
			$query->execute([
				'token' => $this->unsubscribeToken,
				'company_id' => $company_id
			]);
		} else {
			$this->unsubscribeToken = $company['alert_unsubscribe_token'];
		}
	}

	public function generateUnsubscrubeLink() {
		return sprintf("If you don't want to receive these emails you can unsubscribe <a target=\"_blank\" href=\"https://viewgentool.com/alert_unsubscribe.php?token=%s\">here</a>.", $this->unsubscribeToken);
	}

	public function merge($alert) {
		// Add the alert to this alert.
		if (!isset($this->parameters['merge'])) {
			$this->parameters['merge'] = [];
		}

		$this->parameters['merge'][] = $alert->parameters;
	}

	public function shouldMerge() {
		return true;
	}
}

class PrematureQueryAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertPrematureQueryFinish");
	}

	public function getSubject() {
		return "alert: prematurely finished campaign(s)";
	}

	public function getBody() {

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that, for <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>), LinkedIn is only letting <?=$plural ? 'these users' : 'this user'?> see & visit the first 4 profiles for any query.

This is occurring because <?=$plural ? 'those users' : 'that user'?> does not have either the <b>Business Plus</b> or <b>Executive</b> LinkedIn premium subscription needed to be able to visit all the profiles for the queries in that campaign. As a result, LinkedIn has applied their <b>Commercial Use Limit</b> which only lets you see & visit the first <b>4</b> profiles for any query. Make sure <?=$plural ? 'those users' : 'the user'?> gets a Business Plus or Executive LinkedIn premium subscription, and then <?=$plural ? 'they' : 'he/she'?> will need to re-run the campaign(s).

[You can read about LI's Commercial Use Limit <a href="https://help.linkedin.com/app/answers/detail/a_id/52950/~/commercial-use-limit-on-search/help.linkedin.com/app/answers/detail">here</a>; note that it was introduced January 2015, and changed somewhat in May 2015.]

Thanks,

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
}

class LargeQueriesAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertLargeQueries");
	}

	public function getSubject() {
		return "alert: query with >1000 profiles";
	}

	public function getBody() {

		ob_start();

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		?>
Dear user,

Our automated systems have detected that <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>) has a query that with more than 1000 profiles. Because LinkedIn only lets <?=$plural ? 'them' : 'you'?> see & visit the first 1000 profiles for any one query, if <?=$plural ? 'they' : 'you'?> want to visit all the profiles in this query, we suggest <?=$plural ? 'they' : 'you'?> 'cut' the query into 25-200 smaller queries using the <a href="https://viewgentool.com/geonames/find_location.php">Query Slicer</a> (<a href="http://bit.ly/SQSlicer">1m30s how-to video</a>).

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 3 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
	
}

class NoViewbacksAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertNoViewbacks");
	}

	public function getSubject() {
		return "alert: your View Back Rate (VBR) is low";
	}

	public function getBody() {

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>) <?=$plural ? 'are' : 'is'?> getting fewer Return Profile Views (RPVs) from <?=$plural ? 'their' : 'his/her'?> outbound visits than is normal.

<b><u>Visit Well-Targeted Prospect Profiles</u></b>
Turn your knowledge of your target market into queries that use all the different LinkedIn Advanced People Search parameters you needed to find and visit your target market. Ask yourself what are the industries, job functions, size companies, geographies, job titles, keywords and LinkedIn groups for the prospect base you want to visit.  Also, familiarize yourselves with and use the 'Special Search Types' listed here, which are ways to refine keyword searches on LinkedIn.

<b><u>Make Sure Your Photo & Tagline Are Compelling</u></b>
These profiles should give you some ideas for how you might quickly optimize your own profile for use and value: 
<ul><li><a href="https://www.linkedin.com/in/stevenbenson" target="_blank">Steven Benson</a> (sells customer mapping solution to field salespeople) Good tagline & good photo. </li><li><a href="https://www.linkedin.com/in/phillippearsoncfp" target="_blank">Phillip Pearson</a> (Northwestern Mutual)</li><li><a href="https://www.linkedin.com/in/lindaginac" target="_blank">Linda Ginac</a> - Best tagline of 2015, because it is short but FULL of compelling content</li><li><a href="https://www.linkedin.com/in/gregory-costa-b4a31118" target="_blank">Gregory Costa</a> (President, Sextant Financial & Estate Tax Planning, and user): good tagline, solid, compelling Summary and easy-to-find contact info. </li><li><a href="https://www.linkedin.com/in/stellarhirepartners" target="_blank">Rachel Budlong</a> (agency recruiter), great photo, great tagline. </li><li><a href="https://www.linkedin.com/in/roshawnna" target="_blank">Dr. Roshawnna Novellus</a>, great photo, great tagline, incredibly concise, direct and compelling Summary.  </li></ul>
works well when you visit lots of the right people, and when they see a high-resolution, forward-facing head shot photo coupled with a tagline that compellingly conveys your solution’s value to your target market.  Make these optimizations and your View-Back Rate will soar! 

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
}

class AnonymousAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertAnonymousAccount");
	}

	public function getSubject() {
		return "alert: your LinkedIn account is anonymous";
	}

	public function getBody() {

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>) <?=$plural ? 'are' : 'is'?> getting fewer Return Profile Views (RPVs) from <?=$plural ? 'their' : 'his/her'?> outbound visits than is normal. Check in your LinkedIn privacy settings to make sure <?=$plural ? 'they' : 'you'?> are not set to Anonymous or Semi-Anonymous.

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
}

class OldVersionAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertOldClientVersion");
	}

	public function getSubject() {
		return "alert: update to v3.0 of desktop software";
	}

	public function getBody() {

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>) <?=$plural ? 'are' : 'is'?> running an old desktop client. Please download the latest version either by going to your account or by clicking on one of the following links.

Mac version: <a href="https://viewgentool.com/client/download.php?company_id=<?=$this->parameters['company_id']?>&type=mac" target="_blank">download</a>
Windows version: <a href="https://viewgentool.com/client/download.php?company_id=<?=$this->parameters['company_id']?>&type=windows" target="_blank">download</a>

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
}

class LowVolumeAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertLowVolume");
	}

	public function getSubject() {
		return "alert: low outbound profile visiting volume";
	}

	public function getBody() {

		$otherUsers = "";
		$plural = false;
		if (isset($this->parameters['merge']) and count($this->parameters['merge']) > 0) {
			$plural = true;
			$usernames = [];
			foreach ($this->parameters['merge'] as $user) {
				$usernames[] = $user['linked_in_user'];
			}
			if (count($usernames) == 1) {
				$otherUsers = " and " . $usernames[0];
			} else {
				$otherUsers = ", " . implode(", ", array_slice($usernames, 0, count($usernames) - 1));
				$otherUsers .= ' and ' . $usernames[count($usernames) - 1];
			}
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that <?=$plural ? 'several' : 'one'?> of the users in your account (<?=$this->parameters['linked_in_user']?><?=$otherUsers?>) <?=$plural ? "haven't been" : "hasn't been"?> visiting many profiles using this service. With our new <a href="https://viewgentool.com/geonames/find_location.php?company_id=<?=$this->parameters['company_id']?>">Query Slicer</a> and <a href="https://viewgentool.com/geonames/company_generator.php?company_id=<?=$this->parameters['company_id']?>">Company Query Generator</a>, it should only take <?=$plural ? 'them' : 'you'?> 5-7 minutes to build one or more always-on campaigns that will ensure <?=$plural ? 'they' : 'you'?> visit 500-800 profiles/day for weeks or months on end. <b>Exposure breeds opportunity</b>, so take a minute to build your always-on campaigns.

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 7 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		$hasNew = false;
		if ($query->rowCount() == 0) return true;
		while ($row = $query->fetch()) {
			$parameter = json_decode($row['parameters']);

			$all_ids = [$parameter->linked_in_id];
			if (isset($parameter->merge)) {
				foreach ($parameter->merge as $merge) {
					$all_ids[] = $merge->linked_in_id;
				}
			}
			if (!in_array($this->parameters['linked_in_id'], $all_ids)) $hasNew = true;
			if (isset($this->parameters['merge'])) {
				foreach ($this->parameters['merge'] as $merge) {
					if (!in_array($merge['linked_in_id'], $all_ids)) $hasNew = true;
				}
			}
		}
		return $hasNew;
	}
}

class LoggedOutAlert extends AlertItem {

	public function __construct() {
		parent::__construct("alertLoggedOut");
	}

	public function getSubject() {
		return "alert: one of your accounts are logged out";
	}

	public function getBody() {

		$user_str = "";
		if (isset($this->parameters['linked_in_user'])) {
			$user_str = " (we think it's this user: " . $this->parameters['linked_in_user'] . ")";
		}

		ob_start();
		?>
Dear user,

Our automated systems have detected that, for one of the users in your account<?=$user_str?>, the client software has been logged out from LinkedIn. Try these steps to get it back online: 

a) click on the red/yellow/green circle icon in your computer's task bar; 
b) click on the 'Sign In' button you'll then see (lower left of the dialog box that'll come up); 
c) sign in to your LinkedIn account from within the browser window that will open. 

If steps (a)-(c) don't fix the issue, then try the following steps: 

d) click on the red/yellow/green circle icon in your computer's task bar; 
e) in the dialog box that'll open, click on the cogwheel in the tiny blue box (upper right) and click 'Quit'; 
f) find the app in your downloads folder (or wherever you keep the SQ app) and click on it to re-launch; 

To prevent this from occurring in the future, we just released a new version of the desktop software that automatically logs you back in to your LI account if/when LI logs you out (<a href="https://viewgentool.com/client/download.php?type=windows">Windows</a> and <a href="https://viewgentool.com/client/download.php?type=mac">Mac</a> download links). Once you've installed it, click on the the cogwheel and store your LI credentials.

Thanks, 

Customer Support
support@viewgentool.com

<?=$this->generateUnsubscrubeLink();?>
		<?php
		$body = ob_get_contents();
		ob_end_clean();

		return str_replace("\n", "<br/>", $body);
	}

	// Returns true if the alert should be sent out.
	public function shouldSend($company_id) {
		$query = DB::prep("SELECT * FROM sent_alert WHERE company_id = :company_id AND alert_type = :alert_type AND
			sent_at >= DATE_SUB(NOW(), INTERVAL 3 DAY) ORDER BY sent_at DESC LIMIT 1");
		$query->execute([
			'company_id' => $company_id,
			'alert_type' => $this->mType
		]);

		while ($row = $query->fetch()) {
			// We alerted them today, lets not do it again.
			return false;
		}
		return true;
	}

	public function shouldMerge() {
		return false;
	}
}

class AlertSystem {

	private $sendCount = 0;

	public function __construct() {
		
	}

	public function run() {
		$query = DB::prep("SELECT *
			FROM company
			WHERE
				company.state = '' AND
				company.account_type = 'paid' AND
				send_automatic_alerts = 1 AND
				email NOT LIKE '%NotInUse' AND
				frozen = 0");
		$query->execute();

		while ($company = $query->fetch()) {
			$this->runForCompany($company);
		}
	}

	public function runForCompany($company) {
		$alerts = [];

		echo ".";

		$this->alertPrematureQueryFinish($company, $alerts);
		$this->alertDidNotCreateCampaign($company, $alerts);
		$this->alertLargeQueries($company, $alerts);
		$this->alertNoViewbacks($company, $alerts);
		$this->alertAnonymousAccount($company, $alerts);
		$this->alertOldClientVersion($company, $alerts);
		//$this->alertNoReferrals($company, $alerts);
		$this->alertLowVolume($company, $alerts);
		$this->alertPhrasesInQuotes($company, $alerts);
		$this->alertHighOverlap($company, $alerts);
		$this->alertNotUsingReports($company, $alerts);
		$this->alertClientSoftwareIsOffline($company, $alerts);
		$this->alertLoggedOut($company, $alerts);

		if (count($alerts) > 0) {
			$this->sendAlerts($company, $alerts);
		}
	}

	private function alertPrematureQueryFinish($company, &$alerts) {

		// Find all recent last pages for this company.
		$query = DB::prep("SELECT people_list.* FROM search_url JOIN people_list ON (people_list.search_url_id = search_url.id)
			WHERE company_id = :company_id AND people_list_last_visit >= DATE_SUB(NOW(), INTERVAL 2 HOUR)
			ORDER BY people_list_last_visit DESC");
		$query->execute([
			'company_id' => $company['id']
		]);

		$added_ids = [];
		while ($peopleList = $query->fetch()) {
			if (isset($added_ids[$peopleList['crawled_by_id']])) continue;
			$html = HtmlStorage::retrieve('people_list', $peopleList['crawled_by_id'], $peopleList['id']);

			if (strpos($html, "reached the commercial use limit on search") !== false) {
				$alert = new PrematureQueryAlert();
				$alert->setParameter("linked_in_user", $peopleList['crawled_by']);
				$alert->setParameter("linked_in_id", $peopleList['crawled_by_id']);
				$alert->setParameter("people_list_id", $peopleList['id']);
				$alerts[] = $alert;

				$added_ids[$peopleList['crawled_by_id']] = 1;
			}
		}
	}

	private function alertDidNotCreateCampaign($company, &$alerts) {
	}

	private function alertLargeQueries($company, &$alerts) {
		$query = DB::prep("SELECT * FROM search_url WHERE total_matches > 2000 AND company_id = :company_id AND
			created >= DATE_SUB(NOW(), INTERVAL 1 HOUR)");
		$query->execute([
			'company_id' => $company['id']
		]);

		$added_ids = [];
		while ($search_url = $query->fetch()) {
			if (isset($added_ids[$search_url['visit_by_id']])) continue;

			/* Check if the query contains a zip code. Then just ignore it since it probably comes from the query
				slicer. */
			if (strpos($search_url['url'], "postalCode=") === false) {
				$alert = new LargeQueriesAlert();
				$alert->setParameter("linked_in_user", $search_url['visit_by']);
				$alert->setParameter("linked_in_id", $search_url['visit_by_id']);
				$alerts[] = $alert;

				$added_ids[$search_url['visit_by_id']] = 1;
			}
		}
	}

	private function alertNoViewbacks($company, &$alerts) {

		$query = $this->_getLiUsersByCompanyId($company['id']);

		$added_ids = [];
		while ($li_user = $query->fetch()) {

			// Check that this is your linked in user that you are actually using right now.
			$query2 = DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND account_volume =
				(SELECT MAX(account_volume) FROM li_users WHERE linked_in_id = :linked_in_id)");
			$query2->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			$row = $query2->fetch();
			if ($row['id'] != $li_user['id']) continue;

			// Check that this linked in user has been using the tool more than a week.
			$check = DB::prep("SELECT sent_to_crawler AS first_visit FROM `people` WHERE
				crawled_by_id = :linked_in_id ORDER BY id ASC LIMIT 1");
			$check->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			if ($check->rowCount() == 0) continue;
			$row = $check->fetch();
			if (time() - strtotime($row['first_visit']) < 14*24*3600) {
				// This user is too fresh.
				continue;
			}

			if (isset($added_ids[$li_user['linked_in_id']])) continue;
			if ($li_user['account_volume'] !== null and $li_user['account_vbr'] !== null) {
				if ($li_user['account_vbr'] < 0.027 and $li_user['account_volume'] > 200 and $li_user['account_vbr'] > 0.003) {
					// This is not good. Add alert
					$alert = new NoViewbacksAlert();
					$alert->setParameter("linked_in_user", $li_user['name']);
					$alert->setParameter("linked_in_id", $li_user['linked_in_id']);
					$alerts[] = $alert;

					$added_ids[$li_user['linked_in_id']] = 1;
				}
			}
		}
	}

	private function alertAnonymousAccount($company, &$alerts) {

		$query = $this->_getLiUsersByCompanyId($company['id']);

		$added_ids = [];
		while ($li_user = $query->fetch()) {

			// Check that this is your linked in user that you are actually using right now.
			$query2 = DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND account_volume =
				(SELECT MAX(account_volume) FROM li_users WHERE linked_in_id = :linked_in_id)");
			$query2->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			$row = $query2->fetch();
			if ($row['id'] != $li_user['id']) continue;

			// Check that this linked in user has been using the tool more than a week.
			$check = DB::prep("SELECT sent_to_crawler AS first_visit FROM `people` WHERE
				crawled_by_id = :linked_in_id ORDER BY id ASC LIMIT 1");
			$check->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			if ($check->rowCount() == 0) continue;
			$row = $check->fetch();
			if (time() - strtotime($row['first_visit']) < 14*24*3600) {
				// This user is too fresh.
				continue;
			}

			if (isset($added_ids[$li_user['linked_in_id']])) continue;

			// Check visitors in last epoch.
			$check = DB::prep("SELECT * FROM visitors WHERE linked_in_user_id = :linked_in_user_id AND
				company_id = :company_id ORDER BY id DESC LIMIT 1");
			$check->execute([
				'linked_in_user_id' => $li_user['linked_in_id'],
				'company_id' => $li_user['company_id']
			]);

			if ($check->rowCount() > 0) {
				$row = $check->fetch();
				if ($row['visitor_count'] < 10 AND $row['profiles_visited'] > 100) {
					if ($li_user['new_client_version'] == "Windows 1.0" || $li_user['new_client_version'] == "Mac 1.0") {
						// Probably not anonymous but rather has an old version.
						continue;
					}

					// Most probably anonymous.
					$alert = new AnonymousAlert();
					$alert->setParameter("linked_in_user", $li_user['name']);
					$alert->setParameter("linked_in_id", $li_user['linked_in_id']);
					$alerts[] = $alert;

					$added_ids[$li_user['linked_in_id']] = 1;
				}
			}
		}
	}

	private function alertOldClientVersion($company, &$alerts) {

		$query = $this->_getLiUsersByCompanyId($company['id']);

		$added_ids = [];
		while ($li_user = $query->fetch()) {

			// Check that this is your linked in user that you are actually using right now.
			$query2 = DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND account_volume =
				(SELECT MAX(account_volume) FROM li_users WHERE linked_in_id = :linked_in_id)");
			$query2->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			$row = $query2->fetch();
			if ($row['id'] != $li_user['id']) continue;

			if ($li_user['new_client_version'] == "Windows 1.0") {
				$alert = new OldVersionAlert();
				$alert->setParameter("linked_in_user", $li_user['name']);
				$alert->setParameter("linked_in_id", $li_user['linked_in_id']);
				$alert->setParameter("os", "windows");
				$alert->setParameter("company_id", $li_user['company_id']);
				$alerts[] = $alert;

				$added_ids[$li_user['linked_in_id']] = 1;
				continue;
			}

			// Check that this linked in user has been using the tool more than a week.
			$check = DB::prep("SELECT sent_to_crawler AS first_visit FROM `people` WHERE
				crawled_by_id = :linked_in_id ORDER BY id ASC LIMIT 1");
			$check->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			if ($check->rowCount() == 0) continue;
			$row = $check->fetch();
			if (time() - strtotime($row['first_visit']) < 14*24*3600) {
				// This user is too fresh.
				continue;
			}

			if (isset($added_ids[$li_user['linked_in_id']])) continue;

			// Check visitors in last epoch.
			$check = DB::prep("SELECT * FROM visitors WHERE linked_in_user_id = :linked_in_user_id AND
				company_id = :company_id ORDER BY id DESC LIMIT 1");
			$check->execute([
				'linked_in_user_id' => $li_user['linked_in_id'],
				'company_id' => $li_user['company_id']
			]);

			if ($check->rowCount() > 0) {
				$row = $check->fetch();
				if ($row['visitor_count'] < 10 AND $row['profiles_visited'] > 100) {
					if ($li_user['new_client_version'] == "Mac 1.0") {
						$alert = new OldVersionAlert();
						$alert->setParameter("linked_in_user", $li_user['name']);
						$alert->setParameter("linked_in_id", $li_user['linked_in_id']);
						$alert->setParameter("os", "mac");
						$alert->setParameter("company_id", $li_user['company_id']);
						$alerts[] = $alert;

						$added_ids[$li_user['linked_in_id']] = 1;
						continue;
					}
				}
			}
		}
	}

	private function alertNoReferrals($company, &$alerts) {
	}

	private function alertLowVolume($company, &$alerts) {

		$query = $this->_getLiUsersByCompanyId($company['id']);

		$added_ids = [];
		while ($li_user = $query->fetch()) {

			// Check that this is your linked in user that you are actually using right now.
			$query2 = DB::prep("SELECT * FROM li_users WHERE linked_in_id = :linked_in_id AND account_volume =
				(SELECT MAX(account_volume) FROM li_users WHERE linked_in_id = :linked_in_id)");
			$query2->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			$row = $query2->fetch();
			if ($row['id'] != $li_user['id']) continue;

			// Check that this linked in user has been using the tool more than a week.
			$check = DB::prep("SELECT sent_to_crawler AS first_visit FROM `people` WHERE
				crawled_by_id = :linked_in_id ORDER BY id ASC LIMIT 1");
			$check->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);

			if ($check->rowCount() == 0) continue;
			$row = $check->fetch();
			if (time() - strtotime($row['first_visit']) < 14*24*3600) {
				// This user is too fresh.
				continue;
			}

			if (isset($added_ids[$li_user['linked_in_id']])) continue;

			// Count volume.
			$count = DB::prep("SELECT SUM(number_of_visits) AS volume FROM reports_cache WHERE
				li_users_id = :linked_in_id AND cache_date >= DATE(DATE_SUB(NOW(), INTERVAL 7 DAY))");
			$count->execute([
				'linked_in_id' => $li_user['linked_in_id']
			]);
			$row = $count->fetch();
			if ($row['volume'] < 900) {
				// This is not good. Add alert
				$alert = new LowVolumeAlert();
				$alert->setParameter("linked_in_user", $li_user['name']);
				$alert->setParameter("linked_in_id", $li_user['linked_in_id']);
				$alert->setParameter("company_id", $li_user['company_id']);
				$alerts[] = $alert;

				$added_ids[$li_user['linked_in_id']] = 1;
			}
		}
	}

	private function alertPhrasesInQuotes($company, &$alerts) {
	}

	private function alertHighOverlap($company, &$alerts) {
	}

	private function alertNotUsingReports($company, &$alerts) {
	}

	private function alertClientSoftwareIsOffline($company, &$alerts) {
	}

	private function alertLoggedOut($company, &$alerts) {
		// session_key-login
		// re getting things cleaned up
		// "session_key" id="login-email"

		// Check if this is a single user account.
		$query = $this->_getLiUsersByCompanyId($company['id']);
		$singleUserAccount = false;
		if ($query->rowCount() == 1) {
			$singleUserAccount = true;
		}

		// Look at the latest hour of html dumps for this company.

		$query = DB::prep("SELECT * FROM `htmlDump` WHERE companyId = :company_id AND
			htmlDumpCreated >= DATE_SUB(NOW(), INTERVAL 1 HOUR) ORDER BY htmlDumpCreated DESC");
		$query->execute([
			'company_id' => $company['id']
		]);

		$htmlDumps = $query->fetchAll();

		$ok_ips = [];
		foreach ($htmlDumps as $html_dump) {
			if (in_array($html_dump['ip'], $ok_ips) || (count($ok_ips) > 0 && $singleUserAccount)) continue;
			if ($html_dump['crawled_by_id'] == "") {
				// Investigate!
				$html = HtmlStorage::retrieve('htmlDump', "", $html_dump['htmlDumpId']);

				$loggedOut = false;
				if (preg_match('#session_key-login#is', $html)) {
					$loggedOut = true;
				}
				if (preg_match('#"session_key" id="login-email"#is', $html)) {
					$loggedOut = true;
				}

				if ($loggedOut) {
					$alert = new LoggedOutAlert();

					// Try to find the user with same ip.
					foreach ($htmlDumps as $other_html_dump) {
						if ($other_html_dump['ip'] == $html_dump['ip'] and $other_html_dump['crawled_by_id'] != "") {
							$alert->setParameter("linked_in_user", $other_html_dump['crawled_by']);
							$alert->setParameter("linked_in_id", $other_html_dump['crawled_by_id']);
						}
					}

					$alerts[] = $alert;
					break;
				}
			} else {
				$ok_ips[] = $html_dump['ip'];
			}
		}
	}

	private function mergeAlerts($alerts) {
		// Merge all alerts that are of the same type so that we don't send out similar emails to customers.

		$merged = [];
		foreach ($alerts as $alert) {
			if (!$alert->shouldMerge()) {
				$merged[] = $alert;
				continue;
			}
			if (!isset($merged[$alert->getType()])) {
				$merged[$alert->getType()] = $alert;
				continue;
			}
			$merged[$alert->getType()]->merge($alert);
		}

		return $merged;
	}

	private function sendAlerts($company, $alerts) {

		if (trim($company['automatic_alerts_email']) != "") {
			$emailAdresses = explode("\n", trim(str_replace("\r", "", $company['automatic_alerts_email'])));
		} else {
			$emailAdresses = [$company['email']];
		}

		$alerts = $this->mergeAlerts($alerts);

		foreach ($alerts as $alert) {
			if (!$alert->shouldSend($company['id'])) {
				echo ".";
				continue;
			}

			foreach ($emailAdresses as $emailAdress) {

				$alert->generateUnsubscribeToken($company['id']);
				//$alert->store($company['id'], $emailAdress, $alert->getSubject(), $alert->getBody());

				echo "\n\nWill send alert (" . $alert->getType() . ") to $emailAdress about user " .
					(isset($alert->parameters['linked_in_user']) ? $alert->parameters['linked_in_user'] : 'UNKNOWN') . "\n";

				echo "Subject: " . $alert->getSubject() . "\n\n";
				echo "Email: \n" . $alert->getBody() . "\n\n\n";
				$this->sendCount++;

				file_put_contents("preview_alerts.html", "<h2>Subject: " . $alert->getSubject() . "</h2>", FILE_APPEND);
				file_put_contents("preview_alerts.html", "<h5>To: " . $emailAdress . "</h5>", FILE_APPEND);
				file_put_contents("preview_alerts.html", "<p>" . $alert->getBody() . "</p>", FILE_APPEND);
				file_put_contents("preview_alerts.html", "<br /><br /><br />", FILE_APPEND);

				/*$email = new Email();
				$email->setSubject($alert->getSubject());
				$email->addTo($emailAdress);
				$email->addReplyTo("support@viewgentool.com");
				$email->setHtmlBody($alert->getBody());
				$email->send();*/
				echo "Send Count: " . $this->sendCount . "\n\n";
			}
		}
	}

	/**
	 * returns query execution results li_users
	 *
	 * @param int $id  company id
	 *
	 * @return PDOStatement result
	 */
	private function _getLiUsersByCompanyId($id){
		$query = DB::prep("
			SELECT lu.*, cv.name as new_client_version 
				FROM li_users lu LEFT JOIN client_versions cv ON lu.client_version_id = cv.id 
				WHERE lu.company_id = :company_id AND lu.hidden = 0
		");
		$query->execute([
			'company_id' => $id
		]);
		return $query;
	}

}

?>
