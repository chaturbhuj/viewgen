<?php

include(dirname(__FILE__) . '/../lib/recurly_loader.php');
include_once(dirname(__FILE__) . '/../db.php');

class RefererTracking {

	static $creditsForRefer = 75;

	public static function runTracking() {

		$query = DB::prep("SELECT referer.*, company.*, rc_account.*, refered_by.referer_paypal_account FROM referer JOIN
			company ON (refered_company_id = company.id) JOIN rc_account ON (company.id = rc_account.company_id)
			JOIN li_users AS refered_by ON (refered_by_li_user = refered_by.id) WHERE referer_first_payment IS NULL");
		$query->execute();

		while ($company = $query->fetch()) {
			$account_code = $company['account_code'];

			// Select the company that refered this person.
			$selectQuery = DB::prep("SELECT * FROM company JOIN rc_account ON (company.id = rc_account.company_id)
				WHERE company.id = :company_id");
			$selectQuery->execute([
				'company_id' => $company['refered_by_company']
			]);

			if ($selectQuery->rowCount() == 0) {
				// This company has no recurly account. Ignore.
				continue;
			}

			$referedBy = $selectQuery->fetch();

			try {
				$transactions = Recurly_TransactionList::getForAccount($account_code);

				// Check for at least one real transaction.
				$hasRealTransaction = false;
				foreach ($transactions as $transaction) {
					if ($transaction->status == "success" && $transaction->amount_in_cents > 10000) {
						$hasRealTransaction = true;
					}
				}

				echo "Got company with real transaction: " . $company['name'] . "\n";

				if ($hasRealTransaction and $company['referer_paypal_account'] == null) {
					// Give away credits.
					$updateQuery = DB::prep("UPDATE referer SET referer_first_payment = :transaction_time,
						payment_date = NOW(), paid_as_credit = 1 WHERE referer_id = :referer_id");
					$updateQuery->execute([
						'transaction_time' => $transaction->created_at->format('Y-m-d H:i:s'),
						'referer_id' => $company['referer_id']
					]);

					$credit = new Recurly_Adjustment();
					$credit->account_code = $referedBy['account_code'];
					$credit->description = 'Earnings for refering ' . $company['company_name'];
					$credit->unit_amount_in_cents = -(self::$creditsForRefer * 100); // Negative value in cents
					$credit->currency = 'USD';
					$credit->quantity = 1;
					$credit->create();
				} elseif ($hasRealTransaction) {
					$updateQuery = DB::prep("UPDATE referer SET referer_first_payment = :transaction_time WHERE
						referer_id = :referer_id");
					$updateQuery->execute([
						'transaction_time' => $transaction->created_at->format('Y-m-d H:i:s'),
						'referer_id' => $company['referer_id']
					]);
				}
			} catch (Recurly_NotFoundError $e) {
				print "Account Not Found: $e";
			}
		}
	}

}

?>
