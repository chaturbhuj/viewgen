<?php

include_once(__DIR__ . '/Email.php');

class EmailTemplate {

	private $unsubscribeToken = "";
	private $unsubscribeData = [];

	public function __construct($company_id) {
		$this->generateUnsubscribeToken($company_id);

		// Load unsubscribe data.
		$query = DB::prep("SELECT * FROM emailUnsubscribed WHERE company_id = :company_id");
		$query->execute([
			'company_id' => $company_id
		]);
		$this->unsubscribeData = $query->fetchAll();
	}

	public function sendEmail($emailAddress, $emailTemplateName, $variables, $isPreview = false) {
		if (!$isPreview) {
			$query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateName = :emailTemplateName AND
				emailTemplateStatus = 'active'");
		} else {
			$query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateName = :emailTemplateName");
		}
		$query->execute([
			'emailTemplateName' => $emailTemplateName
		]);
		if ($query->rowCount() == 0) {
			return false;
		}

		$emailTemplate = $query->fetch();
		$query = DB::prep("UPDATE emailTemplate SET availableVariables = :availableVariables WHERE emailTemplateId =
			:id");
		$query->execute([
			'availableVariables' => json_encode(array_keys($variables)),
			'id' => $emailTemplate['emailTemplateId']
		]);

		foreach ($this->unsubscribeData as $row) {
			if ($row['unsubscribeType'] == $emailTemplate['unsubscribeType']) {
				return false;
			}
		}

		try {
			$email = new Email();
			$emailSubject = substituteEmailVariables($emailTemplate['emailTemplateSubject'], $variables);
			$emailBody = substituteEmailVariables($emailTemplate['emailTemplateBody'], $variables);

			$emailBody .= $this->generateUnsubscrubeLink();
			$email->setSubject($emailSubject);
			$email->setHtmlBody($emailBody);
			$email->addTo($emailAddress);
			$email->send();

			if (!$isPreview) {
				$query = DB::prep("INSERT INTO email (emailTemplateId, emailSubject, emailBody, recipient,
					usedVariables) VALUES(?, ?, ?, ?, ?)");
				$query->execute([
					$emailTemplate['emailTemplateId'],
					$emailSubject,
					$emailBody,
					$emailAddress,
					json_encode($variables)
				]);

				$query = DB::prep("UPDATE emailTemplate SET numberOfSentEmails = numberOfSentEmails + 1 WHERE
					emailTemplateId = :emailTemplateId");
				$query->execute([
					'emailTemplateId' => $emailTemplate['emailTemplateId']
				]);
			}
		} catch (Exception $error) {
			var_dump($error);
			return false;
		}
	}

	public function generateUnsubscribeToken($company_id) {

		// Generates a token if none is present.
		$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
		$query->execute([
			'company_id' => $company_id
		]);

		$company = $query->fetch();
		if ($company['alert_unsubscribe_token'] == "") {

			$this->unsubscribeToken = md5(rand());
			$query = DB::prep("UPDATE company SET alert_unsubscribe_token = :token WHERE id = :company_id");
			$query->execute([
				'token' => $this->unsubscribeToken,
				'company_id' => $company_id
			]);
		} else {
			$this->unsubscribeToken = $company['alert_unsubscribe_token'];
		}
	}

	public function generateUnsubscrubeLink() {
		return sprintf("<center><a href=\"%s\"><u>Unsubscribe</u></a></center>",
			base_url('alert_unsubscribe.php?token=' . $this->unsubscribeToken));
	}

}

?>
