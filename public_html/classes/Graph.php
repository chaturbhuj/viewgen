<?php

class Graph {

	private $name;
	private $values;
	private $labels;

	public function __construct($name) {
		$this->name = $name;
		$this->values = [];
		$this->labels = [];
	}

	public function getOutput() {

		$graphData = [];
		$graphLabels = [];

		$numLines = count($this->values);
		foreach ($this->values as $line => $values) {
			if (isset($this->labels[$line])) {
				$graphLabels[] = $this->labels[$line];
			} else {
				$graphLabels[] = "Line " . $line;
			}
			foreach ($values as $x => $y) {
				if (!isset($graphData[$x])) {
					$graphData[$x] = array_fill(0, $numLines + 1, null);
					$graphData[$x][0] = $x;
				}
				$graphData[$x][$line + 1] = $y;
			}
		}
		ksort($graphData);
		$graphData = array_values($graphData);

		$output = makeGraph($graphData, "100%", "400px", $graphLabels);
		return $output;
	}

	public function incrementValue($xValue, $yValue, $line = 0) {
		$this->addLine($line);
		if (!isset($this->values[$line][$xValue])) {
			$this->values[$line][$xValue] = 0;
		}
		$this->values[$line][$xValue] += $yValue;
	}

	public function setValue($xValue, $yValue, $line = 0) {
		$this->addLine($line);
		$this->values[$line][$xValue] = $yValue;
	}

	public function addLine($line) {
		if (!isset($this->values[$line])) {
			$this->values[$line] = [];
		}
	}

	public function setLabel($line, $label) {
		$this->labels[$line] = $label;
	}

}

?>
