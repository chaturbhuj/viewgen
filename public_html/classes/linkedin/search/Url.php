<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 23.08.18
 * Time: 21:53
 */

namespace linkedinSearch;

require_once(__DIR__ . "/sales/Url_V0.php");
require_once(__DIR__."/sales/Url_V1.php");
require_once(__DIR__."/premium/Url_V0.php");
require_once(__DIR__."/Url_V_Base.php");
require_once(__DIR__."/../../../api/APIModel.php");

use APIModel;

class Url
{
    public static function getInstance($user){
        $isSimpleSearch = false;
        if($user['li_account_type'] === APIModel::FREE){
            $isSimpleSearch = true;
        } elseif(empty($user['li_account_type'])){
            $isSimpleSearch = (int)$user['have_sales_navigator'] === 1 ? false : true;
        }

        if($isSimpleSearch){
            return new Premium\Url_V0($user);
        }else{
            if($user["search_design_version"]){
                return new Sales\Url_V1($user);
            }else{
                return new Sales\Url_V0($user);
            }
        }
    }

    public static function getUrlType($url){
        if(strpos($url, 'sales/search/people')){
            return 'sales_v1';
        }
        if(strpos($url, 'sales/search')){
            return 'sales_v0';
        }

        return 'premium_v0';
    }

    public static function getInstanceByType($type, $user){
        switch($type){
            case 'sales_v0': return new Sales\Url_V0($user);
            case 'sales_v1': return new Sales\Url_V1($user);
            case 'premium_v0': return new Premium\Url_V0($user);
            default: return new Premium\Url_V0($user);
        }
    }

    public static function convertUrl($url, $user){
        $fromModel = self::getInstanceByType(self::getUrlType($url), $user);
        $toModel = self::getInstance($user);

        if(get_class($fromModel) !== get_class($toModel)){
            $url = $toModel->assemble($fromModel->disassemble($url));
        }

        return $url;
    }
}