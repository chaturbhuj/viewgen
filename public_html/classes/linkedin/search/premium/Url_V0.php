<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 23.08.18
 * Time: 21:57
 */

namespace linkedinSearch\Premium;

require_once(__DIR__."/../Url_V_Base.php");

use linkedinSearch\Url_V_Base;

class Url_V0 extends Url_V_Base
{
    const BASE_URL = "https://www.linkedin.com/search/results/people/";

    /**
     * @inheritdoc
     */
    public function assemble($params){
        $url = static::BASE_URL;

        $preFacetList = [];
        $companiesList = [];
        $titlesList = [];
        $facetValueList = [];

        if (!empty($params['keywords'])) {
            //Business logic:
            //1) each line treated as a single keyword and enclosed into "quotes"
            //2) keywords should be searched through OR
            $keywordsORString = '("'
                . implode('" OR "', $params['keywords']) . '") '
                . self::booleanSkipper($params['skipKeywords'] ?? []);

            $preFacetList[] = "keywords=".rawurlencode($keywordsORString);
        }

        if (!empty($params["firstName"])) {
            $preFacetList[] = "firstName=" . rawurlencode(implode(" OR ", $params["firstName"]));
        }

        if (!empty($params["lastName"])) {
            $preFacetList[] = "lastName=" . rawurlencode(implode(" OR ", $params["lastName"]));
        }

        if (!empty($params["titles"])) {
            $titlesSkipString = self::booleanSkipper($params['skipTitles'] ?? []);
            foreach ($params["titles"] as $title) {
                $titlesList[] = "title=" . rawurlencode($title . $titlesSkipString);
            }
        }

        if (!empty($params["companies"])) {
            foreach ($params["companies"] as $company) {
                $companiesList[] = "company=" . rawurlencode($company);
            }
        }

        if (!empty($params["relation"])) {
            $facetValueList[] = "facetNetwork=" . rawurlencode($this->getSimpleSearchParams($params["relation"]));
        }

        if (!empty($params["locations"])) {
            $facetValueList[] = "facetGeoRegion=" . rawurlencode($this->getSimpleSearchParams($params["locations"]));
        }

        if (!empty($params["industry"])) {
            $facetValueList[] = 'facetIndustry=' . rawurlencode($this->getSimpleSearchParams($params["industry"]));
        }

        $alwaysOnParameters = [];
        $alwaysOnParameters[] = 'page=1';


        $allParameters = array_merge(
            $preFacetList,
            $titlesList,
            $facetValueList,
            $companiesList,
            $alwaysOnParameters
        );

        $url .= '?' . implode("&", $allParameters);

        $url = $this->addOrFixRsid(trim($url));

        return $url;
    }

    public function get1stDegreeUrl(){
        return $this->assemble(['relation' => ['F']]);
    }

    public function disassemble($url)
    {
        // TODO: Implement disassemble() method.
    }

    public function getSimpleSearchParams($params)
    {
        $facet = '[';
        foreach ($params as $param) {
            if (empty($param)) continue;
            if (strlen($facet) > 1) {
                $facet .= ',"' . $param . '"';
            } else {
                $facet .= '"' . $param . '"';
            }
        }
        $facet .= ']';
        return $facet;
    }

}