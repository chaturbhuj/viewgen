<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 24.08.18
 * Time: 18:02
 */

namespace linkedinSearch;


abstract class Url_V_Base
{
    const BASE_URL = "https://www.linkedin.com/sales/search";

    public $user;

    public function __construct($user){
        $this->user = $user;
    }

    /**
     * @param array $params
     * [
     *  'excludeSavedLeads' => true,
     *  'excludeViewedLeads' => true,
     *  'excludeContactedLeads' => true,
     *  'searchWithinMyAccounts' => true,
     *  'keywords' => [
     *      'qewdd',
     *      'ffsdf',
     *      ...
     *    ],
     *  'skipKeywords' => [],
     *  'firstName' => '',
     *  'lastName' => '',
     *  'titles' => [],
     *  'titleScope' => '',
     *  'skipTitles' => [],
     *  'companies' => [],
     *  'companyScope' => '',
     *  'companiesPast' => [],
     *  'companiesCurrent' => [],
     *  'companySize' => [],
     *  'companyType' => [],
     *  'relation' => [],
     *  'groups' => [],
     *  'seniorityLevel' => [],
     *  'function' => [],
     *  'yearsInCurrentPosition' => [],
     *  'yearsAtCurrentCompany' => [],
     *  'yearsOfExperience' => [],
     *  'locations' => [],
     *  'industry' => [],
     *  'postalCode' => [
     *      'radiusMiles' => '',
     *      'countryCode' => '',
     *      'postalCode' => ''
     *       ],
     *  'tags' => [],
     *  'schools' => [],
     *  'profileLanguage' => [],
     *  'memberSince' => [],
     *  'postedContentKeywords' => []
     * ]
     *
     *
     * @return array of strings urls
     */
    abstract public function assemble($params);

    /**
     * @param string $url
     * @return array of params
     * [
     *  'excludeSavedLeads' => true,
     *  'excludeViewedLeads' => true,
     *  'excludeContactedLeads' => true,
     *  'searchWithinMyAccounts' => true,
     *  'keywords' => [
     *      'qewdd',
     *      'ffsdf',
     *      ...
     *    ],
     *  'skipKeywords' => [],
     *  'firstName' => '',
     *  'lastName' => '',
     *  'titles' => [],
     *  'titleScope' => '',
     *  'skipTitles' => [],
     *  'companies' => [],
     *  'companyScope' => '',
     *  'companiesPast' => [],
     *  'companiesCurrent' => [],
     *  'companySize' => [],
     *  'companyType' => [],
     *  'relation' => [],
     *  'groups' => [],
     *  'seniorityLevel' => [],
     *  'function' => [],
     *  'yearsInCurrentPosition' => [],
     *  'yearsAtCurrentCompany' => [],
     *  'yearsOfExperience' => [],
     *  'locations' => [],
     *  'industry' => [],
     *  'postalCode' => [
     *      'radiusMiles' => '',
     *      'countryCode' => '',
     *      'postalCode' => ''
     *       ],
     *  'tags' => [],
     *  'schools' => [],
     *  'profileLanguage' => [],
     *  'memberSince' => [],
     *  'postedContentKeywords' => []
     * ]
     *
     */
    abstract public function disassemble($url);

    public static function booleanSkipper($skipList)
    {
        $result = implode('" OR "', $skipList);

        if ($result) {
            $result = ' AND NOT ("' . $result . '")';
        }

        return $result;
    }

    public function addOrFixRsid($url) {
        $microTime = round(microtime(true)*1000);
        if (strpos($url,"rsid=")) {
            if (strpos($url, $this->user['linked_in_id'])) { // if we already have this user, then we don't do anything

            } else { // if another LinkedIn ID is already in the URL, we replace it
                $url = preg_replace("#rsid=[0-9]+([0-9]{13})#", 'rsid='.$this->user['linked_in_id'].'${1}', $url);
            }

        } else { // if there is no rsid in the URL we add it
            $url = $url."&rsid=".$this->user['linked_in_id'].$microTime;
            if (!strpos($url,"&orig=")) { // if there is no orig in our URL we add that too
                $url = $url."&orig=MDYS";
            }
        }
        return $url;
    }

    abstract public function get1stDegreeUrl();

    public function getUrlParams($url){
        $result = [];

        $parts = explode('?', $url);
        $params = explode('&', $parts[1]);

        return $params;
    }
}