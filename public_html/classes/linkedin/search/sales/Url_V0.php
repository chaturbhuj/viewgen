<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 23.08.18
 * Time: 21:57
 */

namespace linkedinSearch\Sales;

require_once(__DIR__."/../Url_V_Base.php");

use linkedinSearch\Url_V_Base;

class Url_V0 extends Url_V_Base
{
    const BASE_URL = "https://www.linkedin.com/sales/search";

    /**
     * @inheritdoc
     */
    public function assemble($params){
        $url = static::BASE_URL;

        $preFacetList = [];
        $companiesList = [];
        $titlesList = [];
        $facetIncludeList = [];
        $facetValueList = [];
        $postFacetList = [];

        if (!empty($params["excludeSavedLeads"])) {
            $preFacetList[] = "EL=true";
        }

        if (!empty($params["excludeViewedLeads"])) {
            $preFacetList[] = "profileViewType=NOT_VIEWED";
        }

        if (!empty($params["excludeContactedLeads"])) {
            $preFacetList[] = "ECL=true";
        }

        if (!empty($params["searchWithinMyAccounts"])) {
            $preFacetList[] = "facet=LSA";
        }

        if (!empty($params['keywords'])) {
            //Business logic:
            //1) each line treated as a single keyword and enclosed into "quotes"
            //2) keywords should be searched through OR
            $keywordsORString = '("'
                . implode('" OR "', $params['keywords']) . '") '
                . self::booleanSkipper($params['skipKeywords'] ?? []);

            $preFacetList[] = "keywords=".rawurlencode(
                    (count($params['keywords']) === 1 && empty($params['skipKeywords'])) ? $params['keywords'][0] : $keywordsORString
                );
        }

        if (!empty($params["firstName"])) {
            $preFacetList[] = "firstName=" . rawurlencode(implode(" OR ", $params["firstName"]));
        }

        if (!empty($params["lastName"])) {
            $preFacetList[] = "lastName=" . rawurlencode(implode(" OR ", $params["lastName"]));
        }

        if (!empty($params["titles"])) {
            $titlesSkipString = self::booleanSkipper($params['skipTitles'] ?? []);
            foreach ($params["titles"] as $title) {
                $titlesList[] = "jobTitleEntities=" . rawurlencode($title . $titlesSkipString);
            }

            $postFacetList[] = "titleScope=". (!empty($params["titleScope"]) ? $params["titleScope"] : "CURRENT");
        }

        if (!empty($params["companyScope"])) {
            $postFacetList[] = "companyScope=" . $params["companyScope"];
        }

        if (!empty($params["companies"])) {
            foreach ($params["companies"] as $company) {
                $companiesList[] = "companyEntities=" . rawurlencode($company);
            }
            if(empty($params["companyScope"])){
                $postFacetList[] = "companyScope=CURRENT";
            }
        }

        if (!empty($params["companiesPast"])) {
            $facetIncludeList[] = "facet=PC";
            foreach ($params["companiesPast"] as $company) {
                $companiesList[] = "facet.PC=" . rawurlencode($company);
            }
        }

        if (!empty($params["companiesCurrent"])) {
            $facetIncludeList[] = "facet=CC";
            foreach ($params["companiesCurrent"] as $company) {
                $companiesList[] = "facet.CC=" . rawurlencode($company);
            }
        }

        if (!empty($params["companySize"])) {
            $facetIncludeList[] = "facet=CS";
            foreach ($params["companySize"] as $size) {
                $facetValueList[] = "facet.CS=" . rawurlencode($size);
            }
        }

        if (!empty($params["companyType"])) {
            $facetIncludeList[] = "facet=CT";
            foreach ($params["companyType"] as $val) {
                $facetValueList[] = "facet.CT=" . rawurlencode($val);
            }
        }

        if (!empty($params["relation"])) {
            $facetIncludeList[] = "facet=N";
            foreach ($params["relation"] as $relation) {
                $facetValueList[] = "facet.N=" . rawurlencode($relation);
            }
        }

        if (!empty($params["groups"])) {
            $facetIncludeList[] = "facet=AG";
            foreach ($params["groups"] as $group) {
                $facetValueList[] = "facet.AG=" . rawurlencode($group);
            }
        }

        if (!empty($params["seniorityLevel"])) {
            $facetIncludeList[] = "facet=SE";
            foreach ($params["seniorityLevel"] as $seniorityLevel) {
                $facetValueList[] = "facet.SE=" . rawurlencode($seniorityLevel);
            }
        }

        if (!empty($params["function"])) {
            $facetIncludeList[] = "facet=FA";
            foreach ($params["function"] as $function) {
                $facetValueList[] = "facet.FA=" . rawurlencode($function);
            }
        }

        if (!empty($params["yearsInCurrentPosition"])) {
            $facetIncludeList[] = "facet=YP";
            foreach ($params["yearsInCurrentPosition"] as $yearInCurrentPosition) {
                $facetValueList[] = "facet.YP=" . rawurlencode($yearInCurrentPosition);
            }
        }

        if (!empty($params["yearsAtCurrentCompany"])) {
            $facetIncludeList[] = "facet=YC";
            foreach ($params["yearsAtCurrentCompany"] as $yearAtCurrentCompany) {
                $facetValueList[] = "facet.YC=" . rawurlencode($yearAtCurrentCompany);
            }
        }

        if (!empty($params["yearsOfExperience"])) {
            $facetIncludeList[] = "facet=TE";
            foreach ($params["yearsOfExperience"] as $yearOfExperience) {
                $facetValueList[] = "facet.TE=" . rawurlencode($yearOfExperience);
            }
        }

        if (!empty($params["locations"])) {
            $facetIncludeList[] = "facet=G";
            foreach ($params["locations"] as $location) {
                if ($location) {
                    $facetValueList[] = "facet.G=" . rawurlencode($location);
                }
            }
        }

        if (!empty($params["industry"])) {
            $facetIncludeList[] = "facet=I";
            foreach ($params["industry"] as $industry) {
                $facetValueList[] = "facet.I=" . rawurlencode($industry);
            }
        }

        if (!empty($params["postalCode"])) {
            $postFacetList[] = "radiusMiles=" . rawurlencode($params["postalCode"]["radiusMiles"]);
            $postFacetList[] = "countryCode=" . rawurlencode($params["postalCode"]["countryCode"]);
            $postFacetList[] = "postalCode=" . rawurlencode($params["postalCode"]["postalCode"]);
        }

        if (!empty($params["tags"])) {
            $facetIncludeList[] = "facet=T";
            foreach ($params["tags"] as $val) {
                $facetValueList[] = "facet.T=" . rawurlencode($val);
            }
        }

        if (!empty($params["schools"])) {
            $facetIncludeList[] = "facet=ED";
            foreach ($params["schools"] as $val) {
                $facetValueList[] = "facet.ED=" . rawurlencode($val);
            }
        }

        if (!empty($params["profileLanguage"])) {
            $facetIncludeList[] = "facet=L";
            foreach ($params["profileLanguage"] as $val) {
                $facetValueList[] = "facet.L=" . rawurlencode($val);
            }
        }

        if (!empty($params["memberSince"])) {
            $facetIncludeList[] = "facet=DR";
            foreach ($params["memberSince"] as $val) {
                $facetValueList[] = "facet.DR=" . rawurlencode($val);
            }
        }

        if (!empty($params['postedContentKeywords'])) {
            //Business logic:
            //1) each line treated as a single keyword and enclosed into "quotes"
            //2) keywords should be searched through OR
            $keywordsORString = '("'
                . implode('" OR "', $params['postedContentKeywords']) . '") '
                . self::booleanSkipper($params['skipPostedContentKeywords'] ?? []);

            $preFacetList[] = "memberPostContentKeywords=".rawurlencode(
                    (count($params['postedContentKeywords']) === 1 && empty($params['skipPostedContentKeywords'])) ? $params['postedContentKeywords'][0] : $keywordsORString
                );
        }


        $alwaysOnParameters = [];
        $alwaysOnParameters[] = "count=25";
        $alwaysOnParameters[] = "start=" . (!empty($params['page']) ? (($params['page']-1) * 25) : 0);
        $alwaysOnParameters[] = "updateHistory=true";


        $allParameters = array_merge(
            $preFacetList,
            $titlesList,
            $facetIncludeList,
            $facetValueList,
            $companiesList,
            $postFacetList,
            $alwaysOnParameters
        );

        $url .= '?' . implode("&", $allParameters);

        $url = $this->addOrFixRsid(trim($url));

        return $url;
    }

    public function get1stDegreeUrl(){
        return $this->assemble(['relation' => ['F']]);
    }

    public function disassemble($url)
    {
        $result = [];
        foreach($this->getUrlParams($url) as $param){
            $tmp = explode('=', $param);
            $val = rawurldecode($tmp[1]);
            switch($tmp[0]){
                case 'EL':
                    if($val == 'true') {
                        $result['excludeSavedLeads'] = true;
                    }
                    break;
                case 'profileViewType':
                    if($val == 'NOT_VIEWED'){
                        $result['excludeViewedLeads'] = true;
                    }
                    break;
                case 'ECL':
                    if($val == 'true') {
                        $result['excludeContactedLeads'] = true;
                    }
                    break;
                case 'facet':
                    switch($val){
                        case 'LSA':
                            $result['searchWithinMyAccounts'] = true;
                            break;
                    }
                    break;
                case 'facet.G':
                    $result['locations'][] = $val;
                    break;
                case 'facet.N':
                    $result['relation'][] = $val;
                    break;
                case 'companyScope':
                    $result['companyScope'] = $val;
                    break;
                case 'companyEntities':
                    $result['companies'][] = $this->cleanValue($val);
                    break;
                case 'facet.PC':
                    $result['companiesPast'][] = $this->cleanValue($val);
                    break;
                case 'facet.CC':
                    $result['companiesCurrent'][] = $this->cleanValue($val);
                    break;
                case 'facet.I':
                    $result['industry'][] = $val;
                    break;
                case 'facet.CS':
                    $result['companySize'][] = $val;
                    break;
                case 'facet.FA':
                    $result['function'][] = $val;
                    break;
                case 'titleScope':
                    $result['titleScope'] = $val;
                    break;
                case 'jobTitleEntities':
                    $result['titles'][] = $this->cleanValue($val);
                    break;
                case 'facet.SE':
                    $result['seniorityLevel'][] = $val;
                    break;
                case 'facet.T':
                    $result['tags'][] = $val;
                    break;
                case 'radiusMiles':
                    $result['postalCode']['radiusMiles'] = $val;
                    break;
                case 'countryCode':
                    $result['postalCode']['countryCode'] = $val;
                    break;
                case 'postalCode':
                    $result['postalCode']['postalCode'] = $val;
                    break;
                case 'facet.ED':
                    $result['schools'][] = $val;
                    break;
                case 'firstName':
                    $result['firstName'] = $val;
                    break;
                case 'lastName':
                    $result['lastName'] = $val;
                    break;
                case 'facet.L':
                    $result['profileLanguage'][] = $val;
                    break;
                case 'facet.YP':
                    $result['yearsInCurrentPosition'][] = $val;
                    break;
                case 'facet.YC':
                    $result['yearsAtCurrentCompany'][] = $val;
                    break;
                case 'facet.TE':
                    $result['yearsOfExperience'][] = $val;
                    break;
                case 'facet.CT':
                    $result['companyType'][] = $val;
                    break;
                case 'facet.AG':
                    $result['groups'][] = $val;
                    break;
                case 'facet.DR':
                    $result['memberSince'][] = $val;
                    break;
                case 'memberPostContentKeywords':
                    $result['postedContentKeywords'][] = $val;
                    break;
                case 'keywords':
                    if($val){
                        $result['keywords'][] = $val;
                    }
                    break;
                case 'start':
                    $result['page'] = intdiv($val, 25);
                    break;
            }
        }


        return $result;
    }


    public function cleanValue($val){
        $val = preg_replace('/^(.+)(_\d+)$/', '${1}', $val);
        return $val;
    }
}