<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 23.08.18
 * Time: 21:57
 */

namespace linkedinSearch\Sales;

require_once(__DIR__."/../Url_V_Base.php");

use linkedinSearch\Url_V_Base;

class Url_V1 extends Url_V_Base
{
    const BASE_URL = "https://www.linkedin.com/sales/search/people";

    /**
     * @inheritdoc
     */
    public function assemble($params){
        $url = static::BASE_URL;

        $preFacetList = [];
        $postFacetList = [];

        if (!empty($params["excludeSavedLeads"])) {
            $preFacetList[] = "excludeSavedLeads=true";
        }

        if (!empty($params["excludeViewedLeads"])) {
            $preFacetList[] = "excludeViewedLeads=true";
        }

        if (!empty($params["excludeContactedLeads"])) {
            $preFacetList[] = "excludeContactedLeads=true";
        }

        if (!empty($params["searchWithinMyAccounts"])) {
            $preFacetList[] = "searchWithinMyAccounts=true";
        }

        if (!empty($params['keywords'])) {
            //Business logic:
            //1) each line treated as a single keyword and enclosed into "quotes"
            //2) keywords should be searched through OR
            $keywordsORString = '("'
                . implode('" OR "', $params['keywords']) . '") '
                . self::booleanSkipper($params['skipKeywords'] ?? []);

            $preFacetList[] = "keywords=".rawurlencode(
                    (count($params['keywords']) === 1 && empty($params['skipKeywords'])) ? $params['keywords'][0] : $keywordsORString
                );
        }

        if (!empty($params["firstName"])) {
            $preFacetList[] = "firstName=" . rawurlencode(implode(" OR ", $params["firstName"]));
        }

        if (!empty($params["lastName"])) {
            $preFacetList[] = "lastName=" . rawurlencode(implode(" OR ", $params["lastName"]));
        }

        if (!empty($params["titles"])) {
            $titleORString = '("'
                . implode('" OR "', $params['titles']) . '") '
                . self::booleanSkipper($params['skipTitles'] ?? []);

            $preFacetList[] = "title=" . rawurlencode(
                    (count($params['titles']) === 1 && empty($params['skipTitles'])) ? $params['titles'][0] : $titleORString
                );
            $postFacetList[] = "titleTimeScope=". (!empty($params["titleScope"]) ? $params["titleScope"] : "CURRENT");
        }

        if (!empty($params["companyScope"])) {
            $postFacetList[] = "companyTimeScope=" . $params["companyScope"];
        }

        if (!empty($params["companies"])) {
            $preFacetList[] = "company=" . rawurlencode(implode(",", $this->companiesEncode($params["companies"])));
            if(empty($params["companyScope"])){
                $postFacetList[] = "companyTimeScope=CURRENT";
            }
        }

        if (!empty($params["companiesPast"])) {
            $preFacetList[] = "pastCompany=" . rawurlencode(implode(",", $this->companiesEncode($params["companiesPast"])));
        }

        if (!empty($params["companiesCurrent"])) {
            $preFacetList[] = "currCompany=" . rawurlencode(implode(",", $this->companiesEncode($params["companiesCurrent"])));
        }

        if (!empty($params["companySize"])) {
            $preFacetList[] = "companySize=" . rawurlencode(implode(",", $params["companySize"]));
//            foreach ($params["companySize"] as $size) {
//                $facetValueList[] = "facet.CS=" . self::SIZES_TO_VALUE[$size];
//            }
        }

        if (!empty($params["companyType"])) {
            $preFacetList[] = "companyType=" . rawurlencode(implode(",", $params["companyType"]));
        }

        if (!empty($params["relation"])) {
            $preFacetList[] = "relationship=" . rawurlencode(implode(",", $params["relation"]));
        }

        if (!empty($params["groups"])) {
            $preFacetList[] = "group=" . rawurlencode(implode(",", $params["groups"]));
        }

        if (!empty($params["seniorityLevel"])) {
            $preFacetList[] = "seniority=" . rawurlencode(implode(",", $params["seniorityLevel"]));
        }

        if (!empty($params["function"])) {
            $preFacetList[] = "function=" . rawurlencode(implode(",", $params["function"]));
        }

        if (!empty($params["yearsInCurrentPosition"])) {
            $preFacetList[] = "tenureAtCurrentPosition=" . rawurlencode(implode(",", $params["yearsInCurrentPosition"]));
        }

        if (!empty($params["yearsAtCurrentCompany"])) {
            $preFacetList[] = "tenureAtCurrentCompany=" . rawurlencode(implode(",", $params["yearsAtCurrentCompany"]));
        }

        if (!empty($params["yearsOfExperience"])) {
            $preFacetList[] = "yearsOfExperience=" . rawurlencode(implode(",", $params["yearsOfExperience"]));
        }

        if (!empty($params["locations"])) {
            $preFacetList[] = "geo=" . rawurlencode(implode(",", $params["locations"]));
        }

        if (!empty($params["industry"])) {
            $preFacetList[] = "industry=" . rawurlencode(implode(",", $params["industry"]));
        }

        if (!empty($params["postalCode"])) {
            $postFacetList[] = "radius=" . ($params["postalCode"]["radiusMiles"] ?? '');
            $postFacetList[] = "countryCode=" . rawurlencode($params["postalCode"]["countryCode"]);
            $postFacetList[] = "zips=" . rawurlencode($params["postalCode"]["postalCode"]);
        }

        if (!empty($params["tags"])) {
            $preFacetList[] = "tag=" . rawurlencode(implode(",", $params["tags"]));
        }

        if (!empty($params["schools"])) {
            $preFacetList[] = "school=" . rawurlencode(implode(",", $params["schools"]));
        }

        if (!empty($params["profileLanguage"])) {
            $preFacetList[] = "profileLanguage=" . rawurlencode(implode(",", $params["profileLanguage"]));
        }

        if (!empty($params["memberSince"])) {
            $preFacetList[] = "memberSince=" . rawurlencode(implode(",", $params["memberSince"]));
        }

        if (!empty($params['postedContentKeywords'])) {
            //Business logic:
            //1) each line treated as a single keyword and enclosed into "quotes"
            //2) keywords should be searched through OR
            $keywordsORString = '("'
                . implode('" OR "', $params['postedContentKeywords']) . '") '
                . self::booleanSkipper($params['skipPostedContentKeywords'] ?? []);

            $preFacetList[] = "postedContentKeywords=".rawurlencode(
                    (count($params['postedContentKeywords']) === 1 && empty($params['skipPostedContentKeywords'])) ? $params['postedContentKeywords'][0] : $keywordsORString
                );
        }

        $alwaysOnParameters = [];
        $alwaysOnParameters[] = "page=" . (!empty($params['page']) ? $params['page'] : 1);
        $alwaysOnParameters[] = "logHistory=true";


        $allParameters = array_merge(
            $preFacetList,
            $postFacetList,
            $alwaysOnParameters
        );

        $url .= '?' . implode("&", $allParameters);

        $url = trim($url);

        return $url;
    }

    public function get1stDegreeUrl(){
        return $this->assemble(['relation' => ['F']]);
    }

    public function disassemble($url)
    {
        $result = [];
        foreach($this->getUrlParams($url) as $param){
            $tmp = explode('=', $param);
            $val = rawurldecode($tmp[1]);
            switch($tmp[0]){
                case 'excludeSavedLeads':
                    if($val == 'true') {
                        $result['excludeSavedLeads'] = true;
                    }
                    break;
                case 'excludeViewedLeads':
                    if($val == 'true'){
                        $result['excludeViewedLeads'] = true;
                    }
                    break;
                case 'excludeContactedLeads':
                    if($val == 'true') {
                        $result['excludeContactedLeads'] = true;
                    }
                    break;
                case 'searchWithinMyAccounts':
                    if($val == 'true') {
                        $result['searchWithinMyAccounts'] = true;
                    }
                    break;
                case 'geo':
                    $result['locations'] = $this->paramStringToArray($val);
                    break;
                case 'relationship':
                    $result['relation'] = $this->paramStringToArray($val);
                    break;
                case 'companyTimeScope':
                    $result['companyScope'] = $val;
                    break;
                case 'company':
                    $result['companies'] = $this->cleanValue($this->paramStringToArray($val));
                    break;
                case 'pastCompany':
                    $result['companiesPast'] = $this->cleanValue($this->paramStringToArray($val));
                    break;
                case 'currCompany':
                    $result['companiesCurrent'] = $this->cleanValue($this->paramStringToArray($val));
                    break;
                case 'industry':
                    $result['industry'] = $this->paramStringToArray($val);
                    break;
                case 'companySize':
                    $result['companySize'] = $this->paramStringToArray($val);
                    break;
                case 'function':
                    $result['function'] = $this->paramStringToArray($val);
                    break;
                case 'titleTimeScope':
                    $result['titleScope'] = $val;
                    break;
                case 'title':
                    $result['titles'] = $this->cleanValue($this->paramStringToArray($val));
                    break;
                case 'seniority':
                    $result['seniorityLevel'] = $this->paramStringToArray($val);
                    break;
                case 'tag':
                    $result['tags'] = $this->paramStringToArray($val);
                    break;
                case 'radius':
                    $result['postalCode']['radiusMiles'] = $val;
                    break;
                case 'countryCode':
                    $result['postalCode']['countryCode'] = $val;
                    break;
                case 'zips':
                    $result['postalCode']['postalCode'] = $val;
                    break;
                case 'school':
                    $result['schools'] = $this->paramStringToArray($val);
                    break;
                case 'firstName':
                    $result['firstName'] = $val;
                    break;
                case 'lastName':
                    $result['lastName'] = $val;
                    break;
                case 'profileLanguage':
                    $result['profileLanguage'] = $this->paramStringToArray($val);
                    break;
                case 'tenureAtCurrentPosition':
                    $result['yearsInCurrentPosition'] = $this->paramStringToArray($val);
                    break;
                case 'tenureAtCurrentCompany':
                    $result['yearsAtCurrentCompany'] = $this->paramStringToArray($val);
                    break;
                case 'yearsOfExperience':
                    $result['yearsOfExperience'] = $this->paramStringToArray($val);
                    break;
                case 'companyType':
                    $result['companyType'] = $this->paramStringToArray($val);
                    break;
                case 'group':
                    $result['groups'] = $this->paramStringToArray($val);
                    break;
                case 'memberSince':
                    $result['memberSince'] = $this->paramStringToArray($val);
                    break;
                case 'postedContentKeywords':
                    $result['postedContentKeywords'][] = $val;
                    break;
                case 'keywords':
                    $result['keywords'][] = $val;
                    break;
                case 'page':
                    $result['page'] = intval($val);
                    break;
            }
        }


        return $result;
    }

    private function paramStringToArray($str){
        $result = [];

        foreach(explode(',', $str) as $val){
            $result[] = rawurldecode($val);
        }

        return $result;
    }

    public function cleanValue($val){
        $val = preg_replace('/^ :(\d+)$/', '${1}', $val);
        $val = preg_replace('/^(.+)(:\d+)$/', '${1}', $val);
        return $val;
    }

    public function companiesEncode($companies){
        foreach ($companies as $key => $val){
            $val = trim($val);
            if(preg_match('/^\d+$/', $val)){
                $val = ' :'.$val;
            }
            $companies[$key] = $val;
        }
        return $companies;
    }

}