<?php

class MeanVBROverTimeGraph extends Graph {

	public function __construct() {
		parent::__construct("MeanVBROverTime");
	}

	public function getTitle() {
		return "Mean VBR of all companies over time (users with >2000 outbound/week)";
	}

	public function recalculate($xMin, $xMax) {

		// Fix max, should max be last sunday.
		$lastSunday = date('Y-m-d', strtotime('last Sunday'));
		if ($xMax > $lastSunday) $mMax = $lastSunday;

		$xMin = $this->ceilDateToSunday($xMin);

		// Select the number of viewbacks for the given period.
		$query = DB::prep("SELECT *
				FROM visitors
			WHERE
				date > :dateMin AND
				date <= :dateMax
			ORDER BY date ASC");
		$query->execute([
			'dateMin' => $xMin,
			'dateMax' => $xMax
		]);
		$visitors = $query->fetchAll();
		$visitors = $this->cleanVisitorData($visitors);

		$query = DB::prep("SELECT
				COUNT(*) count,
				sent_to_crawler_date,
				crawled_by_id
			FROM people
			WHERE
				sent_to_crawler_date > :dateMin AND
				sent_to_crawler_date <= :dateMax
			GROUP BY
				sent_to_crawler_date, crawled_by_id");
		$query->execute([
			'dateMin' => $xMin,
			'dateMax' => $xMax
		]);
		$outboundVisits = $query->fetchAll();
		$this->cleanPeopleData($outboundVisits);

		$outboundPerDate = [];
		foreach ($outboundVisits as $outbound) {
			if (!isset($outboundPerDate[$outbound['sent_to_crawler_date']])) {
				$outboundPerDate[$outbound['sent_to_crawler_date']] = array();
			}
			$outboundPerDate[$outbound['sent_to_crawler_date']][] = $outbound;
		}

		$dates = [];
		foreach ($visitors as $visitor) {
			$dates[] = $visitor['date'];
		}
		sort($dates);
		$dates = array_unique($dates);

		// Just sum and plot.
		foreach ($dates as $date) {
			$totalOutbound = 0;
			$totalInbound = 0;
			foreach ($visitors as $visitor) {
				if ($visitor['date'] != $date) continue;
				$usersOutbound = 0;
				if (!isset($outboundPerDate[$date])) continue;
				foreach ($outboundPerDate[$date] as $outbound) {
					if ($visitor['linked_in_user_id'] == $outbound['crawled_by_id']) {
						$usersOutbound += $outbound['count'];
					}
				}
				if ($usersOutbound > 2000) {
					$totalInbound += $visitor['visitor_count'];
					$totalOutbound += $usersOutbound;
				}
			}
			$this->setValue($date, $totalInbound / $totalOutbound);
		}
	}

	private function cleanVisitorData($visitors) {

		// Some people are having mon-sun, some have sun-sat as weeks. Just make it one.
		foreach ($visitors as &$visitor) {
			$visitor['date'] = $this->ceilDateToSunday($visitor['date']);
		}

		return $visitors;
	}

	private function cleanPeopleData(&$peoples) {

		// Some people are having mon-sun, some have sun-sat as weeks. Just make it one.
		foreach ($peoples as &$people) {
			$people['sent_to_crawler_date'] = $this->ceilDateToSunday($people['sent_to_crawler_date']);
		}
	}

	private function ceilDateToSunday($date) {
		
		$timestamp = strtotime($date);
		$week = date('W', $timestamp);
		$year = date('o', $timestamp);
		return date('Y-m-d', strtotime(sprintf('%dW%02d', $year, $week)) + 6 * 24 * 3600);
	}
}

?>