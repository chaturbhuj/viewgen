<?php

class BigCompanyAbsoluteVBOverTimeGraph extends Graph {

	public function __construct() {
		parent::__construct("MeanVBROverTime");
	}

	public function getTitle() {
		return "Absolute viewbacks of 10 users in largest companies over time";
	}

	public function recalculate($xMin, $xMax) {
		
		// Fetch the users in the biggest companies.
		$query = DB::prep("SELECT
				company.*,
				li_users.linked_in_id AS linked_in_id,
				li_users.name AS li_user_name
			FROM company
			JOIN li_users ON (li_users.company_id = company.id)
			ORDER BY people_count DESC
			LIMIT 10");
		$query->execute();

		// One line for each user.
		$numLines = $query->rowCount();

		$i = 0;
		$peoples = [];
		while ($company = $query->fetch()) {
			$this->setLabel($i, $company['company_name'] . sprintf('(%d)', $company['id']) . " : " . $company['li_user_name']);

			// Fetch the number of inbound views.
			$queryVisitors = DB::prep("SELECT *
				FROM visitors
				WHERE
					company_id = :company_id AND
					linked_in_user_id = :linked_in_user_id AND
					date >= :dateMin AND
					date <= :dateMax
				ORDER BY date");
			$queryVisitors->execute([
				'company_id' => $company['id'],
				'linked_in_user_id' => $company['linked_in_id'],
				'dateMin' => $xMin,
				'dateMax' => $xMax
			]);

			while ($viewback = $queryVisitors->fetch()) {
				$this->setValue($viewback['date'], $viewback['visitor_count'], $i);
			}
			$i++;
		}
	}
}

?>