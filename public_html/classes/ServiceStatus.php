<?php

namespace classes;


class ServiceStatus
{
    public $company_id;
    public $user_id;

    const SEVICE_TABLE = 'vg_service_runtime';

    const STATUS_RUNNING = 'running';
    const STATUS_STOPPED = 'stopped';
    const STATUS_DISABLED = 'disabled';

    public function __construct($company_id = null, $user_id = null)
    {
        $this->company_id = $company_id;
        $this->user_id = $user_id;
    }

    public function getStatus($name){
        $query = \DB::prep("SELECT * FROM ".self::SEVICE_TABLE." WHERE service_id = :id AND user_id = :user_id AND param_id = 'is_running'");
        $query->execute([
            'id' => $name,
            'user_id' => $this->user_id
        ]);

        $result = !empty($query->fetch()['value']) ? self::STATUS_RUNNING : self::STATUS_STOPPED;

        return $result;
    }

    public function start($name){
        $query = \DB::prep("SELECT * FROM ".self::SEVICE_TABLE." WHERE service_id = :id AND user_id = :user_id AND param_id = 'is_running'");
        $query->execute([
            'id' => $name,
            'user_id' => $this->user_id
        ]);

        if(!empty($query->fetch())){
            $query = \DB::prep("
                            UPDATE 
                              ".self::SEVICE_TABLE."
                            SET
                              `value` = 1
                            WHERE
                              service_id = :id
                              AND user_id = :user_id
                              AND param_id = 'is_running'
                           ");
            $query->execute([
                'id' => $name,
                'user_id' => $this->user_id,
            ]);
        }else{
            $query = \DB::prep("
                            INSERT INTO 
                              ".self::SEVICE_TABLE."
                            (param_id, service_id, user_id, `value`)
                            VALUES (:param_id, :service_id, :user_id, :value)
                           ");
            $query->execute([
                'service_id' => $name,
                'user_id' => $this->user_id,
                'param_id' => 'is_running',
                'value' => true
            ]);
        }

    }

}