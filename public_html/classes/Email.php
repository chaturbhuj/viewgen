<?php

include (__DIR__ . '/../lib/sendgrid/sendgrid.php');

class Email {

	private $apiUser = "ViewGen";
	private $apiKey = "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";

	private $sendGrid;
	private $message;
	private $toEmail;
	private $senderEmail;

	private $subject = "";
	private $body = "";

	/*
		Constructor for the email class. The siteId is mandatory to make sure that we send it from the correct address.

		@param integer $siteId
			The siteId used to fetch which sender to use.
	*/

	public function __construct() {
		$this->sendGrid = new SendGrid($this->apiKey);
		$this->message = new SendGrid\Email();

		$this->senderEmail = "ViewGen<noreply@mailer.viewgentools.com>";

	    $this->message->setFrom($this->senderEmail);
		$this->message->addHeader("From", $this->senderEmail);
	}

	public function addTo($toEmail) {
		$this->toEmail = $toEmail;
		$this->message->addTo($toEmail);	
	}

	public function addReplyTo($replyToEmail) {
		$this->message->setReplyTo($replyToEmail);	
	}

	public function addSubstitution($key, $value) {
		$this->message->addSubstitution($key, $value);
	}

	public function setSubject($subject) {
		$this->message->setSubject($subject);
		$this->subject = $subject;
	}

	public function setTextBody($body) {
		$this->message->setText($body);
		$this->body = $body;
	}

	public function setHtmlBody($body) {
		$this->message->setHtml($body);
		$this->body = $body;
	}

	public function send() {
		$this->sendGrid->send($this->message);
	}

	public function addAttachment($fileName, $content) {
		$this->message->addAttachment($fileName, $content);
	}

	public function setTemplateId($templateId) {
		$this->message->setTemplateId($templateId);
	}

}

?>
