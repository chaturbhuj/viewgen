<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 22.04.20
 * Time: 18:24
 */

namespace numeria;


class Connector
{
    const API_BASE_PATH = 'https://www.nymeria.io/api/v2/';
    const ALLOWED_PARAMS = [
        'api_key',
        'email',
        'name',
        'first_name',
        'last_name',
        'linkedin_id',
        'linkedin_member_id',
        'linkedin_public_identifier',
        'linkedin_url',
        'employer',
        'github_login',
        'github_url'
    ];

    protected $_api_key = '';

    public function __construct(string $api_key)
    {
        if(empty($api_key)){
            throw new \ErrorException('Api key should be provided');
        }
        $this->_api_key = $api_key;
    }

    public function getEmail(array $params){

        $result = $this->_postUrl(self::API_BASE_PATH . 'emails', $params);

        return $result->emails;
    }

    public function getEmailByMemberId($member_id, $return_first = true){

        $result = $this->getEmail([
            'linkedin_member_id' => $member_id,
        ]);

        if($return_first && !empty($result)){
            return $result[0]->email;
        }

        return $result;
    }

    protected static function _cleanParams(array $params){
        $data = [];

        foreach ($params as $name => $value){
            if(in_array($name, self::ALLOWED_PARAMS) && !empty($value)){
                $data[$name] = $value;
            }
        }

        return $data;
    }

    protected function _postUrl(string $url, array $params){
        $params = self::_cleanParams($params);

        $params['api_key'] = $this->_api_key;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $result = curl_exec($ch);

        $error = curl_error($ch);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($http_code !== 200){
            throw new \ErrorException('Numeria http: ' . 'Code: ' .$http_code. ' Message: ' . $error);
        }

        $result = json_decode($result);

        if($result === null){
            throw new \ErrorException('Numeria http: ' . 'Result empty or can not be decoded');
        }

        if($result->status != 'success'){
            throw new \ErrorException('Numeria http: ' . $result->developer_message);
        }

        return $result->data;
    }
}