<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 08.07.18
 * Time: 17:04
 */

namespace classes;


class Settings
{
    public $company_id;
    public $user_id;

    const SETTINGS_TABLE = 'vg_settings';
    const TYPE_COMPANY = 'company';
    const TYPE_USER = 'user';
    const TYPE_GLOBAL = 'global';

    const GLOBAL_DEFAULT = [
        'uninviter-time_period' => 60*60*24,
        'uninviter-max_count' => 100,
        'uninviter-enabled' => false,
        'itc-enabled' => false,
        'invite-auto_accept' => false,
        'visiting-disable_visited_skip' => false,
        'email_extractor-enabled' => false,
        'email_extractor_visited-enabled' => false,
        'email_extractor-expiration_date' => null,
        'email_extractor-limit' => 100,
    ];

    public function __construct($company_id = null, $user_id = null)
    {
        $this->company_id = $company_id;
        $this->user_id = $user_id;
    }

    public function getForCompany($name){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id AND `name` = :name");
        $query->execute([
            'type' => static::TYPE_COMPANY,
            'object_id' => $this->company_id,
            'name' => $name
        ]);

        if($query->rowCount()){
            $param = $query->fetch();
            return $param['value'];
        }

        return self::GLOBAL_DEFAULT[$name] ?? null;
    }

    public function getAllForCompany(){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id");
        $query->execute([
            'type' => static::TYPE_COMPANY,
            'object_id' => $this->company_id
        ]);
        $params = $query->fetchAll();

        $result = [];
        foreach($params as $param){
            $result[$param['name']] = $param['value'];
        }

        $result = array_merge(self::GLOBAL_DEFAULT, $result);

        return $result;
    }

    public function setForCompany($name, $value){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id AND `name` = :name");
        $query->execute([
            'type' => static::TYPE_COMPANY,
            'object_id' => $this->company_id,
            'name' => $name
        ]);

        if($query->rowCount()){
            self::updateInDB($name, $value, static::TYPE_COMPANY, $this->company_id);
        } else {
            self::insertToDB($name, $value, static::TYPE_COMPANY, $this->company_id);
        }
    }

    public function getForUser($name){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id AND `name` = :name");
        $query->execute([
            'type' => static::TYPE_USER,
            'object_id' => $this->user_id,
            'name' => $name
        ]);

        if($query->rowCount()){
            $param = $query->fetch();
            return $param['value'];
        }

        return $this->getForCompany($name);
    }

    public function getAllForUser(){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id");
        $query->execute([
            'type' => static::TYPE_USER,
            'object_id' => $this->user_id
        ]);
        $params = $query->fetchAll();

        $result = [];
        foreach($params as $param){
            $result[$param['name']] = $param['value'];
        }

        $result = array_merge($this->getAllForCompany(), $result);

        return $result;
    }

    public function setForUser($name, $value){
        $query = \DB::prep("SELECT * FROM ".self::SETTINGS_TABLE." WHERE type = :type AND object_id = :object_id AND `name` = :name");
        $query->execute([
            'type' => static::TYPE_USER,
            'object_id' => $this->user_id,
            'name' => $name
        ]);

        if($query->rowCount()){
            self::updateInDB($name, $value, static::TYPE_USER, $this->user_id);
        } else {
            self::insertToDB($name, $value, static::TYPE_USER, $this->user_id);
        }
    }

//-----------------------------PRIVATE----------------------------------------------------------------------------------

    private static function updateInDB($name, $value, $type, $object_id){
        $query = \DB::prep("
                            UPDATE 
                              ".self::SETTINGS_TABLE."
                            SET
                              `value` = :value
                            WHERE
                              type = :type
                              AND object_id = :object_id
                              AND name = :name
                           ");
        $query->execute([
            'type' => $type,
            'object_id' => $object_id,
            'name' => $name,
            'value' => $value
        ]);
    }

    private static function insertToDB($name, $value, $type, $object_id){
        $query = \DB::prep("
                            INSERT INTO 
                              ".self::SETTINGS_TABLE."
                            (type, object_id, `name`, `value`)
                            VALUES (:type, :object_id, :name, :value)
                           ");
        $query->execute([
            'type' => $type,
            'object_id' => $object_id,
            'name' => $name,
            'value' => $value
        ]);
    }
}

