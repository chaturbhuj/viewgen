<?php
include_once(__DIR__ . '/EmailCustomTemplate.php');
require_once (__DIR__ . '/../lib/recurly.php');


/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 22.03.18
 * Time: 18:28
 */
class Referrer
{
    const EMAIL_TEMPLATE_NAME = 'Add Referrer';
    public $emailTemplate;

    public function __construct(){
        $this->emailTemplate = new EmailCustomTemplate(Referrer::EMAIL_TEMPLATE_NAME);
    }

    public function create($companyId, $email, $firstName, $lastName, $message){
        if(empty($email)){
            return 'Incorrect email';
        }
        $currentReferrer = $this->getByEmail($email);

        if($currentReferrer && $currentReferrer['company_id'] !== $companyId){
            return 'This user already invited by other company';
        }

        if($currentReferrer && $currentReferrer['referred_company_id']){
            return 'This user is registered already';
        }

        if(!$currentReferrer){
            $hash = md5($companyId.$email.time());
            $currentReferrer = $this->getById($this->saveToDb($companyId, $hash, $email, $firstName, $lastName));
        }

        $this->sendEmail($companyId, $currentReferrer['hash'], $email, $message, $firstName, $lastName);

        $this->logSentReferrer($currentReferrer['id'], $message);

        return true;
    }

    public function getByEmail($email){
        $query = DB::prep("SELECT * FROM vg_referrer WHERE referred_email = ?");
        $query->execute([
            $email
        ]);

        if($query->rowCount() === 0){
            return false;
        }

        return $query->fetch();
    }

    public function getById($id){
        $query = DB::prep("SELECT * FROM vg_referrer WHERE id = ?");
        $query->execute([
            $id
        ]);

        if($query->rowCount() === 0){
            return false;
        }

        return $query->fetch();
    }

    public function getByHash($hash){
        $query = DB::prep("SELECT * FROM vg_referrer WHERE hash = ?");
        $query->execute([
            $hash
        ]);

        if($query->rowCount() === 0){
            return false;
        }

        return $query->fetch();
    }

    private function saveToDb($company_id, $hash, $email, $firstName, $lastName){
        $query = DB::prep("
                            INSERT INTO
                              vg_referrer (
                                company_id,
                                referred_email,
                                first_name,
                                last_name,
                                hash
                              )
                            VALUES
                              (?, ?, ?, ?, ?)
                          ");
        if(!$query->execute([
            $company_id,
            $email,
            $firstName,
            $lastName,
            $hash
        ])){
            return false;
        }

        return \DB::get_instance()->lastInsertId();
    }


    public function getMessageTemplate(){
        return $this->emailTemplate->body;
    }

    public function connect($hash, $email, $companyId){
        $linkedBy = 'link';
        $referrer = $this->getByHash($hash);
        if(!$referrer){
            $linkedBy = 'email';
            $referrer = $this->getByEmail($email);
        }

        if(!$referrer || $referrer['referred_company_id']){
            return false;
        }

        $query = DB::prep("
                            UPDATE
                              vg_referrer
                            SET
                              referred_company_id = ?,
                              linked_by = ?
                            WHERE 
                              id = ?
                          ");
        $query->execute([
            $companyId,
            $linkedBy,
            $referrer['id']
        ]);

        return true;
    }

    public function convertAll(){
        $query = DB::prep("
                            SELECT
                              vg_referrer.*
                            FROM
                              vg_referrer
                              LEFT JOIN company ref ON vg_referrer.referred_company_id = ref.id
                              LEFT JOIN company ON vg_referrer.company_id = company.id
                            WHERE
                              referred_company_id IS NOT NULL
                              AND converted_at IS NULL
                              AND ref.frozen = 0
                              AND company.frozen = 0
                          ");
        $query->execute([
        ]);

        while($referrer = $query->fetch()){
            $this->convert($referrer);
        }
    }

    public function getCompanyById($id){
        $query = DB::prep("SELECT * FROM company WHERE id = ?");
        $query->execute([
            $id
        ]);
        return $query->fetch();
    }

    public function convert($referrer){
        if(!$referrer){
            return false;
        }

        $company = $this->getCompanyById($referrer['referred_company_id']);
        if(!$company){
            return false;
        }

        $transactions = [];

        $converted = false;
        try{
            $transactions = Recurly_InvoiceList::getForAccount($company['recurlyAccountCode'], ['state' => 'collected', 'sort' => 'created_at', 'order' => 'desc', 'begin_time' => $company['company_created']]);
            foreach($transactions as $transaction){
                if($transaction->total_in_cents > 0){
                    $converted = true;
                    break;
                }
            }
        }catch(Exeption $e){
            var_dump($e);
        }

        if($converted){
            $query = DB::prep("
                            UPDATE
                              vg_referrer
                            SET
                              converted_at = NOW()
                            WHERE 
                              id = ?
                          ");
            $query->execute([
                $referrer['id']
            ]);
        }
        return true;
    }

    public function markAsPaid($id, $message = ''){
        $query = DB::prep("
                            UPDATE
                              vg_referrer
                            SET
                              paid_at = NOW(),
                              notes = ?
                            WHERE 
                              id = ?
                          ");
        $query->execute([
            $message,
            $id
        ]);
    }

    private function logSentReferrer($referrerId, $message){
        $query = DB::prep("
                            INSERT INTO
                              vg_referrer_email (
                                referrer_id,
                                message
                              )
                            VALUES
                              (?, ?)
                          ");
        $query->execute([
            $referrerId,
            $message
        ]);
    }

    private function sendEmail($companyId, $hash, $email, $message, $firstName, $lastName){
        $company = $this->getCompanyById($companyId);
        $ref_link = 'https://viewgentools.com/ui/sign_up?referrer='.$hash;

        $this->emailTemplate->to = $email;
        $this->emailTemplate->body = $message;
        $this->emailTemplate->variables = [
            'referLink' => $ref_link,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'companyName' => $company['company_name'],
            'companyFirstName' => $company['first_name'],
            'companyLastName' => $company['last_name'],
            'companyEmail' => $company['email']
        ];
        $this->emailTemplate->sendEmail();
    }

}