<?php

class BigCompanyVBROverTimeGraph extends Graph {

	public function __construct() {
		parent::__construct("MeanVBROverTime");
	}

	public function getTitle() {
		return "VBR of 10 users in largest companies over time";
	}

	public function recalculate($xMin, $xMax) {
		
		// Fetch the users in the biggest companies.
		$query = DB::prep("SELECT
				company.*,
				li_users.linked_in_id AS linked_in_id,
				li_users.name AS li_user_name
			FROM company
			JOIN li_users ON (li_users.company_id = company.id)
			ORDER BY people_count DESC
			LIMIT 10");
		$query->execute();

		// One line for each user.
		$numLines = $query->rowCount();

		// Query for fetching people in the company by day.
		$peopleQuery = DB::prep("SELECT
				COUNT(*) numPeoples,
				sent_to_crawler_date AS date,
				crawled_by_id
			FROM people
			WHERE
				company_id = :company_id AND
				sent_to_crawler_date >= :dateMin AND
				sent_to_crawler_date <= :dateMax
			GROUP BY
				sent_to_crawler_date, crawled_by_id");

		$i = 0;
		$peoples = [];
		while ($company = $query->fetch()) {
			$this->setLabel($i, $company['company_name'] . sprintf('(%d)', $company['id']) . " : " . $company['li_user_name']);

			// Fetch the number of inbound views.
			$queryVisitors = DB::prep("SELECT *
				FROM visitors
				WHERE
					company_id = :company_id AND
					linked_in_user_id = :linked_in_user_id AND
					date < DATE_SUB(NOW(), INTERVAL 6 DAY) AND
					date >= :dateMin AND
					date <= :dateMax
				ORDER BY date");
			$queryVisitors->execute([
				'company_id' => $company['id'],
				'linked_in_user_id' => $company['linked_in_id'],
				'dateMin' => $xMin,
				'dateMax' => $xMax
			]);

			// Load peoples if we dont have any.
			if (!isset($peoples[$company['id']])) {
				$peopleQuery->execute([
					'company_id' => $company['id'],
					'dateMin' => $xMin,
					'dateMax' => $xMax
				]);
				$peoples[$company['id']] = $peopleQuery->fetchAll();
			}

			while ($viewback = $queryVisitors->fetch()) {

				$peopleSum = 0;
				foreach ($peoples[$company['id']] as $numPeoples) {
					$numPeopleTimestamp = strtotime($numPeoples['date']);
					$viewbackTimestamp = strtotime($viewback['date']);
					if ($numPeoples['crawled_by_id'] == $company['linked_in_id'] and $numPeopleTimestamp > ($viewbackTimestamp - 7*24*3600) and $numPeopleTimestamp <= $viewbackTimestamp) {
						$peopleSum += $numPeoples['numPeoples'];
					}
				}
				if ($peopleSum > 100 and $viewback['visitor_count'] > 0) {
					$this->setValue($viewback['date'], round(100 * $viewback['visitor_count'] / $peopleSum, 2), $i);
				}
			}
			$i++;
		}
	}
}

?>