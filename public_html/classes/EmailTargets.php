<?php

class EmailTarget {

	private $id;
	private $companyId;
	private $email;
	private $variables;

	public function __construct($companyId, $email, $variables) {
		$this->companyId = $companyId;
		$this->email = $email;
		$this->variables = $variables;
		$this->id = md5($email . serialize($variables));
	}

	public function getId() {
		return $this->id;
	}

	public function getCompanyId() {
		return $this->companyId;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getVariables() {
		return $this->variables;
	}

}

class EmailTargets {

	public static function getTargets($emailTemplateName) {
		$targets = [];

		if ($emailTemplateName == "Anonymous User") {

			$query = DB::prep("SELECT *, li_users.name AS linked_in_name FROM li_users JOIN company ON(li_users.company_id = company.id) WHERE anonymous='Private mode' GROUP BY company.id");
			$query->execute();

			while ($row = $query->fetch()) {
				$name_parts = explode(" ", trim($row['linked_in_name']));
				$variables = [
					'linkedin first name' => $name_parts[0],
					'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
					'company name' => $row['company_name']
				];

				$targets[] = new EmailTarget($row['company_id'], $row['email'], $variables);
			}
		} elseif ($emailTemplateName == "Subscription: Basic only") {
			$query = DB::prep("SELECT *, li_users.name AS linked_in_name FROM li_users JOIN company ON(li_users.company_id = company.id) WHERE subscriptions = 'Basic' GROUP BY company.id");
			$query->execute();

			while ($row = $query->fetch()) {
				$name_parts = explode(" ", trim($row['linked_in_name']));
				$variables = [
					'linkedin first name' => $name_parts[0],
					'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
					'company name' => $row['company_name']
				];

				$targets[] = new EmailTarget($row['company_id'], $row['email'], $variables);
			}
		} elseif ($emailTemplateName == "Subscription: Job Seeker only") {
			$query = DB::prep("SELECT *, li_users.name AS linked_in_name FROM li_users JOIN company ON(li_users.company_id = company.id) WHERE subscriptions = 'Job Seeker' GROUP BY company.id");
			$query->execute();

			while ($row = $query->fetch()) {
				$name_parts = explode(" ", trim($row['linked_in_name']));
				$variables = [
					'linkedin first name' => $name_parts[0],
					'linkedin last name' => implode(' ', array_slice($name_parts, 1)),
					'company name' => $row['company_name']
				];

				$targets[] = new EmailTarget($row['company_id'], $row['email'], $variables);
			}
		} elseif ($emailTemplateName == "Email after 1 day") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 1) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 2 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 2) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 3 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 3) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 4 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 4) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 5 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 5) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 6 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 6) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 7 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 7) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		} elseif ($emailTemplateName == "Email after 14 days") {

			$query = DB::prep("SELECT * FROM company WHERE company_created > 0 AND state = '' AND frozen = 0");
			$query->execute();

			while ($row = $query->fetch()) {
				$dt = time() - strtotime($row['company_created']);
				$dDays = intval($dt / (3600*24));
				$variables = [
					'first name' => $row['first_name'],
					'last name' => $row['last_name'],
					'company name' => $row['company_name']
				];
				if ($dDays == 14) {
					$targets[] = new EmailTarget($row['id'], $row['email'], $variables);
				}
			}
		}

		return $targets;
	}

	public static function findEmailTarget($emailTemplateName, $id) {
		$targets = self::getTargets($emailTemplateName);
		foreach ($targets as $target) {
			if ($target->getId() == $id) return $target;
		}
		return null;
	}
}

?>
