<?php

namespace services\utilities_svc;

require_once (__DIR__ . '/../config/constants.php');


function encodeSearchUrl($url)
{
    $url = str_replace('[', '%5B', $url);
    $url = str_replace(']', '%5D', $url);
    $url = str_replace(',', '%2C', $url);
    $url = str_replace('"', '%22', $url);
    return $url;
}


?>
