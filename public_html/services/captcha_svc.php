<?php

namespace services\captcha;

$captcha_secret = '6Ld-oyUUAAAAAAW95_RAWUpj68v3iEwyD_6SsDCK';

function validate($data){
    global $captcha_secret;
    if (! isset($data['g-recaptcha-response'])) return false;
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $captcha_secret . "&response=" . $data['g-recaptcha-response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
    $obj = json_decode($response);
    return $obj->success == true;
}

?>