<?php
namespace services\version;

/**
 * @param $file_name
 * @return string
 */
function get_client_binary_version($file_name)
{
    if(empty($file_name)){
        return "";
    }
   $key = "P\x00r\x00o\x00d\x00u\x00c\x00t\x00V\x00e\x00r\x00s\x00i\x00o\x00n\x00\x00\x00";
   $fptr = fopen($file_name, "rb");
    if(!$fptr){
        return "";
    }
   $data = "";
   while (!feof($fptr))
   {
      $data .= fread($fptr, 65536);
      if (strpos($data, $key)!==FALSE)
         break;
      $data = substr($data, strlen($data)-strlen($key));
   }
   fclose($fptr);
   if (strpos($data, $key)===FALSE)
      return "";
   $pos = strpos($data, $key)+strlen($key);
   $version = "";
   for ($i=$pos; $data[$i]!="\x00"; $i+=2)
      $version .= $data[$i];
   return $version;
}

function get_version_number_as_integer($versionNumber) {

	$parts = explode(".", $versionNumber);
	$versionNumber = (int)$parts[0]*1000000000 + (int)$parts[1]*1000000 + (int)$parts[2]*1000 + (int)$parts[3];

	return $versionNumber;
}


?>