<?php

namespace services\ajax;
	
    //require_once('../authenticate.php');
define("NON_SET", 'adslUIiBuuYioinhoNb5r%&$Fdtsu78jp,,,<<<<MNj ,8h6gf%,45q11-l');

function get_param($dict, $param_name, $default = NON_SET){
    if (! isset($dict[$param_name])) {
        if ($default !== NON_SET) return $default;
        json_fail(
            ["unset parameter, and there is no default: ".$param_name]
        );
    }
    return $dict[$param_name];
}


function json_response($status, $payload = [])
{
    header('Content-Type: application/json');
    echo json_encode([
        'status' => $status,
        'payload' => $payload
    ]);
    die;
}

function json_fail($payload = [])
{
    json_response('error', $payload);
}


function json_ok($payload = [])
{
    json_response('success', $payload);
}

function get_request_data($methodsMap){
    $data = [];

    if ($_SERVER['REQUEST_METHOD'] != 'POST' and $_SERVER['REQUEST_METHOD'] != 'GET') {
        json_fail('Wrong HTTP method: ' . $_SERVER['REQUEST_METHOD']);
    }

    if (array_key_exists('GET', $methodsMap)) {
        foreach ($methodsMap['GET'] as $param) {
            $data[$param[0]] = get_param($_GET, $param[0], $param[1] ?? null);
        }
    }
    
    $has_post = array_key_exists('POST', $methodsMap);
    $has_json = array_key_exists('JSON', $methodsMap);
    
    if ($_SERVER['REQUEST_METHOD'] === 'POST'){
        if ($has_post) {
            //url-encoded parameters in POST body
            foreach($methodsMap['POST'] as $param){
                $data[$param[0]] = get_param($_POST, $param[0], $param[1] ?? null);
            }
            
        }else if ($has_json) {
            $rawData = file_get_contents("php://input");
            $data = json_decode($rawData, true);
            if ($data === null) {
                json_fail('Wrong JSON in the POST body');
            }
            
            if (isset($methodsMap['JSON'])){
                foreach($methodsMap['JSON'] as $param){
                    //for validation and assignment of defaults
                    $data[$param[0]] = get_param($data, $param[0], $param[1] ?? null);
                }
            }
        }else{
            json_fail('either JSON or encoded POST data should be present');
        }
    }else if ($has_post || $has_json) {
        json_fail('HTTP method = GET, but either JSON or encoded POST data is requested.');
    }
    
    return $data;
}


?>
