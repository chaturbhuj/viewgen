<?php

namespace services\template;

spl_autoload_register(function ($class) {
    // project-specific namespace prefix
    $prefix = 'Twig';
    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/../lib/Twig/';
    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }
    // get the relative class name
    $relative_class = substr($class, $len);
    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('_', '/', $relative_class) . '.php';
    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

//require_once(__DIR__ . '/../lib/Twig/');
//require_once(__DIR__ . '/../lib/Twig/Loader/Filesystem.php');
//require_once(__DIR__ . '/../lib/Twig/Environment.php');

/**
 * @param string|array $templates_root_folders
 * @return \Closure
 */
function get_twig($templates_root_folders){
    static $twigs_by_folders = [];
    $indicative_name = is_array($templates_root_folders) ?
        implode('|', $templates_root_folders) :
        $templates_root_folders;
    
    if (! array_key_exists($indicative_name, $twigs_by_folders)){
        $loader = new \Twig_Loader_Filesystem($templates_root_folders);
        $twigs_by_folders[$indicative_name] = new \Twig_Environment($loader, array(
            //'cache' => $templates_root_folder . '/_cache',
        ));
    }
    
    $twig = $twigs_by_folders[$indicative_name];
    
    return function (string $template_relative_path, array $context_array = []) 
    use ($twig) { 
        $template_name = $template_relative_path . '.twig';
        return $twig
            ->load($template_name)
            ->render($context_array); 
    };
}

?>