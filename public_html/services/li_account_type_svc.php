<?php

namespace services\li_account_type;

require_once (__DIR__ . '/../config/constants.php');
require_once (__DIR__ . '/../api/APIModel.php');

require_once(__DIR__ . '/utilities_svc.php');
use function services\utilities_svc\encodeSearchUrl;


function checkLIAccount($li_user){
    $isSimpleSearch = false;
    if ($li_user['li_account_type'] === \APIModel::FREE) {
        $isSimpleSearch = true;
    } elseif (empty($li_user['li_account_type'])) {
        $isSimpleSearch = (int)$li_user['have_sales_navigator'] === 1 ? false : true;
    }
    return $isSimpleSearch ? 'simple' : " sales_nav";
}

function prepareLIUrls($queryUrls, $isSimpleSearch){
    global $PROJECT_CONSTANTS;

    $encoded_urls = [];
    $the_error = false;
    $last_url = '';

    foreach ($queryUrls as $url_val) {
        $last_url = trim($url_val);
        
        //error_log('url ' . var_export($url_val, true));

        $isSalesNav = preg_match($PROJECT_CONSTANTS->LI_url_type->sales_nav_regex, $last_url);
        $isPremiumIndex = preg_match($PROJECT_CONSTANTS->LI_url_type->premium_regex_index, $last_url);
        
        if ($isPremiumIndex){
            $last_url = preg_replace(
                $PROJECT_CONSTANTS->LI_url_type->premium_regex_to_people, 
                    '${1}people${2}', $last_url);
            $isPremiumPeople = true;
        }else{
            $isPremiumPeople = preg_match($PROJECT_CONSTANTS->LI_url_type->premium_regex_people, $last_url);
        }
        
        if (! $isSalesNav && ! $isPremiumPeople) {
            $the_error = 'bad_format';
            break;
        }
        
        if (strpos($last_url, "page_num=")) {
            $the_error = 'contains_page_num';
            break;
        } 
        
        if ($isSimpleSearch && $isSalesNav){
            //In case of Manual URLs, we should keep Premium/Free URL structure wile using Sales Nav
            //but not vice versa
            //we dont need $urlBase here
            //TODO: in future, when manual URLs gone, Premium URL entered under Sales Nav account
            //TODO: should be transformed into Sales Nav URL
            $the_error = 'sales_nav_not_capable';
            break;
        }
        
        $encoded_urls[] = encodeSearchUrl($last_url);
    }
    return [
        'error' => $the_error,
        'error_url' => $last_url,
        'encoded_urls'=> $encoded_urls
    ];
}




?>
