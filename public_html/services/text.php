<?php

namespace services\text;

/**
 * Joins file paths
 * @return mixed
 */
function join_paths() {
    $paths = array();

    foreach (func_get_args() as $arg) {
        if ($arg !== '') { $paths[] = $arg; }
    }

    return preg_replace('#/+#','/',join('/', $paths));
}

?>
