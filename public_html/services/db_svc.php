<?php
    namespace services\db;
    
    require_once(__DIR__.'/../db.php');

    class DBException extends \Exception
    {
        private $arrayRepr;

        public function __construct($message, $code = 0, $query, $arguments, \Exception $previous = null)
        {
            $this->arrayRepr = [$message, $code, $query, $arguments];
            parent::__construct(var_export($this->arrayRepr, true), $code, $previous);
        }

        public function toArray()
        {
            return $this->arrayRepr;
        }

    }
    
    //---------------------------------------------------------------------------------------------------------------
    function pdo_debugStrParams($statement){
        ob_start();
        $statement->debugDumpParams();
        $r = ob_get_contents();
        ob_end_clean();
        return $r;
    }
    
    function prep_insert($table, $fields) {
	    if (! is_array($fields)){
	        $fields = preg_split('/\s*,\s*/',trim($fields));
        }
        if (! preg_match('/[\w\d]+/', $table)) {
            throw new \Exception('SQL injection or bad identifier! :' . $table);
        }
	    $sql = "INSERT INTO `" . $table . "` ( `";
        
	    foreach($fields as $f){
	        if (! preg_match('/[\w\d]+/', $f)){
                throw new \Exception('SQL injection or bad identifier! :' . $f);
            }
        }
        $sql .= implode('`, `', $fields);
        $sql .= "` ) VALUES ( :" . implode(', :', $fields) . " )";
        
   		return \DB::prep($sql);
   	}

    function _bind_and_execute($queryString, $arguments){
        $query = \DB::prep($queryString);
        return execute_prep($query, $arguments);
    }
    
    function execute_prep($query, $arguments = []){
        try{
            foreach($arguments as $name => $val){
                if (is_int($val)) {
                    $query->bindValue(':' . $name, $val, \PDO::PARAM_INT);
                }else if (is_bool($val)){
                    $query->bindValue(':'.$name, $val, \PDO::PARAM_BOOL);
                }else{
                    $query->bindValue(':'.$name, $val, \PDO::PARAM_STR);
                }
            }
            $query->execute();
            
            return $query;
            
        }catch(\PDOException $e){
            throw new DBException($e->getMessage(), $e->getCode(), $query->queryString, $arguments /*pdo_debugStrParams($query)*/);
        }
    }
    
    function gen_select($queryString, $arguments){
        try{
            $query = _bind_and_execute($queryString, $arguments);
            
           	while ($row = $query->fetch()) {
           		yield $row;
           	}
        }catch(\PDOException $e){
            throw new DBException($e->getMessage(), $e->getCode(), $queryString, $arguments);
        }
    }
    
    function select($query, $arguments, $mappingOrName = false){
        $results = [];
        foreach (gen_select($query, $arguments) as $entry){
            if (! $mappingOrName) {
                $results[] = $entry; //all fields
            }else{
                if (is_string($mappingOrName)){
                    //$mappingOrName - is a name of the field
                    $results[] = $entry[$mappingOrName];
                }else if (is_array($mappingOrName)) {
                    $row = [];
                    foreach($entry as $db_key => $value){
                        if (! is_numeric($db_key)){
                            $res_key = $mappingOrName[$db_key];
                            if ($res_key){
                                $row[$res_key] = $value;
                            }
                        }
                    }
                    $results[] = $row;
                } else {
                    throw new DBException('Invalid mapping: ', 0, $query, (string)$mappingOrName);
                }
            }
        }
        return $results;
    }

    function execute($queryString, $arguments = []){
        return _bind_and_execute($queryString, $arguments);
    }
    
    
?>