<?php
/**
 * Created by PhpStorm.
 * User: mihael
 * Date: 26.05.20
 * Time: 14:13
 */

namespace services\emailCapture;

require_once(__DIR__ . '/../../db.php');
require_once(__DIR__ . '/../../classes/numeria/Connector.php');
require_once(__DIR__ . "/../../classes/Settings.php");
require_once(__DIR__ . "/EmailCaptureException.php");

use \numeria\Connector;
use \classes\Settings;


class EmailCapture
{
    public $company_id;
    protected $settinsModel;
    protected $Numeria;

    public function __construct($company_id){
        $this->company_id = $company_id;
        $this->settingsModel = new Settings($company_id);
        $this->Numeria = new Connector('bf43309f8ef75baf5d78daca037aecaa');
    }

    public function getEmail($people_id){

        if(!$this->settingsModel->getForCompany('email_extractor-enabled')){
            throw new EmailCaptureException('Service unavailable');
        }

        $people = $this->_getPeople($people_id);

        if(!empty($people['email'])){
            return $people['email'];
        }

        if(!$people['member_id']){
            return null;
        }

        //try find email in our db in visits from this account
        $email = $this->_findEmailInOwnAccount($people['member_id']);

        if(empty($email)){
            $email = $this->_getPaidEmail($people['member_id']);
        }

        if(!empty($email)){
            $this->_saveEmail($email, $people['member_id']);
            return $email;
        }

        return $email;
    }

    public function getAll($campaign_id, $ignore_limit = false, $visited = false){
        if(!$this->settingsModel->getForCompany('email_extractor-enabled')){
            throw new EmailCaptureException('Service unavailable');
        }

        $result = ['status' => 'success'];

        $this->_getCampaign($campaign_id);

        $this->_updateAllInOwnAccount($campaign_id);

        $member_ids = $this->_getMemberIds($campaign_id, $visited);

        $limit = $this->settingsModel->getForCompany('email_extractor-limit');
        $requested = count($member_ids);
        if($requested > $limit && !$ignore_limit){
            return ['status' => 'limit', 'data' => ['limit' => $limit, 'requested' => $requested]];
        }

        foreach ($member_ids as $member_id){
            $email = null;
            try{
                $email = $this->_getPaidEmail($member_id);
            }catch(EmailCaptureException $e){
                if($e->getMessage() == 'Limit reached'){
                    break;
                }
            }

            if($email){
                $this->_saveEmail($email, $member_id);
            }
        }

        return $result;
    }

    private function _saveEmail($email, $member_id){
        $q = \DB::prep('
                    UPDATE
                      people
                    SET
                      email = :email
                    WHERE
                      member_id = :member_id
                      AND company_id = :company_id
                  ');
        $q->execute([
            'email' => $email,
            'member_id' => $member_id,
            'company_id' => $this->company_id,
        ]);
    }

    private function _getPeople($people_id){
        $q = \DB::prep('
                    SELECT
                      *
                    FROM
                      people
                    WHERE
                      company_id = :company_id
                      AND id = :id
                  ');
        $q->execute([
            'company_id' => $this->company_id,
            'id' => $people_id
        ]);

        if(!$q->rowCount()){
            throw new EmailCaptureException('People not found');
        }

        return $q->fetch();
    }

    private function _getMemberIds($campaign_id, $visited){
        $q = \DB::prep('
                    SELECT DISTINCT
                      people.member_id
                    FROM
                      people
                  '.(!$visited ? 'JOIN visit_connection ON(people.id = visit_connection.people_id)' : '').'
                    WHERE
                      people.company_id = :company_id
                      AND people.campaign_id = :campaign_id
                      AND (email = "" OR email IS NULL)
                      AND people.member_id <> 0
                    GROUP BY people.id
                  ');
        $q->execute([
            'company_id' => $this->company_id,
            'campaign_id' => $campaign_id
        ]);

        return $q->fetchAll(\PDO::FETCH_COLUMN);
    }

    private function _getCampaign($campaign_id){
        $q = \DB::prep('
                    SELECT
                      *
                    FROM
                      campaign
                    WHERE
                      company_id = :company_id
                      AND id = :id
                  ');
        $q->execute([
            'company_id' => $this->company_id,
            'id' => $campaign_id
        ]);

        if(!$q->rowCount()){
            throw new EmailCaptureException('Campaign not found');
        }

        return $q->fetch();
    }

    private function _findEmailInOwnAccount($member_id){
        $q = \DB::prep('
                    SELECT
                      email
                    FROM
                      people
                    WHERE
                      member_id = :member_id
                      AND email <> ""
                      AND email IS NOT NULL
                      AND company_id = :company_id
                    LIMIT 1
                  ');
        $q->execute([
            'member_id' => $member_id,
            'company_id' => $this->company_id,
        ]);

        return $q->fetchColumn(0);
    }

    private function _updateAllInOwnAccount($campaign_id){
        $q = \DB::prep('
                        UPDATE
                            people A
                            INNER JOIN people B
                                ON A.member_id = B.member_id
                        SET
                            A.email = B.email
                        WHERE
                            A.company_id = :company_id
                            AND A.campaign_id = :campaign_id
                            AND (A.email = "" OR A.email IS NULL)
                            AND A.member_id <> 0
                            
                            AND B.company_id = :company_id
                            AND B.email <> ""
                            AND B.email IS NOT NULL
                  ');
        $q->execute([
            'campaign_id' => $campaign_id,
            'company_id' => $this->company_id,
        ]);
    }


    private function _getPaidEmail($member_id){
        if(!$this->settingsModel->getForCompany('email_extractor-limit')){
            throw new EmailCaptureException('Limit reached');
        }
        //try find email in our db
        $email = $this->_findEmailInDb($member_id);

        if(empty($email)){
            //get from numeria
            $email = $this->Numeria->getEmailByMemberId($member_id);
        }

        if(!empty($email)){
            $this->settingsModel->setForCompany('email_extractor-limit', $this->settingsModel->getForCompany('email_extractor-limit') - 1);
        }

        return $email;
    }

    private function _findEmailInDb($member_id){
        $q = \DB::prep('
                    SELECT
                      email
                    FROM
                      people
                    WHERE
                      member_id = :member_id
                      AND email <> ""
                      AND email IS NOT NULL
                    LIMIT 1
                  ');
        $q->execute([
            'member_id' => $member_id,
        ]);

        return $q->fetchColumn(0);
    }
}