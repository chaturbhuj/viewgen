<?php
namespace services\file_logger;

const LOG_DIR = __DIR__ . '/../log/';

function log_to_file($content, $filename = 'main.log'){
    $dt = '\n\n\n\n' . (string)date('Y-m-j H:i:s:') . substr((string)(microtime()), 2, 3) . '\n\n';
    file_put_contents(LOG_DIR . $filename, $dt . (string)$content, FILE_APPEND);
}
?>