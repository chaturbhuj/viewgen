<?php
namespace services\recurly;

require_once(__DIR__.'/db_svc.php');
require_once (__DIR__.'/../lib/recurly.php');

use function \services\db\execute;


function get_subscriptions($account){
  	return \Recurly_SubscriptionList::getForAccount($account);
}

function get_active_subscription($subscriptions){
    $activeSubscriptions = array();
    $lastActivatedSubscription = null;
    foreach ($subscriptions as $tmpSubscription) {
    	if ($tmpSubscription->state == "active") {
    		$activeSubscriptions[] = $tmpSubscription;
    	}
    
    	if ($lastActivatedSubscription == null) {
    		$lastActivatedSubscription = $tmpSubscription;
    	} else if ($lastActivatedSubscription->activated_at < $tmpSubscription->activated_at) {
    		$lastActivatedSubscription = $tmpSubscription;
    	}
    }
    
    if (count($activeSubscriptions) == 1) {
    	return $activeSubscriptions[0];
    } else if (count($activeSubscriptions) > 1) {
    	new \Exception("This account has multiple active subscriptions, please contact info@searchquant.net to handle your subscriptions.");
    } else {
    	return $lastActivatedSubscription;
    }
    return null;
}

function cancel_subscription($subscription){
    $subscription->cancel();
    return execute("UPDATE company SET frozen = 1 WHERE id = :company_id ", [
        'company_id' => $_GET['company_id'],
    ]);
}

    
?>