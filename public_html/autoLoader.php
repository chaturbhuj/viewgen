<?php

function classAutoLoader($className) {
	
	$filename = dirname(__FILE__) . "/classes/$className.php";
	if (file_exists($filename)) {
		require_once($filename);
		return;
	}

}
spl_autoload_register('classAutoLoader');

?>