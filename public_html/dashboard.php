<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

$HTML_TITLE = "Dashboard";
$ACTIVE_HEADER_PAGE = "dashboard";

require_once('header.php');
require_once('autoLoader.php');
require_once("shared_functions.php");
if (!isset($_GET['company_id'])) {
	if(isset($_USER_DATA["admin"])){
		redirect("adminDashboard.php");
	}
	die;
}

header("Location: /new-ui/campaign_builder.php?company_id=" . $_GET['company_id']);
die;

?>
