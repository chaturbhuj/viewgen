(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		$( "#accordion" ).accordion({
            heightStyle: "content",
            icons:"",
            collapsible:true
        });


        $('#accordion .accClicked').off('click').click(function(){
            $(this).next().toggle('fast');
            $.ajaxSetup({cache:false});
        });

        $(".post-link").click(function(){
            var post_link = $(this).attr("href");
            $(".post-link").removeClass('active');
            $(this).addClass('active');
            
            $("#single-post-container").html("content loading");
            $("#single-post-container").load(post_link);
            
            $.smoothScroll({ scrollTarget: '#single-post-container', speed: 800 });
            return false;
        });
        
        
          $('.mejs-overlay-loading').closest('.mejs-overlay').addClass('load'); //just a helper class
        
          var $video = $('div.video video');
          var vidWidth = $video.attr('width');
          var vidHeight = $video.attr('height');
        
          $(window).resize(function() {
            var targetWidth = $(this).width(); //using window width here will proportion the video to be full screen; adjust as needed
            $('div.video, div.video .mejs-container').css('height', Math.ceil(vidHeight * (targetWidth / vidWidth)));
          }).resize();
          
        $('a.toTop').smoothScroll({speed: 800 }); 		
	});
	
})(jQuery, this);
