<?php $post = get_post($_POST['id']); ?>
<?php while (have_posts()) : the_post(); ?>
    <!-- article -->
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry">
        <h1 class="article_title"><?php the_title(); ?></h1>
        <?php the_content(); ?>
        </div>
    </article>
    <!-- /article -->
<?php endwhile; ?>