<!-- sidebar -->
<?php
$main_categories = get_categories( array(
	'taxonomy' => 'category',
	'parent' => 0,
	'hide_empty' => 0,
) );
?>
<aside class="sidebar" role="complementary">
    <form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
	    <input class="search-input" type="search" name="s" placeholder="<?php _e( 'SEARCH', 'html5blank' ); ?>">
	    <button class="search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
    </form>

    <div id="search_results">
        <?php
        echo '<h2>Search Results:</h2>';
        $i=1;
        echo '<ul>';
        if (have_posts()): while (have_posts()) : the_post();
            if($i==1){
                echo '<li><a class="post-link active" rel="'. get_the_ID() .'" href="'. get_permalink() .'">' . get_the_title() .'</a></li>';            
            }else{
                echo '<li><a class="post-link" rel="'. get_the_ID() .'" href="'. get_permalink() .'">' . get_the_title() .'</a></li>';            
            }
            $i++;
        endwhile;
        echo '</ul>';
        else:
    		echo '<h4>Sorry, no results found!</h4>';
        endif;
        ?>
    </div>
    
    <div id="accordion">
        <?php
        $cat_iterator = 0;
        foreach ( $main_categories as $cat ) :
        $pcat_totals = ipt_kb_total_cat_post_count( $cat->term_id );
        echo '<h3>' . $cat->name . ' ('.$pcat_totals.')</h3>';
        
        $cat_posts = new WP_Query( array(
            'posts_per_page' => -1,
            'cat' => $cat->term_id,
        ) );
   
        if ( $cat_posts->have_posts() ) :
        echo '<div><ul>';
        while ( $cat_posts->have_posts() ) : $cat_posts->the_post();
            
            echo '<li><a class="post-link" rel="'. get_the_ID() .'" href="'. get_permalink() .'">' . get_the_title() .'</a></li>';
            
        endwhile;
        echo '</ul></div>';
        endif;
        
        wp_reset_postdata();
        ?>        
        <?php endforeach; ?>
    </div>
    
</aside>
<!-- /sidebar -->