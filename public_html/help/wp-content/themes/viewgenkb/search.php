<?php get_header(); ?>
<div class="group main_content">
    <?php get_sidebar('search'); ?>
    
	<main role="main" id="main">
		<!-- section -->
		<section>
		<div id="single-post-container">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>       
                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry">
                    <h1 class="article_title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                    </div>
                </article>
                <!-- /article -->
                <?php break; ?>
            <?php endwhile; ?>
            <?php else: ?>
            <article>
                <div class="entry">
                    <h1 class="article_title">Sorry, no results found!</h1>
                </div>
            </article>
            <?php endif; ?>
        </div>	
		</section>
		<!-- /section -->
	</main>
</div>
<?php get_footer('search'); ?>
