<?php get_header(); ?>
<div class="group main_content">
    <?php get_sidebar(); ?>
    
	<main role="main" id="main">
		<!-- section -->
		<section>
		<div id="single-post-container">
        <div class="entry">
        <?php
            $page_slug ='intro';
            $page_data = get_page_by_path($page_slug);
            $page_id = $page_data->ID;
            echo apply_filters('the_content', $page_data->post_content);        
        ?>   
        </div>     
        </div>	
		</section>
		<!-- /section -->
	</main>
</div>
<?php get_footer(); ?>