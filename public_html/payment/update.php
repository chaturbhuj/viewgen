<?php

$AUTH_DRAFT_OK = true;

require_once('../authenticate.php');
require_once ('../lib/recurly.php');

if (!isset($_POST['recurly_token'])) die;

$result = Recurly_js::fetch($_POST['recurly_token']);

redirect('payment_methods.php?success=1');

/*

$account = $result->account->get();

// Check if the user has an account.
$query = DB::prep("select * from rc_account where account_code=?");
$query->execute(array($account->account_code));

function insert_subscription($subscription, $account_id) {

	if (!isset($_SESSION['auth']['company_id'])) return;
	$company_id = $_SESSION['auth']['company_id'];

	$query = DB::prep("select * from rc_subscription where company_id=?");
	$query->execute(array($company_id));

	if ($query->rowCount() > 0) {
		// Insert into history.
		$query = DB::prep("insert into rc_subscription_history select * from rc_subscription rcs where rcs.company_id=?");
		$query->execute(array($company_id));

		$query = DB::prep("update rc_subscription set plan_code=?, uuid=?, currency=?, quantity=? where company_id=?");
		$query->execute(array($subscription->plan->plan_code, $subscription->uuid, $subscription->currency, $subscription->quantity, $company_id));
	} else {
		// Create subscription
		$query = DB::prep("insert into rc_subscription(company_id, rc_account_id, plan_code, uuid, currency, quantity) values(?, ?, ?, ?, ?, ?)");
		$query->execute(array(
			$company_id,
			$account_id,
			$subscription->plan->plan_code,
			$subscription->uuid,
			$subscription->currency,
			$subscription->quantity
		));
	}
}

if ($query->rowCount() > 0) {
	// Update account
	$query = DB::prep("update rc_account set state=?, email=?, first_name=?, last_name=? where account_code=?");
	$query->execute(array($account->state, $account->email, $account->first_name, $account->last_name, $account->account_code));

	$query = DB::prep("select * from rc_account where account_code=?");
	$query->execute(array($account->account_code));

	$account = $query->fetch();

	insert_subscription($result, $account->id);
	redirect('payment_methods.php?success=1');
} else {
	// Create account
	$query = DB::prep("insert into rc_account (company_id, account_code, state, email, first_name, last_name) values(?, ?, ?, ?, ?)");
	$query->execute(array($_SESSION['auth']['company_id'], $account->account_code, $account->state, $account->email, $account->first_name, $account->last_name));

	if ($query->rowCount() > 0) {
		insert_subscription($result, DB::prep()->lastInsertId());
	}

	// set not draft.
	$query = DB::prep("update company set state='' where id=?");
	$query->execute(array($_SESSION['auth']['company_id']));

	redirect('payment_methods.php?success=1');
}

//if (isset($_GET['']))
*/

?>
