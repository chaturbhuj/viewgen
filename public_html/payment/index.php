<?php

require_once('../header.php');
require_once('../lib/recurly.php');

// Check if the user has an account.
$query = DB::prep("select * from rc_subscription rcs join rc_account rca on(rca.id=rcs.rc_account_id) where rcs.company_id=?");
$query->execute(array($_SESSION['auth']['company_id']));

$account = false;
if ($query->rowCount() > 0) {
	$account = $query->fetch();
}

Recurly_js::$privateKey = '46a4ec9695db4e8386e82da4741201ca';

$signature = Recurly_js::sign(
	array('subscription' => array('plan_code' => 'silver'))
);


?><!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>RecurlyJS Example</title>
    
    <?php include(__DIR__ . '/../ui/include/favicon.php'); ?>
    
	<link rel="stylesheet" href="/dev/lib/recurly-theme/default/recurly.css">
	<script src="/dev/assets/js/jquery.min.js"></script>
	<script src="/dev/assets/js/recurly.min.js"></script>
</head>
<body>

<div style="margin: 0 auto; ">
<script type="application/javascript">
Recurly.config({
	subdomain: 'searchquant',
	currency: 'USD'
});
</script>

<div id="subscribe"></div>

<?php if ($account) { ?>
<script type="application/javascript">
Recurly.buildBillingInfoUpdateForm({
	target: '#subscribe',
	accountCode: '<?=$account['account_code'] ?>',
	successURL: 'http://affecting.us/dev/payment/confirm.php',
	billingInfo: {
		firstName: '<?=$account['first_name']?>',
		lastName: '<?=$account['last_name']?>'
	},
	signature: '<?=$signature?>'
});
</script>
<?php } else { ?>
<script type="application/javascript">
Recurly.buildSubscriptionForm({
	target: '#subscribe',
	planCode: 'silver',
	successURL: 'http://affecting.us/dev/payment/confirm.php',
	signature: '<?=$signature?>'
});
</script>
<?php } ?>
</div>

</body>
</html>
