<?php
die;


require_once('../authenticate.php');
require_once ('../lib/recurly.php');

$query = DB::prep("select * from rc_subscription");
$query->execute();

while ($row = $query->fetch()) {
	$subscription = Recurly_Subscription::get($row['uuid']);

	$update = DB::prep("update rc_subscription set current_period_started_at=?, current_period_ends_at=? where company_id=?");
	$update->execute(array(
		$subscription->current_period_started_at->format('Y-m-d H:i:s'),
		$subscription->current_period_ends_at->format('Y-m-d H:i:s'),
		$row['company_id']
	));
}

?>
