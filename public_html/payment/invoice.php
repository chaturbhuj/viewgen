<?php

$STOPMENU = 1;

require_once('../header.php');
require_once ('../lib/recurly.php');

try {
	$pdf = Recurly_Invoice::getInvoicePdf(1002);
} catch (Recurly_NotFoundError $e) {
	print "Invoice not found.\n";
}

header('Content-Type: application/pdf');
echo $pdf;

?>
