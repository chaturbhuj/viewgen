<?php
//immediately send response and close connection
ignore_user_abort(true);
header("Content-Length: 0");
header("Connection: Close");
flush();
session_write_close();

global $CONFIG;
$IGNORE_AUTH = true;

require_once(__DIR__.'/../db.php');
require_once (__DIR__.'/../lib/recurly.php');
require_once __DIR__.'/../intercom/helper.php';


$post_xml = file_get_contents ("php://input");
$notification = new Recurly_PushNotification($post_xml);
//each webhook is defined by a type


$company = null;
$query = DB::prep("SELECT * FROM company WHERE recurlyAccountCode = ?");
$query->execute([$notification->account->account_code]);
if($query->rowCount() > 0){
    $company = $query->fetch();
}else{
    $query = DB::prep("SELECT * FROM company JOIN rc_account ON company.id = rc_account.company_id WHERE rc_account.account_code = ?");
    $query->execute([$notification->account->account_code]);
    $company = $query->fetch();
}

if(empty($company)){
    return;
}

$rc_account = Recurly_Account::get($notification->account->account_code);
$hasActiveSubscription = false;
$lastSubscription = null;
$canceledInTrial = false;
if ($rc_account !== null) {
    // This company has a recurly account. Look at the subscriptions.
    $subscriptions = Recurly_SubscriptionList::getForAccount($rc_account->account_code);
    foreach ($subscriptions as $subscription) {
        $canceledInTrialTMP = false;
        if ($subscription->state == "active" || $subscription->state == "future") {
            $hasActiveSubscription = true;
            $lastSubscription = $subscription;
            $canceledInTrial = false;
            break;
        }
        if ($subscription->state == "canceled" || $subscription->state == "expired") {
            if (isset($subscription->trial_started_at) && isset($subscription->trial_ends_at)) {
                $endDateTmp = !empty($subscription->canceled_at) ? $subscription->canceled_at : $subscription->expires_at;

                if ($subscription->trial_started_at->format('Y-m-d H:i:s') != $subscription->trial_ends_at->format('Y-m-d H:i:s') and
                    (strtotime($subscription->trial_ends_at->format('Y-m-d H:i:s')) >=
                        strtotime($endDateTmp->format('Y-m-d H:i:s')))) {
                    $canceledInTrialTMP = true;
                }
            }
            if(empty($lastSubscription)){
                $canceledInTrial = $canceledInTrialTMP;
                $lastSubscription = $subscription;
            }else if($lastSubscription->expires_at < $subscription->expires_at){
                $canceledInTrial = $canceledInTrialTMP;
                $lastSubscription = $subscription;
            }
        }
    }
}

$event_name = "";
switch ($notification->type){
    case "new_account_notification":
        break;
    case "updated_account_notification":
        break;
    case "canceled_account_notification":
        break;
    case "billing_info_updated_notification":
        break;
    case "billing_info_update_failed_notification":
        break;
    case "new_subscription_notification":
        break;
    case "updated_subscription_notification":
        break;
    case "canceled_subscription_notification":
        $event_name = "rc_subscription-canceled";
        break;
    case "expired_subscription_notification":
        if($lastSubscription->state === "expired") {
            $query = DB::prep("UPDATE company SET frozen = 1 WHERE id = ?");
            $query->execute([$company['id']]);
        }
        $event_name = "rc_subscription-expired";
        break;
    case "renewed_subscription_notification":
        break;
    case "reactivated_account_notification":
        break;
    default:
}

$intercomHelper = new IntercomHelper($CONFIG['intercom_api_key']);
$query = DB::prep("SELECT * FROM company WHERE id = ?");
$query->execute([$company['id']]);
$company = $query->fetch();
$intercomHelper->pushCompanyData($company);
$intercomHelper->pushEvent($company, $event_name);

?>
