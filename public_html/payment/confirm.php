<?php

$AUTH_DRAFT_OK = true;

require_once('authenticate.php');
require_once ('lib/recurly.php');

if (!isset($_POST['recurly_token'])) die;

$result = Recurly_js::fetch($_POST['recurly_token']);

$account = $result->account->get();

// Update account with contact info.
$account->company_name = $_COMPANY_DATA['company_name'];
$account->first_name = $_COMPANY_DATA['first_name'];
$account->last_name = $_COMPANY_DATA['last_name'];
$account->email = $_COMPANY_DATA['email'];
$account->update();

// Check if the user has an account.
$query = DB::prep("select * from rc_account where account_code=?");
$query->execute(array($account->account_code));

if ($query->rowCount() > 0) {
	// Update account
	$query = DB::prep("update rc_account set company_id=?, state=?, email=?, first_name=?, last_name=? where account_code=?");
	$query->execute(array($_SESSION['auth']['company_id'], $account->state, $account->email, $account->first_name, $account->last_name, $account->account_code));

	$query = DB::prep("update company set state='',account_type='paid' where id=?");
	$query->execute(array($_SESSION['auth']['company_id']));
	
	redirect('sign_up_step_two_success.php');
} else {
	// Create account
	$query = DB::prep("insert into rc_account (company_id, account_code, state, email, first_name, last_name) values(?, ?, ?, ?, ?, ?)");
	$query->execute(array($_SESSION['auth']['company_id'], $account->account_code, $account->state, $account->email, $account->first_name, $account->last_name));

	if ($query->rowCount() > 0) {
		// Create subscription
		$query = DB::prep("insert into rc_subscription(company_id, rc_account_id, state, plan_code, uuid, currency, quantity, current_period_started_at, current_period_ends_at) values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$query->execute(array(
			$_SESSION['auth']['company_id'],
			DB::get_instance()->lastInsertId(),
			$result->state,
			$result->plan->plan_code,
			$result->uuid,
			$result->currency,
			$result->quantity,
			$result->current_period_started_at->format('Y-m-d H:i:s'),
			$result->current_period_ends_at->format('Y-m-d H:i:s')
		));
	}

	// set not draft.
	$query = DB::prep("update company set state='',account_type='paid' where id=?");
	$query->execute(array($_SESSION['auth']['company_id']));

	redirect('sign_up_step_two_success.php');
}

?>
