<?php
$STOPHEAD = true;
$STOPMENU = true;
$HTML_TITLE = "Client Versions";
require_once ('lib/recurly.php');
require_once('header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

if($_SERVER['REQUEST_METHOD'] === "POST"){
	$limit = "";

	if(isset($_POST['length'])){
		$limit .= " LIMIT ".intval($_POST['length'])." ";
	}
	if(isset($_POST['start'])){
		$limit .= " OFFSET ".intval($_POST['start'])." ";
	}

    $order = "";
	if(isset($_POST['order'])){
        foreach($_POST['order'] as $item){
            $dir = $item['dir'] === "asc" ? "ASC" : "DESC";
            switch($item['column']){
                case 0:
                    $order .= "li_users.company_id ".$dir.", ";
                    break;
                case 1:
                    $order .= "company.company_name ".$dir.", ";
                    break;
                case 2:
                    $order .= "company.email ".$dir.", ";
                    break;
                case 3:
                    $order .= "li_users.name ".$dir.", ";
                    break;
                case 4:
                    $order .= "IF(new_client_version IS NULL, client_version, new_client_version) ".$dir.", ";
                    break;
                case 6:
                    $order .= "li_users.last_connection ".$dir.", ";
                    break;
                case 7:
                    $order .= "company.last_visit ".$dir.", ";
                    break;
            }
        }
        $order = trim($order, ", ");
	}

    $search = "";
    if(isset($_POST['search']) && !empty($_POST['search']['value'])){
        $searchValue = $_POST['search']['value'];
        $search = "
            AND (
               li_users.company_id = ".intval($searchValue)."
             OR
               company.company_name LIKE '%".$searchValue."%'
             OR
               company.email LIKE '%".$searchValue."%'
             OR
               li_users.name LIKE '%".$searchValue."%'
             OR
               IF(client_versions.name IS NULL, client_version, client_versions.name) LIKE '%".$searchValue."%'
            )
        ";
    }


	$query = DB::prep("
		SELECT
 			count(*)
		FROM 
			li_users 
			JOIN company ON (company.id = li_users.company_id)
		WHERE 
			company.company_name NOT LIKE '%NotInUse' AND
			company.frozen = 0 AND
			li_users.hidden = 0
	");
	$query->execute();
	$count = $query->fetchColumn();

    $query = DB::prep("SELECT 
 			count(*)
	FROM li_users 
	JOIN company ON (company.id = li_users.company_id)
	LEFT JOIN client_versions ON (li_users.client_version_id = client_versions.id)
	WHERE 
		company.company_name NOT LIKE '%NotInUse' AND
		company.frozen = 0 AND
		li_users.hidden = 0
		".$search."
    ");
    $query->execute();
    $filteredCount = $query->fetchColumn();


	$query = DB::prep("SELECT 
		li_users.*, 
		company.company_name, 
		company.email, 
		company.last_visit AS company_last_visit,
		client_versions.name AS new_client_version
	FROM li_users 
	JOIN company ON (company.id = li_users.company_id)
	LEFT JOIN client_versions ON (li_users.client_version_id = client_versions.id)
	WHERE 
		company.company_name NOT LIKE '%NotInUse' AND
		company.frozen = 0 AND
		li_users.hidden = 0
		".$search."
	ORDER BY ".$order." ".$limit);
	$query->execute();
	$li_users = $query->fetchAll();

	$queryVisitors = DB::prep("SELECT SUM(visitor_count) num
	FROM visitors
	WHERE
		company_id = :company_id AND
		linked_in_user_id = :linked_in_user_id AND
		date >= DATE_SUB(NOW(), interval 14 day)
	ORDER BY date");

	$data = [];
	foreach ($li_users as $li_user) {

		$queryVisitors->execute([
			'company_id' => $li_user['company_id'],
			'linked_in_user_id' => $li_user['linked_in_id']
		]);
		$visitors = $queryVisitors->fetch();

		$data[] = [
			'<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$li_user['company_id'].'&user_id='.$li_user['id'].'">'.$li_user['company_id'].'</a>',
			'<a target="_blank" href="/new-ui/campaign_builder.php?company_id='.$li_user['company_id'].'&user_id='.$li_user['id'].'">'.$li_user['company_name'].'</a>',
			$li_user['email'],
			$li_user['name'],
			'<a href="/list_company_client_versions.php?company_id='.$li_user['company_id'].'">'.($li_user['new_client_version'] ? $li_user['new_client_version'] : $li_user['client_version']).'</a>',
			'<td bgcolor=white> <a href=/client/download.php?company_id='.$li_user['company_id'].'&type='.(strpos($li_user['new_client_version'],"indows")?"windows":"mac").'><font color=blue><u>this link</u></font></a>',
			'User '.$li_user['last_connection'],
			$li_user['company_last_visit'],
			$visitors['num']
		];
	}


	$response = [
		"draw" => intval($_POST['draw']),
		"recordsTotal" => $count,
    	"recordsFiltered" => $filteredCount,
		"data" => $data
	];

	http_response_code(200);
	header('Content-Type: application/json');
	echo json_encode($response);
	die;
}

?>





<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.15/fh-3.1.2/r-2.1.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.15/fh-3.1.2/r-2.1.1/datatables.min.js"></script>

<script>
	$(document).ready(function() {
		$('#data').DataTable( {
            "fixedHeader": {
                "header": true
            },
            "columnDefs": [
                { "orderable": false, "targets": [5,8] }
            ],
            "order": [[ 4, "desc" ], [7, "desc"]],
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: "list_client_versions.php",
				type: 'POST'
			}
		} );
	} );
</script>
<table id="data" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>Company ID</th>
			<th>Company</th>
			<th>Company Email</th>
			<th>Linked In User</th>
			<th>Client Version</th>
			<th>Download Link</th>
			<th>Last Active</th>
			<th>Company Last Visit</th>
			<th>Viewbacks last 14 days</th>
		</tr>
	</thead>
</table>
<?php




require_once('footer.php');

?>
