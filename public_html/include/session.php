<?php

require_once __DIR__ . '/../config.php';

require_once(dirname(__FILE__) . '/../db.php');

if (!class_exists('Session')) {
class Session {

	public static function start() {
        global $CONFIG;
		session_set_save_handler(
			array('Session', 'onOpen'),
			array('Session', 'onClose'),
			array('Session', 'onRead'),
			array('Session', 'onWrite'),
			array('Session', 'onDestroy'),
			array('Session', 'onGarbageCollect')
		);
		//see here:  https://stackoverflow.com/a/1457582/7462705
        //ini_set('session.cookie_domain', '.viewgentools.com');  -superfluous 
        session_name("viewgen_session"); //recommended due to some buggy behaviour
        if ($CONFIG['localhost']){
            session_set_cookie_params(0, '/', stristr($CONFIG['localhost'], ':', true));
        }else{
            session_set_cookie_params(0, '/', '.viewgentools.com');
        }
        session_start();
	}

	public static function destroy() {
		session_destroy();
	}

	public static function setCookie($name, $value, $expires, $path) {
		setcookie($name, $value, $expires, $path);
	}

	public static function onOpen($savePath, $sessionName) {
		return true;
	}

	public static function onClose() {
		return true;
	}

	public static function onRead($id) {

		$query = DB::prep("SELECT * FROM session WHERE sessionId = :sessionId");
		$query->execute([
			'sessionId' => $id
		]);


		if ($query->rowCount() > 0) {
			$row = $query->fetch();
			return $row['sessionData'];
		}

		return "";
	}

	public static function onWrite($id, $data) {

		if ($data === "") return true;

		$query = DB::prep("INSERT IGNORE INTO session (sessionId, sessionData) VALUES(?, ?) ON DUPLICATE KEY UPDATE
			sessionData = ?, lastUsed = NOW()");
		$query->execute(array($id, $data, $data));

		return true;
	}

	public static function onDestroy($id) {

		$query = DB::prep("DELETE FROM session WHERE sessionId = ?");
		$query->execute(array($id));

		return true;
	}

	public static function onGarbageCollect($maxlifetime) {

		$query = DB::prep("DELETE FROM session WHERE TIMESTAMPDIFF(DAY, lastUsed, NOW()) > 100");
		$query->execute(array());

		return true;
	}
}

Session::start();
if (isset($GLOBALS['NOSESSION']) and $GLOBALS['NOSESSION'] === true) {

} else {
	//ini_set('session.gc_maxlifetime', 7*24*3600);
	//session_set_cookie_params(7*24*3600);
	//session_start();
}

}



?>
