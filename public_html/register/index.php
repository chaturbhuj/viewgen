<?php

require('../include/session.php');

if (!isset($_GET['code'])) die;

$code = $_GET['code'];

$availableCodes = array(
		'dirk',
		'jamieshanks',
		'villamizar',
		'scottscales',
		'vargas',
		'fischbein',
		'3hr84y',
		'dougmitchell',
		'planb4sales',
		'ringlead',
		'maabadi',
		'ephraim',
		'trube',
		'affg',
		'brettmartin',
		'scottward',
		'lovato',
		'richardyoung',
		'bear2bull',
		'viveka',
		'30-Day Free Trial',
		'0746',
		'200',
		'7_Day_Trial_1_User',
		'7_Day_Trial_2_Users',
		'7_Day_Trial_3_Users',
		'7_Day_Trial_4_Users',
		'7_Day_Trial_5_Users',
'6521',
'7821',
'6924',
'3215',
'2165',
'bluestartups'
);

foreach ($availableCodes as $availableCode) {
	if ($code == $availableCode) {
		$selectedCode = $availableCode;
		break;
	}
}
if ($selectedCode !== null) {
	// Add tracking to session.
	$_SESSION['_TRACKING'] = $selectedCode;
}
if( $selectedCode == '7_Day_Trial_1_User' or
	$selectedCode == '7_Day_Trial_2_Users' or
	$selectedCode == '7_Day_Trial_3_Users' or
	$selectedCode == '7_Day_Trial_4_Users' or
	$selectedCode == '7_Day_Trial_5_Users'
	) {
	header('Location: /freetrial.php');
} else if ($selectedCode == 'bluestartups') {
	header('Location: /sign_up.php');
} else {
	header('Location: /');
}


?>
