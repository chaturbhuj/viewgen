<?php
ini_set('memory_limit','2048M');

$HTML_TITLE = "Prospects";
if (isset($_GET["export"])){
	$STOPHEAD = true;
	$STOPMENU = true;
}
require_once('../header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');

function clean($string) {
   return trim(preg_replace("/[\'\"]/", '', $string)); // Removes special chars.
}

function prepareParameter($name){
    $filter = "";
    $val = "";
    if (isset($_GET[$name])) {
        $val = clean($_GET[$name]);
    }
    if ($val != "") {
        $parts = explode("\n", $val);
        $first = true;
        $filter = "(";
        foreach ($parts as $part) {
            if (!$first){
                $filter .= " OR ";
            }
            $filter .= " ".$name." LIKE '%".qq(trim($part))."%'";
            $first = false;
        }
        $filter .= ") AND ";
    }
    return [
        'filter' => $filter,
        'val' => $val
    ];
}

function prepareKeywordsParameter(){
    $fields = [
        'company',
        'industry',
        'location',
        'countryCode',
        'tagline',
        'firstName',
        'lastName',
        'email'
    ];
    $filter = "";
    $val = "";
    if (isset($_GET['keywords'])) {
        $val = clean($_GET['keywords']);
    }
    if ($val != "") {
        $parts = explode("\n", $val);
        $first = true;
        $filter = "(";
        foreach ($parts as $part) {
            foreach ($fields as $field){
                if (!$first){
                    $filter .= " OR ";
                }
                $filter .= " ".$field." LIKE '%".qq(trim($part))."%'";
                $first = false;
            }
        }
        $filter .= ") AND ";
    }
    return [
        'filter' => $filter,
        'val' => $val
    ];
}

$company = prepareParameter('company');
$industry = prepareParameter('industry');
$location = prepareParameter('location');
$countryCode = prepareParameter('countryCode');
$tagline = prepareParameter('tagline');
$firstName = prepareParameter('firstName');
$lastName = prepareParameter('lastName');
$email = prepareParameter('email');
$keywords = prepareKeywordsParameter();

$premiumFilter = "";
$premium = "";
if (isset($_GET["premium"])) {
    $premium = clean($_GET["premium"]);
}
if ($premium != "") {
    $premium = intval($premium);
    $premiumFilter .= " premiumServiceLevel = ".qq($premium)." AND ";
}

$limit = 1000;
if (isset($_GET["limit"])) {
	$limit = (int)$_GET["limit"];
}

$query = DB::prep("
SELECT * 
FROM prospects
WHERE 
	".$countryCode['filter']."
	".$location['filter']."
	".$industry['filter']."
	$premiumFilter
	".$tagline['filter']."
	".$company['filter']."
	".$firstName['filter']."
	".$lastName['filter']."
	".$email['filter']."
	".$keywords['filter']."
	tagline NOT LIKE '%microsoft%' AND tagline NOT LIKE '%linkedin%' AND tagline NOT LIKE '%linked in%' AND
	company NOT LIKE '%microsoft%' AND company NOT LIKE '%linkedin%' AND company NOT LIKE '%linked in%' AND
	email NOT LIKE '%microsoft%' AND email NOT LIKE '%linkedin%' AND email NOT LIKE '%linked in%'
ORDER BY firstEncounter DESC
LIMIT $limit
");

$query->execute();
if (!isset($_GET["export"])){
//<input type=text name=company value='$company'>
echo "


<form>
<table>
<tr>
<td>
Limit<br>
<select name=limit>
	<option ".($limit==1000?"selected":"").">1000</option>
	<option ".($limit==3000?"selected":"").">3000</option>
	<option ".($limit==10000?"selected":"").">10000</option>
	<option ".($limit==50000?"selected":"").">50000</option>
	<option ".($limit==100000?"selected":"").">100000</option>
	<option ".($limit==300000?"selected":"").">300000</option>
	<option ".($limit==500000?"selected":"").">500000</option>
</select>
</td>
<td>
First<br>
<textarea rows='5' name=firstName>".$firstName['val']."</textarea>
</td><td>
Last<br>
<textarea rows='5' name=lastName>".$lastName['val']."</textarea>
</td><td>
Email<br>
<textarea rows='5' name=email>".$email['val']."</textarea>
</td><td>
Country Code<br>
<textarea rows='5' name=countryCode>".$countryCode['val']."</textarea>
</td><td>
Location<br>
<textarea rows='5' name=location>".$location['val']."</textarea>
</td><td>
Industry<br>
<textarea rows='5' name=industry>".$industry['val']."</textarea>
</td><td>
Premium<br>
<textarea rows='5' name=premium>$premium</textarea>
</td><td>
Title<br>
<textarea rows='5'  cols=35 cols=25 name=tagline>".$tagline['val']."</textarea>
</td><td>
Company<br>
<textarea rows='5' cols=35 name=company>".$company['val']."</textarea>
</td><td>
Keywords<br>
<textarea rows='5' cols=35 name=keywords>".$keywords['val']."</textarea>
</td><td>
We also make sure that 'linkedin'<br> or 'microsoft' does not appear<br> in email, company or title
</td><td>
&nbsp;
</td><td>
<label><input type=checkbox name=export value='export as csv'>Export as CSV</label>
</td><td>
<input type=submit value='Go'>
</td>
</tr>
</table>
</form>

<table class=table>
<tr>
	<th>#</th>
	<th>firstEncounter</th>
	<th>First</th>
	<th>Last</th>
	<th>Email</th>
	<th>Company</th>
	<th>Premium</th>
	<th>Title</th>
	<th>Industry</th>
	<th>Location</th>
	<th>Country Code</th>
</tr>";
}
$nr = 1;
if (isset($_GET["export"])){
	header("Content-type: application/octet-stream");
	header('Content-Disposition: attachment; filename="Prospects.csv"');
	header("Pragma: no-cache");
	header("Expires: 0");
	$row = ["Timestamp","First","Last","Email","Company","Premium","Title","Industry","Location","Country Code"];
	echo "sep=,\r\n";
	echo '"'.implode('","',$row).'"'."\r\n";
}

$results = $query->fetchAll();
foreach ($results as $data) {
	if (isset($_GET["export"])){
	$row = ["$data[firstEncounter]","$data[firstName]","$data[lastName]","$data[email]","$data[company]","$data[premiumServiceLevel]","$data[tagline]","$data[industry]","$data[location]","$data[countryCode]"];
	echo '"'.implode('","',$row).'"'."\r\n";
	} else {
	echo "<tr>
		<td>$nr</td>
		<td>$data[firstEncounter]</td>
		<td>$data[firstName]</td>
		<td>$data[lastName]</td>
		<td>$data[email]</td>
		<td>$data[company]</td>
		<td>$data[premiumServiceLevel]</td>
		<td>$data[tagline]</td>
		<td>$data[industry]</td>
		<td>$data[location]</td>
		<td>$data[countryCode]</td>
	</tr>";
	}
	$nr++;
}
if (isset($_GET["export"])){
	die;
} else {
	echo "</table>";
}
?>