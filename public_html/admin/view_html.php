<?php
$STOPHEAD = true;
$STOPMENU = true;
include('../header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}
error_reporting(E_ALL);
ini_set('display_errors', '1');

if (!isset($_GET['linked_in_id']) and !isset($_GET['ssi_id'])) {
	die;
}
function myJsonDecode($jsonText){
	$jsonObject = json_decode('["'.$jsonText.'"]',true);
	return $jsonObject[0];
}

if (isset($_GET["ssi_id"])) {
	$query = DB::prep("select * from ssi where ssi_id = ?");
	$query->execute(array($_GET['ssi_id']));
	$ssi_data = $query->fetch();
	
	echo str_replace("linkedin.com","REMOVEDURL",str_replace("staticlicdn.com","REMOVEDURL",$ssi_data["ssi_html"]));
} else {
	$query = DB::prep("select * from li_users where linked_in_id = ? and company_id = ?");
	$query->execute(array($_GET['linked_in_id'], $_GET['company_id']));
	$li_user = $query->fetch();
	echo str_replace("linkedin.com","REMOVEDURL",str_replace("staticlicdn.com","REMOVEDURL",$li_user["anonymousHtml"]));
}





?>
