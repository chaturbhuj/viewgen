<?php

$HTML_TITLE = "List Companies";
require_once('../header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Company Name</th>
						<th>Last Active</th>
						<th>Last Anonymous Email Sent</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<? foreach ($companies as $company) { ?>
						<tr>
							<th><??></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					<? } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
