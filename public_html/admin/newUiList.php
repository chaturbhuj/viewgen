<?php

$HTML_TITLE = "New UI List";

require_once('../header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');


$query = "SELECT 
*,
NOW() currentTimeStamp
FROM li_users
JOIN company ON li_users.company_id = company.id
WHERE
	li_users.have_new_interface = 1 AND
	hidden = 0 AND
	company_name NOT LIKE '%NotInUse'
ORDER BY new_interface_detected DESC
";

$result = mysql_query($query);

$output = "
<h2>User with new interface</h2>
<table border=1><tr>
		<td>Company</td>
		<td>User</td>
		<td>Detected New Interface</td>
		<td>Phone</td>
		<td>Email</td>
	</tr>";
$currentTimeStamp = "";
$firstRow = true;
while($data = mysql_fetch_array($result)){

	$output .= "
	<tr>
		<td><a href=/dashboard.php?company_id=".$data["company_id"]."><u>".$data["company_name"]."</u></a></td>
		<td>".$data["name"]."</td>
		<td>".($data["new_interface_detected"] != "0000-00-00 00:00:00"?date("M d Y H:i:s", strtotime($data["new_interface_detected"])):"")."</td>
		<td>".$data["phone"]."</td>
		<td>".$data["email"]."</td>
	</tr>";
	$currentTimeStamp = date("M d Y H:i:s", strtotime($data["currentTimeStamp"]));
}
$output .= "</table>";
echo "Current Time: $currentTimeStamp<br>";
echo $output;

?>