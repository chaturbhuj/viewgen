<?php

//$list_id = 'a18de8efa0';
//$api_key = '09d2a9949980387d779bee312baf9c1d-us15';

//include('../classes/mailchimp/Mailchimp.php');
//$mailChimp = new MailChimp($api_key);

$HTML_TITLE = "List Companies";

require_once('../header.php');
require_once(__DIR__."/../classes/Settings.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

if (!isset($_GET['company_id'])) {
	redirect("/adminDashboard.php");
	die;
}

$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
$query->execute([
	'company_id' => $_GET['company_id']
]);

if ($query->rowCount() == 0) {
	redirect("/adminDashboard.php");
	die;
}

$company = $query->fetch();

$editableFields = [
	'company_name' => 'Company Name',
	'first_name' => 'First Name',
	'last_name' => 'Last Name',
	'email' => 'Email',
	'phone' => 'Phone',
	'company_do_not_email' => 'Do Not Email',
	'is_test' => 'Is Test',
	'do_not_autorerun' => 'Do not autorerun campaigns',
	'recurlyAccountCode' => 'Recurly account code',
];

$settingsModel = new \classes\Settings($company['id']);

if (isset($_POST['action']) && $_POST['action'] == "Save") {

//	if ($_POST['email'] != $company['email']) {
//		// Handle update in mailchimp.
//		$mailChimp->lists->unsubscribe($list_id, array('email' => $company['email']), true, false, false);
//	}

	foreach ($editableFields as $field => $_) {
		$update[] = sprintf("%s = %s", $field, DB::get_instance()->quote($_POST[$field]));
	}
	$update = implode(", ", $update);
	$query = sprintf("UPDATE company SET %s WHERE id = :company_id", $update);

	$query = DB::prep($query);
	$query->execute(['company_id' => $company['id']]);


    $settings = !empty($_POST['settings']) ? $_POST['settings'] : [];

    foreach($settings as $key => $value){
        $settingsModel->setForCompany($key, $value);
    }


//	try {
//		$mailChimp->lists->subscribe($list_id, array('email' => $_POST['email']), [
//			'C_PHONE' => $_POST['phone'],
//			'FNAME' => $_POST["first_name"],
//			'LNAME' => $_POST["last_name"],
//			'C_NAME' => $_POST['company_name'],
//			'NO_MAIL' => $_POST['company_do_not_email']
//		], 'html', false, true);
//	} catch (Exception $error) {
//	}

	redirect("/admin/edit_company.php?company_id=" . $company['id']);
}

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
            <h2>Account Details</h2>
            <div class="row rowbox">
                <form method="post">
                    <? foreach ($editableFields as $field => $name) { ?>
                        <div class="form-group">
                            <label><?=$name?></label>
                            <input type="text" name="<?=$field?>" value="<?=htmlspecialchars($company[$field])?>" class="form-control">
                        </div>
                    <? } ?>
                    <div class="form-group">
                        <label>ITC</label>
                        <input type='hidden' value='0' name='settings[itc-enabled]'>
                        <input type="checkbox" name="settings[itc-enabled]" value="1" class="form-control" <?=$settingsModel->getForCompany('itc-enabled') ? 'checked="checked"' : '' ?> style="float: unset" >
                    </div>
                    <div class="form-group">
                        <label>Uninviter</label>
                        <input type='hidden' value='0' name='settings[uninviter-enabled]'>
                        <input type="checkbox" name="settings[uninviter-enabled]" value="1" class="form-control" <?=$settingsModel->getForCompany('uninviter-enabled') ? 'checked="checked"' : '' ?> style="float: unset" >
                    </div>
                    <div class="form-group">
                        <label>Autoaccept invites</label>
                        <input type='hidden' value='0' name='settings[invite-auto_accept]'>
                        <input type="checkbox" name="settings[invite-auto_accept]" value="1" class="form-control" <?=$settingsModel->getForCompany('invite-auto_accept') ? 'checked="checked"' : '' ?> style="float: unset" >
                    </div>
                    <div class="form-group">
                        <label>Disable visited profile skip</label>
                        <input type='hidden' value='0' name='settings[visiting-disable_visited_skip]'>
                        <input type="checkbox" name="settings[visiting-disable_visited_skip]" value="1" class="form-control" <?=$settingsModel->getForCompany('visiting-disable_visited_skip') ? 'checked="checked"' : '' ?> style="float: unset" >
                    </div>
                    <div class="form-group">
                        <label>Enable Numeria</label>
                        <input type='hidden' value='0' name='settings[email_extractor-enabled]'>
                        <input type="checkbox" name="settings[email_extractor-enabled]" value="1" class="form-control" <?=$settingsModel->getForCompany('email_extractor-enabled') ? 'checked="checked"' : '' ?> style="float: unset" >
                        <label>Enable "Get all" button for visited profiles</label>
                        <input type='hidden' value='0' name='settings[email_extractor_visited-enabled]'>
                        <input type="checkbox" name="settings[email_extractor_visited-enabled]" value="1" class="form-control" <?=$settingsModel->getForCompany('email_extractor_visited-enabled') ? 'checked="checked"' : '' ?> style="float: unset" >
                        <input type='date' value='<?=$settingsModel->getForCompany('email_extractor-expiration_date') ?>' name='settings[email_extractor-expiration_date]'>
                        <input type='number' value='<?=$settingsModel->getForCompany('email_extractor-limit') ?>' name='settings[email_extractor-limit]'>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default" name="action" value="Save">Save</button>
                    </div>
                </form>
            </div>
		</div>
        <div class="col-md-4">
            <h2>KPIs</h2>
            <div class="row rowbox"></div>
        </div>
        <div class="col-md-4">
            <h2>Account Stats (EXPERIMENTAL!)</h2>
            <div class="row rowbox">
                <button class="btn btn-default" value="">Refund</button>
                <button id="edit-company-cancel-subscription" class="btn btn-default" value="">Cancel</button>
                <button class="btn btn-default" value="">Apply Coupon</button>
            </div>
        </div>
	</div>
</div>
<script>
    $(function(){
        $('#edit-company-cancel-subscription').on('click', function (event) {
            ajaxPostJSON('/new-ui/api/recurly.php', {action: 'cancel-subscription'}, function(payload){
                alert('Done: ' + JSON.stringify(payload, null, '  |'))
            })
        });
    });
</script>
