<?php

$HTML_TITLE = "Contact Form";

require_once('../header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');


$query = "SELECT 
*,
NOW() currentTimeStamp
FROM contactForm

ORDER BY contactFormId DESC
";

$result = mysql_query($query);

echo "
<h2>Contact</h2>
<table border=1><tr>
		<td>Received</td>
		<td>Name</td>
		<td>Company</td>
		<td>Email</td>
		<td>Phone</td>
		<td>How did you hear about us?</td>
		<td>What are you interested in?</td>

	</tr>";
$currentTimeStamp = "";
while($data = mysql_fetch_array($result)){
	echo "
	<tr>
		<td>".date("M d Y H:i:s", strtotime($data["contactFormCreated"]))."</td>
		<td>".$data["name"]."</td>
		<td>".$data["company"]."</td>
		<td>".$data["email"]."</td>
		<td>".$data["phone"]."</td>
		<td>".$data["howDidYouHearAboutUs"]."</td>
		<td>".$data["whatAreYouInterestedIn"]."</td>
	</tr>";
	$currentTimeStamp = date("M d Y H:i:s", strtotime($data["currentTimeStamp"]));
}
echo "</table>
<br>Current Time: $currentTimeStamp";


?>