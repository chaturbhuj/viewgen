<?php

$HTML_TITLE = "Subscription Rate Per Title";
require_once ('../lib/recurly.php');
require_once('../header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');

if (!isset($_GET["input"])) {
	$_GET["input"] = "Sales\nMarketing";
} else {
	$_GET["input"] = trim($_GET["input"]);
}
if (!isset($_GET["sample_size"])) {
	$_GET["sample_size"] = 100000;
}
$keywords = explode("\n",$_GET["input"]);
for($i=0; $i< count($keywords);$i++) {
	$keywords[$i] = trim($keywords[$i]);
}
$aggregates = array();
foreach ($keywords as $keyword) {
	$aggregates[$keyword] = array();
	$aggregates[$keyword]["premium_count"] = 0;
	$aggregates[$keyword]["total_count"] = 0;
	$aggregates[$keyword]["keyword"] = $keyword;
	$aggregates[$keyword]["samples"] = array();
	
}

$query = "
SELECT 
	title,
	premium
FROM people 
WHERE sent_to_crawler_date > '2016-01-01'
LIMIT ".intval($_GET["sample_size"])."
";
$result = mysql_query($query);
while ($data = mysql_fetch_array($result)) {
	foreach ($keywords as $keyword) {
		if (stripos($data["title"], $keyword) !== false) {
			$aggregates[$keyword]["total_count"] ++;
			if ($data["premium"]) {
				$aggregates[$keyword]["premium_count"] ++;
			}
			if (count($aggregates[$keyword]["samples"]) < 7) {
				$aggregates[$keyword]["samples"][] = $data["title"];
			}
		}
	}
}
//print_r($aggregates);
echo "
<form>
<textarea rows=5 cols=80 name=input>
$_GET[input]
</textarea><br>
Sample Size (higher number takes longer to load): <input type=text name=sample_size value='$_GET[sample_size]'><br>
<input type=submit value='Search'>
</form>
<table class='table'>
	<tr>
		<th>Keyword</th>
		<th>Total Matches</th>
		<th>Matches With Premium</th>
		<th>Premium Rate</th>
		<th>Sample</th>
		<th>Sample</th>
		<th>Sample</th>
		<th>Sample</th>
		<th>Sample</th>
		<th>Sample</th>
		<th>Sample</th>
	</tr>
";
foreach ($keywords as $keyword) {
	echo "<tr>
		<td>$keyword</td>
		<td>".$aggregates[$keyword]["total_count"]."</td>
		<td>".$aggregates[$keyword]["premium_count"]."</td>
		<td>";
		if ($aggregates[$keyword]["total_count"] > 0) {
			echo number_format(($aggregates[$keyword]["premium_count"]/$aggregates[$keyword]["total_count"])*100,2)."%";
		}
	echo "</td>";
	foreach ($aggregates[$keyword]["samples"] as $sample) {
		echo "<td>$sample</td>";
	}
	echo "</tr>";
}
echo "</table>"


?>