<?php

$query = DB::prep("
SELECT *,
li_users.id li_users_id,
prosperworks_id,
company.first_name,
company.last_name
FROM li_users 
LEFT JOIN company ON company.id = li_users.company_id
WHERE
	hidden = 0 AND
	company_name NOT LIKE '%NotInUse' AND
	deleted = 0 AND
	prosperworks_people_id = 0 AND
	name != ''
ORDER BY company_id DESC, li_users.id DESC");
$query->execute(array());

echo "<table border=1>";
$lastCompany = "";
while($data = $query->fetch()) {
	$userNumber = 1;
	if ($lastCompany == $data["company_name"]) {
		$userNumber++;
	} else {
		$lastCompany = $data["company_name"];
	}
	echo "<tr>
		<td>$userNumber</td>
		<td>$data[company_id]</td>
		<td>$data[prosperworks_id]</td>
		<td>$data[company_name]</td>
		<td>$data[first_name]</td>
		<td>$data[last_name]</td>
		<td>$data[name]</td>
	</tr>";
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/people"); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
	'X-PW-Application:developer_api',
	'Content-Type: application/json',
	'X-PW-UserEmail:chris@searchquant.net'
	));
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		json_encode(
			array(
				"company_id" => $data["prosperworks_id"],
				"name" => $data["name"]
			)
		)
	);
	$output = curl_exec($ch);
	$response = json_decode($output, true);
	if (isset($response["id"])) {
		$update = DB::prep("
		UPDATE li_users
		SET prosperworks_people_id = :prosperworks_people_id
		WHERE id = :li_users_id
		");
		$update->execute(array("prosperworks_people_id" => $response["id"], "li_users_id" => $data["li_users_id"]));
	}
	curl_close($ch);
	echo $output;
	

	
}
echo "</table>";

$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/people/search?page_size=200&page_number=6"); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
'X-PW-Application:developer_api',
'X-PW-UserEmail:chris@searchquant.net'
));
curl_setopt($ch, CURLOPT_POSTFIELDS,
	json_encode(
		array()
	)
);

$output = curl_exec($ch);
curl_close($ch);
$persons = json_decode($output, true);
//Total Visited = 93054
$_TOTALVISITED = 93054;
echo "<table border=1>";
foreach ($persons as $person) {
	$query = DB::prep("
	SELECT 
		*,
		company.company_name
	FROM li_users
	JOIN company ON li_users.company_id = company.id
	WHERE prosperworks_people_id = :prosperworks_people_id");
	$query->execute(array("prosperworks_people_id" => $person["id"]));
	if($li_user = $query->fetch()) {
	}
	//print_r($li_user);
	//print_r($person);
	//see if we can update
	foreach ($person["custom_fields"] as $customField) {
		if ($customField["custom_field_definition_id"] == $_TOTALVISITED) {
			$currentTotalVisited = $customField["value"];
		}
	}
	echo "<tr>
		<td>$li_user[company_name]</td>
		<td>$person[company_name]</td>
		<td>$person[name]</td>
		<td>$currentTotalVisited</td>
		<td>$li_user[total_visited]</td>
		<td>$person[id]</td>
		<td>$li_user[id]</td>
		<td>";
		if (isset($person["emails"][0]["email"])) {
			echo $person["emails"][0]["email"];
		}
		
		echo"</td>
	</tr>";
	if ($currentTotalVisited != $li_user["total_visited"]) {

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/people/$person[id]"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
		'X-PW-Application:developer_api',
		'Content-Type: application/json',
		'X-PW-UserEmail:chris@searchquant.net'
		));

		curl_setopt($ch, CURLOPT_POSTFIELDS,
			json_encode(
				array(
					"custom_fields" => array(
						array("custom_field_definition_id" => $_TOTALVISITED, "value" => $li_user["total_visited"])
					)
				)
			)
		);
		/*echo "\n<br><br>\n".json_encode(
				array(
					"custom_fields" => array(
						array("custom_field_definition_id" => $_TOTALVISITED, "value" => $li_user["total_visited"])
					)
				)
			)."\n<br><br>\n";*/
		$response = curl_exec($ch);
		//echo $response;
		curl_close($ch);
	}
	//print_r($currentTotalVisited);
	//die;
}
echo "</table>";
/*
this was code for matching existing prosperworks users with existing li_users. Probably not needed again
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/people/search?page_size=200&page_number=6"); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
'X-PW-Application:developer_api',
'X-PW-UserEmail:chris@searchquant.net'
));
curl_setopt($ch, CURLOPT_POSTFIELDS,
	json_encode(
		array()
	)
);

$output = curl_exec($ch);
$persons = json_decode($output, true);
foreach ($persons as $person) {
	$prosperworksCompanyId = $person["company_id"];
	$query = DB::prep("
	SELECT * 
	FROM company 
	WHERE
	prosperworks_id = :prosperworks_company_id
	ORDER BY id DESC");
	$query->execute(array("prosperworks_company_id" => $prosperworksCompanyId));


	if($company = $query->fetch()) {
		//print_r($company);
		$query = DB::prep("
		UPDATE li_users
		SET prosperworks_people_id = :prosperworks_people_id
		WHERE
		company_id = :company_id AND
		name = :people_name
		ORDER BY id DESC");
		if ($query->execute(array(
			"company_id" => $company["id"],
			"prosperworks_people_id" => $person["id"],
			"people_name" => $person["first_name"] ." ". $person["last_name"]))) {
			echo "Updated ID for ".$person["first_name"] ." ". $person["last_name"]. " at ".$person["company_name"]."<br>";
		} else {
			echo "FAILED TO UPDATE ID for ".$person["first_name"] ." ". $person["last_name"]. " at ".$person["company_name"]."<br>";
		}
	}
}
//echo $output;
curl_close($ch);


die;
echo "<br><br>";
*/





?>
