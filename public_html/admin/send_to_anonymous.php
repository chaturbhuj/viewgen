<?php

include('db.php');

$query = DB::prep("SELECT * FROM company JOIN li_users ON(li_users.company_id = company.id) ORDER BY company_id DESC");
$query->execute();

while ($row = $query->fetch()) {
	$query2 = DB::prep("SELECT 
		people.*,
		campaign.name campaignName,
		people_list.page_nr,
		people_list.url,
		(SELECT inbound_visit.guessed_visit_date FROM inbound_visit WHERE inbound_visit.crawled_by_id = :user_id AND inbound_visit.name = people.name AND substr(inbound_visit.headline, 1, 40) sounds LIKE substr(people.title, 1, 40) ORDER BY guessed_visit_date DESC LIMIT 1) AS guessed_visit_date,
		(SELECT inbound_visit.inbound_visit_id FROM inbound_visit WHERE inbound_visit.crawled_by_id = :user_id AND inbound_visit.name = people.name AND substr(inbound_visit.headline, 1, 40) sounds LIKE substr(people.title, 1, 40) ORDER BY guessed_visit_date DESC LIMIT 1) AS inbound_visit_id
	FROM people 
	JOIN people_list ON people_list.id = people.people_list_id
	JOIN campaign ON campaign.id = people.campaign_id
	WHERE people.name != '' AND
	:company_id = people.company_id AND
	:user_id = people.crawled_by_id
	GROUP BY inbound_visit_id
	HAVING
	guessed_visit_date IS NOT NULL
	ORDER BY inbound_visit_id desc LIMIT 20");
	$query2->execute([
		'company_id' => $row['company_id'],
		'user_id' => $row['linked_in_id']
	]);

	if ($query2->rowCount() == 20) {
		$numViewbacks = 0;
		while ($row2 = $query2->fetch()) {
			if ($row2['inbound_visit_id']) {
				$numViewbacks++;
			}
		}
		if ($numViewbacks < 5) {
			var_dump($row);
		}

		var_dump($numViewbacks);
	}
}

?>
