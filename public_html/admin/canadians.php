<?php

$HTML_TITLE = "Canadians";

include ("../db.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');

function myJsonDecode($jsonText){
	if($jsonObject = json_decode('["'.$jsonText.'"]',true)) {
		return $jsonObject[0];
	} else {
		return $jsonText;
	}
	
}


$dates = array();

$min_date = "2015-01-01";

$max_date = date('Y-m-d');
$active_date = $min_date;
while ($active_date <= $max_date) {
	$dates[] = $active_date;
	$active_date = date('Y-m-d', strtotime($active_date.' + 1 days'));
}
$dates = array_reverse($dates);
$countPerDay = array();
$duplicates = array();
foreach ($dates as $date) {
	$todaysCount = 0;
	$todaysDuplicateCount = 0;
	//echo "Starting with:".$date."\n";
	
	$query = "
	SELECT
		SUBSTRING(linked_in_id,1,26) subId,
		linked_in_id,
		name,
		location,
		title,
		employer,
		industry,
		premium,
		email
	FROM people 
	WHERE sent_to_crawler_date = '$date'

	";
	
	$result = mysql_query($query);

	$lastDate = "";
	while ($data = mysql_fetch_array($result)) {
		if (strpos($data["location"],"Canada") !== false) {
			$isDuplicate = false;
			if (isset($duplicates[$data["subId"]])) {
				$isDuplicate = true;
				//echo "<b>DUPLICATE!!!</b> ";
			} else {
				$duplicates[$data["subId"]] = true;
				
				$workAt = "";
				$atSplit = explode(" at ",myJsonDecode($data["title"]));
				if(count($atSplit) == 2) {
					$workAt = $atSplit[1];
				}
				$currentCompany = "";
				if ($data["employer"] != "") {
					$currentCompany = $data["employer"];
				} else {
					$currentCompany = $workAt;
				}
		
				$linkedInId = html_entity_decode(myJsonDecode($data["linked_in_id"]));
				file_put_contents("canadiansExport.txt", $date."\t".
					html_entity_decode($data["name"])."\t".
					html_entity_decode($data["title"])."\t".
					html_entity_decode($data["industry"])."\t".
					html_entity_decode($data["location"])."\t".
					html_entity_decode($data["email"])."\t".
					html_entity_decode($data["premium"])."\t".
					"https://www.linkedin.com/profile/view?id=".$linkedInId."\n"
				, FILE_APPEND);
			}
			//echo substr($data["linked_in_id"],0,25)." - ".$data["name"]." - ".$data["location"]." - ".$data["sent_to_crawler_date"]."<br>";
			
			if (!$isDuplicate) {
				if (!isset($countPerDay[$date])) {
					$countPerDay[$date] = 0;
				}
				$countPerDay[$date]++;
				$todaysCount++;
			} else {
				$todaysDuplicateCount++;
			}
		}
	}
	echo $date."\t".$todaysCount."\t".$todaysDuplicateCount."\n";
	file_put_contents("canadiansPerDay.txt", $date."\t".$todaysCount."\t".$todaysDuplicateCount."\n", FILE_APPEND);
}

foreach ($countPerDay as $date => $count) {
	//echo $date." - ".$count."<br>\n";
}
//print_r($countPerDay);

?>