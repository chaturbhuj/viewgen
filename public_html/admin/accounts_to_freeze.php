<?php

include('init.php');
require_once ('lib/recurly.php');

if (isset($_POST['action']) && $_POST['action'] == 'freeze') {
	$company_id = (int)$_POST['company_id'];

	$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
	$query->execute([
		'company_id' => $company_id
	]);

	$company = $query->fetch();

	$query = DB::prep("UPDATE company SET frozen = 1 WHERE id = :company_id");
	$query->execute([
		'company_id' => $company_id
	]);

	// Send the email.
	require('classes/EmailTemplate.php');
	$emailTemplate = new EmailTemplate($row['company_id']);

	$variables = [
		'company name' => $company['company_name'],
		'first name' => $company['first_name'],
		'last name' => $company['last_name'],
		'company id' => $company['id'],
		'token' => md5($company['id'] . 'asddsa')
	];

	$emailTemplate->sendEmail($row['email'], "Account Frozen", $variables);

	header("Location: /admin/accounts_to_freeze.php");
	die;
}


if (isset($_GET['action']) && $_GET['action'] == "recalculate") {

	$query = DB::prep("DELETE FROM freeze_cache");
	$query->execute();

	$query = DB::prep("select company.id AS company_id, li_users.name, company_name, company.first_name, company.last_name, company.email, account_code, count(*) cnt from company join li_users on (company.id = li_users.company_id) join rc_account on (company.id = rc_account.company_id) where li_users.hidden = 0 and account_type = 'paid' and company.email not like '%NotInUse%' group by company.id");
	$query->execute();

	$companies = [];
	while ($row = $query->fetch()) {

		try {
			$subscriptions = Recurly_SubscriptionList::getForAccount($row['account_code']);
			$invoices = Recurly_InvoiceList::getForAccount($row['account_code']);
			foreach ($invoices as $invoice) {break;}

			$invoiceDescription = "";
			foreach ($invoice->line_items as $item) {
				$invoiceDescription = $item->description . ' (' . $item->total_in_cents/100 . ' ' . $item->currency . ') ';
			}

			$has_active = false;
			$has_sub = false;
			$subscription = false;
			foreach ($subscriptions as $subscription) {
				$has_sub = true;
				if (in_array($subscription->state, ['active', 'in_trial'])) {
					// This is a real paying customer.
					$has_active = true;
					break;
				}
			}

			if (!$has_active || ($subscription && $subscription->unit_amount_in_cents == 0)) {

				$query2 = DB::prep("SELECT MAX(last_visit) AS last_visit FROM li_users WHERE company_id = :company_id");
				$query2->execute([
					'company_id' => $row['company_id']
				]);
				$row2 = $query2->fetch();
				if (strtotime($row2['last_visit']) >= time() - 10*3600*24) {
					// Active last 10 days. We should freeze this bugger.
					$insert = DB::prep("INSERT INTO freeze_cache (company_id, is_zero_account, invoice_date, invoice_title) VALUES(:company_id, :is_zero_account, :invoice_date, :invoice_title)");
					$insert->execute([
						'company_id' => $row['company_id'],
						'is_zero_account' => ($has_sub && $subscription->unit_amount_in_cents == 0) ? 1 : 0,
						'invoice_date' => $invoice->created_at->format("M j, Y"),
						'invoice_title' => $invoiceDescription
					]);
				}
			}

		} catch (Recurly_NotFoundError $error) {
			var_dump($error->getMessage());
		}
	}

	header("Location: /admin/accounts_to_freeze.php");
	die;
}

$query = DB::prep("SELECT * FROM company JOIN freeze_cache ON (company_id = id) WHERE company_name != 'ivanh'");
$query->execute();

$companies = $query->fetchAll();

?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<br/>
			<div class="bordered-box">
				<table class="table bordered-table">
					<thead>
						<tr>
							<th>Account #</th>
							<th>Company Name</th>
							<th>Email</th>
							<th></th>
							<th></th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<? foreach ($companies as $company) { ?>
						<tr class="<?=$company['frozen'] ? ' is_frozen' : ''?> <?=$company['is_zero_account'] ? ' is_zero_account' : ''?>">
							<td><?=$company['company_id']?></td>
							<td><?=$company['company_name']?></td>
							<td><a href="/billing.php?company_id=<?=$company['id']?>" target="_blank"><u><?=$company['email']?></u></a></td>
							<td><?=$company['invoice_date']?></td>
							<td><?=$company['invoice_title']?></td>
							<td>
								<form method="post" action="/admin/accounts_to_freeze.php">
									<input type="hidden" name="action" value="freeze" />
									<input type="hidden" name="company_id" value="<?=$company['company_id']?>" />
									<button type="submit" class="btn btn-default">Freeze and send email</button>
								</form>
							</td>
						</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
