<?php

function updateCompanyInProsper($company_id, $data) {

	$query = DB::prep("SELECT * FROM company WHERE id = :company_id");
	$query->execute([
		'company_id' => $company_id
	]);

	if ($query->rowCount() == 0) return;
	
	$company = $query->fetch();

	if ($company['prosperworks_id'] == 0) {
		// Create this one.

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/companies"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
		'X-PW-Application:developer_api',
		'Content-Type: application/json',
		'X-PW-UserEmail:chris@searchquant.net'
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS,
			json_encode(
				array(
				"name" => $company["company_name"],
				"phone_numbers" => array(array("number" => $company["phone"], "category" => "work")),
				"emails" => array(array("email" => $company["email"], "category" => "work")),
				"date_created" => strtotime($company["company_created"])
				)
			)
		);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		if (isset($response["id"])) {
			$update = DB::prep("UPDATE company SET prosperworks_id = ? WHERE id = ?");
			$update->execute(array($response["id"], $company["id"]));
			
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/people"); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
			'X-PW-Application:developer_api',
			'Content-Type: application/json',
			'X-PW-UserEmail:chris@searchquant.net'
			));
			curl_setopt($ch, CURLOPT_POSTFIELDS,
				json_encode(
					array(
						"company_id" => $response["id"],
						"name" => $company["first_name"]." ".$company["last_name"],
						"emails" => array(array("email" => $company["email"], "category" => "work")),
						"phone_numbers" => array(array("number" => $company["phone"], "category" => "work"))
					)
				)
			);
			$userResponse = curl_exec($ch);
			curl_close($ch);
		}
	} else {
	//	var_dump("asd");
	}


	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/companies/$company[prosperworks_id]"); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
	'X-PW-Application:developer_api',
	'Content-Type: application/json',
	'X-PW-UserEmail:chris@searchquant.net'
	));
	//91860 = subscription
	//92602 = User Count
	//93011 = ID

	$custom_fields = [];
	foreach ($data as $id => $value) {
		if ($value == "") continue;
		$custom_fields[] = ['custom_field_definition_id' => $id, "value" => $value];
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		json_encode(
			array(
				"custom_fields" => $custom_fields
			)
		)
	);
	$response = curl_exec($ch);
	curl_close($ch);
	if (strpos($response, "Invalid argument") !== false) {
		var_dump("Invalid argument: " . $company_id);
		return false;
		//var_dump($company['id']);die;
	}
	//echo $response;
	return true;
}

?>
