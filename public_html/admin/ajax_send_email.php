<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

chdir('..');
include('authenticate.php');
include('classes/Email.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$response = [
];

if ($_POST['type'] == 'emailLISub') {
	$emailTo = $_POST['emailTo'];
	$accountType = $_POST['accountType'];
	$company_id = $_POST['company_id'];

$emailSubject = "Tip, increase your outbound visits.";
$emailBody = "Hello,<br/>
We have detected that your LinkedIn account only has the <b>$accountType</b> subscription level. To be able to visit a lot of people you need the \"Sales Navigator\", \"Business Plus\" or \"Executive\" subscription. You can read about LI's Commercial Use Limit <a href=\"https://help.linkedin.com/app/answers/detail/a_id/52950/~/commercial-use-limit-on-search/help.linkedin.com/app/answers/detail\">here</a>; note that it was introduced January 2015, and changed somewhat in May 2015.<br/>
<br/>
Kind regards,<br/><br/>
";


	$email = new Email();
	$email->setSubject($emailSubject);
	//$email->addTo($emailTo);
	$email->addReplyTo("support@connectionsmaker.net");

	$email->setHtmlBody($emailBody);
	$email->send();

	$query = DB::prep("INSERT INTO email_log (email, subject, body, company_id, email_type) VALUES(?, ?, ?, ?, ?)");
	$query->execute([
		$emailTo,
		$emailSubject,
		$emailBody,
		$company_id,
		$_POST['type']
	]);
}

if ($_POST['type'] == 'emailLISub2') {
	//this was an email saying that sales navigator could not be used, but we support it now
}

if ($_POST['type'] == 'emailLISub3') {
	$emailTo = $_POST['emailTo'];
	$accountType = $_POST['accountType'];
	$company_id = $_POST['company_id'];

	$name = $_POST['name'];
	$name = explode(" ", $name);
	$name = $name[0];

$emailSubject = "Tip, increase your outbound visits";
$emailBody = "Hi $name, <br/>
<br/>
We have detected that you have the Job Seeker LinkedIn subscription. Job Seeker limits the number of searches you can make during one month. Therefore we suggest that unless you’re getting irreplaceable value from Job Seeker, you contact LinkedIn to get them to change your subscription to Sales Navigator<br/>
<br/>
Email me if you have any questions, OK? <br/>
<br/>

<br/>
Cheers, <br/>
<br/>
";


	$email = new Email();
	$email->setSubject($emailSubject);
	//$email->addTo($emailTo);
	$email->addReplyTo("support@connectionsmaker.net");

	$email->setHtmlBody($emailBody);
	$email->send();

	$query = DB::prep("INSERT INTO email_log (email, subject, body, company_id, email_type) VALUES(?, ?, ?, ?, ?)");
	$query->execute([
		$emailTo,
		$emailSubject,
		$emailBody,
		$company_id,
		$_POST['type']
	]);
}

if ($_POST['type'] == 'emailCanceled') {
	$emailTo = $_POST['emailTo'];
	$accountType = $_POST['accountType'];
	$company_id = $_POST['company_id'];

	$name = $_POST['name'];
	$name = explode(" ", $name);
	$name = $name[0];

$emailSubject = "Extended trial";
$emailBody = "Hi $name, <br/>
<br/>
We noticed that you have Cancelled your free trial subscription account.  If you want to, <b>we can extend your trial by 10 days</b> so that we can together have more time to get you to the ROI we will bring if you a) visit large volumes of well-targeted profiles; and b) optimize your LinkedIn profile so that the awareness this scaled profile visiting generates turns into interest and inbound leads. Just reply to this email saying ‘<b>Yes</b>’ and we will set that up for you.<br/>
<br/>
Cheers, <br/>
<br/>

";


	$email = new Email();
	$email->setSubject($emailSubject);
	$email->addTo($emailTo);
	$email->addReplyTo("support@connectionsmaker.net");

	$email->setHtmlBody($emailBody);
	$email->send();

	$query = DB::prep("INSERT INTO email_log (email, subject, body, company_id, email_type) VALUES(?, ?, ?, ?, ?)");
	$query->execute([
		$emailTo,
		$emailSubject,
		$emailBody,
		$company_id,
		$_POST['type']
	]);
}



echo json_encode($response);

?>
