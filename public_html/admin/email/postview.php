<?php

$STOPMENU = true;
$STOPHEAD = true;

include('../init.php');

if (!isset($_GET['emailId'])) {
	echo "Email id is needed";
	die;
}

$query = DB::prep("SELECT * FROM email WHERE emailId = :emailId");
$query->execute([
	'emailId' => $_GET['emailId']
]);
$email = $query->fetch();

?>
<pre>
SUBJECT: <?=$email['emailSubject']?>

RECIPIENT: <?=$email['recipient']?>

DATE SENT: <?=$email['emailCreated']?>

BODY:</pre><?=$email['emailBody']?>
