<?php

include('../init.php');

$query = DB::prep("SELECT * FROM email JOIN emailTemplate USING(emailTemplateId) ORDER BY emailCreated ASC");
$query->execute();

$emailLog = $query->fetchALl();

?>
<div class="container admin-email">
	<div class="row">
		<div class="col-xs-12">
			<a href="index.php" class="btn btn-default">Back</a>
			<br/>
			<br/>
			<div class="bordered-box">
				<table class="bordered-table">
					<thead>
						<tr>
							<th>Recipient</th>
							<th>Sent</th>
							<th>Subject</th>
							<th>Message</th>
							<th>Template</th>
						</tr>
					</thead>
					<tbody>
						<? foreach ($emailLog as $logItem) { ?>
							<tr>
								<td><?=$logItem['recipient']?></td>
								<td><?=$logItem['emailCreated']?></td>
								<td><?=$logItem['emailSubject']?></td>
								<td>
									<?=strip_tags(substr($logItem['emailBody'], 0, 600)) ?> ...
									<a href="postview.php?emailId=<?=$logItem['emailId']?>" target="_blank">
										<u>View</u>
									</a>
								</td>
								<td><?=$logItem['emailTemplateName']?></td>
							</tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
