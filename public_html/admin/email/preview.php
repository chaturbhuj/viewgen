<?php

$STOPMENU = true;
$STOPHEAD = true;

include('../init.php');
include('classes/EmailTemplate.php');
include('classes/EmailTargets.php');

if (!isset($_GET['emailTemplateId'])) {
	echo "Template id is needed";
	die;
}

if (!isset($_GET['id'])) {
	echo "id is needed";
	die;
}

$query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateStatus != 'deleted' AND
	emailTemplateId = :emailTemplateId");
$query->execute([
	'emailTemplateId' => $_GET['emailTemplateId']
]);
$emailTemplate = $query->fetch();

$emailTemplate['availableVariables'] = json_decode($emailTemplate['availableVariables']);
if ($emailTemplate['availableVariables'] === null) {
	$emailTemplate['availableVariables'] = [];
}

$target = EmailTargets::findEmailTarget($emailTemplate['emailTemplateName'], $_GET['id']);

if ($target === null) {
	echo "Could not find preview";
	die;
}

$emailSubject = substituteEmailVariables($emailTemplate['emailTemplateSubject'], $target->getVariables());
$emailBody = substituteEmailVariables($emailTemplate['emailTemplateBody'], $target->getVariables());

?>
<pre>
SUBJECT: <?=$emailSubject?>

RECIPIENT: <?=$target->getEmail()?>

BODY:</pre><?=$emailBody?>
