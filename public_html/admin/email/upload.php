<?php

$STOPMENU = true;
$STOPHEAD = true;

include('../init.php');

if (isset($_FILES["image"])) {
	$imageSize = getimagesize($_FILES["image"]["tmp_name"]);
	if ($imageSize === false) {
		echo 1;
		die;
	}

	if (!preg_match("#^.*\.([a-zA-Z0-9]+)$#", $_FILES['image']['name'], $match)) {
		echo 2;
		die;
	}
	$extension = $match[1];

	$fileName = md5(rand()) . "." . $extension;
	$targetFile = dirname(__FILE__) . "/uploads/" . $fileName;
	if (move_uploaded_file($_FILES['image']['tmp_name'], $targetFile)) {
		echo "/admin/email/uploads/" . $fileName;
	} else {
		echo 3;
		die;
	}
}

?>
