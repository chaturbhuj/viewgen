<?php

include('../init.php');
include('classes/EmailTemplate.php');
include('classes/EmailTargets.php');

$query = DB::prep("SELECT * FROM emailTemplate WHERE emailTemplateStatus != 'deleted' ORDER BY emailTemplateOrder ASC");
$query->execute();
$availableEmailTemplates = $query->fetchAll();

if (count($availableEmailTemplates) == 0) {
	echo "No templates";
	die;
}

$emailTemplate = $availableEmailTemplates[0];
if (isset($_GET['emailTemplateId'])) {
	foreach ($availableEmailTemplates as $_template) {
		if ($_template['emailTemplateId'] == $_GET['emailTemplateId']) {
			$emailTemplate = $_template;
		}
	}
}

$emailTemplate['availableVariables'] = json_decode($emailTemplate['availableVariables']);
if ($emailTemplate['availableVariables'] === null) {
	$emailTemplate['availableVariables'] = [];
}

if (isset($_POST['action']) && $_POST['action'] == 'sendPreview') {
	$template = new EmailTemplate(291);
	$dummyVariables = [
		'first name' => 'John',
		'last name' => 'Doe',
		'company name' => 'Example Company Name',
		'campaign name' => 'Example Campaign Name',
		'num profiles visited' => 123,
		'num inbound views' => 45,
		'view back rate' => '36%',
		'linkedin first name' => 'John',
		'linkedin last name' => 'Doe',
		'campaign link' => sprintf('<a href="%1$s">%1$s</a>', 'http://viewgentool.com//visitedProfiles.php?campaign_id=35568&company_id=291'),
		'company id' => 291,
		'token' => md5('62asddsa')
	];
	$template->sendEmail($_POST['previewEmail'], $emailTemplate['emailTemplateName'], $dummyVariables, true);

	redirect('admin/email/?emailTemplateId=' . $emailTemplate['emailTemplateId']);
}

if (isset($_POST['action']) && $_POST['action'] == 'save') {

	$query = DB::prep("UPDATE emailTemplate SET
		emailTemplateStatus = :emailTemplateStatus,
		emailTemplateSubject = :emailTemplateSubject,
		emailTemplateBody = :emailTemplateBody
		WHERE
			emailTemplateId = :emailTemplateId");
	$query->execute(array(
		'emailTemplateStatus' => $_POST['emailTemplateStatus'],
		'emailTemplateSubject' => $_POST['emailTemplateSubject'],
		'emailTemplateBody' => $_POST['emailTemplateBody'],
		'emailTemplateId' => $emailTemplate['emailTemplateId']
	));

	redirect('admin/email/?emailTemplateId=' . $emailTemplate['emailTemplateId']);
}

$targets = EmailTargets::getTargets($emailTemplate['emailTemplateName']);

?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector:'textarea.email-body',
		plugins: "textcolor link code image table",
		toolbar: "undo redo | styleselect | bold italic link | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect | forecolor backcolor image table | fontselect",
		fontsize_formats: "8px 9px 10px 12px 14px 18px 24px 36px",
		file_browser_callback: function(field_name, url, type, win) {
			$('#image_upload input').data('field_name', field_name);
			if(type=='image') $('#image_upload input').click();
		},
		relative_urls: false,
		remove_script_host: false
	});

	$(function () {
		$('#image_upload input').change(function () {
			var fdata = new FormData(document.getElementById('image_upload'));
			var field_name = $(this).data('field_name');
			$.ajax({
				url: "/admin/email/upload.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: fdata, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(imagePath)   // A function to be called if request succeeds
				{
					$('#' + field_name).val(document.location.protocol + "//" + document.location.hostname + imagePath);
				}
			});
		});
	});
	
</script>
<div class="container admin-email">
	<div class="row">
		<div class="col-xs-3">
			<div class="bordered-box">
				<ul>
					<? foreach ($availableEmailTemplates as $_template) { ?>
						<li>
							<a href="?emailTemplateId=<?=$_template['emailTemplateId']?>">
								<? if ($_template['emailTemplateStatus'] == "inactive") { ?>
									<span class="inactive-template"><?=$_template['emailTemplateName']?>
										<small><small>(<?=$_template['numberOfSentEmails']?>)</small></small></span>
								<? } else { ?>
									<span class="active-template"><?=$_template['emailTemplateName']?>
										<small><small>(<?=$_template['numberOfSentEmails']?>)</small></small></span>
								<? } ?>
							</a>
						</li>
					<? } ?>
				</ul>
			</div>
			<br/>
			<a href="log.php" class="btn btn-default">View Log</a>
		</div>
		<div class="col-xs-9">
			<div class="bordered-box">
				<form method="post">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label>Subject</label>
								<input type="text" class="form-control" name="emailTemplateSubject"
									value="<?=htmlspecialchars($emailTemplate['emailTemplateSubject'])?>">
							</div>
							<div class="form-group">
								<textarea class="form-control email-body" name="emailTemplateBody" rows="15"><?=
									htmlspecialchars($emailTemplate['emailTemplateBody'])?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<ul>
								<? if (count($emailTemplate['availableVariables']) == 0) { ?>
									<li><b class="text-warning">No variables available</b></li>
								<? } else { ?>
									<li><b>Available variables:</b></li>
									<? foreach ($emailTemplate['availableVariables'] as $variable) { ?>
										<li>{<?=$variable?>}</li>
									<? } ?>
								<? } ?>
							</ul>
						</div>
						<div class="col-xs-2">
							<select class="form-control" name="emailTemplateStatus">
								<option value="inactive"<?=$emailTemplate['emailTemplateStatus'] == 'inactive' ?
									' selected="selected"' : ''?>>Inactive</option>
								<option value="active"<?=$emailTemplate['emailTemplateStatus'] == 'active' ?
									' selected="selected"' : ''?>>Active</option>
							</select>
						</div>
						<div class="col-xs-4 text-right">
							<?/*
							<a class="btn btn-default" target="_blank"
								href="preview.php?emailTemplateId=<?=$emailTemplate['emailTemplateId']?>&all=all">
								Preview All
							</a>
							<a class="btn btn-default" target="_blank"
								href="preview.php?emailTemplateId=<?=$emailTemplate['emailTemplateId']?>">
								Preview
							</a>*/?>
							<button class="btn btn-success" type="submit" name="action" value="save">Save</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4 col-xs-offset-8 text-right">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Send preview to..."
									name="previewEmail">
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit" name="action" value="sendPreview">
										Send!
									</button>
								</span>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<? if (count($targets) > 0) { ?>
			<div class="col-xs-9 col-xs-offset-3">
				<br/>
				<div class="bordered-box">
					<b>Targets</b> (<?=count($targets)?>)
					<table class="table">
						<tbody>
							<? foreach ($targets as $target) { ?>
								<tr>
									<td><?=$target->getEmail()?></td>
									<td><a href="preview.php?emailTemplateId=<?=$_GET['emailTemplateId']?>&id=<?=$target->getId();?>" target="_blank"><u>Preview</u></a></td>
								</tr>
							<? } ?>
						</tbody>
					</table>
				</div>
			</div>
		<? } ?>
	</div>
</div>

<form id="image_upload" action="/admin/email/upload.php" target="form_target" method="post"
	enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
	<input name="image" type="file">
</form>
