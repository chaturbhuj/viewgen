<?php

chdir(dirname(__FILE__) . "/..");

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('header.php');
require_once("shared_functions.php");
require_once('classes/Email.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

?>
