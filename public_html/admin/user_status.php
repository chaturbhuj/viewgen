<?php

$HTML_TITLE = "User Status";
require_once ('../lib/recurly.php');
require_once('../header.php');

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

error_reporting(E_ALL);
ini_set('display_errors', '1');

$orderDirection = "desc";
if (isset($_GET["direction"]) and $_GET["direction"] == "asc") {
	$orderDirection = "asc";
}
$sortBy = "li_users.first_visit";
if (isset($_GET["sort_by"])) {
	if ($_GET["sort_by"] == "Company") {
		$sortBy = "company.company_name";
	} else if ($_GET["sort_by"] == "User") {
		$sortBy = "li_users.name";
	} else if ($_GET["sort_by"] == "Subscriptions") {
		$sortBy = "li_users.Subscriptions";
	} else if ($_GET["sort_by"] == "Anonymous") {
		$sortBy = "li_users.anonymous";
	} else if ($_GET["sort_by"] == "Last Anonymous Check") {
		$sortBy = "li_users.lastAnonymusCheck";
	} else if ($_GET["sort_by"] == "Act") {
		$sortBy = "active_campaign_count";
	} else if ($_GET["sort_by"] == "Pau") {
		$sortBy = "paused_campaign_count";
	} else if ($_GET["sort_by"] == "Arc") {
		$sortBy = "archived_campaign_count";
	} else if ($_GET["sort_by"] == "Fin") {
		$sortBy = "finished_campaign_count";
	} else if ($_GET["sort_by"] == "Total Visits") {
		$sortBy = "li_users.total_visited";
	} else if ($_GET["sort_by"] == "First Visit") {
		$sortBy = "li_users.first_visit";
	} else if ($_GET["sort_by"] == "Last Visit") {
		$sortBy = "li_users.last_visit";
	} else if ($_GET["sort_by"] == "Total Visitors") {
		$sortBy = "li_users.visitors_during_visiting";
	} else if ($_GET["sort_by"] == "Days Visiting") {
		$sortBy = "li_users.total_days_visiting";
	} else if ($_GET["sort_by"] == "Avg. Views Before SQ") {
		$sortBy = "li_users.avg_views_before_sq";
	} else if ($_GET["sort_by"] == "Avg. Views With SQ") {
		$sortBy = "li_users.avg_views_with_sq";
	} else if ($_GET["sort_by"] == "VBR") {
		$sortBy = "li_users.vbr_during_visiting";
	} else if ($_GET["sort_by"] == "Phone") {
		$sortBy = "company.phone";
	}
}
$query = "SELECT 
company.subscription_plan_code,
company.email,
company_name,
company.phone,
ssi.ssi_html,
li_users.name username,
company_created,
linked_in_id,
company.id company_id,
people_count,
anonymous,
lastAnonymusCheck,
subscriptions,
active_campaign_count,
paused_campaign_count,
archived_campaign_count,
finished_campaign_count,
li_users.total_visited as total_visited,
li_users.first_visit,
li_users.last_visit,
li_users.visitors_during_visiting,
li_users.total_days_visiting,
avg_views_before_sq,
avg_views_with_sq,
vbr_during_visiting
FROM company
LEFT JOIN li_users ON li_users.company_id = company.id
LEFT JOIN ssi ON (ssi.crawled_by_id = li_users.linked_in_id AND ssi.company_id = li_users.company_id)
WHERE 
	company.state != 'draft' AND 
	company.state != 'draft KILLED' AND
	company.company_name NOT LIKE '%NotInUse' AND
	company.frozen = 0 AND
	li_users.hidden = 0
ORDER BY $sortBy $orderDirection
";

$result = mysql_query($query);
/*
<?=($_GET["order_by"] == "title" and $_GET["direction"] == "asc")?"&#x25B2;":""?>
				<?=($_GET["order_by"] == "title" and $_GET["direction"] == "desc")?"&#x25BC;":""?>
*/
if (!isset($_GET["sort_by"])) {
	$_GET["sort_by"] = "";
}
if (!isset($_GET["direction"])) {
	$_GET["direction"] = "";
}
$columnHeaders = array();
$columnHeaders[] = "Company";
$columnHeaders[] = "Phone";
$columnHeaders[] = "User";
$columnHeaders[] = "Subscriptions";
$columnHeaders[] = "Anonymous";
$columnHeaders[] = "Last Anonymous Check";
$columnHeaders[] = "Act";
$columnHeaders[] = "Pau";
$columnHeaders[] = "Fin";
$columnHeaders[] = "Arc";
$columnHeaders[] = "Total Visits";
$columnHeaders[] = "Total Visitors";
$columnHeaders[] = "First Visit";
$columnHeaders[] = "Last Visit";
$columnHeaders[] = "Days Visiting";
//$columnHeaders[] = "Avg. Views Before SQ";
//$columnHeaders[] = "Avg. Views With SQ";
$columnHeaders[] = "VBR";
//$columnHeaders[] = "SSI";

echo "
<h1>User Status</h1>
<table class='table'>
	<tr>
		<td colspan=5></td>
		<td colspan=4 align=center>#Campaigns</td>
		<td colspan=10>
	</tr>
	<tr>";
	
	foreach ($columnHeaders as $columnHeader) {
		if ($columnHeader == "SSI") {
			echo "<th>SSI</th>";
		} else {
			echo "<th>";
			echo "<a href='/admin/user_status.php?sort_by=$columnHeader&";
			if ($columnHeader == $_GET["sort_by"] and $_GET["direction"] != "asc") {
				echo "direction=asc";
			} else {
				echo "direction=desc";
			}
			echo "'>";
			echo "$columnHeader";
			if ($_GET["sort_by"] == $columnHeader) {
				if ($_GET["direction"] == "desc") {
					echo "&#x25BC;";
				} else {
					echo "&#x25B2;";
				}
			}
			echo "</a></th>";
		}
	}
		/*<th><a href=$baseUrl&order_by=company>Company</a></th>
		<th>User</th>
		<th>Subscriptions</th>
		<th>Anonymous</th>
		<th>Last Anonymous Check</th>*/
echo"	</tr>
";
while($data = mysql_fetch_array($result)){
	echo "<tr>
	<td><a href=/dashboard.php?company_id=$data[company_id]><u>$data[company_name]</u></a></td>
	<td>$data[phone]</td>
	<td>$data[username]</td>
	<td>$data[subscriptions]</td>
	<td>";
	if ($data["anonymous"] == "Private mode") {
		echo "<font color=darkred><b>$data[anonymous]</b></font>";
	} else if ($data["anonymous"] == "Characteristics") {
		echo "<font color=darkorange><b>$data[anonymous]</b></font>";
	} else {
		echo $data["anonymous"];
	}
	echo "</td>
	<td><a href=view_html.php?linked_in_id=$data[linked_in_id]&company_id=$data[company_id]>";
	if ($data["lastAnonymusCheck"] != "0000-00-00 00:00:00") {
		echo "<u>".date("M d Y", strtotime($data["lastAnonymusCheck"]))."</u>";
	}
	echo "</a></td>
	<td>$data[active_campaign_count]</td>
	<td>$data[paused_campaign_count]</td>
	<td>$data[finished_campaign_count]</td>
	<td>$data[archived_campaign_count]</td>
	<td>$data[total_visited]</td>
	<td>$data[visitors_during_visiting]</td>
	<td>";
	if ($data["first_visit"] != "0000-00-00") {
		echo date("M d Y", strtotime($data["first_visit"]));
	}
	echo "</td>
	<td>";
	if ($data["last_visit"] != "0000-00-00") {
		echo date("M d Y", strtotime($data["last_visit"]));
	}
	echo "</td>
	<td>$data[total_days_visiting]</td>
	
	<td>";
	if ($data["total_visited"] < 2000) {
		echo "Too few visits";
	} else {
		echo $data["vbr_during_visiting"]."%";
	}
	
	$overall_score = "";
	$rows = explode("\n",$data["ssi_html"]);
	foreach ($rows as $row) {
		if(strpos($row,'"mScore":{"overall":') !== false){
			$cells = explode('"mScore":{"overall":',$row);
			$p = explode(',',$cells[1]);
			$overall_score = $p[0];
		}
	}
	
	echo "</td>
	</tr>";

}
echo "</table>";

?>