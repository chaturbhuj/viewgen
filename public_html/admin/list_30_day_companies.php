<?php

$HTML_TITLE = "List Companies";
require_once('../header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

$query = DB::prep("SELECT * FROM company WHERE DATE(company_created) > DATE_SUB(NOW(), INTERVAL 30 DAY)");
$query->execute();
$companies = $query->fetchAll();

?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Company Name</th>
					</tr>
				</thead>
				<tbody>
					<? foreach ($companies as $company) { ?>
						<tr>
							<td><?=$company['company_name']?></td>
						</tr>
					<? } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?
require_once('footer.php');

?>
