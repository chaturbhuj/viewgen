<?php
chdir(dirname(__FILE__) . "/..");

error_reporting(E_ALL);
ini_set('display_errors', 1);

include('db.php');

require_once ('lib/recurly.php');
require_once ('classes/mailchimp/Mailchimp.php');

$mailChimp = new MailChimp("4ebb3ecef8c8440befdf8860f534292d-us8");

$lists = [
	'Paying Accounts With 1 User' => '468f3dd67f',
	'Paying Accounts With 2 Users' => 'aad57c59e2',
	'Canceled Active Accounts (Active Last 10 Days)' => '025043add9',
	'Canceled Inactive Accounts (Not Active Last 10 Days)' => 'eaa2d45a78',
	'Users In The Abort Page' => 'f9315eb5fa'
];

$usersInLists = [
	'Paying Accounts With 1 User' => [],
	'Paying Accounts With 2 Users' => [],
	'Canceled Active Accounts (Active Last 10 Days)' => [],
	'Canceled Inactive Accounts (Not Active Last 10 Days)' => [],
	'Users In The Abort Page' => []
];

$query = DB::prep("select company.id AS company_id, li_users.name, company_name, company.first_name, company.last_name, company.email, account_code, count(*) cnt from company join li_users on (company.id = li_users.company_id) join rc_account on (company.id = rc_account.company_id) where li_users.hidden = 0 and frozen = 0 and account_type = 'paid' and company.email not like '%NotInUse%' group by company.id");
$query->execute();

while ($row = $query->fetch()) {

	try {
		$subscriptions = Recurly_SubscriptionList::getForAccount($row['account_code']);
		$has_active = false;
		foreach ($subscriptions as $subscription) {
			if (in_array($subscription->state, ['active', 'in_trial']) && $subscription->unit_amount_in_cents > 0) {
				// This is a real paying customer.
				$has_active = true;
			}
		}

		if ($has_active) {
			if ($row['cnt'] == 1) {
				$listName = 'Paying Accounts With 1 User';
			}
			if ($row['cnt'] == 2) {
				$listName = 'Paying Accounts With 2 Users';
			}
		} else {

			$query2 = DB::prep("SELECT MAX(last_visit) AS last_visit FROM li_users WHERE company_id = :company_id");
			$query2->execute([
				'company_id' => $row['company_id']
			]);
			$row2 = $query2->fetch();
			if (strtotime($row2['last_visit']) >= time() - 10*3600*24) {
				// Active last 10 days.
				$listName = "Canceled Active Accounts (Active Last 10 Days)";
			} else {
				$listName = "Canceled Inactive Accounts (Not Active Last 10 Days)";
			}
		}

		$usersInLists[$listName][] = [
			'email' => $row['email'],
			'company_name' => $row['company_name'],
			'company_id' => $row['company_id'],
			'first_name' => $row['first_name'],
			'last_name' => $row['last_name']
		];

	} catch (Recurly_NotFoundError $error) {
		var_dump($error->getMessage());
	}
}

$query = DB::prep("SELECT * FROM company 
	WHERE 
		state = 'draft' AND
		company_name NOT LIKE '%NotInUse' AND
		id > 310
	ORDER BY id DESC");
$query->execute();

while ($row = $query->fetch()) {
	// Check if it is in another list.
	$inAnotherList = false;
	foreach ($usersInLists as $listName => $users) {
		if ($listName == "Users In The Abort Page") continue;
		foreach ($users as $user) {
			if ($user['email'] == $row['email']) $inAnotherList = true;
		}
	}
	if ($inAnotherList) {
		continue;
	}
	$usersInLists['Users In The Abort Page'][] = [
		'email' => $row['email'],
		'company_name' => $row['company_name'],
		'company_id' => $row['id'],
		'first_name' => $row['first_name'],
		'last_name' => $row['last_name']
	];
}

foreach ($usersInLists as $listName => $users) {
	if ($listName != 'Users In The Abort Page') continue;
	//if ($listName != 'Paying Accounts With 1 User') continue;
	foreach ($users as $user) {
		try {
			var_dump(	$mailChimp->lists->subscribe($lists[$listName], array('email' => $user['email']), [
				'FNAME' => $user['first_name'],
				'LNAME' => $user['last_name'],
				'COMPANY_NAME' => $user['company_name'],
				'COMPANY_ID' => $user['company_id'],
				"MMERGE3" => "Customer"
			], 'html', false, true));
		} catch (Exception $error) {
			
		}
	}
}

?>
