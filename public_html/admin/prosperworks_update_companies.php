<?php
$HTML_TITLE = "List Companies";
require_once ('../lib/recurly.php');


if(isset($_POST["delete_company"]) AND $_USER_DATA["admin"]){
	$query = "UPDATE company SET company_name = CONCAT(company_name,' is deleted',NOW()), deleted = 1 WHERE id = '".mysql_real_escape_string($_POST["delete_company"])."'";
	if(mysql_query($query)){
		echo "<h1>Company Was Deleted</h1>";
	}
}


if(isset($_GET["track"]) AND $_USER_DATA["admin"]){
	$query = "UPDATE company SET track = 1 WHERE id = '".mysql_real_escape_string($_GET["track"])."'";
	if(mysql_query($query)){
		echo "<h1>Company Is Being Tracked</h1>";
	}
}

if(isset($_GET["freeze"]) AND $_USER_DATA["admin"]){
	$query = "UPDATE company SET frozen = 1 WHERE id = '".mysql_real_escape_string($_GET["freeze"])."'";
	if(mysql_query($query)){
		echo "<h1>Company Is Frozen</h1>";
	}
}

if(isset($_GET["unfreeze"]) AND $_USER_DATA["admin"]){
	$query = "UPDATE company SET frozen = 0 WHERE id = '".mysql_real_escape_string($_GET["unfreeze"])."'";
	if(mysql_query($query)){
		echo "<h1>Company Is Unfrozen</h1>";
	}
}


if(isset($_POST["company_name"])){
	$assignedTo = 0;
	if(!$_USER_DATA["admin"]){
		$assignedTo = $_USER_DATA["id"];
	}
	$query = "INSERT INTO company (company_name,company_password,assignedTo) VALUES ('".mysql_real_escape_string($_POST["company_name"])."','".mysql_real_escape_string(substr(md5($_POST["company_name"]."fewwf few"),0,8))."','$assignedTo')";
	if(mysql_query($query)){
	
	}else{
		echo "<h1>Could not add company. ".mysql_error()."<h2>";
	}
}
if($_USER_DATA["admin"] AND isset($_GET["assignCompany"]) AND isset($_GET["assignTo"])){
	$query = "UPDATE company SET assignedTo = '".mysql_real_escape_string($_GET["assignTo"])."' WHERE id = '".mysql_real_escape_string($_GET["assignCompany"])."'";
	mysql_query($query);
}
echo "<center>";

error_reporting(E_ALL);
ini_set('display_errors', '1');
$uuIdToSubscription = array();
$emailToSubscription = array();
$subscriptions = Recurly_SubscriptionList::getActive(['per_page' => 200]);
echo $subscriptions->count()." active subscriptions";
$count = 0;
foreach ($subscriptions as $i => $subscription) {
	//print_r($subscription->account);
	//die;
	$uuIdToSubscription[$subscription->uuid] = $subscription;
	$p = explode("/",$subscription->account->getHref());
	
	$emailToSubscription[$p[count($p)-1]][] = $subscription;
	$count++;
	if ($count > 190) {
		//break;
	}
}


//die("Kommer aldrig hit");
/*$query = "select company_id, count(*) cnt from people where sent_to_crawler_date > '2015-05-14' and crawled_by_id != 0 group by company_id;";
$result = mysql_query($query);
$visitsLast7Days = array();
while($data = mysql_fetch_array($result)){
	$visitsLast7Days[$data["company_id"]] = $data["cnt"];
	
}*/
$getFilter ="";
if (isset($_GET["show_frozen"])) {
	$getFilter = "&show_frozen=true";
}


echo "</div><table width=700><tr><td width=400><center>";

echo "Sort by Date Added: <a href=list_companies.php?sortBy=added$getFilter><u>Oldest</u></a> <a href=list_companies.php?sortBy=added&direction=desc$getFilter><u>Newest</u></a><br>";
echo "Sort by Company Name: <a href=list_companies.php?sortBy=name$getFilter><u>A-Z</u></a> <a href=list_companies.php?sortBy=name&direction=desc$getFilter><u>Z-A</u></a><br>";
echo "Sort by Company Total Visits: <a href=list_companies.php?sortBy=totalVisits$getFilter><u>Least</u></a> <a href=list_companies.php?sortBy=totalVisits&direction=desc$getFilter><u>Most</u></a><br>";
echo "Sort by Last Active: <a href=list_companies.php?sortBy=lastActive$getFilter><u>Least Recent</u></a> <a href=list_companies.php?sortBy=lastActive&direction=desc$getFilter><u>Most Recent</u></a><br>";

echo "Sort by Total Users: <a href=list_companies.php?sortBy=totalUsers$getFilter><u>Least</u></a> <a href=list_companies.php?sortBy=totalUsers&direction=desc$getFilter><u>Most</u></a><br>";
echo "Sort by Profiles Visited: <a href=list_companies.php?sortBy=profilesVisited$getFilter><u>Least</u></a> <a href=list_companies.php?sortBy=profilesVisited&direction=desc$getFilter><u>Most</u></a><br>";
echo "Sort by Free Account: <a href=list_companies.php?sortBy=freeAccount&direction=desc$getFilter><u>Free First</u></a> <a href=list_companies.php?sortBy=freeAccount$getFilter><u>Free Last</u></a><br>";


echo "</center></td><td valign=top width=300 align=center>";

if (isset($_GET["show_frozen"])) {
	echo "<a href=list_companies.php><u>Hide Frozen</u></a>";
	
} else {
	echo "<a href=list_companies.php?show_frozen=true><u>Show Frozen</u></a>";
}
echo "</td></tr></table>";
$query = "SELECT *,
				rc_account.account_code,
				rc_subscription.uuId,
				company.id as company_id,
				(SELECT count(*) FROM search_url WHERE search_url.company_id = company.id)  as search_url_count,
				(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0) as li_user_count,
				(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0 and have_new_interface = 1) as new_ui_user_count,
				(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0 AND li_users.user_created >= DATE_SUB(NOW(), INTERVAL 24 HOUR)) as li_user_24h,
				(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0 AND li_users.user_created >= DATE_SUB(NOW(), INTERVAL 7 DAY)) as li_user_7_days,
				last_visit as last_activity,
				DATE_SUB(NOW(), INTERVAL 15 MINUTE) as last15Minutes,
				DATE_SUB(NOW(), INTERVAL 240 MINUTE) as last4Hours
				FROM company LEFT JOIN user ON assignedTo = user.id 
				LEFT JOIN rc_subscription ON rc_subscription.company_id = company.id 
				LEFT JOIN rc_account ON rc_account.company_id = company.id 
				WHERE company_name NOT LIKE '%NotInUse' AND deleted = 0 AND company.state = '' AND company.id < 602";
if($_USER_DATA["admin"] != 1){
	$query .= " AND assignedTo = '$_USER_DATA[id]'";
}
if (isset($_GET["show_frozen"])) {
	$query .= " AND frozen = 1";
} else {
	$query .= " AND frozen = 0";
}
if(isset($_GET["sortBy"])){
	if ($_GET["sortBy"] == "added") {
		$query .= " ORDER BY company.id";
	}
	if ($_GET["sortBy"] == "profilesVisited") {
		$query .= " ORDER BY people_count";
	}
	if ($_GET["sortBy"] == "totalUsers") {
		$query .= " ORDER BY li_user_count";
	}
	if ($_GET["sortBy"] == "name") {
		$query .= " ORDER BY company.company_name";
	}
	if ($_GET["sortBy"] == "lastActive") {
		$query .= " ORDER BY last_activity";
	}
	if ($_GET["sortBy"] == "freeAccount") {
		$query .= " ORDER BY subscription_plan_code";
	}

	if ($_GET["sortBy"] == "totalVisits") {
		$query .= " ORDER BY people_count";
	}
	if (isset($_GET["direction"]) and $_GET["direction"] == "desc") {
		$query .= " DESC";
	}
}else{
	$query .= " ORDER BY company.id DESC";
}

	/*			   
				   
$query = "SELECT *,(SELECT count(*) FROM search_url WHERE company_id = company.id) as search_url_count,
				   (SELECT count(*) FROM people WHERE company_id = company.id and people.name != '') as people_count,
				   (SELECT sent_to_crawler FROM people WHERE people.company_id = company.id AND sent_to_crawler > 0 ORDER BY people.id DESC LIMIT 1) as last_activity,
				   DATE_SUB(NOW(), INTERVAL 60 MINUTE) as last15Minutes FROM company WHERE deleted = 0";


*/


$result = mysql_query($query);
echo "<table width=100% class='pure-table'  border=1>";
echo "
<thead>
<tr><td><b>ID</b></td><td><b>Track</b></td><td><b>Company</b></td></td>";
if($_USER_DATA["admin"]){
	echo "<td><b>Manage</b></td>";
}


echo "<td><b>Search URLs</b></td><td><b>Visited Profiles</b></td><td><b>QS</b></td>";
if($_USER_DATA["admin"]){
	echo "<td><b>VBR</b></td><td><b>PE</b></td>";
}
echo "<td><b>7 Days</b></td>
<td><b>Total Users</b></td>
<td><b>Active Subscription</b></td>
<td><b>Last Visit</b></td>
<td><b>Created</b></td>
</tr>
</thead>
<tbody>
";
$urlCount = 0;
$profileCount = 0;
$searchUrlCountTotal = 0;
$searchScoreCountTotal = 0;
$i=0;
while($data = mysql_fetch_array($result)){
	$i++;
	$thisSubscription = "";
	$searchCount = 0;
	$searchScoreCount = 0;
	$urlScore = "";
	/*$urlQuery = "SELECT * FROM search_url WHERE company_id = $data[company_id]";
	$urlResult = mysql_query($urlQuery);
	
	while($urlData = mysql_fetch_array($urlResult)){
		
		$searchCount++;
		$searchScoreCount += getScore($urlData["url"],false);
	}
	
	if($searchCount > 0){
		$urlScore = number_format(($searchScoreCount/$searchCount),1);
	}*/
	$searchUrlCountTotal += $searchCount;
	$searchScoreCountTotal += $searchScoreCount;
	
	$newVbRate = 0;
	/*$vbQuery = "SELECT sum(visitor_count) as visitor_count,sum(profiles_visited) as profiles_visited FROM visitors WHERE company_id = $data[company_id] AND visitor_count > 0 AND profiles_visited > 100";
	$vbResult = mysql_query($vbQuery);
	$vbData = mysql_fetch_array($vbResult);
	$newVbRate = 0;
	if($vbData["profiles_visited"] > 0 AND $vbData["visitor_count"] > 0){
		$newVbRate = $vbData["visitor_count"]/$vbData["profiles_visited"];
	}
	
	
	
	if(isset($_GET["calculate_vb_rate"])){
		$vbRate = getVbRate($data["company_id"]);
	}else{
		$vbRate = 0;
	}*/
	$css="";
	if($i%2==0){
		$css = ' class="pure-table-odd"';
	}
	echo "<tr $css><td>".$data["company_id"];
	
	if ($data["frozen"]) {
		echo "&nbsp;-&nbsp;<b>Frozen</b><br><a href=/list_companies.php?unfreeze=".$data["company_id"]."><font color=gray>Unfreeze</font></a>";
	} else {
		echo "&nbsp;-&nbsp;<a href=/list_companies.php?freeze=".$data["company_id"]."><font color=gray>Freeze</font></a>";
	}
	

	
	echo "</td>";
	
	if ($data["track"]) {
		echo "<td><a href='/newAdmin/html_dumps.php?companyId=".$data["company_id"]."'>HTML</a>&nbsp;-&nbsp;<a href='/analyze_html.php?company_id=".$data["company_id"]."'>Analyze</a>&nbsp;-&nbsp;<a href='/auto_generate.php?company_id=".$data["company_id"]."'>Generated</a></td>";
	} else {
		echo "<td><a href=list_companies.php?track=".$data["company_id"]."><font color=gray>Turn on</font></a></td>";
	}
	
	echo"<td width=400><u><a href=/dashboard.php?company_id=".$data["company_id"].">".$data["company_name"]."</a></u>
		- ".$data["email"]." - <a href=\"/edit_company.php?edit_company_id=".$data['company_id']."\"><u>Edit</u></a>
	</td>";
	
	
	//-<a href=/dashboard.php?company_id=".$data["company_id"].">Campaign</a>
	if($_USER_DATA["admin"]){
		/*echo "<td><a href=assign.php?company_id=".$data["company_id"].">";
		if($data["user_name"] == ""){
			echo "<i>Assign</i>";
		}else{
			echo $data["user_name"];
		}
		echo "</a></td>";*/
		
		echo "<td><a href=delete.php?company_id=".$data["company_id"]."&delete=true>Delete</a></td>";
	}
	
	
	
	echo "<td align=right>".number_format($data["search_url_count"])."</td><td align=right>".number_format($data["people_count"])."</td><td align=right>".$urlScore."</td>";
	
	
	if($_USER_DATA["admin"]){
		if(isset($_GET["calculate_vb_rate"])){
			if($vbRate > 0){
				echo "<td align=right><a href=vb_company_score.php?company_id=".$data["company_id"].">".number_format($vbRate*100,2)."% - $newVbRate </a></td>";
			}else{
				echo "<td></td>";
			}
			if($vbRate > 0 and $urlScore > 0){
				$overallScore = number_format($vbRate*$urlScore*10,1);
				echo "<td align=right>$overallScore</td>";
			}else{
				echo "<td></td>";
			}
		}else{
			//echo "<td colspan=2><a href=list_companies.php?calculate_vb_rate=true>Calculate View-back Rate and Prospecting Efficiency</a></td>";
			if($newVbRate > 0){
				echo "<td align=right><a href=vb_company_score.php?company_id=".$data["company_id"].">".number_format($newVbRate*100,2)."%</a></td>";
			}else{
				echo "<td></td>";
			}
			if($newVbRate > 0 and $urlScore > 0){
				$overallScore = number_format($newVbRate*$urlScore*10,1);
				echo "<td align=right>$overallScore</td>";
			}else{
				echo "<td></td>";
			}
		}
	}
	if (isset($visitsLast7Days[$data["company_id"]])) {
		echo "<td align=right>".$visitsLast7Days[$data["company_id"]]."</td>";
	} else {
		echo "<td>".$data['phone']."</td>";
	}
	
	
		echo "<td align=right>";
		if ($data["li_user_24h"] > 0 ) {
			echo "<font color=darkgreen><b>".$data["li_user_24h"]." new 24h</b></font>";
		}
		if ($data["li_user_7_days"] > 0 ) {
			echo "<font color=darkorange><b>".$data["li_user_7_days"]." new over 7 days</b></font>";
		}
		if ($data["new_ui_user_count"] > 0 ) {
			echo "<font color=darkred><b>".$data["new_ui_user_count"]." USER WITH NEW UI</b></font>";
		}
		echo $data["li_user_count"]."</td>";
		
		echo "<td width=300>";
		/*if (isset($uuIdToSubscription[$data["uuid"]])) {
			echo $uuIdToSubscription[$data["uuid"]]->plan->name;
			echo " - $".round($uuIdToSubscription[$data["uuid"]]->unit_amount_in_cents*0.01);
			
			$uuIdToSubscription[$data["uuid"]] = "";*/
		if (isset($emailToSubscription[$data["account_code"]]) or isset($emailToSubscription[$data["email"]])) {
			$tmpEmail = "";
			if (isset($emailToSubscription[$data["account_code"]])) {
				$tmpEmail = $data["account_code"];
			} else {
				$tmpEmail = $data["email"];
			}
		
			foreach ($emailToSubscription[$tmpEmail] as $subscription) {
				$thisSubscription .= $subscription->plan->name;
				echo $subscription->plan->name;
				echo " - $".round($subscription->unit_amount_in_cents*0.01);
				echo "<br>";
			}
			if (count($emailToSubscription[$tmpEmail]) > 1) {
				echo "<b>This account have multiple active subscriptions</b>";
			}
			$emailToSubscription[$tmpEmail] = "";
		} else if ($data["subscription_plan_code"] == "noLimit"){
			echo "Free Unlimited Usage";
			$thisSubscription = "Free Unlimited Usage";
		} else if ($data["subscription_plan_code"] == "free"){
			echo "50 Visits Per Day";
			$thisSubscription = "50 Visits Per Day";
		}
		echo "</td>";
	
	
	//echo "<td>".$vbData["profiles_visited"]."</td>";
	
	if($data["last_activity"] > $data["last15Minutes"]){
		//<b><font color=DarkGreen>&lt;5&nbsp;minutes</a></b><br>(
		echo "<td width=100><b><font color=DarkGreen>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
	}else if($data["last_activity"] > $data["last4Hours"]){
		//<b><font color=DarkOrange>&lt;4&nbsp;hours</a></b><br>(
		echo "<td width=100><b><font color=DarkOrange>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
	}else if($data["last_activity"] == ""){
		echo "<td></td>";
	}else{
		if($data["last_activity"] == "0000-00-00 00:00:00"){
			echo "<td></td>";
		}else{
			echo "<td width=100>".date("M d Y", strtotime($data["last_activity"]))."</td>";
		}
		
	}
	echo "<td>";
	if ($data["company_created"] != "0000-00-00 00:00:00"){
		echo date("M d Y", strtotime($data["company_created"]));
	}
	echo "</td>";
	
	$urlCount += $data["search_url_count"];
	$profileCount += $data["people_count"];
	
	//Here is the actual prosperworks code
	
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, "https://api.prosperworks.com/developer_api/v1/companies/$data[prosperworks_id]"); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'X-PW-AccessToken:963d7614f7f9df2425bb386acba494d0',
	'X-PW-Application:developer_api',
	'Content-Type: application/json',
	'X-PW-UserEmail:chris@searchquant.net'
	));
	//91860 = subscription
	//92602 = User Count
	//93011 = ID
	if ($thisSubscription == "") {
		$thisSubscription = "No Subscription";
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		json_encode(
			array(
				"custom_fields" => array(
					array("custom_field_definition_id" => 91860, "value" => $thisSubscription),
					array("custom_field_definition_id" => 92602, "value" => $data["li_user_count"]),
					array("custom_field_definition_id" => 93011, "value" => $data["company_id"])
				)
			)
		)
	);
	echo "https://api.prosperworks.com/developer_api/v1/companies/$data[prosperworks_id] \n\n<br><br>\n\n $thisSubscription";
	$response = curl_exec($ch);
	echo $response;
	curl_close($ch);
	

	
	
}

$urlScore = 0;
if($searchUrlCountTotal > 0){
	$urlScore = number_format(($searchScoreCountTotal/$searchUrlCountTotal),1);
}

echo "</tr><tr><td><b>Total</b></td>";
if($_USER_DATA["admin"]){
	echo "<td></td><td></td>";
}


echo "<td align=right><b>".number_format($urlCount)."</b></td><td align=right><b><a href=/visitsPerDay.php>".number_format($profileCount)."</a></b></td><td align=right><b>$urlScore</b></td>";
if($_USER_DATA["admin"]){
	echo "<td></td><td></td>";

}


echo"<td></td></tr>";
echo "</tbody></table>";

/*
echo "<br><form action=/list_companies.php method=post class='pure-form'><fieldset>
<legend>Add New Company</legend>
<input type=text name=company_name  placeholder='Company Name'> <input type=submit value='Add Company'  class='pure-button pure-button-primary'></form>";
*/
echo "</div>";

echo "<h2>Subscriptions with no match</h2>";
//foreach ($uuIdToSubscription as $subscription) {
echo "<table border=1>";
foreach ($emailToSubscription as $subscriptions) {
	if ($subscriptions != "") {
		foreach ($subscriptions as $subscription) {
			$p = explode("/",$subscription->account->getHref());
			echo "<tr>
				<td>".$p[count($p)-1]."</td>
				<td>".$subscription->plan->name."</td>
				<td>$".round($subscription->unit_amount_in_cents*0.01)."</td>
			</tr>";
		}
	}
}
echo "</table>"
?>

<?
require_once('footer.php');

?>
