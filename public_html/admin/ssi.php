<?php
$HTML_TITLE = "List Companies";
require_once('../header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}
error_reporting(E_ALL);
ini_set('display_errors', '1');

$query = "SELECT * FROM ssi JOIN company ON company.id = ssi.company_id ORDER BY ssi_crawled_at DESC";
$result = mysql_query($query);
echo "<table class=table>
	<tr>
		<td>Company</td>
		<td>User</td>
		<td>Score</td>
		<td>Timestamp</td>
	</tr>";
$output = array();
while ($data = mysql_fetch_array($result)) {
	$rows = explode("\n",$data["ssi_html"]);
	$overall_score = "";
	foreach ($rows as $row) {
		if(strpos($row,'"mScore":{"overall":') !== false){
			$cells = explode('"mScore":{"overall":',$row);
			$p = explode(',',$cells[1]);
			$overall_score = $p[0];
		}
	}
	$output[$overall_score] =  "
	<tr>
		<td><a href=/dashboard.php?company_id=$data[company_id]><u>".$data["company_name"]."</u></a></td>
		<td>".$data["crawled_by"]."</td>
		<td>".$overall_score."</td>
		<td><a href=view_html.php?ssi_id=$data[ssi_id]><u>".$data["ssi_crawled_at"]."</u></a></td>
	</tr>";

}
krsort($output);
echo implode("\n",$output);
echo "</table>"
?>