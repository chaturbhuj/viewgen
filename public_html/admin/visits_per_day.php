<?
//ini_set('memory_limit', '64M');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../header.php');
require_once("../shared_functions.php");

if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}



error_reporting(E_ALL);
ini_set('display_errors', '1');

updateStat();

$query = "SELECT 
		visits as count, 
		visits_to_premium as premiumCount,
		DATE_FORMAT(admin_daily_statistics_date,'%b %d %Y') as onDate,
		DATE_FORMAT(admin_daily_statistics_date,'%a') as dayOfWeek,
		admin_daily_statistics_date as sent_to_crawler_date,
		active_companies,
		active_users,
		visits_to_premium,
		active_windows,
		active_mac,
		visits_windows,
		visits_mac,
		inbound_visits
	FROM admin_daily_statistics 
	ORDER BY admin_daily_statistics_date";
$result = mysql_query($query);
$cumulative = 0;
$i=0;
$dates = array();
$cumulated = array();
$daily = array();

$valuesPerDay = array();

$cumulatedValuesPerDay = array(); 
$usersPerDay = array(); 
$companiesPerDay = array(); 
$clientPerDay = array(); 
$clientVisitsPerDay = array(); 
$premiumPercentPerDay = array(); 
$premiumPercentPerMonth = array();
$currentMonth = "2015-01";
$premiumMonthCount = 0;
$visitsMonthCount = 0;
$tableRows = array();
while($data = mysql_fetch_array($result)){
	$cumulative+=$data["count"];
	$css = "";
	if($i %2 == 0){
		$css = ' class="pure-table-odd"';
	}
	if($data["onDate"] == ""){
		$data["dayOfWeek"] ="";
	}
	if($i % 5 == 0 OR $i == 1){
		$dates[] = $data["onDate"];
	}else{
		$dates[] = "";
	}
	/*$tableRows[] = "
	<tr>
		<td>$data[sent_to_crawler_date]</td>
		<td>$data[count]</td>
		<td>$data[visits_to_premium]</td>
		<td>".(round($data["visits_to_premium"]/$data["count"],4)*100)."%</td>
		<td>$data[active_companies]</td>
		<td>$data[active_users]</td>
	</tr>";*/
	
	if ($data["sent_to_crawler_date"] != "0000-00-00") {
		$valuesPerDay[] = array($data["sent_to_crawler_date"],$data["count"],0);
		$cumulatedValuesPerDay[] = array($data["sent_to_crawler_date"],$cumulative,0);
		$companiesPerDay[] = array($data["sent_to_crawler_date"],$data["active_companies"],$data["active_users"]);
		$clientPerDay[] = array($data["sent_to_crawler_date"],$data["active_windows"],$data["active_mac"]);
		$clientVisitsPerDay[] = array($data["sent_to_crawler_date"],$data["visits_windows"],$data["visits_mac"]);
		$premiumPercentPerDay[] = array($data["sent_to_crawler_date"],round($data["visits_to_premium"]/$data["count"],4)*100);
		

		if ($currentMonth != date('Y-m', strtotime($data["sent_to_crawler_date"]))) {
			if ($visitsMonthCount > 0) {
				$premiumPercentPerMonth[] = array($currentMonth."-01",round($premiumMonthCount/$visitsMonthCount,4)*100);
			}
			$currentMonth = date('Y-m', strtotime($data["sent_to_crawler_date"]));
			$visitsMonthCount = 0;
			$premiumMonthCount = 0;
		}
		$visitsMonthCount += $data["count"];
		$premiumMonthCount += $data["visits_to_premium"];
	}

	

	$tableRows[] = "<tr $css><td>$data[onDate]</td><td>$data[dayOfWeek]</td><td align=right>".number_format($data["count"])."</td><td align=right>".number_format($cumulative)."</td><td align=right>$data[premiumCount]</td><td align=right>".round(($data["premiumCount"]/$data["count"])*100)."%</td><td align=right>$data[inbound_visits]</td></tr>";
	
	$i++;
}
unset($data, $dates, $query, $result);

echo '<div id="page">';



makeGraph($valuesPerDay,"100%","400px",array("Visits per day"));
makeGraph(newSignups(),"100%","400px",array("Daily sign ups","Weekly sign ups"));
makeGraph($companiesPerDay,"100%","400px",array("Active Companies","Active Users"));
echo '"Active" is defined as having made at least one visit during the day';
makeGraph($clientPerDay,"100%","400px",array("Active Windows","Active Mac"));
makeGraph($clientVisitsPerDay,"100%","400px",array("Windows Visits","Mac Visits"));
makeGraph($premiumPercentPerDay,"100%","400px",array("Percent with premium"));
makeGraph($premiumPercentPerMonth,"100%","400px",array("Percent with premium monthly"));
makeGraph($cumulatedValuesPerDay,"100%","400px",array("Cumulated Visits"));
//echo implode("\n",$tableRows);

echo "</div>";

echo "<table border=1>
	<tr>
		<th>Date</th>
		<th>Day of Week</th>
		<th>Visits</th>
		<th>Cumulated Visits</th>
		<th>Premium#</th>
		<th>Premium%</th>
		<th>Inbound</th>
	</tr>";
$tableRows = array_reverse($tableRows);
echo implode("",$tableRows);
echo "</table>";
/*
echo '
	<script>

		var lineChartData = {
			labels : ["'.implode('","',$dates).'"],
			datasets : [
			
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : ['.implode(',',$cumulated).']
				}
			]
			
		}

	var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);
	
			var lineChartData2 = {
			labels : ["'.implode('","',$dates).'"],
			datasets : [
			
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : ['.implode(',',$daily).']
				}
			]
			
		}

	var myLine2 = new Chart(document.getElementById("canvas2").getContext("2d")).Line(lineChartData2);
	</script>
';
*/

//require_once('../footer.php');

/**
 * Functions
 */

function newSignups(){
	$query = DB::prep("
		SELECT 
			count(*) numberOfSignUps,
			date(company_created) signUpDate 
		FROM `company` 
		WHERE 
			company_created > '0000-00-00 00:00:00' AND
			state = ''
		GROUP BY date(company_created)
	");
	$query->execute();
	$lastDate = "2015-01-01";
	while ($data = $query->fetch()) {
		if($lastDate != ""){

			$fromDate = strtotime($lastDate);
			$toDate = strtotime($data["signUpDate"]);

			$nextDay = strtotime(date("Y-m-d",$fromDate).' + 1 day');
			while($nextDay < $toDate){
				//$dateToValue[date("Y-m-d",$nextDay)] = array(date("Y-m-d",$nextDay),0,"null");
				$newSignUps[] = array(date("Y-m-d",$nextDay),0,0);
				$nextDay = strtotime(date("Y-m-d",$nextDay).' + 1 day');
			}
		}
		$lastDate = $data["signUpDate"];
		$newSignUps[] = array($data["signUpDate"],$data["numberOfSignUps"],0);
	}
	for ($i=0;$i<count($newSignUps);$i++) {
		$dayOfWeek = date( "w", strtotime($newSignUps[$i][0]));
		if ($dayOfWeek == 0) {
			@$wholeWeek = $newSignUps[$i][1]+$newSignUps[$i+1][1]+$newSignUps[$i+2][1]+$newSignUps[$i+3][1]+$newSignUps[$i+4][1]+$newSignUps[$i+5][1]+$newSignUps[$i+6][1];
			$newSignUps[$i][2] = $wholeWeek;
			if (isset($newSignUps[$i+1]))
				$newSignUps[$i+1][2] = $wholeWeek;
			if (isset($newSignUps[$i+2]))
				$newSignUps[$i+2][2] = $wholeWeek;
			if (isset($newSignUps[$i+3]))
				$newSignUps[$i+3][2] = $wholeWeek;
			if (isset($newSignUps[$i+4]))
				$newSignUps[$i+4][2] = $wholeWeek;
			if (isset($newSignUps[$i+5]))
				$newSignUps[$i+5][2] = $wholeWeek;
			if (isset($newSignUps[$i+6]))
				$newSignUps[$i+6][2] = $wholeWeek;
		}

	}

	return $newSignUps;
}

function updateStat(){
	$dates = array();

	$idToClient = array();

	$query = DB::prep("SELECT lu.linked_in_id, cv.name as new_client_version FROM li_users lu LEFT JOIN client_versions cv ON lu.client_version_id = cv.id WHERE client_version_id IS NOT NULL ORDER BY last_connection DESC");
	$query->execute();
	while ($data = $query->fetch()) {
		if (isset($idToClient[$data["linked_in_id"]])) {
			//we already have assigned this ID with a more recent last_connection
		} else if (strpos($data["new_client_version"],"Windows") !== false) {
			$idToClient[$data["linked_in_id"]] = "Windows";
		} else {
			$idToClient[$data["linked_in_id"]] = "Mac";
		}

	}

	$min_date = "2016-01-01";
	$query = DB::prep("SELECT max(`admin_daily_statistics_date`) last_date FROM `admin_daily_statistics`");
	$query->execute();
	if ($data = $query->fetch()) {
		$min_date = $data["last_date"];

	}

	$max_date = date('Y-m-d');

//$min_date = "2016-01-01";


	$active_date = $min_date;
	while ($active_date <= $max_date) {
		$dates[] = $active_date;
		$active_date = date('Y-m-d', strtotime($active_date.' + 1 days'));
	}
//$dates = array("2016-10-01");
	$campaigns_to_update = array();
	foreach ($dates as $date) {
		//echo "Starting with $date - ".date("H:i:s")."\n";
		$inboundCount = 0;

		$query = DB::prep("
			SELECT 
				count(*)
			FROM inbound_visit 
			WHERE 
				DATE(visit_created) = ?
		");
		$query->execute([$date]);
		$inboundCount = $query->fetchColumn();

		$query = DB::prep("
			SELECT 
				crawled_by_id,
				company_id,
				premium
			FROM people 
			WHERE 
				name != '' AND
				sent_to_crawler_date = ?
		");

		$query->execute([$date]);
		$aggregates = array();
		$peoplCount = 0;
		$premiumCount = 0;
		$uniqueCompanies = array();
		$uniqueUsers = array();

		$uniqueMac = array();
		$uniqueWindows = array();

		$visitsMac = 0;
		$visitsWindows = 0;


		while ($data = $query->fetch()) {
			$peoplCount++;
			if ($data["premium"]) {
				$premiumCount++;
			}
			$uniqueCompanies[$data["company_id"]] = true;
			$uniqueUsers[$data["crawled_by_id"]] = true;
			if (isset($idToClient[$data["crawled_by_id"]])) {
				if ($idToClient[$data["crawled_by_id"]] == "Windows") {
					$uniqueWindows[$data["crawled_by_id"]] = true;
					$visitsWindows++;


				} else {
					$uniqueMac[$data["crawled_by_id"]] = true;
					$visitsMac++;

				}
			}
		}


		$query = DB::prep("
			INSERT INTO admin_daily_statistics
			(admin_daily_statistics_date, visits, visits_to_premium, active_users, active_companies, active_windows, active_mac, visits_windows, visits_mac, inbound_visits)
			VALUES
			(:admin_daily_statistics_date, :visits, :visits_to_premium, :active_users, :active_companies, :active_windows, :active_mac, :visits_windows, :visits_mac, :inbound_visits)
			ON DUPLICATE KEY UPDATE
			visits = :visits,
			visits_to_premium = :visits_to_premium,
			active_users = :active_users,
			active_companies = :active_companies,
			active_windows = :active_windows,
			active_mac = :active_mac,
			visits_windows = :visits_windows,
			visits_mac = :visits_mac,
			inbound_visits = :inbound_visits
		");

		$query->execute([
			"admin_daily_statistics_date" => $date,
			"visits" => $peoplCount,
			"visits_to_premium" => $premiumCount,
			"active_users" => count($uniqueUsers),
			"active_companies" => count($uniqueCompanies),
			"active_windows" => count($uniqueWindows),
			"active_mac" => count($uniqueMac),
			"visits_windows" => $visitsWindows,
			"visits_mac" => $visitsMac,
			"inbound_visits" => $inboundCount
		]);

	}

}