<?php

require_once('db.php');

$redirect = base_url("dashboard.php");

if (isset($_GET['redirect'])) {
	$redirect = $_GET['redirect'];
}

if (isset($_POST['redirect']) && $_POST['redirect'] != '') {
	$redirect = $_POST['redirect'];
}

$error_msg = "";

if(isset($_POST['username']) && $_POST["username"] != "" AND $_POST["password"] != ""){
	// Login code
	$username = $_POST['username'];
	$hash_password = md5($_POST["password"]."j567cdJKHL@@87g");

	// Authenticate the user.
	$query = DB::prep("select * from user where user_name=? and password=?");
	$query->execute(array($username, $hash_password));

	$is_authenticated = false;
	if ($query->rowCount() == 0) {
		// No match. Check if a company can take it's place
		$query = DB::prep("select * from company where company_name=? or email = ?");
		$query->execute(array($username, $username));

		if ($query->rowCount() > 0) {
			$company = $query->fetch();

			if ($company['password_salt'] == "") {
				if (md5($company['company_password'].'j567cdJKHL@@87g') == $hash_password) {
					$_SESSION['auth'] = array(
						'user_id' => 0,
						'company_id' => $company['id']
					);
					$is_authenticated = true;
				}
			} else {
				if (hash("sha256",$_POST["password"].$company['password_salt']) == $company['password_hash']) {
					$_SESSION['auth'] = array(
						'user_id' => 0,
						'company_id' => $company['id']
					);
					$is_authenticated = true;

					if ($company['state'] == "draft") {
						redirect("sign_up_step_two.php");
					}
				}
			}
		}
	} else {
		$user = $query->fetch();
		$_SESSION['auth'] = array(
			'user_id' => $user['id'],
			'company_id' => 0
		);
		$is_authenticated = true;
	}

	if ($is_authenticated) {
		header(sprintf('Location: %s', $redirect));
		exit;
	}
	$error_msg = "wrong_password";
}

if ($error_msg != "") {
	header("Location: /ui/login/?error_msg=" . urlencode($error_msg));
	die;
}

?>
<center>
<?=$error_msg?>
<form method="post" action="/login.php">
<input type=text name=username placeholder=username><br>
<input type=password name=password placeholder=password><br>
<input type=submit value="Sign In">
</form>
</center>
