<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

$ACTIVE_HEADER_PAGE = "reports";
$HTML_TITLE = "Reports";

require_once("header.php");
require_once("shared_functions.php");

$global_volume_per_day = getGlobalAverageVisitsPerDay(7);

?>
	<div class="container reports">
		<div class="row">
			<div class="col-xs-8">
				<h1>Reports</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-8">
				<div id="campaign_list">
<?

	$query = DB::prep("SELECT * FROM li_users WHERE company_id = ? AND linked_in_id > 0 AND hidden = 0");
	$query->execute(array($_GET['company_id']));
	while($data = $query->fetch()){
		$dateToValue = array();
		$values = array();

		// Check the cache first.
		$subQuery = DB::prep("SELECT number_of_visits AS count, cache_date AS date FROM reports_cache WHERE li_users_id = ? ORDER BY cache_date ");
		$subQuery->execute(array($data["linked_in_id"]));

		$lastDate = "";
		while($subData = $subQuery->fetch()){
			if($lastDate != ""){
			
				$fromDate = strtotime($lastDate);
				$toDate = strtotime($subData["date"]);

				$nextDay = strtotime(date("Y-m-d",$fromDate).' + 1 day');
				while($nextDay < $toDate){
					$dateToValue[date("Y-m-d",$nextDay)] = array(date("Y-m-d",$nextDay),0,"null");
					$nextDay = strtotime(date("Y-m-d",$nextDay).' + 1 day');
				}
			}
			
			
			$lastDate = $subData["date"];
			$dateToValue[$subData["date"]] = array($subData["date"],$subData["count"],"null");
		}

		/*$subQuery = DB::prep("SELECT count(*) count, sent_to_crawler_date date FROM people WHERE company_id = ? AND crawled_by_id = ? AND sent_to_crawler_date > ? GROUP BY sent_to_crawler_date ORDER BY sent_to_crawler_date");
		$subQuery->execute(array($_GET['company_id'],$data["linked_in_id"], $last_date));
		while($subData = $subQuery->fetch()){
			if($lastDate != ""){
			
				$fromDate = strtotime($lastDate);
				$toDate = strtotime($subData["date"]);

				$nextDay = strtotime(date("Y-m-d",$fromDate).' + 1 day');
				while($nextDay < $toDate){
					$dateToValue[date("Y-m-d",$nextDay)] = array(date("Y-m-d",$nextDay),0,"null");
					$nextDay = strtotime(date("Y-m-d",$nextDay).' + 1 day');
				}
			}
			
			
			$lastDate = $subData["date"];
			$dateToValue[$subData["date"]] = array($subData["date"],$subData["count"],"null");
		}*/
		
		
		$subQuery = DB::prep("SELECT *  FROM visitors WHERE company_id = ? AND linked_in_user_id = ? ORDER BY date");
		$subQuery->execute(array($_GET['company_id'],$data["linked_in_id"]));
		
		while($subData = $subQuery->fetch()){
			for($i=0;$i<7;$i++){
				$tmpDay = date("Y-m-d",strtotime($subData["date"]." - $i day")); 
				
				if(isset($dateToValue[$tmpDay]) && count($dateToValue[$tmpDay]) == 3){
					$dateToValue[$tmpDay][2] = $subData["visitor_count"];
				}else{
					$dateToValue[$tmpDay] = array($tmpDay,0,$subData["visitor_count"]);
				}
				
			}

		}
		$values = array();
		$dateKeys = array_keys($dateToValue);
		sort($dateKeys);
		foreach($dateKeys as $tmpDate){
			$values[] = $dateToValue[$tmpDate];
		}

		if(count($values) > 0){
			echo "<div class=\"campaign\"><h2>$data[name]</h2>";
			echo "&nbsp;&nbsp;&nbsp;<a href=/visitedProfiles.php?company_id=".$_GET["company_id"]."&user=".$data["linked_in_id"]."><u style='font-size: 18px;'>".$data["name"]."'s Visited Profiles</u></a>";
			//echo " - <a href=whoHasViewedMyProfile.php?company_id=".$_GET["company_id"]."&user=".$data["linked_in_id"]."><u>Who Has Viewed My Profile</u></a>";
			echo " - <a href=visitedProfiles.php?company_id=".$_GET["company_id"]."&user=".$data["linked_in_id"]."&days=0&only_view_backs=on><u style='font-size: 18px;'>Who Has Viewed My Profile</u></a>";
			//if (isset($companyIsAutenticated) && !$companyIsAutenticated) { 
				echo " - <a href=/overlap.php?company_id=".$_GET["company_id"]."&user=".$data["linked_in_id"]."><u style='font-size: 18px;'>Overlap Rate</u></a>";
			//}
			echo " - <a href=/urlAnalysis.php?company_id=$_GET[company_id]&user=$data[linked_in_id]><u style='font-size: 18px;'>Query Analysis</u></a>";
			// Output some metrics (volume and VBR compared to averages).

			$vbr = $data['account_vbr'];
			if ($vbr === null) {
				$vbr = "N/A";
			} else {
				$vbr = round($vbr*100, 2) . "%";
			}

			$period_days = 14;
			$volume_last_period = 0;
			$days = 0;
			for ($i = count($values) - 1; $i >= count($values) - $period_days; $i--) {
				if ($values[$i][1] == 0) continue;
				$volume_last_period += $values[$i][1];
				$days++;
			}
			if ($days > 0) $volume_last_period = round($volume_last_period / $days);
			?>
			<div class="vbr-metrics">
				<div class="row">
					<div class="col-xs-12">
						<strong>Key Performance Indicators</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<b>Average Daily Volume</b><br/>
						<span class="trow">
							You:
							<span <?=$volume_last_period < $global_volume_per_day * 0.8 ? ' class="red"' : ''?>>
								<?=$volume_last_period ?> / day
							</span>
						</span><br/>
						<span class="trow over-all">SQ average: 581 / day</span>
					</div>
					<div class="col-xs-6">
						<b>View-Back Rate (VBR)</b><br/>
						<span class="trow">
							You:
							<span<?=$data['account_vbr'] < 0.064*0.8 ? ' class="red"' : ''?>><?=$vbr?></span>
						</span><br/>
						<span class="trow over-all">SQ average: 6.4%</span>
					</div>
				</div>
			</div>
			<?php
			
			echo makeGraph($values,"100%","400px",array("Profiles visited (Daily)","Visitors to profile (Weekly)"));
			
			if($_USER_DATA["admin"]){
				echo "Only for admins: 
				<a href=/timeOfDayAnalysis.php?company_id=$_GET[company_id]&user=$data[linked_in_id]><u>Time of Day</u></a>
				";
			}
			echo "</div>";
		}
	}

?>


			</div>
		</div>
		
		
	</div>

<?php

require_once("footer.php");

?>
