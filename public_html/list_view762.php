<?php
    global $CONFIG;
    $IS_CRON_JOB = true;
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    require_once (__DIR__ . '/../vendor/autoload.php');
    require_once (__DIR__ . '/db.php');
    require_once (__DIR__ . '/lib/recurly.php');

    $query = DB::prep("
        SELECT
          c.id,
          c.company_name,
          c.last_visit,
          c.people_count,
          c.first_name,
          c.last_name,
          c.email,
          c.phone,
          c.automatic_alerts_email,
          c.company_created,
          c.last_page_view_searchquant,
          c.last_page_view_viewgen,
          c.recurlyAccountCode,
          rc_account.account_code,
          count(li_users.id) AS users
        FROM company c 
          LEFT JOIN rc_account
            ON c.id = rc_account.company_id
          LEFT JOIN li_users
            ON c.id = li_users.company_id
		WHERE
		  c.frozen = 1
          AND c.deleted = 0
          AND c.is_test = 0
          AND c.email like \"%@%\"
        GROUP BY
          c.id
        HAVING
          users >= 3
    ");
    $query->execute();



?>
sep=,
ID,name,last visit,visits count,first name,last name,email,phone,Created,SQ Page View,VG Page View,recurly code,old Recurly code,users count,paid month

<?php
    while($data = $query->fetch()){

        if(!empty($data['recurlyAccountCode'])) {
            try {
                $invoices = Recurly_InvoiceList::getForAccount($data['recurlyAccountCode']);
            } catch (Exception $e) {
            }
        }
        if(empty($invoices) || $invoices->count() < 2){
            continue;
        }

        $startInvoice = null;
        $endInvoice = null;
        foreach ($invoices AS $invoice){
            if(!$endInvoice && $invoice->state === 'collected'){
                $endInvoice = $invoice;
            }

            if($invoice->state === 'collected'){
                $startInvoice = $invoice;
            }
        }

//        var_dump($startInvoice);
//        var_dump($endInvoice);


        $startDate = $startInvoice->created_at;
        $endDate = $endInvoice->created_at;

        $interval = $endDate->diff($startDate, true);
//        var_dump($interval->m);

        if($interval->m < 4){
            continue;
        }


?>
<?=$data["id"]?>,<?=$data["company_name"]?>,<?=$data["last_visit"]?>,<?=$data["people_count"]?>,<?=$data["first_name"]?>,<?=$data["last_name"]?>,<?=$data["email"]?>,<?=$data["phone"]?>,<?=$data["company_created"]?>,<?=$data["last_page_view_searchquant"]?>,<?=$data["last_page_view_viewgen"]?>,<?=$data["recurlyAccountCode"]?>,<?=$data["account_code"]?>,<?=$data["users"]?>,<?=$interval->m?>

<?php }?>