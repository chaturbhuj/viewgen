<?php
$HTML_TITLE = "Title Suggestions";
$ACTIVE_HEADER_PAGE = "dashboard";
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include('header.php');

if (!isset($_GET['company_id'])) {
	die("You must supply company_id");
}

if (!isset($_GET['user'])) {
	die("You must supply user parameter");
}
print_r(getSuggestedUrls(291, 16148769, 10, 10, false));
function getSuggestedUrls($company_id, $linked_in_id, $maxTitles = 30, $maxLocations = 30, $printHtml = false) {

	$htmlOutput = array();
	$urls = array();
	$query = DB::prep("SELECT * 
		FROM people 
		WHERE 
			company_id = ? AND
			crawled_by_id = ?
		LIMIT 100000");
	$query->execute(array($company_id, $linked_in_id));

	$locationCount = array();
	$locationOnce = array();
	$titleCount = array();
	$titleOnce = array();
	while ($people = $query->fetch()) {
		
		$p = explode(" at",$people["title"]);
		$p = explode(" @",$p[0]);
		$p = explode(" -",$p[0]);
		$cleanTitle = trim(mb_strtolower($p[0]));
		if ($titleOnce[$cleanTitle]) {
			if ($titleCount[$cleanTitle]) {
				$titleCount[$cleanTitle] ++;
			} else {
				$titleCount[$cleanTitle] = 2;
			}
			
		} else {
			$titleOnce[$cleanTitle] = 1;
		}
		
		if ($locationOnce[$people["location"]]) {
			if ($locationCount[$people["location"]]) {
				$locationCount[$people["location"]] ++;
			} else {
				$locationCount[$people["location"]] = 2;
			}
			
		} else {
			$locationOnce[$people["location"]] = 1;
		}
	}
	arsort($titleCount);
	$count = 0;
	$relevantTitles = array();
	foreach ($titleCount as $key => $value) {
		$words = explode(" ",$key);
		if ($count < $maxTitles and count($words) == 3) {
			$relevantTitles[] = $key;
			$count++;
		}
	}
	arsort($locationCount);


	$count = 0;
	foreach ($locationCount as $locationName => $value) {
		
		if ($count < $maxLocations) {
			
			$locationQuery = DB::prep("SELECT * 
			FROM li_location 
			WHERE location_name = ? LIMIT 1");
			$locationQuery->execute(array($locationName));
			$locationObject = $locationQuery->fetch();
			$locationId = $locationObject["linked_in_id"];
			if ($locationId != "") {
				$htmlOutput[] = "<h1>$locationName</h1>\n";
				foreach ($relevantTitles as $relevantTitle) {
					/*$hitsQuery = DB::prep("SELECT * 
				FROM people 
				WHERE location = '".addslashes($locationName)."' AND title LIKE '".addslashes(trim($relevantTitle))."%' LIMIT 10");
					
					$hitsQuery->execute(array());
					$hitsObject = $hitsQuery->fetchAll();
					//print_r($hitsObject);
					$hitsCount = count($hitsObject);
					echo "<b>'$relevantTitle' in $locationName gives $hitsCount hits</b><br>";*/
					$tmpUrl = "https://www.linkedin.com/vsearch/p?title=%22".str_replace("+","%20",urlencode(html_entity_decode($relevantTitle)))."%22&openAdvancedForm=true&titleScope=C&locationType=Y&f_G=".$locationId."&rsid=161487691456278556279&orig=ADVS";
					$urls[] = $tmpUrl;
					$htmlOutput[] = "<b>$relevantTitle</b><br>$tmpUrl<br>\n";
				
				}
				
				$count++;
			}
		}
	}
	if ($print_html) {
		echo implode("\n",$htmlOutput);
	}
	return $urls;
}
?>

