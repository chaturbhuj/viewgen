<?php

$IGNORE_AUTH = true;
$STOPMENU = true;

require_once('db.php');

require_once('shared_functions.php');

require_once("header.php");
require_once("header_index.php");
require_once(__DIR__ . '/classes/EmailTemplate.php');

/*$redirect = base_url("dashboard.php");
if (isset($_GET['redirect'])) {
	$redirect = $_GET['redirect'];
}*/

$error_msg = "";
$msg = "";

if (isset($_POST["email"]) && $_POST["email"] != "") {
	// Login code
    $email = $_POST['email'];
    $query = DB::prep("select * from company where company_name=? or email = ?");
    $query->execute(array($email, $email));
    if ($query->rowCount() > 0) {
        $company = $query->fetch();
        if ($company['email']) {
            $hash = md5($company['company_name'] . time());
            $url = base_url('reset_password.php?hash=' . $hash);
            if (!preg_match('#^http#', $uri)) {
                $url = 'https:' . $url;
            }
            $query = DB::prep("UPDATE company SET reset_password_hash = :hash, reset_password_date = now() WHERE id = :company_id");
            $query->execute([
                'hash' => $hash,
                'company_id' => $company['id']
            ]);
            $emailTemplate = new EmailTemplate($company['id']);
            $param = [
                'company name' => $company['company_name'],
                'reset password url' => $url
            ];
            $emailTemplate->sendEmail($company['email'], "Reset Password", $param);
            $msg = "Email sent.<br> Check your email.";
        } else {
            $error_msg = "Email not set";
        }
    } else {
        $error_msg = "Wrong Email or Account Name";
    }

	
}

?>
<div class="segment sign_up_title">
	<div class="segment_inner">
		<h1>Request New Password</h1>
	</div>
</div>
<div class="segment sign_up_step_one">
	<div class="segment_inner">
        <span style="color:red;"><?=$error_msg;?></span>
			<?=$msg;?>
		<form action="<?=base_url('requestNewPassword.php')?>" method="post">
		<input type="submit" class="removed" value="Sign In">
		<div class="form_section login">
			<h2>Please enter the email you used to create your account or the account name</h2>
			<input type="text" name="email" placeholder="Email or Account Name" style="width: 280px;">
			
			<a href="<?=base_url('requestNewPassword.php')?>" class="button medium green" id="form_submit"  style="width: 240px;">Reset my Password</a>
			
		</div>
		</form>
	</div>
</div>
<div class="segment bottom">
	<div class="segment_inner">
	</div>
</div>
<?php
require_once('footer.php');
?>
