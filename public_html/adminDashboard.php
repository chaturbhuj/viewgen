<?php

$HTML_TITLE = "Admin Dashboard";

require_once('header.php');



if(!$_USER_DATA["admin"]){

	die("You don't have permission to see this page");

}



?>



<div class="container">



	<br>

	Go to:<br>

	<b><font color=green>Pages loading relatively quickly:</font></b><br>

	<a target="_blank" href="list_companies.php">List Companies</a> - <a href="list_companies.php?sortBy=name">Alphabetically, A-Z</a> - <a href="list_companies.php?sortBy=lastActive&direction=desc">Most recently active</a><br>

	<a target="_blank" href="admin/visits_per_day.php">Total visits per day</a><br><br>

	<a target="_blank" href="adminReferer.php">Referrals Admin</a><br>

	<a target="_blank" href="sign_up_aborted.php">Aborted Sign Ups (Leads)</a><br>

	<a target="_blank" href="success_status.php">Success Metrics For Individual Users</a><br>

	<a target="_blank" href="companies_to_upgrade.php">Companies to Upgrade</a><br>

	<a target="_blank" href="/admin/email/">Email System</a><br>

	<a target="_blank" href="/admin/user_status.php">User Status</a><br>

	<a target="_blank" href="/admin/subscription_rate_per_title.php">Subscription Rate Per Title</a><br>

	<a target="_blank" href="/admin/ssi.php">User SSI</a><br>

	<a target="_blank" href="/admin/newUiList.php"><b>User With NEW UI</b></a><br>

	<a target="_blank" href="/admin/contactForm.php">Contact Form</a><br><br>

    <a target="_blank" href="/new-ui/newDashboardLanding.html">New Dashboard</a><br><br>

	<a target="_blank" href="/admin/prospects.php">Prospects</a><br>

	<a target="_blank" href="/newAdmin/list_campaigns.php">Campaigns list</a><br>
	<a target="_blank" href="/newAdmin/list_urls_stopped_early.php">Urls stopped early</a><br>
	<a target="_blank" href="/newAdmin/lockouts_last_24h.php">Linked in lockouts last 24h</a><br>
	<a target="_blank" href="/newAdmin/users_with_low_visits.php">Users with low visits for over than week</a><br>
	<a target="_blank" href="/newAdmin/referrers_filtered.php">Referrers</a><br>




	<br>



	<br><br>

	<b><font color=darkred>Pages that may load slowly and make the entire site slow for everyone. Be cautious!!</font></b><br>

	<a target="_blank" href="user_activity.php">List User Activity</a><br>

	<a target="_blank" href="list_client_versions.php">List Client Versions</a><br>

	<a target="_blank" href="/newAdmin/html_dumps.php">List html dumps</a><br>

	<a target="_blank" href="/newAdmin/bad_pages.php">Bad pages</a><br>

	<br>

	<a target="_blank" href="stats/perDay.php">Visits per day of week as a percentage</a><br>

	<a target="_blank" href="stats/perQuery.php">Visits per Query URL</a><br>

	<a target="_blank" href="stats/perCampaign.php">Queries per Campaign</a><br>

	<br>

	<a target="_blank" href="stats/vbrGraph.php">VBR Graph</a><br>

	<br>

	<a target="_blank" href="/allUsersReport.php">Graphs for all users in paying accounts</a><br>

	<a target="_blank" href="/canceled.php">Canceled Companies List</a><br>

	<br>

	<a target="_blank" href="/admin_kpi_overview.php">Admin KPI Overview</a><br>

</div>



