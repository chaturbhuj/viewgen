<?php
$HTML_TITLE = "List Companies";
require_once ('lib/recurly.php');
require_once('header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

echo "<center>";

error_reporting(E_ALL);
ini_set('display_errors', '1');
$uuIdToSubscription = array();
$subscriptions = Recurly_SubscriptionList::getActive(['per_page' => 200]);

$count = 0;
foreach ($subscriptions as $i => $subscription) {

	$uuIdToSubscription[$subscription->uuid] = $subscription;
	
	$count++;
	if ($count > 190) {
		//break;
	}
}
//die("Kommer aldrig hit");
/*$query = "select company_id, count(*) cnt from people where sent_to_crawler_date > '2015-05-14' and crawled_by_id != 0 group by company_id;";
$result = mysql_query($query);
$visitsLast7Days = array();
while($data = mysql_fetch_array($result)){
	$visitsLast7Days[$data["company_id"]] = $data["cnt"];
	
}*/
$getFilter ="";
if (isset($_GET["show_frozen"])) {
	$getFilter = "&show_frozen=true";
}


echo "</div>";


$query = "SELECT *,
				rc_subscription.uuId,
				company.id as company_id,
				(SELECT count(*) FROM search_url WHERE search_url.company_id = company.id)  as search_url_count,
				(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0) as li_user_count,
				last_visit as last_activity,
				DATE_SUB(NOW(), INTERVAL 15 MINUTE) as last15Minutes,
				DATE_SUB(NOW(), INTERVAL 240 MINUTE) as last4Hours
				FROM company LEFT JOIN user ON assignedTo = user.id 
				LEFT JOIN rc_subscription ON rc_subscription.company_id = company.id 
				WHERE 
					company_name NOT LIKE '%NotInUse' AND 
					deleted = 0 AND 
					company.state = '' AND
					(SELECT count(*) FROM li_users WHERE li_users.company_id = company.id and li_users.hidden = 0) > 1";
if($_USER_DATA["admin"] != 1){
	$query .= " AND assignedTo = '$_USER_DATA[id]'";
}
if (isset($_GET["show_frozen"])) {
	$query .= " AND frozen = 1";
} else {
	$query .= " AND frozen = 0";
}
if(isset($_GET["sortBy"])){
	if ($_GET["sortBy"] == "added") {
		$query .= " ORDER BY company.id";
	}
	if ($_GET["sortBy"] == "profilesVisited") {
		$query .= " ORDER BY people_count";
	}
	if ($_GET["sortBy"] == "totalUsers") {
		$query .= " ORDER BY li_user_count";
	}
	if ($_GET["sortBy"] == "name") {
		$query .= " ORDER BY company.company_name";
	}
	if ($_GET["sortBy"] == "lastActive") {
		$query .= " ORDER BY last_activity";
	}
	if ($_GET["sortBy"] == "freeAccount") {
		$query .= " ORDER BY subscription_plan_code";
	}

	if ($_GET["sortBy"] == "totalVisits") {
		$query .= " ORDER BY people_count";
	}
	if (isset($_GET["direction"]) and $_GET["direction"] == "desc") {
		$query .= " DESC";
	}
}else{
	$query .= " ORDER BY company.id DESC";
}

	/*			   
				   
$query = "SELECT *,(SELECT count(*) FROM search_url WHERE company_id = company.id) as search_url_count,
				   (SELECT count(*) FROM people WHERE company_id = company.id and people.name != '') as people_count,
				   (SELECT sent_to_crawler FROM people WHERE people.company_id = company.id AND sent_to_crawler > 0 ORDER BY people.id DESC LIMIT 1) as last_activity,
				   DATE_SUB(NOW(), INTERVAL 60 MINUTE) as last15Minutes FROM company WHERE deleted = 0";


*/


$result = mysql_query($query);
echo "<table width=100% class='pure-table'  border=1>";
echo "
<thead>
<tr><td><b>ID</b></td><td><b>Company</b></td><td><b>Email</b></td><td><b>User Count</b></td><td><b>Subscription</b><td><b>Last Activity</b></td></td></td>

</tr>
</thead>
<tbody>
";
$urlCount = 0;
$profileCount = 0;
$searchUrlCountTotal = 0;
$searchScoreCountTotal = 0;
$i=0;
while($data = mysql_fetch_array($result)){
	$i++;
	
	if (isset($uuIdToSubscription[$data["uuid"]])) {
		if (
			stripos($uuIdToSubscription[$data["uuid"]]->plan->name,"1 User") !== false or
			(stripos($uuIdToSubscription[$data["uuid"]]->plan->name,"5 User") !== false and $data["li_user_count"] > 5)
			) {
		echo "<tr>
		<td>".$data["company_id"]."</td>
		<td><a href=/dashboard.php?company_id=$data[company_id]>".$data["company_name"]."</a></td>
		<td>".$data["email"]."</td>
		<td>".$data["li_user_count"]."</td>
		<td>".$uuIdToSubscription[$data["uuid"]]->plan->name."</td>";
		
		
		
		if($data["last_activity"] > $data["last15Minutes"]){
			//<b><font color=DarkGreen>&lt;5&nbsp;minutes</a></b><br>(
			echo "<td width=100><b><font color=DarkGreen>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
		}else if($data["last_activity"] > $data["last4Hours"]){
			//<b><font color=DarkOrange>&lt;4&nbsp;hours</a></b><br>(
			echo "<td width=100><b><font color=DarkOrange>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
		}else if($data["last_activity"] == ""){
			echo "<td></td>";
		}else{
			if($data["last_activity"] == "0000-00-00 00:00:00"){
				echo "<td></td>";
			}else{
				echo "<td width=100>".date("M d Y", strtotime($data["last_activity"]))."</td>";
			}
			
		}
		echo "</tr>";
		}
	}
	/*
	$searchCount = 0;
	$searchScoreCount = 0;
	$urlScore = "";

	$searchUrlCountTotal += $searchCount;
	$searchScoreCountTotal += $searchScoreCount;
	
	$newVbRate = 0;
	
	$css="";
	if($i%2==0){
		$css = ' class="pure-table-odd"';
	}
	echo "<tr $css><td>".$data["company_id"];
	
	if ($data["frozen"]) {
		echo "&nbsp;-&nbsp;<b>Frozen</b><br><a href=/list_companies.php?unfreeze=".$data["company_id"]."><font color=gray>Unfreeze</font></a>";
	} else {
		echo "&nbsp;-&nbsp;<a href=/list_companies.php?freeze=".$data["company_id"]."><font color=gray>Freeze</font></a>";
	}
	

	
	echo "</td>";
	
	if ($data["track"]) {
		echo "<td><a href='/list_html.php?company_id=".$data["company_id"]."'>HTML</a>&nbsp;-&nbsp;<a href='/analyze_html.php?company_id=".$data["company_id"]."'>Analyze</a>&nbsp;-&nbsp;<a href='/auto_generate.php?company_id=".$data["company_id"]."'>Generated</a></td>";
	} else {
		echo "<td><a href=list_companies.php?track=".$data["company_id"]."><font color=gray>Turn on</font></a></td>";
	}
	
	echo"<td width=400><u><a href=/dashboard.php?company_id=".$data["company_id"].">".$data["company_name"]."</a></u>
		- ".$data["email"]." - <a href=\"/edit_company.php?edit_company_id=".$data['company_id']."\"><u>Edit</u></a>
	</td>";
	
	
	//-<a href=/dashboard.php?company_id=".$data["company_id"].">Campaign</a>
	if($_USER_DATA["admin"]){
		
		
		echo "<td><a href=delete.php?company_id=".$data["company_id"]."&delete=true>Delete</a></td>";
	}
	
	
	
	echo "<td align=right>".number_format($data["search_url_count"])."</td><td align=right>".number_format($data["people_count"])."</td><td align=right>".$urlScore."</td>";
	
	
	if($_USER_DATA["admin"]){
		if(isset($_GET["calculate_vb_rate"])){
			if($vbRate > 0){
				echo "<td align=right><a href=vb_company_score.php?company_id=".$data["company_id"].">".number_format($vbRate*100,2)."% - $newVbRate </a></td>";
			}else{
				echo "<td></td>";
			}
			if($vbRate > 0 and $urlScore > 0){
				$overallScore = number_format($vbRate*$urlScore*10,1);
				echo "<td align=right>$overallScore</td>";
			}else{
				echo "<td></td>";
			}
		}else{
			//echo "<td colspan=2><a href=list_companies.php?calculate_vb_rate=true>Calculate View-back Rate and Prospecting Efficiency</a></td>";
			if($newVbRate > 0){
				echo "<td align=right><a href=vb_company_score.php?company_id=".$data["company_id"].">".number_format($newVbRate*100,2)."%</a></td>";
			}else{
				echo "<td></td>";
			}
			if($newVbRate > 0 and $urlScore > 0){
				$overallScore = number_format($newVbRate*$urlScore*10,1);
				echo "<td align=right>$overallScore</td>";
			}else{
				echo "<td></td>";
			}
		}
	}
	if (isset($visitsLast7Days[$data["company_id"]])) {
		echo "<td align=right>".$visitsLast7Days[$data["company_id"]]."</td>";
	} else {
		echo "<td>".$data['phone']."</td>";
	}
	
	
		echo "<td align=right>".$data["li_user_count"]."</td>";
		echo "<td width=300>";
		if (isset($uuIdToSubscription[$data["uuid"]])) {
			echo $uuIdToSubscription[$data["uuid"]]->plan->name;
			echo " - $".round($uuIdToSubscription[$data["uuid"]]->unit_amount_in_cents*0.01);
			
			$uuIdToSubscription[$data["uuid"]] = "";
		} else if ($data["subscription_plan_code"] == "noLimit"){
			echo "Free Unlimited Usage";
		} else if ($data["subscription_plan_code"] == "free"){
			echo "50 Visits Per Day";
		}
		echo "</td>";
	
	
	//echo "<td>".$vbData["profiles_visited"]."</td>";
	
	if($data["last_activity"] > $data["last15Minutes"]){
		//<b><font color=DarkGreen>&lt;5&nbsp;minutes</a></b><br>(
		echo "<td width=100><b><font color=DarkGreen>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
	}else if($data["last_activity"] > $data["last4Hours"]){
		//<b><font color=DarkOrange>&lt;4&nbsp;hours</a></b><br>(
		echo "<td width=100><b><font color=DarkOrange>".date("M d Y", strtotime($data["last_activity"]))."</font></b></td>";
	}else if($data["last_activity"] == ""){
		echo "<td></td>";
	}else{
		if($data["last_activity"] == "0000-00-00 00:00:00"){
			echo "<td></td>";
		}else{
			echo "<td width=100>".date("M d Y", strtotime($data["last_activity"]))."</td>";
		}
		
	}
	
	$urlCount += $data["search_url_count"];
	$profileCount += $data["people_count"];
	*/
}

echo "</tbody></table>";


echo "</div>";
/*
echo "<h2>Subscription with no match</h2>";
foreach ($uuIdToSubscription as $subscription) {
	if ($subscription != "") {
		echo "<pre>";
		print_r($subscription->account);
		echo "</pre>";
	}
}*/
?>

<?
require_once('footer.php');

?>
