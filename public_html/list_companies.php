<?php
    global $_USER_DATA;
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    $HTML_TITLE = "List Companies";
    require_once ('lib/recurly.php');
    require_once ('lib/recurly_config.php');
    require_once('header.php');

    if(!$_USER_DATA["admin"]){
        die("You don't have permission to see this page");
    }
//-----------------Actions----------------------------------------------------------------------------------------------
    if (isset($_GET['reload_recurly_data'])) {
        require_once ('cron/update_recurly_report.php');
        redirect('list_companies.php');
    }

    if (isset($_GET['enable_query_slicer'])) {
        DB::prep("UPDATE company SET company_runs_query_slicer = 1 WHERE id = ?")->execute([$_GET['enable_query_slicer']]);
        redirect('list_companies.php');
    }
    if (isset($_GET['disable_query_slicer'])) {
        DB::prep("UPDATE company SET company_runs_query_slicer = 0 WHERE id = ?")->execute([$_GET['disable_query_slicer']]);
        redirect('list_companies.php');
    }

//    if(isset($_POST["delete_company"]) AND $_USER_DATA["admin"]){
//        $query = "UPDATE company SET company_name = CONCAT(company_name,' is deleted',NOW()), deleted = 1 WHERE id = '".mysql_real_escape_string($_POST["delete_company"])."'";
//        if(mysql_query($query)){
//            echo "<h1>Company Was Deleted</h1>";
//        }
//    }

    if(isset($_GET["track"]) AND $_USER_DATA["admin"]){
        $query = "UPDATE company SET track = 1 WHERE id = '".mysql_real_escape_string($_GET["track"])."'";
        if(mysql_query($query)){
            echo "<h1>Company Is Being Tracked</h1>";
        }
    }

    if(isset($_GET["freeze"]) AND $_USER_DATA["admin"]){
        $query = "UPDATE company SET frozen = 1 WHERE id = '".mysql_real_escape_string($_GET["freeze"])."'";
        if(mysql_query($query)){
            echo "<h1>Company Is Frozen</h1>";
        }
    }

    if(isset($_GET["unfreeze"]) AND $_USER_DATA["admin"]){
        $query = "UPDATE company SET frozen = 0 WHERE id = '".mysql_real_escape_string($_GET["unfreeze"])."'";
        if(mysql_query($query)){
            echo "<h1>Company Is Unfrozen</h1>";
        }
    }

    if(isset($_POST["company_name"])){
        $assignedTo = 0;
        if(!$_USER_DATA["admin"]){
            $assignedTo = $_USER_DATA["id"];
        }
        $query = "INSERT INTO company (company_name,company_password,assignedTo) VALUES ('".mysql_real_escape_string($_POST["company_name"])."','".mysql_real_escape_string(substr(md5($_POST["company_name"]."fewwf few"),0,8))."','$assignedTo')";
        if(mysql_query($query)){

        }else{
            echo "<h1>Could not add company.<h2>";
        }
    }
    if($_USER_DATA["admin"] AND isset($_GET["assignCompany"]) AND isset($_GET["assignTo"])){
        $query = "UPDATE company SET assignedTo = '".mysql_real_escape_string($_GET["assignTo"])."' WHERE id = '".mysql_real_escape_string($_GET["assignCompany"])."'";
        mysql_query($query);
    }
//----------pager-------------------------------------------------------------------------------------------------------
    $page = (isset($_GET['page']) && intval($_GET['page'])) ? intval($_GET['page']) : 1;

    $limit = 200;
    $offset = $page > 0 ? ($page - 1) * $limit : 0;



//-----------filters----------------------------------------------------------------------------------------------------
    $getFilter ="";
    $queryFilters = '';
    if (isset($_GET["frozen"])) {
        $getFilter .= "&frozen=on";
        $queryFilters .= " OR c.frozen = 1";
    }
    if (isset($_GET["no_subscription"])) {
        $getFilter .= "&no_subscription=on";
        $queryFilters .= " OR RCAR.has_active_subscription = 0";
    }
    if (isset($_GET["no_account"])) {
        $getFilter .= "&no_account=on";
        $queryFilters .= " OR RCAR.account_code IS NULL";
    }
    if (isset($_GET["inactive_account"])) {
        $getFilter .= "&inactive_account=on";
        $queryFilters .= " OR RCAR.state <> 'active'";
    }
    if (isset($_GET["past_due"])) {
        $getFilter .= "&past_due=on";
        $queryFilters .= " OR RCAR.has_past_due_invoice = 1";
    }

    if($queryFilters){
        $queryFilters = ' AND (false '.$queryFilters.')';
    }

    if($_USER_DATA["admin"] != 1){
        $queryFilters .= " AND c.assignedTo = '$_USER_DATA[id]'";
    }
    if (!isset($_GET["frozen"])) {
        $queryFilters .= " AND c.frozen = 0";
    }
    if (isset($_GET["search"])) {
        $getFilter .= "&search=".$_GET["search"];
        $queryFilters .= " AND ( 
            c.company_name LIKE '%".$_GET["search"]."%'
            OR c.id = ".intval($_GET["search"])."
            OR c.email LIKE '%".$_GET["search"]."%'
            OR c.first_name LIKE '%".$_GET["search"]."%'
            OR c.last_name LIKE '%".$_GET["search"]."%'
            OR c.phone LIKE '%".$_GET["search"]."%'
            OR c.recurlyCoupon LIKE '%".$_GET["search"]."%'
            OR RCAR.account_code LIKE '%".$_GET["search"]."%'
            OR RCAR.email LIKE '%".$_GET["search"]."%'
            OR RCAR.first_name LIKE '%".$_GET["search"]."%'
            OR RCAR.last_name LIKE '%".$_GET["search"]."%'
            OR RCAR.company_name LIKE '%".$_GET["search"]."%'
        )";
    }



//-----------sort-------------------------------------------------------------------------------------------------------
    $getOrder ="";
    $orderBy = '';
    if(isset($_GET["sortBy"])){
        if ($_GET["sortBy"] == "added") {
            $orderBy .= " c.id";
        }
        if ($_GET["sortBy"] == "profilesVisited") {
            $orderBy .= " c.people_count";
        }
        if ($_GET["sortBy"] == "totalUsers") {
            $orderBy .= " li_user_count";
        }
        if ($_GET["sortBy"] == "totalVisitors") {
            $orderBy .= " visitors_count";
        }
        if ($_GET["sortBy"] == "name") {
            $orderBy .= " c.company_name";
        }
        if ($_GET["sortBy"] == "lastActive") {
            $orderBy .= " last_activity";
        }
        if ($_GET["sortBy"] == "freeAccount") {
            $orderBy .= " c.subscription_plan_code";
        }
        if ($_GET["sortBy"] == "last_page_view_viewgen") {
            $orderBy .= " c.last_page_view_viewgen";
        }
        if ($_GET["sortBy"] == "last_page_view_searchquant") {
            $orderBy .= " c.last_page_view_searchquant";
        }
        if ($_GET["sortBy"] == "totalVisits") {
            $orderBy .= " c.people_count";
        }
        if (isset($_GET["direction"]) and $_GET["direction"] == "desc") {
            $orderBy .= " DESC";
        }
        $getOrder .= "&sortBy=".$_GET["sortBy"]."&direction=".$_GET["direction"];
    }else{
        $orderBy .= " c.id DESC";
    }
    $orderBy = $orderBy ? ' ORDER BY '.$orderBy : '';


    $selectSelect = "
      c.*,
      RCAR.account_code AS rc_account_code,
      RCAR.state AS rc_account_state,
      RCAR.has_active_subscription AS rc_account_has_active_subscriptions,
      RCAR.has_past_due_invoice AS rc_account_has_past_due,
      c.id as company_id,
      (SELECT count(*) FROM search_url WHERE search_url.company_id = c.id)  as search_url_count,
      (SELECT count(*) FROM li_users WHERE li_users.company_id = c.id and li_users.hidden = 0) as li_user_count,
      (SELECT sum(total_visitors) FROM campaign WHERE campaign.company_id = c.id) as visitors_count,
      c.last_visit as last_activity,
      DATE_SUB(NOW(), INTERVAL 15 MINUTE) as last15Minutes,
      DATE_SUB(NOW(), INTERVAL 240 MINUTE) as last4Hours
    ";

    function getQuery($select = 'count(*)', $filters = '', $sort = '', $group = '', $limit = ''){
        return "
            SELECT
             ".$select."
            FROM company c 
              LEFT JOIN rc_account
                ON rc_account.company_id = c.id
              LEFT JOIN rc_account_report RCAR 
                ON RCAR.account_code = c.recurlyAccountCode
                   OR RCAR.account_code = c.email
                   OR RCAR.account_code = rc_account.account_code
            WHERE 
              c.company_name NOT LIKE '%NotInUse'
              AND c.deleted = 0
              AND c.state = ''
              AND c.is_test = 0
      ".$filters.$sort.$group.$limit;
    }

        /*

    $query = "SELECT *,(SELECT count(*) FROM search_url WHERE company_id = company.id) as search_url_count,
                       (SELECT count(*) FROM people WHERE company_id = company.id and people.name != '') as people_count,
                       (SELECT sent_to_crawler FROM people WHERE people.company_id = company.id AND sent_to_crawler > 0 ORDER BY people.id DESC LIMIT 1) as last_activity,
                       DATE_SUB(NOW(), INTERVAL 60 MINUTE) as last15Minutes FROM company WHERE deleted = 0";


    */
    $query = DB::prep(getQuery('count(*)' , $queryFilters));
    $query->execute();
    $pages = ceil($query->fetchColumn() / $limit);

    $query = DB::prep(getQuery('sum((SELECT count(*) FROM search_url WHERE search_url.company_id = c.id)) as search_url_count, sum(c.people_count) as people_count' , $queryFilters, '', '  '));
    $query->execute();
    $total = $query->fetch();

    $query = DB::prep("
        SELECT
          RCAR.*
        FROM rc_account_report RCAR 
          LEFT JOIN rc_account
            ON RCAR.account_code = rc_account.account_code
          LEFT JOIN company c 
            ON RCAR.account_code = c.recurlyAccountCode
               OR RCAR.account_code = c.email
               OR rc_account.company_id = c.id
        WHERE 
          c.id IS NULL
    ");
    $query->execute();
    $unmatchedAccounts = $query->fetchAll();


    $result = DB::prep(getQuery($selectSelect, $queryFilters, $orderBy, '', ' LIMIT '.$limit.' OFFSET '.$offset));
    $result->execute();


    /**
     * calculate subscription price regarding of users count
     * result in cents
     *
     * @param int $usersCount
     *
     * @return mixed int when success or null in case users count < 1
     *
     * @TODO use this function from api/functions.php
     */
    function calcSubscriptionPrice($usersCount){
        $result = null;
        if($usersCount > 0){
            if($usersCount > 10) {
                $usersCount = 10;
            }
            $result = (200 - ($usersCount - 1) * 10) * 100;
        }

        return $result;
    }

?>
    <div>
        <table width=700>
            <tr>
                <td width=400 align="center">
                    Sort by Date Added:
                    <a href=list_companies.php?sortBy=added<?=$getFilter?>><u>Oldest</u></a>
                    <a href=list_companies.php?sortBy=added&direction=desc<?=$getFilter?>><u>Newest</u></a>
                    <br>
                    Sort by Company Name:
                    <a href=list_companies.php?sortBy=name$getFilter><u>A-Z</u></a>
                    <a href=list_companies.php?sortBy=name&direction=desc<?=$getFilter?>><u>Z-A</u></a>
                    <br>
                    Sort by Company Total Visits:
                    <a href=list_companies.php?sortBy=totalVisits<?=$getFilter?>><u>Least</u></a>
                    <a href=list_companies.php?sortBy=totalVisits&direction=desc<?=$getFilter?>><u>Most</u></a>
                    <br>
                    Sort by Company Total Return profile visits:
                    <a href=list_companies.php?sortBy=totalVisitors<?=$getFilter?>><u>Least</u></a>
                    <a href=list_companies.php?sortBy=totalVisitors&direction=desc<?=$getFilter?>><u>Most</u></a>
                    <br>
                    Sort by Last Active:
                    <a href=list_companies.php?sortBy=lastActive$<?=$getFilter?>><u>Least Recent</u></a>
                    <a href=list_companies.php?sortBy=lastActive&direction=desc<?=$getFilter?>><u>Most Recent</u></a>
                    <br>

                    Sort by Total Users:
                    <a href=list_companies.php?sortBy=totalUsers<?=$getFilter?>><u>Least</u></a>
                    <a href=list_companies.php?sortBy=totalUsers&direction=desc<?=$getFilter?>><u>Most</u></a>
                    <br>
                    Sort by Profiles Visited:
                    <a href=list_companies.php?sortBy=profilesVisited<?=$getFilter?>><u>Least</u></a>
                    <a href=list_companies.php?sortBy=profilesVisited&direction=desc<?=$getFilter?>><u>Most</u></a>
                    <br>
                    Sort by Free Account:
                    <a href=list_companies.php?sortBy=freeAccount&direction=desc<?=$getFilter?>><u>Free First</u></a>
                    <a href=list_companies.php?sortBy=freeAccount<?=$getFilter?>><u>Free Last</u></a>
                    <br>

                    Sort by Last SearchQuant Page View:
                    <a href=list_companies.php?sortBy=last_page_view_searchquant<?=$getFilter?>><u>Oldest</u></a>
                    <a href=list_companies.php?sortBy=last_page_view_searchquant&direction=desc<?=$getFilter?>><u>Newest</u></a>
                    <br>
                    Sort by Last ViewGen Page View:
                    <a href=list_companies.php?sortBy=last_page_view_viewgen<?=$getFilter?>><u>Oldest</u></a>
                    <a href=list_companies.php?sortBy=last_page_view_viewgen&direction=desc<?=$getFilter?>><u>Newest</u></a>
                    <br>
                </td>
                <td valign=top width=300 align=center>
                    <form method="get" style="border: medium">
                        <input type="checkbox" name="frozen" id="frozen" <?php if(isset($_GET["frozen"])): ?>checked="checked"<?php endif; ?>/><label for="frozen">Frozen</label>
                        <input type="checkbox" name="no_subscription" id="no_subscription" <?php if(isset($_GET["no_subscription"])): ?>checked="checked"<?php endif; ?>/><label for="no_subscription">No active subscription</label>
                        <input type="checkbox" name="no_account" id="no_account" <?php if(isset($_GET["no_account"])): ?>checked="checked"<?php endif; ?>/><label for="no_account">No recurly account</label>
                        <input type="checkbox" name="inactive_account" id="inactive_account" <?php if(isset($_GET["inactive_account"])): ?>checked="checked"<?php endif; ?>/><label for="inactive_account">Inactive recurly account</label>
                        <input type="checkbox" name="past_due" id="past_due" <?php if(isset($_GET["past_due"])): ?>checked="checked"<?php endif; ?>/><label for="past_due">Has past due invoice</label>
                        <input type="search" name="search" id="search" title="search" <?php if(isset($_GET["search"])): ?>value="<?=$_GET["search"]?>"<?php endif; ?>/>
                        <input type="submit" title="Apply filter" value="Apply filter">
                    </form>
                </td>
                <td>
                    <a href=list_companies.php?reload_recurly_data=true><u>Reload Recurly data</u></a>
                </td>
            </tr>
        </table>

        <style>
            .rc_info {
                display: none;
            }
            .open .rc_info {
                display: block;
            }
            .open .rc_info_short {
                display: none;
            }

            .rc_info_short {
                cursor: pointer;
            }

            .js-close {
                cursor: pointer;
                display: block;
                font-weight: bold;
                color: grey;
                text-align: right;
                top: -5px;
                right: 2px;
                position: absolute;
            }

            .js-accardion-cell {
                position: relative;
            }

            .description_content {
                display: block;
                margin-left: 20px;
            }
        </style>

        <table width=100% class='pure-table js-table'  border=1>
            <thead>
                <tr>
                    <td>
                        <b>ID</b>
                    </td>
                    <td>
                        <b>Track</b>
                    </td>
                    <td>
                        <b>Company</b>
                    </td>
<!--                    --><?php //if($_USER_DATA["admin"]): ?>
<!--                        <td>-->
<!--                            <b>Manage</b>-->
<!--                        </td>-->
<!--                    --><?php //endif; ?>
                    <td>
                        <b>Search URLs</b>
                    </td>
                    <td>
                        <b>Visited Profiles</b>
                    </td>
                    <td>
                        <b>Return Profile Views</b>
                    </td>
<!--                    <td>-->
<!--                        <b>QS</b>-->
<!--                    </td>-->
<!--                    --><?php //if($_USER_DATA["admin"]): ?>
<!--                        <td>-->
<!--                            <b>VBR</b>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <b>PE</b>-->
<!--                        </td>-->
<!--                    --><?php //endif; ?>
                    <td>
                        <b>Phone</b>
                    </td>
                    <td>
                        <b>Total Users</b>
                    </td>
                    <td>
                        <b>Last Visit</b>
                    </td>
                    <td>
                        <b>Created</b>
                    </td>
                    <td>
                        <b>SQ Page View</b>
                    </td>
                    <td>
                        <b>VG Page View</b>
                    </td>
                    <td>
                        <b>Auto Slicer</b>
                    </td>
                    <td>
                        <b>Recurly</b>
                    </td>
                    <td>
                        <b>Can be charged more</b>
                    </td>
                </tr>
            </thead>
            <tbody>

<?php
    $urlCount = 0;
    $profileCount = 0;
    $searchUrlCountTotal = 0;
    $searchScoreCountTotal = 0;
    $i=0;
    while($data = $result->fetch()){
        $i++;

        $searchCount = 0;
        $searchScoreCount = 0;
        $urlScore = "";
        /*$urlQuery = "SELECT * FROM search_url WHERE company_id = $data[company_id]";
        $urlResult = mysql_query($urlQuery);

        while($urlData = mysql_fetch_array($urlResult)){

            $searchCount++;
            $searchScoreCount += getScore($urlData["url"],false);
        }

        if($searchCount > 0){
            $urlScore = number_format(($searchScoreCount/$searchCount),1);
        }*/
        $searchUrlCountTotal += $searchCount;
        $searchScoreCountTotal += $searchScoreCount;

        $newVbRate = 0;
        $vbRate = 0;
        /*$vbQuery = "SELECT sum(visitor_count) as visitor_count,sum(profiles_visited) as profiles_visited FROM visitors WHERE company_id = $data[company_id] AND visitor_count > 0 AND profiles_visited > 100";
        $vbResult = mysql_query($vbQuery);
        $vbData = mysql_fetch_array($vbResult);
        $newVbRate = 0;
        if($vbData["profiles_visited"] > 0 AND $vbData["visitor_count"] > 0){
            $newVbRate = $vbData["visitor_count"]/$vbData["profiles_visited"];
        }



        if(isset($_GET["calculate_vb_rate"])){
            $vbRate = getVbRate($data["company_id"]);
        }else{
            $vbRate = 0;
        }*/

        $querySubscriptions = DB::prep("
          SELECT rcsr.* 
          FROM rc_subscription_report rcsr
            JOIN rc_account_report rcar 
              ON rcar.id = rcsr.rc_account_report_id
          WHERE
            rcar.account_code = ?
        ");
        $querySubscriptions->execute([$data['rc_account_code']]);
        $subscriptions = $querySubscriptions->fetchAll();

?>
                <tr <?=$i%2==0 ? ' class="pure-table-odd"' : '' ?>>
                    <td>
                        <?=$data["company_id"]?>

                        <?php if ($data["frozen"]): ?>
                            &nbsp;-&nbsp;<b>Frozen</b>
                            <br>
                            <a href=list_companies.php?unfreeze=<?=$data["company_id"]?>>
                                <span style="color:gray">Unfreeze</span>
                            </a>
                        <?php else: ?>
                            &nbsp;-&nbsp;<a href=list_companies.php?freeze=<?=$data["company_id"]?>><span style="color:gray">Freeze</span></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($data["track"]): ?>
                            <a href='/newAdmin/html_dumps.php?companyId=<?=$data["company_id"]?>'>HTML</a>&nbsp;-&nbsp;
                            <a href='analyze_html.php?company_id=<?=$data["company_id"]?>'>Analyze</a>&nbsp;-&nbsp;
                            <a href='auto_generate.php?company_id=<?=$data["company_id"]?>'>Generated</a>
                        <?php else: ?>
                            <a href=list_companies.php?track=<?=$data["company_id"]?>>
                                <span style="color:gray">Turn on</span>
                            </a>
                        <?php endif; ?>
                    </td>
                    <td width=400>
                        <u>
                            <a href="new-ui/campaign_builder.php?company_id=<?=$data["company_id"]?>"><?=$data["company_name"]?></a>
                        </u>
                        - <?=$data["email"]?> -
                        <a href="admin/edit_company.php?company_id=<?=$data["company_id"]?>">
                            <u>Edit</u>
                        </a>
                    </td>
    <!--	//-<a href=/dashboard.php?company_id=".$data["company_id"].">Campaign</a>-->
<!--                    --><?php //if($_USER_DATA["admin"]): ?>
    <!--		/*echo "<td><a href=assign.php?company_id=".$data["company_id"].">";-->
    <!--		if($data["user_name"] == ""){-->
    <!--			echo "<i>Assign</i>";-->
    <!--		}else{-->
    <!--			echo $data["user_name"];-->
    <!--		}-->
    <!--		echo "</a></td>";*/-->
<!--                        <td>-->
<!--                            <a href=delete.php?company_id=--><?//=$data["company_id"]?><!--&delete=true>Delete</a>-->
<!--                        </td>-->
<!--                    --><?php //endif; ?>
                    <td align=right>
                        <?=number_format($data["search_url_count"])?>
                    </td>
                    <td align=right>
                        <?=number_format($data["people_count"])?>
                    </td>
                    <td align=right>
                        <?=number_format($data["visitors_count"])?>
                    </td>
<!--                    <td align=right>-->
<!--                        --><?//=$urlScore?>
<!--                    </td>-->
<!--                    --><?php //if($_USER_DATA["admin"]): ?>
<!--                        --><?php //if(isset($_GET["calculate_vb_rate"])): ?>
<!--                            <td align=right>-->
<!--                                --><?php //if($vbRate > 0): ?>
<!--                                    <a href=vb_company_score.php?company_id=--><?//=$data["company_id"]?><!-- >-->
<!--                                        --><?//=number_format($vbRate*100,2)?><!--% - --><?//=$newVbRate?>
<!--                                    </a>-->
<!--                                --><?php //endif; ?>
<!--                            </td>-->
<!--                            <td align=right>-->
<!--                                --><?php //if($vbRate > 0 and $urlScore > 0): ?>
<!--                                    --><?//=number_format($vbRate*$urlScore*10,1)?>
<!--                                --><?php //endif; ?>
<!--                            </td>-->
<!--                        --><?php //else: ?>
<!--                            <td colspan=2>-->
<!--                                <a href=list_companies.php?calculate_vb_rate=true>Calculate View-back Rate and Prospecting Efficiency</a>-->
<!--                            </td>-->
<!--                            <td align=right>-->
<!--                                --><?php //if($newVbRate > 0): ?>
<!--                                    <a href=vb_company_score.php?company_id=--><?//=$data["company_id"]?><!-- >-->
<!--                                        --><?//=number_format($newVbRate*100,2)?><!--%-->
<!--                                    </a>-->
<!--                                --><?php //endif; ?>
<!--                            </td>-->
<!--                            <td align=right>-->
<!--                                --><?php //if($newVbRate > 0 and $urlScore > 0): ?>
<!--                                    --><?//=number_format($newVbRate*$urlScore*10,1)?>
<!--                                --><?php //endif; ?>
<!--                            </td>-->
<!--                        --><?php //endif; ?>
<!--                    --><?php //endif; ?>
                    <td align=right>
<!--                        --><?php //if(isset($visitsLast7Days[$data["company_id"]])): ?>
<!--                            --><?//=$visitsLast7Days[$data["company_id"]]?>
<!--                        --><?php //else: ?>
                            <?=$data['phone']?>
<!--                        --><?php //endif; ?>
                    </td>
                    <td align=right>
<!--                        --><?php //if ($data["li_user_24h"] > 0 ): ?>
<!--                            <span style="color:darkgreen">-->
<!--                                <b>-->
<!--                                    --><?//=$data["li_user_24h"]?><!-- new 24h-->
<!--                                </b>-->
<!--                            </span>-->
<!--                        --><?php //endif; ?>
<!--                        --><?php //if ($data["li_user_7_days"] > 0 ): ?>
<!--                            <span style="color:darkorange">-->
<!--                                <b>-->
<!--                                    --><?//=$data["li_user_7_days"]?><!-- new over 7 days-->
<!--                                </b>-->
<!--                            </span>-->
<!--                        --><?php //endif; ?>
<!--                        --><?php //if ($data["new_ui_user_count"] > 0 ): ?>
<!--                            <span style="color:darkred">-->
<!--                                <b>-->
<!--                                    --><?//=$data["new_ui_user_count"]?><!-- USER WITH NEW UI-->
<!--                                </b>-->
<!--                            </span>-->
<!--                        --><?php //endif; ?>
                        <?=$data["li_user_count"]?>
                    </td>
    <!--	//echo "<td>".$vbData["profiles_visited"]."</td>";-->
                    <td width=100>
                        <?php
                            $last_activity_style = '';
                            if($data["last_activity"] > $data["last15Minutes"]){
                                $last_activity_style = 'color:DarkGreen';
                            } elseif($data["last_activity"] > $data["last4Hours"]){
                                $last_activity_style = 'color:DarkOrange';
                            }
                        ?>
                        <?php if($data["last_activity"] != "" && $data["last_activity"] != "0000-00-00 00:00:00"): ?>
                            <b>
                                <span style="<?=$last_activity_style?>">
                                    <?=date("M d Y", strtotime($data["last_activity"]))?>
                                </span>
                            </b>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($data["company_created"] != "0000-00-00 00:00:00"): ?>
                            <?=date("M d Y", strtotime($data["company_created"]))?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($data["last_page_view_searchquant"] != "0000-00-00 00:00:00"): ?>
                            <?=date("M d Y", strtotime($data["last_page_view_searchquant"]))?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($data["last_page_view_viewgen"] != "0000-00-00 00:00:00"): ?>
                            <?=date("M d Y", strtotime($data["last_page_view_viewgen"]))?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($data['company_runs_query_slicer']): ?>
                            <a href='list_companies.php?disable_query_slicer=<?=$data['id']?>' class='disable-query-slicer'>
                                <u>Disable</u>
                            </a>
                        <?php else: ?>
                            <a href='list_companies.php?enable_query_slicer=<?=$data['id']?>' class='enable-query-slicer'>
                                <u>Enable</u>
                            </a>
                        <?php endif; ?>
                    </td>
                    <td class="js-accardion-cell">
                        <span class="rc_info">
                            <span class="js-close">
                                X
                            </span>
                            <?php if($data['recurlyCoupon']): ?>
                                <span style="font-weight: bold">Coupon:</span><br/>
                                <span class="description_content">
                                    <?=$data['recurlyCoupon']?>
                                </span>
                            <?php endif; ?>

                            <span style="font-weight: bold">Account:</span><br/>
                            <span class="description_content">
                                Auto sync: <span style="color:<?=$data['recurly_sync'] != '1' ? 'red' : 'green' ?>"><?=$data['recurly_sync'] == '1' ? 'On' : 'Off' ?></span><br/>
                                <?php if($data['rc_account_code']): ?>
                                    <?=$data['rc_account_code']?> <br>
                                    <span style="color:<?=$data['rc_account_state'] != 'active' ? 'red' : 'green' ?>"><?=$data['rc_account_state']?></span><br/>
                                    <?php if($data['rc_account_has_past_due'] == '1'): ?>
                                        <span style="color:red">Past due invoice!</span><br/>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <span style="color:red">Account not found! <br/> <?=$data['recurlyAccountCode'] ? '('.$data['recurlyAccountCode'].')' : ''?></span><br/>
                                <?php endif; ?>
                            </span>
                            <span style="font-weight: bold">Subscriptions:</span><br/>
                            <span class="description_content">
                                <?php $activeSubscription = null ?>
                                <?php foreach ($subscriptions as $subscription): ?>
                                    <?php if($subscription['state'] == 'active' || $subscription['state'] == 'future') $activeSubscription = $subscription; ?>
                                    UUID: <?=$subscription['uuid']?><br/>
                                    Status: <span style="color:<?=$subscription['state'] == 'active' ? 'green' : ($subscription['state'] == 'future' ? 'yellow' : 'red') ?>"><?=$subscription['state']?></span><br/>
                                    Plan: <?=$subscription['plan_code']?><br/>
                                    Price: <?=$subscription['unit_amount_in_cents'] / 100?> <?=$subscription['currency']?><br/>
                                    Users: <?=$subscription['quantity']?><br/>
                                    Activated at: <?=$subscription['activated_at']?><br/>
                                    Canceled at: <?=$subscription['canceled_at']?><br/>
                                    Expires at: <?=$subscription['expires_at']?><br/>
                                    Current period started at: <?=$subscription['current_period_started_at']?><br/>
                                    Current period ends at: <?=$subscription['current_period_ends_at']?><br/>
                                    Trial started at: <?=$subscription['trial_started_at']?><br/>
                                    Trial ends at: <?=$subscription['trial_ends_at']?><br/>
                                    <br/>
                                <?php endforeach; ?>
                            </span>
                        </span>
                        <span class="rc_info_short" style="color:<?=($activeSubscription && $data['rc_account_state'] == 'active') ? 'green' : 'red' ?>">
                            <?php if($activeSubscription && $data['rc_account_state'] == 'active'): ?>
                                Active
                            <?php else: ?>
                                No active subscription
                            <?php endif; ?>
                            <?php if($data['rc_account_has_past_due'] == '1'): ?>
                                <span style="color:red">Past due invoice!</span><br/>
                            <?php endif; ?>
                        </span>
                    </td>
                    <td>
                        <?php if($activeSubscription && ($data['li_user_count'] > $activeSubscription['quantity'])): ?>
                            +<?=$data['li_user_count'] - $activeSubscription['quantity']?> users<br/>
                            New price: <?=calcSubscriptionPrice($data['li_user_count']) / 100?> <?=$activeSubscription['currency']?><br/>
                            Total: <?=calcSubscriptionPrice($data['li_user_count']) * $data['li_user_count'] / 100?> <?=$activeSubscription['currency']?>
                        <?php endif; ?>
                    </td>
                </tr>


<?php
        $urlCount += $data["search_url_count"];
        $profileCount += $data["people_count"];
    }

    $urlScore = 0;
    if($searchUrlCountTotal > 0){
        $urlScore = number_format(($searchScoreCountTotal/$searchUrlCountTotal),1);
    }
?>
                <tr>
                    <td colspan="2">
                        <b>Total</b>
                    </td>
                    <?php if($_USER_DATA["admin"]): ?>
                        <td></td>
<!--                        <td></td>-->
                    <?php endif; ?>
                    <td align=right>
                        <b><?=number_format($total['search_url_count'])?></b>
                    </td>
                    <td align=right>
                        <b>
                            <a href=visitsPerDay.php>
                                <?=number_format($total['people_count'])?>
                            </a>
                        </b>
                    </td>
<!--                    <td align=right>-->
<!--                        <b>--><?//=$urlScore?><!--</b>-->
<!--                    </td>-->
<!--                    --><?php //if($_USER_DATA["admin"]): ?>
<!--                        <td></td>-->
<!--                        <td></td>-->
<!--                    --><?php //endif; ?>
                    <td></td>
                </tr>
                <tr>
                    <td align="left" style="border-right: none">
                        <? if($page > 1) : ?>
                            <a href="?page=<?=$page - 1?><?=$getFilter.$getOrder?>">
                                << Prev
                            </a>
                        <? endif ?>
                    </td>
                    <td align="center" style="border-left: none; border-right: none">
                        <?=$page?>/<?=$pages?>
                    </td>
                    <td align="right" style="border-left: none">
                        <? if($page < $pages) : ?>
                            <a href="?page=<?=$page + 1?><?=$getFilter.$getOrder?>">
                                Next >>
                            </a>
                        <? endif ?>
                    </td>
                </tr>
            </tbody>
        </table>

    <!--/*-->
    <!--echo "<br><form action=/list_companies.php method=post class='pure-form'><fieldset>-->
    <!--<legend>Add New Company</legend>-->
    <!--<input type=text name=company_name  placeholder='Company Name'> <input type=submit value='Add Company'  class='pure-button pure-button-primary'></form>";-->
    <!--*/-->
    </div>

    <h2>Accounts with no match</h2>
    <table border=1>
        <thead>
            <tr>
                <td>
                    Account code
                </td>
                <td>
                    Email
                </td>
                <td>
                    First name
                </td>
                <td>
                    Last name
                </td>
                <td>
                    Company name
                </td>
                <td>
                    Created at
                </td>
                <td>
                    Updated at
                </td>
                <td>
                    Closed at
                </td>
                <td>
                    State
                </td>
                <td>
                    Info
                </td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($unmatchedAccounts as $account): ?>
                <tr>
                    <td>
                        <?=$account['account_code']?>
                    </td>
                    <td>
                        <?=$account['email']?>
                    </td>
                    <td>
                        <?=$account['first_name']?>
                    </td>
                    <td>
                        <?=$account['last_name']?>
                    </td>
                    <td>
                        <?=$account['company_name']?>
                    </td>
                    <td>
                        <?=$account['created_at']?>
                    </td>
                    <td>
                        <?=$account['updated_at']?>
                    </td>
                    <td>
                        <?=$account['closed_at']?>
                    </td>
                    <td>
                        <span style="color:<?=$account['state'] != 'active' ? 'red' : 'green' ?>"><?=$account['state']?></span>
                    </td>
                    <td>
                        <?php if($account['has_active_subscription'] == '0'): ?>
                            <span style="color:red">No active subscription!</span><br/>
                        <?php endif; ?>
                        <?php if($account['has_past_due_invoice'] == '1'): ?>
                            <span style="color:red">Past due invoice!</span><br/>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<script type="application/javascript">
    $('.js-table').on('click', function(event){
        var $target = $(event.target);
        if($target.parent().hasClass('js-accardion-cell') || $target.hasClass('js-accardion-cell')) {
            if(!$target.closest('.js-accardion-cell').hasClass('open')) {
                $target.closest('.js-accardion-cell').addClass('open')
            }
        }
        if($target.hasClass('js-close')) {
            $target.closest('.js-accardion-cell').removeClass('open');
        }
    })
</script>

<?
    require_once('footer.php');
?>
