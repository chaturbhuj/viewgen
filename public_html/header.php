<?php

include_once(__DIR__ . '/config/versions.php');
$cssVersion = \config\versions\ASSETS_VERSION;

ob_start(); // Because sometimes we do redirects after the header is printed.

include('force_https.php');

if (!isset($IGNORE_AUTH)) {
	require_once('authenticate.php');
	if(!$_USER_DATA["admin"]){
		//Vi ska bara registrera om man inte är admin
		$query = "UPDATE company SET last_page_view_viewgen = NOW() WHERE id = '".mysql_real_escape_string($_COMPANY_DATA["id"])."'";
		mysql_query($query);
	}
}

$company_id = (string)($_GET['company_id'] ?? $_COMPANY_DATA["id"] ?? '');

$title = "The Most Immediately Powerful, Low-Cost Sales Weapon Ever Devised";
if (isset($HTML_TITLE)) {
	$title = $HTML_TITLE;
}

if (!isset($STOPHEAD)) { ?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">

	<title><?=$title?></title>
    
    <?php include(__DIR__ . '/ui/include/favicon.php')?>
    
	<link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/assets/vendor/bootstrap3/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url('lib/recurly-theme/default/recurly.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/css/index.css?' . $cssVersion); ?>">
	<link rel="stylesheet" href="<?=base_url('assets/css/datepickr.css'); ?>">
	<link rel="stylesheet" href="<?=base_url('lib/owl/owl-carousel/owl.carousel.css')?>">
	<link rel="stylesheet" href="<?=base_url('lib/owl/owl-carousel/owl.theme.css')?>">
	<link rel="stylesheet" href="/assets/css/grid.css">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<script src="<?=base_url('amcharts/amcharts.js" type="text/javascript"')?>"></script>
	<script src="<?=base_url('amcharts/serial.js" type="text/javascript')?>"></script>
		
	<script type="application/javascript" src="<?=base_url('assets/js/jquery.min.js')?>"></script>
	<script type="application/javascript" src="<?=base_url('assets/js/datepickr.min.js')?>"></script>

    <script type="application/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script type="application/javascript" src="<?=base_url('assets/js/utilities.js?' . $cssVersion)?>"></script>

	<script type="application/javascript" src="<?=base_url('/assets/js/Chart.js') ?>"></script>
	<script type="application/javascript" src="<?=base_url('assets/js/app.js?' . $cssVersion)?>"></script>
	<script type="application/javascript" src="<?=base_url('assets/js/slider.js?' . $cssVersion)?>"></script>
	<script type="application/javascript" src="<?=base_url('assets/js/dynamicDashboard.js?' . $cssVersion)?>"></script>
	<script src="<?=base_url('assets/js/recurly.min.js')?>"></script>
	<script src="<?=base_url('lib/owl/owl-carousel/owl.carousel.js')?>"></script>
	
	<script type="text/javascript" language="javascript"> 
		var tl813v = 27340; 
		(function() { 
		var tl813 = document.createElement('script'); tl813.type = 'text/javascript'; tl813.async = true; 
		tl813.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '1.tl813.com/tl813.js'; 
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tl813, s); 
		})(); 
	</script>
	
	<script type="text/javascript" language="javascript">
		var timezone_offset = new Date().getTimezoneOffset();
		var localTime = new Date().toString();
	</script>
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-262737-14', 'auto');
  ga('send', 'pageview');

</script>
<?php
}

if(!isset($STOPMENU) && !isset($IGNORE_AUTH)) {
	$company_auth_name = "";

	if($company_id != ""){
		$query = "SELECT * FROM company WHERE id = '".mysql_real_escape_string($company_id)."'";
		$result = mysql_query($query);
		$data = mysql_fetch_array($result);

		$company_auth_name = $data["company_name"];
		$shownUsername = $company_auth_name;
		if($company_auth_name == ""){
			if(mb_strlen($shownUsername) > 18){
				$shownUsername = trim(mb_substr($shownUsername,0,15))."...";
				
			}
		}else{
			if(mb_strlen($shownUsername) > 18){
				$shownUsername = trim(mb_substr($shownUsername,0,15))."...";
			}
		}
	}
?>

<div id="header">
	<div class="container inner">
		<div class="row">
			<div class="col-xs-12">
				<a href="/" id="logo"></a>
				<ul id="nav">
					
					<?php if ($company_id != '') { ?>
					<li>
						<a class="button small orange<?=(isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "dashboard") ? ' active' : ''?>" href="<?=base_url('dashboard.php?company_id='.$company_id);?>">Campaigns</a>
						<? if (isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "dashboard") { ?>
							<i class="active-arrow"></i>
							<i class="active-arrow-outer"></i>
						<? } ?>
					</li>
					<li>
						<a class="button small orange<?=(isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "reports") ? ' active' : ''?>" href="<?=base_url('reports.php?company_id='.$company_id)?>">Reports</a>

						<? if (isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "reports") { ?>
							<i class="active-arrow"></i>
							<i class="active-arrow-outer"></i>
						<? } ?>
					</li>
					<li>
						<a class="button small orange<?=(isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") ? ' active' : ''?>" href="<?=base_url('referer.php?company_id='.$company_id)?>">Referrals</a>

						<? if (isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") { ?>
							<i class="active-arrow"></i>
							<i class="active-arrow-outer"></i>
						<? } ?>
					</li>
					<li>
						
						<?/*
						//Julian ska prata med Chris om att vi inte ska ha en blog
						<a class="button small orange<?=(isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") ? ' active' : ''?>" href="<?=base_url('blog')?>">Blog</a>
						*/?>
						<? if (isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") { ?>
							<i class="active-arrow"></i>
							<i class="active-arrow-outer"></i>
						<? } ?>
					</li>
					<li>
						<a target="_blank" class="button small orange<?=(isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") ? ' active' : ''?>" href="https://viewgen.zendesk.com">Help</a>

						<? if (isset($ACTIVE_HEADER_PAGE) && $ACTIVE_HEADER_PAGE == "referer") { ?>
							<i class="active-arrow"></i>
							<i class="active-arrow-outer"></i>
						<? } ?>
					</li>
					
					<?php } ?>
					
				</ul>
				
				
				<div class="username" style="text-decoration: underline;"> <?='Account' /*isset($shownUsername) ? $shownUsername : ''*/?> <div class="small-down-arrow"></div>
					<ul class="user-menu">
						<i class="inner-arrow"></i>
						<i class="outer-arrow"></i>
						<li><a href="<?=base_url('settings.php?company_id='.$company_id); ?>">Settings</a></li>
						<li><a href="<?=base_url('logout.php'); ?>">Log Out</a></li>
					</ul>
				</div>
				<div class="add-user-link">
					<? if (isset($data['new_add_user_page']) && $data["new_add_user_page"]) {?>
						<a href=/add_users.php?company_id=<?=$company_id?> >Add Users <div class="vertical-thin-line"></div></a>
					<? } else { ?>
						<a href=/change_plan.php?company_id=<?=$company_id?> >Add Users <div class="vertical-thin-line"></div></a>
					<? } ?>
				</div>

		<script type="text/javascript">
			$(function () {
				$('#header .username').click(function (event) {
					event.stopPropagation();
					var $box = $('.user-menu');
					if ($box.is(":visible")) {
						$box.fadeOut('fast');
					} else {
						$box.fadeIn('fast');
					}
				});
				$('body').click(function () {
					$('.user-menu').fadeOut('fast');
				});
			});
		</script>
			</div>
		</div>
	</div>
</div>
<div class="outer-page">
<?php
}
?>
