<?php
$HTML_TITLE = "User Activity";
require_once ('lib/recurly.php');
require_once('header.php');
if(!$_USER_DATA["admin"]){
	die("You don't have permission to see this page");
}

echo "<center>";

error_reporting(E_ALL);
ini_set('display_errors', '1');


$uuIdToSubscription = array();
$subscriptions = Recurly_SubscriptionList::getActive();
foreach ($subscriptions as $subscription) {
	
	$uuIdToSubscription[$subscription->uuid] = $subscription;

}


//	(SELECT sent_to_crawler_date FROM people WHERE crawled_by_id = li_users.linked_in_id ORDER BY sent_to_crawler_date DESC LIMIT 1) lastUserActivity,
$query = "SELECT company.company_name,
    li_users.last_connection,
	li_users.name li_name,
	rc_subscription.uuId AS uuid,
	company.id as company_id,
	company.last_visit as last_activity,
	NOW() currentTime,
	DATE_SUB(NOW(), INTERVAL 7 DAY) lastWeek,
	DATE_SUB(NOW(), INTERVAL 14 DAY) 14DaysAgo,
	DATE_SUB(NOW(), INTERVAL 1 MONTH) lastMonth,
	(SELECT MAX(sent_to_crawler) FROM people WHERE people.company_id = company.id AND people.crawled_by_id = li_users.linked_in_id) last_visit,
	client_versions.name AS client_version
	FROM company
	LEFT JOIN rc_subscription ON rc_subscription.company_id = company.id
	JOIN li_users ON li_users.company_id = company.id
	LEFT JOIN client_versions ON li_users.client_version_id = client_versions.id
	WHERE 
		company_name NOT LIKE '%NotInUse' AND 
		deleted = 0 AND company.state = '' AND 
		li_users.name != '' AND 
		li_users.hidden = 0
	ORDER BY last_activity ASC";
$result = mysql_query($query);	
echo "
Showing all none-hidden users in all companies with an active subscription sorted by last company activity
<br>
<table border=1><tr>
	<td>Company</td>
	<td>User</td>
	<td>Last Company Activity</td>
	<td>Last User Activity</td>
	<td>Last User Visit</td>
	<td>Client Version</td>
	<td>Company Subscription</td>

</tr>";
while($data = mysql_fetch_array($result)){
	if (isset($uuIdToSubscription[$data["uuid"]])) {
		echo "<tr>
		<td><a href=dashboard.php?company_id=$data[company_id]><u>$data[company_name]</u></a></td>
		<td>$data[li_name]</td>
		<td>$data[last_activity]</td>
		<td>$data[last_connection]</td>
		<td>";
		if ($data["last_visit"] == "") {
			echo "NEVER";
		}else if ($data["last_visit"] < $data["lastMonth"]) {
			echo "<font color=darkred><b>More than one month ago</b></font><br>";
		}else if ($data["last_visit"] < $data["lastWeek"]) {
			echo "<font color=darkorange><b>More than one week ago</b></font><br>";
		}
		echo "$data[last_visit]</td>
		<td>$data[client_version]</td>
		";
		echo "<td>";
		echo $uuIdToSubscription[$data["uuid"]]->plan->name;
		echo " - $".round($uuIdToSubscription[$data["uuid"]]->unit_amount_in_cents*0.01);
		
		echo "</td></tr>";
	}
}
echo "</table>";


?>