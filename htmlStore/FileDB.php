<?php

class FileDB {

	private $socket;
	private $databaseName;
	private $port;
	const ACTION_WRITE = 0x1;
	const ACTION_READ = 0x2;

	public function __construct($databaseName, $host = "127.0.0.1", $port = 5000) {
		$this->port = $port;
		$this->databaseName = $databaseName;
	}

	private function sendNumber($number) {
		if (socket_write($this->socket, pack("L", $number) . pack("L", 0)) === false) {
			throw new Exception("Could not write $number to socket");
		}
	}

	public function get($key) {
		$this->connect();

		$this->sendNumber(strlen($this->databaseName));
		socket_write($this->socket, $this->databaseName);

		$this->sendNumber(self::ACTION_READ);
		$this->sendNumber($key);

		$status = unpack("I", socket_read($this->socket, 8));

		if ($status[1] == 0) {
			$dataLength = unpack("I", socket_read($this->socket, 8));
			$dataLength = $dataLength[1];

			$readSoFar = 0;
			$str = "";
			while ($readSoFar < $dataLength) {
				$chunk = unpack("C*", socket_read($this->socket, $dataLength));
				$readSoFar += count($chunk);
				foreach ($chunk as $d) {
					$str .= chr($d);
				}
			}
			$this->close();

			return $str;
		}
		$this->close();
		return null;
	}

	public function set($key, $value) {
		$this->connect();

		$this->sendNumber(strlen($this->databaseName));
		socket_write($this->socket, $this->databaseName);

		$this->sendNumber(self::ACTION_WRITE);
		$this->sendNumber($key);
		$this->sendNumber(strlen($value));
		socket_write($this->socket, $value);

		$status = unpack("I", socket_read($this->socket, 8));
		$this->close();

		if ($status[1] === 0) {
			return true;
		} else {
			return false;
		}
	}

	private function connect() {
		if (($this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) == false) {
			throw new Exception("Could not create");
		}
		if (!@socket_connect($this->socket, "127.0.0.1", $this->port)) {
			throw new Exception("Could not connect");
		}
	}

	private function close() {
		socket_close($this->socket);
	}

}

?>
