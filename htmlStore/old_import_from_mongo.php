<?php

include("FileDB.php");

$scope = "htmlDump";

$fdb = new FileDB($scope);

$mongo = new MongoClient();

$db = $mongo->searchquant;

$collection = $db->{$scope};

$cursor = $collection->find();
$cursor->timeout(1000000);
//$cursor->skip(55974);
//$cursor->limit(1);
$count = 0;
foreach ($cursor as $document) {
	$fdb->set($document['id'], $document['data']);
	$count++;
	if ($count % 1000 == 0) {
		echo "$count\n";
	}
}

?>
