<?php

include("FileDB.php");

if (!isset($_GET['scope'])) die;
if (!isset($_GET['crawled_by_id'])) die;
if (!isset($_GET['id'])) die;

$scope = $_GET['scope'];
$crawled_by_id = $_GET['crawled_by_id'];
$id = $_GET['id'];

if ($scope == "htmlDump") {
	
	if ($id > 14434026) {
		try {
			$fdb = new FileDB($scope);
			echo $fdb->get($id);
			die;
		} catch (Exception $error) {
		}
	} else {
		$mongo = new MongoClient();

		$db = $mongo->searchquant;

		$collection = $db->{$scope};

		$document = array('crawled_by_id' => $crawled_by_id, 'id' => $id);
		$cursor = $collection->find($document);

		foreach ($cursor as $document) {
			echo $document['data'];
		}
	}
} else {

	if ($scope == "people_list" && $id <= 4462579) {
		try {
			$fdb = new FileDB($scope, "127.0.0.1", 5001);
			echo $fdb->get($id);
			die;
		} catch (Exception $error) {
		}
	}
	try {
		$fdb = new FileDB($scope);
		echo $fdb->get($id);
		die;
	} catch (Exception $error) {
	}
}

?>
