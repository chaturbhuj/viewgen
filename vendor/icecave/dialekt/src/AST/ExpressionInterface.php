<?php
namespace Icecave\Dialekt\AST;

/**
 * An AST node that is an expression.
 *
 * Not all nodes in the tree represent an entire (sub-)expression.
 */
interface ExpressionInterface extends NodeInterface
{
}
